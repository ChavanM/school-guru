﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddContractTemplate : System.Web.UI.Page
    {
        protected static int TemplateId;
        protected static int CustId;
        protected static int UId;
        protected static int RoleId;
        protected static List<Cont_SP_GetDepartmentUserMapping_Result> Deptlist;
        protected static List<Cont_tbl_UserDeptMapping> getmappingDetails;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                divsuccessmsgaCTem.Visible = false;
                if (!IsPostBack)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long templateID = 0;
                    BindDepartment();

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        getmappingDetails = (from row in entities.Cont_tbl_UserDeptMapping
                                             where row.IsActive == true
                                             && row.CustomerID == customerID
                                             select row).ToList();
                    }


                    if (!string.IsNullOrEmpty(Request.QueryString["accessID"]))
                    {
                        templateID = Convert.ToInt64(Request.QueryString["accessID"]);

                        var objRecord = ContractTemplateManagement.GetTemplateDetailsByID(templateID, customerID);

                        if (objRecord != null)
                        {
                            tbxTemplateName.Text = objRecord.TemplateName;
                            if (objRecord.ApproverCount != null)
                            {
                                tbxApprover.Text = objRecord.ApproverCount.ToString();
                            }
                            
                            //tbxVersion.Text = objRecord.Version;

                            #region Department

                            var lstDeptMapping = ContractManagement.GetDeptMapping(templateID,AuthenticationHelper.CustomerID);

                            if (lstBoxDepartment.Items.Count > 0)
                            {
                                lstBoxDepartment.ClearSelection();
                            }

                            if (lstDeptMapping.Count > 0)
                            {
                                foreach (var VendorID in lstDeptMapping)
                                {
                                    if (lstBoxDepartment.Items.FindByValue(VendorID.ToString()) != null)
                                        lstBoxDepartment.Items.FindByValue(VendorID.ToString()).Selected = true;
                                }
                            }

                            #endregion

                            if (objRecord.CreatedBy == AuthenticationHelper.UserID)
                            {
                                tbxTemplateName.Enabled = true;
                                tbxApprover.Enabled = true;
                                lstBoxDepartment.Enabled = true;
                                btnSaveSections.Enabled = true;
                                btnClearSections.Enabled = true;
                            }
                            else
                            {
                                tbxTemplateName.Enabled = false;
                                tbxApprover.Enabled = false;
                                lstBoxDepartment.Enabled = false;
                                btnSaveSections.Enabled = false;
                                btnClearSections.Enabled = false;
                            }
                            ViewState["Mode"] = 1;
                            ViewState["TemplateID"] = templateID;
                        }
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                        templateID = 0;
                    }
                    lnkBtnTemplate_Click(sender, e);                 
                    BindSections_All(grdTemplateSection, templateID);
                }

                if (ViewState["TemplateID"] != null)
                {
                    int newTemplateID = -1;
                    if (ViewState["TemplateID"] != null)
                    {
                        newTemplateID = Convert.ToInt32(ViewState["TemplateID"]);

                        RoleId = getRoleID(Convert.ToInt32(newTemplateID), AuthenticationHelper.UserID);
                        TemplateId = Convert.ToInt32(newTemplateID);
                        UId = Convert.ToInt32(AuthenticationHelper.UserID);
                        CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }

                    btnExportWord1.Enabled = true;
                    btnExportWord2.Enabled = true;
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', true);", lnkBtnImportSection.ClientID), true);
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', true);", lnkBtnEditor.ClientID), true);
                }
                else
                {
                    btnExportWord1.Enabled = false;
                    btnExportWord2.Enabled = false;
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', false);", lnkBtnImportSection.ClientID), true);
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', false);", lnkBtnEditor.ClientID), true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int userID = -1;
            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            //var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);
            var obj = ContractMastersManagement.GetContractUserDepartmentMapping_Paging(customerID);
          
            if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")            
                obj = obj.Where(x => x.UserID == userID).ToList();

            

            if (obj.Count > 0)
            {
                obj = (from row in obj
                       select row).GroupBy(a => a.DeptID).Select(a => a.FirstOrDefault()).ToList();
            }
            Deptlist = obj.ToList();
            lstBoxDepartment.DataTextField = "Name";
            lstBoxDepartment.DataValueField = "DeptID";

            lstBoxDepartment.DataSource = obj;
            lstBoxDepartment.DataBind();            
        }

        public static void DisableLinkButton(LinkButton linkButton)
        {
            linkButton.Attributes.Remove("href");
            linkButton.Attributes.CssStyle[HtmlTextWriterStyle.Color] = "gray";
            linkButton.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            if (linkButton.Enabled != false)
            {
                linkButton.Enabled = false;
            }

            if (linkButton.OnClientClick != null)
            {
                linkButton.OnClientClick = null;
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string sectionID = null;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal('" + sectionID + "','0');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnClearTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                tbxTemplateName.Text = string.Empty;
                tbxApprover.Text = string.Empty;
                //tbxVersion.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public bool ExporttoWordNew(int userID, int CustomerID, int TemplateID)
        {
            bool resultdata = false;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstSections = (from row in entities.Cont_SP_GetTemplates_Excel(CustomerID, userID, TemplateID)
                                   select row).FirstOrDefault();
                if (lstSections != null)
                {
                    resultdata = true;
                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                    string fileName = lstSections.TemplateName + "-" + lstSections.Version;
                    string templateName = lstSections.TemplateName;

                    strExporttoWord.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header: h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }

                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                .break { page-break-before: always; }
                -->
                </style></head>");

                    strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                    strExporttoWord.Append(lstSections.TemplateContent);

                    strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr>
                    <td>
                        <div style='mso-element:header' id=h1 >
                            <p class=MsoHeader style='text-align:left'>
                        </div>
                    </td>
                    <td>
                        <div style='mso-element:footer' id=f1>
                            <p class=MsoFooter>
                            <span style=mso-tab-count:2'></span>
                            <span style='mso-field-code:"" PAGE ""'></span>
                            of <span style='mso-field-code:"" NUMPAGES ""'></span>
                            </p>
                        </div>
                    </td>
                </tr>
                </table>
                </body>
                </html>");

                    fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                    Response.AppendHeader("Content-Type", "application/msword");
                    Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
                    Response.Write(strExporttoWord);
                }
            }
            return resultdata;
        }

        protected void btnExportWord_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TemplateID"] != null)
                {
                    long templateID = Convert.ToInt64(ViewState["TemplateID"]);

                    if (templateID != 0)
                    {
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        int userID = -1;
                        userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                        
                        bool result = ExporttoWordNew(userID, customerID, Convert.ToInt32(templateID));
                        if (!result)
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "No Section(s) Mapped to Selected Template";
                        }
                        //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        //{
                        //    var lstSections = (from row in entities.Cont_SP_GetTemplateSectionDetails(customerID, templateID)
                        //                       select row).ToList();
                        //    if (lstSections.Count > 0)
                        //    {
                        //        //GenerateTemplateDocx(lstSections);
                        //        ExporttoWord(lstSections);
                        //    }
                        //    else
                        //    {
                        //        cvAddEditTemplate.IsValid = false;
                        //        cvAddEditTemplate.ErrorMessage = "No Section(s) Mapped to Selected Template";
                        //    }
                        //}
                    }
                }
                else
                {
                    cvAddEditTemplate.IsValid = false;
                    cvAddEditTemplate.ErrorMessage = "No Section Mapped to Selected Template";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearSections_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                {
                    if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        TextBox txtOrder = (TextBox)grdTemplateSection.Rows[i].FindControl("txtOrder");
                        if (txtOrder != null)
                            txtOrder.Text = string.Empty;
                        //txtOrder.Text = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                bool validateSuccess = false;
                bool saveSuccess = false;
                List<string> lstErrorMsg = new List<string>();

                #region Validation

                if (!string.IsNullOrEmpty(tbxTemplateName.Text))
                    validateSuccess = true;
                else
                    lstErrorMsg.Add("Please Enter Template Name");

                if (!string.IsNullOrEmpty(tbxApprover.Text))
                    validateSuccess = true;
                else
                    lstErrorMsg.Add("Please Enter Approver");

                //if (!string.IsNullOrEmpty(tbxVersion.Text))
                //    validateSuccess = true;
                //else
                //    lstErrorMsg.Add("Required Version");

                int selectedSectionCount = 0;
                foreach (GridViewRow eachGridRow in grdTemplateSection.Rows)
                {
                    if (eachGridRow.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (CheckBox)eachGridRow.FindControl("chkRow");

                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                                selectedSectionCount++;
                        }
                    }
                }

                if (selectedSectionCount == 0)
                    lstErrorMsg.Add("Provide Select at lease one section to include in the template");
                else if (selectedSectionCount > 0)
                    validateSuccess = true;

                if (lstErrorMsg.Count > 0)
                {
                    validateSuccess = false;
                    ShowErrorMessages(lstErrorMsg, cvAddEditTemplate);
                }

                #endregion

                if (validateSuccess)
                {
                    Cont_tbl_TemplateMaster _objTemplate = new Cont_tbl_TemplateMaster()
                    {
                        TemplateName = tbxTemplateName.Text,
                        ApproverCount = Convert.ToInt32(tbxApprover.Text),
                        Version = "1.0",//tbxVersion.Text,

                        CustomerID = customerID,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (ContractTemplateManagement.ExistsTemplate(_objTemplate, customerID, 0))
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Template with Same Name and Version already exists";
                        }
                        else
                        {
                            long newTemplateID = 0;

                            newTemplateID = ContractTemplateManagement.CreateTemplate(_objTemplate);

                            if (newTemplateID > 0)
                            {
                                saveSuccess = true;

                                //Save Mapping Template Section
                                saveSuccess = Save_TemplateSectionMapping(newTemplateID);

                                //clearControls();
                                ViewState["TemplateID"] = newTemplateID;
                            }

                            if (saveSuccess)
                            {
                                //cvAddEditTemplate.IsValid = false;
                                //cvAddEditTemplate.ErrorMessage = "Template Save Successfully.";
                                //vsAddEditTemplate.CssClass = "alert alert-success";
                                if (cvAddEditTemplate.IsValid)
                                {
                                    divsuccessmsgaCTem.Visible = true;
                                    successmsgaCTem.Text = "Template Save Successfully.";
                              ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                                }

                            }
                            else
                            {
                                divsuccessmsgaCTem.Visible = false;
                                cvAddEditTemplate.IsValid = false;
                                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        _objTemplate.ID = Convert.ToInt64(ViewState["TemplateID"]);

                        if (ContractTemplateManagement.ExistsTemplate(_objTemplate, customerID, _objTemplate.ID))
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Template with Same Name and Version already exists";
                        }
                        else
                        {
                            _objTemplate.UpdatedBy = AuthenticationHelper.UserID;
                            _objTemplate.UpdatedOn = DateTime.Now;

                            saveSuccess = ContractTemplateManagement.UpdateTemplate(_objTemplate);

                            if (saveSuccess)
                            {
                                saveSuccess = Save_TemplateSectionMapping(_objTemplate.ID);
                            }

                            if (saveSuccess)
                            {
                                //cvAddEditTemplate.IsValid = false;
                                //cvAddEditTemplate.ErrorMessage = "Template Updated Successfully.";
                                //vsAddEditTemplate.CssClass = "alert alert-success";
                                if (cvAddEditTemplate.IsValid)
                                {
                                    divsuccessmsgaCTem.Visible = true;
                                    successmsgaCTem.Text = "Template Updated Successfully.";
                              ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                                }


                            }
                            else
                            {
                                cvAddEditTemplate.IsValid = false;
                                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                                divsuccessmsgaCTem.Visible = false;
                            }
                        }
                    }
                }

                upAddEditTemplate.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                divsuccessmsgaCTem.Visible = false;
            }
        }

        protected void btnSaveTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                SaveTemplate(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = SaveTemplate(false);

                if (saveSuccess)
                {
                    lnkBtnImportSection_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSaveSections_Click(object sender, EventArgs e)
        {
            try
            {
                int FlagGridValidation = 0;
                int FlagGridDeptValidation = 0;
                int FlagGridValidationorder = 0;
                int notchecked = 0;
                if (grdTemplateSection != null)
                {
                    for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                    {
                        if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chkRow = (CheckBox)grdTemplateSection.Rows[i].FindControl("chkRow");
                            if (chkRow != null)
                            {
                                if (chkRow.Checked)
                                {
                                    notchecked = 1;
                                    int ReviewerId = -1;
                                    DropDownList ddluser = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlReviewer");
                                    if (ddluser.SelectedValue != "" && ddluser.SelectedValue != "-1" && ddluser.SelectedValue != "0")
                                    {
                                        ReviewerId = Convert.ToInt32(ddluser.SelectedValue);
                                    }
                                    if (ReviewerId == -1)
                                    {
                                        FlagGridValidation = 1;
                                    }

                                    int DeptId = -1;
                                    DropDownList ddldpetsection = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddldpetsection");
                                    if (ddldpetsection.SelectedValue != "" && ddldpetsection.SelectedValue != "-1" && ddldpetsection.SelectedValue != "0")
                                    {
                                        DeptId = Convert.ToInt32(ddldpetsection.SelectedValue);
                                    }
                                    if (DeptId == -1)
                                    {
                                        FlagGridDeptValidation = 1;
                                    }

                                    TextBox txtOrder = (TextBox)grdTemplateSection.Rows[i].FindControl("txtOrder");
                                    if (!string.IsNullOrEmpty(txtOrder.Text.Trim()))
                                    {
                                        if (!string.IsNullOrEmpty(txtOrder.Text))
                                        {

                                        }
                                        else
                                            FlagGridValidationorder = 1;
                                    }
                                    else
                                        FlagGridValidationorder = 1;                                    
                                }
                            }
                        }
                    }
                }
                
                bool validateSuccess1 = false;               
                List<string> lstErrorMsg1 = new List<string>();

                #region Validation

                if (FlagGridValidationorder == 0)
                    validateSuccess1 = true;
                else
                    lstErrorMsg1.Add("Please enter Section Order.");

                if (FlagGridDeptValidation == 0)
                    validateSuccess1 = true;
                else
                    lstErrorMsg1.Add("Please select department of Section.");

                if (FlagGridValidation == 0)
                    validateSuccess1 = true;
                else
                    lstErrorMsg1.Add("Please provide Reviewer of each Section.");

                if (!string.IsNullOrEmpty(tbxTemplateName.Text.Trim()))
                    validateSuccess1 = true;
                else
                    lstErrorMsg1.Add("Please enter Template Name.");

                if (!string.IsNullOrEmpty(tbxApprover.Text.Trim()))
                {
                    if (Convert.ToInt32(tbxApprover.Text) > 0)
                    {
                        validateSuccess1 = true;
                    }
                    else
                        lstErrorMsg1.Add("Required Approver");
                }                    
                else
                    lstErrorMsg1.Add("Required Approver");
                
                if (notchecked == 0)
                    lstErrorMsg1.Add("Please select at lease one Section.");
                else
                    validateSuccess1 = true;

                if (lstErrorMsg1.Count > 0)
                {
                    validateSuccess1 = false;
                    ShowErrorMessages(lstErrorMsg1, cvAddEditTemplate);
                }
                #endregion


                if (validateSuccess1)
                {
                    //SaveTemplate(true);
                    bool saveSuccessdetail = SaveTemplate(false);

                    if (saveSuccessdetail)
                    {
                        if (ViewState["TemplateID"] != null)
                        {
                            List<long> lstDeptMapping = new List<long>();
                            if (lstBoxDepartment.Items.Count > 0)
                            {
                                foreach (ListItem eachdept in lstBoxDepartment.Items)
                                {
                                    if (eachdept.Selected)
                                    {
                                        if (Convert.ToInt64(eachdept.Value) != 0)
                                            lstDeptMapping.Add(Convert.ToInt64(eachdept.Value));
                                    }
                                }
                            }
                            if (lstDeptMapping.Count > 0)
                            {
                                long newTemplateID = Convert.ToInt64(ViewState["TemplateID"]);
                                List<Cont_tbl_TemplateDepartmentMapping> lstDeptMapping_ToSave = new List<Cont_tbl_TemplateDepartmentMapping>();

                                lstDeptMapping.ForEach(EachVendor =>
                                {
                                    Cont_tbl_TemplateDepartmentMapping _DeptMappingRecord = new Cont_tbl_TemplateDepartmentMapping()
                                    {
                                        TemplateID = newTemplateID,
                                        DeptID = EachVendor,
                                        IsActive = true,
                                        CustomerID = AuthenticationHelper.CustomerID
                                    };
                                    lstDeptMapping_ToSave.Add(_DeptMappingRecord);
                                });
                                bool deactivatedetails = ContractManagement.DeactivateDeptContractMapping(Convert.ToInt32(newTemplateID), Convert.ToInt32(AuthenticationHelper.CustomerID));
                                bool saveDeptSuccess = ContractManagement.CreateUpdate_DeptMapping(lstDeptMapping_ToSave);
                                if (saveDeptSuccess)
                                {
                                    ContractManagement.CreateAuditLog("C", newTemplateID, "Cont_tbl_TemplateDepartmentMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Department Mapping Created", true, Convert.ToInt32(newTemplateID));
                                }
                            }

                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            bool validateSuccess = false;
                            bool saveSuccess = false;
                            List<string> lstErrorMsg = new List<string>();

                            #region Validation

                            lstErrorMsg.Clear();

                            int selectedSectionCount = 0;
                            foreach (GridViewRow eachGridRow in grdTemplateSection.Rows)
                            {
                                if (eachGridRow.RowType == DataControlRowType.DataRow)
                                {
                                    CheckBox chkRow = (CheckBox)eachGridRow.FindControl("chkRow");

                                    if (chkRow != null)
                                    {
                                        if (chkRow.Checked)
                                            selectedSectionCount++;
                                    }
                                }
                            }

                            if (selectedSectionCount == 0)
                                lstErrorMsg.Add("Provide Select at least one section to include in the template");
                            else if (selectedSectionCount > 0)
                                validateSuccess = true;

                            if (lstErrorMsg.Count > 0)
                            {
                                validateSuccess = false;
                                ShowErrorMessages(lstErrorMsg, cvAddEditTemplate);
                            }

                            #endregion

                            if (validateSuccess)
                            {
                                long newTemplateID = Convert.ToInt64(ViewState["TemplateID"]);

                                //Save Mapping Template Section
                                
                                saveSuccess = Save_TemplateSectionMapping(newTemplateID);

                                if (saveSuccess)
                                {
                                    if (cvAddEditTemplate.IsValid)
                                    {
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            var entitiesData = (from row in entities.Cont_tbl_TemplateStatusMapping
                                                                where row.TemplateID == newTemplateID
                                                                && row.CustomerID == customerID
                                                                select row).FirstOrDefault();
                                            if (entitiesData != null)
                                            {
                                                entitiesData.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                entitiesData.UpdatedOn = DateTime.Now;
                                                entitiesData.IsActive = true;
                                                entitiesData.Status = false;
                                                entities.SaveChanges();
                                            }
                                        }
                                        lnkBtnImportSection_Click(sender, e);
                                    }
                                }
                                else
                                {
                                    cvAddEditTemplate.IsValid = false;
                                    cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                                }
                            }
                            upAddEditTemplate.Update();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        public int getRoleID(int TemplateId, int UserID)
        {
            int RoleID = -1;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TemplateAssignment data = (from row in entities.TemplateAssignments
                                           where row.TemplateInstanceID == TemplateId
                                           && row.UserID == UserID
                                           select row).FirstOrDefault();
                if (data != null)
                {
                    RoleID = Convert.ToInt32(data.RoleID);
                }
            }
            return RoleID;
        }
        public bool Save_TemplateSectionMapping(long templateID)
        {
            bool saveSuccess = false;

            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            ContractTemplateManagement.DeleteTemplateAssignment(Convert.ToInt32(templateID), 3);
            ContractTemplateManagement.DeleteTemplateAssignment(Convert.ToInt32(templateID), 4);
            if (grdTemplateSection != null)
            {
                List<Cont_tbl_TemplateSectionMapping> lstObjMapping_ToSave = new List<Cont_tbl_TemplateSectionMapping>();

                for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                {
                    if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (CheckBox)grdTemplateSection.Rows[i].FindControl("chkRow");
                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                            {
                                int ReviewerId = -1;
                                int DepartmentId = -1;
                                Label lblSectionID = (Label)grdTemplateSection.Rows[i].FindControl("lblSectionID");
                                Label lblSectionvisibility = (Label)grdTemplateSection.Rows[i].FindControl("lblSectionvisibility");
                                
                                DropDownList ddluser = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlReviewer");
                                //DropDownList VisibilitySectionHeader = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlVisibilitySectionHeader");

                                if (ddluser.SelectedValue != "" && ddluser.SelectedValue != "-1")
                                {
                                    ReviewerId = Convert.ToInt32(ddluser.SelectedValue);
                                }

                                DropDownList ddldpetsection = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddldpetsection");
                                if (ddldpetsection.SelectedValue != "" && ddldpetsection.SelectedValue != "-1")
                                {
                                    DepartmentId = Convert.ToInt32(ddldpetsection.SelectedValue);
                                }

                                if (lblSectionID != null && ReviewerId != -1 && DepartmentId != -1)
                                {
                                    if (!string.IsNullOrEmpty(lblSectionID.Text) || lblSectionID.Text != "0")
                                    {
                                        TemplateAssignment templateAssignment = new TemplateAssignment()
                                        {
                                            TemplateInstanceID = templateID,
                                            RoleID = 4,
                                            UserID = ReviewerId,
                                            IsActive = true,
                                            SectionID = Convert.ToInt32(lblSectionID.Text)
                                        };
                                        ContractTemplateManagement.CreateTemplateAssignment(templateAssignment);

                                        Cont_tbl_TemplateSectionMapping _objMapping = new Cont_tbl_TemplateSectionMapping()
                                        {
                                            TemplateID = templateID,
                                            SectionID = Convert.ToInt64(lblSectionID.Text),
                                            CustomerID = customerID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            UpdatedOn = DateTime.Now,
                                            Headervisibilty = Convert.ToInt32(lblSectionvisibility.Text),
                                            DeptID = DepartmentId
                                        };

                                        TextBox txtOrder = (TextBox)grdTemplateSection.Rows[i].FindControl("txtOrder");
                                        if (!string.IsNullOrEmpty(txtOrder.Text.Trim()))
                                        {
                                            if (!string.IsNullOrEmpty(txtOrder.Text))
                                                _objMapping.SectionOrder = Convert.ToInt32(txtOrder.Text);

                                        }
                                        lstObjMapping_ToSave.Add(_objMapping);                                       
                                    }
                                }
                            }
                        }
                    }
                }//End For Each

                bool MakeFalseAllTemplate = ContractTemplateManagement.GetTemplateSectionMapping(templateID, AuthenticationHelper.UserID);
                if (MakeFalseAllTemplate)
                {
                    saveSuccess = ContractTemplateManagement.DeActiveExistingTemplateSectionMapping(templateID);
                    saveSuccess = ContractTemplateManagement.CreateUpdate_TemplateSectionMapping(lstObjMapping_ToSave);
                }
                #region Trasanction Entry

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstSections = (from row in entities.Cont_SP_GetTemplateSectionDetails(customerID, templateID)
                                       select row).ToList();
                    if (lstSections.Count > 0)
                    {
                        lstSections = lstSections.OrderBy(row => row.SectionOrder).ToList();
                        foreach (var item in lstSections)
                        {                           
                            TemplateAssignment templateAssignment = new TemplateAssignment()
                            {
                                TemplateInstanceID = templateID,
                                RoleID = 3,
                                UserID = AuthenticationHelper.UserID,
                                IsActive = true,
                                SectionID = Convert.ToInt32(item.SectionID)
                            };
                            ContractTemplateManagement.CreateTemplateAssignment(templateAssignment);

                            TemplateTransaction obj = new TemplateTransaction();
                            obj.TemplateInstanceID = templateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = AuthenticationHelper.UserID;
                            obj.TemplateContent = item.BodyContent;
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            obj.Status = 1;
                            obj.OrderID = Convert.ToInt32(item.SectionOrder);//1
                            obj.SectionID = Convert.ToInt32(item.SectionID);
                            entities.TemplateTransactions.Add(obj);
                            entities.SaveChanges();
                        }

                        RoleId = getRoleID(Convert.ToInt32(templateID), AuthenticationHelper.UserID);
                        TemplateId = Convert.ToInt32(templateID);
                        UId = Convert.ToInt32(AuthenticationHelper.UserID);
                        CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        #region set transaction history
                        var entitiesData = (from row in entities.Cont_SP_GetSectionsDetail(customerID, Convert.ToInt32(TemplateId))
                                            select row).ToList();


                        var data1 = (from row in entities.Sp_GetTemplateTransaction()
                                     where row.UserID == UId
                                     && row.TemplateInstanceID == TemplateId
                                     && row.RoleID == RoleId
                                     select row).ToList();
                        if (data1.Count > 0)
                        {
                            var data = (from row in data1
                                        join row1 in entities.Cont_tbl_TemplateSectionMapping
                                        on row.sectionID equals row1.SectionID
                                        where row1.TemplateID == TemplateId
                                        select new getTemplateTransactionDetail
                                        {
                                            TemplateTransactionID = row.TemplateTransactionID,
                                            TemplateInstanceID = row.TemplateInstanceID,
                                            ComplianceStatusID = row.ComplianceStatusID,
                                            CreatedOn = row.CreatedOn,
                                            Status = row.Status,
                                            StatusChangedOn = row.StatusChangedOn,
                                            TemplateContent = row.TemplateContent,
                                            RoleID = row.RoleID,
                                            UserID = row.UserID,
                                            sectionID = row.sectionID,
                                            orderID = Convert.ToInt32(row1.SectionOrder),
                                            visibilty = Convert.ToInt32(row1.Headervisibilty),
                                            sectionHeader = row.SectionHeader
                                        }).OrderBy(x => x.orderID).ToList();

                            System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                            data.ForEach(eachSection =>
                            {
                                if (eachSection.visibilty == 1)
                                {
                                    strExporttoWord.Append(@"<div>");
                                    strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                    strExporttoWord.Append(@"</div>");
                                }

                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                strExporttoWord.Append(@"</div>");
                            });

                            TemplateTransactionHistory obj = new TemplateTransactionHistory();
                            obj.TemplateID = templateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = AuthenticationHelper.UserID;
                            obj.TemplateContent = strExporttoWord.ToString(); 
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            obj.Status = 1;
                            obj.OrderID = 1;
                            entities.TemplateTransactionHistories.Add(obj);
                            entities.SaveChanges();
                        }
                        #endregion
                    }
                }
                #endregion
            }

            return saveSuccess;
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }

        public bool SaveTemplate(bool showMsg)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            bool validateSuccess = false;
            bool saveSuccess = false;
            List<string> lstErrorMsg = new List<string>();

            #region Validation

            if (!string.IsNullOrEmpty(tbxTemplateName.Text.Trim()))
                validateSuccess = true;
            else
                lstErrorMsg.Add("Required Template Name");

            //if (!string.IsNullOrEmpty(tbxVersion.Text.Trim()))
            //    validateSuccess = true;
            //else
            //    lstErrorMsg.Add("Required Version");

            if (lstErrorMsg.Count > 0)
            {
                validateSuccess = false;
                ShowErrorMessages(lstErrorMsg, cvAddEditTemplate);
            }

            #endregion

            if (validateSuccess)
            {
                Cont_tbl_TemplateMaster _objTemplate = new Cont_tbl_TemplateMaster()
                {
                    TemplateName = tbxTemplateName.Text,
                    ApproverCount = Convert.ToInt32(tbxApprover.Text),
                    Version = "1.0",//tbxVersion.Text,

                    CustomerID = customerID,
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false
                };

                if ((int)ViewState["Mode"] == 0)
                {
                    if (ContractTemplateManagement.ExistsTemplate(_objTemplate, customerID, 0))
                    {
                        cvAddEditTemplate.IsValid = false;
                        cvAddEditTemplate.ErrorMessage = "Template with Same Name and Version already exists";
                    }
                    else
                    {
                        long newTemplateID = 0;

                        newTemplateID = ContractTemplateManagement.CreateTemplate(_objTemplate);

                        if (newTemplateID > 0)
                        {                            
                            saveSuccess = true;

                            ViewState["Mode"] = 1;
                            //clearControls();
                            ViewState["TemplateID"] = newTemplateID;

                            if (ViewState["TemplateID"] != null)
                            {
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', true);", lnkBtnImportSection.ClientID), true);
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', false);", lnkBtnEditor.ClientID), true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', false);", lnkBtnImportSection.ClientID), true);
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', false);", lnkBtnEditor.ClientID), true);
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', false);", lnkBtnImportSection.ClientID), true);
                            }
                        }

                        if (saveSuccess)
                        {
                            if (showMsg)
                            {
                                if (cvAddEditTemplate.IsValid)
                                {
                                    divsuccessmsgaCTem.Visible = true;
                                    successmsgaCTem.Text = "Template Save Successfully.";
                                   ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                                }
                            }
                        }
                        else
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                            divsuccessmsgaCTem.Visible = false;
                        }
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (ViewState["TemplateID"] != null)
                    {
                        _objTemplate.ID = Convert.ToInt64(ViewState["TemplateID"]);

                        if (ContractTemplateManagement.ExistsTemplate(_objTemplate, customerID, _objTemplate.ID))
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Template with Same Name and Version already exists";
                            divsuccessmsgaCTem.Visible = false;
                        }
                        else
                        {
                            _objTemplate.UpdatedBy = AuthenticationHelper.UserID;
                            _objTemplate.UpdatedOn = DateTime.Now;

                            saveSuccess = ContractTemplateManagement.UpdateTemplate(_objTemplate);

                            if (saveSuccess)
                            {
                                if (showMsg)
                                {
                                    //cvAddEditTemplate.IsValid = false;
                                    //cvAddEditTemplate.ErrorMessage = "Template Updated Successfully.";
                                    //vsAddEditTemplate.CssClass = "alert alert-success";
                                    if (cvAddEditTemplate.IsValid)
                                    {
                                        divsuccessmsgaCTem.Visible = true;
                                        successmsgaCTem.Text = "Template Updated Successfully.";
                                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                                    }
                                }
                            }
                            else
                            {
                                cvAddEditTemplate.IsValid = false;
                                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                                divsuccessmsgaCTem.Visible = false;
                            }
                        }
                    }
                }
            }
            upAddEditTemplate.Update();
            return saveSuccess;
        }

        public void ShowErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }

        public void BindSections_All(GridView grdTemplateSection, long templateID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userID = -1;
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                long sectionID = 0;

                string Filter = string.Empty;

                grdTemplateSection.DataSource = ContractTemplateManagement.GetSections_All(customerID, tbxFilter.Text.Trim().ToString());
                grdTemplateSection.DataBind();

                if (templateID != 0)
                {
                    btnSaveSections.Enabled = false;
                    var prevTemplateSectionMapping = ContractTemplateManagement.GetExistingTemplateSectionMapping(templateID);

                    if (prevTemplateSectionMapping != null)
                    {
                        if (prevTemplateSectionMapping.Count > 0)
                        {
                            List<object> internalUsers = new List<object>();

                            var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                            //lstAllUsers = lstAllUsers.Where(x => x.ID != userID).ToList();

                            if (lstAllUsers.Count > 0)
                                lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();
                            
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var getData = (from row in entities.TemplateAssignments
                                                  where row.TemplateInstanceID == templateID
                                                  //&& row.SectionID == sectionID
                                                  && row.IsActive == true
                                                  && row.RoleID == 4
                                                  select row).ToList();

                                if (true)//getData.Count > 0
                                {
                                    for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                                    {
                                        if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                                        {
                                            Label lblSectionID = (Label)grdTemplateSection.Rows[i].FindControl("lblSectionID");

                                            if (lblSectionID != null)
                                            {
                                                sectionID = Convert.ToInt64(lblSectionID.Text);

                                                if (sectionID != 0)
                                                {
                                                    if (prevTemplateSectionMapping.Select(row => row.SectionID).Contains(sectionID))
                                                    {
                                                        CheckBox chkRow = (CheckBox)grdTemplateSection.Rows[i].FindControl("chkRow");

                                                        if (chkRow != null)
                                                        {
                                                            chkRow.Checked = true;
                                                        }

                                                        TextBox txtOrder = (TextBox)grdTemplateSection.Rows[i].FindControl("txtOrder");

                                                        var secTemplateMappingRecord = prevTemplateSectionMapping.Where(row => row.SectionID == sectionID).FirstOrDefault();
                                                        
                                                        if (txtOrder != null)
                                                        {                                                            
                                                            if (secTemplateMappingRecord != null)
                                                            {
                                                                txtOrder.Text = secTemplateMappingRecord.SectionOrder.ToString();
                                                            }
                                                        }

                                                        DropDownList ddldpetsection = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddldpetsection");
                                                        
                                                        if (ddldpetsection != null)
                                                        {
                                                            ddldpetsection.SelectedValue = Convert.ToString(secTemplateMappingRecord.DeptID);
                                                        }

                                                        if (getData.Count > 0)
                                                        {
                                                            var getdetails = (from row in getData
                                                                              where row.TemplateInstanceID == templateID
                                                                              && row.SectionID == sectionID
                                                                              //&& row.IsActive == true
                                                                              //&& row.RoleID == 4
                                                                              select row).FirstOrDefault();

                                                            //var getdetails = (from row in entities.TemplateAssignments
                                                            //                  where row.TemplateInstanceID == templateID
                                                            //                  && row.SectionID == sectionID
                                                            //                  && row.IsActive == true
                                                            //                  && row.RoleID == 4
                                                            //                  select row).FirstOrDefault();

                                                            if (getdetails != null)
                                                            {
                                                                DropDownList ddlUser = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlReviewer");

                                                                int DEPTID = Convert.ToInt32(secTemplateMappingRecord.DeptID);
                                                                
                                                                List<long> getdata1 = (from row in getmappingDetails
                                                                                       where row.DeptID == DEPTID
                                                                                       select row.UserID).ToList();

                                                                var lstAllUsers1 = lstAllUsers.Where(x => getdata1.Contains(x.ID)).ToList();

                                                                internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers1, 1); 

                                                                ddlUser.DataValueField = "ID";
                                                                ddlUser.DataTextField = "Name";
                                                                ddlUser.DataSource = internalUsers;
                                                                ddlUser.DataBind();
                                                                
                                                                if (ddlUser != null)
                                                                {
                                                                    ddlUser.SelectedValue = Convert.ToString(getdetails.UserID);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } //END FOR
                        }
                    }

                    prevTemplateSectionMapping.Clear();
                    prevTemplateSectionMapping = null;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnTemplate_Click(object sender, EventArgs e)
        {
            liTemplate.Attributes.Add("class", "active");
            liImportSection.Attributes.Add("class", "");
            liTransactionTemplate.Attributes.Add("class", "");
            //liUpdateTemplate.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myModal", "pageLoad();", true);

        }

        protected void lnkBtnImportSection_Click(object sender, EventArgs e)
        {            
            liTemplate.Attributes.Add("class", "");
            liImportSection.Attributes.Add("class", "active");
            liTransactionTemplate.Attributes.Add("class", "");
            
            MainView.ActiveViewIndex = 1;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "countSelected", "checkUncheckRow();", true);            
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenTransaction('" + TemplateId + "','" + CustId + "','" + UId + "','" + RoleId + "');", true);
        }

        protected void lnkBtnRebind_Sections_Click(object sender, EventArgs e)
        {
            if (ViewState["TemplateID"] != null)
            {
                long templateID = Convert.ToInt64(ViewState["TemplateID"]);

                BindSections_All(grdTemplateSection, templateID);
            }
        }

        protected void grdTemplateSections_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (e.CommandName.Equals("View_Section"))
                {
                    long sectionID = Convert.ToInt64(e.CommandArgument);

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal('" + sectionID + "','1');", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong. Please try again";
            }
        }

        public void ExporttoWord(List<Cont_SP_GetTemplateSectionDetails_Result> lstSections)
        {
            if (lstSections.Count > 0)
            {
                System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                string fileName = "Template";
                string templateName = "Template";

                if (lstSections[0] != null)
                {
                    templateName = lstSections[0].TemplateName;
                    fileName = templateName + "-" + lstSections[0].TemplateVersion;
                }

                strExporttoWord.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header: h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }

                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                .break { page-break-before: always; }
                -->
                </style></head>");

                strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                //strExporttoWord.Append(@"<table style='width:100%; font-family:Calibri;border:none;'>");

                lstSections = lstSections.OrderBy(row => row.SectionOrder).ToList();

                strExporttoWord.Append(@"<div style='text-align:center;'>");
                strExporttoWord.Append(@"<p><b>" + templateName + "</b></p>");
                strExporttoWord.Append(@"</div>");

                lstSections.ForEach(eachSection =>
                {
                    strExporttoWord.Append(@"<div>");
                    strExporttoWord.Append(@"<p><b>" + eachSection.Header + "</b></p>");
                    strExporttoWord.Append(@"</div>");

                    strExporttoWord.Append(@"<div>");
                    strExporttoWord.Append(@"<p>" + eachSection.BodyContent + "</p>");
                    strExporttoWord.Append(@"</div>");

                    //strExporttoWord.Append(@"<tr>");
                    //strExporttoWord.Append(@"<td><b>" + eachSection.Header + "</b></td>");
                    //strExporttoWord.Append(@"</tr>");

                    //strExporttoWord.Append(@"<tr>");
                    //strExporttoWord.Append(@"<td>" + eachSection.BodyContent + "</td>");
                    //strExporttoWord.Append(@"</tr>");
                });

                //strExporttoWord.Append(@"</table>");

                strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr>
                    <td>
                        <div style='mso-element:header' id=h1 >
                            <p class=MsoHeader style='text-align:left'>
                        </div>
                    </td>
                    <td>
                        <div style='mso-element:footer' id=f1>
                            <p class=MsoFooter>
                            <span style=mso-tab-count:2'></span>
                            <span style='mso-field-code:"" PAGE ""'></span>
                            of <span style='mso-field-code:"" NUMPAGES ""'></span>
                            </p>
                        </div>
                    </td>
                </tr>
                </table>
                </body>
                </html>");

                fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                Response.AppendHeader("Content-Type", "application/msword");
                Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
                Response.Write(strExporttoWord);
            }
        }

        //protected void lnkBtnEditor_Click(object sender, EventArgs e)
        //{
        //    liTemplate.Attributes.Add("class", "");
        //    liImportSection.Attributes.Add("class", "");
        //    //liUpdateTemplate.Attributes.Add("class", "active");

        //    MainView.ActiveViewIndex = 2;
        //    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "countSelected", "checkUncheckRow();", true);
        //}

        protected void lnkBtnTransactionHistry_Click(object sender, EventArgs e)
        {

            TemplateId = -1;
            RoleId = -1;
            UId = -1;
            CustId = -1;
            if (ViewState["TemplateID"] != null)
            {
                TemplateId = Convert.ToInt32(ViewState["TemplateID"]);
                RoleId = getRoleID(Convert.ToInt32(TemplateId), AuthenticationHelper.UserID);
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            liTemplate.Attributes.Add("class", "");
            liImportSection.Attributes.Add("class", "");
            liTransactionTemplate.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 2;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenTransactionHistory('" + TemplateId + "','" + CustId + "','" + UId + "','" + RoleId + "');", true);
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "countSelected", "checkUncheckRow();", true);
        }

        protected void grdTemplateSection_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTime"]);
                Label lblDeptID = (Label)e.Row.FindControl("lblDeptID");
                DropDownList ddldpetsection = (DropDownList)e.Row.FindControl("ddldpetsection");
                ddldpetsection.DataValueField = "DeptID";
                ddldpetsection.DataTextField = "Name";
                ddldpetsection.DataSource = Deptlist;
                ddldpetsection.DataBind();
                //if (lblDeptID != null)
                //{
                //    if (!string.IsNullOrEmpty(lblDeptID.Text) && lblDeptID.Text != "0")
                //    {
                //        ddldpetsection.SelectedValue = lblDeptID.Text.ToString();
                //    }
                //}
                //if (lblDeptID != null)
                //{
                //    if (!string.IsNullOrEmpty(lblDeptID.Text) && lblDeptID.Text != "0")
                //    {
                //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //        {
                //            int deptId = Convert.ToInt32(lblDeptID.Text);
                //            DropDownList ddlUser = (DropDownList)e.Row.FindControl("ddlReviewer");

                //            int customerID = -1;
                //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //            int userID = -1;
                //            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                //            List<object> internalUsers = new List<object>();
                //            string CacheName = "internalUsersforContract" + userID + "_" + customerID;
                //            internalUsers = (List<object>)HttpContext.Current.Cache[CacheName];
                //            if (internalUsers == null)
                //            {
                //                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                //                lstAllUsers = lstAllUsers.Where(x => x.ID != userID).ToList();
                //                if (lstAllUsers.Count > 0)
                //                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                //                //List<long> getdata = (from row in entities.Cont_tbl_UserDeptMapping
                //                //                      where row.DeptID == deptId
                //                //                      && row.IsActive == true
                //                //                      && row.CustomerID == customerID
                //                //                      select row.UserID).ToList();
                //                //    lstAllUsers = lstAllUsers.Where(x => getdata.Contains(x.ID)).ToList();

                //                internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                //                HttpContext.Current.Cache.Insert(CacheName, internalUsers, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                //            }
                //            ddlUser.DataValueField = "ID";
                //            ddlUser.DataTextField = "Name";
                //            ddlUser.DataSource = internalUsers;
                //            ddlUser.DataBind();
                //        }
                //    }
                //}
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {           
            try
            {
                long templateID = 0;
                if (ViewState["TemplateID"] != null)
                {
                    templateID = Convert.ToInt64(ViewState["TemplateID"]);
                }

                BindSections_All(grdTemplateSection, templateID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        protected void ddldpetsection_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList selectedDropDown = (DropDownList)sender;
            GridViewRow selectedRow = (GridViewRow)selectedDropDown.Parent.Parent;
            int i = selectedRow.RowIndex;

            DropDownList ddldpetsection = (DropDownList)selectedRow.Cells[2].FindControl("ddldpetsection");
            DropDownList ddlReviewer = (DropDownList)selectedRow.Cells[3].FindControl("ddlReviewer");
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int deptId = Convert.ToInt32(ddldpetsection.Text);
                
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTime"]);

                int userID = -1;
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                List<object> internalUsers = new List<object>();

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                lstAllUsers = lstAllUsers.Where(x => x.ID != userID).ToList();
                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                List<long> getdata1 = (from row in getmappingDetails
                                      where row.DeptID == deptId
                                      select row.UserID).ToList();

                lstAllUsers = lstAllUsers.Where(x => getdata1.Contains(x.ID)).ToList();

                internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1);

                ddlReviewer.DataValueField = "ID";
                ddlReviewer.DataTextField = "Name";
                ddlReviewer.DataSource = internalUsers;
                ddlReviewer.DataBind();
            }            
        }
    }
}