﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddContSubType : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            divsuccessmsgaCSubType.Visible = false;
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["ContractSubTypeId"]))
                    {
                        long subTypeID = Convert.ToInt64(Request.QueryString["ContractSubTypeId"]);

                        Cont_tbl_SubTypeMaster objContractSubTypeRecord = ContractTypeMasterManagement.GetContractSubTypeDetailByID(subTypeID, Convert.ToInt32(CustomerID));

                        if (objContractSubTypeRecord != null)
                        {
                            tbxSubContType.Text = objContractSubTypeRecord.SubTypeName;
                            ViewState["Mode"] = 1;
                            ViewState["ContSubTypeID"] = subTypeID;
                        }
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    divsuccessmsgaCSubType.Visible = false;
                    cvAddEditSubType.IsValid = false;
                    cvAddEditSubType.ErrorMessage = "Server Error Occurred. Please try again.";
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ContractTypeID"] != null && ViewState["Mode"] != null)
                {
                    ViewState["ContractTypeID"] = Session["ContractTypeID"];

                    Cont_tbl_SubTypeMaster objSubType = new Cont_tbl_SubTypeMaster()
                    {
                        SubTypeName = tbxSubContType.Text,
                        CustomerID = (int)CustomerID,
                        ContractTypeID = Convert.ToInt64(ViewState["ContractTypeID"])
                    };

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (ContractTypeMasterManagement.ExistsSubContractType(objSubType, 0))
                        {
                            divsuccessmsgaCSubType.Visible = false;
                            cvAddEditSubType.IsValid = false;
                            cvAddEditSubType.ErrorMessage = "Contract Sub-Type already exists.";
                        }
                        else
                        {
                            long newSubTypeID = 0;

                            objSubType.CreatedBy = AuthenticationHelper.UserID;
                            objSubType.CreatedOn = DateTime.Now;
                            objSubType.IsDeleted = false;

                            newSubTypeID = ContractTypeMasterManagement.CreateContractSubType(objSubType);

                            if (newSubTypeID > 0)
                            {
                                //cvAddEditSubType.IsValid = false;
                                //cvAddEditSubType.ErrorMessage = "Contract Sub-Type Save Successfully";
                                //vsAddEditSubType.CssClass = "alert alert-success";
                                if (cvAddEditSubType.IsValid)
                                {
                                    divsuccessmsgaCSubType.Visible = true;
                                    successmsgaCSubType.Text = "Contract Sub-Type Saved Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCSubType.ClientID + "').style.display = 'none' },2000);", true);
                                }
                                tbxSubContType.Text = string.Empty;

                                Session["subTypeID"] = newSubTypeID;
                            }
                            else
                            {
                                divsuccessmsgaCSubType.Visible = false;
                                cvAddEditSubType.IsValid = false;
                                cvAddEditSubType.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        objSubType.ID = Convert.ToInt64(ViewState["ContSubTypeID"]);

                        if (ContractTypeMasterManagement.ExistsSubContractType(objSubType, objSubType.ID))
                        {
                            divsuccessmsgaCSubType.Visible = false;
                            cvAddEditSubType.IsValid = false;
                            cvAddEditSubType.ErrorMessage = "Contract Sub-Type already exists.";
                        }
                        else
                        {
                            objSubType.UpdatedBy = AuthenticationHelper.UserID;
                            objSubType.UpdatedOn = DateTime.Now;

                            bool saveSuccess = ContractTypeMasterManagement.UpdateContractSubType(objSubType);

                            if (saveSuccess)
                            {

                                //cvAddEditSubType.IsValid = false;
                                //cvAddEditSubType.ErrorMessage = "Contract Sub-Type Updated Successfully.";
                                //vsAddEditSubType.CssClass = "alert alert-success";
                                if (cvAddEditSubType.IsValid)
                                {
                                    divsuccessmsgaCSubType.Visible = true;
                                    successmsgaCSubType.Text = "Contract Sub-Type Updated Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCSubType.ClientID + "').style.display = 'none' },2000);", true);
                                }

                            }
                            else
                            {
                                divsuccessmsgaCSubType.Visible = false;
                                cvAddEditSubType.IsValid = false;
                                cvAddEditSubType.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgaCSubType.Visible = false;
                cvAddEditSubType.IsValid = false;
                cvAddEditSubType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        [WebMethod]
        public static string getSubTypeID()
        {
            string subtypeID = string.Empty;
            try
            {

                subtypeID = ContractTypeMasterManagement.GetSubTypeID();
                return subtypeID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return subtypeID;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}