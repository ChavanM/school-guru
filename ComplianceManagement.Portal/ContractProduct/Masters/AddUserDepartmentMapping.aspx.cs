﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddUserDepartmentMapping : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        public static string docTypeID;
        protected void Page_Load(object sender, EventArgs e)
        {
            divsuccessmsgaCDoctype.Visible = false;
            if (!IsPostBack)
            {
                try
                {
                    BindDeptAll(grddept);
                    BindUser();
                    if (!string.IsNullOrEmpty(Request.QueryString["ContractDocId"]))
                    {                        
                        long docTypeID = Convert.ToInt64(Request.QueryString["ContractDocId"]);

                        Cont_tbl_DocumentTypeMaster _objContDocRecord = ContractMastersManagement.GetDocTypeDetailsByID(docTypeID, Convert.ToInt32(CustomerID));

                        if (_objContDocRecord != null)
                        {
                            //tbxDocumentType.Text = _objContDocRecord.TypeName;
                            ViewState["Mode"] = 1;
                            ViewState["ContDocTypeID"] = docTypeID;
                        }
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    divsuccessmsgaCDoctype.Visible = false;
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
        public void BindUser()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();
                
                var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                ddlUser.DataValueField = "ID";
                ddlUser.DataTextField = "Name";
                ddlUser.DataSource = internalUsers;
                ddlUser.DataBind();

                ddlUser.Items.Insert(0, new ListItem("Select User", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindDeptAll(GridView grddept)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

                grddept.DataSource = obj;
                grddept.DataBind();
                
                long UserId = -1;
                if (Convert.ToInt64(ddlUser.SelectedValue) != -1)
                {
                    UserId = Convert.ToInt64(ddlUser.SelectedValue);
                }
                if (UserId != -1)
                {
                    var lstDeptMapping = ContractManagement.GetContractDeptUserMapping(UserId, AuthenticationHelper.CustomerID);
                    if (lstDeptMapping != null)
                    {
                        if (lstDeptMapping.Count > 0)
                        {
                            for (int i = 0; i < grddept.Rows.Count; i++)
                            {
                                if (grddept.Rows[i].RowType == DataControlRowType.DataRow)
                                {
                                    Label lblDeptID = (Label)grddept.Rows[i].FindControl("lblDeptID");
                                    if (lblDeptID != null)
                                    {
                                        long DeptID = Convert.ToInt64(lblDeptID.Text);

                                        if (DeptID != 0)
                                        {
                                            if (lstDeptMapping.Select(row => row.DeptID).Contains(DeptID))
                                            {
                                                CheckBox chkRow = (CheckBox)grddept.Rows[i].FindControl("chkRow");

                                                if (chkRow != null)
                                                {
                                                    chkRow.Checked = true;
                                                }
                                            }
                                            if (lstDeptMapping.Where(x => x.IsContractOwner == true).Select(row => row.DeptID).Contains(DeptID))
                                            {
                                                CheckBox chkRow = (CheckBox)grddept.Rows[i].FindControl("chkRowisAllow");

                                                if (chkRow != null)
                                                {
                                                    chkRow.Checked = true;
                                                }
                                            }
                                        }
                                    }   
                                }
                            }
                        }
                    }
                    lstDeptMapping.Clear();
                    lstDeptMapping = null;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public class gteinfo
        {
            public long DeptID { get; set; }
            public bool IsAllowContract { get; set; }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlUser.SelectedValue != "")
                {
                    int SelectedUserID = Convert.ToInt32(ddlUser.SelectedValue);

                    List<gteinfo> lstDeptMapping = new List<gteinfo>();
                    if (grddept != null)
                    {
                        for (int i = 0; i < grddept.Rows.Count; i++)
                        {
                            if (grddept.Rows[i].RowType == DataControlRowType.DataRow)
                            {
                                long deptId = -1;
                                bool flagisallow = false;
                                CheckBox chkRow = (CheckBox)grddept.Rows[i].FindControl("chkRow");

                                if (chkRow != null)
                                {
                                    if (chkRow.Checked)
                                    {
                                        Label lblDeptID = (Label)grddept.Rows[i].FindControl("lblDeptID");
                                        if (lblDeptID != null)
                                        {
                                            deptId = Convert.ToInt64(lblDeptID.Text);

                                            if (deptId != -1)
                                            {
                                                CheckBox chkRowisAllow = (CheckBox)grddept.Rows[i].FindControl("chkRowisAllow");
                                                if (chkRowisAllow != null)
                                                {
                                                    if (chkRowisAllow.Checked)
                                                        lstDeptMapping.Add(new gteinfo { DeptID = deptId, IsAllowContract = true });
                                                    else
                                                        lstDeptMapping.Add(new gteinfo { DeptID = deptId, IsAllowContract = false });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (lstDeptMapping.Count > 0)
                    {
                        List<Cont_tbl_UserDeptMapping> lstDeptMapping_ToSave = new List<Cont_tbl_UserDeptMapping>();

                        lstDeptMapping.ForEach(EachVendor =>
                        {
                            Cont_tbl_UserDeptMapping _DeptMappingRecord = new Cont_tbl_UserDeptMapping()
                            {
                                UserID = SelectedUserID,
                                CustomerID = AuthenticationHelper.CustomerID,
                                DeptID = EachVendor.DeptID,
                                IsActive = true,
                                CreatedBy = (AuthenticationHelper.UserID),
                                CreatedOn = DateTime.Now,
                                IsContractOwner = EachVendor.IsAllowContract
                            };
                            lstDeptMapping_ToSave.Add(_DeptMappingRecord);
                        });
                        bool deactivatedetails = ContractManagement.DeactivateDeptUserMapping(Convert.ToInt32(ddlUser.SelectedValue), Convert.ToInt32(AuthenticationHelper.CustomerID));
                        bool saveDeptSuccess = ContractManagement.CreateUpdate_DeptUserMapping(lstDeptMapping_ToSave);
                        if (saveDeptSuccess)
                        {
                            divsuccessmsgaCDoctype.Visible = true;
                            successmsgaCDoctype.Text = "User Department Mapping Saved Successfully.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCDoctype.ClientID + "').style.display = 'none' },2000);", true);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "BindControls();", true);
                        }
                        else
                        {
                            //divsuccessmsgaCDoctype.Visible = true;
                            //successmsgaCDoctype.Text = "User Department Mapping Saved Successfully";
                        }
                    }
                    else
                    {
                        divsuccessmsgaCDoctype.Visible = true;
                        successmsgaCDoctype.Text = "Please select at least one Department.";
                    }
                }
                else
                {
                    divsuccessmsgaCDoctype.Visible = true;
                    successmsgaCDoctype.Text = "Please select user to map department(s).";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgaCDoctype.Visible = false;
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occurred. Please try again.";
                vsAddDocType.CssClass = "alert alert-danger";
            }
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlUser.SelectedValue != "")
                {
                    BindDeptAll(grddept);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }
        
    }
}