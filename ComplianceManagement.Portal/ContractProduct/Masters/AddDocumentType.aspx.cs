﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddDocumentType : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        public static string docTypeID;
        protected void Page_Load(object sender, EventArgs e)
        {
            divsuccessmsgaCDoctype.Visible = false;
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["ContractDocId"]))
                    {
                        long docTypeID = Convert.ToInt64(Request.QueryString["ContractDocId"]);

                        Cont_tbl_DocumentTypeMaster _objContDocRecord = ContractMastersManagement.GetDocTypeDetailsByID(docTypeID, Convert.ToInt32(CustomerID));

                        if (_objContDocRecord != null)
                        {
                            btnSave.Text = "Update";
                            tbxDocumentType.Text = _objContDocRecord.TypeName;
                            ViewState["Mode"] = 1;
                            ViewState["ContDocTypeID"] = docTypeID;
                        }
                    }
                    else
                    {
                        btnSave.Text = "Save";
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    divsuccessmsgaCDoctype.Visible = false;
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxDocumentType.Text.Trim()))
                {
                    Cont_tbl_DocumentTypeMaster _objContDoc = new Cont_tbl_DocumentTypeMaster()
                    {
                        TypeName = tbxDocumentType.Text,
                        CustomerID = (int)CustomerID,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (ContractMastersManagement.ExistsContDocumentType(_objContDoc))
                        {
                            divsuccessmsgaCDoctype.Visible = false;
                            cvDuplicateLocation.IsValid = false;
                            cvDuplicateLocation.ErrorMessage = "Contract Document-Type already exists.";
                        }
                        else
                        {
                            long newDocTypeID = 0;

                            newDocTypeID = ContractMastersManagement.CreateDocumentType(_objContDoc);

                            if (newDocTypeID > 0)
                            {
                                docTypeID = newDocTypeID.ToString();

                                //cvDuplicateLocation.IsValid = false;
                                //cvDuplicateLocation.ErrorMessage = "Document Type Saved Successfully";
                                //vsAddDocType.CssClass = "alert alert-success";
                                if (cvDuplicateLocation.IsValid)
                                {
                                    divsuccessmsgaCDoctype.Visible = true;
                                    successmsgaCDoctype.Text = "Document Type Saved Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCDoctype.ClientID + "').style.display = 'none' },2000);", true);
                                }
                                tbxDocumentType.Text = string.Empty;
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (ViewState["ContDocTypeID"] != null)
                        {
                            _objContDoc.ID = Convert.ToInt64(ViewState["ContDocTypeID"]);
                            _objContDoc.UpdatedBy = AuthenticationHelper.UserID;
                            _objContDoc.UpdatedOn = DateTime.Now;

                            bool saveSuccess = ContractMastersManagement.UpdateDocTypeDetails(_objContDoc);

                            if (saveSuccess)
                            {
                                //cvDuplicateLocation.IsValid = false;
                                //cvDuplicateLocation.ErrorMessage = "Document Type Updated Successfully";
                                //vsAddDocType.CssClass = "alert alert-success";
                                if (cvDuplicateLocation.IsValid)
                                {
                                    divsuccessmsgaCDoctype.Visible = true;
                                    successmsgaCDoctype.Text = "Document Type Updated Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCDoctype.ClientID + "').style.display = 'none' },2000);", true);
                                }
                            }
                            else
                            {
                                cvDuplicateLocation.IsValid = false;
                                divsuccessmsgaCDoctype.Visible = false;
                                cvDuplicateLocation.ErrorMessage = "Something went wrong, Please try again";
                                vsAddDocType.CssClass = "alert alert-danger";
                            }
                        }
                    }
                }
                else
                {
                    divsuccessmsgaCDoctype.Visible = false;
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Please Enter Document Type.";
                    vsAddDocType.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgaCDoctype.Visible = false;
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occurred. Please try again.";
                vsAddDocType.CssClass = "alert alert-danger";
            }
        }
        [WebMethod]
        public static string getDocTypeID()
        {
            string doctypeID = string.Empty;
            try
            {
                if (docTypeID != null)
                {
                    doctypeID = docTypeID;
                }

                return doctypeID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "null";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divsuccessmsgaCDoctype.Visible = false;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}