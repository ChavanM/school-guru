﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class PageAthorizationMaster : System.Web.UI.Page
    {
        protected int customerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindddlUserContractMaser();
            }
        }

        private void BindddlUserContractMaser()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    var _pageAutenUserList = (entities.SP_Contract_PageUserAutentication(customerID)).ToList();

                    if (_pageAutenUserList != null)
                    {

                        ddlUserType.DataValueField = "ID";
                        ddlUserType.DataTextField = "Name";
                        ddlUserType.DataSource = _pageAutenUserList;
                        ddlUserType.DataBind();
                        ddlUserType.Items.Insert(0, new ListItem("Select User", "-1"));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdPageAuthorization_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdPageAuthorization_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (!string.IsNullOrEmpty((ddlUserType.SelectedValue).ToString()))
                    {
                        int UserID = Convert.ToInt32(ddlUserType.SelectedValue);

                        if (UserID > 0)
                        {
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                            var UserAuthorizedData = (entities.sp_Cont_PageAthorization(UserID, 0, customerID)).ToList();

                            if (UserAuthorizedData != null)
                            {
                                grdPageAuthorization.DataSource = UserAuthorizedData;
                                grdPageAuthorization.DataBind();
                                upPageAuthorizatonMaster.Update();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnSavePageAutorization_Click(object sender, EventArgs e)
        {
            int customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            try
            {
                int lbluserID = 0;

                // int PageID = 0;

                List<Cont_tbl_PageAuthorizationMaster> objMainList = new List<Cont_tbl_PageAuthorizationMaster>();

                for (int i = 0; i < grdPageAuthorization.Rows.Count; i++)
                {
                    Label PageID = (Label)grdPageAuthorization.Rows[i].Cells[3].FindControl("PageID");
                    lbluserID = Convert.ToInt32(ddlUserType.SelectedValue);
                    CheckBox chkADD = (CheckBox)grdPageAuthorization.Rows[i].Cells[2].FindControl("chkADD");
                    CheckBox chkupdate = (CheckBox)grdPageAuthorization.Rows[i].Cells[3].FindControl("chkupdate");
                    CheckBox chkDelete = (CheckBox)grdPageAuthorization.Rows[i].Cells[4].FindControl("chkDelete");
                    CheckBox chkView = (CheckBox)grdPageAuthorization.Rows[i].Cells[5].FindControl("chkView");

                    Cont_tbl_PageAuthorizationMaster _objpageAuthorized = new Cont_tbl_PageAuthorizationMaster()
                    {

                        UserID = lbluserID,
                        CreatedOn = DateTime.Now,
                        CreatedBy = customerID,
                        isActive = true,
                        CustomerID = customerID

                    };

                    if (PageID.Text != null)
                    {
                        _objpageAuthorized.PageID = Convert.ToInt32(PageID.Text);
                    }
                    if (chkADD.Checked == true)
                    {
                        _objpageAuthorized.Addval = true;
                    }
                    else
                    {
                        _objpageAuthorized.Addval = false;
                    }
                    if (chkupdate.Checked == true)
                    {
                        _objpageAuthorized.Modify = true;
                    }
                    else
                    {
                        _objpageAuthorized.Modify = false;
                    }
                    if (chkDelete.Checked == true)
                    {
                        _objpageAuthorized.Deleteval = true;
                    }
                    else
                    {
                        _objpageAuthorized.Deleteval = false;
                    }
                    if (chkView.Checked == true)
                    {
                        _objpageAuthorized.Viewval = true;
                    }
                    else
                    {
                        _objpageAuthorized.Viewval = false;
                    }

                    bool saveSuccess = false;

                    saveSuccess = ContractManagement.SavePageAutorizedval(_objpageAuthorized);
                    if (saveSuccess == true)
                    {
                        cvPage.IsValid = false;
                        cvPage.ErrorMessage = "Page Authorization Value Save Successfully";
                    }
                }
                String key = "Authenticate" + ddlUserType.SelectedValue;

                if (HttpContext.Current.Cache[key] != null)
                {
                    HttpContext.Current.Cache.Remove(key);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
    }
}