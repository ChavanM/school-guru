﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class ContCustomFieldMasters : System.Web.UI.Page
    {
        protected Cont_tbl_PageAuthorizationMaster authpage;
        protected int pageid = 6;
        private long CutomerID = AuthenticationHelper.CustomerID;
        protected bool flag;

        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
                String key = "ContractAuthenticate" + AuthenticationHelper.UserID;
                if (Cache.Get(key) != null)
                {
                    var Records = (List<Cont_tbl_PageAuthorizationMaster>)HttpContext.Current.Cache[key];
                    if (Records.Count > 0)
                    {
                        var query = (from row in Records
                                     where row.PageID == pageid
                                     select row).FirstOrDefault();
                        authpage = query;
                    }
                }
                if (authpage != null)
                {
                    btnAddCustomFields.Visible = false;
                    if (authpage.Addval == true)
                    {
                        btnAddCustomFields.Visible = true;
                    }
                }
                else
                    btnAddCustomFields.Visible = true;

                flag = false;
                BindContractTypes();
                BindContCustomField();
                bindPageNumber();
            }
        }

        private void BindContractTypes()
        {
            try
            {
                var lstDocType = ContractTypeMasterManagement.GetContractTypes_All(Convert.ToInt32(CutomerID));

                ddlType.DataTextField = "TypeName";
                ddlType.DataValueField = "ID";

                ddlType.DataSource = lstDocType;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("All", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContCustomField()
        {
            int typeID = 0;
            if (!string.IsNullOrEmpty(ddlType.SelectedValue) && (ddlType.SelectedValue != "-1"))
            {
                typeID = Convert.ToInt32(ddlType.SelectedValue);
            }
            var customFieldList = ContractMastersManagement.GetCustomFields_Paging(CutomerID, tbxFilter.Text.Trim().ToString(), typeID);

            string SortExpr = string.Empty;
            string CheckDirection = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                {
                    CheckDirection = Convert.ToString(ViewState["Direction"]);

                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (CheckDirection == "Ascending")
                    {
                        customFieldList = customFieldList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                    }
                    else
                    {
                        CheckDirection = "Descending";
                        customFieldList = customFieldList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                    }
                }
            }

            flag = true;
            Session["TotalRows"] = null;
            if (customFieldList.Count > 0)
            {
                grdContCustomField.DataSource = customFieldList;
                Session["TotalRows"] = customFieldList.Count;
                grdContCustomField.DataBind();
            }
            else
            {
                grdContCustomField.DataSource = customFieldList;
                grdContCustomField.DataBind();
            }
            lblStartRecord.Text = DropDownListPageNo.SelectedValue;
        }

        protected void btnAddCustomFields_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;
                string TypeId = null;
                string CustomFieldID = null;
                lblName.InnerText = "Add New Custom Field";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCustFieldPopup('" + ID + "','" + TypeId + "','" + CustomFieldID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdContCustomField_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void grdContCustomField_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int typeID = 0;
                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    typeID = Convert.ToInt32(ddlType.SelectedValue);
                }

                var detailView = ContractMastersManagement.GetCustomFields_Paging(CutomerID, tbxFilter.Text.Trim().ToString(), typeID); ;

                List<object> dataSource = new List<object>();
                foreach (var contInfo in detailView)
                {
                    dataSource.Add(new
                    {
                        contInfo.RowID,
                        contInfo.TypeName,
                        contInfo.ID,
                        contInfo.Label,
                        contInfo.ContractTypeID

                    });
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;
                foreach (DataControlField field in grdContCustomField.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdContCustomField.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;
                if (dataSource.Count > 0)
                {
                    grdContCustomField.DataSource = dataSource;
                    Session["TotalRows"] = dataSource.Count;
                    grdContCustomField.DataBind();
                }
                else
                {
                    grdContCustomField.DataSource = null;
                    grdContCustomField.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContCustomField_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (ContractTypeMasterManagement.getContractRole(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID)))
                {
                    LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("lnkEditContractDocType");

                    LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("lnkDeleteContractDocType");

                    LinkButton2.Visible = true;

                    LinkButton1.Visible = true;
                }
                else if (authpage != null)
                {
                    LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("lnkEditContractDocType");
                    LinkButton2.Visible = false;

                    LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("lnkDeleteContractDocType");
                    LinkButton1.Visible = false;

                    if (authpage.Modify == true)
                    {
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Deleteval == true)
                    {
                        LinkButton1.Visible = true;
                    }
                }
            }
        }

        protected void grdContCustomField_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long CustomerID = AuthenticationHelper.CustomerID;
            lblName.InnerText = "Edit Custom Field Details";
            
            if (e.CommandName.Equals("EDIT_ContCustomField"))
            {
                ViewState["Mode"] = 1;

                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                string lblValue = commandArgs[0];

                long TypeID = Convert.ToInt64(commandArgs[1]);
                long ID = Convert.ToInt64(commandArgs[2]);

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCustFieldPopup('" + lblValue + "','" + TypeID + "','" + ID + "');", true);
            }
            else if (e.CommandName.Equals("DELETE_ContCustomField"))
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                //string lblValue = commandArgs[0];
                long ID = Convert.ToInt64(commandArgs[0]);
                long TypeID = Convert.ToInt64(commandArgs[1]);

                bool deleteSuccess = ContractMastersManagement.DeleteCustomField(ID, Convert.ToInt32(CustomerID));

                if (deleteSuccess)
                {
                    BindContCustomField();
                    bindPageNumber();

                    cvCustomFieldPage.IsValid = false;
                    cvCustomFieldPage.ErrorMessage = "Custom Field Deleted Successfully";
                }
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContCustomField.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindContCustomField();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContCustomField.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                int totalPages = 0;
                if (Session["TotalRows"] != null)
                {
                    TotalRows.Value = Session["TotalRows"].ToString();

                    totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                    // total page item to be displayed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                    
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindContCustomField();
            bindPageNumber();
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContCustomField.PageIndex = chkSelectedPage - 1;
            grdContCustomField.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindContCustomField(); SetShowingRecords();
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {

            BindContCustomField();
            bindPageNumber();

        }

        protected void lnkPreviousSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContDocumentType.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkNextSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContractTemplate.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindContCustomField();
            bindPageNumber();
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindContCustomField();
            bindPageNumber();
        }
    }
}