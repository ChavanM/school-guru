﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractTemplateTransactionReviwerDetail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.ContractTemplateTransactionReviwerDetail" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    <title>Telerik ASP.NET Example</title>
    
     <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
      <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />
 <script type="text/javascript">
            var editor, range;
            function OnClientLoad(sender, args) {
                editor = sender;
            }

            function OnClientSelectionChange(sender, args) {
                range = editor.getSelection().getRange(true);
            }

            
            function CloseModel() {
                window.parent.parent.CloseContractModal();
            }
            function OpenHistorySectionModal() {
                $('#TemplateHistroySectionPopup').modal('show');
                return false;
            }

            function OpenSectionModal(UId, RoleId, templateID, custID, sectionID)
            {
                debugger;
                $('#TemplateSectionPopup').modal('show');
                return false;
            }

            function CloseSectionModal() {
                $('#TemplateSectionPopup').modal('hide');
             
                window.parent.ClosedSectionTemplate();                
            }
            function CloseRefresh()
            {
             
                window.parent.ClosedSectionTemplate();
            }

    </script> 
</head>
 
<body>
    <form id="form1" runat="server">

    <div class="row">

        <div class="col-md-12 colpadding0" style="padding-top: 7px;">
            <div class="col-md-2 colpadding0" style="overflow-y: auto; overflow-x: hidden; max-height: 512px;">
                     <div class="col-md-12 colpadding0" style="padding-left: 5px;">
                <asp:Repeater ID="rptComplianceVersionView"
                            OnItemCommand="rptComplianceVersionView_ItemCommand" runat="server">
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("SectionID")+","+Eval("SectionStatusID") %>' ID="lblDocumentVersionView"
                                              style="" Font-Underline="false"  runat="server">
                                <table style="padding: 6px;border-collapse: separate; border: 1px solid rgb(229, 229, 229); border-radius: 3px; margin-bottom: 7px; width: 95%;">
                                    <tr>
                                        <td><%#Eval("Header")%></td>
                                    </tr>
                                    <tr>
                                      <td><%#Eval("SectionStatus")%></td>                                   
                                    </tr>
                                     <tr>                                         
                                         <td style='color:<%#Eval("DeptColor")%>;font-size: 16px;'><%#Eval("DeptName")%></td>
                                    </tr>
                                     <tr>                                         
                                         <td><%#Eval("SectionReviwerName")%></td>
                                    </tr>
                                    <tr>
                                        <td><%# Eval("CreatedOn") != null ? ((DateTime)Eval("CreatedOn")).ToString("dd-MM-yyyy  h:mm tt") : "" %></td>                                        
                                    </tr>
                                </table>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
              </div>
            </div>
            <div class="col-md-8 colpadding0">                
                <div class="row">
                    <div class="col-md-12 colpadding0" style="margin-left: -2px;">

                        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Visible="false" />
                        <div class="demo-containers">
                            <div class="demo-container">
                                <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" StripFormattingOptions="MSWord" EnableTrackChanges="true" runat="server"
                                    Width="811px" OnClientLoad="OnClientLoad" OnClientSelectionChange="OnClientSelectionChange"
                                    Height="504px" ToolsFile="../../ToolsFile.xml" OnExportContent="RadEditor1_ExportContent"
                                    ContentFilters="DefaultFilters, PdfExportFilter"
                                    SkinID="WordLikeExperience" EditModes="Preview">
                                    <RealFontSizes>
                                        <telerik:EditorRealFontSize Value="12pt" />
                                        <telerik:EditorRealFontSize Value="18pt" />
                                        <telerik:EditorRealFontSize Value="22px" />
                                    </RealFontSizes>
                                    <ExportSettings>
                                        <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                                            PageHeader="Some header text for DOCX documents" />
                                        <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                                            HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                                    </ExportSettings>
                                    <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true"
                                        UserCssId="reU0"></TrackChangesSettings>
                                    <Content>                  
                                    </Content>
                                </telerik:RadEditor>
                            </div>
                        </div>
                        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                            <AjaxSettings>
                                <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                            </AjaxSettings>
                        </telerik:RadAjaxManager>
                        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                        </telerik:RadAjaxLoadingPanel>
                        <div runat="server" id="divHtml"></div>
                    </div>
                </div>                
            </div>
        
            <div class="col-md-2 colpadding0">      
             <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnreviewer" Style="width: 85%;" runat="server" CssClass="btn btn-primary" OnClick="btnreviewer_Click" Text="Approve" />                             
              </div>  
               <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnreviewerreject" Style="width: 85%;" runat="server" CssClass="btn btn-primary" OnClick="btnreviewerreject_Click" Text="Reject" />                             
              </div> 
               <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnApproverPrimary" Style="width: 85%;" runat="server" CssClass="btn btn-primary" OnClick="btnApproverPrimary_Click" Text="Approve" />                             
              </div>  
              <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnRejctedPrimary" Style="width: 85%;" runat="server" CssClass="btn btn-primary" OnClick="btnRejctedPrimary_Click" Text="Reject" />                             
              </div>  
               <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnApprover" Style="width: 85%;" runat="server" CssClass="btn btn-primary" OnClick="btnApprover_Click" Text="Approve" />                             
              </div>  
              <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnRejcted" Style="width: 85%;" runat="server" CssClass="btn btn-primary" OnClick="btnRejcted_Click" Text="Reject" />                             
              </div>                 
              <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="BtnFullHistory" Style="width: 85%;" runat="server" CssClass="btn btn-primary" OnClick="BtnFullHistory_Click" Text="Full History" />                             
              </div>                 
            </div>
        </div>
    </div>
    <div class="modal fade" id="TemplateHistroySectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 1181px;margin-left: -304px;">
                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                                <iframe id="showdetails" src="../Common/ContractTemplateTransactionHistry.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =ContractTemplateId%>&CID=<% =CustId%>" width="100%" height="525px;" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
      <div class="modal fade" id="TemplateSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 1018px;margin-left: -203px;">
                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe id="showdetails" src="../Common/SectionContractTemplateTransactionDetail.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =ContractTemplateId%>&CID=<% =CustId%>&SID=<% =SectionId%>" width="100%" height="525px;" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>        
    </form>
</body>
</html>
