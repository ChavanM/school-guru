﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;


namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class SectionTemplateTransactionDetail : System.Web.UI.Page
    {            //protected static string User;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
                BindUser(TemplateID, SectionID);
                BindTemplateFeilds();
                SetDefaultConfiguratorValues(UserID, RoleID, TemplateID, SectionID);
            }
        }
        
        public void BindTemplateFeilds()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_Master_ContractTemplate
                            where row.IsActive == true
                            select row).ToList();
                if (data.Count > 0)
                {
                    ddlContractTemplate.DataValueField = "ContractTemplate";
                    ddlContractTemplate.DataTextField = "ContractTemplate";
                    ddlContractTemplate.DataSource = data;
                    ddlContractTemplate.DataBind();
                }
            }            
        }

        public void BindUser(int TemplateID,int SectionID)
        {
            try {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                int DeptID = -1;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    DeptID = (from row in entities.Cont_tbl_TemplateSectionMapping
                              where row.TemplateID == TemplateID
                              && row.IsActive == true
                              && row.SectionID == SectionID
                              && row.CustomerID == customerID
                              select (int)row.DeptID).FirstOrDefault();

                    List<long> getdata1 = (from row in entities.Cont_tbl_UserDeptMapping
                                           where row.IsActive == true
                                           && row.CustomerID == customerID
                                           && row.DeptID == DeptID
                                           select row.UserID).ToList();

                    lstAllUsers = lstAllUsers.Where(x => getdata1.Contains(x.ID)).ToList();
                }

                var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                ddlUser.DataValueField = "ID";
                ddlUser.DataTextField = "Name";
                ddlUser.DataSource = internalUsers;
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SetDefaultConfiguratorValues(int UserID,int RoleID,int TemplateID,int SectionID)
        {
            ViewState["TransactionOrderID"] = 0;
            ViewState["TransactionID"] = 0;
            btnSubmit.Visible = true;
            ddlUser.Visible = false;
            btnforClosed.Visible = false;
            Button3.Visible = false;
            Button4.Visible = false;
            btnforApproval.Visible = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string UserName = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                var data = (from row in entities.Sp_GetTemplateTransaction()
                            where row.UserID == UserID
                            && row.TemplateInstanceID == TemplateID
                            && row.RoleID == RoleID
                            && row.sectionID == SectionID
                            select row).FirstOrDefault();

                if (data != null)
                {
                    ViewState["TransactionOrderID"] = data.OrderID;
                    ViewState["TransactionID"] = data.TemplateTransactionID;
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                    theEditor.EnableTrackChanges = true;

                    if (RoleID == 3)
                    {
                        if (data.ComplianceStatusID == 1 || data.ComplianceStatusID == 5)
                        {
                            theEditor.Content = data.TemplateContent;
                            btnSubmit.Visible = true;
                           
                            ddlUser.Visible = true;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            //theEditor.TrackChangesSettings.UserCssId = "reU9";
                            theEditor.TrackChangesSettings.UserCssId = "reU0";
                            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                            theEditor.EnableTrackChanges = true;
                        }
                        else if (data.ComplianceStatusID == 3 || data.ComplianceStatusID == 14 || data.ComplianceStatusID == 15)
                        {
                            theEditor.Content = data.TemplateContent;
                            btnSubmit.Visible = true;
                            ddlUser.Visible = true;                           
                            Button3.Visible = false;
                            Button4.Visible = false;                          
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            //theEditor.TrackChangesSettings.UserCssId = "reU9";
                            theEditor.TrackChangesSettings.UserCssId = "reU0";
                            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                            theEditor.EnableTrackChanges = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                        }
                    }
                    if (RoleID == 4)
                    {
                        if (data.ComplianceStatusID == 2)
                        {
                            theEditor.Content = data.TemplateContent;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            btnSubmit.Visible = false;
                            btnforApproval.Visible = false;
                            Button3.Visible = true;
                            Button4.Visible = true;
                            theEditor.TrackChangesSettings.UserCssId = "reU9";
                            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                            theEditor.EnableTrackChanges = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnforApproval.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                        }
                    }
                    if (RoleID == 6)
                    {
                        if (data.ComplianceStatusID == 4)
                        {
                            theEditor.Content = data.TemplateContent;

                            btnSubmit.Visible = false;
                            btnforApproval.Visible = false;
                            Button3.Visible = true;
                            Button4.Visible = true;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU9";
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnforApproval.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                        }
                    }
                }
                else
                {
                    btnSubmit.Visible = true;
                    btnforApproval.Visible = true;
                    Button3.Visible = false;
                    Button4.Visible = false;
                    theEditor.TrackChangesSettings.Author = UserName;
                    lblUser.Text = UserName;
                    theEditor.TrackChangesSettings.UserCssId = "reU9";
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                    theEditor.EnableTrackChanges = false;
                }
            }                
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ddlUser.SelectedValue != "" && ddlUser.SelectedValue != "-1" && ddlUser.SelectedValue != null)
                {
                    int ReviewerID = Convert.ToInt32(ddlUser.SelectedValue);

                    TemplateAssignment checkdata = (from row in entities.TemplateAssignments
                                                    where row.TemplateInstanceID == TemplateID
                                                    && row.RoleID == 4
                                                    && row.SectionID == SectionID
                                                    select row).FirstOrDefault();
                    if (checkdata != null)
                    {
                        if (checkdata.UserID != ReviewerID)
                        {
                            checkdata.UserID = ReviewerID;
                            entities.SaveChanges();
                        }
                    }
                }

                TemplateTransaction obj = new TemplateTransaction();
                obj.TemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.TemplateContent = theEditor.Content;
                obj.SectionID = SectionID;
                obj.CreatedByText= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;                
                if (RoleID == 4)
                {
                    obj.Status = 3;
                }
                else if (RoleID == 3)
                {
                    obj.Status = 2;
                }
                else
                {
                    obj.Status = 4;
                }
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.TemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }
                    #region set transaction history
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    
                    var entitiesData = (from row in entities.Cont_SP_GetSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();


                    var data1 = (from row in entities.Sp_GetTemplateTransaction()
                                 where row.UserID == UserID
                                 && row.TemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_TemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.TemplateID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.TemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.TemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = row.sectionID,
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        TemplateTransactionHistory obj1 = new TemplateTransactionHistory();
                        obj1.TemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = AuthenticationHelper.UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        obj1.Status = 1;
                        obj1.OrderID = 1;
                        entities.TemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }

                if (RoleID == 3)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    
                    var entitiesData = (from row in entities.Cont_tbl_TemplateStatusMapping
                                        where row.TemplateID == TemplateID
                                        && row.CustomerID == customerID
                                        select row).FirstOrDefault();
                    if (entitiesData != null)
                    {
                        entitiesData.UpdatedBy = UserID;
                        entitiesData.UpdatedOn = DateTime.Now;
                        entitiesData.IsActive = true;                        
                        entitiesData.Status = false;
                        entities.SaveChanges();                        
                    }
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }


        public bool UpdateTransactionHistory(int TemplateId, int TransactionID, string getInput, string TransactionType)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(getInput))
            {
                var getdata = getInput;
                string[] stringSeparators = new string[] { "endend" };
                var resultdata = getdata.Split(stringSeparators, StringSplitOptions.None);
                foreach (var item in resultdata)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string[] stringSeparators1 = new string[] { "andand" };
                        var result1 = item.Split(stringSeparators1, StringSplitOptions.None);
                        string inputtext = result1[0];
                        string author = result1[1];
                        string title = result1[2];
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            cont_TransactionHistoryMapping obj = new cont_TransactionHistoryMapping
                            {
                                Author = result1[1],
                                Inputtext = result1[0],
                                Title = result1[2],
                                CreatedBy = Convert.ToInt64(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID),
                                TemplateInstanceID = TemplateId,
                                TemplateTransactionID = TransactionID,
                                TransactionType = TransactionType,
                                CreatedOn = DateTime.Now,
                                IsActive = true
                            };
                            entities.cont_TransactionHistoryMapping.Add(obj);
                            entities.SaveChanges();
                        }
                    }
                }            
            }
            return result;
        }
        
        protected void RadEditor1_ExportContent(object sender, EditorExportingArgs e)
        {
            ExportType exportType = e.ExportType;

            if (exportType == ExportType.Word)
            {
                string exportedOutput = e.ExportOutput;

                Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                DocxFormatProvider docxProvider = new DocxFormatProvider();
                RadFlowDocument document = docxProvider.Import(output);

                Header defaultHeader = document.Sections.First().Headers.Add();
                Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                defaultHeaderParagraph.TextAlignment = Alignment.Right;
                defaultHeaderParagraph.Inlines.AddRun("This is a sample header.");

                Footer defaultFooter = document.Sections.First().Footers.Add();
                Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                defaultFooterParagraph.TextAlignment = Alignment.Right;
                defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                Byte[] modifiedOutput = docxProvider.Export(document);
                string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                e.ExportOutput = finalOutput;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            theEditor.ExportToDocx();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            divHtml.InnerHtml = theEditor.Content;
        }

        public static TemplateTransaction GetDetail(int TID, int RoleID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.TemplateTransactions
                            where row.Id == TID
                            select row).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

                return data;
            }
        }

        protected void btnforApproval_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            if (RoleID == 3)
            {
                if (ddlUser.SelectedValue != "")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var Approvaldata = (from row in entities.TemplateAssignments
                                            where row.TemplateInstanceID == TemplateID
                                            && row.RoleID == 6
                                            select row).ToList();
                        if (Approvaldata.Count > 0)
                        {
                            foreach (var item in Approvaldata)
                            {
                                TemplateAssignment checkdata = (from row in entities.TemplateAssignments
                                                                where row.TemplateInstanceID == TemplateID
                                                                && row.RoleID == 6
                                                                && row.ID == item.ID
                                                                select row).FirstOrDefault();
                                if (checkdata != null)
                                {
                                    checkdata.IsActive = false;
                                    entities.SaveChanges();
                                }
                            }
                        }

                        int selectedUserID = Convert.ToInt32(ddlUser.SelectedValue);

                        TemplateAssignment data = (from row in entities.TemplateAssignments
                                                   where row.TemplateInstanceID == TemplateID
                                                   && row.RoleID == 6
                                                   && row.UserID == selectedUserID
                                                   select row).FirstOrDefault();
                        if (data != null)
                        {
                            data.IsActive = true;
                            entities.SaveChanges();
                        }
                        else
                        {
                            TemplateAssignment templateAssignment = new TemplateAssignment()
                            {
                                TemplateInstanceID = TemplateID,
                                RoleID = 6,
                                UserID = Convert.ToInt32(ddlUser.SelectedValue),
                                IsActive = true
                            };
                            ContractTemplateManagement.CreateTemplateAssignment(templateAssignment);
                        }
                    }                    
                }
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TemplateTransaction obj = new TemplateTransaction();
                obj.TemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.TemplateContent = theEditor.Content;
                obj.Status = 4;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.TemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }
        
        protected void Button3_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TemplateTransaction obj = new TemplateTransaction();
                obj.TemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.TemplateContent = theEditor.Content;
                obj.Status = 15;
                obj.SectionID = SectionID;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.TemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }

                    #region set transaction history
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    var entitiesData = (from row in entities.Cont_SP_GetSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();
                    
                    var data1 = (from row in entities.Sp_GetTemplateTransaction()
                                 where row.UserID == UserID
                                 && row.TemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_TemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.TemplateID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.TemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.TemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = row.sectionID,
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        TemplateTransactionHistory obj1 = new TemplateTransactionHistory();
                        obj1.TemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = AuthenticationHelper.UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        obj1.Status = 1;
                        obj1.OrderID = 1;
                        entities.TemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TemplateTransaction obj = new TemplateTransaction();
                obj.TemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.TemplateContent = theEditor.Content;
                obj.Status = 5;
                obj.SectionID = SectionID;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.TemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }

                    #region set transaction history
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    var entitiesData = (from row in entities.Cont_SP_GetSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();

                    var data1 = (from row in entities.Sp_GetTemplateTransaction()
                                 where row.UserID == UserID
                                 && row.TemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_TemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.TemplateID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.TemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.TemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = row.sectionID,
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        TemplateTransactionHistory obj1 = new TemplateTransactionHistory();
                        obj1.TemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = AuthenticationHelper.UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        obj1.Status = 1;
                        obj1.OrderID = 1;
                        entities.TemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        public bool UpdateExistRole(int TemplateId, int RoleID,int UserID)
        {
            bool result = true;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TemplateAssignment data = (from row in entities.TemplateAssignments
                                           where row.TemplateInstanceID == TemplateId
                                           && row.RoleID == RoleID
                                           select row).FirstOrDefault();
                if (data != null)
                {
                    data.IsActive = false;
                    entities.SaveChanges();
                }
            }
            return result;
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            if (ViewState["TransactionID"] != null)
            {
                int TrasanctionID = Convert.ToInt32(ViewState["TransactionID"]);

                if (TrasanctionID > 0)
                {
                    int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                    int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                    int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        TemplateTransaction data = (from row in entities.TemplateTransactions
                                                    where row.TemplateInstanceID == TemplateID
                                                    && row.Id == TrasanctionID
                                                    select row).FirstOrDefault();
                        if (data != null)
                        {
                            TemplateTransaction obj = new TemplateTransaction();
                            obj.TemplateInstanceID = TemplateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = UserID;
                            obj.TemplateContent = theEditor.Content;
                            obj.Status = data.Status;
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                            {
                                obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                            }
                            entities.TemplateTransactions.Add(obj);
                            entities.SaveChanges();
                            int TransactionID = Convert.ToInt32(obj.Id);
                            if (TransactionID > 0)
                            {
                                if (!string.IsNullOrEmpty(getHistryInsert.Text))
                                {
                                    UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                                }
                                if (!string.IsNullOrEmpty(getHistryDel.Text))
                                {
                                    UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void btnforClosed_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
                TemplateTransaction obj = new TemplateTransaction();
                obj.TemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.TemplateContent = theEditor.Content;
                obj.SectionID = SectionID;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                obj.Status = 14;                
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.TemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }
                    #region set transaction history
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    var entitiesData = (from row in entities.Cont_SP_GetSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();


                    var data1 = (from row in entities.Sp_GetTemplateTransaction()
                                 where row.UserID == UserID
                                 && row.TemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_TemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.TemplateID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.TemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.TemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = row.sectionID,
                                        orderID = Convert.ToInt32(row1.SectionOrder)
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        TemplateTransactionHistory obj1 = new TemplateTransactionHistory();
                        obj1.TemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = AuthenticationHelper.UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        obj1.Status = 1;
                        obj1.OrderID = 1;
                        entities.TemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }
    }
}