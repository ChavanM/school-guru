﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractTemplateTransactionDetail.aspx.cs" 
Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.ContractTemplateTransactionDetail" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    <title>Telerik ASP.NET Example</title>
    
     <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
      <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />
 <script type="text/javascript">
            var editor, range;
            function OnClientLoad(sender, args) {
                editor = sender;
            }

            function OnClientSelectionChange(sender, args) {
                range = editor.getSelection().getRange(true);
            }

            
            function CloseModel() {
                window.parent.parent.CloseContractModal();
            }

            function OpenInternalReviwerSectionModal()
            {                
                $('#TemplateSetReviwer').modal('show');
                return false;
            }

            function OpenVendorSectionModal() {
                $('#TemplateVendorReviwerPopup').modal('show');
                return false;
            }

            function OpenApprovarSectionModal() {
                $('#TemplateApproverReviwerPopup').modal('show');
                return false;
            }

            function OpenHistorySectionModal() {
                $('#TemplateHistroySectionPopup').modal('show');
                return false;
            }

            function GetReviewerInfo() {
                $('#TemplateSetReviwer').modal('show');
                return false;
            }

            function OpenSectionModal(UId, RoleId, templateID, custID, sectionID)
            {
                $('#TemplateSectionPopup').modal('show');
                return false;
            }

            function CloseSectionModal() {
             
                $('#TemplateSectionPopup').modal('hide');
                window.parent.ClosedSectionTemplate();
            }

            function closedRefreshModel()
            {
                window.parent.ClosedSectionTemplate();
            }
            

    </script> 
</head>
 
<body>
    <form id="form1" runat="server">

    <div class="row">

        <div class="col-md-12 colpadding0" style="padding-top: 7px;">
            <div class="col-md-2 colpadding0" style="overflow-y: auto; overflow-x: hidden; max-height: 512px;">
                     <div class="col-md-12 colpadding0" style="padding-left: 5px;">
                <asp:Repeater ID="rptComplianceVersionView"
                            OnItemCommand="rptComplianceVersionView_ItemCommand" runat="server">
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("SectionID")+","+Eval("SectionStatusID") %>' ID="lblDocumentVersionView"
                                              style="" Font-Underline="false"  runat="server">
                                <table style="padding: 6px;border-collapse: separate; border: 1px solid rgb(229, 229, 229); border-radius: 3px; margin-bottom: 7px; width: 95%;">
                                    <tr>
                                        <td><%#Eval("Header")%></td>
                                    </tr>
                                    <tr>
                                      <td><%#Eval("SectionStatus")%></td>                                   
                                    </tr>
                                     <tr>                                         
                                         <td style='color:<%#Eval("DeptColor")%>;font-size: 16px;'><%#Eval("DeptName")%></td>
                                    </tr>
                                     <tr>                                         
                                         <td><%#Eval("SectionReviwerName")%></td>
                                    </tr>
                                    <tr>
                                        <td><%# Eval("CreatedOn") != null ? ((DateTime)Eval("CreatedOn")).ToString("dd-MM-yyyy  h:mm tt") : "" %></td>                                        
                                    </tr>
                                </table>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
              </div>
            </div>
            <div class="col-md-8 colpadding0">                
                <div class="row">
                    <div class="col-md-12 colpadding0" style="margin-left: -2px;">

                        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Visible="false" />
                        <div class="demo-containers">
                            <div class="demo-container">
                                <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" StripFormattingOptions="MSWord" EnableTrackChanges="true" runat="server"
                                    Width="811px" OnClientLoad="OnClientLoad" OnClientSelectionChange="OnClientSelectionChange"
                                    Height="504px" ToolsFile="../../ToolsFile.xml" OnExportContent="RadEditor1_ExportContent"
                                    ContentFilters="DefaultFilters, PdfExportFilter"
                                    SkinID="WordLikeExperience" EditModes="Preview">
                                    <RealFontSizes>
                                        <telerik:EditorRealFontSize Value="12pt" />
                                        <telerik:EditorRealFontSize Value="18pt" />
                                        <telerik:EditorRealFontSize Value="22px" />
                                    </RealFontSizes>
                                    <ExportSettings>
                                        <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                                            PageHeader="Some header text for DOCX documents" />
                                        <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                                            HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                                    </ExportSettings>
                                    <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true"
                                        UserCssId="reU0"></TrackChangesSettings>
                                    <Content>                  
                                    </Content>
                                </telerik:RadEditor>
                            </div>
                        </div>
                        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                            <AjaxSettings>
                                <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                            </AjaxSettings>
                        </telerik:RadAjaxManager>
                        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                        </telerik:RadAjaxLoadingPanel>
                        <div runat="server" id="divHtml"></div>
                    </div>
                </div>                
            </div>
        
            <div class="col-md-2 colpadding0">   
              <div class="col-md-12 colpadding0" style="text-align: center;">   
                <asp:Label Text="Version" style="font-size: 21px;">Version : </asp:Label>             
                <asp:Label ID="lblVersion" style="font-size: 21px;" runat="server"></asp:Label>                              
              </div>
              <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                 <asp:Button ID="btnReviwer" runat="server" style="width: 185px;" CssClass="btn btn-primary" Text="Submit for Review" OnClick="btnReviwer_Click" />                             
              </div>
              <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                <asp:Button ID="btnVendor" runat="server" style="width: 185px;" CssClass="btn btn-primary" OnClick="btnVendor_Click" Text="Submit for Vendor Review" />                             
              </div>                 
              <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnApprover" style="width: 185px;" runat="server" OnClick="btnApprover_Click" CssClass="btn btn-primary" Text="Submit for Approval" />                             
              </div>                 
              <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="BtnFullHistory" style="width: 185px;" runat="server" CssClass="btn btn-primary" Text="Full History" OnClick="BtnFullHistory_Click" />                             
              </div> 
               <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnconvertcontract" style="width: 185px;" runat="server" CssClass="btn btn-primary" OnClick="btnconvertcontract_Click" Text="Convert to Contract" />                             
              </div>   
                 <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnrenew" style="width: 185px;" runat="server" CssClass="btn btn-primary" OnClick="btnrenew_Click" Text="Renew" />                             
              </div> 
               <div class="col-md-12 colpadding0" style="width: 197px;padding-bottom:5px;text-align: center;">                
                  <asp:Button ID="btnExportDocReport" style="width: 185px;" runat="server" CssClass="btn btn-primary" OnClick="btnExportDocReport_Click" Text="Export Document" />                             
              </div>                 
            </div>
        </div>
    </div>
   

    <div class="modal fade" id="TemplateApproverReviwerPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="">
                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <label style="font-size: 18px;">Select Approver</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                         <div class="row">
                            <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="div1">
                                <asp:Label runat="server" ID="Label1"></asp:Label>
                            </div>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ContractApproverValidationGroup" />
                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                ValidationGroup="ContractApproverValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>


                        <div class="row">
                       <div class="col-md-12 colpadding0">
                           <asp:Repeater ID="rptComplianceApproverView"
                               OnItemDataBound="rptComplianceApproverView_ItemDataBound"
                               runat="server">
                               <ItemTemplate>

                               <table>
                                   <tr>
                                       <td style="width: 124px;">
                                           <asp:Label ID="Label2" runat="server" Text="Approver Level "></asp:Label>
                                           <asp:Label ID="lblApproverID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                       </td>
                                       <td style="width: 330px;">
                                           <asp:DropDownList runat="server"
                                               ID="drplistdynamicApprover" class="form-control" Width="100%" Style="margin-top: 5px;"
                                               AllowSingleDeselect="false" DataPlaceHolder="Select Approver" DisableSearchThreshold="5">
                                           </asp:DropDownList>
                                       </td>
                                   </tr>

                               </table>
                               </ItemTemplate>
                               <FooterTemplate>
                               </FooterTemplate>
                           </asp:Repeater>
                        </div>
                        </div>

                            <div class="row" style="display:none;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0">
                                        <label>Approver Selection</label>
                                    </div>
                                    <div class="col-md-4 colpadding0">
                                        <asp:DropDownList runat="server" ID="ddlselectionApprovar" class="form-control" Style="width: 100%; height: 32px !important;"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlselectionApprovar_SelectedIndexChanged">
                                            <asp:ListItem Text="Single" Value="1" />
                                            <asp:ListItem Text="Multiple" Value="2"  Selected="True"/>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                              <%--  <br />--%>

                            <div class="row" id="divforprimaryapproval" runat="server" style="display:none;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0">
                                        <label>Primary Approver</label>
                                    </div>
                                    <div class="col-md-4 colpadding0">
                                        <asp:DropDownListChosen runat="server"
                                            ID="drplistApproverprimary" class="form-control" Width="100%"
                                            AllowSingleDeselect="false" DataPlaceHolder="Select Approver" DisableSearchThreshold="5">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                            </div>
                           <%-- <br />--%>

                            <div class="row" id="divforsecondaryapproval" runat="server" style="display:none;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0">
                                        <asp:Label runat="server" ID="lblapprover" Text="Secondary Approver"></asp:Label>
                                    </div>
                                    <div class="col-md-4 colpadding0">
                                        <asp:DropDownListChosen runat="server"
                                            ID="drplistApprover" class="form-control" Width="100%"
                                            AllowSingleDeselect="false" DataPlaceHolder="Select Approver" DisableSearchThreshold="5">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                            </div>
                        <br />
                            <div class="row">
                                <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0">

                                </div>
                                <div class="col-md-4 colpadding0">
                                    <asp:Button ID="btnApproverSave" Style="width: 100%;" runat="server" OnClick="btnApproverSave_Click" CssClass="btn btn-primary" Text="Submit for Approval" />
                                </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
            </div>

      <div class="modal fade" id="TemplateVendorReviwerPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="">

                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <label style="font-size: 18px;">Select Vendor Reviewer</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="">
                         <div class="row">
                            <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCType">
                                <asp:Label runat="server" ID="successmsgaCType"></asp:Label>
                            </div>
                            <asp:ValidationSummary ID="vsAddEditContractType" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ContractVendorValidationGroup" />
                            <asp:CustomValidator ID="cvAddEditContractType" runat="server" EnableClientScript="False"
                                ValidationGroup="ContractVendorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div class="row">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0">
                                    <label style="margin-right: 3px;float: left;color: #eb5555;">*</label>
                                    <label>Vendor Reviewer</label>
                                </div>
                                <div class="col-md-4 colpadding0">
                                    <asp:DropDownListChosen AutoPostBack="true" runat="server" ID="ddlVendRev" class="form-control" Width="100%"
                                       OnSelectedIndexChanged="ddlVendRev_SelectedIndexChanged" AllowSingleDeselect="false" DataPlaceHolder="Select Reviewer" DisableSearchThreshold="5">
                                    </asp:DropDownListChosen>

                                     <asp:RequiredFieldValidator ID="rfvContractCategory" ErrorMessage="Please Select Vendor" InitialValue="0"
                                 ControlToValidate="ddlVendRev" runat="server" ValidationGroup="ContractVendorValidationGroup" Display="None" />

                                </div>
                            </div>
                            </div>
                            <br />
                            <div class="row">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0">
                                    <label style="margin-right: 3px;float: left;color: #eb5555;">*</label>
                                    <label>Email</label>
                                </div>
                                <div class="col-md-4 colpadding0">
                                  <asp:TextBox ID="EmailTxt" class="form-control" Text="" runat="server"></asp:TextBox>
                                  
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Enter Email" 
                                     ControlToValidate="EmailTxt" ValidationGroup="ContractVendorValidationGroup"
                                    runat="server" Display="None" />
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0">
                                    <label style="margin-right: 3px;float: left;color: #eb5555;">*</label>
                                    <label>Contact No.</label>
                                </div>
                                <div class="col-md-4 colpadding0">
                                  <asp:TextBox ID="txtContactNum" class="form-control" Text="" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="rfvTypeName" ErrorMessage="Please Enter Contact Number" 
                                     ControlToValidate="txtContactNum" ValidationGroup="ContractVendorValidationGroup"
                                    runat="server" Display="None" />
                                </div>
                            </div>
                        </div>
                        <br />
                            <div class="row">
                                <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0">

                                </div>
                                <div class="col-md-4 colpadding0">
                                    <asp:Button ID="btnvendr" Style="width: 100%;" runat="server" OnClick="btnvendr_Click" ValidationGroup="ContractVendorValidationGroup" CssClass="btn btn-primary" Text="Submit for Vendor Review" />
                                </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
            </div>
      <div class="modal fade" id="TemplateSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 1018px;margin-left: -203px;">
                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                                <iframe id="showdetails1" src="../Common/SectionContractTemplateTransactionDetail.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =ContractTemplateId%>&CID=<% =CustId%>&SID=<% =SectionId%>&StatusID=<% =CurrentStatusId%>" width="100%" height="525px;" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        
        <div class="modal fade" id="TemplateHistroySectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 1181px;margin-left: -304px;">
                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                                <iframe id="showdetails" src="../Common/ContractTemplateTransactionHistry.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =ContractTemplateId%>&CID=<% =CustId%>" width="100%" height="525px;" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
              <div class="modal fade" id="TemplateSetReviwer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 1039px;margin-left: -257px;">
                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <label style="font-size: 18px;">Select user to assign</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <div class="row" style="padding-top: 10px;">                                
                                <div class="col-md-12 colpadding0" style="max-height: 341px;overflow-x: auto;">
                                    
                                            <asp:GridView runat="server" ID="grdTemplateSection" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                                AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                                                OnRowDataBound="grdTemplateSection_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="Header" HeaderText="Section" ItemStyle-Width="25%" />
                                                    <asp:BoundField DataField="DeptName" HeaderText="Department" ItemStyle-Width="23%" />
                                                    <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="25%">
                                                        <HeaderTemplate>
                                                            <asp:DropDownListChosen AutoPostBack="true" runat="server" ID="ddlReviewer1" class="form-control" Width="100%"
                                                                OnSelectedIndexChanged="ddlReviewer1_SelectedIndexChanged"
                                                                AllowSingleDeselect="false" DataPlaceHolder="Select Reviewer" DisableSearchThreshold="5">
                                                            </asp:DropDownListChosen>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSectionID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblStatusID" runat="server" Text='<%# Eval("SectionStatusID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblDeptID" runat="server" Text='<%# Eval("DeptID") %>' Visible="false"></asp:Label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlReviewer" class="form-control" Width="100%"
                                                                AllowSingleDeselect="false" DataPlaceHolder="Select Reviewer" DisableSearchThreshold="5">
                                                            </asp:DropDownListChosen>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="visiblity" ItemStyle-Width="27%">
                                                        <HeaderTemplate>
                                                        <label>Select Section Visbilty to other Users</label>
                                                            <asp:DropDownList runat="server" ID="ddlVisibilityHeader" Visible="false"  class="form-control" Style="width: 100%; height: 32px !important;"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlVisibilityHeader_SelectedIndexChanged">
                                                                <asp:ListItem Text="Show" Value="1" Selected="True" />
                                                                <asp:ListItem Text="Hide" Value="0" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList runat="server" ID="ddlVisibility" class="form-control m-bot15" Style="width: 100%; height: 32px !important;">
                                                                <asp:ListItem Text="Show" Value="1" Selected="True" />
                                                                <asp:ListItem Text="Hide" Value="0" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Record Found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                      
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10px; margin-left: 178px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-6 colpadding0">
                                        <asp:Button Text="Submit for Review" runat="server" Style="float: right;" ID="btnAssignSections"
                                            CssClass="btn btn-primary" onclick="btnAssignSections_Click"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
    
    </form>
</body>
</html>
