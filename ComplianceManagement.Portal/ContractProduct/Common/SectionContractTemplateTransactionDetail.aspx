﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SectionContractTemplateTransactionDetail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.SectionContractTemplateTransactionDetail" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    
     <!-- Bootstrap CSS -->
  <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
  
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet"  type="text/css" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />
    <script>
        
            var editor, range;
            function OnClientLoad(sender, args) {
                editor = sender;
            }

            function OnClientSelectionChange(sender, args) {
                range = editor.getSelection().getRange(true);
            }

            function Button_OnClick() {                
                if (range) {
                    editor.getSelection().selectRange(range);
                }
                var e = document.getElementById("ddlContractTemplate");
                var selectedLocation = e.options[e.selectedIndex].value;
                editor.pasteHtml(selectedLocation);                
            }
                        
            function CloseModel() {                
                window.parent.CloseSectionModal();
                //window.parent.ClosedSectionTemplate();                
            }
            function validationmsg()
            {
                alert("Please select Reviewer");
                return false;
            }
            function GetAllTransaction()
            {
                document.getElementById("getHistryInsert").value = "";
                document.getElementById("getHistryDel").value = "";
                if (editor._contentArea.getElementsByTagName("ins").length > 0)
                {                    
                    var getdetails = "";
                    for (var i = 0; i < editor._contentArea.getElementsByTagName("ins").length; i++)
                    {
                        if (document.getElementById("lblUser").innerText == editor._contentArea.getElementsByTagName("ins")[i].getAttribute("author"))
                        {
                            var text = editor._contentArea.getElementsByTagName("ins")[i].innerText + "andand";
                            var author = editor._contentArea.getElementsByTagName("ins")[i].getAttribute("author") + "andand";
                            var title = editor._contentArea.getElementsByTagName("ins")[i]["title"] + "andand";

                            var res = text.concat(author, title);
                            getdetails = getdetails.concat(res + "endend");
                        }
                    }
                    document.getElementById("getHistryInsert").value = getdetails;
                }


                if (editor._contentArea.getElementsByTagName("del").length > 0) {
                    var getdeletedetails = "";
                    for (var i = 0; i < editor._contentArea.getElementsByTagName("del").length; i++) {
                        if (document.getElementById("lblUser").innerText == editor._contentArea.getElementsByTagName("del")[i].getAttribute("author")) {
                            var text1 = editor._contentArea.getElementsByTagName("del")[i].innerText + "andand";
                            var author1 = editor._contentArea.getElementsByTagName("del")[i].getAttribute("author") + "andand";
                            var title1 = editor._contentArea.getElementsByTagName("del")[i]["title"] + "andand";

                            var res1 = text1.concat(author1, title1);
                            getdeletedetails = getdeletedetails.concat(res1 + "endend");
                        }
                    }
                    document.getElementById("getHistryDel").value = getdeletedetails;
                }
                return true;
            }

            function OpenSectionModal(templateID, viewOnly) {
                debugger;                
                $('#TemplateSectionPopup').modal('show');
                return false;
            }

        function CloseSectionModal() {
            $('#TemplateSectionPopup').modal('hide');
        }
        // Specify the normal table row background color
        //   and the background color for when the mouse 
        //   hovers over the table row.

        var TableBackgroundNormalColor = "#ffffff";
        var TableBackgroundMouseoverColor = "#D3D3D3";

        // These two functions need no customization.
        function ChangeBackgroundColor(row) { row.style.backgroundColor = TableBackgroundMouseoverColor; }
        function RestoreBackgroundColor(row) { row.style.backgroundColor = TableBackgroundNormalColor; }

    </script> 
    <style type="text/css">
        .dropdownclass {
            color: rgb(142, 142, 147);
            background-color: rgb(255, 255, 255);
            padding: 6px 12px;
            border-width: 1px;
            border-style: solid;
            border-color: rgb(199, 199, 204);
            width: 24%;
            height: 32px !important;
            margin-right: 6%;
            border-image: initial;
            border-radius: 4px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        }


    </style>
</head>
 
<body>
    <form id="form1" runat="server">
                <div class="row">
                    <div class="col-md-12 colpadding0 ">
                        <asp:DropDownList runat="server" ID="ddlUser" CssClass="dropdownclass">
                        </asp:DropDownList>
                        <div style="float:right;margin-right:0.3%;">
                        <asp:Button ID="btnPrimaryApprover" runat="server" OnClientClick="return GetAllTransaction();" Text="Approve" CssClass="btn btn-primary" OnClick="btnPrimaryApprover_Click" />                        
                        <asp:Button ID="btnPrimaryRejetced" runat="server" OnClientClick="return GetAllTransaction();" Text="Rejected" CssClass="btn btn-primary" OnClick="btnPrimaryRejetced_Click" />
                        <asp:Button ID="Button4" runat="server" OnClientClick="return GetAllTransaction();" Text="Approve" CssClass="btn btn-primary" OnClick="Button4_Click" />                        
                        <asp:Button ID="Button3" runat="server" OnClientClick="return GetAllTransaction();" Text="Rejected" CssClass="btn btn-primary" OnClick="Button3_Click" />
                        <asp:Button ID="btnSubmit" runat="server" Text="Approve" CssClass="btn btn-primary" OnClientClick="return GetAllTransaction();" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnSubmitr" runat="server" Text="Reject" CssClass="btn btn-primary" OnClientClick="return GetAllTransaction();" OnClick="btnSubmitr_Click" />
                        <asp:Button ID="btnforClosed" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="return GetAllTransaction();" OnClick="btnforClosed_Click" />
                    </div>
                    </div>
                    <div class="col-md-12 colpadding0">
                        
                        <div class="col-md-1 colpadding0">
                        
                        </div>
                        <div class="col-md-1 colpadding0">
                        <asp:Button ID="btnforApproval" runat="server" OnClientClick="return GetAllTransaction();" Text="Approval" CssClass="btn btn-primary" style="display:none;" OnClick="btnforApproval_Click" />    
                        </div>
                        <div class="col-md-1 colpadding0" style="display: none;">
                            User :
                            <asp:Label ID="lblUser" runat="server" Text="P"></asp:Label>
                            <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="Save Version" OnClick="Button1_Click" />
                        </div>
                        <div class="col-md-1 colpadding0" style="display: none;">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Html" OnClick="btnSave_Click" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2 colpadding0" style="margin-right: 14px;display:none;">
                            
                        </div>                       
                        <div class="col-md-1 colpadding0">
                            <asp:TextBox ID="getHistryInsert" runat="server" Style="display: none;"></asp:TextBox>
                            <asp:TextBox ID="getHistryDel" runat="server" Style="display: none;"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 colpadding0" style="margin-left: -2px;">

                        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Visible="false" />
                        <div class="demo-containers" style="padding-top: 6px;">
                            <div class="demo-container">
                                <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" StripFormattingOptions="MSWord" EnableTrackChanges="true" runat="server"
                                    Width="974px" OnClientLoad="OnClientLoad" OnClientSelectionChange="OnClientSelectionChange"
                                    Height="420px" ToolsFile="../../ToolsFile.xml" OnExportContent="RadEditor1_ExportContent"
                                    ContentFilters="DefaultFilters, PdfExportFilter"
                                    SkinID="WordLikeExperience" EditModes="Design,Preview">
                                    <RealFontSizes>
                                        <telerik:EditorRealFontSize Value="12pt" />
                                        <telerik:EditorRealFontSize Value="18pt" />
                                        <telerik:EditorRealFontSize Value="22px" />
                                    </RealFontSizes>
                                    <ExportSettings>
                                        <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                                            PageHeader="Some header text for DOCX documents" />
                                        <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                                            HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                                    </ExportSettings>
                                    <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true"
                                        UserCssId="reU0"></TrackChangesSettings>
                                    <Content>                  
                                    </Content>
                                </telerik:RadEditor>
                            </div>
                        </div>
                        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                            <AjaxSettings>
                                <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                            </AjaxSettings>
                        </telerik:RadAjaxManager>
                        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                        </telerik:RadAjaxLoadingPanel>
                        <div runat="server" id="divHtml"></div>
                    </div>
                </div>              
 </form>
</body>
</html>

