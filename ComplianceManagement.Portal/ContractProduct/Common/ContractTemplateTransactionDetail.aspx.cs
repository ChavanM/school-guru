﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class ContractTemplateTransactionDetail : System.Web.UI.Page
    {
        protected static int ContractTemplateId;
        protected static int CustId;
        protected static int UId;
        protected static int RoleId;
        protected static int SectionId;
        protected static int CurrentStatusId;
        protected static string customerId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);
                customerId = Convert.ToString(ConfigurationManager.AppSettings["IsViewToUser"]);
                SetDefaultConfiguratorValues(UserID, RoleID, ContractTemplateID);
                BindTransactionHistory(ContractTemplateID, RoleID);
                //BindSections_All(grdTemplateSection, ContractTemplateID);
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                lblVersion.Text = ContractVersion;
                if(customerId == Convert.ToString(customerID))
                {
                    IsViewUser(ContractTemplateID, RoleID);
                }
            }
        }
        public void IsViewUser(int ContractTemplateID,int roleID)
        {     
            string userid = Request.QueryString["UID"]; 
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {     
                var queryResult = (from row in entities.Cont_tbl_VisibleToUser
                                    where row.ContractInstanceID == ContractTemplateID
                                    && row.VisibkeUserID == userid
                                   && row.IsActive == true
                                    select row).FirstOrDefault();
                if (queryResult != null)
                {

                    if (queryResult.VisibkeUserID == userid && queryResult.ContractInstanceID == ContractTemplateID)
                    {
                        btnVendor.Enabled = false;
                        btnReviwer.Enabled = false;
                        btnApprover.Enabled = false;
                        btnconvertcontract.Enabled = false;
                        btnrenew.Enabled = false;
                        btnExportDocReport.Enabled = false;
                    }
                    else
                    {
                        BindTransactionHistory(ContractTemplateID, roleID);
                    }
                }
                else
                {
                    BindTransactionHistory(ContractTemplateID, roleID);
                }
            }
        }

        public void BindSections_All(GridView grdTemplateSection, long ContractTemplateID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                grdTemplateSection.DataSource = ContractTemplateManagement.GetContractSections_All(customerID, Convert.ToInt32(ContractTemplateID));
                grdTemplateSection.DataBind();
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTemplateSection_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTime"]);
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    DropDownList ddlUser1 = (DropDownList)e.Row.FindControl("ddlReviewer1");
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int userID = -1;
                    userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<object> internalUsers = new List<object>();

                        string CacheName = "internalUsersforContract" + userID + "_" + customerID;
                        internalUsers = (List<object>)HttpContext.Current.Cache[CacheName];
                        if (internalUsers == null)
                        {
                            var lstAllUsers1 = ContractUserManagement.GetAllUsers_Contract(customerID);
                            if (lstAllUsers1.Count > 0)
                                lstAllUsers1 = lstAllUsers1.Where(entry => entry.ContractRoleID != null).ToList();

                            lstAllUsers1 = lstAllUsers1.Where(x => x.ID != userID).ToList();

                            internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers1, 1); //1--Internal---
                           
                            HttpContext.Current.Cache.Insert(CacheName, internalUsers, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                        }

                        ddlUser1.DataValueField = "ID";
                        ddlUser1.DataTextField = "Name";
                        ddlUser1.DataSource = internalUsers;
                        ddlUser1.DataBind();
                        ddlUser1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Reviewer", "-1"));
                    }
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Label lblDeptID = (System.Web.UI.WebControls.Label)e.Row.FindControl("lblDeptID");
                    if (lblDeptID != null)
                    {
                        int deptId = Convert.ToInt32(lblDeptID.Text);
                        if (!string.IsNullOrEmpty(lblDeptID.Text) || lblDeptID.Text != "0")
                        {
                            DropDownList ddlUser = (DropDownList)e.Row.FindControl("ddlReviewer");
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                            int userID = -1;
                            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                            List<object> internalUsers = new List<object>();

                            string CacheName = "internalUsersforContract" + userID + "_" + customerID;
                            internalUsers = (List<object>)HttpContext.Current.Cache[CacheName];
                            if (internalUsers == null)
                            {
                                var lstAllUsers1 = ContractUserManagement.GetAllUsers_Contract(customerID);
                                if (lstAllUsers1.Count > 0)
                                    lstAllUsers1 = lstAllUsers1.Where(entry => entry.ContractRoleID != null).ToList();

                                internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers1, 1); //1--Internal---

                                HttpContext.Current.Cache.Insert(CacheName, internalUsers, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                            }

                            ddlUser.DataValueField = "ID";
                            ddlUser.DataTextField = "Name";
                            ddlUser.DataSource = internalUsers;
                            ddlUser.DataBind();
                            ddlUser.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Reviewer", "-1"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public int getContractStatusID(int ContractTemplateId)
        {
            int StatusID = -1;
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = (from row in entities.Cont_View_RecentContractStatusTransaction
                                join row1 in entities.Cont_tbl_ContractInstance
                                on row.ContractID equals row1.ID
                                where row1.ID == ContractTemplateId
                                                       && row1.IsDeleted == false
                                                       && row.CustomerID == customerID
                                select row).FirstOrDefault();
                    if (data != null)
                    {
                        if (data.ContractStatusID != null)
                            StatusID = Convert.ToInt32(data.ContractStatusID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return StatusID;
        }

        public int getDocTypeID(int customerID)
        {
            int DoctypeID = -1;
            try
            {
                string DocName = "Contract Document";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_DocumentTypeMaster data = (from row in entities.Cont_tbl_DocumentTypeMaster
                                                        where row.TypeName == DocName
                                                        && row.CustomerID == customerID
                                                        select row).FirstOrDefault();
                    if (data != null)
                    {
                        data.IsDeleted = false;
                        entities.SaveChanges();
                        DoctypeID = Convert.ToInt32(data.ID);
                    }
                    else
                    {
                        Cont_tbl_DocumentTypeMaster _objContDoc = new Cont_tbl_DocumentTypeMaster()
                        {
                            TypeName = DocName,
                            CustomerID = (int)customerID,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            IsDeleted = false
                        };

                        long newDocTypeID = 0;

                        newDocTypeID = ContractMastersManagement.CreateDocumentType(_objContDoc);
                        DoctypeID = Convert.ToInt32(newDocTypeID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return DoctypeID;
        }

        public int getRID(string roleCode)
        {
            int RoleID = -1;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Role data = (from row in entities.Roles
                                 where row.Code == roleCode
                                 select row).FirstOrDefault();
                    if (data != null)
                    {
                        RoleID = Convert.ToInt32(data.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return RoleID;
        }

        public int getRoleID(int ContractTemplateId, int UserID, int sectionID)
        {
            int RoleID = -1;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                       where row.ContractTemplateInstanceID == ContractTemplateId
                                                       && row.UserID == UserID
                                                       && row.SectionID == sectionID
                                                       select row).FirstOrDefault();
                    if (data != null)
                    {
                        RoleID = Convert.ToInt32(data.RoleID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return RoleID;
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    SectionId = Convert.ToInt32(commandArgs[0]);
                    int StatusId = Convert.ToInt32(commandArgs[1]);
                    ContractTemplateId = Convert.ToInt32(Request.QueryString["ContractTID"]);
                    RoleId = Convert.ToInt32(Request.QueryString["RID"]);
                    //RoleId = getRoleID(ContractTemplateId, AuthenticationHelper.UserID, SectionId);
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    CurrentStatusId = StatusId;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var entitiesData = (from row in entities.ContractTemplateAssignments
                                            where row.ContractTemplateInstanceID == ContractTemplateId
                                              && row.UserID == UId
                                              && row.IsActive == true
                                              && row.SectionID == SectionId
                                              && row.RoleID == RoleId
                                            select row).FirstOrDefault();
                        if (entitiesData != null)
                        {
                            int RID = Convert.ToInt32(entitiesData.RoleID);
                            if (RID == 3)
                            {
                                if (StatusId == 1 || StatusId == 15)//|| StatusId == 3   || StatusId == 17
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                            if (RID == 4)
                            {
                                if (StatusId == 2)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public class ContractSectionDetail
        {
            public string Header { get; set; }
            public long ContractTemplateInstanceID { get; set; }
            public long SectionID { get; set; }
            public Nullable<int> SectionOrder { get; set; }
            public int CreatedBy { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string DeptName { get; set; }
            public string DeptColor { get; set; }
            public string SectionStatusID { get; set; }
            public string SectionStatus { get; set; }
            public string SectionReviwerName { get; set; }
            public int Visbilty { get; set; }
        }

        public void BindTransactionHistory(long TemplateInstanceID, long RoleID)
        {
            try
            {
                btnVendor.Enabled = false;
                btnReviwer.Enabled = false;
                btnApprover.Enabled = false;
                btnconvertcontract.Enabled = false;
                btnrenew.Enabled = false;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int UserID = -1;
                    UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                    var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateInstanceID))
                                        select row).ToList();
                    if (entitiesData != null)
                    {
                        var details = (from row in entitiesData
                                       select new ContractSectionDetail()
                                       {
                                           Header = row.Header,
                                           ContractTemplateInstanceID = row.ContractTemplateInstanceID,
                                           SectionID = row.SectionID,
                                           SectionOrder = row.SectionOrder,
                                           CreatedBy = row.CreatedBy,
                                           CreatedOn = row.CreatedOn,
                                           DeptName = row.DeptName,
                                           DeptColor = row.DeptColor,
                                           SectionStatusID = row.SectionStatusID,
                                           SectionStatus = row.SectionStatus,
                                           SectionReviwerName = row.SectionReviwerName,
                                           Visbilty = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.SectionID, RoleID)
                                       }).ToList();

                        if (details.Count > 0)
                        {
                            int SectionTemplateID = getContractStatusID(Convert.ToInt32(TemplateInstanceID)); //Convert.ToInt32(Request.QueryString["SectionStatusID"]);
                            if (SectionTemplateID == 7)
                            {
                                btnReviwer.Enabled = false;
                                btnVendor.Enabled = false;
                                btnApprover.Enabled = false;
                                btnconvertcontract.Enabled = false;
                                btnrenew.Enabled = true;
                            }
                            else if (SectionTemplateID != 7)
                            {
                                //review
                                if (details.Count == details.Where(x => x.SectionStatusID == "1").ToList().Count)
                                {
                                    btnReviwer.Enabled = true;
                                    btnVendor.Enabled = false;
                                    btnApprover.Enabled = false;
                                    btnconvertcontract.Enabled = false;
                                }

                                else if (details.Count == details.Where(x => x.SectionStatusID == "15").ToList().Count)
                                {
                                    btnReviwer.Enabled = true;
                                    btnVendor.Enabled = false;
                                    btnApprover.Enabled = false;
                                    btnconvertcontract.Enabled = false;
                                }

                                //vendor rev
                                else if (details.Count == details.Where(x => x.SectionStatusID == "3").ToList().Count)
                                {
                                    btnReviwer.Enabled = false;
                                    btnVendor.Enabled = true;
                                    btnApprover.Enabled = false;
                                    btnconvertcontract.Enabled = false;
                                }

                                else if (details.Count == details.Where(x => x.SectionStatusID == "17").ToList().Count)
                                {
                                    btnReviwer.Enabled = false;
                                    btnVendor.Enabled = false;
                                    btnApprover.Enabled = true;
                                    btnconvertcontract.Enabled = false;
                                }

                                else if (details.Count == details.Where(x => x.SectionStatusID == "5").ToList().Count)
                                {
                                    btnReviwer.Enabled = false;
                                    btnVendor.Enabled = false;
                                    btnApprover.Enabled = false;
                                    btnconvertcontract.Enabled = true;
                                }
                                else
                                {
                                    if (details.Where(x => x.SectionStatusID == "1" || x.SectionStatusID == "15").ToList().Count > 0)
                                    {
                                        btnReviwer.Enabled = true;
                                        btnVendor.Enabled = false;
                                        btnApprover.Enabled = false;
                                        btnconvertcontract.Enabled = false;
                                    }
                                    else if (details.Where(x => x.SectionStatusID == "3").ToList().Count > 0)
                                    {
                                        btnReviwer.Enabled = false;
                                        btnVendor.Enabled = true;
                                        btnApprover.Enabled = false;
                                        btnconvertcontract.Enabled = false;
                                    }
                                    else if (details.Where(x => x.SectionStatusID == "17").ToList().Count > 0)
                                    {
                                        btnReviwer.Enabled = false;
                                        btnVendor.Enabled = false;
                                        btnApprover.Enabled = false;
                                        //btnApprover.Enabled = true;
                                        btnconvertcontract.Enabled = false;
                                    }
                                    else if (details.Where(x => x.SectionStatusID == "5").ToList().Count == details.Count)
                                    {
                                        btnReviwer.Enabled = false;
                                        btnVendor.Enabled = false;
                                        btnApprover.Enabled = false;
                                        btnconvertcontract.Enabled = true;
                                    }
                                }
                            }                            
                        }
                        if (details.Count > 0)
                        {
                            details = details.Where(x => x.Visbilty == 1).ToList();
                        }
                        rptComplianceVersionView.DataSource = details.OrderBy(entry => entry.SectionOrder).ToList();
                        rptComplianceVersionView.DataBind();
                    }
                    else
                    {
                        rptComplianceVersionView.DataSource = null;
                        rptComplianceVersionView.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public int geVisiblityStatus(long ContractTemplateId, int UserID, long sectionID, long RoleID)
        {
            int OutputVisiblity = 1;
            try
            {
                if (RoleID == 4)
                {
                    OutputVisiblity = 0;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (ContractTemplateId != null && UserID != null && sectionID != null)
                        {
                            ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                               where row.ContractTemplateInstanceID == ContractTemplateId
                                                               && row.UserID == UserID
                                                               && row.SectionID == sectionID
                                                               select row).FirstOrDefault();
                            if (data != null)
                            {
                                if (data.Visibility == 1)
                                {
                                    OutputVisiblity = 1;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return OutputVisiblity;
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }

        private void SetDefaultConfiguratorValues(int UserID, int RoleID, int ContractTemplateID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string UserName = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == UserID
                                 && row.ContractTemplateInstanceID == ContractTemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();

                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == ContractTemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        theEditor.Content = strExporttoWord.ToString();
                        theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                        theEditor.EnableTrackChanges = false;
                    }
                    else
                    {
                        theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                        theEditor.EnableTrackChanges = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void RadEditor1_ExportContent(object sender, EditorExportingArgs e)
        {
            ExportType exportType = e.ExportType;

            if (exportType == ExportType.Word)
            {
                //string exportedOutput = e.ExportOutput;

                //Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                //DocxFormatProvider docxProvider = new DocxFormatProvider();
                //RadFlowDocument document = docxProvider.Import(output);

                //Header defaultHeader = document.Sections.First().Headers.Add();
                //Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                //defaultHeaderParagraph.TextAlignment = Alignment.Right;
                //defaultHeaderParagraph.Inlines.AddRun("This is a sample header.");

                //Footer defaultFooter = document.Sections.First().Footers.Add();
                //Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                //defaultFooterParagraph.TextAlignment = Alignment.Right;
                //defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                //Byte[] modifiedOutput = docxProvider.Export(document);
                //string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                //e.ExportOutput = finalOutput;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            theEditor.ExportToDocx();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            divHtml.InnerHtml = theEditor.Content;
        }

        protected void btnAssignSections_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                List<int> revid = new List<int>();
                revid.Clear();
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);
                if (grdTemplateSection != null)
                {
                    string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                    for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                    {
                        if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                        {
                            int ReviewerId = -1;
                            System.Web.UI.WebControls.Label lblStatusID = (System.Web.UI.WebControls.Label)grdTemplateSection.Rows[i].FindControl("lblStatusID");
                            System.Web.UI.WebControls.Label lblSectionID = (System.Web.UI.WebControls.Label)grdTemplateSection.Rows[i].FindControl("lblSectionID");
                            DropDownList ddluser = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlReviewer");
                            DropDownList ddlVisibility = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlVisibility");

                            int StatusID = -1;
                            if (lblStatusID.Text != "" && lblStatusID.Text != "-1")
                            {
                                StatusID = Convert.ToInt32(lblStatusID.Text);
                            }
                            int visiblty = 1;
                            if (ddlVisibility.SelectedValue != "" && ddlVisibility.SelectedValue != "-1")
                            {
                                visiblty = Convert.ToInt32(ddlVisibility.SelectedValue);
                            }
                            if (ddluser.SelectedValue != "" && ddluser.SelectedValue != "-1")
                            {
                                ReviewerId = Convert.ToInt32(ddluser.SelectedValue);
                            }
                            if (lblSectionID != null && ReviewerId != -1 && StatusID != -1)
                            {
                                if (!string.IsNullOrEmpty(lblSectionID.Text) || lblSectionID.Text != "0")
                                {
                                    if (StatusID == 1 || StatusID == 15)// StatusID == 3
                                    {
                                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                        {
                                            CustomerID = customerID,
                                            ContractID = Convert.ToInt32(ContractTemplateID),
                                            StatusID = 2,
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = UserID,
                                            UpdatedBy = UserID,
                                        };
                                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                                        ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                                        {
                                            ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                                            RoleID = 4,
                                            UserID = ReviewerId,
                                            IsActive = true,
                                            SectionID = Convert.ToInt32(lblSectionID.Text),
                                            Visibility = visiblty
                                        };
                                        ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);
                                        int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                                        revid.Add(ReviewerId);
                                        int sectionID = Convert.ToInt32(lblSectionID.Text);
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            string ContractTemplate = string.Empty;
                                            var data12 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                          where row.UserID == UserID
                                                          && row.ContractTemplateInstanceID == ContractTemplateID
                                                          && row.RoleID == RoleID
                                                          && row.sectionID == sectionID
                                                          select row).FirstOrDefault();
                                            if (data12 != null)
                                            {
                                                ContractTemplate = data12.ContractTemplateContent;
                                            }

                                            ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                            obj.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                                            obj.CreatedOn = DateTime.Now;
                                            obj.CreatedBy = UserID;
                                            obj.ContractTemplateContent = ContractTemplate;
                                            obj.SectionID = Convert.ToInt32(lblSectionID.Text);
                                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                            obj.Status = 2;
                                            obj.Version = ContractVersion;

                                            if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                                            {
                                                obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                                            }
                                            entities.ContractTemplateTransactions.Add(obj);
                                            entities.SaveChanges();
                                            int TransactionID = Convert.ToInt32(obj.Id);
                                            if (TransactionID > 0)
                                            {
                                                #region set transaction history
                                                int TemplateID = ContractTemplateID;

                                                var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                                                    select row).ToList();

                                                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                             where row.UserID == UserID
                                                             && row.ContractTemplateInstanceID == TemplateID
                                                             && row.RoleID == 3
                                                             select row).ToList();
                                                if (data1.Count > 0)
                                                {
                                                    var data = (from row in data1
                                                                join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                                on row.sectionID equals row1.SectionID
                                                                where row1.ContractTemplateInstanceID == TemplateID
                                                                select new getTemplateTransactionDetail
                                                                {
                                                                    TemplateTransactionID = row.TemplateTransactionID,
                                                                    TemplateInstanceID = row.ContractTemplateInstanceID,
                                                                    ComplianceStatusID = row.ComplianceStatusID,
                                                                    CreatedOn = row.CreatedOn,
                                                                    Status = row.Status,
                                                                    StatusChangedOn = row.StatusChangedOn,
                                                                    TemplateContent = row.ContractTemplateContent,
                                                                    RoleID = row.RoleID,
                                                                    UserID = row.UserID,
                                                                    sectionID = Convert.ToInt32(row.sectionID),
                                                                    orderID = Convert.ToInt32(row1.SectionOrder),
                                                                    visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                                }).OrderBy(x => x.orderID).ToList();

                                                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                                    data.ForEach(eachSection =>
                                                    {
                                                        if (eachSection.visibilty == 1)
                                                        {
                                                            strExporttoWord.Append(@"<div>");
                                                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                                            strExporttoWord.Append(@"</div>");
                                                        }

                                                        strExporttoWord.Append(@"<div>");
                                                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                                        strExporttoWord.Append(@"</div>");
                                                    });

                                                    ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                                    obj1.ContractTemplateID = TemplateID;
                                                    obj1.CreatedOn = DateTime.Now;
                                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                                    obj1.TemplateContent = strExporttoWord.ToString();
                                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                                    obj1.Status = 1;
                                                    obj1.OrderID = 1;
                                                    entities.ContractTemplateTransactionHistories.Add(obj1);
                                                    entities.SaveChanges();
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseSectionModal();", true);
                        }
                    }
                    revid = revid.Distinct().ToList();
                    if (revid.Count > 0)
                    {
                        SendReviewers(revid, ContractTemplateID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void SendReviewers(List<int> revid, int ContractTemplateID,int cid)
        {
            if (revid.Count > 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var contractdata = (from row in entities.Cont_tbl_ContractInstance
                                        where row.ID == ContractTemplateID
                                        && row.IsDeleted == false
                                        select row).FirstOrDefault();

                    if (contractdata != null)
                    {
                        foreach (var item in revid)
                        {
                            try
                            {
                                User data = (from row in entities.Users
                                             where row.ID == item
                                             && row.CustomerID == cid
                                             && row.IsDeleted == false
                                             select row).FirstOrDefault();
                                if (data != null)
                                {
                                    string reviewername = data.FirstName + " " + data.LastName;
                                    string contname = contractdata.ContractTitle;
                                    string contnum = contractdata.ContractNo;

                                    string header = "Dear " + reviewername + "<br>" + "<br>" + "Contract has been shared with you for review." + "<br>" + "<br>";

                                    string ContractName = "Contract Name - " + contname + "<br>" + "<br>";

                                    string ContractNo = "Contract no. - " + contnum + "<br>" + "<br>";

                                    string FinalMail = header + ContractName + ContractNo;

                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                                    string ReviewerEmail = data.Email;

                                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { ReviewerEmail }), null, null, "You have received a contract for review", FinalMail);
                                }
                            }
                            catch (Exception ex)
                            {
                                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                    }
                }
            }
        }
        protected void btnReviwer_Click(object sender, EventArgs e)
        {
            try
            {
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                BindSections_All(grdTemplateSection, ContractTemplateID);

                if (grdTemplateSection != null)
                {
                    for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                    {
                        if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                        {
                            System.Web.UI.WebControls.Label lblSectionID = (System.Web.UI.WebControls.Label)grdTemplateSection.Rows[i].FindControl("lblSectionID");
                            DropDownList ddluser = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlReviewer");
                            if (lblSectionID.Text != null)
                            {
                                if (lblSectionID.Text != "0")
                                {
                                    int sectionID = Convert.ToInt32(lblSectionID.Text);

                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        var getdetails = (from row in entities.ContractTemplateAssignments
                                                          where row.ContractTemplateInstanceID == ContractTemplateID
                                                          && row.SectionID == sectionID
                                                          && row.IsActive == true
                                                          && row.RoleID == 4
                                                          select row).FirstOrDefault();

                                        if (getdetails != null)
                                        {
                                            if (getdetails.UserID != null)
                                                ddluser.SelectedValue = Convert.ToString(getdetails.UserID);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenInternalReviwerSectionModal();", true);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlReviewer1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string ddlHeader = (((DropDownList)sender).SelectedValue);
                for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlReviewer");

                    ddlReviewer.SelectedValue = (((DropDownList)sender).SelectedValue);
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenInternalReviwerSectionModal();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlVisibilityHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string ddlHeader = (((DropDownList)sender).SelectedValue);
                for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdTemplateSection.Rows[i].FindControl("ddlVisibility");

                    ddlReviewer.SelectedValue = (((DropDownList)sender).SelectedValue);
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenInternalReviwerSectionModal();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void BtnFullHistory_Click(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(Request.QueryString["UID"]);
            RoleId = Convert.ToInt32(Request.QueryString["RID"]);
            ContractTemplateId = Convert.ToInt32(Request.QueryString["ContractTID"]);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenHistorySectionModal();", true);
        }

        protected void btnVendor_Click(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(Request.QueryString["UID"]);
            RoleId = Convert.ToInt32(Request.QueryString["RID"]);
            ContractTemplateId = Convert.ToInt32(Request.QueryString["ContractTID"]);
            BindVendors(ContractTemplateId, CustId);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenVendorSectionModal();", true);
        }

        public void BindVendors(int ContractTemplateId, int CustId)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstVendors = ContractMastersManagement.GetVendors_All(customerID, ContractTemplateId);

                if (lstVendors.Count > 0)
                    lstVendors = lstVendors.OrderBy(row => row.VendorName).ToList();

                ddlVendRev.DataValueField = "ID";
                ddlVendRev.DataTextField = "VendorName";
                ddlVendRev.DataSource = lstVendors;
                ddlVendRev.DataBind();
                ddlVendRev.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Reviewer", "-1"));
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlVendRev_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlVendRev.SelectedValue != "-1")
                {
                    using (ContractMgmtEntities entities = new ContractMgmtEntities())
                    {
                        int vendorID = Convert.ToInt32(ddlVendRev.SelectedValue);

                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        int userID = -1;
                        userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                        Cont_tbl_VendorMaster data = (from row in entities.Cont_tbl_VendorMaster
                                                      where row.ID == vendorID
                                                           && row.CustomerID == customerID
                                                      select row).FirstOrDefault();
                        if (data != null)
                        {
                            EmailTxt.Text = data.Email;
                            txtContactNum.Text = data.ContactNumber;
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenVendorSectionModal();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnvendr_Click(object sender, EventArgs e)
        {
            try
            {
                bool validation = true;
                List<string> lstErrorMsg = new List<string>();
                if (ddlVendRev.SelectedValue == "" || ddlVendRev.SelectedValue == "-1")
                {
                    validation = false;
                    lstErrorMsg.Add("Please Select Vendor");
                }
                if (string.IsNullOrEmpty(EmailTxt.Text))
                {
                    validation = false;
                    lstErrorMsg.Add("Please Vendor Email");
                }
                if (string.IsNullOrEmpty(txtContactNum.Text))
                {
                    validation = false;
                    lstErrorMsg.Add("Please Vendor Contact");
                }


                if (lstErrorMsg.Count > 0)
                {
                    validation = false;
                    showErrorMessages(lstErrorMsg, cvAddEditContractType);
                }

                if (validation)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                        int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                        int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);
                        int vendorID = Convert.ToInt32(ddlVendRev.SelectedValue);
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);

                        int userID = -1;
                        userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                        var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                     where row.UserID == UserID
                                     && row.ContractTemplateInstanceID == ContractTemplateID
                                     && row.RoleID == RoleID
                                     select row).ToList();

                        var gettemplatesection = (from row in data1
                                                  join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                  on row.sectionID equals row1.SectionID
                                                  where row1.ContractTemplateInstanceID == ContractTemplateID
                                                  select new getTemplateTransactionDetail
                                                  {
                                                      TemplateTransactionID = row.TemplateTransactionID,
                                                      TemplateInstanceID = row.ContractTemplateInstanceID,
                                                      ComplianceStatusID = row.ComplianceStatusID,
                                                      CreatedOn = row.CreatedOn,
                                                      Status = row.Status,
                                                      StatusChangedOn = row.StatusChangedOn,
                                                      TemplateContent = row.ContractTemplateContent,
                                                      RoleID = row.RoleID,
                                                      UserID = row.UserID,
                                                      sectionID = Convert.ToInt32(row.sectionID),
                                                      orderID = Convert.ToInt32(row1.SectionOrder)
                                                  }).OrderBy(x => x.orderID).ToList();

                        if (gettemplatesection.Count > 0)
                        {
                            int RID = getRID("VR");
                            foreach (var item in gettemplatesection)
                            {
                                if (item.ComplianceStatusID == 3)
                                {
                                    ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                                    {
                                        ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                                        RoleID = RID,
                                        UserID = vendorID,
                                        IsActive = true,
                                        SectionID = item.sectionID,
                                        Visibility = 1
                                    };
                                    ContractTemplateManagement.CreateContractTemplateAssignment1(ContracttemplateAssignment);

                                    string templateSectionContent = item.TemplateContent;

                                    ContractTemplateTransaction obj1 = new ContractTemplateTransaction();
                                    obj1.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                                    obj1.CreatedOn = DateTime.Now;
                                    obj1.CreatedBy = userID;
                                    obj1.ContractTemplateContent = templateSectionContent;
                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                    obj1.Status = 16;
                                    obj1.OrderID = 1;
                                    obj1.Version = ContractVersion;
                                    obj1.SectionID = item.sectionID;
                                    entities.ContractTemplateTransactions.Add(obj1);
                                    entities.SaveChanges();
                                }
                            }

                            Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                            {
                                CustomerID = customerID,
                                ContractID = Convert.ToInt32(ContractTemplateID),
                                StatusID = 16,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                CreatedBy = UserID,
                                UpdatedBy = UserID,
                            };
                            ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                            int assignedRoleID = RID;

                            if (ContractTemplateID > 0)
                            {
                                var contractdata = (from row in entities.Cont_tbl_ContractInstance
                                                    where row.ID == ContractTemplateID
                                                    && row.IsDeleted == false
                                                    select row).FirstOrDefault();

                                if (contractdata != null)
                                {
                                    try
                                    {
                                        //string checkSum = Util.CalculateMD5Hash(vendorID.ToString() + ContractTemplateID.ToString() + assignedRoleID.ToString());

                                        string vendorname = ddlVendRev.SelectedItem.Text;
                                        string contname = contractdata.ContractTitle;
                                        string contnum = contractdata.ContractNo;
                                        string portalURL = string.Empty;
                                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                        if (Urloutput != null)
                                        {
                                            portalURL = Urloutput.URL;
                                        }
                                        else
                                        {
                                            portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                        }

                                        string AccessURL = Convert.ToString(portalURL) +
                                                                    "/ContractVerifyOtp.aspx?" +
                                                                    "UID=" + vendorID.ToString() +
                                                                     "&RID=" + assignedRoleID.ToString() +
                                                                      "&CustID=" + customerID +
                                                                        "&ContractID=" + ContractTemplateID.ToString();

                                        //string AccessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) +
                                        //                            "/ContractVerifyOtp.aspx?" +
                                        //                            "UID=" + vendorID.ToString() +
                                        //                             "&RID=" + assignedRoleID.ToString() +
                                        //                              "&CustID=" + customerID +
                                        //                                "&ContractID=" + ContractTemplateID.ToString();

                                        string header = "Dear " + vendorname + "<br>" + "<br>" + "Contract has been shared with you for review." + "<br>" + "<br>";

                                        string ContractName = "Contract Name - " + contname + "<br>" + "<br>";

                                        string ContractNo = "Contract no. - " + contnum + "<br>" + "<br>";

                                        string UrlAccess = "Url to access Contract - " + AccessURL;

                                        string FinalMail = header + ContractName + ContractNo + UrlAccess;

                                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                                        string vendorEmail = EmailTxt.Text;

                                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { vendorEmail }), null, null, "You have received a contract for review", FinalMail);
                                    }
                                    catch (Exception ex)
                                    {
                                        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                            }
                            using (ContractMgmtEntities contentities = new ContractMgmtEntities())
                            {
                                Cont_tbl_VendorMaster data = (from row in contentities.Cont_tbl_VendorMaster
                                                              where row.ID == vendorID
                                                                   && row.CustomerID == customerID
                                                              select row).FirstOrDefault();
                                if (data != null)
                                {
                                    data.Email = EmailTxt.Text;
                                    data.ContactNumber = txtContactNum.Text;
                                    contentities.SaveChanges();
                                }
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedRefreshModel();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenVendorSectionModal();", true);
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }

        protected void btnApprover_Click(object sender, EventArgs e)
        {
            try
            {
                //CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //UId = Convert.ToInt32(Request.QueryString["UID"]);
                //RoleId = Convert.ToInt32(Request.QueryString["RID"]);
                //ContractTemplateId = Convert.ToInt32(Request.QueryString["ContractTID"]);

                //int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTime"]);

                //List<object> internalUsers = new List<object>();

                //string CacheName = "internalUsersforContract" + UId + "_" + CustId;
                //internalUsers = (List<object>)HttpContext.Current.Cache[CacheName];
                //if (internalUsers == null)
                //{
                //    var lstAllUsers1 = ContractUserManagement.GetAllUsers_Contract(CustId);
                //    if (lstAllUsers1.Count > 0)
                //        lstAllUsers1 = lstAllUsers1.Where(entry => entry.ContractRoleID != null).ToList();

                //    internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers1, 1); //1--Internal---

                //    HttpContext.Current.Cache.Insert(CacheName, internalUsers, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                //}

                //drplistApprover.DataValueField = "ID";
                //drplistApprover.DataTextField = "Name";
                //drplistApprover.DataSource = internalUsers;
                //drplistApprover.DataBind();
                //drplistApprover.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Reviewer", "-1"));

                //drplistApproverprimary.DataValueField = "ID";
                //drplistApproverprimary.DataTextField = "Name";
                //drplistApproverprimary.DataSource = internalUsers;
                //drplistApproverprimary.DataBind();
                //drplistApproverprimary.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Reviewer", "-1"));

                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                UId = Convert.ToInt32(Request.QueryString["UID"]);
                RoleId = Convert.ToInt32(Request.QueryString["RID"]);
                ContractTemplateId = Convert.ToInt32(Request.QueryString["ContractTID"]);

                int checkApproverCount = ContractManagement.getApproverCountContracttemplate(CustId, ContractTemplateId); ;
                List<ApproverDetail> Apprpverlist = new List<ApproverDetail>();
                for (int i = 1; i <= checkApproverCount; i++)
                {
                    Apprpverlist.Add(new ApproverDetail { ID= i , AproverStep="Approver" });
                }
                rptComplianceApproverView.DataSource = Apprpverlist;
                rptComplianceApproverView.DataBind();

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenApprovarSectionModal();", true);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public class ApproverDetail
        {
            public int ID { get; set; }
            public string AproverStep { get; set; }            
        }
        public class ApproverSelectionDetail
        {
            public int ID { get; set; }
            public int UserID { get; set; }
        }

        protected void btnApproverSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> lstErrorMsg = new List<string>();
                int Count = rptComplianceApproverView.Items.Count;
                int flagvalidation = 0;
                bool flag = false;
                List<ApproverSelectionDetail> Apprpverlist = new List<ApproverSelectionDetail>();

                foreach (RepeaterItem ri in rptComplianceApproverView.Items)
                {
                    DropDownList ddlApproverID = ri.FindControl("drplistdynamicApprover") as DropDownList;

                    System.Web.UI.WebControls.Label ddllableID = ri.FindControl("lblApproverID") as System.Web.UI.WebControls.Label;

                    if (ddlApproverID.Text != "" && ddlApproverID.Text != "-1"
                        && ddllableID.Text != "" && ddllableID.Text != "-1")
                    {
                        flagvalidation = flagvalidation + 1;
                        int Countcheck = Apprpverlist.Where(x => x.UserID == Convert.ToInt32(ddlApproverID.Text)).ToList().Count;
                        if (Countcheck > 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            Apprpverlist.Add(new ApproverSelectionDetail { ID = Convert.ToInt32(ddllableID.Text), UserID = Convert.ToInt32(ddlApproverID.Text) });
                        }
                    }
                }
                if (flag)
                {
                    lstErrorMsg.Add("Please Select Different Approver");

                    if (lstErrorMsg.Count > 0)
                    {
                        showErrorMessages(lstErrorMsg, CustomValidator1);
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenApprovarSectionModal();", true);
                }
                else if (flagvalidation != Count)
                {
                    lstErrorMsg.Add("Please Select All Approver");

                    if (lstErrorMsg.Count > 0)
                    {
                        showErrorMessages(lstErrorMsg, CustomValidator1);
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenApprovarSectionModal();", true);
                }
                else
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                        int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                        int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                        var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                     where row.UserID == UserID
                                     && row.ContractTemplateInstanceID == ContractTemplateID
                                     && row.RoleID == RoleID
                                     select row).ToList();

                        var gettemplatesection = (from row in data1
                                                  join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                  on row.sectionID equals row1.SectionID
                                                  where row1.ContractTemplateInstanceID == ContractTemplateID
                                                  select new getTemplateTransactionDetail
                                                  {
                                                      TemplateTransactionID = row.TemplateTransactionID,
                                                      TemplateInstanceID = row.ContractTemplateInstanceID,
                                                      ComplianceStatusID = row.ComplianceStatusID,
                                                      CreatedOn = row.CreatedOn,
                                                      Status = row.Status,
                                                      StatusChangedOn = row.StatusChangedOn,
                                                      TemplateContent = row.ContractTemplateContent,
                                                      RoleID = row.RoleID,
                                                      UserID = row.UserID,
                                                      sectionID = Convert.ToInt32(row.sectionID),
                                                      orderID = Convert.ToInt32(row1.SectionOrder)
                                                  }).OrderBy(x => x.orderID).ToList();

                        if (gettemplatesection.Count > 0)
                        {
                            ContractTemplateManagement.DeleteContractTemplateAssignmentApprover(Convert.ToInt32(ContractTemplateID));
                            foreach (var item in gettemplatesection)
                            {
                                if (item.ComplianceStatusID == 17)
                                {
                                    foreach (var detail in Apprpverlist)
                                    {
                                        int ApproverID = detail.UserID;
                                        ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                                        {
                                            ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                                            RoleID = 6,
                                            UserID = ApproverID,
                                            //IsActive = true,
                                            SectionID = item.sectionID,
                                            Visibility = 1,
                                            IsAction = 0,
                                            ApproveOrder = detail.ID
                                        };
                                        if (detail.ID == 1)
                                        {
                                            ContracttemplateAssignment.IsActive = true;
                                        }
                                        else
                                        {
                                            ContracttemplateAssignment.IsActive = false;
                                        }
                                        ContractTemplateManagement.CreateContractTemplateAssignmentApprover(ContracttemplateAssignment);
                                    }
                                    string templateSectionContent = item.TemplateContent;

                                    ContractTemplateTransaction obj1 = new ContractTemplateTransaction();
                                    obj1.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                                    obj1.CreatedOn = DateTime.Now;
                                    obj1.CreatedBy = UserID;
                                    obj1.ContractTemplateContent = templateSectionContent;
                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                    obj1.Status = 4;
                                    obj1.OrderID = 1;
                                    obj1.Version = ContractVersion;
                                    obj1.SectionID = item.sectionID;
                                    entities.ContractTemplateTransactions.Add(obj1);
                                    entities.SaveChanges();
                                }
                            }
                            Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                            {
                                CustomerID = customerID,
                                ContractID = Convert.ToInt32(ContractTemplateID),
                                StatusID = 4,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                CreatedBy = UserID,
                                UpdatedBy = UserID,
                            };
                            ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                            if (Apprpverlist.Count > 0)
                            {
                                List<int> revuser = new List<int>();
                                revuser = Apprpverlist.Select(x => x.UserID).ToList();
                                revuser = revuser.Distinct().ToList();
                                SendReviewers(revuser, ContractTemplateID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedRefreshModel();", true);
                }
                #region old Flow Code               
                //List<string> lstErrorMsg = new List<string>();
                //if (ddlselectionApprovar.SelectedValue != "" && ddlselectionApprovar.SelectedValue != "-1")
                //{
                //    if (ddlselectionApprovar.SelectedValue == "1")
                //    {
                //        if (drplistApprover.SelectedValue != "" && drplistApprover.SelectedValue != "-1")
                //        {
                //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //            {
                //                int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                //                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                //                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);
                //                int ApproverID = Convert.ToInt32(drplistApprover.SelectedValue);
                //                int customerID = -1;
                //                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                //                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                //                             where row.UserID == UserID
                //                             && row.ContractTemplateInstanceID == ContractTemplateID
                //                             && row.RoleID == RoleID
                //                             select row).ToList();

                //                var gettemplatesection = (from row in data1
                //                                          join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                //                                          on row.sectionID equals row1.SectionID
                //                                          where row1.ContractTemplateInstanceID == ContractTemplateID
                //                                          select new getTemplateTransactionDetail
                //                                          {
                //                                              TemplateTransactionID = row.TemplateTransactionID,
                //                                              TemplateInstanceID = row.ContractTemplateInstanceID,
                //                                              ComplianceStatusID = row.ComplianceStatusID,
                //                                              CreatedOn = row.CreatedOn,
                //                                              Status = row.Status,
                //                                              StatusChangedOn = row.StatusChangedOn,
                //                                              TemplateContent = row.ContractTemplateContent,
                //                                              RoleID = row.RoleID,
                //                                              UserID = row.UserID,
                //                                              sectionID = Convert.ToInt32(row.sectionID),
                //                                              orderID = Convert.ToInt32(row1.SectionOrder)
                //                                          }).OrderBy(x => x.orderID).ToList();

                //                if (gettemplatesection.Count > 0)
                //                {
                //                    foreach (var item in gettemplatesection)
                //                    {
                //                        if (item.ComplianceStatusID == 17)
                //                        {
                //                            ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                //                            {
                //                                ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                //                                RoleID = 6,
                //                                UserID = ApproverID,
                //                                IsActive = true,
                //                                SectionID = item.sectionID,
                //                                Visibility = 1
                //                            };
                //                            ContractTemplateManagement.CreateContractTemplateAssignment1(ContracttemplateAssignment);

                //                            string templateSectionContent = item.TemplateContent;

                //                            ContractTemplateTransaction obj1 = new ContractTemplateTransaction();
                //                            obj1.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                //                            obj1.CreatedOn = DateTime.Now;
                //                            obj1.CreatedBy = UserID;
                //                            obj1.ContractTemplateContent = templateSectionContent;
                //                            obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                //                            obj1.Status = 4;
                //                            obj1.OrderID = 1;
                //                            obj1.Version = ContractVersion;
                //                            obj1.SectionID = item.sectionID;
                //                            entities.ContractTemplateTransactions.Add(obj1);
                //                            entities.SaveChanges();
                //                        }
                //                    }

                //                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                //                    {
                //                        CustomerID = customerID,
                //                        ContractID = Convert.ToInt32(ContractTemplateID),
                //                        StatusID = 4,
                //                        StatusChangeOn = DateTime.Now,
                //                        IsActive = true,
                //                        CreatedBy = UserID,
                //                        UpdatedBy = UserID,
                //                    };
                //                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                //                }
                //            }
                //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedRefreshModel();", true);
                //        }
                //        else
                //        {
                //            if (drplistApprover.SelectedValue == "" || drplistApprover.SelectedValue == "-1")
                //            {
                //                lstErrorMsg.Add("Please Select Approver");
                //            }
                //            if (lstErrorMsg.Count > 0)
                //            {
                //                showErrorMessages(lstErrorMsg, CustomValidator1);
                //            }
                //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenApprovarSectionModal();", true);
                //        }
                //    }
                //    if (ddlselectionApprovar.SelectedValue == "2")
                //    {
                //        if (drplistApproverprimary.SelectedValue != "" && drplistApproverprimary.SelectedValue != "-1"
                //            && drplistApprover.SelectedValue != "" && drplistApprover.SelectedValue != "-1")
                //        {
                //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //            {
                //                int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                //                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                //                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);
                //                int ApproverID1 = Convert.ToInt32(drplistApproverprimary.SelectedValue);
                //                int FinalApproverID = Convert.ToInt32(drplistApprover.SelectedValue);
                //                int customerID = -1;
                //                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                //                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                //                             where row.UserID == UserID
                //                             && row.ContractTemplateInstanceID == ContractTemplateID
                //                             && row.RoleID == RoleID
                //                             select row).ToList();

                //                var gettemplatesection = (from row in data1
                //                                          join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                //                                          on row.sectionID equals row1.SectionID
                //                                          where row1.ContractTemplateInstanceID == ContractTemplateID
                //                                          select new getTemplateTransactionDetail
                //                                          {
                //                                              TemplateTransactionID = row.TemplateTransactionID,
                //                                              TemplateInstanceID = row.ContractTemplateInstanceID,
                //                                              ComplianceStatusID = row.ComplianceStatusID,
                //                                              CreatedOn = row.CreatedOn,
                //                                              Status = row.Status,
                //                                              StatusChangedOn = row.StatusChangedOn,
                //                                              TemplateContent = row.ContractTemplateContent,
                //                                              RoleID = row.RoleID,
                //                                              UserID = row.UserID,
                //                                              sectionID = Convert.ToInt32(row.sectionID),
                //                                              orderID = Convert.ToInt32(row1.SectionOrder)
                //                                          }).OrderBy(x => x.orderID).ToList();

                //                if (gettemplatesection.Count > 0)
                //                {
                //                    foreach (var item in gettemplatesection)
                //                    {
                //                        if (item.ComplianceStatusID == 17)
                //                        {
                //                            ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                //                            {
                //                                ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                //                                RoleID = 29,
                //                                UserID = ApproverID1,
                //                                IsActive = true,
                //                                SectionID = item.sectionID,
                //                                Visibility = 1
                //                            };
                //                            ContractTemplateManagement.CreateContractTemplateAssignment1(ContracttemplateAssignment);

                //                            ContractTemplateAssignment ContracttemplateAssignment1 = new ContractTemplateAssignment()
                //                            {
                //                                ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                //                                RoleID = 6,
                //                                UserID = FinalApproverID,
                //                                IsActive = true,
                //                                SectionID = item.sectionID,
                //                                Visibility = 1
                //                            };
                //                            ContractTemplateManagement.CreateContractTemplateAssignment1(ContracttemplateAssignment1);

                //                            string templateSectionContent = item.TemplateContent;

                //                            ContractTemplateTransaction obj1 = new ContractTemplateTransaction();
                //                            obj1.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                //                            obj1.CreatedOn = DateTime.Now;
                //                            obj1.CreatedBy = UserID;
                //                            obj1.ContractTemplateContent = templateSectionContent;
                //                            obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                //                            obj1.Status = 18;
                //                            obj1.OrderID = 1;
                //                            obj1.Version = ContractVersion;
                //                            obj1.SectionID = item.sectionID;
                //                            entities.ContractTemplateTransactions.Add(obj1);
                //                            entities.SaveChanges();
                //                        }
                //                    }

                //                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                //                    {
                //                        CustomerID = customerID,
                //                        ContractID = Convert.ToInt32(ContractTemplateID),
                //                        StatusID = 18,
                //                        StatusChangeOn = DateTime.Now,
                //                        IsActive = true,
                //                        CreatedBy = UserID,
                //                        UpdatedBy = UserID,
                //                    };
                //                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                //                }
                //            }
                //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedRefreshModel();", true);
                //        }
                //        else
                //        {
                //            if (drplistApproverprimary.SelectedValue == "" || drplistApproverprimary.SelectedValue == "-1")
                //            {
                //                lstErrorMsg.Add("Please Select Primary Approver");
                //            }
                //            if (drplistApprover.SelectedValue == "" || drplistApprover.SelectedValue == "-1")
                //            {
                //                lstErrorMsg.Add("Please Select Secondary Approver");
                //            }
                //            if (lstErrorMsg.Count > 0)
                //            {
                //                showErrorMessages(lstErrorMsg, CustomValidator1);
                //            }
                //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenApprovarSectionModal();", true);
                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnconvertcontract_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userID = Convert.ToInt32(Request.QueryString["UID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                if (ContractTemplateID != 0)
                {
                    int docTypeID = getDocTypeID(customerID);
                    bool shreee = ExporttoWordNew(userID, customerID, ContractTemplateID, docTypeID);
                    bool result = ExportPdf(userID, customerID, ContractTemplateID, docTypeID);
                    if (result)
                    {
                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                        {
                            CustomerID = customerID,
                            ContractID = Convert.ToInt32(ContractTemplateID),
                            StatusID = 7,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = userID,
                            UpdatedBy = userID,
                        };
                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedRefreshModel();", true);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public bool ExporttoWordNew(int userID, int CustomerID, int ContractTemplateID,int docTypeID)
        {
            bool resultdata = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == userID
                                 && row.ContractTemplateInstanceID == ContractTemplateID
                                 && row.RoleID == 3
                                 select row).ToList();

                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == ContractTemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();
                        
                        string fileName = string.Empty;
                        string templateName = string.Empty;

                        var getContractDetails = (from row in entities.Cont_tbl_ContractInstance
                                                  where row.ID == ContractTemplateID
                                                  select row).FirstOrDefault();

                        if (getContractDetails != null)
                        {
                            fileName = getContractDetails.ContractTitle;
                            templateName = getContractDetails.ContractTitle;
                        }

                        strExporttoWord.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header: h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }

                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                .break { page-break-before: always; }
                -->
                </style></head>");

                        strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr>
                    <td>
                        <div style='mso-element:header' id=h1 >
                            <p class=MsoHeader style='text-align:left'>
                        </div>
                    </td>
                    <td>
                        <div style='mso-element:footer' id=f1>
                            <p class=MsoFooter>
                            <span style=mso-tab-count:2'></span>
                            <span style='mso-field-code:"" PAGE ""'></span>
                            of <span style='mso-field-code:"" NUMPAGES ""'></span>
                            </p>
                        </div>
                    </td>
                </tr>
                </table>
                </body>
                </html>");

                        fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                        //Response.AppendHeader("Content-Type", "application/msword");
                        //Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
                        //Response.Write(strExporttoWord);

                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);                    
                        string reportName = string.Empty;
                        reportName = fileName + ".doc";
                        Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                        {
                            ContractID = ContractTemplateID,
                            DocTypeID = docTypeID,
                            TaskID = null,
                            TaskResponseID = null,
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            IsDeleted = false,
                            FileName = reportName
                    };
                        var contractDocVersion = ContractDocumentManagement.ExistsContractDocumentReturnVersion(objContDoc);

                        contractDocVersion++;
                        objContDoc.Version = contractDocVersion + ".0";
                        
                        string p_strPath = string.Empty;

                        string directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(ContractTemplateID) + "/" + docTypeID + "/" + objContDoc.Version);
                        p_strPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(ContractTemplateID) + "/" + docTypeID + "/" + objContDoc.Version + "/" + reportName);

                        if (File.Exists(p_strPath))
                            File.Delete(p_strPath);
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(reportName));
                        byte[] buffer = Encoding.ASCII.GetBytes(strExporttoWord.ToString());
                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, buffer));
                        DocumentManagement.Contract_SaveDocFiles(Filelist);
                                               
                        objContDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        objContDoc.FileKey = fileKey.ToString();
                        objContDoc.VersionDate = DateTime.Now;
                        objContDoc.CreatedOn = DateTime.Now;
                        objContDoc.FileSize = buffer.ToArray().Length;
                        long newFileID = ContractDocumentManagement.CreateContractDocumentMapping(objContDoc);
                        resultdata = true;
                    }

                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return resultdata;
        }


        public bool ExportPdf(int userID, int CustomerID, int ContractTemplateID,int docTypeID)
        {
            bool resultdata = false;

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string fileName = string.Empty;
                    string templateName = string.Empty;

                    var getContractDetails = (from row in entities.Cont_tbl_ContractInstance
                                              where row.ID == ContractTemplateID
                                              select row).FirstOrDefault();

                    if (getContractDetails != null)
                    {
                        fileName = getContractDetails.ContractTitle;
                        templateName = getContractDetails.ContractTitle;
                    }

                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                   
                    string reportName = string.Empty;
                    reportName = fileName + ".pdf";
                    Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                    {
                        ContractID = ContractTemplateID,
                        DocTypeID = docTypeID,
                        TaskID = null,
                        TaskResponseID = null,
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                        FileName = reportName
                };
                    var contractDocVersion = ContractDocumentManagement.ExistsContractDocumentReturnVersion(objContDoc);

                    contractDocVersion++;
                    objContDoc.Version = contractDocVersion + ".0";

                    using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                    {
                        Document document = new Document(PageSize.A3, 5, 5, 5, 5);

                        PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                        document.Open();

                        string text = theEditor.Text;
                        iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph();
                        paragraph.SpacingBefore = 15;
                        paragraph.SpacingAfter = 15;
                        paragraph.Add(text);
                        document.Add(paragraph);
                        document.Close();
                        
                        string p_strPath = string.Empty;

                        string directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(ContractTemplateID) + "/" + docTypeID + "/" + objContDoc.Version);
                        p_strPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(ContractTemplateID) + "/" + docTypeID + "/" + objContDoc.Version + "/" + reportName);

                        if (File.Exists(p_strPath))
                            File.Delete(p_strPath);
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(reportName));
                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, memoryStream.ToArray()));
                        DocumentManagement.Contract_SaveDocFiles(Filelist);

                    
                        objContDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        objContDoc.FileKey = fileKey.ToString();
                        objContDoc.VersionDate = DateTime.Now;
                        objContDoc.CreatedOn = DateTime.Now;
                        objContDoc.FileSize = memoryStream.ToArray().Length;
                        long newFileID = ContractDocumentManagement.CreateContractDocumentMapping(objContDoc);
                        resultdata = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return resultdata;
        }

        protected void ddlselectionApprovar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlselectionApprovar.SelectedValue == "1")
            {
                lblapprover.Text = "Approver";
                divforsecondaryapproval.Visible = true;
                divforprimaryapproval.Visible = false;
            }
            else
            {
                lblapprover.Text = "Secondary Approver";
                divforsecondaryapproval.Visible = true;
                divforprimaryapproval.Visible = true;
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenApprovarSectionModal();", true);
        }

        protected void btnrenew_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = customerID,
                        ContractID = Convert.ToInt32(ContractTemplateID),
                        StatusID = 1,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                    string ContractVersion1 = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                    if (!string.IsNullOrEmpty(ContractVersion1))
                    {
                        string[] commandArgs = ContractVersion1.ToString().Split(new char[] { '.' });
                        int version = Convert.ToInt32(commandArgs[0]);
                        version++;
                        string Version = version + ".0";

                        Cont_tbl_ContractInstance objRecord = (from row in entities.Cont_tbl_ContractInstance
                                                               where row.ID == ContractTemplateID
                                                               && row.CustomerID == customerID
                                                               && row.IsDeleted == false
                                                               select row).FirstOrDefault();
                        if (objRecord != null)
                        {
                            objRecord.Version = Version;
                            entities.SaveChanges();
                        }
                    }
                    string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == UserID
                                 && row.ContractTemplateInstanceID == ContractTemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();

                    var gettemplatesection = (from row in data1
                                              join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                              on row.sectionID equals row1.SectionID
                                              where row1.ContractTemplateInstanceID == ContractTemplateID
                                              select new getTemplateTransactionDetail
                                              {
                                                  TemplateTransactionID = row.TemplateTransactionID,
                                                  TemplateInstanceID = row.ContractTemplateInstanceID,
                                                  ComplianceStatusID = row.ComplianceStatusID,
                                                  CreatedOn = row.CreatedOn,
                                                  Status = row.Status,
                                                  StatusChangedOn = row.StatusChangedOn,
                                                  TemplateContent = row.ContractTemplateContent,
                                                  RoleID = row.RoleID,
                                                  UserID = row.UserID,
                                                  sectionID = Convert.ToInt32(row.sectionID),
                                                  orderID = Convert.ToInt32(row1.SectionOrder)
                                              }).OrderBy(x => x.orderID).ToList();

                    if (gettemplatesection.Count > 0)
                    {
                        foreach (var item in gettemplatesection)
                        {
                            if (item.ComplianceStatusID == 5)
                            {
                                string templateSectionContent = item.TemplateContent;

                                ContractTemplateTransaction obj1 = new ContractTemplateTransaction();
                                obj1.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                                obj1.CreatedOn = DateTime.Now;
                                obj1.CreatedBy = UserID;
                                obj1.ContractTemplateContent = templateSectionContent;
                                obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                obj1.Status = 1;
                                obj1.OrderID = 1;
                                obj1.Version = ContractVersion;
                                obj1.SectionID = item.sectionID;
                                entities.ContractTemplateTransactions.Add(obj1);
                                entities.SaveChanges();
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedRefreshModel();", true);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceApproverView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTime"]);
                
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                DropDownList ddlApprover = (DropDownList)e.Item.FindControl("drplistdynamicApprover");

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userID = -1;
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<object> internalUsers = new List<object>();

                    string CacheName = "internalUsersforContract" + userID + "_" + customerID;
                    internalUsers = (List<object>)HttpContext.Current.Cache[CacheName];
                    if (internalUsers == null)
                    {
                        var lstAllUsers1 = ContractUserManagement.GetAllUsers_Contract(customerID);
                        if (lstAllUsers1.Count > 0)
                            lstAllUsers1 = lstAllUsers1.Where(entry => entry.ContractRoleID != null).ToList();

                        internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers1, 1); //1--Internal---

                        HttpContext.Current.Cache.Insert(CacheName, internalUsers, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                    }

                    ddlApprover.DataValueField = "ID";
                    ddlApprover.DataTextField = "Name";
                    ddlApprover.DataSource = internalUsers;
                    ddlApprover.DataBind();
                    ddlApprover.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Approver", "-1"));
                }
            }
        }

        protected void btnExportDocReport_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userID = Convert.ToInt32(Request.QueryString["UID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                if (ContractTemplateID != 0)
                {
                    bool shreee = ExporttoWordDoc(userID, customerID, ContractTemplateID);                    
                }
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedRefreshModel();", true);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public bool ExporttoWordDoc(int userID, int CustomerID, int ContractTemplateID)
        {
            bool resultdata = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == userID
                                 && row.ContractTemplateInstanceID == ContractTemplateID
                                 && row.RoleID == 3
                                 select row).ToList();

                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == ContractTemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        string fileName = string.Empty;
                        string templateName = string.Empty;

                        var getContractDetails = (from row in entities.Cont_tbl_ContractInstance
                                                  where row.ID == ContractTemplateID
                                                  select row).FirstOrDefault();

                        if (getContractDetails != null)
                        {
                            fileName = getContractDetails.ContractTitle;
                            templateName = getContractDetails.ContractTitle;
                        }

                        strExporttoWord.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header: h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }

                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                .break { page-break-before: always; }
                -->
                </style></head>");

                        strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr>
                    <td>
                        <div style='mso-element:header' id=h1 >
                            <p class=MsoHeader style='text-align:left'>
                        </div>
                    </td>
                    <td>
                        <div style='mso-element:footer' id=f1>
                            <p class=MsoFooter>
                            <span style=mso-tab-count:2'></span>
                            <span style='mso-field-code:"" PAGE ""'></span>
                            of <span style='mso-field-code:"" NUMPAGES ""'></span>
                            </p>
                        </div>
                    </td>
                </tr>
                </table>
                </body>
                </html>");

                        fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                        Response.AppendHeader("Content-Type", "application/msword");
                        Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
                        Response.Write(strExporttoWord);
                    }
                }                
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return resultdata;
        }
    }
}