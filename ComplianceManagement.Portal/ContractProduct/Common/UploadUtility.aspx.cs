﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class UploadUtility : System.Web.UI.Page
    {
        //private static int CustomerID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Common Methods


        public bool checkDuplicateDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, int colNum, string providedText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            TextToCompare = xlWorksheet.Cells[i, colNum].Text.ToString().Trim();
                            if (String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                            {
                                matchSuccess = true;
                                i = lastRow; //exit from for Loop
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }

        public bool checkDuplicateMultipleDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, int colNum, string providedText, string MultipleProviderText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            string TypeTextToCompare = xlWorksheet.Cells[i, 1].Text.ToString().Trim();

                            if (MultipleProviderText.Equals(TypeTextToCompare))
                            {
                                TextToCompare = xlWorksheet.Cells[i, colNum].Text.ToString().Trim();
                                if (String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                                {
                                    matchSuccess = true;
                                    i = lastRow; //exit from for Loop
                                }
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }

        public void ErrorMessages(List<string> emsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        #endregion

        protected void rdoBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoBtn_Contract.Checked || rdoBtn_Vendor.Checked || rdoBtn_ContractType.Checked || rdoBtn_ContractSubType.Checked || rdoBtn_DocType.Checked)
            {
                MasterFileUpload.Visible = true;
                btnUploadFile.Visible = true;
            }
            else
            {
                MasterFileUpload.Visible = false;
                btnUploadFile.Visible = false;
            }
        }

        protected void lnkSampleFormat_Click(object sender, EventArgs e)
        {
            try
            {
               bool RenameNoticeTerm = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "RenameNoticeTerm");
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                string filePath = Server.MapPath("~/ContractProduct/SampleFormat/Contract_Upload_Format.xlsx");
                string filePath1 = Server.MapPath("~/ContractProduct/SampleFormat/Contract_Upload_Format_new.xlsx");

                if(RenameNoticeTerm==true)
                {
                    if (!string.IsNullOrEmpty(filePath1))
                    {
                        response.Clear();
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Buffer = true;
                        response.AddHeader("Content-Disposition", "attachment;filename=Contract_Upload_Format_new.xlsx");
                        byte[] data = req.DownloadData(filePath1);
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        response.BinaryWrite(data);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Flush();
                        Response.End();
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "No Format Available for Download, Please Contact Admin for more detail";
                    }
                }
                else if(RenameNoticeTerm != true)
                {
                  if (!string.IsNullOrEmpty(filePath))
                  {
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.AddHeader("Content-Disposition", "attachment;filename=Contract_Upload_Format.xlsx");
                    byte[] data = req.DownloadData(filePath);
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    response.BinaryWrite(data);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "No Format Available for Download, Please Contact Admin for more detail";
                }
              }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/ContractProduct/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/ContractProduct/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoBtn_Contract.Checked)
                            {
                                bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "Contract");
                                if (matchSuccess)
                                {
                                    ProcessContractData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No data found in Excel Document or sheet name must be 'Contract";
                                }
                            }
                            else if (rdoBtn_Vendor.Checked)
                            {
                                bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "Vendor");
                                if (matchSuccess)
                                {
                                    ProcessVendorData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No data found in Excel Document or sheet name must be 'Vendor'.";
                                }
                            }
                            else if (rdoBtn_ContractType.Checked)
                            {
                                bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "Contract Type");
                                if (matchSuccess)
                                {
                                    ProcessContractTypeData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No data found in Excel Document or sheet name must be 'Contract Type'.";
                                }
                            }
                            else if (rdoBtn_ContractSubType.Checked)
                            {
                                bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "Contract Sub-Type");
                                if (matchSuccess)
                                {
                                    ProcessContractSubTypeData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No data found in Excel Document or sheet name must be 'Contract Sub-Type'.";
                                }
                            }
                            else if (rdoBtn_DocType.Checked)
                            {
                                bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "Document Type");
                                if (matchSuccess)
                                {
                                    ProcessDocumentTypeData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No data found in Excel Document or sheet name must be 'Document Type'.";
                                }
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }

        private void ProcessContractData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;
                int uploadedContractCount = 0;
                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);                

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Contract"];

                if (xlWorksheet != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        long newContractID = 0;

                        int xlrow2 = xlWorksheet.Dimension.End.Row;

                        List<long> lstVendorIDs = new List<long>();
                        List<int> lstContractOwnerIDs = new List<int>();
                        List<string> errorMessage = new List<string>();

                        string ContractNumber = string.Empty;
                        string ContractTitle = string.Empty;
                        string ContractDescription = string.Empty;
                        string ContractLocation = string.Empty;
                        string ContractVendor = string.Empty;
                        string ContractDepartment = string.Empty;
                        string EffectiveDate = string.Empty;
                        string ExpirationDate = string.Empty;
                        string ContractType = string.Empty;
                        string ContractSubType = string.Empty;
                        string ContractOwner = string.Empty;
                        string ProposalDate = string.Empty;
                        string LockINDate = string.Empty;
                        string AgreementDate = string.Empty;
                        string ReviewDate = string.Empty;
                        string ContractAmount = string.Empty;
                        string ContractTax = string.Empty;
                        string NoticeTerm = string.Empty;
                        string NoticeTermType = string.Empty;
                        string PaymentTerm = string.Empty;

                        long ContractTypeID = -1;
                        long ContractSubTypeID = -1;
                        long vendorID = -1;
                        int DeptID = -1;
                        int OwnerID = -1;
                        int Entity_Location_BranchID = -1;

                        //Fetch List from database of Master Data
                        var masterlstExistingContracts = entities.Cont_tbl_ContractInstance.Where(row => row.CustomerID == CustomerID && row.IsDeleted==false).ToList();
                        var masterlstCustomerBranches = entities.CustomerBranches.Where(row => row.CustomerID == CustomerID).ToList();
                        var masterlstUsers = entities.Users.Where(row => row.CustomerID == CustomerID).ToList();
                        //var masterlstVendors = entities.Cont_tbl_VendorMaster.Where(row => row.CustomerID == CustomerID).ToList();

                        var masterlstVendors = VendorDetails.GetVendorDetails(CustomerID);
                        var masterlstDepts = entities.Departments.Where(row => row.CustomerID == CustomerID).ToList();
                        var masterlstContactTypes = entities.Cont_tbl_TypeMaster.Where(row => row.CustomerID == CustomerID).ToList();
                        var masterlstContactSubTypes = entities.Cont_tbl_SubTypeMaster.Where(row => row.CustomerID == CustomerID).ToList();

                        #region Validations

                        for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                        {
                            ContractNumber = string.Empty;
                            ContractTitle = string.Empty;
                            ContractDescription = string.Empty;
                            ContractLocation = string.Empty;
                            ContractVendor = string.Empty;
                            ContractDepartment = string.Empty;
                            EffectiveDate = string.Empty;
                            ExpirationDate = string.Empty;
                            ContractType = string.Empty;
                            ContractSubType = string.Empty;
                            ContractOwner = string.Empty;
                            ProposalDate = string.Empty;
                            LockINDate = string.Empty;
                            AgreementDate = string.Empty;
                            ReviewDate = string.Empty;
                            ContractAmount = string.Empty;
                            ContractTax = string.Empty;
                            ContractTypeID = -1;
                            DeptID = -1;
                            ContractSubTypeID = -1;
                            vendorID = -1;
                            OwnerID = -1;

                            NoticeTerm = string.Empty;
                            NoticeTermType = string.Empty;
                            PaymentTerm = string.Empty;
                            Entity_Location_BranchID = -1;

                            if (lstVendorIDs.Count > 0)
                                lstVendorIDs.Clear();

                            if (lstContractOwnerIDs.Count > 0)
                                lstContractOwnerIDs.Clear();

                            #region 1 Contract Number

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                            {
                                ContractNumber = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();

                                bool matchSuccess = false;
                                matchSuccess = ContractCommonMethods.checkDuplicateDataExistExcelSheet(xlWorksheet, rowNum, 1, ContractNumber);

                                if (matchSuccess)
                                {
                                    errorMessage.Add("Contract with Same Contract Number at Row - " + rowNum + " Exists Multiple Times in the Uploaded Excel Document");
                                }
                            }

                            if (String.IsNullOrEmpty(ContractNumber))
                            {
                                errorMessage.Add("Required Contract Number at row number-" + rowNum);
                            }
                            else
                            {
                                bool existContractNumber = ContractManagement.ExistsContractNo(masterlstExistingContracts, CustomerID, ContractNumber, 0);
                                if (existContractNumber)
                                {
                                    errorMessage.Add("Contract with Same Contract Number already exists in System, Check at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 2 Contract Title

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                            {
                                ContractTitle = Convert.ToString(xlWorksheet.Cells[rowNum, 2].Text).Trim();

                              //  bool matchSuccess = false;
                             //   matchSuccess = ContractCommonMethods.checkDuplicateDataExistExcelSheet(xlWorksheet, rowNum, 2, ContractTitle);

                                //if (matchSuccess)
                                //{
                                //    errorMessage.Add("Contract with Same Contract Title at Row - " + rowNum + " Exists Multiple Times in the Uploaded Excel Document");
                                //}
                            }

                            if (String.IsNullOrEmpty(ContractTitle))
                            {
                                errorMessage.Add("Required Contract Title at row number-" + rowNum);
                            }
                            else
                            {
                                //bool ExistContractTitle = ContractManagement.ExistsContractTitle(masterlstExistingContracts, CustomerID, ContractTitle, 0);
                                //if (ExistContractTitle)
                                //{
                                //    errorMessage.Add("Contract with Same Contract Title already exists in System, Check at row number-" + rowNum);
                                //}
                            }
                            #endregion

                            #region 3 Contract Description
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                            {
                                ContractDescription = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                            }
                            else
                            {
                                errorMessage.Add("Required Contract Description at row number-" + rowNum);
                            }
                            #endregion

                            #region 4 Contract Location
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                            {
                                ContractLocation = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim();
                                Entity_Location_BranchID = ContractMastersManagement.GetBranchIDByName(masterlstCustomerBranches, CustomerID, ContractLocation);
                                if (Entity_Location_BranchID == 0 || Entity_Location_BranchID == -1)
                                {
                                    errorMessage.Add("Please Correct the Location or Location not defined in the Legal Entity Master, Correct at row number- " + rowNum);
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required Location associated with Contract, Correct at row number-" + rowNum);
                            }
                            #endregion

                            #region 5 Contract Vendor
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                            {
                                ContractVendor = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                string[] split = ContractVendor.Split(',');
                                if (split.Length > 0)
                                {
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        vendorID = VendorDetails.GetVendorIDByName(masterlstVendors, split[rs].ToString().Trim(), CustomerID);
                                        //if (existIDVendor == 0 && existIDVendor == -1)
                                        if (vendorID <= 0)
                                        {
                                            errorMessage.Add("Please Correct the Vendor Name or Vendor (" + split[rs].ToString().Trim() + ") not defined in the Vendor Master, Correct at row number- " + rowNum);
                                        }
                                        else
                                        {
                                            lstVendorIDs.Add(vendorID);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required Contract Vendor at row number-" + rowNum);
                            }
                            #endregion

                            #region 6 Contract Department
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                            {
                                ContractDepartment = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                                DeptID = CompDeptManagement.GetDepartmentIDByName(masterlstDepts, ContractDepartment, CustomerID);
                                if (DeptID == 0 || DeptID == -1)
                                {
                                    errorMessage.Add("Please Correct the Department or Department (" + ContractDepartment + ") not Defined in the Masters, Correct at row number-" + rowNum);
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required Department Name at row number-" + rowNum);
                            }
                            #endregion

                            #region 7 Effective Date
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString().Trim()))
                            {
                                EffectiveDate = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text).Trim();
                                bool checkDate = ContractCommonMethods.CheckDate(EffectiveDate);
                                if (!checkDate)
                                {
                                    errorMessage.Add("Please Check the Start Date or Start Date can not be empty, Correct at row number-" + rowNum);
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required Start Date at row number-" + rowNum);
                            }
                            #endregion

                            #region 8 End Date
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 11].Text.ToString().Trim()))
                            {
                                ExpirationDate = Convert.ToString(xlWorksheet.Cells[rowNum, 11].Text).Trim();
                                bool checkDate = ContractCommonMethods.CheckDate(ExpirationDate);
                                if (!checkDate)
                                {
                                    errorMessage.Add("Please Check the End Date or End Date can not be empty at row number-" + rowNum);
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required End Date at row number-" + rowNum);
                            }
                            #endregion

                            #region 9 Contract Type and Sub-Type
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 14].Text.ToString().Trim()))
                            {
                                ContractType = Convert.ToString(xlWorksheet.Cells[rowNum, 14].Text).Trim();
                                ContractTypeID = Convert.ToInt32(ContractTypeMasterManagement.GetContractTypeID(masterlstContactTypes, ContractType, CustomerID));
                                if (ContractTypeID == 0 || ContractTypeID == -1)
                                {
                                    errorMessage.Add("Please Correct the Contract Type or Contract Type (" + ContractType + ") not defined in the Contract Type Maters, Correct at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 15].Text.ToString().Trim()))
                                    {
                                        ContractSubType = Convert.ToString(xlWorksheet.Cells[rowNum, 15].Text).Trim();
                                        if (ContractTypeID != 0 && ContractTypeID != -1)
                                        {
                                            ContractSubTypeID = ContractTypeMasterManagement.GetContractSubTypeIDByName(masterlstContactSubTypes, CustomerID, ContractTypeID, ContractSubType);

                                            if (ContractSubTypeID == 0 || ContractSubTypeID == -1)
                                            {
                                                errorMessage.Add("Please Correct the Contract Sub-Type or Contract Sub Type(" + ContractSubType + ") not defined in the Contract Sub-Type Maters for Contract Type(" + ContractType + "), Correct at row number - " + rowNum);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required Contract Type at row number-" + rowNum);
                            }
                            #endregion

                            #region 10 Contract Owner
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 16].Text.ToString().Trim()))
                            {
                                ContractOwner = Convert.ToString(xlWorksheet.Cells[rowNum, 16].Text).Trim();

                                string[] split = ContractOwner.Split(',');
                                if (split.Length > 0)
                                {
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        OwnerID = ContractUserManagement.GetContractOwnerUserIDByEmail(masterlstUsers, CustomerID, split[rs].ToString().Trim());
                                        if (OwnerID == 0 || OwnerID == -1)
                                        {
                                            errorMessage.Add("Please Correct the Owner(Email) or Owner (" + split[rs].ToString().Trim() + ") not defined in the User Master, Correct at row number-" + rowNum);
                                        }
                                        else
                                        {
                                            lstContractOwnerIDs.Add(OwnerID);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required Contract Owner(Email) at row number-" + rowNum);
                            }
                            #endregion

                            #region 11 Proposal Date
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                            {
                                ProposalDate = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text).Trim();
                                bool checkDate = ContractCommonMethods.CheckDate(ProposalDate);
                                if (!checkDate)
                                {
                                    errorMessage.Add("Invalid Proposal Date or Please provide Proposal Date in Correct format, Check at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 12 Agreement Date
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                            {
                                AgreementDate = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                                bool checkDate = ContractCommonMethods.CheckDate(AgreementDate);
                                if (!checkDate)
                                {
                                    errorMessage.Add("Invalid Agreement Date or Please provide Agreement Date in Correct format, Check at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 13 Review Date
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 10].Text.ToString().Trim()))
                            {
                                ReviewDate = Convert.ToString(xlWorksheet.Cells[rowNum, 10].Text).Trim();
                                bool checkDate = ContractCommonMethods.CheckDate(ReviewDate);
                                if (!checkDate)
                                {
                                    errorMessage.Add("Invalid Review Date or Please provide Review Date in Correct format, Check at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 14 Contract Amount
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 17].Text.ToString().Trim()))
                            {
                                ContractAmount = Convert.ToString(xlWorksheet.Cells[rowNum, 17].Text).Trim();
                                bool checkNumber = ContractCommonMethods.IsValidNumber(ContractAmount);
                                if (!checkNumber)
                                {
                                    errorMessage.Add("Please Enter only numbers in Contract Amount, Correct at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 14 Notice Term
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 12].Text.ToString().Trim()))
                            {
                                NoticeTerm = Convert.ToString(xlWorksheet.Cells[rowNum, 12].Text).Trim();
                                bool checkNumber = ContractCommonMethods.IsValidNumber(NoticeTerm);
                                if (!checkNumber)
                                {
                                    errorMessage.Add("Please Enter only Numbers in Notice Term at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 13].Text.ToString().Trim()))
                                    {
                                        NoticeTermType = Convert.ToString(xlWorksheet.Cells[rowNum, 13].Text).Trim();
                                        if (NoticeTermType.Equals("Days") || NoticeTermType.Equals("Weeks") || NoticeTermType.Equals("Months") || NoticeTermType.Equals("Years"))
                                        {

                                        }
                                        else
                                        {
                                            errorMessage.Add("Please Correct Notice Term Type, Only Values(Days, Weeks, Months, Years) allowed, check at row number-" + rowNum);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //Notice Term Empty 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 13].Text.ToString().Trim()))
                                {
                                    errorMessage.Add("Please provide Notice Term Value, Correct at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 14 Payment Term
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 18].Text.ToString().Trim()))
                            {
                                PaymentTerm = Convert.ToString(xlWorksheet.Cells[rowNum, 18].Text).Trim();
                                string[] split = PaymentTerm.Split(',');
                                bool flag = false;
                                if (split.Length > 0)
                                {
                                    foreach (var item in split)
                                    {
                                        if (!(string.IsNullOrEmpty(item)))
                                        {
                                            if (item.Trim().Equals("One-Time") || item.Trim().Equals("Daily") ||
                                            item.Trim().Equals("Weekly") || item.Trim().Equals("Periodically") || item.Trim().Equals("Monthly") ||
                                            item.Trim().Equals("Quarterly") || item.Trim().Equals("Half-Yearly") || item.Trim().Equals("Yearly"))
                                            {

                                            }
                                            else
                                            {
                                                flag = true;
                                            }
                                        }
                                        else
                                            flag = true;
                                    }
                                }
                                else
                                {
                                    flag = true;
                                }
                                if (flag == true)
                                {
                                    errorMessage.Add("Please Correct Payment Term, Only Values(One-Time, Daily, Weekly, Periodically, Quarterly, Half-Yearly, Yearly) allowed, Correct at row number-" + rowNum);
                                }
                            }
                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 18].Text.ToString().Trim()))
                            //{
                            //    PaymentTerm = Convert.ToString(xlWorksheet.Cells[rowNum, 18].Text).Trim();

                            //    if (PaymentTerm.Equals("One-Time") || PaymentTerm.Equals("Daily") ||
                            //        PaymentTerm.Equals("Weekly") || PaymentTerm.Equals("Periodically") || PaymentTerm.Equals("Monthly") ||
                            //        PaymentTerm.Equals("Quarterly") || PaymentTerm.Equals("Half-Yearly") || PaymentTerm.Equals("Yearly"))
                            //    {

                            //    }
                            //    else
                            //    {
                            //        errorMessage.Add("Please Correct Payment Term, Only Values(One-Time, Daily, Weekly, Periodically, Quarterly, Half-Yearly, Yearly) allowed, Correct at row number-" + rowNum);
                            //    }
                            //}
                            #endregion

                            #region 15 Lock-in Period Date
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 19].Text.ToString().Trim()))
                            {
                                LockINDate = Convert.ToString(xlWorksheet.Cells[rowNum, 19].Text).Trim();
                                bool checkDate = ContractCommonMethods.CheckDate(LockINDate);
                                if (!checkDate)
                                {
                                    errorMessage.Add("Invalid Lock In Period Date or Please provide Lock In Period Date in Correct format, Check at row number-" + rowNum);
                                }
                            }
                            #endregion
                            
                            #region 16 Contract Tax
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 20].Text.ToString().Trim()))
                            {
                                ContractTax = Convert.ToString(xlWorksheet.Cells[rowNum, 20].Text).Trim();
                                bool checkNumber = ContractCommonMethods.IsValidNumber(ContractTax);
                                if (!checkNumber)
                                {
                                    errorMessage.Add("Please Enter only numbers in Contract Taxes, Correct at row number-" + rowNum);
                                }
                            }
                            #endregion
                        }

                        #endregion

                        string ContractTypeNameFlag = string.Empty;

                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                        else
                        {
                            #region Save 

                            List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();
                            List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();

                            for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                            {
                                ContractNumber = string.Empty;
                                ContractTitle = string.Empty;
                                ContractDescription = string.Empty;
                                ContractLocation = string.Empty;
                                ContractVendor = string.Empty;
                                ContractDepartment = string.Empty;
                                EffectiveDate = string.Empty;
                                ExpirationDate = string.Empty;
                                ContractType = string.Empty;
                                ContractSubType = string.Empty;
                                ContractOwner = string.Empty;
                                ProposalDate = string.Empty;
                                LockINDate = string.Empty;
                                AgreementDate = string.Empty;
                                ReviewDate = string.Empty;
                                ContractAmount = string.Empty;
                                ContractTax = string.Empty;
                                ContractTypeID = -1;
                                DeptID = -1;
                                ContractSubTypeID = -1;
                                vendorID = -1;
                                OwnerID = -1;

                                NoticeTerm = string.Empty;
                                NoticeTermType = string.Empty;
                                PaymentTerm = string.Empty;
                                Entity_Location_BranchID = -1;

                                if (lstVendorIDs.Count > 0)
                                    lstVendorIDs.Clear();

                                if (lstContractOwnerIDs.Count > 0)
                                    lstContractOwnerIDs.Clear();

                                #region 1 Contract Number
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                                {
                                    ContractNumber = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();
                                }
                                #endregion

                                #region 2 Contract Title
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                                {
                                    ContractTitle = Convert.ToString(xlWorksheet.Cells[rowNum, 2].Text).Trim();
                                }
                                #endregion

                                #region 3 Contract Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                                {
                                    ContractDescription = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                                }
                                #endregion

                                #region 4 Contract Location
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                                {
                                    ContractLocation = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim();
                                    Entity_Location_BranchID = ContractMastersManagement.GetBranchIDByName(masterlstCustomerBranches, CustomerID, ContractLocation);
                                }
                                #endregion

                                #region 5 Contract Vendor
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                {
                                    ContractVendor = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                    string[] split = ContractVendor.Split(',');
                                    if (split.Length > 0)
                                    {
                                        for (int rs = 0; rs < split.Length; rs++)
                                        {
                                            vendorID = VendorDetails.GetVendorIDByName(masterlstVendors, split[rs].ToString().Trim(), CustomerID);

                                            if (vendorID > 0)
                                            {
                                                lstVendorIDs.Add(vendorID);
                                            }
                                        }
                                    }
                                }

                                #endregion

                                #region 6 Contract Department
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                                {
                                    ContractDepartment = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                                    DeptID = CompDeptManagement.GetDepartmentIDByName(masterlstDepts, ContractDepartment, CustomerID);
                                }
                                #endregion

                                #region 7 Proposal Date
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                                {
                                    ProposalDate = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text).Trim();
                                }
                                #endregion

                                #region 8 Agreement Date
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                {
                                    AgreementDate = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                                }
                                #endregion

                                #region 9 Effective Date
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString().Trim()))
                                {
                                    EffectiveDate = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text).Trim();
                                }
                                #endregion

                                #region 10 Review Date
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 10].Text.ToString().Trim()))
                                {
                                    ReviewDate = Convert.ToString(xlWorksheet.Cells[rowNum, 10].Text).Trim();
                                }
                                #endregion

                                #region 11 Expiration Date
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 11].Text.ToString().Trim()))
                                {
                                    ExpirationDate = Convert.ToString(xlWorksheet.Cells[rowNum, 11].Text).Trim();
                                }
                                #endregion

                                #region 12 Notice Term
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 12].Text.ToString().Trim()))
                                {
                                    NoticeTerm = Convert.ToString(xlWorksheet.Cells[rowNum, 12].Text).Trim();
                                }
                                #endregion

                                #region 13 Notice Term Type
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 13].Text.ToString().Trim()))
                                {
                                    NoticeTermType = Convert.ToString(xlWorksheet.Cells[rowNum, 13].Text).Trim();
                                }
                                #endregion

                                #region 14 Contract Type 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 14].Text.ToString().Trim()))
                                {
                                    ContractType = Convert.ToString(xlWorksheet.Cells[rowNum, 14].Text).Trim();
                                    ContractTypeID = Convert.ToInt32(ContractTypeMasterManagement.GetContractTypeID(masterlstContactTypes, ContractType, CustomerID));
                                }
                                #endregion

                                #region 15 Contract Sub-Type
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 15].Text.ToString().Trim()))
                                {
                                    ContractSubType = Convert.ToString(xlWorksheet.Cells[rowNum, 15].Text).Trim();
                                    if (ContractTypeID != 0 && ContractTypeID != -1)
                                    {
                                        ContractSubTypeID = ContractTypeMasterManagement.GetContractSubTypeIDByName(masterlstContactSubTypes, CustomerID, ContractTypeID, ContractSubType);
                                    }
                                }
                                #endregion

                                #region 16 Contract Owner
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 16].Text.ToString().Trim()))
                                {
                                    ContractOwner = Convert.ToString(xlWorksheet.Cells[rowNum, 16].Text).Trim();

                                    string[] split = ContractOwner.Split(',');
                                    if (split.Length > 0)
                                    {
                                        for (int rs = 0; rs < split.Length; rs++)
                                        {
                                            OwnerID = ContractUserManagement.GetContractOwnerUserIDByEmail(masterlstUsers, CustomerID, split[rs].ToString().Trim());
                                            if (OwnerID != 0 || OwnerID != -1)
                                            {
                                                lstContractOwnerIDs.Add(OwnerID);
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region 17 Contract Amount
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 17].Text.ToString().Trim()))
                                {
                                    ContractAmount = Convert.ToString(xlWorksheet.Cells[rowNum, 17].Text).Trim();
                                }
                                #endregion


                                //#region 18 Payment Term
                                //if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 18].Text.ToString().Trim()))
                                //{
                                //    PaymentTerm = Convert.ToString(xlWorksheet.Cells[rowNum, 18].Text).Trim();
                                //}
                                //#endregion

                                #region 19 Lock in Date Date
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 19].Text.ToString().Trim()))
                                {
                                    LockINDate = Convert.ToString(xlWorksheet.Cells[rowNum, 19].Text).Trim();
                                }
                                #endregion
                                #region 20 Contract Taxes
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 20].Text.ToString().Trim()))
                                {
                                    ContractTax = Convert.ToString(xlWorksheet.Cells[rowNum, 20].Text).Trim();
                                }
                                #endregion
                                //DateTime dtEffectiveDate = DateTime.Parse(EffectiveDate);
                                //DateTime dtExpirationDate = DateTime.Parse(ExpirationDate);

                                DateTime dtEffectiveDate = DateTime.ParseExact(EffectiveDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                DateTime dtExpirationDate = DateTime.ParseExact(ExpirationDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                                Cont_tbl_ContractInstance contractRecord = new Cont_tbl_ContractInstance()
                                {
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    IsDeleted = false,
                                    ContractType = 1,
                                    ContractNo = ContractNumber,
                                    ContractTitle = ContractTitle,
                                    ContractDetailDesc = ContractDescription,
                                    CustomerBranchID = Convert.ToInt32(Entity_Location_BranchID),
                                    DepartmentID = Convert.ToInt32(DeptID),
                                    ContractTypeID = ContractTypeID,
                                    ContractSubTypeID = ContractSubTypeID,
                                    EffectiveDate = dtEffectiveDate,
                                    ExpirationDate = dtExpirationDate,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };

                                if (!(string.IsNullOrEmpty(NoticeTerm)))
                                {
                                    contractRecord.NoticeTermNumber = Convert.ToInt32(NoticeTerm);

                                    if (!(string.IsNullOrEmpty(NoticeTermType)))
                                    {
                                        if (NoticeTermType.Equals("Days"))
                                            contractRecord.NoticeTermType = 1;

                                        if (NoticeTermType.Equals("Weeks"))
                                            contractRecord.NoticeTermType = 2;

                                        if (NoticeTermType.Equals("Months"))
                                            contractRecord.NoticeTermType = 3;

                                        if (NoticeTermType.Equals("Years"))
                                            contractRecord.NoticeTermType = 4;
                                    }
                                }

                                //if (!(string.IsNullOrEmpty(PaymentTerm.Trim())))
                                //{
                                //    if (PaymentTerm.Equals("One-Time"))
                                //        contractRecord.PaymentTermID = 0;

                                //    if (PaymentTerm.Equals("Daily"))
                                //        contractRecord.PaymentTermID = 1;

                                //    if (PaymentTerm.Equals("Weekly"))
                                //        contractRecord.PaymentTermID = 7;

                                //    if (PaymentTerm.Equals("Periodically"))
                                //        contractRecord.PaymentTermID = 15;

                                //    if (PaymentTerm.Equals("Monthly"))
                                //        contractRecord.PaymentTermID = 30;

                                //    if (PaymentTerm.Equals("Quarterly"))
                                //        contractRecord.PaymentTermID = 4;

                                //    if (PaymentTerm.Equals("Half-Yearly"))
                                //        contractRecord.PaymentTermID = 6;

                                //    if (PaymentTerm.Equals("Yearly"))
                                //        contractRecord.PaymentTermID = 12;
                                //}
                                if (!(string.IsNullOrEmpty(LockINDate)))
                                {
                                    contractRecord.LockInPeriodDate = DateTime.ParseExact(LockINDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                if (!(string.IsNullOrEmpty(ContractAmount)))
                                {
                                    contractRecord.ContractAmt = Convert.ToDecimal(ContractAmount);
                                }
                                if (!(string.IsNullOrEmpty(ContractTax)))
                                {
                                    contractRecord.Taxes = ContractTax;
                                }
                                if (!(string.IsNullOrEmpty(ProposalDate)))
                                {
                                    //contractRecord.ProposalDate = DateTimeExtensions.GetDate(ProposalDate);                                    
                                    contractRecord.ProposalDate = DateTime.ParseExact(ProposalDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                if (!(string.IsNullOrEmpty(AgreementDate)))
                                {
                                    //ontractRecord.AgreementDate = DateTimeExtensions.GetDate(AgreementDate);
                                    contractRecord.AgreementDate = DateTime.ParseExact(AgreementDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }
                                if (!(string.IsNullOrEmpty(ReviewDate)))
                                {
                                    //contractRecord.ReviewDate = DateTimeExtensions.GetDate(ReviewDate);
                                    contractRecord.ReviewDate = DateTime.ParseExact(ReviewDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                }

                                newContractID = ContractManagement.CreateContract(contractRecord);


                                if (newContractID > 0)
                                {
                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Contract Created by Excel Upload", true, Convert.ToInt32(newContractID));
                                    saveSuccess = true;
                                }

                                if (saveSuccess)
                                {
                                    #region Payment term Mapping
                                    
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 18].Text.ToString().Trim()))
                                    {
                                        PaymentTerm = Convert.ToString(xlWorksheet.Cells[rowNum, 18].Text).Trim();
                                    }
                                    string[] split = PaymentTerm.Split(',');
                                    List<long> lstPaymenttermMapping = new List<long>();
                                    if (split.Length > 0)
                                    {                                       
                                        foreach (var item in split)
                                        {
                                            if (!(string.IsNullOrEmpty(item)))
                                            {
                                                if (item.Trim().Equals("One-Time"))
                                                    lstPaymenttermMapping.Add(0);

                                                if (item.Trim().Equals("Daily"))
                                                    lstPaymenttermMapping.Add(1);

                                                if (item.Trim().Equals("Weekly"))
                                                    lstPaymenttermMapping.Add(7);

                                                if (item.Trim().Equals("Periodically"))
                                                    lstPaymenttermMapping.Add(15);

                                                if (item.Trim().Equals("Monthly"))
                                                    lstPaymenttermMapping.Add(30);

                                                if (item.Trim().Equals("Quarterly"))
                                                    lstPaymenttermMapping.Add(4);

                                                if (item.Trim().Equals("Half-Yearly"))
                                                    lstPaymenttermMapping.Add(6);

                                                if (item.Trim().Equals("Yearly"))
                                                    lstPaymenttermMapping.Add(12);
                                            }
                                        }
                                    }
                                    if (lstPaymenttermMapping.Count > 0)
                                    {
                                        List<Cont_tbl_PaymentTermMapping> lstPaymenttermMapping_ToSave = new List<Cont_tbl_PaymentTermMapping>();

                                        lstPaymenttermMapping.ForEach(EachPaymentterm =>
                                        {
                                            Cont_tbl_PaymentTermMapping paymenttermMappingRecord = new Cont_tbl_PaymentTermMapping()
                                            {
                                                ContractID = newContractID,
                                                PaymentTermID = EachPaymentterm,
                                                IsDeleted = false,
                                                CustomerID = Convert.ToInt64(AuthenticationHelper.CustomerID),
                                            };

                                            lstPaymenttermMapping_ToSave.Add(paymenttermMappingRecord);
                                        });

                                        saveSuccess = ContractManagement.CreateUpdate_PaymentTermMapping(lstPaymenttermMapping_ToSave);
                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_PaymentTermMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Payment Term Mapping Created", true, Convert.ToInt32(newContractID));
                                        }

                                        //Refresh List
                                        lstPaymenttermMapping_ToSave.Clear();
                                        lstPaymenttermMapping_ToSave = null;

                                        lstPaymenttermMapping.Clear();
                                        lstPaymenttermMapping = null;
                                    }
                                    #endregion
                                  
                                    #region Contract Status Transaction - Draft
                                    if (contractRecord.EffectiveDate < DateTime.Now.Date && (contractRecord.ExpirationDate > DateTime.Now.Date))
                                    {
                                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                        {
                                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                            ContractID = newContractID,
                                            StatusID = Convert.ToInt32(7), //Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            UpdatedOn = DateTime.Now,
                                        };
                                        saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                    }
                                    else if (contractRecord.ExpirationDate < DateTime.Now.Date)
                                    {
                                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                        {
                                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                            ContractID = newContractID,
                                            StatusID = Convert.ToInt32(10), //Expired
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            UpdatedOn = DateTime.Now,
                                        };
                                        saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                                    }
                                    else
                                    {
                                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                        {
                                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                            ContractID = newContractID,
                                            StatusID = Convert.ToInt32(1), //Draft
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            UpdatedOn = DateTime.Now,
                                        };
                                        saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                    }

                                    #endregion

                                    #region Vendor Mapping
                                    if (lstVendorIDs.Count > 0)
                                    {
                                        lstVendorMapping_ToSave.Clear();

                                        lstVendorIDs.ForEach(EachVendor =>
                                        {
                                            Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                            {
                                                ContractID = newContractID,
                                                VendorID = EachVendor,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                            };

                                            lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                        });

                                        saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Mapping Created", true, Convert.ToInt32(newContractID));
                                        }

                                        lstVendorIDs.Clear();
                                        lstVendorMapping_ToSave.Clear();
                                    }
                                    #endregion

                                    #region User Assignment                           

                                    if (lstContractOwnerIDs.Count > 0)
                                    {
                                        lstUserAssignmentRecord_ToSave.Clear();

                                        lstContractOwnerIDs.ForEach(eachOwner =>
                                        {
                                            Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                            {
                                                AssignmentType = 1,
                                                ContractID = newContractID,
                                                UserID = eachOwner,
                                                RoleID = 3,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            lstUserAssignmentRecord_ToSave.Add(newAssignment);
                                        });

                                        if (lstUserAssignmentRecord_ToSave.Count > 0)
                                            saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                        if (saveSuccess)
                                        {
                                            uploadedContractCount++;
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner Mapping Created", true, Convert.ToInt32(newContractID));
                                        }

                                        lstContractOwnerIDs.Clear();
                                        lstUserAssignmentRecord_ToSave.Clear();
                                    }
                                    #endregion
                                }
                            }

                            #endregion
                        }

                        if (saveSuccess)
                        {
                            if (uploadedContractCount > 0)
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = uploadedContractCount + " Contract(s) Details Uploaded Successfully";
                                cvUploadUtilityPage.CssClass = "alert alert-success";
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                            }
                        }

                        masterlstCustomerBranches = null;
                        masterlstUsers = null;
                        masterlstVendors = null;
                        masterlstDepts = null;
                        masterlstContactTypes = null;
                        masterlstContactSubTypes = null;

                    }// END DB Context
                } 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void ProcessVendorData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedVendorCount = 0;

                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Vendor"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string VendorName = string.Empty;
                    string VendorType = string.Empty;
                    int VendorTypeID = 0;
                    string CorporateName = string.Empty;
                    int CorporateTypeID = 0;
                    string isMSME = string.Empty;
                    int MSMEID = 0;


                    #region Validations

                    List<State> Stateobj = new List<State>();
                    Stateobj = ContractManagement.GetStateData(1);

                    List<City> CityObj = new List<City>();
                    CityObj = ContractManagement.GetCityData();

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        #region 1 Vendor Type and Name

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()) && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            VendorType = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            VendorName = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

                            if (!(string.IsNullOrEmpty(VendorType)) && (!(string.IsNullOrEmpty(VendorName))))
                            {
                                if (VendorType.Equals("Individual"))
                                {
                                    VendorTypeID = 1;
                                }
                                else
                                {
                                    VendorTypeID = 2;
                                }

                                bool matchSuccess = false;
                                matchSuccess = checkDuplicateMultipleDataExistExcelSheet(xlWorksheet, i, 2, VendorName, VendorType);
                                if (matchSuccess)
                                {
                                    errorMessage.Add("Vendor Name at Row - " + (count + 1) + " Exists Multiple Times For Vendor Type in the Uploaded Excel Document");
                                }
                                bool existVendor = VendorDetails.ExistsVendorData(VendorName, CustomerID, 0, VendorTypeID);
                                if (existVendor)
                                {
                                    errorMessage.Add("Vendor Name already exists at row number - " + (count + 1) + "");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Please enter Vendor Name at row number - " + (count + 1) + "");
                            }
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                        {
                            errorMessage.Add("Please enter Vendor Name at row number" + (count + 1) + ".");
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                        {
                            errorMessage.Add("Please enter Vendor Type at row number" + (count + 1) + ".");
                        }
                        else
                        {
                            if((xlWorksheet.Cells[i, 1].Text).Trim() == "Individual" || (xlWorksheet.Cells[i, 1].Text).Trim() == "Corporate")
                            {

                            }
                            else
                            {
                                errorMessage.Add("Please enter valid Vendor Type(Individual/Corporate) at row number" + (count + 1) + ".");
                            }
                        }
                        #endregion

                        #region 2 Contact
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            bool ValidatePhone = ContractCommonMethods.IsValidPhoneNumber(xlWorksheet.Cells[i, 9].Text.ToString().Trim());
                            if (ValidatePhone == false)
                            {
                                errorMessage.Add("Please enter valid Contact Number at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 3 Email
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        {
                            bool ValidateEmail = ContractCommonMethods.IsValidEmail(xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                            if (ValidateEmail == false)
                            {
                                errorMessage.Add("Please enter valid Email ID at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 3 Country,State,City
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            int CountryID = -1;
                            int StateId = -1;
                            int CityId = -1;
                            string Country = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                            if (!string.IsNullOrEmpty(Country))
                            {
                                if (Country.Equals("India"))
                                {
                                    CountryID = 1;
                                }
                                else
                                {
                                    CountryID = -1;
                                    errorMessage.Add("Please enter valid Country Name at row number - " + (count + 1) + "");
                                }
                                if (CountryID == 1)
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                                    {
                                        string State = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                                        if (!(string.IsNullOrEmpty(State)))
                                        {
                                            StateId = Convert.ToInt32((from row in Stateobj
                                                                       where row.Name.Trim().ToUpper().Equals(State.Trim().ToUpper())
                                                                       && row.IsDeleted == false && row.CountryID == CountryID
                                                                       select row.ID).FirstOrDefault());
                                            if (StateId <= 0)
                                            {
                                                StateId = -1;
                                                errorMessage.Add("Please enter valid State Name at row number - " + (count + 1) + "");
                                            }
                                            else
                                            {
                                                string City = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                                if (!(string.IsNullOrEmpty(City)))
                                                {
                                                    CityId = Convert.ToInt32((from row in CityObj
                                                                              where row.Name.Trim().ToUpper().Equals(City.Trim().ToUpper())
                                                                              && row.IsDeleted == false && row.StateId == StateId
                                                                              select row.ID).FirstOrDefault());
                                                    if (CityId <= 0)
                                                    {
                                                        CityId = -1;
                                                        errorMessage.Add("Please enter valid City Name at row number - " + (count + 1) + "");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                                    {
                                        errorMessage.Add("Please enter valid State Name at row number - " + (count + 1) + "");
                                    }
                                }
                            }
                        }
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Please enter valid Country Name at row number - " + (count + 1) + "");
                        }
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Please enter valid Country Name at row number - " + (count + 1) + "");
                        }
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                        {
                            bool ValidateEmail = ContractCommonMethods.IsValidPanNumber(xlWorksheet.Cells[i, 13].Text.ToString().Trim());
                            if (ValidateEmail == false)
                            {
                                errorMessage.Add("Please enter valid Pan No at row number - " + (count + 1) + "");
                            }
                        }
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                        {
                           if(xlWorksheet.Cells[i, 14].Text == "Yes" || xlWorksheet.Cells[i, 14].Text == "No")
                            {

                            }
                           else
                            {
                                errorMessage.Add("Please enter valid MSME(Yes/No) at row number - " + (count + 1) + "");
                            }
                        }
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                        {
                            if (xlWorksheet.Cells[i, 15].Text.Trim() == "LLP" || xlWorksheet.Cells[i, 15].Text.Trim() == "Firm" || xlWorksheet.Cells[i, 15].Text.Trim() == "Company")
                            {

                            }
                            else
                            {
                                errorMessage.Add("Please enter valid Corporate Type at row number - " + (count + 1) + "");
                            }
                        }


                        #endregion
                    }

                    #endregion

                    string ContractTypeNameFlag = string.Empty;

                    if (errorMessage.Count <= 0)
                    {
                        #region Save                        
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            VendorType = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            VendorName = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            CorporateName = Convert.ToString(xlWorksheet.Cells[i, 15].Text).Trim();
                            isMSME = Convert.ToString(xlWorksheet.Cells[i, 14].Text).Trim();
                            int CountryID = -1;
                            int StateId = -1;
                            int CityId = -1;

                            string Country = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                            if (!string.IsNullOrEmpty(Country))
                            {
                                if (Country.Equals("India"))
                                {
                                    CountryID = 1;
                                }
                            }
                            if (CountryID > 0)
                            {
                                string State = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                                if (!(string.IsNullOrEmpty(State)))
                                {
                                    //StateId = ContractMastersManagement.GetStateID(State, CountryID);
                                    StateId = Convert.ToInt32((from row in Stateobj
                                                               where row.Name.Trim().ToUpper().Equals(State.Trim().ToUpper())
                                                               && row.IsDeleted == false && row.CountryID == CountryID
                                                               select row.ID).FirstOrDefault());
                                    if (StateId <= 0)
                                    {
                                        StateId = -1;
                                    }
                                }
                                if (StateId > 0)
                                {
                                    string City = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                    if (!(string.IsNullOrEmpty(City)))
                                    {
                                        //CityId = ContractMastersManagement.GetCityID(City, StateId);
                                        CityId = Convert.ToInt32((from row in CityObj
                                                                  where row.Name.Trim().ToUpper().Equals(City.Trim().ToUpper())
                                                                  && row.IsDeleted == false && row.StateId == StateId
                                                                  select row.ID).FirstOrDefault());
                                        if (CityId <= 0)
                                        {
                                            CityId = -1;
                                        }
                                    }
                                }
                            }


                            if (VendorType.Equals("Individual"))
                            {
                                VendorTypeID = 1;
                            }
                            else
                            {
                                VendorTypeID = 2;
                            }
                            if(CorporateName.Equals("LLP"))
                            {
                                CorporateTypeID = 1;
                            }
                            else if(CorporateName.Equals("Firm"))
                            {
                                CorporateTypeID = 2;
                            }
                            else if(CorporateName.Equals("Company"))
                            {
                                CorporateTypeID = 3;
                            }
                            else
                            {
                                CorporateTypeID = -1;
                            }
                            if (isMSME.Equals("Yes"))
                            {
                                MSMEID = 1;
                            }
                            else
                            {
                                MSMEID = 0;
                            }
                            Cont_tbl_VendorMaster objVendor = new Cont_tbl_VendorMaster()
                            {
                                Type = Convert.ToInt32(VendorTypeID),
                                VendorName = VendorName,
                                CustomerID = Convert.ToInt32(CustomerID),
                                IsDeleted = false,
                                CreatedOn = DateTime.Now,
                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                UpdatedOn = DateTime.Now,
                                UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                CountryID = CountryID,
                                StateID = StateId,
                                CityID = CityId,
                                Address = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim(),
                                ContactPerson = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim(),
                                Email = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim(),
                                ContactNumber = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim(),
                                VAT = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim(),
                                TIN = Convert.ToString(xlWorksheet.Cells[i, 11].Text).Trim(),
                                GSTIN = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim(),
                                PAN = Convert.ToString(xlWorksheet.Cells[i, 13].Text).Trim(),
                                isMSME =Convert.ToBoolean(MSMEID),
                                TOC=CorporateTypeID
                            };

                            VendorDetails.CreateVendorData(objVendor);
                            uploadedVendorCount++;
                            saveSuccess = true;
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (saveSuccess)
                    {
                        if (uploadedVendorCount > 0)
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = uploadedVendorCount + " Vendor(s) Details Uploaded Successfully.";
                            cvUploadUtilityPage.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void ProcessContractSubTypeData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedSubTypeCount = 0;

                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Contract Sub-Type"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Contract_Type_Name = string.Empty;
                    string Contract_SubType_Name = string.Empty;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        #region 1 Contract Type
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()) && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            Contract_Type_Name = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            Contract_SubType_Name = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

                            bool existContractType = ContractTypeMasterManagement.ExistsContractType(Contract_Type_Name, CustomerID, 0);
                            if (existContractType)
                            {
                                bool matchSuccess = false;
                                matchSuccess = checkDuplicateMultipleDataExistExcelSheet(xlWorksheet, i, 2, Contract_SubType_Name, Contract_Type_Name);
                                if (matchSuccess)
                                {
                                    errorMessage.Add("Contract Sub Type at Row - " + (count + 1) + " Exists Multiple Times For Contract Type in the Uploaded Excel Document");
                                }
                                int ContractTypeIdExist = Convert.ToInt32(ContractTypeMasterManagement.GetContractTypeID(Contract_Type_Name, CustomerID));
                                bool existSubContType = ContractTypeMasterManagement.ExistsSubContractType(Contract_SubType_Name, CustomerID, 0, ContractTypeIdExist);
                                if (existSubContType)
                                {
                                    errorMessage.Add("Contract Sub Type already exists at row number - " + (count + 1) + "");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Contract Type Not found at row number - " + (count + 1) + "");
                            }
                        }
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Required Contract Type at row number" + (count + 1) + ".");
                        }
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Required Contract Sub Type at row number" + (count + 1) + ".");
                        }
                        #endregion

                    }
                    #endregion

                    string ContractTypeNameFlag = string.Empty;
                    int ContractTypeId = 0;
                    if (errorMessage.Count <= 0)
                    {
                        #region Save                        
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            Contract_Type_Name = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            Contract_SubType_Name = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

                            if (!(ContractTypeNameFlag.Equals(Contract_SubType_Name)))
                            {
                                ContractTypeId = Convert.ToInt32(ContractTypeMasterManagement.GetContractTypeID(Contract_Type_Name, CustomerID));
                                ContractTypeNameFlag = Contract_SubType_Name;
                            }
                            if (ContractTypeId > 0)
                            {
                                Cont_tbl_SubTypeMaster objSubType = new Cont_tbl_SubTypeMaster()
                                {
                                    ContractTypeID = ContractTypeId,
                                    SubTypeName = Contract_SubType_Name.ToString(),
                                    CustomerID = (int) CustomerID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false
                                };
                                ContractTypeMasterManagement.CreateContractSubType(objSubType);
                                uploadedSubTypeCount++;
                                saveSuccess = true;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (saveSuccess)
                    {
                        if (uploadedSubTypeCount > 0)
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = uploadedSubTypeCount + " Contract Sub-Type Details Uploaded Successfully";
                            cvUploadUtilityPage.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void ProcessContractTypeData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedTypeCount = 0;

                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Contract Type"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Contract_Type_Name = string.Empty;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        #region 1 Contract Type
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Contract_Type_Name = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();

                            bool matchSuccess = false;
                            matchSuccess = checkDuplicateDataExistExcelSheet(xlWorksheet, i, 1, Contract_Type_Name);

                            if (matchSuccess)
                            {
                                errorMessage.Add("Contract Type at Row - " + (count + 1) + " Exists Multiple Times in the Uploaded Excel Document");
                            }
                            else
                            {
                                bool existContractType = ContractTypeMasterManagement.ExistsContractType(Contract_Type_Name, CustomerID, 0);
                                if (existContractType)
                                {
                                    errorMessage.Add("Contract Type already exists at row number - " + (count + 1) + "");
                                }
                            }
                        }
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Required Contract Type " + (count + 1) + ".");
                        }
                        #endregion

                    }
                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save                        
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            Contract_Type_Name = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            Cont_tbl_TypeMaster objCont = new Cont_tbl_TypeMaster()
                            {
                                TypeName = Contract_Type_Name.ToString(),
                                CustomerID = (int) CustomerID,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };
                            ContractTypeMasterManagement.CreateContractTypeDetails(objCont, CustomerID);
                            uploadedTypeCount++;
                            saveSuccess = true;
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (saveSuccess)
                    {
                        if (uploadedTypeCount > 0)
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = uploadedTypeCount + " Contract Type Details Uploaded Successfully.";
                            cvUploadUtilityPage.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void ProcessDocumentTypeData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedDocTypeCount = 0;

                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Document Type"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Document_type = string.Empty;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        Document_type = string.Empty;

                        #region 1 Document Type
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Document_type = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();

                            bool matchSuccess = false;
                            matchSuccess = checkDuplicateDataExistExcelSheet(xlWorksheet, i, 1, Document_type);

                            if (matchSuccess)
                            {
                                errorMessage.Add("Document Type at Row - " + (count + 1) + " Exists Multiple Times in the Uploaded Excel Document");
                            }
                            else
                            {
                                bool existDocumentType = ContractDocumentManagement.ExistsContractDocumentType(Document_type, CustomerID, 0);
                                if (existDocumentType)
                                {
                                    errorMessage.Add("Document Type already exists at row number - " + (count + 1) + "");
                                }
                            }
                        }
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Required Document Type " + (count + 1) + ".");
                        }
                        #endregion

                    }
                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save                        
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            Document_type = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            Cont_tbl_DocumentTypeMaster objCont = new Cont_tbl_DocumentTypeMaster()
                            {
                                TypeName = Document_type.ToString(),
                                CustomerID = (int) CustomerID,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };
                            ContractMastersManagement.CreateDocumentType(objCont);
                            uploadedDocTypeCount++;
                            saveSuccess = true;
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (saveSuccess)
                    {
                        if (uploadedDocTypeCount > 0)
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = uploadedDocTypeCount + " Document Type Details Uploaded Successfully";
                            cvUploadUtilityPage.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        
    }
}