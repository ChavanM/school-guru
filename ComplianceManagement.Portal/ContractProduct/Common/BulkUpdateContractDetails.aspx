﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="BulkUpdateContractDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.BulkUpdateContractDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftuploadmenu');
            fhead('Bulk Update Details');
        });


        function ShowDialog(CaseInstanceID) {
           
            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '600px');
            $('#showdetails').attr('src', "../Common/BulkUpdateDetails.aspx?AccessID=" + CaseInstanceID);
            
        };

        function ShowDialog1(CaseInstanceID) {
           
            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '610px');
            $('#showdetails').attr('src', "../Common/ContractKendoGrid.aspx?AccessID=" + CaseInstanceID);
         
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upUploadUtility">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upUploadUtility" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">

                    <div class="col-lg-12 col-md-12" style="min-height: 0px; max-height: 250px; overflow-y: auto;">
                        <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                            <asp:CustomValidator ID="cvUploadUtility" runat="server" EnableClientScript="False"
                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                        </asp:Panel>
                    </div>
                     <div class="col-lg-12 col-md-12" style="min-height: 0px; max-height: 250px; overflow-y: auto;">
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto">
                            <asp:ValidationSummary ID="vsUploadUtilitySuccess" runat="server"
                                class="alert alert-block alert-success fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroupSuccess" />
                            <asp:CustomValidator ID="cvUploadUtilityPageSuccess" runat="server" EnableClientScript="False"
                                ValidationGroup="uploadUtilityValidationGroupSuccess" Display="None" Enabled="true" ShowSummary="true" />
                        </asp:Panel>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-lg-12 col-md-12 colpadding0">

                        <div class="col-md-3 colpadding0 d-none" runat="server" id="divUploadContract" >
                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnOpenUploaddialog" data-toggle="tooltip" ToolTip="Select Fields Bulk Update"
                                OnClick="btnOpenUploaddialog_Click"></i></span>&nbsp;Select Fields for Update</asp:LinkButton>
                        </div>

                        <div class="col-md-3 colpadding0 " runat="server" id="div1">
                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnOpenkendodialog" data-toggle="tooltip" ToolTip="Select Fields Bulk Update"
                                OnClick="btnOpenkendodialog_Click"></i></span>&nbsp;Select Fields for Update</asp:LinkButton>
                        </div>

                        <div class="col-md-4 colpadding0 ">
                            <asp:FileUpload ID="MasterFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                            <asp:RequiredFieldValidator ID="rfvFileUpload" ErrorMessage="Please Select File" ControlToValidate="MasterFileUpload"
                                runat="server" Display="None" ValidationGroup="uploadUtilityValidationGroup" />
                        </div>

                        <div class="col-md-2 colpadding0 text-left">
                            <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="uploadUtilityValidationGroupSuccess"
                                class="btn btn-primary" OnClick="btnUploadFile_Click"  />
                        </div>


                      <%--  <div class="col-md-2 colpadding0 text-right d-none">
                            <asp:Button ID="BulkConUpload" runat="server" Text="Bulk Contract Upload"
                                class="btn btn-primary" OnClientClick="OpenUploadWindow()" />
                        </div>--%>

                    </div>
                </div>
            </div>
            </div>
        </ContentTemplate>
        <Triggers>


            <asp:PostBackTrigger ControlID="btn_Download" />
            <asp:PostBackTrigger ControlID="btnUploadFile" />


        </Triggers>
    </asp:UpdatePanel>

    <div class="modal fade" id="SelectFields" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 1000px">
            <div class="modal-content" style="height: 200px">

                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Select Fields</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="clearfix"></div>

                <div class="modal-body">
                    <div class="col-md-12 colpadding0">

                        <div class="col-md-2 ">
                            <asp:CheckBox ID="ContractID" Text="ContractID" runat="server" />
                            <label style="width: 10px; float: left; font-size: 13px;"></label>
                        </div>

                        <div class="col-md-2 colpadding0 ">
                            <asp:Button ID="btn_Download" runat="server" Text="Download" OnClick="btn_Download_Click" ValidationGroup="uploadUtilityValidationGroupSuccess"
                                class="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="divContractFieldDetails">
        <div class="chart-loading" id="loader"></div>
        <iframe id="iframeContractField" style="width: 580px; height: 300px; border: none"></iframe>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 240px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Update Contract Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">


        function OpenUploadWindow() {
            $('#divContractFieldDetails').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeContractField').attr('src', "/Setup/BulkUpdateContractDetails?CustID=<%=CustomerID%>");
            var myWindowAdv = $("#divContractFieldDetails");

            function onClose() {

            }
            myWindowAdv.kendoWindow({
                width: "70%",
                height: "70%",
                title: "Contract Field",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;
        }


    </script>


</asp:Content>
