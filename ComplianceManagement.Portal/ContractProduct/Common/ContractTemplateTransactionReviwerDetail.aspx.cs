﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class ContractTemplateTransactionReviwerDetail : System.Web.UI.Page
    {
        protected static int ContractTemplateId;        
        protected static int CustId;
        protected static int UId;
        protected static int RoleId;
        protected static int SectionId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);                
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                SetDefaultConfiguratorValues(UserID, RoleID, ContractTemplateID);
                BindTransactionHistory(ContractTemplateID, RoleID);         
            }
        }

        public void BindSections_All(GridView grdTemplateSection, long ContractTemplateID)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            grdTemplateSection.DataSource = ContractTemplateManagement.GetContractSections_All(customerID, Convert.ToInt32(ContractTemplateID));
            grdTemplateSection.DataBind();
        }

        protected void grdTemplateSection_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DropDownList ddlUser1 = (DropDownList)e.Row.FindControl("ddlReviewer1");
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //
                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<long> getdata = (from row in entities.Cont_tbl_UserDeptMapping
                                          where row.IsActive == true
                                          && row.CustomerID == customerID
                                          select row.UserID).ToList();

                    var data = lstAllUsers.Where(x => getdata.Contains(x.ID)).ToList();
                    var internalUsers = ContractUserManagement.GetRequiredUsers(data, 1); //1--Internal---
                    ddlUser1.DataValueField = "ID";
                    ddlUser1.DataTextField = "Name";
                    ddlUser1.DataSource = internalUsers;
                    ddlUser1.DataBind();
                    ddlUser1.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Web.UI.WebControls.Label lblDeptID = (System.Web.UI.WebControls.Label)e.Row.FindControl("lblDeptID");
                if (lblDeptID != null)
                {
                    int deptId = Convert.ToInt32(lblDeptID.Text);
                    if (!string.IsNullOrEmpty(lblDeptID.Text) || lblDeptID.Text != "0")
                    {
                        DropDownList ddlUser = (DropDownList)e.Row.FindControl("ddlReviewer");
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                        if (lstAllUsers.Count > 0)
                            lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            List<long> getdata = (from row in entities.Cont_tbl_UserDeptMapping
                                                  where row.DeptID == deptId
                                                  && row.IsActive == true
                                                  && row.CustomerID == customerID
                                                  select row.UserID).ToList();

                            var data = lstAllUsers.Where(x => getdata.Contains(x.ID)).ToList();
                            var internalUsers = ContractUserManagement.GetRequiredUsers(data, 1); //1--Internal---
                            ddlUser.DataValueField = "ID";
                            ddlUser.DataTextField = "Name";
                            ddlUser.DataSource = internalUsers;
                            ddlUser.DataBind();
                            ddlUser.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                        }
                    }
                }
            }
        }
        public bool getApproverStatus(int ContractTemplateId, int UserID, int RoleID)
        {
            bool status = false;
            if (RoleID == 6)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                       where row.ContractTemplateInstanceID == ContractTemplateId
                                                       && row.UserID == UserID
                                                       && row.IsActive == true
                                                       && row.IsAction == 0
                                                       select row).FirstOrDefault();
                    if (data != null)
                    {
                        status = true;
                    }
                }
            }
            else
                status = true;

            return status;
        }
        public int getRoleID(int ContractTemplateId, int UserID, int sectionID)
        {
            int RoleID = -1;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                   where row.ContractTemplateInstanceID == ContractTemplateId
                                                   && row.UserID == UserID
                                                   && row.SectionID == sectionID
                                                   select row).FirstOrDefault();
                if (data != null)
                {
                    RoleID = Convert.ToInt32(data.RoleID);
                }
            }
            return RoleID;
        }
        public int getRID(string roleCode)
        {
            int RoleID = -1;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Role data = (from row in entities.Roles
                                 where row.Code == roleCode
                                 select row).FirstOrDefault();
                    if (data != null)
                    {
                        RoleID = Convert.ToInt32(data.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return RoleID;
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    int RolegetID = getRID("PAPPR");
                    SectionId = Convert.ToInt32(commandArgs[0]);
                    int StatusId = Convert.ToInt32(commandArgs[1]);
                    ContractTemplateId = Convert.ToInt32(Request.QueryString["ContractTID"]);
                    RoleId = Convert.ToInt32(Request.QueryString["RID"]);
                    //RoleId = getRoleID(ContractTemplateId, AuthenticationHelper.UserID, SectionId);                    
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var entitiesData = (from row in entities.ContractTemplateAssignments
                                            where row.ContractTemplateInstanceID == ContractTemplateId
                                              && row.UserID == UId
                                              && row.IsActive == true
                                              && row.SectionID == SectionId
                                              && row.RoleID == RoleId
                                            select row).FirstOrDefault();
                        if (entitiesData != null)
                        {
                            int RID = Convert.ToInt32(entitiesData.RoleID);
                            if (RID == 3)
                            {
                                if (StatusId == 1 || StatusId == 3)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                            if (RID == 4)
                            {
                                if (StatusId == 2)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                            if (RID == 6)
                            {
                                if (StatusId == 4)
                                {
                                    bool status = getApproverStatus(Convert.ToInt32(ContractTemplateId), UId, RID);
                                    if (status)
                                    {
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                    }
                                }
                            }
                            if (RID == RolegetID)//Primary Approver
                            {
                                if (StatusId == 18)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public class ContractSectionDetail
        {
            public string Header { get; set; }
            public long ContractTemplateInstanceID { get; set; }
            public long SectionID { get; set; }
            public Nullable<int> SectionOrder { get; set; }
            public int CreatedBy { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string DeptName { get; set; }
            public string DeptColor { get; set; }
            public string SectionStatusID { get; set; }
            public string SectionStatus { get; set; }
            public string SectionReviwerName { get; set; }
            public int Visbilty { get; set; }
        }
        public void BindTransactionHistory(long TemplateInstanceID, long RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RolegetID = getRID("PAPPR");
                btnreviewer.Visible = false;
                btnreviewerreject.Visible = false;
                btnApprover.Visible = false;
                btnRejcted.Visible = false;
                btnApproverPrimary.Visible = false;
                btnRejctedPrimary.Visible = false;

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateInstanceID))
                                    select row).ToList();

                if (entitiesData != null)
                {
                    var details = (from row in entitiesData
                                   select new ContractSectionDetail()
                                   {
                                       Header = row.Header,
                                       ContractTemplateInstanceID = row.ContractTemplateInstanceID,
                                       SectionID = row.SectionID,
                                       SectionOrder = row.SectionOrder,
                                       CreatedBy = row.CreatedBy,
                                       CreatedOn = row.CreatedOn,
                                       DeptName = row.DeptName,
                                       DeptColor = row.DeptColor,
                                       SectionStatusID = row.SectionStatusID,
                                       SectionStatus = row.SectionStatus,
                                       SectionReviwerName = row.SectionReviwerName,
                                       Visbilty = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.SectionID, RoleID)
                                   }).ToList();

                    if (details.Count > 0)
                    {
                        details = details.Where(x => x.Visbilty == 1).ToList();
                    }
                    if (details.Count > 0)
                    {
                        if (RoleID == 4)
                        {
                            btnreviewer.Visible = true;
                            btnreviewerreject.Visible = true;
                            btnApprover.Visible = false;
                            btnRejcted.Visible = false;
                            btnApproverPrimary.Visible = false;
                            btnRejctedPrimary.Visible = false;
                            
                            btnreviewer.Enabled = false;
                            btnreviewerreject.Enabled = false;

                            int flagpendingrev = details.Where(x => x.SectionStatusID == "2").ToList().Count;
                            if (flagpendingrev > 0)
                            {
                                btnreviewer.Enabled = true;
                                btnreviewerreject.Enabled = true;

                            }
                        }
                        if (RoleID == RolegetID)//Primary Approver
                        {
                            btnreviewerreject.Visible = false;
                            btnreviewer.Visible = false;
                            btnApprover.Visible = false;
                            btnRejcted.Visible = false;
                            btnApproverPrimary.Visible = true;
                            btnRejctedPrimary.Visible = true;

                            btnApproverPrimary.Enabled = false;
                            btnRejctedPrimary.Enabled = false;

                            int flagpendingrev = details.Where(x => x.SectionStatusID == "18").ToList().Count;
                            if (flagpendingrev > 0)
                            {
                                btnRejctedPrimary.Enabled = true;
                                btnApproverPrimary.Enabled = true;
                            }
                        }
                        if (RoleID == 6)
                        {
                            btnreviewerreject.Visible = false;
                            btnreviewer.Visible = false;
                            btnApprover.Visible = true;
                            btnRejcted.Visible = true;
                            btnApproverPrimary.Visible = false;
                            btnRejctedPrimary.Visible = false;

                            btnApprover.Enabled = false;
                            btnRejcted.Enabled = false;

                            int flagpendingrev = details.Where(x => x.SectionStatusID == "4").ToList().Count;
                            if (flagpendingrev > 0)
                            {
                                bool status = getApproverStatus(Convert.ToInt32(TemplateInstanceID), UserID, 6);
                                if (status)
                                {
                                    btnApprover.Enabled = true;
                                    btnRejcted.Enabled = true;
                                }                              
                            }
                        }
                    }
                    rptComplianceVersionView.DataSource = details.OrderBy(entry => entry.SectionOrder).ToList();
                    rptComplianceVersionView.DataBind();
                }
                else
                {
                    rptComplianceVersionView.DataSource = null;
                    rptComplianceVersionView.DataBind();
                }
            }
        }
        public int geVisiblityStatus(long ContractTemplateId, int UserID, long sectionID, long RoleID)
        {
            int OutputVisiblity = 1;
            if (RoleID == 4)
            {
                OutputVisiblity = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (ContractTemplateId != null && UserID != null && sectionID != null)
                    {
                        ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                           where row.ContractTemplateInstanceID == ContractTemplateId
                                                           && row.RoleID == RoleID
                                                           && row.SectionID == sectionID
                                                           select row).FirstOrDefault();
                        if (data != null)
                        {
                            if (data.UserID == UserID)
                            {
                                OutputVisiblity = 1;
                            }
                            else
                            {
                                if (data.Visibility == 1)
                                {
                                    OutputVisiblity = 1;
                                }
                            }
                        }
                    }
                }
            }
            return OutputVisiblity;
        }
        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }

            public int visiblityID { get; set; }

            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }

        private void SetDefaultConfiguratorValues(int UserID, int RoleID, int ContractTemplateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string UserName = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where
                             //row.UserID == UserID && 
                             row.ContractTemplateInstanceID == ContractTemplateID
                             && 
                             row.RoleID == RoleID
                             select row).ToList();
               
                if (data1.Count > 0)
                {
                    data1 = data1.Where(x => x.UserID == UserID).ToList();

                    var data = (from row in data1
                                join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                on row.sectionID equals row1.SectionID
                                where row1.ContractTemplateInstanceID == ContractTemplateID
                                select new getTemplateTransactionDetail
                                {
                                    TemplateTransactionID = row.TemplateTransactionID,
                                    TemplateInstanceID = row.ContractTemplateInstanceID,
                                    ComplianceStatusID = row.ComplianceStatusID,
                                    CreatedOn = row.CreatedOn,
                                    Status = row.Status,
                                    StatusChangedOn = row.StatusChangedOn,
                                    TemplateContent = row.ContractTemplateContent,
                                    RoleID = row.RoleID,
                                    UserID = row.UserID,
                                    sectionID = Convert.ToInt32(row.sectionID),
                                    orderID = Convert.ToInt32(row1.SectionOrder),
                                    visiblityID = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.sectionID, RoleID),
                                    visibilty = Convert.ToInt32(row1.Headervisibilty),
                                    sectionHeader = row.SectionHeader
                                }).OrderBy(x => x.orderID).ToList();

                    data = data.Where(x => x.visiblityID == 1).ToList();

                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                    data.ForEach(eachSection =>
                    {
                        if (eachSection.visibilty == 1)
                        {
                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                            strExporttoWord.Append(@"</div>");
                        }

                        strExporttoWord.Append(@"<div>");
                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                        strExporttoWord.Append(@"</div>");
                    });

                    theEditor.Content = strExporttoWord.ToString();
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                    theEditor.EnableTrackChanges = false;
                }
                else
                {
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                    theEditor.EnableTrackChanges = false;
                }
            }
        }

        protected void RadEditor1_ExportContent(object sender, EditorExportingArgs e)
        {
            ExportType exportType = e.ExportType;

            if (exportType == ExportType.Word)
            {
                string exportedOutput = e.ExportOutput;

                Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                DocxFormatProvider docxProvider = new DocxFormatProvider();
                RadFlowDocument document = docxProvider.Import(output);

                Header defaultHeader = document.Sections.First().Headers.Add();
                Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                defaultHeaderParagraph.TextAlignment = Alignment.Right;
                defaultHeaderParagraph.Inlines.AddRun("This is a sample header.");

                Footer defaultFooter = document.Sections.First().Footers.Add();
                Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                defaultFooterParagraph.TextAlignment = Alignment.Right;
                defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                Byte[] modifiedOutput = docxProvider.Export(document);
                string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                e.ExportOutput = finalOutput;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            theEditor.ExportToDocx();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            divHtml.InnerHtml = theEditor.Content;
        }
        protected void BtnFullHistory_Click(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(Request.QueryString["UID"]);
            RoleId = Convert.ToInt32(Request.QueryString["RID"]);
            ContractTemplateId = Convert.ToInt32(Request.QueryString["ContractTID"]);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenHistorySectionModal();", true);
        }

        protected void btnreviewer_Click(object sender, EventArgs e)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where row.UserID == UserID
                             && row.ContractTemplateInstanceID == ContractTemplateID
                             && row.RoleID == RoleID
                             select row).ToList();

                if (data1.Count > 0)
                {
                    var datasectiondetail = (from row in data1
                                             join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                             on row.sectionID equals row1.SectionID
                                             where row1.ContractTemplateInstanceID == ContractTemplateID
                                             select new getTemplateTransactionDetail
                                             {
                                                 TemplateTransactionID = row.TemplateTransactionID,
                                                 TemplateInstanceID = row.ContractTemplateInstanceID,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 CreatedOn = row.CreatedOn,
                                                 Status = row.Status,
                                                 StatusChangedOn = row.StatusChangedOn,
                                                 TemplateContent = row.ContractTemplateContent,
                                                 RoleID = row.RoleID,
                                                 UserID = row.UserID,
                                                 sectionID = Convert.ToInt32(row.sectionID),
                                                 orderID = Convert.ToInt32(row1.SectionOrder),
                                                 visiblityID = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.sectionID, RoleID)
                                             }).OrderBy(x => x.orderID).ToList();

                    if (datasectiondetail.Count > 0)
                    {
                        datasectiondetail = datasectiondetail.Where(x => x.visiblityID == 1).ToList();
                    }
                    foreach (var item in datasectiondetail)
                    {
                        if (item.ComplianceStatusID == 2)
                        {
                            ContractTemplateTransaction obj = new ContractTemplateTransaction();
                            obj.ContractTemplateInstanceID = ContractTemplateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = UserID;
                            obj.ContractTemplateContent = item.TemplateContent;
                            obj.SectionID = item.sectionID;
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            obj.Status = 3;
                            obj.OrderID = 1;
                            obj.Version = ContractVersion;
                            entities.ContractTemplateTransactions.Add(obj);
                            entities.SaveChanges();

                            int TransactionID = Convert.ToInt32(obj.Id);
                            if (TransactionID > 0)
                            {
                                #region set transaction history

                                var dataHistory = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                   where row.UserID == UserID
                                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                                   && row.RoleID == RoleID
                                                   select row).ToList();
                                if (dataHistory.Count > 0)
                                {
                                    var getdata = (from row in dataHistory
                                                   join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                   on row.sectionID equals row1.SectionID
                                                   where row1.ContractTemplateInstanceID == ContractTemplateID
                                                   select new getTemplateTransactionDetail
                                                   {
                                                       TemplateTransactionID = row.TemplateTransactionID,
                                                       TemplateInstanceID = row.ContractTemplateInstanceID,
                                                       ComplianceStatusID = row.ComplianceStatusID,
                                                       CreatedOn = row.CreatedOn,
                                                       Status = row.Status,
                                                       StatusChangedOn = row.StatusChangedOn,
                                                       TemplateContent = row.ContractTemplateContent,
                                                       RoleID = row.RoleID,
                                                       UserID = row.UserID,
                                                       sectionID = Convert.ToInt32(row.sectionID),
                                                       orderID = Convert.ToInt32(row1.SectionOrder),
                                                       visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                       sectionHeader = row.SectionHeader
                                                   }).OrderBy(x => x.orderID).ToList();

                                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                    getdata.ForEach(eachSection =>
                                    {
                                        if (eachSection.visibilty == 1)
                                        {
                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                            strExporttoWord.Append(@"</div>");
                                        }

                                        strExporttoWord.Append(@"<div>");
                                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                        strExporttoWord.Append(@"</div>");
                                    });

                                    ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                    obj1.ContractTemplateID = ContractTemplateID;
                                    obj1.CreatedOn = DateTime.Now;
                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                    obj1.TemplateContent = strExporttoWord.ToString();
                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                    obj1.Status = 3;
                                    obj1.OrderID = 1;
                                    entities.ContractTemplateTransactionHistories.Add(obj1);
                                    entities.SaveChanges();
                                }
                                #endregion
                            }
                        }
                    }
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = customerID,
                        ContractID = Convert.ToInt32(ContractTemplateID),
                        StatusID = 3,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
            }
        }

        protected void btnApprover_Click(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);

                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                
                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where row.UserID == UserID
                             && row.ContractTemplateInstanceID == ContractTemplateID
                             && row.RoleID == RoleID
                             select row).ToList();

                if (data1.Count > 0)
                {
                    var datasectiondetail = (from row in data1
                                join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                on row.sectionID equals row1.SectionID
                                where row1.ContractTemplateInstanceID == ContractTemplateID
                                select new getTemplateTransactionDetail
                                {
                                    TemplateTransactionID = row.TemplateTransactionID,
                                    TemplateInstanceID = row.ContractTemplateInstanceID,
                                    ComplianceStatusID = row.ComplianceStatusID,
                                    CreatedOn = row.CreatedOn,
                                    Status = row.Status,
                                    StatusChangedOn = row.StatusChangedOn,
                                    TemplateContent = row.ContractTemplateContent,
                                    RoleID = row.RoleID,
                                    UserID = row.UserID,
                                    sectionID = Convert.ToInt32(row.sectionID),
                                    orderID = Convert.ToInt32(row1.SectionOrder),
                                    visiblityID = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.sectionID, RoleID)
                                }).OrderBy(x => x.orderID).ToList();

                    if (datasectiondetail.Count > 0)
                    {
                        datasectiondetail = datasectiondetail.Where(x => x.visiblityID == 1).ToList();
                    }
                    bool flagcheck = false;
                    foreach (var item in datasectiondetail)
                    {
                        if (item.ComplianceStatusID == 4)
                        {
                            int SecId = item.sectionID;

                            ContractTemplateAssignment datacheck = (from row in entities.ContractTemplateAssignments
                                                                    where row.ContractTemplateInstanceID == ContractTemplateID
                                                                    && row.RoleID == RoleID
                                                                    && row.IsActive == true
                                                                    && row.UserID == UserID
                                                                    && row.SectionID == SecId
                                                                    && row.IsAction == 0
                                                                    select row).FirstOrDefault();
                            if (datacheck != null)
                            {
                                #region assiggnment next


                                bool updatestatus = false;
                                int nextorderId = 0;
                                var objRecord = (from row in entities.ContractTemplateAssignments
                                                 where row.ContractTemplateInstanceID == ContractTemplateID
                                                 && row.RoleID == RoleID
                                                 && row.SectionID == SecId
                                                 select row).OrderBy(x => x.ApproveOrder).ToList();

                                if (objRecord.Count > 1)
                                {
                                    ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                                       where row.ContractTemplateInstanceID == ContractTemplateID
                                                                       && row.RoleID == RoleID
                                                                       && row.UserID == UserID
                                                                       && row.IsActive == true
                                                                       && row.SectionID == SecId
                                                                       //&& row.IsAction == 0
                                                                       select row).FirstOrDefault();
                                    if (data != null)
                                    {
                                        int orderID = Convert.ToInt32(data.ApproveOrder);
                                        nextorderId = orderID + 1;
                                        var getdetails = objRecord.Where(x => x.ApproveOrder == nextorderId && x.IsActive == false && x.IsAction == 0).FirstOrDefault();
                                        if (getdetails != null)
                                        {
                                            updatestatus = true;
                                            data.IsAction = 1;
                                        }
                                    }
                                }
                                #endregion

                                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                obj.ContractTemplateInstanceID = ContractTemplateID;
                                obj.CreatedOn = DateTime.Now;
                                obj.CreatedBy = UserID;
                                obj.ContractTemplateContent = item.TemplateContent;
                                obj.SectionID = item.sectionID;
                                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                                if (updatestatus)
                                {
                                    obj.Status = 4;
                                    ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                                       where row.ContractTemplateInstanceID == ContractTemplateID
                                                                       && row.RoleID == RoleID
                                                                       && row.ApproveOrder == nextorderId
                                                                       && row.IsActive == false
                                                                       && row.SectionID == SecId
                                                                       select row).FirstOrDefault();
                                    if (data != null)
                                    {
                                        flagcheck = true;
                                        data.IsActive = true;
                                    }
                                }
                                else
                                    obj.Status = 5;

                                obj.OrderID = 1;
                                obj.Version = ContractVersion;
                                entities.ContractTemplateTransactions.Add(obj);
                                entities.SaveChanges();

                                int TransactionID = Convert.ToInt32(obj.Id);
                                if (TransactionID > 0)
                                {
                                    #region set transaction history

                                    var dataHistory = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                       where row.UserID == UserID
                                                       && row.ContractTemplateInstanceID == ContractTemplateID
                                                       && row.RoleID == RoleID
                                                       select row).ToList();
                                    if (dataHistory.Count > 0)
                                    {
                                        var getdata = (from row in dataHistory
                                                       join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                       on row.sectionID equals row1.SectionID
                                                       where row1.ContractTemplateInstanceID == ContractTemplateID
                                                       select new getTemplateTransactionDetail
                                                       {
                                                           TemplateTransactionID = row.TemplateTransactionID,
                                                           TemplateInstanceID = row.ContractTemplateInstanceID,
                                                           ComplianceStatusID = row.ComplianceStatusID,
                                                           CreatedOn = row.CreatedOn,
                                                           Status = row.Status,
                                                           StatusChangedOn = row.StatusChangedOn,
                                                           TemplateContent = row.ContractTemplateContent,
                                                           RoleID = row.RoleID,
                                                           UserID = row.UserID,
                                                           sectionID = Convert.ToInt32(row.sectionID),
                                                           orderID = Convert.ToInt32(row1.SectionOrder),
                                                           visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                           sectionHeader = row.SectionHeader
                                                       }).OrderBy(x => x.orderID).ToList();

                                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                        getdata.ForEach(eachSection =>
                                        {
                                            if (eachSection.visibilty == 1)
                                            {
                                                strExporttoWord.Append(@"<div>");
                                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                                strExporttoWord.Append(@"</div>");
                                            }

                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                            strExporttoWord.Append(@"</div>");
                                        });

                                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                        obj1.ContractTemplateID = ContractTemplateID;
                                        obj1.CreatedOn = DateTime.Now;
                                        obj1.CreatedBy = AuthenticationHelper.UserID;
                                        obj1.TemplateContent = strExporttoWord.ToString();
                                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                                        if (updatestatus)
                                            obj1.Status = 4;
                                        else
                                            obj1.Status = 5;

                                        obj1.OrderID = 1;
                                        entities.ContractTemplateTransactionHistories.Add(obj1);
                                        entities.SaveChanges();
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    if (flagcheck == false)
                    {
                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                        {
                            CustomerID = customerID,
                            ContractID = Convert.ToInt32(ContractTemplateID),
                            StatusID = 5,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = UserID,
                            UpdatedBy = UserID,
                        };
                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
            }
        }

        protected void btnRejcted_Click(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);

                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where row.UserID == UserID
                             && row.ContractTemplateInstanceID == ContractTemplateID
                             && row.RoleID == RoleID
                             select row).ToList();

                if (data1.Count > 0)
                {
                    var datasectiondetail = (from row in data1
                                             join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                             on row.sectionID equals row1.SectionID
                                             where row1.ContractTemplateInstanceID == ContractTemplateID
                                             select new getTemplateTransactionDetail
                                             {
                                                 TemplateTransactionID = row.TemplateTransactionID,
                                                 TemplateInstanceID = row.ContractTemplateInstanceID,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 CreatedOn = row.CreatedOn,
                                                 Status = row.Status,
                                                 StatusChangedOn = row.StatusChangedOn,
                                                 TemplateContent = row.ContractTemplateContent,
                                                 RoleID = row.RoleID,
                                                 UserID = row.UserID,
                                                 sectionID = Convert.ToInt32(row.sectionID),
                                                 orderID = Convert.ToInt32(row1.SectionOrder),
                                                 visiblityID = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.sectionID, RoleID)
                                             }).OrderBy(x => x.orderID).ToList();

                    if (datasectiondetail.Count > 0)
                    {
                        datasectiondetail = datasectiondetail.Where(x => x.visiblityID == 1).ToList();
                    }
                    foreach (var item in datasectiondetail)
                    {
                        if (item.ComplianceStatusID == 4)
                        {
                            ContractTemplateTransaction obj = new ContractTemplateTransaction();
                            obj.ContractTemplateInstanceID = ContractTemplateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = UserID;
                            obj.ContractTemplateContent = item.TemplateContent;
                            obj.SectionID = item.sectionID;
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            obj.Status = 15;
                            obj.OrderID = 1;
                            obj.Version = ContractVersion;
                            entities.ContractTemplateTransactions.Add(obj);
                            entities.SaveChanges();

                            int TransactionID = Convert.ToInt32(obj.Id);
                            if (TransactionID > 0)
                            {
                                #region set transaction history

                                var dataHistory = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                   where row.UserID == UserID
                                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                                   && row.RoleID == RoleID
                                                   select row).ToList();
                                if (dataHistory.Count > 0)
                                {
                                    var getdata = (from row in dataHistory
                                                   join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                   on row.sectionID equals row1.SectionID
                                                   where row1.ContractTemplateInstanceID == ContractTemplateID
                                                   select new getTemplateTransactionDetail
                                                   {
                                                       TemplateTransactionID = row.TemplateTransactionID,
                                                       TemplateInstanceID = row.ContractTemplateInstanceID,
                                                       ComplianceStatusID = row.ComplianceStatusID,
                                                       CreatedOn = row.CreatedOn,
                                                       Status = row.Status,
                                                       StatusChangedOn = row.StatusChangedOn,
                                                       TemplateContent = row.ContractTemplateContent,
                                                       RoleID = row.RoleID,
                                                       UserID = row.UserID,
                                                       sectionID = Convert.ToInt32(row.sectionID),
                                                       orderID = Convert.ToInt32(row1.SectionOrder),
                                                       visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                       sectionHeader = row.SectionHeader
                                                   }).OrderBy(x => x.orderID).ToList();

                                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                    getdata.ForEach(eachSection =>
                                    {
                                        if (eachSection.visibilty == 1)
                                        {
                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                            strExporttoWord.Append(@"</div>");
                                        }

                                        strExporttoWord.Append(@"<div>");
                                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                        strExporttoWord.Append(@"</div>");
                                    });

                                    ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                    obj1.ContractTemplateID = ContractTemplateID;
                                    obj1.CreatedOn = DateTime.Now;
                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                    obj1.TemplateContent = strExporttoWord.ToString();
                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                    obj1.Status = 15;
                                    obj1.OrderID = 1;
                                    entities.ContractTemplateTransactionHistories.Add(obj1);
                                    entities.SaveChanges();
                                }
                                #endregion
                            }
                        }
                    }
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = customerID,
                        ContractID = Convert.ToInt32(ContractTemplateID),
                        StatusID = 15,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
            }
        }

        protected void btnApproverPrimary_Click(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);

                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where row.UserID == UserID
                             && row.ContractTemplateInstanceID == ContractTemplateID
                             && row.RoleID == RoleID
                             select row).ToList();

                if (data1.Count > 0)
                {
                    var datasectiondetail = (from row in data1
                                             join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                             on row.sectionID equals row1.SectionID
                                             where row1.ContractTemplateInstanceID == ContractTemplateID
                                             select new getTemplateTransactionDetail
                                             {
                                                 TemplateTransactionID = row.TemplateTransactionID,
                                                 TemplateInstanceID = row.ContractTemplateInstanceID,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 CreatedOn = row.CreatedOn,
                                                 Status = row.Status,
                                                 StatusChangedOn = row.StatusChangedOn,
                                                 TemplateContent = row.ContractTemplateContent,
                                                 RoleID = row.RoleID,
                                                 UserID = row.UserID,
                                                 sectionID = Convert.ToInt32(row.sectionID),
                                                 orderID = Convert.ToInt32(row1.SectionOrder),
                                                 visiblityID = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.sectionID, RoleID)
                                             }).OrderBy(x => x.orderID).ToList();

                    if (datasectiondetail.Count > 0)
                    {
                        datasectiondetail = datasectiondetail.Where(x => x.visiblityID == 1).ToList();
                    }
                    foreach (var item in datasectiondetail)
                    {
                        if (item.ComplianceStatusID == 18)
                        {
                            ContractTemplateTransaction obj = new ContractTemplateTransaction();
                            obj.ContractTemplateInstanceID = ContractTemplateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = UserID;
                            obj.ContractTemplateContent = item.TemplateContent;
                            obj.SectionID = item.sectionID;
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            obj.Status = 4;
                            obj.Version = ContractVersion;
                            obj.OrderID = 1;
                            entities.ContractTemplateTransactions.Add(obj);
                            entities.SaveChanges();

                            int TransactionID = Convert.ToInt32(obj.Id);
                            if (TransactionID > 0)
                            {
                                #region set transaction history

                                var dataHistory = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                   where row.UserID == UserID
                                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                                   && row.RoleID == RoleID
                                                   select row).ToList();
                                if (dataHistory.Count > 0)
                                {
                                    var getdata = (from row in dataHistory
                                                   join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                   on row.sectionID equals row1.SectionID
                                                   where row1.ContractTemplateInstanceID == ContractTemplateID
                                                   select new getTemplateTransactionDetail
                                                   {
                                                       TemplateTransactionID = row.TemplateTransactionID,
                                                       TemplateInstanceID = row.ContractTemplateInstanceID,
                                                       ComplianceStatusID = row.ComplianceStatusID,
                                                       CreatedOn = row.CreatedOn,
                                                       Status = row.Status,
                                                       StatusChangedOn = row.StatusChangedOn,
                                                       TemplateContent = row.ContractTemplateContent,
                                                       RoleID = row.RoleID,
                                                       UserID = row.UserID,
                                                       sectionID = Convert.ToInt32(row.sectionID),
                                                       orderID = Convert.ToInt32(row1.SectionOrder),
                                                       visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                       sectionHeader = row.SectionHeader
                                                   }).OrderBy(x => x.orderID).ToList();

                                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                    getdata.ForEach(eachSection =>
                                    {
                                        if (eachSection.visibilty == 1)
                                        {
                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                            strExporttoWord.Append(@"</div>");
                                        }

                                        strExporttoWord.Append(@"<div>");
                                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                        strExporttoWord.Append(@"</div>");
                                    });

                                    ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                    obj1.ContractTemplateID = ContractTemplateID;
                                    obj1.CreatedOn = DateTime.Now;
                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                    obj1.TemplateContent = strExporttoWord.ToString();
                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                    obj1.Status = 4;
                                    obj1.OrderID = 1;
                                    entities.ContractTemplateTransactionHistories.Add(obj1);
                                    entities.SaveChanges();
                                }
                                #endregion
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
            }
        }

        protected void btnRejctedPrimary_Click(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);

                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where row.UserID == UserID
                             && row.ContractTemplateInstanceID == ContractTemplateID
                             && row.RoleID == RoleID
                             select row).ToList();

                if (data1.Count > 0)
                {
                    var datasectiondetail = (from row in data1
                                             join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                             on row.sectionID equals row1.SectionID
                                             where row1.ContractTemplateInstanceID == ContractTemplateID
                                             select new getTemplateTransactionDetail
                                             {
                                                 TemplateTransactionID = row.TemplateTransactionID,
                                                 TemplateInstanceID = row.ContractTemplateInstanceID,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 CreatedOn = row.CreatedOn,
                                                 Status = row.Status,
                                                 StatusChangedOn = row.StatusChangedOn,
                                                 TemplateContent = row.ContractTemplateContent,
                                                 RoleID = row.RoleID,
                                                 UserID = row.UserID,
                                                 sectionID = Convert.ToInt32(row.sectionID),
                                                 orderID = Convert.ToInt32(row1.SectionOrder),
                                                 visiblityID = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.sectionID, RoleID)
                                             }).OrderBy(x => x.orderID).ToList();

                    if (datasectiondetail.Count > 0)
                    {
                        datasectiondetail = datasectiondetail.Where(x => x.visiblityID == 1).ToList();
                    }
                    foreach (var item in datasectiondetail)
                    {
                        if (item.ComplianceStatusID == 18)
                        {
                            ContractTemplateTransaction obj = new ContractTemplateTransaction();
                            obj.ContractTemplateInstanceID = ContractTemplateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = UserID;
                            obj.ContractTemplateContent = item.TemplateContent;
                            obj.SectionID = item.sectionID;
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            obj.Status = 15;
                            obj.Version = ContractVersion;
                            obj.OrderID = 1;
                            entities.ContractTemplateTransactions.Add(obj);
                            entities.SaveChanges();

                            int TransactionID = Convert.ToInt32(obj.Id);
                            if (TransactionID > 0)
                            {
                                #region set transaction history

                                var dataHistory = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                   where row.UserID == UserID
                                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                                   && row.RoleID == RoleID
                                                   select row).ToList();
                                if (dataHistory.Count > 0)
                                {
                                    var getdata = (from row in dataHistory
                                                   join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                   on row.sectionID equals row1.SectionID
                                                   where row1.ContractTemplateInstanceID == ContractTemplateID
                                                   select new getTemplateTransactionDetail
                                                   {
                                                       TemplateTransactionID = row.TemplateTransactionID,
                                                       TemplateInstanceID = row.ContractTemplateInstanceID,
                                                       ComplianceStatusID = row.ComplianceStatusID,
                                                       CreatedOn = row.CreatedOn,
                                                       Status = row.Status,
                                                       StatusChangedOn = row.StatusChangedOn,
                                                       TemplateContent = row.ContractTemplateContent,
                                                       RoleID = row.RoleID,
                                                       UserID = row.UserID,
                                                       sectionID = Convert.ToInt32(row.sectionID),
                                                       orderID = Convert.ToInt32(row1.SectionOrder),
                                                       visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                       sectionHeader = row.SectionHeader
                                                   }).OrderBy(x => x.orderID).ToList();

                                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                    getdata.ForEach(eachSection =>
                                    {
                                        if (eachSection.visibilty == 1)
                                        {
                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                            strExporttoWord.Append(@"</div>");
                                        }

                                        strExporttoWord.Append(@"<div>");
                                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                        strExporttoWord.Append(@"</div>");
                                    });

                                    ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                    obj1.ContractTemplateID = ContractTemplateID;
                                    obj1.CreatedOn = DateTime.Now;
                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                    obj1.TemplateContent = strExporttoWord.ToString();
                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                    obj1.Status = 15;
                                    obj1.OrderID = 1;
                                    entities.ContractTemplateTransactionHistories.Add(obj1);
                                    entities.SaveChanges();
                                }
                                #endregion
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
            }
        }

        protected void btnreviewerreject_Click(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractTID"]);

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where row.UserID == UserID
                             && row.ContractTemplateInstanceID == ContractTemplateID
                             && row.RoleID == RoleID
                             select row).ToList();

                if (data1.Count > 0)
                {
                    var datasectiondetail = (from row in data1
                                             join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                             on row.sectionID equals row1.SectionID
                                             where row1.ContractTemplateInstanceID == ContractTemplateID
                                             select new getTemplateTransactionDetail
                                             {
                                                 TemplateTransactionID = row.TemplateTransactionID,
                                                 TemplateInstanceID = row.ContractTemplateInstanceID,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 CreatedOn = row.CreatedOn,
                                                 Status = row.Status,
                                                 StatusChangedOn = row.StatusChangedOn,
                                                 TemplateContent = row.ContractTemplateContent,
                                                 RoleID = row.RoleID,
                                                 UserID = row.UserID,
                                                 sectionID = Convert.ToInt32(row.sectionID),
                                                 orderID = Convert.ToInt32(row1.SectionOrder),
                                                 visiblityID = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.sectionID, RoleID)
                                             }).OrderBy(x => x.orderID).ToList();

                    if (datasectiondetail.Count > 0)
                    {
                        datasectiondetail = datasectiondetail.Where(x => x.visiblityID == 1).ToList();
                    }
                    foreach (var item in datasectiondetail)
                    {
                        if (item.ComplianceStatusID == 2)
                        {
                            ContractTemplateTransaction obj = new ContractTemplateTransaction();
                            obj.ContractTemplateInstanceID = ContractTemplateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = UserID;
                            obj.ContractTemplateContent = item.TemplateContent;
                            obj.SectionID = item.sectionID;
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            obj.Status = 15;
                            obj.OrderID = 1;
                            obj.Version = ContractVersion;
                            entities.ContractTemplateTransactions.Add(obj);
                            entities.SaveChanges();

                            int TransactionID = Convert.ToInt32(obj.Id);
                            if (TransactionID > 0)
                            {
                                #region set transaction history

                                var dataHistory = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                   where row.UserID == UserID
                                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                                   && row.RoleID == RoleID
                                                   select row).ToList();
                                if (dataHistory.Count > 0)
                                {
                                    var getdata = (from row in dataHistory
                                                   join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                   on row.sectionID equals row1.SectionID
                                                   where row1.ContractTemplateInstanceID == ContractTemplateID
                                                   select new getTemplateTransactionDetail
                                                   {
                                                       TemplateTransactionID = row.TemplateTransactionID,
                                                       TemplateInstanceID = row.ContractTemplateInstanceID,
                                                       ComplianceStatusID = row.ComplianceStatusID,
                                                       CreatedOn = row.CreatedOn,
                                                       Status = row.Status,
                                                       StatusChangedOn = row.StatusChangedOn,
                                                       TemplateContent = row.ContractTemplateContent,
                                                       RoleID = row.RoleID,
                                                       UserID = row.UserID,
                                                       sectionID = Convert.ToInt32(row.sectionID),
                                                       orderID = Convert.ToInt32(row1.SectionOrder),
                                                       visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                       sectionHeader = row.SectionHeader
                                                   }).OrderBy(x => x.orderID).ToList();

                                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                    getdata.ForEach(eachSection =>
                                    {
                                        if (eachSection.visibilty == 1)
                                        {
                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                            strExporttoWord.Append(@"</div>");
                                        }

                                        strExporttoWord.Append(@"<div>");
                                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                        strExporttoWord.Append(@"</div>");
                                    });

                                    ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                    obj1.ContractTemplateID = ContractTemplateID;
                                    obj1.CreatedOn = DateTime.Now;
                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                    obj1.TemplateContent = strExporttoWord.ToString();
                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                    obj1.Status = 15;
                                    obj1.OrderID = 1;
                                    entities.ContractTemplateTransactionHistories.Add(obj1);
                                    entities.SaveChanges();
                                }
                                #endregion
                            }
                        }
                    }
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = customerID,
                        ContractID = Convert.ToInt32(ContractTemplateID),
                        StatusID = 15,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
            }
        }
    }
}