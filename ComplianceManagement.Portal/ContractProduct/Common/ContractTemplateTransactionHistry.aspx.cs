﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class ContractTemplateTransactionHistry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                BindTransactionHistory(ContractTemplateID);
            }
        }

        public void BindTransactionHistory(long TemplateInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var entitiesData = (from row in entities.ContractTemplateTransactionHistories
                                    where row.ContractTemplateID == TemplateInstanceID
                                    select row).ToList();

                rptComplianceVersionView.DataSource = entitiesData.OrderByDescending(entry => entry.CreatedOn);
                rptComplianceVersionView.DataBind();
            }
        }
        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    theEditor.Content = "";
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    int TransactionID = Convert.ToInt32(commandArg[0]);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                        
                        var entitiesData = (from row in entities.ContractTemplateTransactionHistories
                                            where row.ContractTemplateID == TemplateID
                                            && row.ID == TransactionID
                                            select row).FirstOrDefault();
                        if (entitiesData != null)
                        {
                            theEditor.Content = entitiesData.TemplateContent;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}