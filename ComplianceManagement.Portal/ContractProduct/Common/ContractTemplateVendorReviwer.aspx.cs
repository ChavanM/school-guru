﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class ContractTemplateVendorReviwer : System.Web.UI.Page
    {
        protected static int ContractTemplateId;
        protected static int CustId;
        protected static int UId;
        protected static int RoleId;
        protected static int SectionId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int CustomerID = Convert.ToInt32(Request.QueryString["CustID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractID"]);
                ViewState["CustomerID"] = CustomerID;
                SetDefaultConfiguratorValues(UserID, RoleID, ContractTemplateID, CustomerID);
                BindTransactionHistory(ContractTemplateID, RoleID, CustomerID, UserID);
                ShowContractDetails(ContractTemplateID);
            }
        }
               
        public int getRoleID(int ContractTemplateId, int UserID, int sectionID)
        {
            int RoleID = -1;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                   where row.ContractTemplateInstanceID == ContractTemplateId
                                                   && row.UserID == UserID
                                                   && row.SectionID == sectionID
                                                   && row.IsActive == true
                                                   select row).FirstOrDefault();
                if (data != null)
                {
                    RoleID = Convert.ToInt32(data.RoleID);
                }
            }
            return RoleID;
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int RolegetVIRID = getRID("VIR");//Vendor Internal Reviewer
                int RolegetVRID = getRID("VR");//Vendor Reviewer
                if (e.CommandName.Equals("View"))
                {
                    SectionId = Convert.ToInt32(commandArgs[0]);
                    int StatusId = Convert.ToInt32(commandArgs[1]);
                    ContractTemplateId = Convert.ToInt32(Request.QueryString["ContractID"]);
                    RoleId = Convert.ToInt32(Request.QueryString["RID"]);
                    UId = Convert.ToInt32(Request.QueryString["UID"]);
                    CustId = Convert.ToInt32(Request.QueryString["CustID"]);

                    int uid = UId;
                    int cid = CustId;
                    int rid = RoleId;
                    int tid = ContractTemplateId;
                    int sid = SectionId;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var entitiesData = (from row in entities.ContractTemplateAssignments
                                            where row.ContractTemplateInstanceID == ContractTemplateId
                                              && row.UserID == UId
                                              && row.IsActive == true
                                              && row.SectionID == SectionId
                                            select row).FirstOrDefault();
                        if (entitiesData != null)
                        {
                            int RID = Convert.ToInt32(entitiesData.RoleID);

                            if (RID == RolegetVRID)
                            {
                                if (StatusId == 1 || StatusId == 3 || StatusId == 16 || StatusId == 21 || StatusId == 22)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal('" + uid + "','" + tid + "','" + rid + "','" + cid + "','" + sid + "');", true);
                                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                            if (RID == RolegetVIRID)
                            {
                                if (StatusId == 20)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal('" + uid + "','" + tid + "','" + rid + "','" + cid + "','" + sid + "');", true);
                                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                            //if (RID == 4)
                            //{
                            //    if (StatusId == 20)
                            //    {
                            //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal('" + uid + "','" + tid + "','" + rid + "','" + cid + "','" + sid + "');", true);
                            //        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                            //    }
                            //}
                            //if (RID == 5)
                            //{
                            //    if (StatusId == 16)
                            //    {
                            //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal('" + uid + "','" + tid + "','" + rid + "','" + cid + "','" + sid + "');", true);
                            //        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                            //    }
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    var branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    if (branchs.Count > 0)
                    {
                        branch = branchs[0];
                    }
                    tbxBranch.Text = "Select Entity/Location";
                    List<System.Web.UI.WebControls.TreeNode> nodes = new List<System.Web.UI.WebControls.TreeNode>();
                    BindBranchesHierarchy(null, branch, nodes);
                    foreach (System.Web.UI.WebControls.TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }

                    tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }

        private void BindBranchesHierarchy(System.Web.UI.WebControls.TreeNode parent, NameValueHierarchy nvp, List<System.Web.UI.WebControls.TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        System.Web.UI.WebControls.TreeNode node = new System.Web.UI.WebControls.TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                
            }
        }

        public void BindDepartments()
        {
            int customerID = -1;
            if (ViewState["CustomerID"] != null)
            {
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                customerID = Convert.ToInt32(ViewState["CustomerID"]);

                var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";

                ddlDepartment.DataSource = obj;
                ddlDepartment.DataBind();
            }
        }

        public void BindVendors()
        {
            int customerID = -1;

            if (ViewState["CustomerID"] != null)
            {
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                customerID = Convert.ToInt32(ViewState["CustomerID"]);

                var lstVendors = VendorDetails.GetVendors_All(customerID);

                //Drop-Down at Modal Pop-up
                lstBoxVendor.DataTextField = "VendorName";
                lstBoxVendor.DataValueField = "ID";

                lstBoxVendor.DataSource = lstVendors;
                lstBoxVendor.DataBind();

                lstBoxVendor.Items.Add(new ListItem("Add New", "0"));
            }
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;

                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                    if (lstAllUsers.Count > 0)
                        lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                    var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                    lstBoxOwner.DataValueField = "ID";
                    lstBoxOwner.DataTextField = "Name";
                    lstBoxOwner.DataSource = internalUsers;
                    lstBoxOwner.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                    ddlContractType.DataTextField = "TypeName";
                    ddlContractType.DataValueField = "ID";

                    ddlContractType.DataSource = lstContractTypes;
                    ddlContractType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearContractControls()
        {
            try
            {
                txtContractNo.Text = "";
                txtTitle.Text = "";
                tbxDescription.Text = "";

                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                lstBoxVendor.ClearSelection();
                ddlDepartment.ClearSelection();

                txtProposalDate.Text = "";
                txtAgreementDate.Text = "";
                txtEffectiveDate.Text = "";

                txtReviewDate.Text = "";
                txtExpirationDate.Text = "";
                txtNoticeTerm.Text = "";

                ddlContractType.ClearSelection();
                ddlContractSubType.ClearSelection();
                lstBoxOwner.ClearSelection();

                tbxContractAmt.Text = "";
                ddlPaymentTerm.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableContractSummaryTabControls(bool flag)
        {
            try
            {
                ddlContractStatus.Enabled = flag;

                txtContractNo.Enabled = flag;
                txtTitle.Enabled = flag;
                tbxDescription.Enabled = flag;

                tbxBranch.Enabled = flag;
                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;

                txtProposalDate.Enabled = flag;
                txtAgreementDate.Enabled = flag;
                txtEffectiveDate.Enabled = flag;

                txtReviewDate.Enabled = flag;
                txtExpirationDate.Enabled = flag;

                txtNoticeTerm.Enabled = flag;
                ddlNoticeTerm.Enabled = flag;

                tbxContractAmt.Enabled = flag;
                ddlPaymentTerm.Enabled = flag;

                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;

                ddlContractType.Enabled = flag;
                ddlContractSubType.Enabled = flag;
                lstBoxOwner.Enabled = flag;

                txtBoxProduct.Enabled = flag;

                if (flag)
                {
                    ddlDepartment.Attributes.Remove("disabled");
                    ddlNoticeTerm.Attributes.Remove("disabled");
                    ddlContractType.Attributes.Remove("disabled");
                    ddlContractSubType.Attributes.Remove("disabled");

                    lstBoxOwner.Attributes.Remove("disabled");
                    lstBoxVendor.Attributes.Remove("disabled");
                }
                else
                {
                    ddlDepartment.Attributes.Add("disabled", "disabled");
                    ddlNoticeTerm.Attributes.Add("disabled", "disabled");
                    ddlContractType.Attributes.Add("disabled", "disabled");
                    ddlContractSubType.Attributes.Add("disabled", "disabled");
                    lstBoxOwner.Attributes.Add("disabled", "disabled");
                    lstBoxVendor.Attributes.Add("disabled", "disabled");
                }

                if (grdCustomField != null)
                {
                    grdCustomField.Enabled = flag;
                    HideShowGridColumns(grdCustomField, "Action", flag);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void HideShowGridColumns(GridView gridView, string headerTextToMatch, bool flag)
        {
            try
            {
                if (gridView == null)
                {
                    return;
                }

                // Loop through all of the columns in the grid.
                for (int i = 0; i < gridView.Columns.Count; i++)
                {
                    String headerText = gridView.Columns[i].HeaderText;

                    //Show Hide Columns with Specific headerText
                    if (headerText == headerTextToMatch)
                        gridView.Columns[i].Visible = flag;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ShowContractDetails(long contractID)
        {
            try
            {
                int customerID = -1;

                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    VendorDetails.BindVendors(lstBoxVendor, customerID);

                    BindCustomerBranches();
                    BindDepartments();
                    BindVendors();
                    BindUsers();
                    BindContractCategoryType();

                    BindContractStatusList(false, true);

                    clearContractControls();
                    enableDisableContractSummaryTabControls(false);

                    if (contractID != 0)
                    {
                        var contractRecord = ContractManagement.GetContractDetailsByContractID(customerID, contractID);
                        //var contractRecord = ContractManagement.GetContractByID(contractID);

                        if (contractRecord != null)
                        {
                            ddlContractStatus.ClearSelection();

                            if (ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()) != null)
                                ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()).Selected = true;

                            if (ddlContractStatus.SelectedValue != "" && ddlContractStatus.SelectedValue != "-1")
                            {
                                txtboxMilestoneStatus.Text = ddlContractStatus.SelectedItem.ToString();
                            }
                            
                            txtContractNo.Text = contractRecord.ContractNo;
                            txtTitle.Text = contractRecord.ContractTitle;
                            tbxDescription.Text = contractRecord.ContractDetailDesc;

                            if (contractRecord.CustomerBranchID != 0)
                            {
                                foreach (System.Web.UI.WebControls.TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == contractRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (System.Web.UI.WebControls.TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == contractRecord.CustomerBranchID.ToString())
                                            item1.Selected = true;
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            tbxBranchlbl.Text = tbxBranch.Text;

                            #region Vendor

                            var lstVendorMapping = VendorDetails.GetVendorMapping(contractID);

                            if (lstBoxVendor.Items.Count > 0)
                            {
                                lstBoxVendor.ClearSelection();
                            }

                            if (lstVendorMapping.Count > 0)
                            {
                                foreach (var VendorID in lstVendorMapping)
                                {
                                    if (lstBoxVendor.Items.FindByValue(VendorID.ToString()) != null)
                                        lstBoxVendor.Items.FindByValue(VendorID.ToString()).Selected = true;
                                }
                            }

                            #endregion

                            LabelVendor.Text = lstBoxVendor.SelectedItem.Text;
                            
                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(contractRecord.DepartmentID.ToString()) != null)
                            {
                                ddlDepartment.SelectedValue = contractRecord.DepartmentID.ToString();
                                lblDepartment.Text = ddlDepartment.SelectedItem.ToString();
                            }
                            if (contractRecord.ProposalDate != null)
                                txtProposalDate.Text = Convert.ToDateTime(contractRecord.ProposalDate).ToString("dd-MM-yyyy");
                            else
                                txtProposalDate.Text = string.Empty;

                            if (contractRecord.AgreementDate != null)
                                txtAgreementDate.Text = Convert.ToDateTime(contractRecord.AgreementDate).ToString("dd-MM-yyyy");
                            else
                                txtAgreementDate.Text = string.Empty;

                            if (contractRecord.EffectiveDate != null)
                                txtEffectiveDate.Text = Convert.ToDateTime(contractRecord.EffectiveDate).ToString("dd-MM-yyyy");
                            else
                                txtEffectiveDate.Text = string.Empty;

                            if (contractRecord.ReviewDate != null)
                                txtReviewDate.Text = Convert.ToDateTime(contractRecord.ReviewDate).ToString("dd-MM-yyyy");
                            else
                                txtReviewDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtExpirationDate.Text = Convert.ToDateTime(contractRecord.ExpirationDate).ToString("dd-MM-yyyy");
                            else
                                txtExpirationDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtNoticeTerm.Text = Convert.ToString(contractRecord.NoticeTermNumber);
                            else
                                txtNoticeTerm.Text = string.Empty;

                            if (ddlNoticeTerm.Items.FindByValue(contractRecord.NoticeTermType.ToString()) != null)
                                ddlNoticeTerm.SelectedValue = contractRecord.NoticeTermType.ToString();

                            ddlContractType.ClearSelection();

                            if (ddlContractType.Items.FindByValue(contractRecord.ContractTypeID.ToString()) != null)
                                ddlContractType.SelectedValue = contractRecord.ContractTypeID.ToString();
                            
                            if (contractRecord.ContractSubTypeID != null)
                            {
                                ddlContractSubType.ClearSelection();

                                if (ddlContractSubType.Items.FindByValue(contractRecord.ContractSubTypeID.ToString()) != null)
                                    ddlContractSubType.SelectedValue = contractRecord.ContractSubTypeID.ToString();
                            }

                            #region Owner && Approvers                            

                            var lstContractUserAssignment = ContractManagement.GetContractUserAssignment_All(contractID);

                            if (lstContractUserAssignment.Count > 0)
                            {
                                lstBoxOwner.ClearSelection();
                                lstBoxApprover.ClearSelection();

                                foreach (var eachAssignmentRecord in lstContractUserAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 6)
                                    {
                                        if (lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }
                            #endregion

                            if (!string.IsNullOrEmpty(tbxContractAmt.Text))
                                contractRecord.ContractAmt = Convert.ToDecimal(tbxContractAmt.Text);
                            else
                                contractRecord.ContractAmt = 0;

                            if (contractRecord.PaymentTermID != null)
                            {
                                ddlPaymentTerm.ClearSelection();

                                if (ddlPaymentTerm.Items.FindByValue(contractRecord.PaymentTermID.ToString()) != null)
                                    ddlPaymentTerm.SelectedValue = contractRecord.PaymentTermID.ToString();
                            }

                            if (!string.IsNullOrEmpty(txtBoxProduct.Text))
                                contractRecord.ProductItems = Convert.ToString(txtBoxProduct.Text.Trim());

                        }

                        //BindContractDocuments_Paging();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
              
            }
        }
        public class ContractSectionDetail
        {
            public string Header { get; set; }
            public long ContractTemplateInstanceID { get; set; }
            public long SectionID { get; set; }
            public Nullable<int> SectionOrder { get; set; }
            public int CreatedBy { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string DeptName { get; set; }
            public string DeptColor { get; set; }
            public string SectionStatusID { get; set; }
            public string SectionStatus { get; set; }
            public string SectionReviwerName { get; set; }
            public int Visbilty { get; set; }
        }

        public void BindTransactionHistory(long TemplateInstanceID, long RoleID,int CustomerID,int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RolegetVRID = getRID("VR");//Vendor Reviewer
                int RolegetVIRID = getRID("VIR");//Vendor Internal Reviewer
                btnReview.Enabled = false;
                btnReject.Enabled = false;
                btnReviwer.Enabled = false;

                var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(CustomerID, Convert.ToInt32(TemplateInstanceID))
                                    select row).ToList();

                if (entitiesData != null)
                {
                    var details = (from row in entitiesData
                                   select new ContractSectionDetail()
                                   {
                                       Header = row.Header,
                                       ContractTemplateInstanceID = row.ContractTemplateInstanceID,
                                       SectionID = row.SectionID,
                                       SectionOrder = row.SectionOrder,
                                       CreatedBy = row.CreatedBy,
                                       CreatedOn = row.CreatedOn,
                                       DeptName = row.DeptName,
                                       DeptColor = row.DeptColor,
                                       SectionStatusID = row.SectionStatusID,
                                       SectionStatus = row.SectionStatus,
                                       SectionReviwerName = row.SectionReviwerName,
                                       Visbilty = geVisiblityStatus(row.ContractTemplateInstanceID, UserID, row.SectionID, RoleID)
                                   }).ToList();

                    if (details.Count > 0)
                    {
                        details = details.Where(x => x.Visbilty == 1).ToList();
                    }
                    if (details.Count > 0)
                    {
                        if (RoleID == RolegetVRID)
                        {
                            int flag = details.Where(x => x.SectionStatusID == "16" || x.SectionStatusID == "21" || x.SectionStatusID == "22").ToList().Count;
                            if (flag > 0)
                            {
                                btnReview.Enabled = true;
                                btnReject.Enabled = true;
                                btnReviwer.Enabled = true;
                            }
                        }
                        if (RoleID == RolegetVIRID)
                        {
                            int flag = details.Where(x => x.SectionStatusID == "20").ToList().Count;
                            if (flag > 0)
                            {
                                btnReview.Enabled = true;
                                btnReject.Enabled = true;
                                btnReviwer.Enabled = false;
                            }
                        }
                        //int flag = details.Where(x => x.SectionStatusID == "16").ToList().Count;
                        //if (flag > 0)
                        //{
                        //    btnReview.Enabled = true;
                        //    btnReject.Enabled = true;
                        //}
                    }
                    rptComplianceVersionView.DataSource = details.OrderBy(entry => entry.SectionOrder).ToList();
                    rptComplianceVersionView.DataBind();
                }
                else
                {
                    rptComplianceVersionView.DataSource = null;
                    rptComplianceVersionView.DataBind();
                }
            }
        }

        public int geVisiblityStatus(long ContractTemplateId, int UserID, long sectionID, long RoleID)
        {
            int OutputVisiblity = 0;
            int RolegetVIRID = getRID("VIR");//Vendor Internal Reviewer
            int RolegetVRID = getRID("VR");//Vendor Reviewer
            if (RoleID == RolegetVRID || RoleID == RolegetVIRID)
            {
                OutputVisiblity = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (ContractTemplateId != null && UserID != null && sectionID != null)
                    {
                        ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                           where row.ContractTemplateInstanceID == ContractTemplateId
                                                           && row.RoleID == RoleID
                                                           && row.SectionID == sectionID
                                                           && row.IsActive == true
                                                           select row).FirstOrDefault();
                        if (data != null)
                        {
                            if (data.UserID == UserID)
                            {
                                OutputVisiblity = 1;
                            }
                            else
                            {
                                //if (data.Visibility == 1)
                                //{
                                //    OutputVisiblity = 1;
                                //}
                            }
                        }
                    }
                }
            }
            return OutputVisiblity;
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }

        private void SetDefaultConfiguratorValues(int UserID, int RoleID, int ContractTemplateID,int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where row.UserID == UserID
                             && row.ContractTemplateInstanceID == ContractTemplateID
                             && row.RoleID == RoleID
                             select row).ToList();

                if (data1.Count > 0)
                {
                    var data = (from row in data1
                                join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                on row.sectionID equals row1.SectionID
                                where row1.ContractTemplateInstanceID == ContractTemplateID
                                select new getTemplateTransactionDetail
                                {
                                    TemplateTransactionID = row.TemplateTransactionID,
                                    TemplateInstanceID = row.ContractTemplateInstanceID,
                                    ComplianceStatusID = row.ComplianceStatusID,
                                    CreatedOn = row.CreatedOn,
                                    Status = row.Status,
                                    StatusChangedOn = row.StatusChangedOn,
                                    TemplateContent = row.ContractTemplateContent,
                                    RoleID = row.RoleID,
                                    UserID = row.UserID,
                                    sectionID = Convert.ToInt32(row.sectionID),
                                    orderID = Convert.ToInt32(row1.SectionOrder),
                                    visibilty = Convert.ToInt32(row1.Headervisibilty),
                                    sectionHeader = row.SectionHeader
                                }).OrderBy(x => x.orderID).ToList();

                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                    data.ForEach(eachSection =>
                    {
                        if (eachSection.visibilty == 1)
                        {
                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                            strExporttoWord.Append(@"</div>");
                        }

                        strExporttoWord.Append(@"<div>");
                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                        strExporttoWord.Append(@"</div>");
                    });

                    theEditor.Content = strExporttoWord.ToString();
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                    theEditor.EnableTrackChanges = false;
                }
                else
                {
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                    theEditor.EnableTrackChanges = false;
                }
            }
        }

        protected void RadEditor1_ExportContent(object sender, EditorExportingArgs e)
        {
            ExportType exportType = e.ExportType;

            if (exportType == ExportType.Word)
            {
                string exportedOutput = e.ExportOutput;

                Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                DocxFormatProvider docxProvider = new DocxFormatProvider();
                RadFlowDocument document = docxProvider.Import(output);

                Header defaultHeader = document.Sections.First().Headers.Add();
                Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                defaultHeaderParagraph.TextAlignment = Alignment.Right;
                defaultHeaderParagraph.Inlines.AddRun("This is a sample header.");

                Footer defaultFooter = document.Sections.First().Footers.Add();
                Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                defaultFooterParagraph.TextAlignment = Alignment.Right;
                defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                Byte[] modifiedOutput = docxProvider.Export(document);
                string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                e.ExportOutput = finalOutput;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            theEditor.ExportToDocx();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            divHtml.InnerHtml = theEditor.Content;
        }

        protected void BtnFullHistory_Click(object sender, EventArgs e)
        {
            int cid = Convert.ToInt32(Request.QueryString["CustID"]);
            int uid = Convert.ToInt32(Request.QueryString["UID"]);
            int rid = Convert.ToInt32(Request.QueryString["RID"]);
            int tid = Convert.ToInt32(Request.QueryString["ContractID"]);

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenHistorySectionModal('" + uid + "','" + tid + "','" + rid + "','" + cid + "');", true);

            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenHistorySectionModal();", true);
        }
        public static string getVendorName(long UserID, long CustomerID, long RoleID,int RolegetVIRID,int RolegetVRID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                string VendorName = string.Empty;
                if (RoleID == RolegetVRID)
                {
                    //Cont_tbl_VendorMaster vendorObj = (from row in entities.Cont_tbl_VendorMaster
                    //                                   where row.ID == UserID
                    //                                        && row.CustomerID == CustomerID
                    //                                        && row.IsDeleted == false
                    //                                   select row).FirstOrDefault();

                    Cont_tbl_VendorMaster vendorObj = VendorDetails.GetVendorMasterDetails(UserID, CustomerID);
                    if (vendorObj != null)
                    {
                        VendorName = vendorObj.VendorName;
                    }
                }
                else if (RoleID == RolegetVIRID)
                {
                    Cont_tbl_SubVendorMaster vendorObj = (from row in entities.Cont_tbl_SubVendorMaster
                                                          where row.ID == UserID
                                                               && row.CustomerID == CustomerID
                                                               && row.IsDeleted == false
                                                          select row).FirstOrDefault();
                    if (vendorObj != null)
                    {
                        VendorName = vendorObj.VendorName;
                    }
                }
                return VendorName;
            }
        }
        protected void btnReview_Click(object sender, EventArgs e)
        {
            int RolegetVIRID = getRID("VIR");//Vendor Internal Reviewer
            int RolegetVRID = getRID("VR");//Vendor Reviewer
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractID"]);
                int CustomerID = Convert.ToInt32(ViewState["CustomerID"]);
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), CustomerID);
                
                string UserName = getVendorName(UserID, CustomerID, RoleID, RolegetVIRID, RolegetVRID);

                //string UserName = getVendorName(UserID, CustomerID, RoleID);

                //string UserName = string.Empty;
                //Cont_tbl_VendorMaster vendorObj = (from row in entities.Cont_tbl_VendorMaster
                //                                   where row.ID == UserID
                //                                        && row.CustomerID == CustomerID
                //                                   select row).FirstOrDefault();
                //if (vendorObj != null)
                //{
                //    UserName = vendorObj.VendorName;
                //}
                if (RoleID == RolegetVIRID)
                {
                    var getdata = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                   where row.UserID == UserID
                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                   && row.RoleID == RoleID
                                   select row).ToList();

                    if (getdata.Count > 0)
                    {
                        var details = (from row in getdata
                                       join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                       where row1.ContractTemplateInstanceID == ContractTemplateID
                                       select new getTemplateTransactionDetail
                                       {
                                           TemplateTransactionID = row.TemplateTransactionID,
                                           TemplateInstanceID = row.ContractTemplateInstanceID,
                                           ComplianceStatusID = row.ComplianceStatusID,
                                           CreatedOn = row.CreatedOn,
                                           Status = row.Status,
                                           StatusChangedOn = row.StatusChangedOn,
                                           TemplateContent = row.ContractTemplateContent,
                                           RoleID = row.RoleID,
                                           UserID = row.UserID,
                                           sectionID = Convert.ToInt32(row.sectionID),
                                           orderID = Convert.ToInt32(row1.SectionOrder)
                                       }).OrderBy(x => x.orderID).ToList();

                        foreach (var item in details)
                        {
                            if (item.ComplianceStatusID == 20)
                            {
                                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                obj.ContractTemplateInstanceID = ContractTemplateID;
                                obj.CreatedOn = DateTime.Now;
                                obj.CreatedBy = UserID;
                                obj.ContractTemplateContent = item.TemplateContent;
                                obj.SectionID = item.sectionID;
                                obj.CreatedByText = UserName;
                                obj.Status = 21;
                                obj.OrderID = 1;
                                obj.Version = ContractVersion;
                                entities.ContractTemplateTransactions.Add(obj);
                                entities.SaveChanges();
                                int TransactionID = Convert.ToInt32(obj.Id);
                                if (TransactionID > 0)
                                {
                                    #region set transaction history

                                    var entitiesData1 = (from row in entities.Cont_SP_GetContractSectionsDetail(CustomerID, Convert.ToInt32(ContractTemplateID))
                                                         select row).ToList();

                                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                 where row.UserID == UserID
                                                 && row.ContractTemplateInstanceID == ContractTemplateID
                                                 && row.RoleID == RoleID
                                                 select row).ToList();
                                    if (data1.Count > 0)
                                    {
                                        var data = (from row in data1
                                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                    on row.sectionID equals row1.SectionID
                                                    where row1.ContractTemplateInstanceID == ContractTemplateID
                                                    select new getTemplateTransactionDetail
                                                    {
                                                        TemplateTransactionID = row.TemplateTransactionID,
                                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                                        ComplianceStatusID = row.ComplianceStatusID,
                                                        CreatedOn = row.CreatedOn,
                                                        Status = row.Status,
                                                        StatusChangedOn = row.StatusChangedOn,
                                                        TemplateContent = row.ContractTemplateContent,
                                                        RoleID = row.RoleID,
                                                        UserID = row.UserID,
                                                        sectionID = Convert.ToInt32(row.sectionID),
                                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                        sectionHeader = row.SectionHeader
                                                    }).OrderBy(x => x.orderID).ToList();

                                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                        data.ForEach(eachSection =>
                                        {
                                            if (eachSection.visibilty == 1)
                                            {
                                                strExporttoWord.Append(@"<div>");
                                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                                strExporttoWord.Append(@"</div>");
                                            }

                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                            strExporttoWord.Append(@"</div>");
                                        });

                                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                        obj1.ContractTemplateID = ContractTemplateID;
                                        obj1.CreatedOn = DateTime.Now;
                                        obj1.CreatedBy = UserID;
                                        obj1.TemplateContent = strExporttoWord.ToString();
                                        obj1.CreatedByText = UserName;
                                        obj1.Status = 21;
                                        obj1.OrderID = 1;
                                        entities.ContractTemplateTransactionHistories.Add(obj1);
                                        entities.SaveChanges();
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = CustomerID,
                        ContractID = Convert.ToInt32(ContractTemplateID),
                        StatusID = 20,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
                }
                if (RoleID == RolegetVRID)
                {
                    var getdata = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                   where row.UserID == UserID
                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                   && row.RoleID == RoleID
                                   select row).ToList();

                    if (getdata.Count > 0)
                    {
                        var details = (from row in getdata
                                       join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                       where row1.ContractTemplateInstanceID == ContractTemplateID
                                       select new getTemplateTransactionDetail
                                       {
                                           TemplateTransactionID = row.TemplateTransactionID,
                                           TemplateInstanceID = row.ContractTemplateInstanceID,
                                           ComplianceStatusID = row.ComplianceStatusID,
                                           CreatedOn = row.CreatedOn,
                                           Status = row.Status,
                                           StatusChangedOn = row.StatusChangedOn,
                                           TemplateContent = row.ContractTemplateContent,
                                           RoleID = row.RoleID,
                                           UserID = row.UserID,
                                           sectionID = Convert.ToInt32(row.sectionID),
                                           orderID = Convert.ToInt32(row1.SectionOrder)
                                       }).OrderBy(x => x.orderID).ToList();

                        foreach (var item in details)
                        {
                            if (item.ComplianceStatusID == 16 || item.ComplianceStatusID == 21 || item.ComplianceStatusID == 22)
                            {
                                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                obj.ContractTemplateInstanceID = ContractTemplateID;
                                obj.CreatedOn = DateTime.Now;
                                obj.CreatedBy = UserID;
                                obj.ContractTemplateContent = item.TemplateContent;
                                obj.SectionID = item.sectionID;
                                obj.CreatedByText = UserName;
                                obj.Status = 17;
                                obj.OrderID = 1;
                                obj.Version = ContractVersion;
                                entities.ContractTemplateTransactions.Add(obj);
                                entities.SaveChanges();
                                int TransactionID = Convert.ToInt32(obj.Id);
                                if (TransactionID > 0)
                                {
                                    #region set transaction history

                                    var entitiesData1 = (from row in entities.Cont_SP_GetContractSectionsDetail(CustomerID, Convert.ToInt32(ContractTemplateID))
                                                         select row).ToList();

                                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                 where row.UserID == UserID
                                                 && row.ContractTemplateInstanceID == ContractTemplateID
                                                 && row.RoleID == RoleID
                                                 select row).ToList();
                                    if (data1.Count > 0)
                                    {
                                        var data = (from row in data1
                                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                    on row.sectionID equals row1.SectionID
                                                    where row1.ContractTemplateInstanceID == ContractTemplateID
                                                    select new getTemplateTransactionDetail
                                                    {
                                                        TemplateTransactionID = row.TemplateTransactionID,
                                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                                        ComplianceStatusID = row.ComplianceStatusID,
                                                        CreatedOn = row.CreatedOn,
                                                        Status = row.Status,
                                                        StatusChangedOn = row.StatusChangedOn,
                                                        TemplateContent = row.ContractTemplateContent,
                                                        RoleID = row.RoleID,
                                                        UserID = row.UserID,
                                                        sectionID = Convert.ToInt32(row.sectionID),
                                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                        sectionHeader = row.SectionHeader
                                                    }).OrderBy(x => x.orderID).ToList();

                                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                        data.ForEach(eachSection =>
                                        {
                                            if (eachSection.visibilty == 1)
                                            {
                                                strExporttoWord.Append(@"<div>");
                                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                                strExporttoWord.Append(@"</div>");
                                            }

                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                            strExporttoWord.Append(@"</div>");
                                        });

                                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                        obj1.ContractTemplateID = ContractTemplateID;
                                        obj1.CreatedOn = DateTime.Now;
                                        obj1.CreatedBy = UserID;
                                        obj1.TemplateContent = strExporttoWord.ToString();
                                        obj1.CreatedByText = UserName;
                                        obj1.Status = 17;
                                        obj1.OrderID = 1;
                                        entities.ContractTemplateTransactionHistories.Add(obj1);
                                        entities.SaveChanges();
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = CustomerID,
                        ContractID = Convert.ToInt32(ContractTemplateID),
                        StatusID = 17,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
                }
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractID"]);
                int CustomerID = Convert.ToInt32(ViewState["CustomerID"]);
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), CustomerID);
                //RolegetVIRID,int RolegetVRID
                int RolegetVIRID = getRID("VIR");//Vendor Internal Reviewer
                int RolegetVRID = getRID("VR");//Vendor Reviewer
                string UserName = getVendorName(UserID, CustomerID, RoleID, RolegetVIRID, RolegetVRID);

                //string UserName = string.Empty;
                //Cont_tbl_VendorMaster vendorObj = (from row in entities.Cont_tbl_VendorMaster
                //                                   where row.ID == UserID
                //                                        && row.CustomerID == CustomerID
                //                                   select row).FirstOrDefault();
                //if (vendorObj != null)
                //{
                //    UserName = vendorObj.VendorName;
                //}
                //int RolegetVIRID = getRID("VIR");//Vendor Internal Reviewer
                if (RoleID == RolegetVIRID)
                {
                    var getdata = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                   where row.UserID == UserID
                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                   && row.RoleID == RoleID
                                   select row).ToList();

                    if (getdata.Count > 0)
                    {
                        var details = (from row in getdata
                                       join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                       where row1.ContractTemplateInstanceID == ContractTemplateID
                                       select new getTemplateTransactionDetail
                                       {
                                           TemplateTransactionID = row.TemplateTransactionID,
                                           TemplateInstanceID = row.ContractTemplateInstanceID,
                                           ComplianceStatusID = row.ComplianceStatusID,
                                           CreatedOn = row.CreatedOn,
                                           Status = row.Status,
                                           StatusChangedOn = row.StatusChangedOn,
                                           TemplateContent = row.ContractTemplateContent,
                                           RoleID = row.RoleID,
                                           UserID = row.UserID,
                                           sectionID = Convert.ToInt32(row.sectionID),
                                           orderID = Convert.ToInt32(row1.SectionOrder)
                                       }).OrderBy(x => x.orderID).ToList();

                        foreach (var item in details)
                        {
                            if (item.ComplianceStatusID == 20)
                            {
                                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                obj.ContractTemplateInstanceID = ContractTemplateID;
                                obj.CreatedOn = DateTime.Now;
                                obj.CreatedBy = UserID;
                                obj.ContractTemplateContent = item.TemplateContent;
                                obj.SectionID = item.sectionID;
                                obj.CreatedByText = UserName;
                                obj.Status = 22;
                                obj.OrderID = 1;
                                obj.Version = ContractVersion;
                                entities.ContractTemplateTransactions.Add(obj);
                                entities.SaveChanges();
                                int TransactionID = Convert.ToInt32(obj.Id);
                                if (TransactionID > 0)
                                {
                                    #region set transaction history

                                    var entitiesData1 = (from row in entities.Cont_SP_GetContractSectionsDetail(CustomerID, Convert.ToInt32(ContractTemplateID))
                                                         select row).ToList();

                                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                 where row.UserID == UserID
                                                 && row.ContractTemplateInstanceID == ContractTemplateID
                                                 && row.RoleID == RoleID
                                                 select row).ToList();
                                    if (data1.Count > 0)
                                    {
                                        var data = (from row in data1
                                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                    on row.sectionID equals row1.SectionID
                                                    where row1.ContractTemplateInstanceID == ContractTemplateID
                                                    select new getTemplateTransactionDetail
                                                    {
                                                        TemplateTransactionID = row.TemplateTransactionID,
                                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                                        ComplianceStatusID = row.ComplianceStatusID,
                                                        CreatedOn = row.CreatedOn,
                                                        Status = row.Status,
                                                        StatusChangedOn = row.StatusChangedOn,
                                                        TemplateContent = row.ContractTemplateContent,
                                                        RoleID = row.RoleID,
                                                        UserID = row.UserID,
                                                        sectionID = Convert.ToInt32(row.sectionID),
                                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                        sectionHeader = row.SectionHeader
                                                    }).OrderBy(x => x.orderID).ToList();

                                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                        data.ForEach(eachSection =>
                                        {
                                            if (eachSection.visibilty == 1)
                                            {
                                                strExporttoWord.Append(@"<div>");
                                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                                strExporttoWord.Append(@"</div>");
                                            }

                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                            strExporttoWord.Append(@"</div>");
                                        });

                                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                        obj1.ContractTemplateID = ContractTemplateID;
                                        obj1.CreatedOn = DateTime.Now;
                                        obj1.CreatedBy = UserID;
                                        obj1.TemplateContent = strExporttoWord.ToString();
                                        obj1.CreatedByText = UserName;
                                        obj1.Status = 22;
                                        obj1.OrderID = 1;
                                        entities.ContractTemplateTransactionHistories.Add(obj1);
                                        entities.SaveChanges();
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = CustomerID,
                        ContractID = Convert.ToInt32(ContractTemplateID),
                        StatusID = 22,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
                }
                if (RoleID == RolegetVRID)
                {
                    var getdata = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                   where row.UserID == UserID
                                   && row.ContractTemplateInstanceID == ContractTemplateID
                                   && row.RoleID == RoleID
                                   select row).ToList();

                    if (getdata.Count > 0)
                    {
                        var details = (from row in getdata
                                       join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                       where row1.ContractTemplateInstanceID == ContractTemplateID
                                       select new getTemplateTransactionDetail
                                       {
                                           TemplateTransactionID = row.TemplateTransactionID,
                                           TemplateInstanceID = row.ContractTemplateInstanceID,
                                           ComplianceStatusID = row.ComplianceStatusID,
                                           CreatedOn = row.CreatedOn,
                                           Status = row.Status,
                                           StatusChangedOn = row.StatusChangedOn,
                                           TemplateContent = row.ContractTemplateContent,
                                           RoleID = row.RoleID,
                                           UserID = row.UserID,
                                           sectionID = Convert.ToInt32(row.sectionID),
                                           orderID = Convert.ToInt32(row1.SectionOrder)
                                       }).OrderBy(x => x.orderID).ToList();

                        foreach (var item in details)
                        {
                            if (item.ComplianceStatusID == 16 || item.ComplianceStatusID == 21 || item.ComplianceStatusID == 22)
                            {
                                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                obj.ContractTemplateInstanceID = ContractTemplateID;
                                obj.CreatedOn = DateTime.Now;
                                obj.CreatedBy = UserID;
                                obj.ContractTemplateContent = item.TemplateContent;
                                obj.SectionID = item.sectionID;
                                obj.CreatedByText = UserName;
                                obj.Status = 15;
                                obj.OrderID = 1;
                                obj.Version = ContractVersion;
                                entities.ContractTemplateTransactions.Add(obj);
                                entities.SaveChanges();
                                int TransactionID = Convert.ToInt32(obj.Id);
                                if (TransactionID > 0)
                                {
                                    #region set transaction history

                                    var entitiesData1 = (from row in entities.Cont_SP_GetContractSectionsDetail(CustomerID, Convert.ToInt32(ContractTemplateID))
                                                         select row).ToList();

                                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                 where row.UserID == UserID
                                                 && row.ContractTemplateInstanceID == ContractTemplateID
                                                 && row.RoleID == RoleID
                                                 select row).ToList();
                                    if (data1.Count > 0)
                                    {
                                        var data = (from row in data1
                                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                    on row.sectionID equals row1.SectionID
                                                    where row1.ContractTemplateInstanceID == ContractTemplateID
                                                    select new getTemplateTransactionDetail
                                                    {
                                                        TemplateTransactionID = row.TemplateTransactionID,
                                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                                        ComplianceStatusID = row.ComplianceStatusID,
                                                        CreatedOn = row.CreatedOn,
                                                        Status = row.Status,
                                                        StatusChangedOn = row.StatusChangedOn,
                                                        TemplateContent = row.ContractTemplateContent,
                                                        RoleID = row.RoleID,
                                                        UserID = row.UserID,
                                                        sectionID = Convert.ToInt32(row.sectionID),
                                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                        sectionHeader = row.SectionHeader
                                                    }).OrderBy(x => x.orderID).ToList();

                                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                        data.ForEach(eachSection =>
                                        {
                                            if (eachSection.visibilty == 1)
                                            {
                                                strExporttoWord.Append(@"<div>");
                                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                                strExporttoWord.Append(@"</div>");
                                            }

                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                            strExporttoWord.Append(@"</div>");
                                        });

                                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                        obj1.ContractTemplateID = ContractTemplateID;
                                        obj1.CreatedOn = DateTime.Now;
                                        obj1.CreatedBy = UserID;
                                        obj1.TemplateContent = strExporttoWord.ToString();
                                        obj1.CreatedByText = UserName;
                                        obj1.Status = 15;
                                        obj1.OrderID = 1;
                                        entities.ContractTemplateTransactionHistories.Add(obj1);
                                        entities.SaveChanges();
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = CustomerID,
                        ContractID = Convert.ToInt32(ContractTemplateID),
                        StatusID = 15,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRefresh();", true);
                }
            }
        }

        protected void btnReviwer_Click(object sender, EventArgs e)
        {
            try
            {
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractID"]);
                BindSections_All(grdTemplateSection, ContractTemplateID);
                
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenInternalReviwerSectionModal();", true);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindSections_All(GridView grdTemplateSection, long ContractTemplateID)
        {
            try
            {
                int customerID = Convert.ToInt32(Request.QueryString["CustID"]);
                grdTemplateSection.DataSource = ContractTemplateManagement.GetContractSections_All(customerID, Convert.ToInt32(ContractTemplateID));
                grdTemplateSection.DataBind();
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
       
        public static int UpdateVendorRev(string Email,string ContactNo,int CID, int UID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_SubVendorMaster data = (from row in entities.Cont_tbl_SubVendorMaster
                                                  where row.Email == Email
                                                       && row.ContactNumber == ContactNo
                                                       && row.CustomerID == CID
                                                  select row).FirstOrDefault();
                    if (data != null)
                    {
                        data.IsDeleted = false;
                        entities.SaveChanges();
                        return Convert.ToInt32(data.ID);
                    }
                    else
                    {
                        Cont_tbl_SubVendorMaster obj = new Cont_tbl_SubVendorMaster
                        {
                            CustomerID = CID,
                            IsDeleted = false,                           
                            VendorName = Email,                            
                            ContactPerson = "",
                            ContactNumber = ContactNo,
                            Email = Email,
                            CreatedBy = UID,
                            CreatedOn = DateTime.Now
                        };
                        entities.Cont_tbl_SubVendorMaster.Add(obj);
                        entities.SaveChanges();
                        long getVID= obj.ID;
                        return Convert.ToInt32(getVID);
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
           
        }
        protected void grdTemplateSection_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }
        public int getRID(string roleCode)
        {
            int RoleID = -1;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Role data = (from row in entities.Roles
                                 where row.Code == roleCode
                                 select row).FirstOrDefault();
                    if (data != null)
                    {
                        RoleID = Convert.ToInt32(data.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return RoleID;
        }

        protected void btnAssignSections_Click(object sender, EventArgs e)
        {
            try
            {
                int RolegetID = getRID("VIR");//Vendor Internal Reviewer
                int selectedSectionCount = 0;
                int flagvalidation = 0;
                List<string> lstErrorMsg = new List<string>();
                foreach (GridViewRow eachGridRow in grdTemplateSection.Rows)
                {
                    if (eachGridRow.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.CheckBox chkRow = (System.Web.UI.WebControls.CheckBox)eachGridRow.FindControl("chkRow");

                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                            {
                                selectedSectionCount++;

                                System.Web.UI.WebControls.TextBox VendorReviewerEmail = (System.Web.UI.WebControls.TextBox)eachGridRow.FindControl("VendorReviewerEmail");
                                System.Web.UI.WebControls.TextBox VendorReviewerContact = (System.Web.UI.WebControls.TextBox)eachGridRow.FindControl("VendorReviewerContact");
                                if (VendorReviewerEmail.Text == "" || VendorReviewerContact.Text == "")
                                {
                                    flagvalidation = 1;
                                }
                            }

                        }
                    }
                }
                if (selectedSectionCount <= 0 || flagvalidation == 1)
                {
                    if (flagvalidation == 1)
                    {
                        lstErrorMsg.Add("Please enter data on selected row.");                        
                    }
                    else
                    {
                        lstErrorMsg.Add("Please select at least one row with data.");                       
                    }
                    if (lstErrorMsg.Count > 0)
                    {
                        showErrorMessages(lstErrorMsg, CustomValidator1);
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "errormessage();", true); 
                }
                else
                {
                    int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                    int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                    int customerID = Convert.ToInt32(Request.QueryString["CustID"]);
                    int ContractTemplateID = Convert.ToInt32(Request.QueryString["ContractID"]);
                    List<int> vendorlist = new List<int>();
                    vendorlist.Clear();
                    if (grdTemplateSection != null)
                    {
                        string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                        for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                        {
                            if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                            {
                                //int ReviewerId = -1;
                                System.Web.UI.WebControls.Label lblStatusID = (System.Web.UI.WebControls.Label)grdTemplateSection.Rows[i].FindControl("lblStatusID");
                                System.Web.UI.WebControls.Label lblSectionID = (System.Web.UI.WebControls.Label)grdTemplateSection.Rows[i].FindControl("lblSectionID");

                                System.Web.UI.WebControls.TextBox VendorReviewerEmail = (System.Web.UI.WebControls.TextBox)grdTemplateSection.Rows[i].FindControl("VendorReviewerEmail");
                                System.Web.UI.WebControls.TextBox VendorReviewerContact = (System.Web.UI.WebControls.TextBox)grdTemplateSection.Rows[i].FindControl("VendorReviewerContact");

                                int StatusID = -1;
                                int vendorID = -1;
                                if (lblStatusID.Text != "" && lblStatusID.Text != "-1")
                                {
                                    StatusID = Convert.ToInt32(lblStatusID.Text);
                                }
                                if (VendorReviewerEmail.Text != "" && VendorReviewerContact.Text != "")
                                {
                                    vendorID = UpdateVendorRev(VendorReviewerEmail.Text, VendorReviewerContact.Text, customerID, UserID);
                                }
                                if (lblSectionID != null && vendorID != -1 && StatusID != -1)
                                {
                                    if (!string.IsNullOrEmpty(lblSectionID.Text) || lblSectionID.Text != "0")
                                    {
                                        long SectionID = Convert.ToInt64(lblSectionID.Text);
                                        if (StatusID == 16 || StatusID == 21 || StatusID == 22)
                                        {
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                string ContractTemplate = string.Empty;
                                                var data12 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                              where row.UserID == UserID
                                                              && row.ContractTemplateInstanceID == ContractTemplateID
                                                              && row.RoleID == RoleID
                                                              && row.sectionID == SectionID
                                                              select row).FirstOrDefault();
                                                if (data12 != null)
                                                {
                                                    ContractTemplate = data12.ContractTemplateContent;
                                                }

                                                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                                obj.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                                                obj.CreatedOn = DateTime.Now;
                                                obj.CreatedBy = UserID;
                                                obj.ContractTemplateContent = ContractTemplate;
                                                obj.SectionID = Convert.ToInt32(SectionID);
                                                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                                obj.Status = 20;
                                                obj.Version = ContractVersion;
                                                obj.OrderID = 1;
                                                entities.ContractTemplateTransactions.Add(obj);
                                                entities.SaveChanges();
                                            }

                                            ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                                            {
                                                ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                                                RoleID = RolegetID,
                                                UserID = vendorID,
                                                IsActive = true,
                                                SectionID = Convert.ToInt32(SectionID),
                                                Visibility = 1
                                            };
                                            ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);

                                            vendorlist.Add(vendorID);
                                        }
                                    }
                                }
                                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRevTemplateModal();", true);
                            }
                        }
                    }
                    if (ContractTemplateID > 0 && vendorlist.Count > 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var contractdata = (from row in entities.Cont_tbl_ContractInstance
                                                where row.ID == ContractTemplateID
                                                && row.IsDeleted == false
                                                select row).FirstOrDefault();

                            if (contractdata != null)
                            {
                                Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                {
                                    CustomerID = customerID,
                                    ContractID = Convert.ToInt32(ContractTemplateID),
                                    StatusID = 20,
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    CreatedBy = UserID,
                                    UpdatedBy = UserID,
                                };
                                ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                                vendorlist = vendorlist.Distinct().ToList();
                                foreach (var item in vendorlist)
                                {
                                    try
                                    {
                                        Cont_tbl_SubVendorMaster data = (from row in entities.Cont_tbl_SubVendorMaster
                                                                         where row.ID == item
                                                                         && row.IsDeleted == false
                                                                         select row).FirstOrDefault();
                                        if (data != null)
                                        {
                                            int vendorID = item;
                                            int assignedRoleID = RolegetID;//Vendor Internal Reviewer
                                            string checkSum = Util.CalculateMD5Hash(vendorID.ToString() + ContractTemplateID.ToString() + assignedRoleID.ToString());

                                            string vendorname = data.VendorName;
                                            string contname = contractdata.ContractTitle;
                                            string contnum = contractdata.ContractNo;
                                            string portalURL = string.Empty;
                                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                            if (Urloutput != null)
                                            {
                                                portalURL = Urloutput.URL;
                                            }
                                            else
                                            {
                                                portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                            }
                                            string AccessURL = Convert.ToString(portalURL) +
                                                                       "/ContractVerifyOtp.aspx?" +
                                                                       "UID=" + vendorID.ToString() +
                                                                        "&RID=" + assignedRoleID.ToString() +
                                                                         "&CustID=" + customerID +
                                                                           "&ContractID=" + ContractTemplateID.ToString();

                                            //string AccessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) +
                                            //                            "/ContractVerifyOtp.aspx?" +
                                            //                            "UID=" + vendorID.ToString() +
                                            //                             "&RID=" + assignedRoleID.ToString() +
                                            //                              "&CustID=" + customerID +
                                            //                                "&ContractID=" + ContractTemplateID.ToString();

                                            string header = "Dear " + vendorname + "<br>" + "<br>" + "Contract has been shared with you for review." + "<br>" + "<br>";

                                            string ContractName = "Contract Name - " + contname + "<br>" + "<br>";

                                            string ContractNo = "Contract no. - " + contnum + "<br>" + "<br>";

                                            string UrlAccess = "Url to access Contract - " + AccessURL;

                                            string FinalMail = header + ContractName + ContractNo + UrlAccess;

                                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                                            string vendorEmail = data.Email;

                                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { vendorEmail }), null, null, "You have received a contract for review", FinalMail);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                            }
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseRevTemplateModal();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}