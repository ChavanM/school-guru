﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="BulkDocumentUploadContract.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.BulkDocumentUploadContract" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Bulk Document Upload');
        });

        function scrollUpPage() {
            $("#divMainView").animate({ scrollTop: 0 }, 'slow');
        }

        function REcheckAll(objRef) {
            for (var i = 0; i < $('span.classrem > input').length; i++) {
                if ($('#ContentPlaceHolder1_grdBulkDocUpload_remindercheckAll').is(':checked')) {
                    $($('span.classrem > input')[i]).prop("checked", true);

                } else {
                    $($('span.classrem > input')[i]).prop("checked", false);
                }
            }
        }

        function REchildcheckAll(objRef) {
            for (var i = 0; i < $('span.classrem > input').length; i++) {
                if ($($('span.classrem > input')[i]).is(':checked')) {
                    $($('span.classrem > input')[i]).prop("checked", true);
                    break;
                }
                else {
                    $($('span.classrem > input')[i]).prop("checked", false);
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="dashboard" id="divMainView" runat="server">
                <div class="col-md-12 colpadding0">

                    <asp:ValidationSummary ID="VsBulkDocUpload" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                        ValidationGroup="NoticePageValidationGroup" />
                    <asp:CustomValidator ID="cvBulkDocUpload" runat="server" EnableClientScript="False"
                        ValidationGroup="NoticePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                </div>

                <div class="col-md-12 colpadding0">
                    <div class="row">
                        <div class="form-group required col-md-2" style="display: none;">
                        </div>
                        <div class="form-group required col-md-4" style="display: none;">
                        </div>
                        <div class="form-group required col-md-4">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group required col-md-4">
                            <asp:FileUpload ID="fuBulkDocUpload" Multiple="Multiple" runat="server" Style="color: black" />
                        </div>
                        <div class="form-group required col-md-2">
                            <asp:LinkButton ID="btnUpload" OnClick="btnUpload_Click" class="btn btn-primary" runat="server">Upload</asp:LinkButton>
                        </div>
                    </div>
                    <div class="row" id="gridandpaging">
                        <div class="row">
                            <div class="col-md-12 colpadding0">
                                <asp:GridView runat="server" ID="grdBulkDocUpload" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                                    OnRowDataBound="grdBulkDocUpload_RowDataBound">
                                    <Columns>

                                        <asp:TemplateField HeaderText="Reminder" ItemStyle-Width="1%">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="remindercheckAll" runat="server" onclick="REcheckAll(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="rechild" runat="server" CssClass="classrem" onclick="REchildcheckAll(this);" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="1%">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HiddenID" runat="server" Value='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; width: 70%; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label runat="server" ID="lblFileNo" TextMode="SingleLine" Text='<%# Eval("FileNo") %>' ToolTip='<%# Eval("FileNo") %>'> ></asp:Label>
                                                    <asp:DropDownList ID="ddlFileNo" name="ddlFileNo" runat="server" class="form-control" Style="width: 100%;">
                                                    </asp:DropDownList>
                                                </div>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Documents" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>

                                <div class="row">
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-10 colpadding0">
                                            <div runat="server" id="DivRecordsScrum" style="color: #999">
                                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-
                                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                 <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-right colpadding0">
                                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 100%; float: right; height: 32px !important; margin-right: 6%"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem Text="5" />
                                                <asp:ListItem Text="10" Selected="True" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-1 text-right colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                                        </asp:LinkButton>
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="noteId" runat="server">
                                <p style="color: red;"><strong>Note:</strong> Document file name should like Contractno.XXX example - CN11001_test.pdf  </p>
                            </div>
                            <div class="form-group col-md-12 text-center" id="DivProcessDocUpload" runat="server">
                                <asp:Button Text="Process" runat="server" ID="btnProcessDocUpload" CssClass="btn btn-primary"
                                    OnClick="btnProcessDocUpload_Click" OnClientClick="return fvalidateinput();"
                                    ValidationGroup="NoticePageValidationGroup"></asp:Button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

