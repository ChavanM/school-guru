﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class SharedContDocument : System.Web.UI.Page
    {
        private long CutomerID = AuthenticationHelper.CustomerID;
        protected bool flag;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                flag = false;
                BindsharedContractDocument();
                bindPageNumber();
            }
        }

        protected void lnkApply_Click(object sender, EventArgs e)
        {
            BindsharedContractDocument();
            bindPageNumber();
        }

        private void BindsharedContractDocument()
        {
            try
            {

                List<Cont_SP_GetSharingDocDetail_Result> lstContDocs = new List<Cont_SP_GetSharingDocDetail_Result>();
                long CustID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                long uid = Convert.ToInt64(AuthenticationHelper.UserID);
                lstContDocs = VendorDetails.GetDocSharing_All(CustID, tbxFilter.Text.Trim().ToString());
                lstContDocs = lstContDocs.Where(x => x.CreatedBy == uid).ToList();
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstContDocs = lstContDocs.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstContDocs = lstContDocs.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (lstContDocs.Count > 0)
                {
                    grdContDoctType.DataSource = lstContDocs;
                    Session["TotalRows"] = lstContDocs.Count;
                    grdContDoctType.DataBind();
                }
                else
                {
                    grdContDoctType.DataSource = lstContDocs;
                    grdContDoctType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdContDoctType.PageIndex = 0;
                BindsharedContractDocument();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindsharedContractDocument(); bindPageNumber();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContDoctType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindsharedContractDocument();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContDoctType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                int totalPages = 0;
                if (Session["TotalRows"] != null)
                {
                    TotalRows.Value = Session["TotalRows"].ToString();

                    totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                    // total page item to be displayed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                    // return totalPages;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContDoctType.PageIndex = chkSelectedPage - 1;
            grdContDoctType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindsharedContractDocument(); SetShowingRecords();
        }
             

        protected void grdContDoctType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userID = -1;
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                if (e.CommandName.Equals("EDIT_ContDocShare"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    List<int> fileIds = new List<int>();
                    int contractID = -1;

                    var lstContDocs = VendorDetails.GetDocSharing_All(customerID, string.Empty);
                    lstContDocs = lstContDocs.Where(x => x.CreatedBy == userID).ToList();

                    var lstContDocs1 = lstContDocs.Where(x => x.Id == ID).FirstOrDefault();
                    if (lstContDocs1 != null)
                    {
                        fileIds.Add(Convert.ToInt32(lstContDocs1.FileID));
                        contractID = Convert.ToInt32(lstContDocs1.ContractID);
                    }
                    var fileID = String.Join(",", fileIds);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocSharePopup('" + fileID + "','" + contractID + "','" + ID + "');", true);
                }
                else if (e.CommandName.Equals("DELETE_ContDocShare"))
                {
                    int RecordId = Convert.ToInt32(e.CommandArgument);
                   
                   

                    bool saveSuccess = VendorDetails.CrontactDocUnSharingMappingByID(RecordId, customerID, userID);
                    if (saveSuccess)
                    {
                        BindsharedContractDocument();
                        bindPageNumber();

                        cvPageDocType.IsValid = false;
                        cvPageDocType.ErrorMessage = "Document Unshare Successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdContDoctType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int uid = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                
                List<Cont_SP_GetSharingDocDetail_Result> lstContDocs = new List<Cont_SP_GetSharingDocDetail_Result>();
         
                lstContDocs = VendorDetails.GetDocSharing_All(Convert.ToInt64(customerid), tbxFilter.Text.Trim().ToString());
                lstContDocs = lstContDocs.Where(x => x.CreatedBy == uid).ToList();
                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstContDocs = lstContDocs.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstContDocs = lstContDocs.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdContDoctType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdContDoctType.Columns.IndexOf(field);
                    }
                }
                flag = true;
                grdContDoctType.DataSource = lstContDocs;
                grdContDoctType.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdContDoctType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        

        protected void grdContDoctType_RowDataBound(object sendser, GridViewRowEventArgs e)
        {
            
        }
    }
}