﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractTemplateTransactionHistry.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.ContractTemplateTransactionHistry" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    <title>Telerik ASP.NET Example</title>
     <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
  
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet"  type="text/css" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />
    <script>
        
            var editor, range;
            function OnClientLoad(sender, args) {
               
                editor = sender;
                editor.enableEditing(false);
                editor.cancelRawEvent(e);              
            }

            function Button_OnClick() {                
                if (range) {
                    editor.getSelection().selectRange(range);
                }
                var e = document.getElementById("ddlContractTemplate");
                var selectedLocation = e.options[e.selectedIndex].value;
                editor.pasteHtml(selectedLocation);                
            }
            
            function CloseModel() {
                window.parent.parent.CloseContractModal();
            }            
    </script>
    <script type="text/javascript">
        // Specify the normal table row background color
        //   and the background color for when the mouse 
        //   hovers over the table row.

        var TableBackgroundNormalColor = "#ffffff";
        var TableBackgroundMouseoverColor = "#D3D3D3";

        // These two functions need no customization.
        function ChangeBackgroundColor(row) { row.style.backgroundColor = TableBackgroundMouseoverColor; }
        function RestoreBackgroundColor(row) { row.style.backgroundColor = TableBackgroundNormalColor; }
    </script>


</head>
 
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-12 colpadding0">
                <div class="col-md-2 colpadding0" style="overflow-y: auto;overflow-x: hidden;max-height: 457px;">                
                    <div class="col-md-12 colpadding0">
                        <asp:Repeater ID="rptComplianceVersionView"
                            OnItemCommand="rptComplianceVersionView_ItemCommand" runat="server">
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("Id") %>' ID="lblDocumentVersionView"
                                              style="" Font-Underline="false"  runat="server">
                                <table onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)" style="border-collapse: separate; border: 1px solid rgb(229, 229, 229); border-radius: 3px; padding: 1px; margin-bottom: 7px; width: 95%;">
                                    <tr>
                                        <td><%#Eval("CreatedByText")%></td>
                                    </tr>
                                    <tr>
                                        <td><%# Eval("CreatedOn") != null ? ((DateTime)Eval("CreatedOn")).ToString("dd-MM-yyyy  h:mm tt") : "" %></td>                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                </table>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="col-md-8 colpadding0" style="margin-left: 7px;">
                    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Visible="false" />
                    <div class="demo-containers" style="padding-top: 14px;">
                        <div class="demo-container">
                            <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" 
                            StripFormattingOptions="MSWord" EnableTrackChanges="false"
                             runat="server"
                                Width="929px" OnClientLoad="OnClientLoad" 
                               
                                unselectable="off"
                                Height="444px" ToolsFile="../../ToolsFile.xml" 
                                ContentFilters="DefaultFilters, PdfExportFilter"
                                SkinID="WordLikeExperience" EditModes="Design">
                                <RealFontSizes>
                                    <telerik:EditorRealFontSize Value="12pt" />
                                    <telerik:EditorRealFontSize Value="18pt" />
                                    <telerik:EditorRealFontSize Value="22px" />
                                </RealFontSizes>
                                <ExportSettings>
                                    <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                                        PageHeader="Some header text for DOCX documents" />
                                    <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                                        HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                                </ExportSettings>
                                <TrackChangesSettings Author="RadEditorUser" 
                                CanAcceptTrackChanges="false"
                                    UserCssId="reU0"></TrackChangesSettings>
                                <Content>                  
                                </Content>
                            </telerik:RadEditor>
                        </div>
                    </div>
                    <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                                    <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>
                    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                    </telerik:RadAjaxLoadingPanel>
                    <div runat="server" id="divHtml"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
