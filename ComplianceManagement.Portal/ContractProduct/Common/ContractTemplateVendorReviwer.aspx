﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractTemplateVendorReviwer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.ContractTemplateVendorReviwer" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">--%>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    <title>Telerik ASP.NET Example</title>
    
     <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>    
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
      <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />


 <script type="text/javascript">

function checkUncheckRow() {
    debugger;
                var selectedRowCount = 0;
                //Get the reference of GridView
              var grid = document.getElementById("grdTemplateSection");

                if (grid != null) {

                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    //The First element is the Header Checkbox
                    var headerCheckBox = inputList[0];
                    var checked = true;
                    for (var i = 0; i < inputList.length; i++) {
                        //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                        if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                            if (!inputList[i].checked) {
                                checked = false;
                            }
                            if (inputList[i].checked) {
                                selectedRowCount++;
                            }
                        }
                    }
                    headerCheckBox.checked = checked;
                }
            }

     function checkAll(chkHeader) {
         var selectedRowCount = 0;
         debugger;
          
         var grid = document.getElementById("grdTemplateSection");
         if (grid != null) {
             if (chkHeader != null) {
                 //Get all input elements in Gridview
                 var inputList = grid.getElementsByTagName("input");

                 for (var i = 1; i < inputList.length; i++) {
                     if (inputList[i].type == "checkbox") {
                         if (chkHeader.checked) {
                             inputList[i].checked = true;
                             selectedRowCount++;
                         }
                         else if (!chkHeader.checked)
                             inputList[i].checked = false;
                     }
                 }
             }
         }
     }



            var editor, range;
            function OnClientLoad(sender, args) {
                editor = sender;
            }

            function OnClientSelectionChange(sender, args) {
                range = editor.getSelection().getRange(true);
            }

            function CloseRefresh() {
                window.parent.CloseSectionModal();
                //window.parent.ClosedSectionTemplate();                
            }
            function OpenHistorySectionModal(uid,tid,rid,cid) {
                $('#TemplateHistroySectionPopup').modal('show');
                $('#showdetails1').attr('src', "../Common/ContractTemplateTransactionHistry.aspx?UID=" + uid + "&RID=" + rid + "&TID=" + tid + "&CID=" + cid);
                return false;
            }
            function OpenInternalReviwerSectionModal() {
                $('#TemplateSetReviwer').modal('show');
                return false;
            }
            function CloseRevTemplateModal() {                
                $('#TemplateSectionPopup').modal('hide');
                window.location.href = window.location.pathname + window.location.search + window.location.hash;
            }
            function errormessage() {
                //
                $('#TemplateSetReviwer').modal('show');
                //alert("1");
                //$('#TemplateSetReviwer').modal('show');
                return false;
            }
            function CloseSectionModal() {

                $('#TemplateSectionPopup').modal('hide');
                //window.parent.ClosedSectionTemplate();
            }

            function OpenSectionModal(uid, tid, rid, cid, sid)
            {
                debugger;
                $('#TemplateSectionPopup').modal('show');
                $('#showdetails').attr('src', "../Common/SectionContractReviewDetail.aspx?UID=" + uid + "&RID=" + rid + "&TID=" + tid + "&CID=" + cid + "&SID=" + sid);
                  <%--<iframe id="showdetails" src="../Common/SectionContractReviewDetail.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =ContractTemplateId%>&CID=<% =CustId%>&SID=<% =SectionId%>" width="100%" height="525px;" frameborder="0"></iframe>--%>
                return false;
            }

            function CloseSectionModal() {         
                $('#TemplateSectionPopup').modal('hide');
                window.location.href = window.location.pathname + window.location.search + window.location.hash;
            }
            function CloseHistorySectionModal()
            {                
                $('#TemplateHistroySectionPopup').modal('hide');
                window.location.href = window.location.pathname + window.location.search + window.location.hash;
            }

            $(document).ready(function () {
                $('.btn-minimize').click(function () {
                    var s1 = $(this).find('i');
                    if ($(this).hasClass('collapsed')) {
                        $(s1).removeClass('fa-chevron-down');
                        $(s1).addClass('fa-chevron-up');
                    } else {
                        $(s1).removeClass('fa-chevron-up');
                        $(s1).addClass('fa-chevron-down');
                    }
                });
                BindControls();


            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            hideDivBranch();
            });
            function btnminimize(obj) {
            debugger;
                var s1 = $(obj).find('i');
                if ($(obj).hasClass('collapsed')) {

                    $(s1).removeClass('fa-chevron-up');
                    $(s1).addClass('fa-chevron-down');
                } else {
                    $(s1).removeClass('fa-chevron-down');
                    $(s1).addClass('fa-chevron-up');
                }
            }
           

            function BindControls() {
                var startDate = new Date();
                $(function () {
                    $('input[id*=txtProposalDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('input[id*=txtAgreementDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('input[id*=txtEffectiveDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('input[id*=txtReviewDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('input[id*=txtExpirationDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('input[id*=tbxTaskDueDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                    });
                });

                $(function () {
                    $('[id*=lstBoxVendor]').multiselect({
                        includeSelectAllOption: true,
                        numberDisplayed: 2,
                        buttonWidth: '100%',
                        enableCaseInsensitiveFiltering: true,
                        filterPlaceholder: 'Type to Search for Vendor..',
                        nSelectedText: ' - Vendor(s) selected',
                    });

                    $('[id*=lstBoxOwner]').multiselect({
                        includeSelectAllOption: true,
                        numberDisplayed: 2,
                        buttonWidth: '100%',
                        enableCaseInsensitiveFiltering: true,
                        filterPlaceholder: 'Type to Search for User..',
                        nSelectedText: ' - Owner(s) selected',
                    });

                    $('[id*=lstBoxApprover]').multiselect({
                        includeSelectAllOption: true,
                        numberDisplayed: 2,
                        buttonWidth: '100%',
                        enableCaseInsensitiveFiltering: true,
                        filterPlaceholder: 'Type to Search for User..',
                        nSelectedText: ' - Approver(s) selected',
                    });
                });
            }
            function hideDivBranch() {
                $('#divBranches').hide("blind", null, 500, function () { });
            }

            
    </script> 

 <style type="text/css">
        h4 {
            height: 100%;
            font-weight: 400;
            display: inline-block;
        }
    </style>
</head>
 
<body style="background-color: #f7f7f7;">
    <form id="form1" runat="server">
        <div class="container" style="background-color: #f7f7f7;">

        <div class="row Dashboard-white-widget">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Contract Detail(s)">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractAggriment">
                            <a>
                                <h4>Contract Detail(s)</h4>
                            </a>
                            <div class="panel-actions">
                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractAggriment">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="collapseDivContractAggriment" class="panel-collapse collapse">
                      <asp:Panel ID="pnlContract" runat="server">
                                        <div class="container plr0">
                                            
                                            <div class="row" style="padding-top: 6px;">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="col-lg-2 col-md-2">
                                                        <label class="control-label">Contract Status</label>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10">
                                                        <asp:Label ID="txtboxMilestoneStatus" runat="server" CssClass="text-label"></asp:Label>
                                                          <asp:DropDownListChosen runat="server" Visible="false" ID="ddlContractStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        class="form-control" Width="100%" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row" style="padding-top: 6px;">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="col-lg-2 col-md-2">
                                                        <label class="control-label">Contract Number</label>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10">
                                                        <asp:Label ID="txtContractNo" runat="server" CssClass="text-label"></asp:Label>                                                          
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 6px;">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="col-lg-2 col-md-2">
                                                        <label class="control-label">Contract Title</label>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10">
                                                        <asp:Label ID="txtTitle" runat="server" CssClass="text-label"></asp:Label>                                                          
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 6px;">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="col-lg-2 col-md-2">
                                                        <label class="control-label">Description</label>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10">
                                                        <asp:Label ID="tbxDescription" runat="server" CssClass="text-label"></asp:Label>                                                          
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 6px;">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="col-lg-2 col-md-2">
                                                        <label class="control-label">Entity/Location</label>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10">
                                                        <asp:Label ID="tbxBranchlbl" runat="server" CssClass="text-label"></asp:Label>
                                                        <div style="display: none;">
                                                            <asp:TextBox runat="server" ID="tbxBranch" CssClass="form-control" ReadOnly="true" autocomplete="off" AutoCompleteType="None" CausesValidation="true"
                                                                Style="cursor: pointer;" />

                                                            <div style="position: absolute; z-index: 10; display: none" id="divBranches">
                                                                <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px"
                                                                    Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                                    ShowLines="true">
                                                                </asp:TreeView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row" style="padding-top: 6px;">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="col-lg-2 col-md-2">
                                                        <label class="control-label">Vendor</label>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10">
                                                        <asp:Label ID="LabelVendor" runat="server" CssClass="text-label"></asp:Label>
                                                    <asp:ListBox ID="lstBoxVendor" Visible="false" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 6px;">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="col-lg-2 col-md-2">
                                                        <label class="control-label">Department</label>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10">
                                                        <asp:Label ID="lblDepartment" runat="server" CssClass="text-label"></asp:Label>
                                                      <asp:DropDownListChosen runat="server" Visible="false" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="display:none;">
                                                <div class="form-group col-md-4 pl0">
                                                    <label for="txtProposalDate" class="control-label">Proposal Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtProposalDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 pl0" data-provide="datepicker">
                                                    <label for="txtAgreementDate" class="control-label">Agreement Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtAgreementDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group required col-md-4 pl0">
                                                    <label for="txtEffectiveDate" class="control-label">Effective Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEffectiveDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="display:none;">
                                                <div class="form-group col-md-4 pl0">
                                                    <label for="txtAgreementDate" class="control-label">Review Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtReviewDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 pl0 d-none">
                                                    <label for="rbContractEndType" class="control-label">&nbsp;</label>
                                                    <asp:RadioButtonList ID="rbContractEndType" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem class="radio-inline" Text="Expiration Date" Value="E" Selected="True"></asp:ListItem>
                                                        <asp:ListItem class="radio-inline" Text="Duration " Value="D"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>

                                                <div class="form-group col-md-4 pl0" id="divExpirationDate">
                                                    <label for="txtExpirationDate" class="control-label">Expiration Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtExpirationDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 pl0 d-none" id="divDuration">
                                                    <label for="txtDuration">Duration</label>
                                                    <div class="input-group">
                                                        <asp:TextBox ID="txtDuration" runat="server" placeholder="" class="form-control col-md-8" />
                                                        <div class="input-group-btn">
                                                            <asp:DropDownList ID="ddlDuration" runat="server" CssClass="form-control col-md-4">
                                                                <asp:ListItem Text="Days" Value="D" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Weeks" Value="W"></asp:ListItem>
                                                                <asp:ListItem Text="Months" Value="M"></asp:ListItem>
                                                                <asp:ListItem Text="Years" Value="Y"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 pl0">
                                                    <label for="txtNoticeTerm" class="control-label">Notice Term</label>
                                                    <div class="input-group">
                                                        <asp:TextBox ID="txtNoticeTerm" runat="server" class="form-control col-md-8" />
                                                        <div class="input-group-btn">
                                                            <asp:DropDownList ID="ddlNoticeTerm" runat="server" CssClass="form-control col-md-4"
                                                                Style="border-bottom-right-radius: 4px; border-top-right-radius: 4px; border-left: none;">
                                                                <asp:ListItem Text="Days" Value="1" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Weeks" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Months" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Years" Value="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="display:none;">                                      

                                                        <div class="form-group required col-md-4 pl0">
                                                            <label for="ddlContractType" class="control-label">Contract Type</label>
                                                            <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlContractType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                class="form-control" Width="100%" DataPlaceHolder="Select Contract Type">
                                                            </asp:DropDownListChosen>
                                                        </div>

                                                        <div class="form-group col-md-4 pl0">
                                                            <label for="ddlContractSubType" class="control-label">Contract Sub-Type</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlContractSubType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                class="form-control" Width="100%" DataPlaceHolder="Select Contract Type">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                  

                                                <div class="form-group required col-md-4 pl0">
                                                    <label for="lstBoxOwner" class="control-label">Contract Owner</label>
                                                    <asp:ListBox ID="lstBoxOwner" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                </div>
                                            </div>

                                            <div class="row"  style="display:none;">
                                                <div class="form-group col-md-4 pl0">
                                                    <label for="tbxContractAmt" class="control-label">Contract Amount</label>
                                                    <asp:TextBox runat="server" ID="tbxContractAmt" CssClass="form-control" autocomplete="off" />
                                                </div>

                                                <div class="form-group col-md-4 pl0">
                                                    <label for="ddlPaymentTerm" class="control-label">Payment Term</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlPaymentTerm" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        class="form-control" Width="100%" DataPlaceHolder="Select Court">
                                                        <asp:ListItem Text="One-Time" Value="0" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Daily" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Weekly" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="Periodically" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="Monthly" Value="30"></asp:ListItem>
                                                        <asp:ListItem Text="Quarterly" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="Half-Yearly" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="Yearly" Value="12"></asp:ListItem>
                                                    </asp:DropDownListChosen>
                                                </div>

                                                <div class="form-group col-md-4 pl0">
                                                    <label for="txtBoxProduct" class="control-label">Product/Items(s)</label>
                                                    <asp:TextBox ID="txtBoxProduct" CssClass="form-control" runat="server"></asp:TextBox>                                                   
                                                </div>

                                                <div class="form-group required col-md-4 d-none">
                                                    <label for="lstBoxApprover" class="control-label">Contract Approver(s)</label>
                                                    <asp:ListBox ID="lstBoxApprover" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>                                                 
                                                </div>
                                            </div>

                                             <div class="row" style="display:none;">
                                                <div class="form-group col-md-12 pl0">
                                                  
                                                            <asp:GridView runat="server" ID="grdCustomField" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="true"
                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                        <ItemTemplate>
                                                                            <%#Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-Width="25%" FooterStyle-Width="25%"><%--HeaderText="Field"--%>
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer" CssClass="unique-footer-select" DataPlaceHolder="Select"
                                                                                AllowSingleDeselect="false" DisableSearchThreshold="5" Width="100%">
                                                                            </asp:DropDownListChosen>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-Width="1%" FooterStyle-Width="1%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblImgAddNew" runat="server"></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <img id="imgAddNewCustomField" src='<%# ResolveUrl("~/Images/add_icon_new.png")%>'
                                                                                onclick="OpenAddNewCustomFieldPopUp()" alt="Add" data-toggle="tooltip" data-placement="bottom" title="Click to Add New Custom Field" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-Width="25%" FooterStyle-Width="25%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"><%--HeaderText="Value"--%>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control" PlaceHolder="Value" Text='<%# Eval("labelValue") %>'></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox runat="server" AutoPostBack="true" ID="txtFieldValue_Footer" PlaceHolder="Value" CssClass="form-control" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
                    

        <div class="row Dashboard-white-widget">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Contract Detail(s)">
                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractSummary">
                            <a>
                                <h4>Agreement Detail(s)</h4>
                            </a>
                            <div class="panel-actions">
                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractSummary">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="collapseDivContractSummary" class="panel-collapse collapse in">
                        <div class="row">
                            <div class="col-md-12 colpadding0" style="padding-top: 10px;">
                                <div class="col-md-2 colpadding0" style="overflow-y: auto; overflow-x: hidden; max-height: 600px;">
                                    <div class="col-md-12 colpadding0" style="padding-left: 5px;">
                                        <asp:Repeater ID="rptComplianceVersionView"
                                            OnItemCommand="rptComplianceVersionView_ItemCommand" runat="server">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("SectionID")+","+Eval("SectionStatusID") %>' ID="lblDocumentVersionView"
                                                    Style="" Font-Underline="false" runat="server">
                                <table style="padding: 6px;border-collapse: separate; border: 1px solid rgb(229, 229, 229); border-radius: 3px; margin-bottom: 7px; width: 95%;">
                                    <tr>
                                        <td><%#Eval("Header")%></td>
                                    </tr>
                                    <tr>
                                      <td><%#Eval("SectionStatus")%></td>                                   
                                    </tr>
                                     <tr>                                         
                                         <td style='color:<%#Eval("DeptColor")%>;font-size: 16px;'><%#Eval("DeptName")%></td>
                                    </tr>
                                     <tr>                                         
                                         <td><%#Eval("SectionReviwerName")%></td>
                                    </tr>
                                    <tr>
                                        <td><%# Eval("CreatedOn") != null ? ((DateTime)Eval("CreatedOn")).ToString("dd-MM-yyyy  h:mm tt") : "" %></td>                                        
                                    </tr>
                                </table>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="col-md-8 colpadding0">
                                    <div class="row">
                                        <div class="col-md-12 colpadding0" style="margin-left: -2px;">

                                            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                                            <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Visible="false" />
                                            <div class="demo-containers">
                                                <div class="demo-container">
                                                    <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" StripFormattingOptions="MSWord" EnableTrackChanges="true" runat="server"
                                                        Width="811px" OnClientLoad="OnClientLoad" OnClientSelectionChange="OnClientSelectionChange"
                                                        Height="600px" ToolsFile="../../ToolsFile.xml" OnExportContent="RadEditor1_ExportContent"
                                                        ContentFilters="DefaultFilters, PdfExportFilter"
                                                        SkinID="WordLikeExperience" EditModes="Preview">
                                                        <RealFontSizes>
                                                            <telerik:EditorRealFontSize Value="12pt" />
                                                            <telerik:EditorRealFontSize Value="18pt" />
                                                            <telerik:EditorRealFontSize Value="22px" />
                                                        </RealFontSizes>
                                                        <ExportSettings>
                                                            <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                                                                PageHeader="Some header text for DOCX documents" />
                                                            <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                                                                HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                                                        </ExportSettings>
                                                        <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true"
                                                            UserCssId="reU0"></TrackChangesSettings>
                                                        <Content>                  
                                                        </Content>
                                                    </telerik:RadEditor>
                                                </div>
                                            </div>
                                            <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                                                <AjaxSettings>
                                                    <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                                                        <UpdatedControls>
                                                            <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                                                            <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                                                        </UpdatedControls>
                                                    </telerik:AjaxSetting>
                                                </AjaxSettings>
                                            </telerik:RadAjaxManager>
                                            <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                                            </telerik:RadAjaxLoadingPanel>
                                            <div runat="server" id="divHtml"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 colpadding0">
                                <div class="col-md-12 colpadding0" style="width: 197px; padding-bottom: 5px; text-align: center;">
                                        <asp:Button ID="btnReview" Style="width: 90%;" runat="server" CssClass="btn btn-primary" Text="Approve" OnClick="btnReview_Click"/>
                                    </div>
                                    <div class="col-md-12 colpadding0" style="width: 197px; padding-bottom: 5px; text-align: center;">
                                        <asp:Button ID="btnReject" Style="width: 90%;" runat="server" CssClass="btn btn-primary" Text="Reject" OnClick="btnReject_Click" />
                                    </div>
                                    <div class="col-md-12 colpadding0" style="width: 197px; padding-bottom: 5px; text-align: center;">
                                        <asp:Button ID="BtnFullHistory" Style="width: 90%;" runat="server" CssClass="btn btn-primary" OnClick="BtnFullHistory_Click" Text="Full History" />
                                    </div>
                                    <div class="col-md-12 colpadding0" style="width: 197px; padding-bottom: 5px; text-align: center;">
                                        <asp:Button ID="btnReviwer" runat="server" Style="width: 90%;" CssClass="btn btn-primary" Text="Share for Internal Review" OnClick="btnReviwer_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    
      <div class="modal fade" id="TemplateHistroySectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content" style="width: 1181px; margin-left: -304px;">
                                    <div class="modal-header" style="margin-bottom: 0px; border-bottom: 0px;">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseHistorySectionModal()">&times;</button>
                                    </div>
                                    <div class="modal-body" style="width: 100%;">
                                      <iframe src="about:blank" id="showdetails1" frameborder="0" runat="server" style="width:100%;height:525px;"></iframe>
                                       <%--<iframe id="showdetails1" src="../Common/ContractTemplateTransactionHistry.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =ContractTemplateId%>&CID=<% =CustId%>" width="100%" height="525px;" frameborder="0"></iframe>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="TemplateSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content" style="width: 1018px; margin-left: -203px;">
                                    <div class="modal-header" style="margin-bottom: 0px; border-bottom: 0px;">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                                    </div>
                                    <div class="modal-body" style="width: 100%;">
                                        <iframe src="about:blank" id="showdetails" frameborder="0" runat="server" style="width:100%;height:525px;"></iframe>
                                        <%--<iframe id="showdetails" src="../Common/SectionContractReviewDetail.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =ContractTemplateId%>&CID=<% =CustId%>&SID=<% =SectionId%>" width="100%" height="525px;" frameborder="0"></iframe>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                          <div class="modal fade" id="TemplateSetReviwer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 1039px;margin-left: -257px;">
                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <label style="font-size: 18px;">Select user to assign</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                          <div class="row">
                            <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="div1">
                                <asp:Label runat="server" ID="Label1"></asp:Label>
                            </div>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ContractApproverValidationGroup" />
                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                ValidationGroup="ContractApproverValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                            <div class="row" style="padding-top: 10px;">                                
                                <div class="col-md-12 colpadding0" style="max-height: 341px;overflow-x: auto;">
                                    
                                            <asp:GridView runat="server" ID="grdTemplateSection" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                                AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                                                OnRowDataBound="grdTemplateSection_RowDataBound">
                                                <Columns>
                                                  <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAll(this)" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRow" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                    <asp:BoundField DataField="Header" HeaderText="Section" ItemStyle-Width="25%" />
                                                    <asp:BoundField DataField="DeptName" HeaderText="Department" ItemStyle-Width="25%" />  
                                                     <asp:TemplateField HeaderText="Reviewer Email" ItemStyle-Width="25%">                                                        
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSectionID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblStatusID" runat="server" Text='<%# Eval("SectionStatusID") %>' Visible="false"></asp:Label>
                                                            <asp:TextBox ID="VendorReviewerEmail" class="form-control" Text="" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>    
                                                    <asp:TemplateField HeaderText="Reviewer Contact" ItemStyle-Width="25%">                                                        
                                                        <ItemTemplate>                                                            
                                                            <asp:TextBox ID="VendorReviewerContact" class="form-control" Text="" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                 
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Record Found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                      
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10px; margin-left: 178px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-6 colpadding0">
                                        <asp:Button Text="Submit for Review" runat="server" Style="float: right;" ID="btnAssignSections"
                                            CssClass="btn btn-primary" onclick="btnAssignSections_Click"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>

        </div>
    </form>
</body>
</html>
