﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class UploadContractDocuments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindContractDocType();

                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["FlagID"]))
                    {
                        //ViewState["FlagID"] = Request.QueryString["FlagID"].ToString();
                        TxtFlag.Text = Request.QueryString["FlagID"].ToString();
                    }
                    //string strTaskID = CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["CID"]));
                    ViewState["ContractInstanceID"] = CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["CID"]));
                }                
            }
        }

        private void BindContractDocType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstdocTypes = ContractMastersManagement.GetContractDocTypes_All(customerID);

                ddlDocType.DataTextField = "TypeName";
                ddlDocType.DataValueField = "ID";

                ddlDocType.DataSource = lstdocTypes;
                ddlDocType.DataBind();
                //ddlDocType.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUploadContractDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    if (!string.IsNullOrEmpty(ViewState["ContractInstanceID"].ToString()))
                    {
                        bool saveSuccess = false;
                        bool isBlankFile = false;
                        long ContractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        if (ContractInstanceID > 0)
                        {
                            #region Upload Document

                            HttpFileCollection fileCollection = Request.Files;
                            if (fileCollection.Count > 0)
                            {
                                string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = null;
                                    uploadfile = fileCollection[i];
                                    int filelength = uploadfile.ContentLength;
                                    string fileName = Path.GetFileName(uploadfile.FileName);
                                    string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                                    if (!string.IsNullOrEmpty(fileName))
                                    {
                                        if (filelength == 0)
                                        {
                                            isBlankFile = true;
                                            break;
                                        }
                                        else if (ext == "")
                                        {
                                            isBlankFile = true;
                                            break;
                                        }
                                        else
                                        {
                                            if (ext != "")
                                            {
                                                for (int j = 0; j < InvalidFileTypes.Length; j++)
                                                {
                                                    if (ext == "." + InvalidFileTypes[j])
                                                    {
                                                        isBlankFile = true;
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                            if (isBlankFile == false)
                            {
                                saveSuccess = uploadDocuments(ContractInstanceID, Convert.ToInt32(ddlDocType.SelectedValue), Request.Files, "ContractFileUpload", null, null, txtDocTags.Text.Trim());

                                if (saveSuccess)
                                {
                                    ContractManagement.CreateAuditLog("C", ContractInstanceID, "Cont_tbl_FileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document(s) Uploaded", true, Convert.ToInt32(ContractInstanceID));
                                    // BindContractDocuments_Paging();
                                }

                                if (saveSuccess)
                                {
                                    cvContractDocument.IsValid = false;
                                    cvContractDocument.ErrorMessage = "Document(s) Uploaded Successfully.";
                                    vsContractDocument.CssClass = "alert alert-success";
                                }
                                else
                                {
                                    cvContractDocument.IsValid = false;
                                    cvContractDocument.ErrorMessage = "Something went wrong, during document upload, Please try again";
                                }
                            }
                            else
                            {
                                cvContractDocument.IsValid = false;
                                cvContractDocument.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                            }

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool uploadDocuments(long contractID, long docTypeID, HttpFileCollection fileCollection, string fileUploadControlName, long? taskID, long? taskResponseID, string fileTags)
        {
            bool uploadSuccess = false;
            try
            {
                #region Upload Document

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string directoryPath = string.Empty;
                string fileName = string.Empty;

                Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                {
                    ContractID = contractID,
                    DocTypeID = docTypeID,

                    TaskID = taskID,
                    TaskResponseID = taskResponseID,

                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false,
                    //added by  renuka
                    PhysicalLocation = txtphysicalLocation.Text,
                    FileNO = txtFileno.Text,
                };

                if (fileCollection.Count > 0)
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                    if (contractID > 0)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadedFile = fileCollection[i];

                            if (uploadedFile.ContentLength > 0)
                            {
                                string[] keys1 = fileCollection.Keys[i].Split('$');

                                if (keys1[keys1.Count() - 1].Equals(fileUploadControlName))
                                {
                                    fileName = uploadedFile.FileName;
                                }

                                objContDoc.FileName = fileName;

                                //Get Document Version
                                var contractDocVersion = ContractDocumentManagement.ExistsContractDocumentReturnVersion(objContDoc);

                                contractDocVersion++;
                                objContDoc.Version = contractDocVersion + ".0";

                                if (taskID == null && taskResponseID == null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + docTypeID + "/" + objContDoc.Version);
                                else if (taskID != null && taskResponseID == null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + taskID + "/" + docTypeID + "/" + objContDoc.Version);
                                else if (taskID != null && taskResponseID != null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + taskID + "/" + taskResponseID + "/" + docTypeID + "/" + objContDoc.Version);

                                if (!Directory.Exists(directoryPath))
                                    Directory.CreateDirectory(directoryPath);

                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                Stream fs = uploadedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                objContDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                objContDoc.FileKey = fileKey1.ToString();
                                objContDoc.VersionDate = DateTime.Now;
                                objContDoc.CreatedOn = DateTime.Now;
                                objContDoc.FileSize = uploadedFile.ContentLength;
                                DocumentManagement.Contract_SaveDocFiles(fileList);
                                long newFileID = ContractDocumentManagement.CreateContractDocumentMapping(objContDoc);

                                //if (string.IsNullOrEmpty(fileTags))
                                //    fileTags = fileName;

                                if (newFileID > 0)
                                    uploadSuccess = true;

                                if (newFileID > 0 & !string.IsNullOrEmpty(fileTags))
                                {
                                    string[] arrFileTags = fileTags.Trim().Split(',');

                                    if (arrFileTags.Length > 0)
                                    {
                                        List<Cont_tbl_FileDataTagsMapping> lstFileTagMapping = new List<Cont_tbl_FileDataTagsMapping>();

                                        for (int j = 0; j < arrFileTags.Length; j++)
                                        {
                                            Cont_tbl_FileDataTagsMapping objFileTagMapping = new Cont_tbl_FileDataTagsMapping()
                                            {
                                                FileID = newFileID,
                                                FileTag = arrFileTags[j].Trim(),
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                            };

                                            lstFileTagMapping.Add(objFileTagMapping);
                                        }

                                        if (lstFileTagMapping.Count > 0)
                                        {
                                            ContractDocumentManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                        }
                                    }
                                }

                                fileList.Clear();
                            }

                        }//End For Each
                    }
                }

                #endregion

                return uploadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return uploadSuccess;
            }
        }


        [WebMethod]
        public static List<ListItem> GetDocTypes()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstdocumentType = new List<ListItem>();

            var lstAllDocTypes = ContractMastersManagement.GetContractDocTypes_All(customerID);
            if (lstAllDocTypes.Count > 0)
            {
                for (int i = 0; i < lstAllDocTypes.Count; i++)
                {
                    lstdocumentType.Add(new ListItem
                    {
                        Value = lstAllDocTypes.ToList()[i].ID.ToString(),
                        Text = lstAllDocTypes.ToList()[i].TypeName,
                    });
                }

                lstdocumentType.Add(new ListItem
                {
                    Value = "0",
                    Text = "Add New",
                });
            }

            return lstdocumentType;
        }
    }
}