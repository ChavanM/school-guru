﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractMyTask : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Page.Title = "Verify OTP::Avantis";

                    if (!String.IsNullOrEmpty(Request.QueryString["TaskID"]))
                    {
                        if (!String.IsNullOrEmpty(Request.QueryString["CID"]))
                        {
                            //if (!String.IsNullOrEmpty(Request.QueryString["userID"]))
                            //{
                            //int userID = Convert.ToInt32(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["userID"])));

                            string strTaskID = CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["TaskID"]));
                            string strNoticeCaseInstanceID = CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["NID"]));

                            if (strTaskID != "" && strNoticeCaseInstanceID != "")
                            {
                                int taskID = Convert.ToInt32(strTaskID);
                                int noticeCaseInstanceID = Convert.ToInt32(strNoticeCaseInstanceID);

                                int userID = 0;

                                if (taskID != 0 && noticeCaseInstanceID != 0) /*&& userID != 0*/
                                {
                                    var taskDetail = LitigationTaskManagement.GetNoticeCaseTaskViewDetails(noticeCaseInstanceID, taskID);

                                    if (taskDetail != null)
                                    {
                                        if (taskDetail.StatusID != null && taskDetail.StatusID != 3) //If Task Closed then No Response
                                        {
                                            if (taskDetail.IsActive) //Task Active or Deleted
                                            {
                                                if (taskDetail.URLExpired != null && taskDetail.URLExpired != true) //URL Active
                                                {
                                                    Session["TaskDetail"] = taskDetail;

                                                    if (taskDetail.LinkCreatedOn != null)
                                                    {
                                                        //Check for Link Active or Expired
                                                        TimeSpan diff = DateTime.Now - Convert.ToDateTime(taskDetail.LinkCreatedOn);
                                                        double hours = diff.TotalHours;

                                                        if (hours <= 48)
                                                        {
                                                            if (taskDetail.AssignTo != null)
                                                            {
                                                                ViewState["userID"] = taskDetail.AssignTo;
                                                                ViewState["TaskID"] = taskDetail.TaskID;
                                                                ViewState["NoticeCaseInstanceID"] = taskDetail.NoticeCaseInstanceID;

                                                                userID = Convert.ToInt32(taskDetail.AssignTo);

                                                                if (taskDetail.Email != null && taskDetail.Email != "")
                                                                {
                                                                    if (taskDetail.ContactNumber != null && taskDetail.ContactNumber != "")
                                                                    {
                                                                        if (sendOTP(userID, taskDetail.Email, taskDetail.ContactNumber))
                                                                        {
                                                                            divOTP.Visible = true;
                                                                            ViewState["WrongOTP"] = 0;
                                                                            manipulatedEmail(taskDetail.Email);
                                                                            manipulatedContactNo(taskDetail.ContactNumber);
                                                                        }
                                                                        else
                                                                        {
                                                                            cvLogin.IsValid = false;
                                                                            cvLogin.ErrorMessage = "OTP sent failed, Please try again after some time.";
                                                                            return;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvLogin.IsValid = false;
                                                            cvLogin.ErrorMessage = "Access URL got expired, Please contact assignor to re-generate new URL.";
                                                            return;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    cvLogin.IsValid = false;
                                                    cvLogin.ErrorMessage = "Access URL got expired, Please contact assignor to re-generate new URL.";
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = "Task Deleted by Assignor. So, Access URL got expired.";
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            cvLogin.IsValid = false;
                                            cvLogin.ErrorMessage = "Task Closed by Assignor. So, Access URL got expired.";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        cvLogin.IsValid = false;
                                        cvLogin.ErrorMessage = "Invalid Access URL, please copy and paste the Access URL into your browser's address bar.";
                                        return;
                                    }
                                }
                                else
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Invalid Access URL, please copy and paste the Access URL into your browser's address bar.";
                                    return;
                                }
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Invalid Access URL, please copy and paste the Access URL into your browser's address bar.";
                                return;
                            }
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Invalid Access URL, please copy and paste the Access URL into your browser's address bar.";
                            return;
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Invalid Access URL, please copy and paste the Access URL into your browser's address bar.";
                        return;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "Something went wrong. Please try again.";
                }
            }
        }

        protected void btnOTP_Click(object sender, EventArgs e)
        {
            try
            {
                int wrongOTPCount = 0;
                if (ViewState["userID"] != null)
                {
                    if (!string.IsNullOrEmpty(ViewState["userID"].ToString()))
                    {
                        int userID;
                        userID = int.Parse(ViewState["userID"].ToString());

                        //if (VerifyOTPManagement.Exists(Convert.ToInt32(txtOTP.Text), Convert.ToInt32(ViewState["userID"].ToString())))
                        if (VerifyOTPManagement.verifyOTP(Convert.ToInt32(txtOTP.Text), Convert.ToInt32(ViewState["userID"].ToString())))
                        {
                            bool verifiedOTP = false;

                            VerifyOTP OTPData = VerifyOTPManagement.GetByID(userID);

                            int currentTime = Convert.ToInt32(DateTime.Now.ToString("hhmm"));
                            int otpCreateTime = Convert.ToInt32(OTPData.CreatedOn.Value.ToString("hhmm"));
                            int timeDiff = currentTime - otpCreateTime;

                            if (timeDiff <= 30)
                            {
                                VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(ViewState["userID"].ToString()), Convert.ToInt32(txtOTP.Text));
                                verifiedOTP = true;
                            }
                            else
                            {
                                cvLogin.ErrorMessage = "OTP Expired.";
                                cvLogin.IsValid = false;

                                if (ViewState["WrongOTP"] != null)
                                {
                                    wrongOTPCount = Convert.ToInt32(ViewState["WrongOTP"]);
                                    ViewState["WrongOTP"] = wrongOTPCount++;
                                }

                                return;
                            }

                            if (verifiedOTP)
                            {
                                //Show Notice Details
                                if (Session["TaskDetail"] != null)
                                {
                                    Business.Data.View_NoticeCaseTaskDetail taskDetail = (Business.Data.View_NoticeCaseTaskDetail)Session["TaskDetail"];

                                    if (taskDetail != null)
                                    {
                                        lblTaskTitle.Text = taskDetail.TaskTitle;
                                        lblTaskDueDate.Text = taskDetail.ScheduleOnDate.ToString("dd-MM-yyyy");
                                        lblAssignBy.Text = taskDetail.CreatedByText;
                                        lblPriority.Text = taskDetail.Priority;
                                        lblTaskDesc.Text = taskDetail.TaskDesc;
                                        lblTaskRemark.Text = taskDetail.Remark;

                                        var docCount = ShowTaskDocCount();

                                        lblTaskDocuments.Text = docCount;

                                        if (docCount != "No Documents")
                                            lnkBtnTaskDocuments.Visible = true;
                                        else
                                            lnkBtnTaskDocuments.Visible = false;

                                        //lnkBtnTaskDocuments.Text = ShowTaskDocCount();

                                        ViewState["WrongOTP"] = 0;
                                        Page.Title = "Task Response::Avantis";

                                        //Bind Task Responses Log Details
                                        BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));

                                        panelOTP.Visible = false;
                                        panelTaskDetail.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                cvLogin.ErrorMessage = "OTP Expired.";
                                cvLogin.IsValid = false;
                                return;
                            }
                        }
                        else
                        {
                            if (ViewState["WrongOTP"] != null)
                            {
                                wrongOTPCount = Convert.ToInt32(ViewState["WrongOTP"]);
                                wrongOTPCount = wrongOTPCount + 1;
                                ViewState["WrongOTP"] = wrongOTPCount;
                            }

                            if (wrongOTPCount <= 3)
                            {
                                cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                                cvLogin.IsValid = false;
                                return;
                            }
                            else
                            {
                                //Expire Access URL 
                                if (ViewState["TaskID"] != null)
                                {
                                    LitigationTaskManagement.ExpireTaskURL(Convert.ToInt32(ViewState["TaskID"]), userID);

                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Access URL got expired, Please contact assignor to re-generate new URL.";
                                    return;
                                }
                                else
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Access URL got expired, Please contact assignor to re-generate new URL.";
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                        cvLogin.IsValid = false;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        public void clearTaskResponseControls()
        {
            try
            {
                ddlStatus.ClearSelection();
                tbxTaskResComment.Text = "";
                fuTaskResponseDocUpload.Attributes.Clear();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableTaskControls(bool flag)
        {
            try
            {
                pnlTaskDetail.Enabled = flag;
                pnlTaskResponse.Enabled = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTaskResponses(int taskID)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();

                grdTaskResponseLog.DataSource = lstTaskResponses;
                grdTaskResponseLog.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnSaveTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    long newResponseID = 0;
                    long taskID = 0;
                    taskID = Convert.ToInt32(ViewState["TaskID"]);

                    int userID = 0;

                    if (taskID != 0)
                    {
                        if (ViewState["userID"] != null)
                        {
                            userID = Convert.ToInt32(ViewState["userID"]);

                            if (userID != 0)
                            {
                                if (tbxTaskResComment.Text != "")
                                {
                                    validateData = true;
                                }
                                else
                                {
                                    cvTaskResponse.IsValid = false;
                                    cvTaskResponse.ErrorMessage = "Provide Response Description.";
                                    return;
                                }
                            }
                        }
                    }
                    
                    if (validateData)
                    {
                        tbl_TaskResponse newRecord = new tbl_TaskResponse()
                        {
                            IsActive = true,
                            TaskID = taskID,
                            ResponseDate = DateTime.Now,
                            Description = tbxTaskResComment.Text,
                            CreatedBy = userID,      
                            UserID=userID                      
                        };

                        if (tbxTaskResComment.Text != "")
                            newRecord.Remark = tbxTaskResComment.Text;

                        newResponseID = LitigationTaskManagement.CreateTaskResponseLog(newRecord);

                        if (newResponseID > 0)
                            saveSuccess = true;
                    }

                    if (saveSuccess)
                    {
                        //Save Notice Response Uploaded Documents
                        #region Upload Document

                        if (fuTaskResponseDocUpload.HasFiles)
                        {
                            tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = taskID,
                                DocTypeInstanceID = newResponseID,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,
                                DocType = "TR"
                            };

                            HttpFileCollection fileCollection1 = Request.Files;

                            if (fileCollection1.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                int customerID = 0;
                                
                                string directoryPath1 = "";

                                if (newResponseID > 0)
                                {
                                    directoryPath1 = Server.MapPath("~/LitigationDocuments/"+ customerID + "/TaskResponseDocs/" + Convert.ToInt32(newResponseID));

                                    DocumentManagement.CreateDirectory(directoryPath1);

                                    for (int i = 0; i < fileCollection1.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection1[i];
                                        string[] keys1 = fileCollection1.Keys[i].Split('$');
                                        String fileName1 = "";
                                        if (keys1[keys1.Count() - 1].Equals("fuTaskResponseDocUpload"))
                                        {
                                            fileName1 = uploadedFile.FileName;
                                        }
                                        Guid fileKey1 = Guid.NewGuid();
                                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                        Stream fs = uploadedFile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            objNoticeDoc.FileName = fileName1;
                                            objNoticeDoc.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objNoticeDoc.FileKey = fileKey1.ToString();
                                            objNoticeDoc.Version = "1.0";
                                            objNoticeDoc.VersionDate = DateTime.UtcNow;
                                            objNoticeDoc.CreatedOn = DateTime.Now.Date;
                                            objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                            if (!NoticeManagement.ExistsNoticeDocumentMapping(objNoticeDoc))
                                            {
                                                DocumentManagement.Contract_SaveDocFiles(Filelist1);
                                                saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);
                                            }
                                        }
                                    }//End For Each                                    
                                }
                            }
                        }

                        #endregion                        

                        if (saveSuccess)
                        {
                            //Update Task Status to Submitted
                            saveSuccess = LitigationTaskManagement.UpdateTaskStatus(taskID, 2, userID, AuthenticationHelper.CustomerID); //Status 2 - Submitted

                            clearTaskResponseControls();

                            cvTaskResponse.IsValid = false;
                            cvTaskResponse.ErrorMessage = "Response Detail Save Successfully.";
                        }

                        //Re-Bind Responses Log Details
                        BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskResponseControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    grdTaskResponseLog.PageIndex = e.NewPageIndex;

                    BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);
                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID,taskResponseID);
                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {                                            
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);
                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }                                            
                                            }                                                
                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "Response Deleted Successfully.";
                    }
                    else
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void manipulatedEmail(string Email)
        {
            // For Email Id Logic Set.
            string[] spl = Convert.ToString(Email).Split('@');
            int lenthusername = (spl[0].Length / 2);
            int mod = spl[0].Length % 2;
            if (mod > 0)
            {
                lenthusername = lenthusername - mod;
            }
            string beforemanupilatedemail = "";
            if (lenthusername == 1)
            {
                beforemanupilatedemail = spl[0].Substring(lenthusername);
            }
            else
            {
                beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
            }

            string appendstar = "";
            for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
            {
                appendstar += "*";
            }

            string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

            string[] spl1 = spl[1].Split('.');
            int lenthatname = (spl1[0].Length / 2);
            int modat = spl1[0].Length % 2;
            if (modat > 0)
            {
                lenthatname = lenthatname - modat;
            }

            string beforatemail = "";
            if (lenthatname == 1)
            {
                beforatemail = spl1[0].Substring(lenthatname);
            }
            else
            {
                beforatemail = spl1[0].Substring(lenthatname + 1);
            }
            string appendstar1 = "";
            for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
            {
                appendstar1 += "*";
            }

            string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
            string emailid = manupilatedemail;

            DateTime NewTime = DateTime.Now.AddMinutes(30);
            Time.Text = NewTime.Hour + ":" + NewTime.Minute + NewTime.ToString(" tt");
            email.Text = Convert.ToString(Email).Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);
        }

        public void manipulatedContactNo(string contactNo)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(contactNo)))
            {
                // For Mobile Number Logic Set.
                if (contactNo.Length == 10 && contactNo.StartsWith("9") || contactNo.StartsWith("8") || contactNo.StartsWith("7"))
                {
                    string s = Convert.ToString(contactNo);
                    int l = s.Length;
                    s = s.Substring(l - 4);
                    string r = new string('*', l - 4);
                    //r = r + s;
                    mobileno.Text = r + s;
                }
                else
                {
                    mobileno.Text = "";
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                }
            }
            else
            {
                mobileno.Text = "";
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
            }
        }

        public bool sendOTP(long taskID, string Email, string contactNumber)
        {
            try
            {
                bool successSendOTP = true;

                //Generate Random Number 6 Digit For OTP.
                Random random = new Random();
                int value = random.Next(1000000);
                long ContactNo;
                
                VerifyOTP OTPData = new VerifyOTP()
                {
                    UserId = Convert.ToInt32(taskID),
                    EmailId = Email,
                    OTP = Convert.ToInt32(value),
                    CreatedOn = DateTime.Now,
                    IsVerified = false
                };

                bool OTPresult = long.TryParse(contactNumber, out ContactNo);

                if (OTPresult)                
                    OTPData.MobileNo = ContactNo;                
                else                
                    OTPData.MobileNo = 0;

                VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.

                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                try
                {
                    //Send Email on User Mail Id.
                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                }
                catch (Exception ex)
                {
                    successSendOTP = false;
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                if (contactNumber.Length == 10 && (contactNumber.StartsWith("9") || contactNumber.StartsWith("8") || contactNumber.StartsWith("7")))
                {
                    try
                    {
                        //Send SMS on User Mobile No.                        
                        var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                        if (data != null)
                        {
                            SendSms.sendsmsto(contactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                        }
                        else
                        {
                            SendSms.sendsmsto(contactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                        }
                    }
                    catch (Exception ex)
                    {
                        successSendOTP = false;
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                return successSendOTP;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowTaskDocCount()
        {
            try
            {
                if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
                {
                    long taskID = Convert.ToInt32(ViewState["TaskID"]);
                    long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                    if (taskID != 0 && noticeCaseInstanceID != 0)
                    {
                        var docCount = LitigationTaskManagement.GetTaskDocumentsCount(taskID, noticeCaseInstanceID);

                        if (docCount == 0)
                            return "No Documents";
                        else if (docCount == 1)
                            return "1 File";
                        else if (docCount > 1)
                            return docCount + " Files";
                        else
                            return "";
                    }
                    else
                        return "";
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void btnDownloadTaskDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
                {
                    long taskID = Convert.ToInt32(ViewState["TaskID"]);
                    long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                    var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(taskID, noticeCaseInstanceID);

                    if (lstTaskDocument.Count > 0)
                    {
                        using (ZipFile responseDocZip = new ZipFile())
                        {
                            int i = 0;
                            foreach (var file in lstTaskDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    //string[] filename = file.Name.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];

                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            responseDocZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                    }
                    else
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
