﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractMilestoneListNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractMilestoneListNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fopenpopup() {
            $('#divContDocTypeDialog').modal('show');
        }

        function OpenDocTypePopup(MilestoneId, ContractID) {
            
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#AddDocumentTypePopUp').modal('show');
            $('.modal-dialog').css('width', '95%');
            $("#IframeDocType").attr('width', '100%');
            $("#IframeDocType").attr('height', modalHeight + "px");
            debugger;
            $('#IframeDocType').attr('src', "/ContractProduct/aspxPages/MileStoneUpdate.aspx?AccessID=" + ContractID + "&MilestoneID=" + MilestoneId);            
        }

        function CloseDocTypePopup() {
            $('#AddDocumentTypePopUp').modal('hide');
              document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('My Workspace/ Milestone');
        });

    </script>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <asp:ValidationSummary ID="vsPageDocType" runat="server" Display="none" class="alert alert-block alert-danger fade in" 
                    ValidationGroup="PageDocTypeValidationGroup" />
                <asp:CustomValidator ID="cvPageDocType" runat="server" EnableClientScript="False"
                    ValidationGroup="PageDocTypeValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>
            <div class="row">
             <div class="col-md-12 plr0 text-right">
              <div class="col-md-2 text-left" style="padding-left: 1px;">
               <asp:DropDownList runat="server" ID="DrpmilestoneContract"
                   AllowSingleDeselect="false"
                   class="form-control" Width="100%" DataPlaceHolder="Select Contract">
               </asp:DropDownList>
           </div>
           <div class="col-md-2 text-left" style="padding-left: 1px;">
               <asp:DropDownList runat="server" ID="drpdownmilestone"
                   AllowSingleDeselect="false"
                   class="form-control" Width="100%" DataPlaceHolder="Select Milestone">
               </asp:DropDownList>
           </div>
            <div class="col-md-2 text-left">
                <asp:DropDownList ID="dropmilestonestatus" runat="server" CssClass="form-control">
                    <asp:ListItem Text="Select Status" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="In Progress" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Open" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Completed" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-6 text-right">
               <asp:Button ID="btnapplymilestone" Text="Apply" CssClass="btn btn-primary" runat="server" OnClick="btnapplymilestone_Click" />
            </div>
        </div>
        </div>

         
            <div class="row">
                <asp:GridView runat="server" ID="grdContDoctType" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" AllowSorting="true" GridLines="none" Width="100%"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                    OnRowCreated="grdContDoctType_RowCreated" 
                    OnRowDataBound="grdContDoctType_RowDataBound"
                     OnRowCommand="grdContDoctType_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                            <ItemTemplate>
                               <%#Container.DataItemIndex+1 %>
                                          <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                       <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Milestone Title" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                            <asp:Label ID="lbl21" runat="server" Text='<%# Eval("Title") %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Title") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Description" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                            <asp:Label ID="lbl4" runat="server" Text='<%# Eval("Description") %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Department" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                            <asp:Label ID="lbl2" runat="server" Text='<%# Eval("DeptName") %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Start Date" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                            <asp:Label ID="lblstartDate" runat="server" Text='<%# Eval("StartDate") != null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StartDate") != null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="End Date" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                            <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("Status") %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Status") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditContractDocType" runat="server"
                                Visible='<%# Eval("StatusID").ToString() == "3" ? false : true %>'
                                 CommandName="EDIT_Milestone" ToolTip="Edit/View Milestone Details" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID")+","+ Eval("ContractTemplateID")%>'>
                                    <img src='<%# ResolveUrl("/Images/edit_icon_new.png")%>' alt="Edit/View Milestone Details" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">                                
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width:100%; float: right; margin-right:6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                        <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                </asp:LinkButton>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
                      
        </div>
    </div>
    <div class="modal fade" id="AddDocumentTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                        Milestone Detail(s)</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="IframeDocType" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>   
 <%--   <div class="modal fade" id="AddDocumentTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                        Milestone Detail(s)</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDocType" runat="server" frameborder="0"  width="95%" height="80%"></iframe>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

