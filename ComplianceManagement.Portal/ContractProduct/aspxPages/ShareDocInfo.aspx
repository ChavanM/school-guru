﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShareDocInfo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ShareDocInfo" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     
    <title></title>
     <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>


    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script src="/Newjs/bootstrap-tagsinput.js"></script>
    <link href="/NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />

    <link href="/NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <script src="/Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="/NewCSS/tag-scrolling.css" rel="stylesheet" />

    <script src="https://avacdn.azureedge.net/newjs/jquery.mentionable.js"></script>
    <link href="https://avacdn.azureedge.net/newcss/jquery.mentionable.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            BindControls();
        });
        //BindControls();
         function CloseMe() {            
             window.parent.CloseUploadShareDocumentPopup();
         }
         function ClosePopVendor() {
             $('#AddVendorPopUp').modal('hide');
                document.getElementById('<%= lnkBtnRebind_Vendor.ClientID %>').click();
         }
        function OpenAddNewVendorPopup()
        {
             $('#AddVendorPopUp').modal('show');
             $('#IframeParty').attr('src', "/ContractProduct/Masters/AddVendor.aspx");
         }
         function BindControls()
         {
 		var startDate = new Date();
             $(function () {
                 $('input[id*=txtfromdt]').datepicker(
                     {
                         dateFormat: 'mm-dd-yy',                      
                         numberOfMonths: 1,
			 minDate: startDate,
                         changeMonth: true,
                         changeYear: true,
                         buttonImageOnly: true,
                     });
             });

             $(function () {
                 $('input[id*=txttodate]').datepicker(
                     {
                         dateFormat: 'mm-dd-yy',
                         numberOfMonths: 1,
			 minDate: startDate,
                         changeMonth: true,
                         changeYear: true,
                         buttonImageOnly: true,
                     });
             });
         }
         function closed() {

         }
    </script>
     <style type="text/css">
        .bootstrap-tagsinput {
            width: 100% !important;
            /*white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;*/
        }

        [class="bootstrap-tagsinput"] input {
            width: 100% !important;
        }
    </style>
</head>
<body class="bgColor-white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smDocInfoPage" EnablePartialRendering="true" runat="server"></asp:ScriptManager>

            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="form-group required col-md-12">
                            <asp:ValidationSummary ID="vsDocInfo" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="vsDocInfoValidationGroup" />
                            <asp:CustomValidator ID="cvDocInfo" runat="server" EnableClientScript="False"
                                ValidationGroup="vsDocInfoValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div class="row form-group" style="display:none;">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocFileName" class="control-label">File</label>
                            <asp:Label ID="lblFileID" runat="server" CssClass="text-label" Visible="false"></asp:Label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblDocFileName" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                    <div class="row form-group" style="display:none;">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblFileType" class="control-label">Type</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblFileType" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                    <div class="row form-group" style="display:none;">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblUpdloadedBy" class="control-label">Uploaded By</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblUpdloadedBy" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                    <div class="row form-group" style="display:none;">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblUpdloadedOn" class="control-label">Uploaded On</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblUpdloadedOn" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                      <div class="row form-group" style="display:none;">
                        <div class="col-md-3 w25per float-left">
                            <label for="LabelLocation" class="control-label">Physical Location</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="LabelLocation" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                      <div class="row form-group" style="display:none;">
                        <div class="col-md-3 w25per float-left">
                            <label for="LabelFileNumber" class="control-label">File Number</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="LabelFileNumber" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                      <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocType" class="control-label">Vendor</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:DropDownListChosen runat="server" ID="ddlVendor" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                DataPlaceHolder="Select" class="form-control" Width="50%" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged"/>
                               <asp:ImageButton ID="lnkShowAddNewVendorModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenAddNewVendorPopup(); return false;"
                               data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Vendor" />
                            <asp:LinkButton ID="lnkBtnRebind_Vendor" OnClick="lnkBtnRebind_Vendor_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                            </asp:LinkButton>
                        </div>
                    </div>

                      <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocType" class="control-label ">Email</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                              <asp:TextBox runat="server" ID="txtEmail" Width="50%" CssClass="form-control"/>
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                        ValidationGroup="vsDocInfoValidationGroup" ErrorMessage="Please enter valid Email ID."
                                        ControlToValidate="txtEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                        <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocType" class="control-label">Contact no.</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                              <asp:TextBox runat="server" ID="txtcontactNum" Width="50%" CssClass="form-control" MaxLength="10" autocomplete="off"/>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                        ValidationGroup="vsDocInfoValidationGroup" ErrorMessage="Please enter a valid contact number."
                                        ControlToValidate="txtcontactNum" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtcontactNum" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                        ValidationGroup="vsDocInfoValidationGroup" ErrorMessage="Please enter 10 digit contact number."
                                        ControlToValidate="txtcontactNum" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                      <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocType" class="control-label">Period</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:DropDownListChosen runat="server" ID="ddlPeriod" DataPlaceHolder="Select Period"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                                AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="50%">
                                <asp:ListItem Text="One Month" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Three Month" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Six Month" Value="3"></asp:ListItem>
                                <asp:ListItem Text="One Year" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Permanent" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Others" Value="6"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                    <div class="row form-group" id="divfrmdt" runat="server">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocType" class="control-label">From Date</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <div class="input-group date">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar color-black"></span>
                                </span>
                                <asp:TextBox ID="txtfromdt" runat="server" placeholder="DD-MM-YYYY" Width="50%" class="form-control" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group" id="divtodt" runat="server">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocType" class="control-label">To Date</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <div class="input-group date">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar color-black"></span>
                                </span>
                                <asp:TextBox ID="txttodate" runat="server" placeholder="DD-MM-YYYY" Width="50%" class="form-control" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group required col-md-12 text-center">
                            <asp:Button Text="Save" runat="server" ID="btnUpdateDocInfo" CssClass="btn btn-primary"
                                OnClick="btnUpdateDocInfo_Click" ValidationGroup="vsDocInfoValidationGroup"></asp:Button>
                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" 
                                data-dismiss="modal" OnClientClick="CloseMe();" />
                        </div>
                    </div>

                    <div id="divDocument" runat="server" class="row col-md-12 plr0">
                                                    <asp:UpdatePanel ID="upContractDocUploadPopup" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView runat="server" ID="grdContractDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                GridLines="None" PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                PagerSettings-Position="Bottom" OnPageIndexChanging="grdContractDocuments_PageIndexChanging"
                                                                OnRowCommand="grdContractDocuments_RowCommand" PagerStyle-HorizontalAlign="Right">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <%#Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Vendor Name" ItemStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblVendorName" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("VendorName") %>' Text='<%# Eval("VendorName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Period" ItemStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPeriodDetail" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("PeriodDetail") %>' Text='<%# Eval("PeriodDetail") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                      <asp:TemplateField HeaderText="File Name" ItemStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFileName" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("FileName") %>' Text='<%# Eval("FileName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                   <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate") != DBNull.Value ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StartDate") != DBNull.Value ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                      <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : ""%>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : ""%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                       <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                <ContentTemplate>
                                                                                      <asp:LinkButton CommandArgument='<%# Eval("Id")%>' AutoPostBack="true" CommandName="DeleteContDoc"
                                                                                        OnClientClick="return confirm('Are you certain you want to unshare this document?');"
                                                                                        ID="lnkBtnDeleteContractDoc" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="Unshare">
                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Unshare" /> 
                                                                                    </asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeleteContractDoc" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpdateDocInfo" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
         <%--Add New Vendor--%>
            <div class="modal fade" id="AddVendorPopUp" tabindex="-1" style="overflow: hidden;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w90per">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Vendor</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopVendor()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeParty" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</body>
</html>
