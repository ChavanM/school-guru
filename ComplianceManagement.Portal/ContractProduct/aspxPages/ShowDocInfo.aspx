﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowDocInfo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ShowDocInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script src="/Newjs/bootstrap-tagsinput.js"></script>
    <link href="/NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />

    <link href="/NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <script type="text/javascript">
         function CloseMe() {            
            window.parent.CloseUploadDocumentPopup(1);
        }
    </script>
     <style type="text/css">
        .bootstrap-tagsinput {
            width: 100% !important;
            /*white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;*/
        }

        [class="bootstrap-tagsinput"] input {
            width: 100% !important;
        }
    </style>
</head>
<body class="bgColor-white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smDocInfoPage" EnablePartialRendering="true" runat="server"></asp:ScriptManager>

            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="form-group required col-md-12">
                            <asp:ValidationSummary ID="vsDocInfo" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="vsDocInfoValidationGroup" />
                            <asp:CustomValidator ID="cvDocInfo" runat="server" EnableClientScript="False"
                                ValidationGroup="vsDocInfoValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocFileName" class="control-label">File</label>
                            <asp:Label ID="lblFileID" runat="server" CssClass="text-label" Visible="false"></asp:Label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblDocFileName" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblFileType" class="control-label">Type</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblFileType" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblUpdloadedBy" class="control-label">Uploaded By</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblUpdloadedBy" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblUpdloadedOn" class="control-label">Uploaded On</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblUpdloadedOn" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                      <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="LabelLocation" class="control-label">Physical Location</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="LabelLocation" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                      <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="LabelFileNumber" class="control-label">File Number</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="LabelFileNumber" runat="server" CssClass="text-label"></asp:Label>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="lblDocType" class="control-label">Document Type</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:DropDownListChosen runat="server" ID="ddlDocType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                DataPlaceHolder="Select" class="form-control" Width="50%" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-3 w25per float-left">
                            <label for="txtFileTags" class="control-label">Tags</label>
                        </div>
                        <div class="col-md-9 w75per float-left">
                            <asp:Label ID="lblFileTags" runat="server" CssClass="text-label" Visible="false"></asp:Label>
                            <asp:TextBox runat="server" ID="txtFileTags" CssClass="form-control" ClientIDMode="Static" data-role="tagsinput"
                                autocomplete="off" Width="100%" Rows="1" Style="border: 1px solid #ccc;" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group required col-md-12 text-center">
                            <asp:Button Text="Update" runat="server" ID="btnUpdateDocInfo" CssClass="btn btn-primary"
                                OnClick="btnUpdateDocInfo_Click" ValidationGroup="vsDocInfoValidationGroup"></asp:Button>
                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" 
                                data-dismiss="modal" OnClientClick="CloseMe();" />
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpdateDocInfo" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
