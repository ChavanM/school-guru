﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractTemplateList : System.Web.UI.Page
    {
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int userID = -1;
                    userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                    BindRole(userID, customerID);
                    BindVendors();

                    //Bind Tree Views
                    //var branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                    List<NameValueHierarchy> branchList;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branchList);
                    }
                    else
                    {
                        branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branchList);
                    }

                    //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);

                    BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);
                    BindContractCategoryType();
                    BindDepartment();
                    BindContractStatusList(false, true);

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        string queryStr = Request.QueryString["Status"].ToString().Trim();
                        if (queryStr == "0")
                        {
                            Session.Remove("ContStatus");
                        }
                        else
                        {
                            Session["ContStatus"] = queryStr;
                            if (ddlContractStatus.Items.FindByText(queryStr) != null)
                            {
                                ddlContractStatus.ClearSelection();
                                ddlContractStatus.Items.FindByText(queryStr).Selected = true;
                            }
                        }
                    }


                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindRole(int UserId,int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.ContractTemplateAssignments
                             where row.UserID == UserId
                             && row.IsActive == true
                             //&& row.Visibility == 1
                             select row).ToList();
                if (Query.Count > 0)
                {
                    int Revflag = Query.Where(x => x.RoleID == 4).ToList().Count;
                    int Appflag = Query.Where(x => x.RoleID == 6).ToList().Count;
                    int PrimaryAppflag = Query.Where(x => x.RoleID == 29).ToList().Count;
                    if (Revflag > 0 && Appflag > 0 && PrimaryAppflag > 0)
                    {
                        ddlContractRole.Items.Insert(0, new ListItem("Reviwer", "4"));
                        ddlContractRole.Items.Insert(1, new ListItem("Approver", "6"));
                        ddlContractRole.Items.Insert(2, new ListItem("Primary Approver", "29"));

                        ddlContractRole.DataTextField = "Text";
                        ddlContractRole.DataValueField = "Value";
                        ddlContractRole.DataBind();

                        ddlContractRole.SelectedValue = "4";
                    }
                    else if (Revflag > 0 && Appflag > 0)
                    {
                        ddlContractRole.Items.Insert(0, new ListItem("Reviwer", "4"));
                        ddlContractRole.Items.Insert(1, new ListItem("Approver", "6"));

                        ddlContractRole.DataTextField = "Text";
                        ddlContractRole.DataValueField = "Value";
                        ddlContractRole.DataBind();

                        ddlContractRole.SelectedValue = "4";
                    }
                    else if (Revflag > 0 && PrimaryAppflag > 0)
                    {
                        ddlContractRole.Items.Insert(0, new ListItem("Reviwer", "4"));
                        ddlContractRole.Items.Insert(1, new ListItem("Primary Approver", "29"));

                        ddlContractRole.DataTextField = "Text";
                        ddlContractRole.DataValueField = "Value";
                        ddlContractRole.DataBind();

                        ddlContractRole.SelectedValue = "4";
                    }
                    else if (Appflag > 0 && PrimaryAppflag > 0)
                    {
                        ddlContractRole.Items.Insert(0, new ListItem("Approver", "6"));
                        ddlContractRole.Items.Insert(1, new ListItem("Primary Approver", "29"));

                        ddlContractRole.DataTextField = "Text";
                        ddlContractRole.DataValueField = "Value";
                        ddlContractRole.DataBind();

                        ddlContractRole.SelectedValue = "29";
                    }
                    else if (PrimaryAppflag > 0)
                    {
                        ddlContractRole.Items.Insert(0, new ListItem("Primary Approver", "29"));

                        ddlContractRole.DataTextField = "Text";
                        ddlContractRole.DataValueField = "Value";
                        ddlContractRole.DataBind();

                        ddlContractRole.SelectedValue = "29";
                    }
                    else if (Revflag > 0)
                    {
                        ddlContractRole.Items.Insert(0, new ListItem("Reviwer", "4"));

                        ddlContractRole.DataTextField = "Text";
                        ddlContractRole.DataValueField = "Value";
                        ddlContractRole.DataBind();

                        ddlContractRole.SelectedValue = "4";
                    }
                    else if (Appflag > 0)
                    {
                        ddlContractRole.Items.Insert(0, new ListItem("Approver", "6"));

                        ddlContractRole.DataTextField = "Text";
                        ddlContractRole.DataValueField = "Value";
                        ddlContractRole.DataBind();

                        ddlContractRole.SelectedValue = "6";
                    }
                }
            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                tbxFilterLocation.Text = "Select Entity/Location";

                TreeNode node = new TreeNode("Select Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }
        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                if (lstContractTypes.Count > 0)
                    lstContractTypes = lstContractTypes.OrderBy(row => row.TypeName).ToList();

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractTypes;
                ddlContractType.DataBind();

                ddlContractType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("Select Department", "-1"));
        }
        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = VendorDetails.GetVendors_All(customerID);

            ddlVendorPage.DataTextField = "VendorName";
            ddlVendorPage.DataValueField = "ID";

            ddlVendorPage.DataSource = lstVendors;
            ddlVendorPage.DataBind();

            ddlVendorPage.Items.Insert(0, new ListItem("Select Vendor", "-1"));
        }
        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                ddlContractStatus.Items.Insert(0, new ListItem("Select Status", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindGrid()
        {
            try
            {
                if (Session["ContStatus"] != null)
                {
                    string ContStatus = Session["ContStatus"].ToString();
                    if (ddlContractStatus.Items.FindByText(ContStatus) != null)
                    {
                        ddlContractStatus.ClearSelection();
                        ddlContractStatus.Items.FindByText(ContStatus).Selected = true;
                    }

                }
                int RoleID = -1;
                if (ddlContractRole.SelectedValue != "" && ddlContractRole.SelectedValue != "-1")
                {
                    RoleID = Convert.ToInt32(ddlContractRole.SelectedValue);
                }
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                
                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                {
                    contractTypeID = Convert.ToInt64(ddlContractType.SelectedValue);
                }
                

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    RoleID, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                #region Filter Code
                if (!string.IsNullOrEmpty(tbxFilter1.Text))
                {
                    if (CheckInt(tbxFilter1.Text))
                    {
                        int a = Convert.ToInt32(tbxFilter1.Text.ToUpper());
                        lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ID == a || entry.ContractNo.ToUpper().Contains(tbxFilter1.Text.ToUpper())).ToList();
                    }
                    else
                    {
                        lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ContractTitle.ToUpper().Contains(tbxFilter1.Text.ToUpper()) || entry.ContractNo.ToUpper().Contains(tbxFilter1.Text.ToUpper()) || entry.StatusName.ToUpper().Contains(tbxFilter1.Text.ToUpper())).ToList();
                    }
                }
                #endregion


                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstAssignedContracts = lstAssignedContracts.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstAssignedContracts = lstAssignedContracts.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }



                Session["TotalRows"] = null;
                if (lstAssignedContracts.Count > 0)
                {
                    flag = true;
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = lstAssignedContracts.Count;
                }
                else
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = null;
                }
                lstAssignedContracts.Clear();
                lstAssignedContracts = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }
        protected void tbxFilter1_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkContract_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/aspxPages/ContractList.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkTask_Click(object sender, EventArgs e)
        {
            try
            {

                Response.Redirect("~/ContractProduct/aspxPages/ContractTaskList.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                //var StartRecord = DropDownListPageNo.SelectedItem.Text;
                //var TotalRecord = Session["TotalRows"].ToString();
                //lblStartRecord.Text = StartRecord.ToString();
                //lblEndRecord.Text = Convert.ToString(count) + " ";
                //lblTotalRecord.Text = TotalRecord.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContractList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnGenerateTxn_Click(object sender, EventArgs e)
        {
            try
            {
                int totalStatusTxn = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var allContracts = (from row in entities.Cont_tbl_ContractInstance
                                        where row.IsDeleted == false
                                        select row).ToList();

                    if (allContracts.Count > 0)
                    {
                        allContracts.ForEach(eachContract =>
                        {
                            Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                            {
                                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                ContractID = eachContract.ID,
                                StatusID = 1,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                            };

                            var alreadyExistTxn = (from row in entities.Cont_tbl_ContractStatusTransaction
                                                   where row.ContractID == eachContract.ID
                                                   where row.IsActive == true
                                                   select row).FirstOrDefault();
                            if (alreadyExistTxn == null)
                            {
                                ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                totalStatusTxn++;
                            }

                        });

                        cvErrorContractListPage.IsValid = false;
                        cvErrorContractListPage.ErrorMessage = totalStatusTxn + "Contract Txn Created.";

                    }


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void lnkEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long contractInstanceID = Convert.ToInt64(btn.CommandArgument);
                    int RoleID = -1;
                    if (ddlContractRole.SelectedValue != "" && ddlContractRole.SelectedValue != "-1")
                    {
                        RoleID = Convert.ToInt32(ddlContractRole.SelectedValue);
                    }
                    if (contractInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + contractInstanceID + "," + RoleID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdContractList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = AuthenticationHelper.UserID;

                if (e.CommandName.Equals("DELETE_Contract"))
                {
                    long contractID = Convert.ToInt64(e.CommandArgument);

                    if (contractID != 0)
                    {
                        bool deleteSuccess = ContractManagement.DeleteContractByID(contractID, userID);

                        if (deleteSuccess)
                        {
                            BindGrid();
                            bindPageNumber();
                            ShowGridDetail();
                        }

                        if (deleteSuccess)
                        {
                            cvErrorContractListPage.IsValid = false;
                            cvErrorContractListPage.ErrorMessage = "Contract Deleted Successfully";
                            vsContractListPage.CssClass = "alert alert-success fade-in";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContractList.PageIndex = chkSelectedPage - 1;

            grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
            ShowGridDetail();
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlDeptPage.ClearSelection();
                ddlVendorPage.ClearSelection();
                ddlContractStatus.ClearSelection();
                ddlContractType.ClearSelection();
                tbxFilter1.Text = string.Empty;
                ClearTreeViewSelection(tvFilterLocation);

                tvFilterLocation_SelectedNodeChanged(sender, e);
                lnkBtnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Select Entity/Location";
                // tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddContract_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + 0 + ");", true);
        }

        protected void grdContractList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int RoleID = -1;
                if (ddlContractRole.SelectedValue != "" && ddlContractRole.SelectedValue != "-1")
                {
                    RoleID = Convert.ToInt32(ddlContractRole.SelectedValue);
                }

                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    RoleID, branchList, vendorID, deptID, contractStatusID, contractTypeID);


                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstAssignedContracts = lstAssignedContracts.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstAssignedContracts = lstAssignedContracts.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdContractList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdContractList.Columns.IndexOf(field);
                    }
                }
                flag = true;
                if (lstAssignedContracts.Count > 0)
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = lstAssignedContracts.Count;
                }
                else
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        #region add Filter
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }

        #endregion

        protected void lnkRev_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/aspxPages/ContractTemplateList.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}