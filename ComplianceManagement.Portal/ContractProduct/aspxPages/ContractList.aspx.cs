﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractList : System.Web.UI.Page
    {
        protected bool flag;
        protected static string ContractAssigned;
        protected static List<Cont_tbl_VisibleToUser> Cont_tbl_VisibleToUserData = null;
        protected static List<Cont_tbl_ContractCustomerDetail> Cont_tbl_ContractCustomerDetaildata = null;
        public static bool HideStatusOnActiveEnable;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int customerID = -1;
                    int userid = -1;
                    if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                    {
                        ContractAssigned = "none";
                    }
                    else
                    {
                        ContractAssigned = "block";
                    }
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    userid = AuthenticationHelper.UserID;
                    HideStatusOnActiveEnable = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "HideStatusOnActive");
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {                          
                        var uid = Convert.ToString(AuthenticationHelper.UserID);
                        Cont_tbl_VisibleToUserData = (from row in entities.Cont_tbl_VisibleToUser
                                                      join row1 in entities.Cont_tbl_ContractInstance
                                                      on row.ContractInstanceID equals row1.ID
                                                      join row2 in entities.Cont_tbl_UserAssignment
                                                      on row.ContractInstanceID equals row2.ContractID
                                                      where row.IsActive == true
                                                      && row.CustomerID == customerID
                                                      && row1.CreatedBy != userid
                                                      && row2.UserID != userid
                                                      //&& row.ContractInstanceID == cinstanceid
                                                      && row.VisibkeUserID == uid
                                                      select row).ToList();

                        Cont_tbl_ContractCustomerDetaildata = (from row in entities.Cont_tbl_ContractCustomerDetail
                                       where row.IsActive == true
                                       select row).ToList();

                        BindVendors();
                        BindUsers();
                    }
                    //Bind Tree Views
                   // var branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                    List<NameValueHierarchy> branchList;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branchList);
                    }
                    else
                    {
                        branchList = CustomerBranchManagement.GetAllContractEntitiesAssignedHierarchyManagementSatutory(Convert.ToInt32(customerID), userid, AuthenticationHelper.Role);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branchList);
                    }
                    //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);

                    BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);
                    BindContractCategoryType();
                    BindDepartment();
                    BindContractStatusList(false, true);

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        string queryStr = Request.QueryString["Status"].ToString().Trim();
                        if (queryStr == "0")
                        {
                            Session.Remove("ContStatus");
                        }
                        else
                        {
                            Session["ContStatus"] = queryStr;
                            if (ddlContractStatus.Items.FindByText(queryStr) != null)
                            {
                                ddlContractStatus.ClearSelection();
                                ddlContractStatus.Items.FindByText(queryStr).Selected = true;
                            }
                        }
                        if (Request.QueryString["Status"] == "Terminated")
                        {
                            lblNote.Visible = true;
                        }
                    }

                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool CanChangeStatus(string Status)
        {
            try
            {
                bool result = true;
                if (HideStatusOnActiveEnable)
                {
                    if (Status == "Active")
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var key = "Contusers-" + Convert.ToString(customerID);
                var lstAllUsers = (List<User>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null)
                {
                    lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                    HttpContext.Current.Cache.Insert(key, lstAllUsers, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
                }

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                DropDownListOwner.DataValueField = "ID";
                DropDownListOwner.DataTextField = "Name";
                DropDownListOwner.DataSource = internalUsers;
                DropDownListOwner.DataBind();

                DropDownListOwner.Items.Insert(0, new ListItem("Select Owner", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                tbxFilterLocation.Text = "Select Entity/Location";

                TreeNode node = new TreeNode("Select Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }


        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                if (lstContractTypes.Count > 0)
                    lstContractTypes = lstContractTypes.OrderBy(row => row.TypeName).ToList();

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractTypes;
                ddlContractType.DataBind();

                ddlContractType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("Select Department", "-1"));
        }

        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = VendorDetails.GetVendors_All(customerID);

            ddlVendorPage.DataTextField = "VendorName";
            ddlVendorPage.DataValueField = "ID";

            ddlVendorPage.DataSource = lstVendors;
            ddlVendorPage.DataBind();

            ddlVendorPage.Items.Insert(0, new ListItem("Select Vendor", "-1"));
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";
                bool IsOnlycontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (IsOnlycontract)
                {
                    var statusList = ContractTaskManagement.GetStatusListAll();

                    ddlContractStatus.DataSource = statusList;
                    ddlContractStatus.DataBind();

                    ddlContractStatus.Items.Insert(0, new ListItem("Select Status", "-1"));
                }
                else
                {
                    var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                    ddlContractStatus.DataSource = statusList;
                    ddlContractStatus.DataBind();

                    ddlContractStatus.Items.Insert(0, new ListItem("Select Status", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindGrid()
        {
            try
            {
                bool IsOnlycontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (!IsOnlycontract)
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    int branchID = -1;
                    int vendorID = -1;
                    int deptID = -1;
                    long contractStatusID = -1;
                    long ContractAssign = 0;
                    long contractTypeID = -1;
                    int OwnerID = -1;

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                    {
                        OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                    {
                        if (ddlContractType.SelectedValue != null)
                        {
                            contractTypeID = Convert.ToInt32(ddlContractType.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                    {
                        vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                    {  
                            contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue); 
                    }
                    if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                    {
                        if (contractStatusID == 8 || contractStatusID == 23)
                        {
                            lblNote.Visible = true;
                        }
                        else
                        {
                            lblNote.Visible = false;
                        }
                    }
                    else
                    {
                        lblNote.Visible = false;
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                    var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                        3, branchList, vendorID, deptID, contractStatusID, contractTypeID);


                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //string CustomerID = Convert.ToString(AuthenticationHelper.CustomerID);
                    //if (AuthenticationHelper.Role == "MGMT" && IsEntityassignmentCustomer == Convert.ToString(CustomerID))
                    //ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                    //if (isexist != null)
                    //{
                    //    var assignedcontractList = CustomerBranchManagement.GetAllAssignedContractListbyuser(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role);
                    //    lstAssignedContracts = lstAssignedContracts.Where(entry => assignedcontractList.Contains(entry.ContractNo)).ToList();
                    //}


                    //var assignedcontractList = CustomerBranchManagement.GetAllAssignedContractListbyuser(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role);
                    //if (assignedcontractList.Count > 0)
                    //{
                    //    lstAssignedContracts = lstAssignedContracts.Where(entry => assignedcontractList.Contains(entry.ContractNo)).ToList();
                    //}


                    #region For All contarct and assigned contract Gaurav
                    if (!string.IsNullOrEmpty(ddlContract.SelectedValue))
                    {
                        ContractAssign = Convert.ToInt64(ddlContract.SelectedValue);
                    }
                    if (ContractAssign != 0)
                    {
                        lstAssignedContracts = lstAssignedContracts.Where(entry => entry.AssignedUserID.Contains(Convert.ToString(AuthenticationHelper.UserID))).ToList();
                    }
                    if (OwnerID != -1)
                    {
                        lstAssignedContracts = lstAssignedContracts.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();
                    }
                    #endregion

                    #region Filter Code
                    if (!string.IsNullOrEmpty(tbxFilter1.Text))
                    {
                        if (CheckInt(tbxFilter1.Text))
                        {
                            int a = Convert.ToInt32(tbxFilter1.Text.ToUpper());
                            lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ID == a || entry.ContractNo.ToUpper().Contains(tbxFilter1.Text.ToUpper()) || entry.ContractTitle.ToUpper().Contains(tbxFilter1.Text.ToUpper())).ToList();
                            //lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ID == a || entry.ContractNo.ToUpper().Contains(tbxFilter1.Text.ToUpper())).ToList();
                        }
                        else
                        {
                            lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ContractTitle.ToUpper().Contains(tbxFilter1.Text.ToUpper()) || entry.ContractNo.ToUpper().Contains(tbxFilter1.Text.ToUpper()) || entry.StatusName.ToUpper().Contains(tbxFilter1.Text.ToUpper())).ToList();
                        }
                    }

                    #endregion


                    string SortExpr = string.Empty;
                    string CheckDirection = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                        {
                            CheckDirection = Convert.ToString(ViewState["Direction"]);

                            SortExpr = Convert.ToString(ViewState["SortExpression"]);
                            if (CheckDirection == "Ascending")
                            {
                                lstAssignedContracts = lstAssignedContracts.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                            }
                            else
                            {
                                CheckDirection = "Descending";
                                lstAssignedContracts = lstAssignedContracts.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                            }
                        }
                    }



                    Session["TotalRows"] = null;
                    if (lstAssignedContracts.Count > 0)
                    {
                        flag = true;
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = lstAssignedContracts.Count;
                    }
                    else
                    {
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = null;
                    }
                    lstAssignedContracts.Clear();
                    lstAssignedContracts = null;
                }
                else
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    int branchID = -1;
                    int vendorID = -1;
                    int deptID = -1;
                    long contractStatusID = -1;
                    long ContractAssign = 0;
                    long contractTypeID = -1;
                    int OwnerID = -1;

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                    {
                        OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                    {
                        if (ddlContractType.SelectedValue != null)
                        {
                            contractTypeID = Convert.ToInt32(ddlContractType.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                    {
                        vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                    {
                        contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                    
                    List<ContractListDetail> obown = new List<ContractListDetail>();
                    List<ContractListDetail> objrejctedrev = new List<ContractListDetail>();
                    List<ContractListDetail> objrejctedappr = new List<ContractListDetail>();
                   
                    var lstAssignedContracts1 = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                        3, branchList, vendorID, deptID, contractStatusID, contractTypeID);
                    
                    obown = (from g in lstAssignedContracts1
                             group g by new
                             {
                                 g.ID, //ContractID
                                 g.CustomerID,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.ContractDetailDesc,
                                 g.VendorIDs,
                                 g.VendorNames,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.ContractTypeID,
                                 g.TypeName,
                                 g.ContractSubTypeID,
                                 g.SubTypeName,
                                 g.ProposalDate,
                                 g.AgreementDate,
                                 g.EffectiveDate,
                                 g.ReviewDate,
                                 g.ExpirationDate,
                                 g.CreatedOn,
                                 g.UpdatedOn,
                                 g.StatusName,
                                 g.ContractAmt,
                                 g.ContactPersonOfDepartment,
                                 g.PaymentType,
                                 g.AddNewClause,
                                 g.Owner,
                                 g.ContractStatusID,
                                 g.OwnerIDs
                             } into GCS
                             select new ContractListDetail()
                             {
                                 ID = GCS.Key.ID, //ContractID
                                 CustomerID = GCS.Key.CustomerID,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                 VendorIDs = GCS.Key.VendorIDs,
                                 VendorNames = GCS.Key.VendorNames,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 ContractTypeID = GCS.Key.ContractTypeID,
                                 TypeName = GCS.Key.TypeName,
                                 ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                 SubTypeName = GCS.Key.SubTypeName,
                                 ProposalDate = GCS.Key.ProposalDate,
                                 AgreementDate = GCS.Key.AgreementDate,
                                 EffectiveDate = GCS.Key.EffectiveDate,
                                 ReviewDate = GCS.Key.ReviewDate,
                                 ExpirationDate = GCS.Key.ExpirationDate,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 StatusName = GCS.Key.StatusName,
                                 ContractAmt = GCS.Key.ContractAmt,
                                 ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                 PaymentType = GCS.Key.PaymentType,
                                 AddNewClause = GCS.Key.AddNewClause,
                                 Owner = GCS.Key.Owner,
                                 ContractStatusID = GCS.Key.ContractStatusID,
                                 OwnerIDs = GCS.Key.OwnerIDs
                             }).ToList();

                    var lstAssignedContractsrejcted = ContractManagement.GetAssignedContractsonlyrejectedList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                          4, branchList, vendorID, deptID, contractStatusID, contractTypeID);


                    objrejctedrev = (from g in lstAssignedContractsrejcted
                              group g by new
                              {
                                  g.ID, //ContractID
                                  g.CustomerID,
                                  g.ContractNo,
                                  g.ContractTitle,
                                  g.ContractDetailDesc,
                                  g.VendorIDs,
                                  g.VendorNames,
                                  g.CustomerBranchID,
                                  g.BranchName,
                                  g.DepartmentID,
                                  g.DeptName,
                                  g.ContractTypeID,
                                  g.TypeName,
                                  g.ContractSubTypeID,
                                  g.SubTypeName,
                                  g.ProposalDate,
                                  g.AgreementDate,
                                  g.EffectiveDate,
                                  g.ReviewDate,
                                  g.ExpirationDate,
                                  g.CreatedOn,
                                  g.UpdatedOn,
                                  g.StatusName,
                                  g.ContractAmt,
                                  g.ContactPersonOfDepartment,
                                  g.PaymentType,
                                  g.AddNewClause,
                                  g.Owner,
                                  g.ContractStatusID,
                                  g.OwnerIDs
                              } into GCS
                              select new ContractListDetail()
                              {
                                  ID = GCS.Key.ID, //ContractID
                                  CustomerID = GCS.Key.CustomerID,
                                  ContractNo = GCS.Key.ContractNo,
                                  ContractTitle = GCS.Key.ContractTitle,
                                  ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                  VendorIDs = GCS.Key.VendorIDs,
                                  VendorNames = GCS.Key.VendorNames,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  BranchName = GCS.Key.BranchName,
                                  DepartmentID = GCS.Key.DepartmentID,
                                  DeptName = GCS.Key.DeptName,
                                  ContractTypeID = GCS.Key.ContractTypeID,
                                  TypeName = GCS.Key.TypeName,
                                  ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                  SubTypeName = GCS.Key.SubTypeName,
                                  ProposalDate = GCS.Key.ProposalDate,
                                  AgreementDate = GCS.Key.AgreementDate,
                                  EffectiveDate = GCS.Key.EffectiveDate,
                                  ReviewDate = GCS.Key.ReviewDate,
                                  ExpirationDate = GCS.Key.ExpirationDate,
                                  CreatedOn = GCS.Key.CreatedOn,
                                  UpdatedOn = GCS.Key.UpdatedOn,
                                  StatusName = GCS.Key.StatusName,
                                  ContractAmt = GCS.Key.ContractAmt,
                                  ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                  PaymentType = GCS.Key.PaymentType,
                                  AddNewClause = GCS.Key.AddNewClause,
                                  Owner = GCS.Key.Owner,
                                  ContractStatusID = GCS.Key.ContractStatusID,
                                  OwnerIDs = GCS.Key.OwnerIDs
                              }).ToList();

                    var lstAssignedApproverContractsrejcted = ContractManagement.GetAssignedContractsonlyrejectedList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                         6, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                    objrejctedappr = (from g in lstAssignedApproverContractsrejcted
                                      group g by new
                                      {
                                          g.ID, //ContractID
                                          g.CustomerID,
                                          g.ContractNo,
                                          g.ContractTitle,
                                          g.ContractDetailDesc,
                                          g.VendorIDs,
                                          g.VendorNames,
                                          g.CustomerBranchID,
                                          g.BranchName,
                                          g.DepartmentID,
                                          g.DeptName,
                                          g.ContractTypeID,
                                          g.TypeName,
                                          g.ContractSubTypeID,
                                          g.SubTypeName,
                                          g.ProposalDate,
                                          g.AgreementDate,
                                          g.EffectiveDate,
                                          g.ReviewDate,
                                          g.ExpirationDate,
                                          g.CreatedOn,
                                          g.UpdatedOn,
                                          g.StatusName,
                                          g.ContractAmt,
                                          g.ContactPersonOfDepartment,
                                          g.PaymentType,
                                          g.AddNewClause,
                                          g.Owner,
                                          g.ContractStatusID,
                                          g.OwnerIDs
                                      } into GCS
                                      select new ContractListDetail()
                                      {
                                          ID = GCS.Key.ID, //ContractID
                                          CustomerID = GCS.Key.CustomerID,
                                          ContractNo = GCS.Key.ContractNo,
                                          ContractTitle = GCS.Key.ContractTitle,
                                          ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                          VendorIDs = GCS.Key.VendorIDs,
                                          VendorNames = GCS.Key.VendorNames,
                                          CustomerBranchID = GCS.Key.CustomerBranchID,
                                          BranchName = GCS.Key.BranchName,
                                          DepartmentID = GCS.Key.DepartmentID,
                                          DeptName = GCS.Key.DeptName,
                                          ContractTypeID = GCS.Key.ContractTypeID,
                                          TypeName = GCS.Key.TypeName,
                                          ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                          SubTypeName = GCS.Key.SubTypeName,
                                          ProposalDate = GCS.Key.ProposalDate,
                                          AgreementDate = GCS.Key.AgreementDate,
                                          EffectiveDate = GCS.Key.EffectiveDate,
                                          ReviewDate = GCS.Key.ReviewDate,
                                          ExpirationDate = GCS.Key.ExpirationDate,
                                          CreatedOn = GCS.Key.CreatedOn,
                                          UpdatedOn = GCS.Key.UpdatedOn,
                                          StatusName = GCS.Key.StatusName,
                                          ContractAmt = GCS.Key.ContractAmt,
                                          ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                          PaymentType = GCS.Key.PaymentType,
                                          AddNewClause = GCS.Key.AddNewClause,
                                          Owner = GCS.Key.Owner,
                                          ContractStatusID = GCS.Key.ContractStatusID,
                                          OwnerIDs = GCS.Key.OwnerIDs
                                      }).ToList();
                                        
                    var combinedList = obown.Union(objrejctedrev).Union(objrejctedappr).Distinct().ToList();

                    combinedList = combinedList.Distinct().ToList();

                    #region For All contarct and assigned contract Gaurav
                    if (!string.IsNullOrEmpty(ddlContract.SelectedValue))
                    {
                        ContractAssign = Convert.ToInt64(ddlContract.SelectedValue);
                    }
                    if (ContractAssign != 0)
                    {
                        combinedList = combinedList.Where(entry => entry.AssignedUserID.Contains(Convert.ToString(AuthenticationHelper.UserID))).ToList();
                    }
                    if (OwnerID != -1)
                    {
                        combinedList = combinedList.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();
                    }
                    if (vendorID != -1)
                    {
                        combinedList = combinedList.Where(entry => entry.VendorIDs.Trim().Split(',').ToList().Contains(vendorID.ToString())).ToList();
                    }
                    #endregion

                    #region Filter Code
                    if (!string.IsNullOrEmpty(tbxFilter1.Text))
                    {
                        if (CheckInt(tbxFilter1.Text))
                        {
                            int a = Convert.ToInt32(tbxFilter1.Text.ToUpper());
                            combinedList = combinedList.Where(entry => entry.ID == a || entry.ContractNo.ToUpper().Contains(tbxFilter1.Text.ToUpper()) || entry.ContractTitle.ToUpper().Contains(tbxFilter1.Text.ToUpper())).ToList();
                        }
                        else
                        {
                            combinedList = combinedList.Where(entry => entry.ContractTitle.ToUpper().Contains(tbxFilter1.Text.ToUpper()) || entry.ContractNo.ToUpper().Contains(tbxFilter1.Text.ToUpper()) || entry.StatusName.ToUpper().Contains(tbxFilter1.Text.ToUpper())).ToList();
                        }
                    }
                    #endregion


                    string SortExpr = string.Empty;
                    string CheckDirection = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                        {
                            CheckDirection = Convert.ToString(ViewState["Direction"]);

                            SortExpr = Convert.ToString(ViewState["SortExpression"]);
                            if (CheckDirection == "Ascending")
                            {
                                combinedList = combinedList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                            }
                            else
                            {
                                CheckDirection = "Descending";
                                combinedList = combinedList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                            }
                        }
                    }



                    Session["TotalRows"] = null;
                    if (combinedList.Count > 0)
                    {
                        flag = true;
                        grdContractList.DataSource = combinedList;
                        grdContractList.DataBind();
                        Session["TotalRows"] = combinedList.Count;
                    }
                    else
                    {
                        grdContractList.DataSource = combinedList;
                        grdContractList.DataBind();
                        Session["TotalRows"] = null;
                    }
                    combinedList.Clear();
                    combinedList = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }
      
        protected void tbxFilter1_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkTask_Click(object sender, EventArgs e)
        {
            try
            {
               
                Response.Redirect("~/ContractProduct/aspxPages/ContractTaskList.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                //var StartRecord = DropDownListPageNo.SelectedItem.Text;
                //var TotalRecord = Session["TotalRows"].ToString();
                //lblStartRecord.Text = StartRecord.ToString();
                //lblEndRecord.Text = Convert.ToString(count) + " ";
                //lblTotalRecord.Text = TotalRecord.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContractList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(ddlTypePage.SelectedValue))
        //        {
        //            if (ddlTypePage.SelectedValue == "1")
        //                Response.Redirect("~/ContractProduct/aspxPages/ContractList.aspx", false);

        //            else if (ddlTypePage.SelectedValue == "2")
        //                Response.Redirect("~/ContractProduct/aspxPages/ContractTaskList.aspx", false);

        //            Context.ApplicationInstance.CompleteRequest();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnGenerateTxn_Click(object sender, EventArgs e)
        {
            try
            {
                int totalStatusTxn = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var allContracts = (from row in entities.Cont_tbl_ContractInstance
                                        where row.IsDeleted == false
                                        select row).ToList();

                    if (allContracts.Count > 0)
                    {
                        allContracts.ForEach(eachContract =>
                        {
                            Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                            {
                                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                ContractID = eachContract.ID,
                                StatusID = 1,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                            };

                            var alreadyExistTxn = (from row in entities.Cont_tbl_ContractStatusTransaction
                                                   where row.ContractID == eachContract.ID
                                                   where row.IsActive == true
                                                   select row).FirstOrDefault();
                            if (alreadyExistTxn == null)
                            {
                                ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                totalStatusTxn++;
                            }

                        });

                        cvErrorContractListPage.IsValid = false;
                        cvErrorContractListPage.ErrorMessage = totalStatusTxn + "Contract Txn Created.";

                    }


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    //long contractInstanceID = Convert.ToInt64(btn.CommandArgument);
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                    long contractInstanceID = Convert.ToInt64(commandArgs[0]);
                    long ContractStatusID = Convert.ToInt64(commandArgs[1]);
                    if (contractInstanceID != 0)
                    {
                        if (ContractStatusID == 1)
                        {
                            lblname.InnerText = "Edit Contract Detail(s)";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + contractInstanceID + ");", true);
                        }
                        else
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                long uid = Convert.ToInt32(AuthenticationHelper.UserID);
                                var isLegal = (from row in entities.Cont_tbl_UserAssignment
                                               where row.ContractID == contractInstanceID
                                               && row.IsActive == true
                                               && row.UserID == uid
                                               && row.RoleID == 3
                                               select row).FirstOrDefault();
                                if (isLegal != null)
                                {
                                    if (ContractStatusID != 5)
                                    {
                                        lblname.InnerText = "Edit Contract Detail(s)";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + contractInstanceID + ");", true);
                                    }
                                    else
                                    {
                                        lblname.InnerText = "Edit Contract Detail(s)";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialogforlegal(" + contractInstanceID + ");", true);
                                    }
                                }
                                else
                                {
                                    lblname.InnerText = "Edit Contract Detail(s)";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + contractInstanceID + ");", true);
                                }
                                //if (ContractStatusID != 5)
                                //{
                                //    if (contractInstanceID != 0)
                                //    {
                                //        lblname.InnerText = "Edit Contract Detail(s)";
                                //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + contractInstanceID + ");", true);
                                //    }
                                //}
                                //else
                                //{
                                //    if (contractInstanceID != 0)
                                //    {
                                //        lblname.InnerText = "Edit Contract Detail(s)";
                                //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialogforlegal(" + contractInstanceID + ");", true);
                                //    }
                                //}
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdContractList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = AuthenticationHelper.UserID;

                if (e.CommandName.Equals("DELETE_Contract"))
                {
                    long contractID = Convert.ToInt64(e.CommandArgument);

                    if (contractID != 0)
                    {
                        bool deleteSuccess = ContractManagement.DeleteContractByID(contractID, userID);

                        if (deleteSuccess)
                        {
                            BindGrid();
                            bindPageNumber();
                            ShowGridDetail();
                        }

                        if (deleteSuccess)
                        {
                            cvErrorContractListPage.IsValid = false;
                            cvErrorContractListPage.ErrorMessage = "Contract Deleted Successfully";
                            vsContractListPage.CssClass = "alert alert-success fade-in";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContractList.PageIndex = chkSelectedPage - 1;

            grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
            ShowGridDetail();
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlDeptPage.ClearSelection();
                ddlVendorPage.ClearSelection();
                ddlContractStatus.ClearSelection();
                DropDownListOwner.ClearSelection();
                ddlContractType.ClearSelection();
                tbxFilter1.Text = string.Empty;
                ClearTreeViewSelection(tvFilterLocation);

                tvFilterLocation_SelectedNodeChanged(sender, e);
                lnkBtnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Select Entity/Location";
                // tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddContract_Click(object sender, EventArgs e)
        {
            lblname.InnerText = "Add New Contract";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + 0 + ");", true);
        }

        protected void grdContractList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    3, branchList, vendorID, deptID, contractStatusID, contractTypeID);


                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstAssignedContracts = lstAssignedContracts.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstAssignedContracts = lstAssignedContracts.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdContractList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdContractList.Columns.IndexOf(field);
                    }
                }
                flag = true;
                if (lstAssignedContracts.Count > 0)
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = lstAssignedContracts.Count;
                }
                else
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        #region add Filter
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        #endregion

        protected bool CanChangeStatusEdit(long ID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool result = false;
                    string customer = ConfigurationManager.AppSettings["ContractActionDisplay"].ToString();

                    if (customer == Convert.ToString(AuthenticationHelper.CustomerID))
                    {
                        int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        var query = (from row in entities.Cont_SP_GetAssignedContracts_All(customerID)
                                     where row.ID == ID
                                     select row).ToList();

                        query = query.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(UserID.ToString())).ToList();

                        if (query.Count > 0)
                        {
                            
                            result = true;
                        }
                      
                    }
                    else
                    {
                        result = true;
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        protected bool CanChangeStatusDelete(long ID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool result = false;
                    string customer = ConfigurationManager.AppSettings["ContractActionDisplay"].ToString();

                    if (customer == Convert.ToString(AuthenticationHelper.CustomerID))
                    {

                        int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        var query = (from row in entities.Cont_SP_GetAssignedContracts_All(customerID)
                                     where row.ID == ID
                                     select row).ToList();

                        query = query.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(UserID.ToString())).ToList();

                        if (query.Count > 0)
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = true;
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        protected void grdContractList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                string IsCustID = Convert.ToString(ConfigurationManager.AppSettings["IsViewToUser"]);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        LinkButton lnkEditContract = (LinkButton)e.Row.FindControl("lnkEditContract");
                        LinkButton lnkDeleteContract = (LinkButton)e.Row.FindControl("lnkDeleteContract");
                        Label lblOwnerID = (Label)e.Row.FindControl("lblOwnerID");
                        Label lblStatusID = (Label)e.Row.FindControl("lblStatusID");
                        Label lblcontractInstanceID = (Label)e.Row.FindControl("lblcontractInstanceID");
                        
                       // lnkEditContract.Visible = true;
                        lnkDeleteContract.Visible = true;
                        int CID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var IsOnlycontract = (from row in Cont_tbl_ContractCustomerDetaildata
                                           where row.CustId == CID
                                           select row).FirstOrDefault();
                        //bool IsOnlycontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (IsOnlycontract != null)
                        {
                            if (!string.IsNullOrEmpty(lblOwnerID.Text))
                            {
                                if (Convert.ToInt32(lblOwnerID.Text) != Convert.ToInt32(AuthenticationHelper.UserID))
                                {
                                    if (!string.IsNullOrEmpty(lblcontractInstanceID.Text))
                                    {
                                        if (lblStatusID.Text == "5" || lblStatusID.Text == "15" || lblStatusID.Text == "7")
                                            lnkDeleteContract.Visible = false;
                                    }
                                }
                                else
                                    lnkDeleteContract.Visible = false;
                            }
                        }
                        else
                        {
                            string customer = ConfigurationManager.AppSettings["ContractActionDisplay"].ToString();
                            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                            if (customer == Convert.ToString(AuthenticationHelper.CustomerID))
                            {
                                if (!string.IsNullOrEmpty(lblOwnerID.Text))
                                {

                                    bool query = lblOwnerID.Text.Split(',').ToList().Contains(UserID.ToString());

                                    if (query == true)
                                    {
                                        //lnkEditContract.Visible = true;
                                        lnkDeleteContract.Visible = true;
                                    }
                                    else
                                    {
                                        //lnkEditContract.Visible = false;
                                        lnkDeleteContract.Visible = false;
                                    }
                                }
                            }
                            else
                            {
                                if (IsCustID == Convert.ToString(AuthenticationHelper.CustomerID))
                                {
                                    if (!string.IsNullOrEmpty(lblcontractInstanceID.Text))
                                    {
                                        var cinstanceid = Convert.ToInt32(lblcontractInstanceID.Text);


                                        if (Cont_tbl_VisibleToUserData != null)
                                        {
                                            var queryResult = (from row in Cont_tbl_VisibleToUserData
                                                               where row.ContractInstanceID == cinstanceid
                                                               select row).FirstOrDefault();

                                            if (queryResult != null)
                                            {
                                                lnkDeleteContract.Visible = false;
                                            }
                                            else
                                            {
                                                lnkDeleteContract.Visible = true;
                                            }
                                        }
                                        else
                                        {
                                            var uid = Convert.ToString(UserID);
                                            var queryResult = (from row in entities.Cont_tbl_VisibleToUser
                                                               join row1 in entities.Cont_tbl_ContractInstance
                                                               on row.ContractInstanceID equals row1.ID
                                                               join row2 in entities.Cont_tbl_UserAssignment
                                                               on row.ContractInstanceID equals row2.ContractID
                                                               where row.IsActive == true
                                                               && row.CustomerID == CustomerID
                                                               && row1.CreatedBy != UserID
                                                               && row2.UserID != UserID
                                                               && row.ContractInstanceID == cinstanceid
                                                               && row.VisibkeUserID == uid
                                                               select row).FirstOrDefault();

                                            if (queryResult != null)
                                            {
                                                lnkDeleteContract.Visible = false;
                                            }
                                            else
                                            {
                                                lnkDeleteContract.Visible = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        lnkDeleteContract.Visible = true;
                                    }
                                }
                            }
                        }                   
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
