﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractMyTask.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractMyTask" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <style type="text/css">
        .taskDetail-form {
            /* max-width: 350px; */
            margin: 100px auto 0;
            background: #f9f9f9;
        }

        .label{
            text-align: left;
            background:none !important;
            font-size: 13px;
            color:#8e8e93;
            font-weight: normal;
        }

        /*ul {
            padding-left: 0px;
            text-align: justify;
        }*/
    </style>

    <script type="text/javascript">
        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {            
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
    </script>
</head>
<body>
        
     <div class="container">
         <form runat="server" name="login" id="Form2" autocomplete="off">
             <asp:ScriptManager ID="ScriptManager1" runat="server" />
             
             <div class="login-form">
                 <asp:Panel ID="panelOTP" runat="server" DefaultButton="btnOTP">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                         <ContentTemplate>
                             <div class="col-md-12 login-form-head">
                                 <p class="login-img">
                                     <img src="/Images/avantil-logo.png" />
                                 </p>
                             </div>

                             <div class="login-wrap" id="divOTP" runat="server" visible="false">
                                 <span style="font-family: 'Roboto',san-serif; color: #555555; font-size: 13.5px;">One Time Password has been sent to
                                <br />
                                     Your registered email -
                                <asp:Label ID="email" Font-Bold="true" runat="server"></asp:Label>
                                     <br />
                                     Your registered Phone No -
                                <asp:Label ID="mobileno" Font-Bold="true" runat="server"></asp:Label>
                                     <br />
                                     (Please note that the OTP is valid till
                                <asp:Label ID="Time" runat="server"></asp:Label>
                                     IST)</span>

                                 <div class="clearfix" style="height: 10px"></div>
                                 <div class="clearfix" style="height: 10px"></div>

                                 <h1 style="font-size: 15px;">Please Enter One Time Password (OTP)</h1>

                                 <div class="input-group">
                                     <span class="input-group-addon"></span>
                                     <asp:TextBox ID="txtOTP" CssClass="form-control" runat="server" placeholder="Enter OTP" />
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtOTP" runat="server" Display="None"
                                         ErrorMessage="Only Numbers allowed in OTP" ValidationExpression="\d+" ValidationGroup="OTPValidationGroup"></asp:RegularExpressionValidator>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Required OTP."
                                         ControlToValidate="txtOTP" runat="server" ValidationGroup="OTPValidationGroup" />
                                 </div>

                                 <asp:Label ID="lblmsgotp" runat="server" ForeColor="Red" CssClass="pull-left" Text=""></asp:Label>
                                 <asp:Button ID="btnOTP" CssClass="btn btn-primary btn-lg btn-block" Text="Verify OTP"
                                     OnClick="btnOTP_Click" runat="server" ValidationGroup="OTPValidationGroup"></asp:Button>

                                 <div class="clearfix" style="height: 10px"></div>
                             </div>

                             <div class="col-md-12 login-form-head">
                                 <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" ValidationGroup="OTPValidationGroup" />
                                 <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" ValidationGroup="OTPValidationGroup" />
                             </div>

                         </ContentTemplate>
                         <Triggers>
                             <asp:PostBackTrigger ControlID="btnOTP" />
                         </Triggers>
                     </asp:UpdatePanel>
                 </asp:Panel>
             </div>

             <asp:Panel ID="panelTaskDetail" runat="server" Visible="false">
                 <div class="col-md-12" style="text-align: center;">
                     <p class="login-img">
                         <img src="/Images/avantil-logo.png" />
                     </p>
                 </div>

                 <div class="row" style="background: none;">
                     <!--Contract Detail Panel Start-->
                     <div class="row Dashboard-white-widget">
                         <div class="col-lg-12 col-md-12">
                             <div class="panel panel-default">

                                 <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Contract Detail(s)">
                                     <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractSummary">
                                         <a>
                                             <h4>Contract Detail(s)</h4>
                                         </a>
                                         <div class="panel-actions">
                                             <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractSummary">
                                                 <i class="fa fa-chevron-up"></i>
                                             </a>
                                         </div>
                                     </div>
                                 </div>

                                 <div id="collapseDivContractSummary" class="panel-collapse collapse">
                                     <div class="row">
                                         <asp:ValidationSummary ID="VSContractPopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                             ValidationGroup="ContractPopUpValidationGroup" />
                                         <asp:CustomValidator ID="cvContractPopUp" runat="server" EnableClientScript="False"
                                             ValidationGroup="ContractPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                     </div>

                                     <div class="panel-body">
                                         <asp:Panel ID="pnlContract" runat="server">
                                             <div class="container plr0">
                                                 <div class="row">
                                                     <div class="form-group required col-md-4 pl0">
                                                         <label for="txtContractNo" class="control-label">Contract Number</label>
                                                         <asp:TextBox runat="server" ID="txtContractNo" CssClass="form-control" autocomplete="off" />
                                                     </div>

                                                     <div class="form-group required col-md-8 pl0">
                                                         <label for="txtTitle" class="control-label">Contract Title</label>
                                                         <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" autocomplete="off" />
                                                     </div>
                                                 </div>

                                                 <div class="row">
                                                     <div class="form-group required col-md-12 pl0">
                                                         <label for="tbxDescription" class="control-label">Description</label>
                                                         <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" CssClass="form-control" />
                                                     </div>
                                                 </div>

                                                 <div class="row">
                                                     <div class="form-group required col-md-4 pl0">
                                                         <label for="tbxBranch" class="control-label">Entity/Location</label>
                                                         <asp:TextBox runat="server" ID="tbxBranch" CssClass="form-control" ReadOnly="true" autocomplete="off" AutoCompleteType="None" CausesValidation="true"
                                                             Style="cursor: pointer;" />

                                                         <div style="position: absolute; z-index: 10; display: none" id="divBranches">
                                                             <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                 BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px"
                                                                 Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                                 ShowLines="true">
                                                             </asp:TreeView>
                                                         </div>
                                                     </div>

                                                     <div class="form-group required col-md-4 pl0">
                                                         <label for="lstBoxVendor" class="control-label">Vendor</label>
                                                         <asp:ListBox ID="lstBoxVendor" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                                     </div>

                                                     <div class="form-group required col-md-4 pl0">
                                                         <label for="ddlDepartment" class="control-label">Department</label>
                                                         <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                             DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                                                     </div>
                                                 </div>

                                                 <div class="row">
                                                     <div class="form-group col-md-4 pl0">
                                                         <label for="txtProposalDate" class="control-label">Proposal Date</label>
                                                         <div class="input-group date">
                                                             <span class="input-group-addon">
                                                                 <span class="fa fa-calendar color-black"></span>
                                                             </span>
                                                             <asp:TextBox ID="txtProposalDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                         </div>
                                                     </div>

                                                     <div class="form-group col-md-4 pl0" data-provide="datepicker">
                                                         <label for="txtAgreementDate" class="control-label">Agreement Date</label>
                                                         <div class="input-group date">
                                                             <span class="input-group-addon">
                                                                 <span class="fa fa-calendar color-black"></span>
                                                             </span>
                                                             <asp:TextBox ID="txtAgreementDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                         </div>
                                                     </div>

                                                     <div class="form-group required col-md-4 pl0">
                                                         <label for="txtEffectiveDate" class="control-label">Effective Date</label>
                                                         <div class="input-group date">
                                                             <span class="input-group-addon">
                                                                 <span class="fa fa-calendar color-black"></span>
                                                             </span>
                                                             <asp:TextBox ID="txtEffectiveDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                         </div>
                                                     </div>
                                                 </div>

                                                 <div class="row">
                                                     <div class="form-group col-md-4 pl0">
                                                         <label for="txtAgreementDate" class="control-label">Review Date</label>
                                                         <div class="input-group date">
                                                             <span class="input-group-addon">
                                                                 <span class="fa fa-calendar color-black"></span>
                                                             </span>
                                                             <asp:TextBox ID="txtReviewDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                         </div>
                                                     </div>

                                                     <div class="form-group col-md-4 pl0 d-none">
                                                         <label for="rbContractEndType" class="control-label">&nbsp;</label>
                                                         <asp:RadioButtonList ID="rbContractEndType" runat="server" RepeatDirection="Horizontal">
                                                             <asp:ListItem class="radio-inline" Text="Expiration Date" Value="E" Selected="True"></asp:ListItem>
                                                             <asp:ListItem class="radio-inline" Text="Duration " Value="D"></asp:ListItem>
                                                         </asp:RadioButtonList>
                                                     </div>

                                                     <div class="form-group col-md-4 pl0" id="divExpirationDate">
                                                         <label for="txtExpirationDate" class="control-label">Expiration Date</label>
                                                         <div class="input-group date">
                                                             <span class="input-group-addon">
                                                                 <span class="fa fa-calendar color-black"></span>
                                                             </span>
                                                             <asp:TextBox ID="txtExpirationDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                         </div>
                                                     </div>

                                                     <div class="form-group col-md-4 pl0 d-none" id="divDuration">
                                                         <label for="txtDuration">Duration</label>
                                                         <div class="input-group">
                                                             <asp:TextBox ID="txtDuration" runat="server" placeholder="" class="form-control col-md-8" />
                                                             <div class="input-group-btn">
                                                                 <asp:DropDownList ID="ddlDuration" runat="server" CssClass="form-control col-md-4">
                                                                     <asp:ListItem Text="Days" Value="D" Selected="True"></asp:ListItem>
                                                                     <asp:ListItem Text="Weeks" Value="W"></asp:ListItem>
                                                                     <asp:ListItem Text="Months" Value="M"></asp:ListItem>
                                                                     <asp:ListItem Text="Years" Value="Y"></asp:ListItem>
                                                                 </asp:DropDownList>
                                                             </div>
                                                         </div>
                                                     </div>

                                                     <div class="form-group col-md-4 pl0">
                                                         <label for="txtNoticeTerm" class="control-label">Notice Term</label>
                                                         <div class="input-group">
                                                             <asp:TextBox ID="txtNoticeTerm" runat="server" class="form-control col-md-8" />
                                                             <div class="input-group-btn">
                                                                 <asp:DropDownList ID="ddlNoticeTerm" runat="server" CssClass="form-control col-md-4"
                                                                     Style="border-bottom-right-radius: 4px; border-top-right-radius: 4px; border-left: none;">
                                                                     <asp:ListItem Text="Days" Value="1" Selected="True"></asp:ListItem>
                                                                     <asp:ListItem Text="Weeks" Value="2"></asp:ListItem>
                                                                     <asp:ListItem Text="Months" Value="3"></asp:ListItem>
                                                                     <asp:ListItem Text="Years" Value="4"></asp:ListItem>
                                                                 </asp:DropDownList>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>

                                                 <div class="row">
                                                     <asp:UpdatePanel ID="upContractTypeSubType" runat="server" UpdateMode="Conditional">
                                                         <ContentTemplate>

                                                             <div class="form-group required col-md-4 pl0">
                                                                 <label for="ddlContractType" class="control-label">Contract Type</label>
                                                                 <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlContractType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                     class="form-control" Width="100%" DataPlaceHolder="Select Contract Type"
                                                                     OnSelectedIndexChanged="ddlContractType_SelectedIndexChanged">
                                                                 </asp:DropDownListChosen>
                                                             </div>

                                                             <div class="form-group col-md-4 pl0">
                                                                 <label for="ddlContractSubType" class="control-label">Contract Sub-Type</label>
                                                                 <asp:DropDownListChosen runat="server" ID="ddlContractSubType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                     class="form-control" Width="100%" DataPlaceHolder="Select Contract Type">
                                                                 </asp:DropDownListChosen>
                                                             </div>
                                                         </ContentTemplate>
                                                     </asp:UpdatePanel>

                                                     <div class="form-group required col-md-4 pl0">
                                                         <label for="lstBoxOwner" class="control-label">Contract Owner</label>
                                                         <asp:ListBox ID="lstBoxOwner" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                     </div>
                                                 </div>

                                                 <div class="row">
                                                     <div class="form-group col-md-4 pl0">
                                                         <label for="tbxContractAmt" class="control-label">Contract Amount</label>
                                                         <asp:TextBox runat="server" ID="tbxContractAmt" CssClass="form-control" autocomplete="off" />
                                                     </div>

                                                     <div class="form-group col-md-4 pl0">
                                                         <label for="ddlPaymentTerm" class="control-label">Payment Term</label>
                                                         <asp:DropDownListChosen runat="server" ID="ddlPaymentTerm" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                             class="form-control" Width="100%" DataPlaceHolder="Select Court">
                                                             <asp:ListItem Text="One-Time" Value="0" Selected="True"></asp:ListItem>
                                                             <asp:ListItem Text="Daily" Value="1"></asp:ListItem>
                                                             <asp:ListItem Text="Weekly" Value="7"></asp:ListItem>
                                                             <asp:ListItem Text="Periodically" Value="15"></asp:ListItem>
                                                             <asp:ListItem Text="Monthly" Value="30"></asp:ListItem>
                                                             <asp:ListItem Text="Quarterly" Value="4"></asp:ListItem>
                                                             <asp:ListItem Text="Half-Yearly" Value="6"></asp:ListItem>
                                                             <asp:ListItem Text="Yearly" Value="12"></asp:ListItem>
                                                         </asp:DropDownListChosen>
                                                     </div>

                                                     <div class="form-group col-md-4 pl0"></div>
                                                 </div>

                                                 <div class="row">
                                                     <div class="form-group col-md-12 pl0">
                                                         <asp:UpdatePanel ID="upCustomField" runat="server">
                                                             <ContentTemplate>
                                                                 <asp:GridView runat="server" ID="grdCustomField" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                     GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="true"
                                                                     PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowDataBound="grdCustomField_Common_RowDataBound">
                                                                     <Columns>
                                                                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                             <ItemTemplate>
                                                                                 <%#Container.DataItemIndex+1 %>
                                                                             </ItemTemplate>
                                                                         </asp:TemplateField>

                                                                         <asp:TemplateField Visible="false">
                                                                             <ItemTemplate>
                                                                                 <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                     data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                             </ItemTemplate>
                                                                         </asp:TemplateField>

                                                                         <asp:TemplateField ItemStyle-Width="25%" FooterStyle-Width="25%"><%--HeaderText="Field"--%>
                                                                             <ItemTemplate>
                                                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                     <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                         data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                 </div>
                                                                             </ItemTemplate>
                                                                             <FooterTemplate>
                                                                                 <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer" CssClass="unique-footer-select" DataPlaceHolder="Select"
                                                                                     AllowSingleDeselect="false" DisableSearchThreshold="5" Width="100%">
                                                                                 </asp:DropDownListChosen>
                                                                             </FooterTemplate>
                                                                         </asp:TemplateField>

                                                                         <asp:TemplateField ItemStyle-Width="1%" FooterStyle-Width="1%">
                                                                             <ItemTemplate>
                                                                                 <asp:Label ID="lblImgAddNew" runat="server"></asp:Label>
                                                                                 </div>
                                                                             </ItemTemplate>
                                                                             <FooterTemplate>
                                                                                 <img id="imgAddNewCustomField" src='<%# ResolveUrl("~/Images/add_icon_new.png")%>'
                                                                                     onclick="OpenAddNewCustomFieldPopUp()" alt="Add" data-toggle="tooltip" data-placement="bottom" title="Click to Add New Custom Field" />
                                                                             </FooterTemplate>
                                                                         </asp:TemplateField>

                                                                         <asp:TemplateField ItemStyle-Width="25%" FooterStyle-Width="25%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"><%--HeaderText="Value"--%>
                                                                             <ItemTemplate>
                                                                                 <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control" PlaceHolder="Value" Text='<%# Eval("labelValue") %>'></asp:TextBox>
                                                                             </ItemTemplate>
                                                                             <FooterTemplate>
                                                                                 <asp:TextBox runat="server" AutoPostBack="true" ID="txtFieldValue_Footer" PlaceHolder="Value" CssClass="form-control" />
                                                                             </FooterTemplate>
                                                                         </asp:TemplateField>
                                                                     </Columns>
                                                                     <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                     <HeaderStyle CssClass="clsheadergrid" />
                                                                     <EmptyDataTemplate>
                                                                         No Records Found
                                                                     </EmptyDataTemplate>
                                                                 </asp:GridView>
                                                             </ContentTemplate>
                                                         </asp:UpdatePanel>
                                                     </div>
                                                 </div>
                                             </div>
                                         </asp:Panel>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Contract Detail Panel End-->

                     <div class="row Dashboard-white-widget">
                         <div class="col-lg-12 col-md-12">
                             <div class="panel panel-default">
                                 <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Case Document(s)">
                                     <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDocument">
                                         <a>
                                             <h4>Contract Document(s)</h4>
                                         </a>
                                         <div class="panel-actions">
                                             <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDocument">
                                                 <i class="fa fa-chevron-up"></i>
                                             </a>
                                         </div>
                                     </div>
                                 </div>

                                 <div id="collapseDivCaseDocument" class="panel-collapse collapse">
                                     <div class="panel-body">
                                         <div class=" row col-md-12 plr0">
                                             <asp:ValidationSummary ID="vsCaseDocument" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                 ValidationGroup="ContractDocumentPopUpValidationGroup" />
                                             <asp:CustomValidator ID="cvContractDocument" runat="server" EnableClientScript="False"
                                                 ValidationGroup="ContractDocumentPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                         </div>

                                         <div class="row col-md-12 plr0">
                                             <asp:UpdatePanel ID="upContractDocUploadPopup" runat="server">
                                                 <ContentTemplate>
                                                     <asp:GridView runat="server" ID="grdContractDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                         GridLines="None" PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                         PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdContDocuments_RowCommand" OnRowDataBound="grdContDocuments_RowDataBound"
                                                         OnPageIndexChanging="grdContDocuments_PageIndexChanging">
                                                         <Columns>
                                                             <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                 <ItemTemplate>
                                                                     <%--   <%#Container.DataItemIndex+1 %>--%>
                                                                     <asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>

                                                                 </ItemTemplate>
                                                             </asp:TemplateField>

                                                             <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%">
                                                                 <ItemTemplate>
                                                                     <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                                 </ItemTemplate>
                                                             </asp:TemplateField>

                                                             <asp:TemplateField HeaderText="Document" ItemStyle-Width="40%">
                                                                 <ItemTemplate>
                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                                         <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                     </div>
                                                                 </ItemTemplate>
                                                             </asp:TemplateField>

                                                             <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%" Visible="false">
                                                                 <ItemTemplate>

                                                                     <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                     </div>
                                                                 </ItemTemplate>
                                                             </asp:TemplateField>

                                                             <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                                 <ItemTemplate>
                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                         <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("UploadedByName") %>'
                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UploadedByName") %>'></asp:Label>
                                                                     </div>
                                                                 </ItemTemplate>
                                                             </asp:TemplateField>

                                                             <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                                 <ItemTemplate>
                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                         <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                     </div>
                                                                 </ItemTemplate>
                                                             </asp:TemplateField>

                                                             <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                                 <ItemTemplate>
                                                                     <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                         <ContentTemplate>
                                                                             <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' CommandName="DownloadContDoc"
                                                                                 ID="lnkBtnDownLoadCaseDoc" runat="server" data-toggle="tooltip" data-placement="bottom" title="Click to Download Document">
                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download" /> 
                                                                             </asp:LinkButton>

                                                                             <asp:LinkButton CommandArgument='<%# Eval("FileID") %>' AutoPostBack="true" CommandName="ViewContOrder"
                                                                                 ID="lnkBtnViewDocCase" runat="server" data-toggle="tooltip" data-placement="bottom" title="Click to View Document">
                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                             </asp:LinkButton>

                                                                         </ContentTemplate>
                                                                         <Triggers>
                                                                             <asp:PostBackTrigger ControlID="lnkBtnDownLoadCaseDoc" />
                                                                         </Triggers>
                                                                     </asp:UpdatePanel>
                                                                 </ItemTemplate>
                                                             </asp:TemplateField>
                                                         </Columns>
                                                         <RowStyle CssClass="clsROWgrid" />
                                                         <HeaderStyle CssClass="clsheadergrid" />
                                                         <EmptyDataTemplate>
                                                             No Records Found
                                                         </EmptyDataTemplate>
                                                     </asp:GridView>
                                                 </ContentTemplate>
                                             </asp:UpdatePanel>
                                         </div>

                                         <div class="row col-md-12 plr0">
                                             <div class="col-md-10 pl0">
                                                 <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                                     <p style="padding-right: 0px !Important;">
                                                         <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                                         <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-<asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                         of 
                                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                     </p>
                                                 </div>
                                             </div>
                                             <div class="col-md-2 text-right pr0">
                                                 <div class="col-md-6 plr0 text-right">
                                                     <p class="clsPageNo">Page</p>
                                                 </div>
                                                 <div class="col-md-6 col-sm-6 col-xs-6 pr0">
                                                     <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                         OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                                     </asp:DropDownListChosen>
                                                 </div>
                                             </div>
                                             <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>

                     <!--Task Detail Panel Start-->
                     <div class="row Dashboard-white-widget">
                         <div class="col-lg-12 col-md-12">
                             <div class="panel panel-default">
                                 <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Task Detail">
                                     <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskDetail">
                                         <a>
                                             <h4>Task Detail(s)</h4>
                                         </a>
                                         <div class="panel-actions">
                                             <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskDetail">
                                                 <i class="fa fa-chevron-up"></i>
                                             </a>
                                         </div>
                                     </div>
                                 </div>

                                 <div id="collapseDivTaskDetail" class="panel-collapse collapse">
                                     <div class="panel-body">
                                         <div class="container plr0">
                                             <asp:Panel ID="pnlTaskDetail" runat="server">
                                                 <div class="row form-group">
                                                     <div class="col-md-2">
                                                         <label for="lblTaskTitle" class="control-label">Task Title</label>
                                                     </div>
                                                     <div class="col-md-10">
                                                         <asp:Label ID="lblTaskTitle" runat="server" CssClass="text-label"></asp:Label>
                                                     </div>
                                                 </div>

                                                 <div class="row form-group">
                                                     <div class="col-md-2">
                                                         <label for="lblTaskDesc" class="control-label">Task Description</label>
                                                     </div>
                                                     <div class="col-md-10">
                                                         <asp:Label ID="lblTaskDesc" runat="server" CssClass="text-label"></asp:Label>
                                                     </div>
                                                 </div>

                                                 <div class="row form-group">
                                                     <div class="col-md-2">
                                                         <label for="lblAssignBy" class="control-label">Assigned By</label>
                                                     </div>
                                                     <div class="col-md-2">
                                                         <asp:Label ID="lblAssignBy" runat="server" CssClass="text-label"></asp:Label>
                                                     </div>

                                                     <div class="col-md-2">
                                                         <label for="lblTaskDueDate" class="control-label">Due Date</label>
                                                     </div>
                                                     <div class="col-md-2">
                                                         <asp:Label ID="lblTaskDueDate" runat="server" CssClass="text-label"></asp:Label>
                                                     </div>

                                                     <div class="col-md-2">
                                                         <label for="lblPriority" class="control-label">Priority</label>
                                                     </div>
                                                     <div class="col-md-2">
                                                         <asp:Label ID="lblPriority" runat="server" CssClass="text-label"></asp:Label>
                                                     </div>
                                                 </div>

                                                 <div class="row form-group">
                                                     <div class="col-md-2">
                                                         <label for="lblExpOutcome" class="control-label">Expected Outcome</label>
                                                     </div>
                                                     <div class="col-md-10">
                                                         <asp:Label ID="lblExpOutcome" runat="server" CssClass="text-label"></asp:Label>
                                                     </div>
                                                 </div>

                                                 <div class="row form-group">
                                                     <div class="col-md-2">
                                                         <label for="lblTaskRemark" class="control-label">Remark</label>
                                                     </div>
                                                     <div class="col-md-10">
                                                         <asp:Label ID="lblTaskRemark" runat="server" CssClass="text-label"></asp:Label>
                                                     </div>
                                                 </div>

                                                 <div class="row form-group">

                                                     <div class="col-md-2">
                                                         <label for="lblTaskDueDate" class="control-label">Document(s)</label>
                                                     </div>

                                                     <div class="col-md-10">
                                                         <asp:UpdatePanel runat="server" ID="upTaskDoc" UpdateMode="Conditional">
                                                             <ContentTemplate>
                                                                 <asp:Label ID="lblTaskDocuments" runat="server" CssClass="text-label"></asp:Label>

                                                                 <asp:LinkButton ID="lnkBtnTaskDocuments" runat="server" OnClick="btnDownloadTaskDoc_Click"
                                                                     data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Download Document(s)">
                                                                <img src='/Images/download_icon_new.png' alt="Download"/>
                                                                 </asp:LinkButton>

                                                                 <asp:LinkButton AutoPostBack="true" ID="lblTaskDocView" runat="server" data-toggle="tooltip"
                                                                     OnClick="lblTaskDocView_Click" data-placement="bottom" ToolTip="Click to View Document(s)">
                                                                <img src='/Images/Eye.png' alt="View" />
                                                                 </asp:LinkButton>
                                                             </ContentTemplate>
                                                             <Triggers>
                                                                 <asp:PostBackTrigger ControlID="lnkBtnTaskDocuments" />
                                                             </Triggers>
                                                         </asp:UpdatePanel>
                                                     </div>

                                                 </div>
                                             </asp:Panel>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Task Detail Panel End-->

                     <!--Task-Response Panel Start-->
                     <div class="row Dashboard-white-widget">
                         <div class="col-lg-12 col-md-12">
                             <div class="panel panel-default">

                                 <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Response Detail">
                                     <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskResponse">
                                         <a>
                                             <h4>Submit Response</h4>
                                         </a>
                                         <div class="panel-actions">
                                             <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskResponse">
                                                 <i class="fa fa-chevron-up"></i>
                                             </a>
                                         </div>
                                     </div>
                                 </div>

                                 <div id="collapseDivTaskResponse" class="panel-collapse collapse in">
                                     <div class="panel-body">
                                         <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                                             <ContentTemplate>
                                                 <div class="container">

                                                     <div class="row">
                                                         <div class="form-group col-md-12">

                                                             <asp:UpdatePanel ID="upTaskResponseDocUpload" runat="server">
                                                                 <ContentTemplate>
                                                                     <asp:GridView runat="server" ID="grdTaskResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                         GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                         PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                         OnPageIndexChanging="grdTaskResponseLog_OnPageIndexChanging" OnRowCommand="grdTaskResponseLog_RowCommand">
                                                                         <Columns>
                                                                             <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                 <ItemTemplate>
                                                                                     <%#Container.DataItemIndex+1 %>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Responded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                         <asp:Label ID="lblResponseDate" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ResponseDate") %>'
                                                                                             Text='<%# Eval("ResponseDate") != DBNull.Value ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Description" ItemStyle-Width="30%" HeaderStyle-Width="30%">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                         <asp:Label ID="lblResDesc" runat="server" Text='<%# Eval("Description") %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Remark" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                         <asp:Label ID="lblResRemark" runat="server" Text='<%# Eval("Remark") %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%" Visible="false">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                         <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                         <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                         </asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" HeaderStyle-Width="15%"
                                                                                 ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                                                 <ItemTemplate>
                                                                                     <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                         <ContentTemplate>
                                                                                             <asp:LinkButton
                                                                                                 CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="ViewTaskResponseDoc"
                                                                                                 ID="lnkBtnViewDocument" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Documents" />
                                                                                             </asp:LinkButton>

                                                                                             <asp:LinkButton
                                                                                                 CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                 ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                             </asp:LinkButton>

                                                                                             <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                 AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                                 OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                                 ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                         <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                             </asp:LinkButton>
                                                                                         </ContentTemplate>
                                                                                         <Triggers>
                                                                                             <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                             <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                         </Triggers>
                                                                                     </asp:UpdatePanel>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                         </Columns>
                                                                         <RowStyle CssClass="clsROWgrid" />
                                                                         <HeaderStyle CssClass="clsheadergrid" />
                                                                         <EmptyDataTemplate>
                                                                             No Records Found
                                                                         </EmptyDataTemplate>
                                                                     </asp:GridView>
                                                                 </ContentTemplate>
                                                             </asp:UpdatePanel>

                                                         </div>
                                                     </div>

                                                     <asp:Panel ID="pnlTaskResponse" runat="server">
                                                         <div class="row">
                                                             <asp:ValidationSummary ID="vsTaskResponse" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                 ValidationGroup="TaskResponseValidationGroup" />
                                                             <asp:CustomValidator ID="cvTaskResponse" runat="server" EnableClientScript="False"
                                                                 ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                                         </div>

                                                         <div class="row">
                                                             <div class="form-group required col-md-4">
                                                                 <label for="ddlStatus" class="control-label">Status</label>
                                                             </div>
                                                             <div class="form-group col-md-3">
                                                                 <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                     class="form-control" Width="100%" DataPlaceHolder="Select Status">
                                                                 </asp:DropDownListChosen>
                                                             </div>
                                                         </div>

                                                         <div class="row">
                                                             <div class="form-group required col-md-4">
                                                                 <label for="tbxTaskResComment" class="control-label">Comment</label>
                                                             </div>
                                                             <div class="form-group col-md-8">
                                                                 <asp:TextBox ID="tbxTaskResComment" runat="server" CssClass="form-control"></asp:TextBox>
                                                                 <asp:RequiredFieldValidator ID="rfvTaskResComment" ErrorMessage="Required Comment"
                                                                     ControlToValidate="tbxTaskResComment" runat="server" ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                                             </div>
                                                         </div>

                                                         <div class="row">
                                                             <div class="form-group col-md-4">
                                                                 <label for="tbxTaskResComment" class="control-label">Upload Document (if any)</label>
                                                             </div>

                                                             <div class="form-group col-md-8">
                                                                 <asp:FileUpload ID="fuTaskResponseDocUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93; margin-bottom: 15px;" />
                                                             </div>
                                                         </div>

                                                         <div class="row form-group text-center">
                                                             <div class="col-md-12">
                                                                 <asp:Button Text="Save" runat="server" ID="btnSaveTaskResponse" CssClass="btn btn-primary" OnClick="btnSaveTaskResponse_Click"
                                                                     ValidationGroup="TaskResponseValidationGroup"></asp:Button>
                                                                 <asp:Button Text="Clear" runat="server" ID="btnTaskResponseClear" CssClass="btn btn-primary" OnClick="btnClearTaskResponse_Click" />
                                                             </div>
                                                         </div>
                                                     </asp:Panel>
                                                 </div>
                                             </ContentTemplate>
                                             <Triggers>
                                                 <asp:PostBackTrigger ControlID="btnSaveTaskResponse" />
                                             </Triggers>
                                         </asp:UpdatePanel>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Task-Response Panel End-->

                     <%--Document Viewer--%>
                     <div class="modal fade" id="DocumentViewerPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                         <div class="modal-dialog" style="width: 100%">
                             <div class="modal-content">
                                 <div class="modal-header">
                                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                 </div>
                                 <div class="modal-body" style="height: 570px;">
                                     <div style="width: 100%;">
                                         <div style="float: left; width: 10%">
                                             <table width="100%" style="text-align: left; margin-left: 5%;">
                                                 <thead>
                                                     <tr>
                                                         <td valign="top">
                                                             <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                                 <ContentTemplate>
                                                                     <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                         OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                         <HeaderTemplate>
                                                                             <table id="tblComplianceDocumnets">
                                                                                 <thead>
                                                                                     <th>File Name</th>
                                                                                 </thead>
                                                                         </HeaderTemplate>
                                                                         <ItemTemplate>
                                                                             <tr>
                                                                                 <td>
                                                                                     <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                         <ContentTemplate>
                                                                                             <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("DocTypeInstanceID") + ","+ Eval("Version") + ","+ Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                                 runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString().Substring(0,10) %>'></asp:LinkButton>
                                                                                         </ContentTemplate>
                                                                                         <Triggers>
                                                                                             <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                         </Triggers>
                                                                                     </asp:UpdatePanel>
                                                                                 </td>
                                                                             </tr>
                                                                         </ItemTemplate>
                                                                         <FooterTemplate>
                                                                             </table>
                                                                         </FooterTemplate>
                                                                     </asp:Repeater>
                                                                 </ContentTemplate>
                                                                 <Triggers>
                                                                     <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                                 </Triggers>
                                                             </asp:UpdatePanel>
                                                         </td>
                                                     </tr>
                                                 </thead>
                                             </table>
                                         </div>
                                         <div style="float: left; width: 90%">
                                             <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                             <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                                 <iframe src="about:blank" id="iframeDocViewer" runat="server" width="100" height="100"></iframe>
                                             </fieldset>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <%--Document Viewer-END--%>
                 </div>

             </asp:Panel>

         </form>
    </div>
    
</body>
</html>
