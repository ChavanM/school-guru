﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Dashboard
{
    public partial class ContractMangDashboard : System.Web.UI.Page
    {
        protected static string seriesData_GraphContractStatus;

        protected static string seriesData_GraphDept;
        protected static string graph_Dept_Categories;

        protected static string seriesData_GraphBranch;
        protected static string graph_Branch_Categories;

        protected static string seriesData_GraphContractType;
        protected static string graph_ContractType_Categories;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.Role == "MGMT"|| AuthenticationHelper.Role == "CADMN")
                {
                    HiddenField home = (HiddenField)Master.FindControl("Ishome");
                    home.Value = "true";

                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    int userid = -1;
                    userid = Convert.ToInt32(AuthenticationHelper.UserID);
                    BindVendors();

                    //Bind Tree Views
                    //var branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                    List<NameValueHierarchy> branchList;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branchList);
                    }
                    else
                    {
                        branchList = CustomerBranchManagement.GetAllContractEntitiesAssignedHierarchyManagementSatutory(Convert.ToInt32(customerID));
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branchList);
                    }
                    //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);

                    BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);
                    
                    BindDepartment();
                    BindContractStatusList(false, true);

                    BindContractCategoryType();

                    BindDashboard();
                    BindUsers();
                }
            }
        }
        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
                var key = "Contusers-" + Convert.ToString(customerID);
                var lstAllUsers = (List<User>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null)
                {
                    lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                    HttpContext.Current.Cache.Insert(key, lstAllUsers, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
                }

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                DropDownListOwner.DataValueField = "ID";
                DropDownListOwner.DataTextField = "Name";
                DropDownListOwner.DataSource = internalUsers;
                DropDownListOwner.DataBind();

                DropDownListOwner.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<NameValueHierarchy> bracnhes;
                    string key = "LocationHierarchy" + 29;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Remove(key);
                    }

                    // bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(2);
                    bracnhes = CustomerBranchManagement.GetAllContractEntitiesAssignedHierarchyManagementSatutory(Convert.ToInt32(AuthenticationHelper.CustomerID),Convert.ToInt32(AuthenticationHelper.UserID), "MGMT");
                    CacheHelper.Set<List<NameValueHierarchy>>(key, bracnhes);
                }
                }
            catch (Exception ex)
            {

            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                //treeTxtBox.Text = "Select Entity/Branch/Location";
                treeTxtBox.Text = "Select Entity/Branch/Location ";

                TreeNode node = new TreeNode("All", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = VendorDetails.GetVendors_All(customerID);

            ddlVendorPage.DataTextField = "VendorName";
            ddlVendorPage.DataValueField = "ID";

            ddlVendorPage.DataSource = lstVendors;
            ddlVendorPage.DataBind();

            ddlVendorPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                ddlContractStatus.Items.Insert(0, new ListItem("All", "-1"));              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                if (lstContractTypes.Count > 0)
                    lstContractTypes = lstContractTypes.OrderBy(row => row.TypeName).ToList();

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractTypes;
                ddlContractType.DataBind();

                ddlContractType.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindDashboard();

                collapseDivFilters.Attributes.Remove("class");
                collapseDivFilters.Attributes.Add("class", "panel-collapse in");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTreeViewSelection(tvFilterLocation);
                tbxFilterLocation.Text = "Select Entity/Location";

                ddlDeptPage.ClearSelection();
                ddlVendorPage.ClearSelection();
                ddlContractStatus.ClearSelection();
                ddlContractType.ClearSelection();
                DropDownListOwner.ClearSelection();

                btnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long contractInstanceID = Convert.ToInt64(btn.CommandArgument);

                    if (contractInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowContractDialog(" + contractInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlContractExpiry_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDashboard();
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        public void BindDashboard()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();

                var lstContracts = ContractManagement.GetContracts_All(Convert.ToInt32(customerID));




                ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (isexist != null)
                {
                    var assignedcontractList = CustomerBranchManagement.GetAllAssignedContractListbyuser(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role);
                    //if (assignedcontractList.Count > 0)
                    //{
                    lstContracts = lstContracts.Where(entry => assignedcontractList.Contains(entry.ContractNo)).ToList();
                    //}
                }


                if (lstContracts != null)
                {
                    //if (lstContracts.Count > 0)
                    //{
                        BindContractStatusCounts(lstContracts);                        

                        ShowChart_ContractStatus(lstContracts);
                        ShowChart_Department(lstContracts);
                        ShowChart_Branch(lstContracts);
                        ShowChart_ContractType(lstContracts);

                        var filteredContracts = GetFilteredContracts(lstContracts);

                        if (filteredContracts != null)
                        {
                            //if (filteredContracts.Count > 0)
                            //{
                                BindContractExpiring(filteredContracts);
                            //}
                        }
                    //}
                }

                //Tasks 
                int priorityID = 0;
                string taskStatus = string.Empty;

                var lstTaskDetails = ContractTaskManagement.GetAssignedTaskList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, taskStatus);

                if (lstTaskDetails != null)
                {  
                    BindUpcomingTasks(lstTaskDetails);
                }
                BindUpcomingMileStones();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public List<Sp_Cont_View_ContractDetails_Result> GetFilteredContracts(List<Sp_Cont_View_ContractDetails_Result> lstMasterRecords)
        {
            try
            {
                int branchID = -1;
                long vendorID = -1;
                int deptID = -1;
                long typeID = -1;                
                long contractStatusID = -1;
                int customerID = -1;
                int OwnerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //Filters
                if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                }

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                {
                    typeID = Convert.ToInt64(ddlContractType.SelectedValue);
                }

                if (branchList.Count > 0)
                    lstMasterRecords = lstMasterRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (vendorID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                if (typeID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractTypeID == typeID).ToList();

                if (deptID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (contractStatusID != 0 && contractStatusID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractStatusID == contractStatusID).ToList();
                }
                if (OwnerID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();
                    //lstMasterRecords = lstMasterRecords.Where(entry => entry.OwnerIDs.Split(',').ToList().Contains(OwnerID.ToString())).ToList();
                }

                if (lstMasterRecords.Count > 0)
                {
                    lstMasterRecords = lstMasterRecords.OrderBy(entry => entry.ExpirationDate).ToList();
                }
                
                return lstMasterRecords; // filteredRecords;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public void BindContractStatusCounts(List<Sp_Cont_View_ContractDetails_Result> lstContractRecords_All)
        {
            try
            {
                divDraftCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Draft").Count().ToString();
                divPendingReviewCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Pending Review").Count().ToString();
                divReviewedCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Review Completed").Count().ToString();
                divPendingApprovalCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Pending Approval").Count().ToString();
                divApprovedcount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Approval Completed").Count().ToString();
                divActiveCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Active").Count().ToString();
                divExpiredCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Expired").Count().ToString();
                divTNR.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Terminated" || row.StatusName == "Not Renewed").Count().ToString();
                divReviwesCount.InnerText = GetCountMyReviews().ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int GetCountMyReviews()
        {
            int Count = 0;
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    4, branchList, vendorID, deptID, contractStatusID, contractTypeID);
                
                var lstAssignedApproverContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    6, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedPrimaryApproverContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
               29, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                Count = lstAssignedContracts.Count + lstAssignedApproverContracts.Count + lstAssignedPrimaryApproverContracts.Count;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Count;
        }
        public void BindContractExpiring(List<Sp_Cont_View_ContractDetails_Result> filteredRecords)
        {
            try
            {
                int upcomingDays = -1;

                if(!string.IsNullOrEmpty(ddlContractExpiry.SelectedValue))
                {
                    upcomingDays = Convert.ToInt32(ddlContractExpiry.SelectedValue);
                }

                if (upcomingDays != -1)
                {
                    if (upcomingDays != 91)
                        filteredRecords = filteredRecords.Where(row => row.ExpirationDate <= DateTime.Now.AddDays(upcomingDays).Date).ToList();
                    else
                        filteredRecords = filteredRecords.Where(row => row.ExpirationDate >= DateTime.Now.AddDays(upcomingDays).Date).ToList();
                }

                if (filteredRecords.Count > 5)
                    lnkShowDetailContract.Visible = true;
                else
                    lnkShowDetailContract.Visible = false;

                //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                //string CustomerID = Convert.ToString(AuthenticationHelper.CustomerID);
                //if (AuthenticationHelper.Role == "MGMT" && IsEntityassignmentCustomer == Convert.ToString(CustomerID))
                ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (isexist != null)
                {
                    var assignedcontractList = CustomerBranchManagement.GetAllAssignedContractListbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role);
                    //if (assignedcontractList.Count )
                    //{
                        filteredRecords = filteredRecords.Where(entry => assignedcontractList.Contains(entry.ContractNo)).ToList();
                    //}
                }

                grdContractExpiring.DataSource = filteredRecords;
                grdContractExpiring.DataBind();               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetContractExpiring(List<View_LitigationCaseResponse> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                else
                    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.CaseRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.CaseType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }


                if (viewRecords.Count > 0)
                {
                    viewRecords = viewRecords.OrderBy(entry => entry.ReminderDate).ToList();
                }

                grdContractExpiring.DataSource = viewRecords;
                grdContractExpiring.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public void BindUpcomingTasks(List<Cont_SP_GetAssignedTasks_All_Result> lstTaskRecords_Assigned)
        {
            try
            {
                var upcomingTasksRecords = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "All");

                grdTaskActivity.DataSource = upcomingTasksRecords;
                grdTaskActivity.DataBind();

                if (upcomingTasksRecords.Count > 5)
                    lnkShowDetailTask.Visible = true;
                else
                    lnkShowDetailTask.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    long taskID = Convert.ToInt64(commandArgs[0]);
                    long contractID = Convert.ToInt64(commandArgs[1]);
                    int roleID = Convert.ToInt32(commandArgs[2]);

                    if (taskID != 0 && contractID != 0)
                    {
                        var strContractID = CryptographyManagement.Encrypt(contractID.ToString());
                        var strTaskID = CryptographyManagement.Encrypt(taskID.ToString());
                        var strUserID = CryptographyManagement.Encrypt(AuthenticationHelper.UserID.ToString());
                        var strRoleID = CryptographyManagement.Encrypt(roleID.ToString());

                        string checkSum = Util.CalculateMD5Hash(contractID.ToString() + taskID.ToString() + AuthenticationHelper.UserID.ToString() + roleID.ToString());

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowTaskDialog('" + strContractID + "','" + strTaskID + "','" + strUserID + "','" + strRoleID + "','" + checkSum + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        #region Graphs

        public void ShowChart_ContractStatus(List<Sp_Cont_View_ContractDetails_Result> lstMasterRecords) //filteredRecords
        {
            try
            {
                int branchID = -1;
                int deptID = -1;
                long vendorID = -1;
                long contractStatusID = -1;
                long typeID = -1;
                long OwnerID = -1;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //Filters
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }
                

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }
                if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                {
                    typeID = Convert.ToInt64(ddlContractType.SelectedValue);
                }

                if (branchList.Count > 0)
                    lstMasterRecords = lstMasterRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (vendorID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                if (deptID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (OwnerID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();

                if (contractStatusID != 0 && contractStatusID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractStatusID == contractStatusID).ToList();
                }

                if (typeID != 0 && typeID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractTypeID == typeID).ToList();
                }

                if (lstMasterRecords.Count > 0)
                {
                    lstMasterRecords = lstMasterRecords.OrderBy(entry => entry.ExpirationDate).ToList();
                }
                
                seriesData_GraphContractStatus = string.Empty;

                if (lstMasterRecords.Count > 0)
                {
                    var lstDistinctStatus = (from g in lstMasterRecords
                                             group g by new
                                               {
                                                   g.ContractStatusID,
                                                   g.StatusName

                                               } into groupedData
                                               select new dummyClass()
                                               {
                                                   ID = (int)groupedData.Key.ContractStatusID,
                                                   Name = groupedData.Key.StatusName,
                                               }).ToList();

                    lstDistinctStatus.ForEach(eachStatus =>
                    {
                        var totalContractStatusWise = (from row in lstMasterRecords
                                                       where row.ContractStatusID == eachStatus.ID
                                                       select row).Count();

                        if (totalContractStatusWise > 0)
                            seriesData_GraphContractStatus += "{name:'" + eachStatus.Name + "', y:" + totalContractStatusWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','" + vendorID + "','" + eachStatus.ID + "','" + typeID + "','" + OwnerID + "') }} },";

                        //strNoticeCaseCategorySeriesData += "['" + eachCategory.Name + "'," + count + "],";
                    });

                    seriesData_GraphContractStatus = seriesData_GraphContractStatus.Trim(',');                                      
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ShowChart_Department(List<Sp_Cont_View_ContractDetails_Result> lstMasterRecords)
        {
            try
            {
                int branchID = -1;
                int deptID = -1;
                long vendorID = -1;
                long contractStatusID = -1;
                long typeID = -1;
                long OwnerID = -1;
                if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                }

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //Filters
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                {
                    typeID = Convert.ToInt64(ddlContractType.SelectedValue);
                }

                if (branchList.Count > 0)
                    lstMasterRecords = lstMasterRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (vendorID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                if (deptID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (OwnerID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();

                if (contractStatusID != 0 && contractStatusID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractStatusID == contractStatusID).ToList();
                }

                if (typeID != 0 && typeID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractTypeID == typeID).ToList();
                }

                if (lstMasterRecords.Count > 0)
                {
                    lstMasterRecords = lstMasterRecords.OrderBy(entry => entry.ExpirationDate).ToList();
                }
                
                graph_Dept_Categories = string.Empty;
                seriesData_GraphDept = string.Empty;

                if (lstMasterRecords.Count > 0)
                {
                    var lstDistinctDepartments = (from g in lstMasterRecords
                                                  group g by new
                                             {
                                                 g.DepartmentID,
                                                 g.DeptName
                                             } into groupedData
                                             select new dummyClass()
                                             {
                                                 ID = (int)groupedData.Key.DepartmentID,
                                                 Name = groupedData.Key.DeptName,
                                             }).ToList();

                    lstDistinctDepartments.ForEach(eachDept =>
                    {
                        var totalContractDeptWise = (from row in lstMasterRecords
                                                     where row.DepartmentID == eachDept.ID
                                                     select row).Count();

                        if (totalContractDeptWise > 0)
                        {
                            graph_Dept_Categories += "'" + eachDept.Name + "',";

                            seriesData_GraphDept += "{name:'" + eachDept.Name + "', y:" + totalContractDeptWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + eachDept.ID + "','" + vendorID + "','" + contractStatusID + "','" + typeID + "','" + OwnerID + "') }} },";
                        }
                    });

                    graph_Dept_Categories = graph_Dept_Categories.Trim(',');

                    seriesData_GraphDept = seriesData_GraphDept.Trim(',');
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ShowChart_Branch(List<Sp_Cont_View_ContractDetails_Result> lstMasterRecords)
        {
            try
            {
                int branchID = -1;
                int deptID = -1;
                long vendorID = -1;
                long contractStatusID = -1;
                long typeID = -1;
                long OwnerID = -1;

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //Filters
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }
                if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                {
                    typeID = Convert.ToInt64(ddlContractType.SelectedValue);
                }

                if (branchList.Count > 0)
                    lstMasterRecords = lstMasterRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (vendorID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();
                
                if (OwnerID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();


                if (deptID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (contractStatusID != 0 && contractStatusID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractStatusID == contractStatusID).ToList();
                }

                if (typeID != 0 && typeID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractTypeID == typeID).ToList();
                }
                
                if (lstMasterRecords.Count > 0)
                {
                    lstMasterRecords = lstMasterRecords.OrderBy(entry => entry.ExpirationDate).ToList();
                }

                graph_Branch_Categories = string.Empty;
                seriesData_GraphBranch = string.Empty;

                if (lstMasterRecords.Count > 0)
                {
                    var lstDistinctBranches = (from g in lstMasterRecords
                                                  group g by new
                                                  {
                                                      g.CustomerBranchID,
                                                      g.BranchName
                                                  } into groupedData
                                                  select new dummyClass()
                                                  {
                                                      ID = (int)groupedData.Key.CustomerBranchID,
                                                      Name = groupedData.Key.BranchName,
                                                  }).ToList();

                    lstDistinctBranches.ForEach(eachBranch =>
                    {
                        var totalContractBranchWise = (from row in lstMasterRecords
                                                       where row.CustomerBranchID == eachBranch.ID
                                                       select row).Count();

                        if (totalContractBranchWise > 0)
                        {
                            graph_Branch_Categories += "'" + eachBranch.Name + "',";

                            seriesData_GraphBranch += "{name:'" + eachBranch.Name + "', y:" + totalContractBranchWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + eachBranch.ID + "','" + deptID + "','" + vendorID + "','" + contractStatusID + "','" + typeID + "','" + OwnerID + "') }} },";
                        }
                    });

                    graph_Branch_Categories = graph_Branch_Categories.Trim(',');

                    seriesData_GraphBranch = seriesData_GraphBranch.Trim(',');
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public void ShowChart_ContractType(List<Sp_Cont_View_ContractDetails_Result> lstMasterRecords)
        {
            try
            {
                int branchID = -1;
                int deptID = -1;
                long vendorID = -1;
                long contractStatusID = -1;
                long typeID = -1;
                long OwnerID = -1;
                if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                }
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //Filters
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                {
                    typeID = Convert.ToInt64(ddlContractType.SelectedValue);
                }

                if (OwnerID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();

                if (branchList.Count > 0)
                    lstMasterRecords = lstMasterRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (vendorID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                if (deptID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (contractStatusID != 0 && contractStatusID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractStatusID == contractStatusID).ToList();
                }

                if (typeID != 0 && typeID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractTypeID == typeID).ToList();
                }

                if (lstMasterRecords.Count > 0)
                {
                    lstMasterRecords = lstMasterRecords.OrderBy(entry => entry.ExpirationDate).ToList();
                }

                graph_ContractType_Categories = string.Empty;
                seriesData_GraphContractType = string.Empty;

                if (lstMasterRecords.Count > 0)
                {
                    var lstDistinctContractTypes = (from g in lstMasterRecords
                                                   group g by new
                                                   {
                                                       g.ContractTypeID,
                                                       g.ContractTypeName
                                                   } into groupedData
                                                   select new dummyClass()
                                                   {
                                                       ID = (int)groupedData.Key.ContractTypeID,
                                                       Name = groupedData.Key.ContractTypeName,
                                                   }).ToList();

                    lstDistinctContractTypes.ForEach(eachContractType =>
                    {
                        var totalContractTypeWise = (from row in lstMasterRecords
                                                       where row.ContractTypeID == eachContractType.ID
                                                       select row).Count();

                        if (totalContractTypeWise > 0)
                        {
                            graph_ContractType_Categories += "'" + eachContractType.Name + "',";

                            seriesData_GraphContractType += "{name:'" + eachContractType.Name + "', y:" + totalContractTypeWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','" + vendorID + "','" + contractStatusID + "','" + eachContractType.ID + "','" + OwnerID + "') }} },";
                        }
                    });

                    graph_ContractType_Categories = graph_ContractType_Categories.Trim(',');

                    seriesData_GraphContractType = seriesData_GraphContractType.Trim(',');
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnBindGrid1_Click(object sender, EventArgs e)
        {
            BindUpcomingMileStones();
        }
        public void BindUpcomingMileStones()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                long UserID = AuthenticationHelper.UserID;
                var MilestoneList = ContractMastersManagement.GetContractMilestoneDetail(CustomerID, UserID);                
                MilestoneList = MilestoneList.Where(x => x.StatusID != 3).ToList();
                GrdMilestone.DataSource = MilestoneList;
                GrdMilestone.DataBind();

                if (MilestoneList.Count > 5)
                    LinkButton1.Visible = true;
                else
                    LinkButton1.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnMilestoneResponse_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    long MilestoneID = Convert.ToInt64(commandArgs[0]);
                    long contractID = Convert.ToInt64(commandArgs[1]);
                    //int roleID = Convert.ToInt32(commandArgs[2]);

                    if (MilestoneID != 0 && contractID != 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocTypePopup('" + MilestoneID + "','" + contractID + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }
    }
}