﻿<%@ Page Title="My Dashboard" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractMangDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Dashboard.ContractMangDashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="/avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="/avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="/avantischarts/highcharts/js/modules/drilldown.js"></script>
    <%--<script type="text/javascript" src="/avantischarts/highcharts/js/modules/no-data-to-display.js"></script>--%>
    <script type="text/javascript" src="/avantischarts/highcharts/js/modules/exporting.js"></script>
    <script type="text/javascript" src="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    
     <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });
        function OpenDocTypePopup(MilestoneId, ContractID) {

            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/ContractProduct/aspxPages/MileStoneUpdate.aspx?AccessID=" + ContractID + "&MilestoneID=" + MilestoneId);

        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
           function CloseDocTypePopup() {
              $('#divShowDialog').modal('hide');
              alert(1);
                  document.getElementById('<%= lnkBtnBindGrid1.ClientID %>').click();
        }
        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

         function ShowContractDialog(ContractInstanceID) {
             var modalHeight = screen.height - 150;

             if (modalHeight < 0)
                 modalHeight = 200;

             $('#divShowDialog').modal('show');
             $('.modal-dialog').css('width', '95%');
             $('#showdetails').attr('width', '100%');
             $('#showdetails').attr('height', modalHeight + "px");
             $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractDetailsPage.aspx?AccessID=" + ContractInstanceID);
         };

         function ShowTaskDialog(contractID, taskID, userID, roleID, checkSum) {

             var modalHeight = screen.height - 200;

             if (modalHeight < 0)
                 modalHeight = screen.height;

             //alert("Screen Height-" + screen.height + "\n Modal Height-" + modalHeight);

             $('#divShowDialog').modal('show');
             $('.modal-dialog').css('width', '95%');
             $('#showdetails').attr('width', '100%');
             $('#showdetails').attr('height', modalHeight + "px");
             $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?A=" + contractID + "&B=" + taskID + "&C=" + userID + "&D=" + roleID + "&Checksum=" + checkSum);
         };

         function ShowGraphDetail(BID, DID, VID, SID, TID, OID) {

             $('#divGraphDetails').modal('show');
             $('.modal-dialog').css('width', '1300px');
             $('#showChartDetails').attr('src', 'about: blank');
             $('#showChartDetails').attr('width', '1250px');
             $('#showChartDetails').attr('height', '620px');
             //$('#showChartDetails').attr('height', 'auto !important');
             $('#showChartDetails').attr('src', "/ContractProduct/Dashboard/DashboardGraphDetail.aspx?BID=" + BID + "&DID=" + DID + "&VID=" + VID + "&SID=" + SID + "&TID=" + TID + "&OID=" + OID);
         }
    </script>

    <script type="text/javascript">
        //<!-- Graph-ContractStatus Wise 
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphContractStatus',
                        type: 'pie',
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        pie: {
                            //innerSize: '40%',
                            dataLabels: { enabled: true, format: '{y}', distance: 2 }
                        }
                    },
                    tooltip: {
                        hideDelay: 0,
                        backgroundColor: 'rgba(247,247,247,1)',
                        headerFormat: '<b>{point.key}</b><br/>',
                        pointFormat: '{series.name}: {point.y}',
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=seriesData_GraphContractStatus%>
                        ],
                        size: '120%',
                        showInLegend: true,
                    }]
                },

        function (chart) { // on complete
            var textX = chart.plotLeft + (chart.plotWidth * 0.5);
            var textY = chart.plotTop + (chart.plotHeight * 0.5);

            var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
            span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
            span += '<span style="font-size: 16px">1000</span>';
            span += '</span>';

            $("#addText").append(span);
            span = $('#pieChartInfoText');
            span.css('left', textX + (span.width() * -0.5));
            span.css('top', textY + (span.height() * -0.5));
        });
            });
        });
        //Graph-ContractStatus Wise-END 
    </script>

    <script>
        //<!-- Graph-Department Wise -->
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                var DeptWiseColumnChart =
        Highcharts.chart('divGraphDepartment', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },

            xAxis: { categories: [<%=graph_Dept_Categories%>] },

            //'High', 'Medium', 'Low'

            yAxis: {
                title: {
                    text: 'No of Contracts'
                }
            },
            legend: {
                enabled: false,
                //enabled: true,
                //itemDistance: 0,
                //itemStyle: {
                //    textshadow: false,
                //},
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}',
                        style: {
                            textShadow: false,
                            // color:'red',
                        }
                    },
                },
            },

            tooltip: {
                hideDelay: 0,
                backgroundColor: 'rgba(247,247,247,1)',
                headerFormat: '<b>{point.key}</b><br/>',
                //headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
            },

            series: [{
                colorByPoint: true,
                showInLegend: true,
                data: [ <%=seriesData_GraphDept%>]
            }]
        });
            });
        });
//<!-- Graph-Department Wise END-->

    </script>

    <script>
        //<!-- Graph-Branch Wise -->
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                var DeptWiseColumnChart =
        Highcharts.chart('divGraphBranch', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },

            xAxis: { categories: [<%=graph_Branch_Categories%>] },

            //'High', 'Medium', 'Low'

            yAxis: {
                title: {
                    text: 'No of Contracts'
                }
            },
            legend: {
                enabled: false,
                //enabled: true,
                //itemDistance: 0,
                //itemStyle: {
                //    textshadow: false,
                //},
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}',
                        style: {
                            textShadow: false,
                            // color:'red',
                        }
                    },
                },
            },

            tooltip: {
                hideDelay: 0,
                backgroundColor: 'rgba(247,247,247,1)',
                headerFormat: '<b>{point.key}</b><br/>',
                //headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
            },

            series: [{
                colorByPoint: true,
                showInLegend: true,
                data: [ <%=seriesData_GraphBranch%>]
            }]
        });
            });
        });
//<!-- Graph-Branch Wise END-->

    </script>

     <script>
        //<!-- Graph-Contract Type Wise -->
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                var DeptWiseColumnChart =
        Highcharts.chart('divGraphContractType', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },

            xAxis: { categories: [<%=graph_ContractType_Categories%>] },

            //'High', 'Medium', 'Low'

            yAxis: {
                title: {
                    text: 'No of Contracts'
                }
            },
            legend: {
                enabled: false,
                //enabled: true,
                //itemDistance: 0,
                //itemStyle: {
                //    textshadow: false,
                //},
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}',
                        style: {
                            textShadow: false,
                            // color:'red',
                        }
                    },
                },
            },

            tooltip: {
                hideDelay: 0,
                backgroundColor: 'rgba(247,247,247,1)',
                headerFormat: '<b>{point.key}</b><br/>',
                //headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
            },

            series: [{
                colorByPoint: true,
                showInLegend: true,
                data: [ <%=seriesData_GraphContractType%>]
            }]
        });
            });
        });
         //<!-- Graph-Contract Type Wise END-->

    </script>
    
      <style type="text/css">

          .seven-cols .col-md-1, .seven-cols .col-sm-1, .seven-cols .col-lg-1 
          {
          width: 12.5%;
          }

           #collapseDivFilters > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapseDivFilters > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }
        @media screen and (max-width: 750px) {
            iframe {
                max-width: 100% !important;
                width: auto !important;
                height: auto !important;
            }
        }

         .mang-dashboard-white-widget {
            background: #fff;
            padding: 5px 0px 5px 0px;
            margin-bottom: 10px;
            border-radius: 10px;
        }

          .panel {
            margin-bottom: 10px;
        }
        .panel .panel-heading {
            border: none;
            background: #fff;
            padding: 0;
        }

            .panel .panel-heading h2 {
                font-size: 20px;
            }
            

            .panel .panel-heading-dashboard {
                border: none;
                background: #fff;
                padding: 0;
            }

        .info-box {
            min-height: 110px;
            margin-bottom: 10px;
            /*border: 1px solid #ADD8E6;*/
        }

        .info-box:hover {
                color: #FF7473;
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }

        .chosen-container-single .chosen-single div b 
        {
           margin-top: 3px;
           display: block;
           width: 100%;
           height: 100%;
           background: url(WebResource.axd?d=vcanw1OGyEXjITnW_Lw6o1Ipyyl_7xqXXsF4l85T_LM075DfB4Sa1X81JR-ERGJWebwjBZ50cFK6Pp0E393ZYqyZGOYHT_-VfdsqCmyDCfaRmJRJ_t0w8jjfIDakqF4U1oofqFWh029b9t4iZjam4LmYmixIQZtujQLO7ryIDAI1&t=637298087674161687) no-repeat 0px 6px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div style="float: right; margin-right: 6px;">            
                            <asp:Label ID="Label1" runat="server" Text="" Style="color:#666;margin-right: 8px;"  ></asp:Label>
                            <asp:LinkButton ID="btnRefresh1" runat="server" OnClick="btnRefresh_Click" ToolTip="Refresh Now" data-toggle="tooltip">
                            <label style="font-weight: bold;color: blue; cursor: pointer;">Refresh Now</label>              
                            </asp:LinkButton>           
                        </div>
    <br />
    <div class="dashboard">
        <div class="col-md-12 colpadding0">
            <asp:ValidationSummary ID="vsContractDashboard" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                ValidationGroup="ContractDashboardValidationGroup" />
            <asp:CustomValidator ID="cvContractDashboard" runat="server" EnableClientScript="False"
                ValidationGroup="ContractDashboardValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
        </div>
       
         <!-- Top Count start -->
        <div id="divTabs" runat="server" class="row seven-cols" style="width: 980px;">
             <div id="draftCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box indigo-bg w100per">
                     <div class="div-location" style="cursor: pointer;">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Draft">
                         <div class="col-md-12 colpadding0 mt12" style="margin-left: 36px;font-size: 14px;color: #fff;margin-top: -1px;">Draft</div>
                             <div class="col-md-5 colpadding0 mt10">
                                 <img src="/img/Draft.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0" style="padding-top: 17px;">
                                <%-- <div class="titleMD">Draft</div>--%>
                                 <div id="divDraftCount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

             <div id="pendingReviewCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box seablue-bg w100per">
                     <div class="div-location" style="cursor: pointer;margin-top: -10px;">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Pending Review">
                         <div class="col-md-12 colpadding0 mt12" style="font-size: 14px;color: #fff;margin-top: -1px;">Pending Review</div>
                             <div class="col-md-5 colpadding0 mt10">
                                 <img src="/img/Pending-Review.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0"  style="padding-top: 17px;">
                                 <%--<div class="titleMD" style="display:none;">Pending Review</div>--%>
                                 <div id="divPendingReviewCount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

             <div id="reviewedCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box bluenew-bg w100per">
                     <div class="div-location" style="cursor: pointer;margin-top: -12px;">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Review Completed">
                         <div class="col-md-12 colpadding0 mt12" style="margin-left: 4px;font-size: 14px;color: #fff;margin-top: -1px;">Reviewed</div>
                             <div class="col-md-5 colpadding0 mt10">
                                 <img src="/img/Reviewed.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0"  style="padding-top: 17px;">
                                 <%--<div class="titleMD" style="margin-left: -9px;">Reviewed</div>--%>
                                 <div id="divReviewedCount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

             <div id="pendingApprovalCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box rednew-bg w100per">
                     <div class="div-location" style="cursor: pointer;margin-top: -10px;">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Pending Approval">
                         <div class="col-md-12 colpadding0 mt12" style="font-size: 13px;color: #fff;margin-top: -1px;">Pending Approval</div>
                             <div class="col-md-5 colpadding0 mt10">
                                 <img src="/img/Pending-approval.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0" style="padding-top: 17px;">
                               <%--  <div class="titleMD">Pending Approval</div>--%>
                                 <div id="divPendingApprovalCount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

             <div id="approvedcount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box greennew-bg w100per">
                     <div class="div-location" style="cursor: pointer;">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Approval Completed">
                         <div class="col-md-12 colpadding0 mt12" style="margin-left: 23px;font-size: 14px;color: #fff;margin-top: -1px;">Approved</div>
                             <div class="col-md-5 colpadding0 mt10">
                                 <img src="/img/Reviewed.png" height="55" />
                             </div>
                           <div class="col-md-7 colpadding0" style="padding-top: 17px;">
                              <%--   <div class="titleMD" style="margin-left: -9px;">Approved</div>--%>
                                 <div id="divApprovedcount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

            <div id="activeCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                <div class="info-box amber-bg w100per">
                    <div class="div-location" style="cursor: pointer;">
                        <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Active">
                        <div class="col-md-12 colpadding0 mt12" style="margin-left: 30px;font-size: 14px;color: #fff;margin-top: -1px;">Active</div>
                            <div class="col-md-5 colpadding0 mt10">
                                <img src="/img/Active.png" height="55" />
                            </div>
                           <div class="col-md-7 colpadding0" style="padding-top: 17px;">
                                <%--<div class="titleMD">Active</div>--%>
                                <div id="divActiveCount" runat="server" class="countMD">0</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

              <div id="expiredCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer">
                          <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Expired">
                          <div class="col-md-12 colpadding0 mt12" style="margin-left: 23px;font-size: 14px;color: #fff;margin-top: -1px;">Expired</div>
                              <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" />
                              </div>
                              <div class="col-md-7 colpadding0"  style="padding-top: 17px;">
                                 <%-- <div class="titleMD">Expired</div>--%>
                                  <div id="divExpiredCount" runat="server" class="countMD">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>
             <div id="Div1" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer">
                          <a href="/ContractProduct/aspxPages/ContractTemplateList.aspx">
                          <div class="col-md-12 colpadding0 mt12" style="margin-left: 16px;font-size: 14px;color: #fff;margin-top: -1px;">My Reviews</div>
                              <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" />
                              </div>
                              <div class="col-md-7 colpadding0"  style="padding-top: 17px;">
                                  <%--<div class="titleMD">Reviews</div>--%>
                                  <div id="divReviwesCount" runat="server" class="countMD">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>
            <div id="TNotRenewed" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5" style="margin-left: 986px;margin-top: -120px;width: 120px;">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer;margin-top: -10px;">
                          <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Terminated">
                          <div class="col-md-12 colpadding0 mt12" style="margin-left: 0px;font-size: 14px;color: #fff;margin-top: -1px;">Terminated & Not Renewed</div>
                              <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" style="margin-top:-10px"/>
                              </div>
                              <div class="col-md-7 colpadding0"  style="padding-top:17px;margin-top:-10px;">
                                 <%-- <div class="titleMD">Expired</div>--%>
                                  <div id="divTNR" runat="server" class="countMD">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>
            
         </div>
        <!-- Top Count End -->

        <!-- Filters-->

       
        <div class="row">
            <div id="DivFilters" class="row mang-dashboard-white-widget">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="margin-left: 5px;">
                            <h2>Filters</h2>
                            <div class="panel-actions">
                                <a class="btn-minimize"data-toggle="collapse" data-parent="#accordion" href="#<%=collapseDivFilters.ClientID%>">
                                    <i class="fa fa-chevron-up"></i></a>
                            </div>
                        </div>
                        <div id="collapseDivFilters" class="panel-collapse collapse in" runat="server">
                            <div class="panel-body">
                                <div class="col-md-12 colpadding0 form-group">

                                    <div class="col-md-2 colpadding0" style="width:27.3%;">
                                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                            <ContentTemplate>
                                                <label for="tbxFilterLocation" class="filter-label">Entity/Branch/Location</label>
                                                <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="95%" />
                                                <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px;width: 270px;width: 270px;" id="divFilterLocation">
                                                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="100%" NodeStyle-ForeColor="#8e8e93"
                                                        Style="overflow: auto;border-top: 1px solid #c7c7cc;height:180px;margin-top: -21px;width: 270px; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                    </asp:TreeView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="col-md-2 colpadding0" style="width:23%;">
                                        <label for="ddlDeptPage" class="filter-label">Department</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Select Department" class="form-control" Width="95%" />
                                    </div>

                                      <div class="col-md-2 colpadding0" style="width:23%;">
                                        <label for="ddlVendorPage" class="filter-label">Vendor</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlVendorPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Select Vendor" class="form-control" Width="95%" />
                                    </div>

                                    <div class="col-md-2 colpadding0" style="width:23%;">
                                        <label for="ddlStatus" class="filter-label">Status</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlContractStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            DataPlaceHolder="Select Status" class="form-control" Width="95%">
                                        </asp:DropDownListChosen>
                                    </div>

                                </div>
                                 <div class="clearfix"></div>
                                  <div class="col-md-12 colpadding0">
                                     <div class="col-md-2 colpadding0" style="width:27.3%;">
                                       <label for="ddlContractType" class="filter-label">Contract Type</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlContractType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            DataPlaceHolder="Select Type" class="form-control" Width="95%">
                                        </asp:DropDownListChosen>
                                    </div>
                                  <div class="col-md-2 colpadding0" style="width:23%;">
                                        <label for="ddlOwner" class="filter-label">Owner</label>
                                        <asp:DropDownListChosen runat="server" ID="DropDownListOwner" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                           DataPlaceHolder="Select Owner" class="form-control" Width="95%" />
                                    </div>
                                      <div class="col-md-2 colpadding0 text-right" style="width:17%;">
                                        <div class="col-md-6 colpadding0 float-left">
                                            <label for="btnFilter" class="hidden-label">Apply</label> 
                                            <asp:Button ID="btnFilter" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnApplyFilter_Click" Width="95%"/>
                                        </div>
                                        <div class="col-md-6 colpadding0 float-right">
                                            <label for="btnClearFilter" class="hidden-label">Clear</label>
                                            <asp:Button ID="btnClearFilter" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClearFilter_Click" Width="95%"/>
                                        </div>
                                    </div>
                               </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-2 colpadding0" style="width:19%"></div>
                                    <div class="col-md-2 colpadding0" style="width:19%"></div>
                                    <div class="col-md-2 colpadding0 w20per"></div>
                                    <div class="col-md-2 colpadding0 w20per"></div>
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Filters End-->

        <!-- Contract Expiring-->
        <div class="row">
            <div id="divOuterContractExpiring" class="row mang-dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"> <%--data-toggle="collapse" data-parent="#accordion" href="#collapseContractExpiring"--%>
                                <div class="float-left">
                                    <h2>Contracts Expiring in</h2>
                                </div>
                                <div class="float-left ml10 mt5">
                                    <asp:DropDownListChosen runat="server" ID="ddlContractExpiry" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                        DataPlaceHolder="Select" class="form-control" OnSelectedIndexChanged="ddlContractExpiry_SelectedIndexChanged">
                                        <asp:ListItem Text="Next 30 Days" Value="30" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Next 60 Days" Value="60"></asp:ListItem>
                                        <asp:ListItem Text="Next 90 Days" Value="90"></asp:ListItem>
                                        <asp:ListItem Text="More than 90 Days" Value="91"></asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="panel-actions">
                                    <a class="btn-minimize"  data-toggle="collapse" data-parent="#accordion" href="#collapseContractExpiring">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="collapseContractExpiring" class="panel-collapse collapse in">
                                <div class="panel-body" style="max-height: 300px; overflow: auto;">  
                                    <div id="divContractExpiring" class="col-md-12 plr0">
                                        <asp:GridView runat="server" ID="grdContractExpiring" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                           <%--  PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"--%>
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contract No" HeaderStyle-Width="20%" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Title" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expiration Date" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ExpirationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                ToolTip='<%# Eval("ExpirationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                    <ItemTemplate>                                                        
                                                            <asp:LinkButton ID="lnkBtnViewContract" runat="server" OnClick="lnkEditContract_Click" CommandArgument='<%# Eval("ID") %>'
                                                                data-toggle="tooltip" data-placement="left" title="View Contract Detail(s)">
                                                                <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View" />
                                                            </asp:LinkButton>                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                            <PagerSettings Visible="false" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <asp:LinkButton runat="server" ID="lnkShowDetailContract" Text="..Show More" Visible="false"
                                    style="margin-left: -5px;color: blue;font-style: italic;border: none;background: white;"
                                        PostBackUrl="~/ContractProduct/aspxPages/ContractList.aspx?Status=0"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contract Expiring END-->
       
        <div class="row">
            <div class="col-md-12 plr0">
                <asp:UpdatePanel ID="upGraphBranch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divOuterGraphBranch" class="row mang-dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" style="margin-left: 10px;">
                                            <h2>By Location</h2>
                                            <div class="panel-actions">
                                                <a class="btn-minimize"  data-toggle="collapse" data-parent="#accordion" href="#collapseGraphBranch"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseGraphBranch" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div id="divGraphBranch" style="height: 300px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
            
        <div class="row">
            <div class="col-md-12 plr0">
                <asp:UpdatePanel ID="upGraphDepartment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divOuterGraphDepartment" class="row mang-dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"  style="margin-left: 10px;">
                                            <h2>By Department</h2>
                                            <div class="panel-actions">
                                                <%--onclick="btnminimize(this)"--%>
                                                <a class="btn-minimize"  data-toggle="collapse" data-parent="#accordion" href="#collapseGraphDepartment"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                        <div id="collapseGraphDepartment" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div id="divGraphDepartment" style="height: 300px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 pl0">
                <asp:UpdatePanel ID="upGraphContractStatus" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divOuterGraphContractStatus" class="row mang-dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"  style="margin-left: 10px;">
                                            <h2>By Contract Status</h2>
                                            <div class="panel-actions">
                                                <a class="btn-minimize"  data-toggle="collapse" data-parent="#accordion" href="#collapseGraphContractStatus"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                        <div id="collapseGraphContractStatus" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div id="divGraphContractStatus" style="height: 300px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="col-md-8 plr0">
                <asp:UpdatePanel ID="upGraphContractType" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divOuterGraphContractType" class="row mang-dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphContractType" style="margin-left: 10px;">
                                            <h2>By Contract Type</h2>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphContractType">
                                                    <i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                        <div id="collapseGraphContractType" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div id="divGraphContractType" style="height: 300px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <!-- Upcoming Task List-->
        <div class="row">
            <div id="divOuterUpcomingTask" class="row mang-dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="margin-left: 10px;">
                                <h2>Task(s)</h2>
                                <div class="panel-actions">
                                    <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseUpcomingTask">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="collapseUpcomingTask" class="panel-collapse collapse in">
                                <div class="panel-body" style="max-height: 300px; overflow: auto;">
                                    <div id="divUpcomingTask" class="col-md-12 plr0">
                                        <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Type" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                            <asp:Label ID="lblTaskType" runat="server" Text='<%# com.VirtuosoITech.ComplianceManagement.Business.Contract.ContractTaskManagement.ShowTaskType((int)Eval("TaskType")) %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# com.VirtuosoITech.ComplianceManagement.Business.Contract.ContractTaskManagement.ShowTaskType((int)Eval("TaskType")) %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Contract" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label ID="lblContractTitle" runat="server" Text='<%# Eval("ContractTitle") %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Priority" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaskPriority" runat="server" Text='<%# Eval("Priority") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assign On" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                            <asp:Label ID="lblAssignOn" runat="server" Text='<%# Eval("AssignOn") != null ? Convert.ToDateTime(Eval("AssignOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignOn") != null ? Convert.ToDateTime(Eval("AssignOn")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                            <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:LinkButton CommandArgument='<%# Eval("TaskID")+","+Eval("ContractID")+","+Eval("RoleID")%>'
                                                                AutoPostBack="true" OnClick="lnkBtnTaskResponse_Click" ID="lnkBtnTaskResponse" runat="server"
                                                                ToolTip="View Task Detail(s)/Submit Response" data-toggle="tooltip" data-placement="left">
                                                                <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="View"/> 
                                                            </asp:LinkButton>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate></PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Upcoming(i.e. in next 30 days) Task Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <asp:LinkButton runat="server" ID="lnkShowDetailTask" Text="..Show More"
                                    style="margin-left: -5px;color: blue;font-style: italic;border: none;background: white;"
                                      Visible="false"   PostBackUrl="~/ContractProduct/aspxPages/ContractTaskList.aspx?Status=nan"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Upcoming Task List END-->
    </div>
      <div class="row">
            <div id="divOuterUpcomingMilestone" class="row mang-dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Milestone(s)</h2>
                                <div class="panel-actions">
                                    <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseUpcomingTask">
                                        <i class="fa fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="collapseUpcomingMilestone" class="panel-collapse collapse in">
                                <div class="panel-body" style="max-height: 300px; overflow: auto;">
                                    <div id="divUpcomingMilestone" class="col-md-12 plr0">
                                        <asp:GridView runat="server" ID="GrdMilestone" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Title" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMilestonetitle" runat="server" Width="100%" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Description" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMilestonedescription" runat="server" Width="100%" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMilestoneStatus" runat="server" Width="100%" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Department" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label ID="lbl2" runat="server" Text='<%# Eval("DeptName") %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                            <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("ContractTemplateID")%>'
                                                                ID="lnkBtnMilestoneResponse" runat="server" AutoPostBack="true" OnClick="lnkBtnMilestoneResponse_Click"
                                                                 data-placement="left" ToolTip="View Milestone Detail(s)/Submit Response" data-toggle="tooltip">
                                                                <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="View"/>  <%----%>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate></PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Upcoming(i.e. in next 30 days) Milestone Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                <asp:LinkButton ID="lnkBtnBindGrid1" OnClick="lnkBtnBindGrid1_Click" Style="float: right; display: none;" Width="100%" runat="server">
                </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="LinkButton1" Text="..Show More"
                                    style="margin-left: -5px;color: blue;font-style: italic;border: none;background: white;"
                                        PostBackUrl="~/ContractProduct/aspxPages/ContractMilestoneListNew.aspx"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="75%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divGraphDetails" role="dialog" aria-labelledby="myModal" aria-hidden="true">
        <div class="modal-dialog" style="width: 90%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="width: 100%; height: 635px">
                    <iframe id="showChartDetails" src="about:blank" style="min-height: 550px !important" scrolling="auto" frameborder="0"></iframe>
                    <%--onload="" --%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
