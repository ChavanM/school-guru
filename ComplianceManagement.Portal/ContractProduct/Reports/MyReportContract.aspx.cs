﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Reports
{
    public partial class MyReportContract : System.Web.UI.Page
    {
        protected bool flag;
       // public string IsEntityassignmentCustomer;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int customerID = -1;
                    int userid = -1;
                    userid = Convert.ToInt32(AuthenticationHelper.UserID);
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    //IsEntityassignmentCustomer = Convert.ToString(ConfigurationManager.AppSettings["IsEntityLocationAssignment"]);
                    BindVendors();
                    BindUsers();

                    //Bind Tree Views
                   // var branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                    List<NameValueHierarchy> branchList;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branchList);
                    }
                    else
                    {
                        branchList = CustomerBranchManagement.GetAllContractEntitiesAssignedHierarchyManagementSatutory(Convert.ToInt32(customerID));
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branchList);
                    }
                    //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);

                    BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);

                    BindDepartment();
                    BindContractStatusList(false, true);

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        string queryStr = Request.QueryString["Status"].ToString().Trim();
                        if (ddlContractStatus.Items.FindByText(queryStr) != null)
                        {
                            ddlContractStatus.ClearSelection();
                            ddlContractStatus.Items.FindByText(queryStr).Selected = true;
                        }
                    }

                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                DropDownListOwner.DataValueField = "ID";
                DropDownListOwner.DataTextField = "Name";
                DropDownListOwner.DataSource = internalUsers;
                DropDownListOwner.DataBind();

                DropDownListOwner.Items.Insert(0, new ListItem("Select Owner", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                tbxFilterLocation.Text = "Select Entity/Location";

                TreeNode node = new TreeNode("Select Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("Select Department", "-1"));
        }

        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = VendorDetails.GetVendors_All(customerID);

            ddlVendorPage.DataTextField = "VendorName";
            ddlVendorPage.DataValueField = "ID";

            ddlVendorPage.DataSource = lstVendors;
            ddlVendorPage.DataBind();

            ddlVendorPage.Items.Insert(0, new ListItem("Select Vendor", "-1"));
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                ddlContractStatus.Items.Insert(0, new ListItem("Select Status", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindGrid()
        {
            try
            {
                bool IsOnlycontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (!IsOnlycontract)
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    int branchID = -1;
                    int vendorID = -1;
                    int deptID = -1;
                    long contractStatusID = -1;
                    long contractTypeID = -1;
                    int OwnerID = -1;

                    if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                    {
                        OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                    {
                        vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                    {
                        contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                    var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                        3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                    //     IsEntityassignmentCustomer = Convert.ToString(ConfigurationManager.AppSettings["IsEntityLocationAssignment"]);

                    //if (IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                   // ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                   // if (isexist != null)
                   // {
                   //    var assignedLocationList = CustomerBranchManagement.GetAllAssignedContractListbyuser(Convert.ToInt32(customerID), AuthenticationHelper.UserID,AuthenticationHelper.Role);
                   //   lstAssignedContracts = lstAssignedContracts.Where(entry => assignedLocationList.Contains(entry.ContractNo)).ToList();
                   //}

                    #region Filter Code
                    if (!string.IsNullOrEmpty(tbxFilter.Text))
                    {
                        if (CheckInt(tbxFilter.Text))
                        {
                            int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                            lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ID == a || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ContractTitle.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                            //lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ID == a || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                            //&& entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())
                        }
                        else
                        {
                            lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ContractTitle.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                        }
                    }
                    if (OwnerID != -1)
                    {
                        lstAssignedContracts = lstAssignedContracts.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();
                    }
                    #endregion

                    if (lstAssignedContracts.Count > 0)
                    {
                        flag = true;
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = lstAssignedContracts.Count;
                    }
                    else
                    {
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = null;
                    }

                    Session["grdDetailData"] = (lstAssignedContracts).ToDataTable(); //(grdContractList.DataSource as List<Cont_SP_GetAssignedContracts_All_Result>).ToDataTable();

                    if (lstAssignedContracts.Count > 0)
                        btnExportExcel.Enabled = true;
                    else
                        btnExportExcel.Enabled = false;

                    lstAssignedContracts.Clear();
                    lstAssignedContracts = null;
                }
                else
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    int branchID = -1;
                    int vendorID = -1;
                    int deptID = -1;
                    long contractStatusID = -1;
                    long contractTypeID = -1;
                    int OwnerID = -1;

                    if (!string.IsNullOrEmpty(DropDownListOwner.SelectedValue))
                    {
                        OwnerID = Convert.ToInt32(DropDownListOwner.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                    {
                        vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                    {
                        contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                    List<Cont_GetMyReports> lstAssignedContracts = new List<Cont_GetMyReports>();
                    List<Cont_GetMyReports> query1 = new List<Cont_GetMyReports>();
                    List<Cont_GetMyReports> query2 = new List<Cont_GetMyReports>();

                    var lstAssignedContracts1 = ContractManagement.GetAssignedContractsListNew(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                        3, branchList, vendorID, deptID, contractStatusID, contractTypeID);


                    query1 = (from g in lstAssignedContracts1
                                            group g by new
                                            {
                                                g.ID, //ContractID
                                                g.CustomerID,
                                                g.ContractNo,
                                                g.ContractTitle,
                                                g.ContractDetailDesc,
                                                g.VendorIDs,
                                                g.VendorNames,
                                                g.CustomerBranchID,
                                                g.BranchName,
                                                g.DepartmentID,
                                                g.DeptName,
                                                g.ContractTypeID,
                                                g.TypeName,
                                                g.ContractSubTypeID,
                                                g.SubTypeName,
                                                g.ProposalDate,
                                                g.AgreementDate,
                                                g.EffectiveDate,
                                                g.ReviewDate,
                                                g.ExpirationDate,
                                                g.CreatedOn,
                                                g.UpdatedOn,
                                                g.StatusName,
                                                g.ContractAmt,
                                                g.ContactPersonOfDepartment,
                                                g.PaymentType,
                                                g.AddNewClause,
                                                g.Owner,
                                                g.AssignedUserID,
                                                g.IsDocument,
                                                g.OwnerIDs,
                                                g.LockInPeriodDate,
                                                g.FileNO,
                                                g.PhysicalLocation,
                                                g.Taxes,
                                                g.ContractStatusID
                                            } into GCS
                                            select new Cont_GetMyReports()
                                            {
                                                ID = GCS.Key.ID, //ContractID
                                                CustomerID = GCS.Key.CustomerID,
                                                ContractNo = GCS.Key.ContractNo,
                                                ContractTitle = GCS.Key.ContractTitle,
                                                ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                                VendorIDs = GCS.Key.VendorIDs,
                                                VendorNames = GCS.Key.VendorNames,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                BranchName = GCS.Key.BranchName,
                                                DepartmentID = GCS.Key.DepartmentID,
                                                DeptName = GCS.Key.DeptName,
                                                ContractTypeID = GCS.Key.ContractTypeID,
                                                TypeName = GCS.Key.TypeName,
                                                ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                                SubTypeName = GCS.Key.SubTypeName,
                                                ProposalDate = GCS.Key.ProposalDate,
                                                AgreementDate = GCS.Key.AgreementDate,
                                                EffectiveDate = GCS.Key.EffectiveDate,
                                                ReviewDate = GCS.Key.ReviewDate,
                                                ExpirationDate = GCS.Key.ExpirationDate,
                                                CreatedOn = GCS.Key.CreatedOn,
                                                UpdatedOn = GCS.Key.UpdatedOn,
                                                StatusName = GCS.Key.StatusName,
                                                ContractAmt = GCS.Key.ContractAmt,
                                                ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                                PaymentType = GCS.Key.PaymentType,
                                                AddNewClause = GCS.Key.AddNewClause,
                                                Owner = GCS.Key.Owner,
                                                AssignedUserID = GCS.Key.AssignedUserID,
                                                IsDocument = GCS.Key.IsDocument,
                                                OwnerIDs = GCS.Key.OwnerIDs,
                                                LockInPeriodDate = GCS.Key.LockInPeriodDate,
                                                FileNO = GCS.Key.FileNO,
                                                PhysicalLocation = GCS.Key.PhysicalLocation,
                                                Taxes = GCS.Key.Taxes,
                                                ContractStatusID = GCS.Key.ContractStatusID
                                            }).ToList();



                    var lstAssignedContracts2 = ContractManagement.GetAssignedContractsListOnlyAssigned(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                        3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                    //if (IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                    if (isexist != null)
                    {
                        query2 = (from g in lstAssignedContracts2
                                  group g by new
                                  {
                                      g.ID, //ContractID
                                      g.CustomerID,
                                      g.ContractNo,
                                      g.ContractTitle,
                                      g.ContractDetailDesc,
                                      g.VendorIDs,
                                      g.VendorNames,
                                      g.CustomerBranchID,
                                      g.BranchName,
                                      g.DepartmentID,
                                      g.DeptName,
                                      g.ContractTypeID,
                                      g.TypeName,
                                      g.ContractSubTypeID,
                                      g.SubTypeName,
                                      g.ProposalDate,
                                      g.AgreementDate,
                                      g.EffectiveDate,
                                      g.ReviewDate,
                                      g.ExpirationDate,
                                      g.CreatedOn,
                                      g.UpdatedOn,
                                      g.StatusName,
                                      g.ContractAmt,
                                      g.ContactPersonOfDepartment,
                                      g.PaymentType,
                                      g.AddNewClause,
                                      g.Owner,
                                      g.AssignedUserID,
                                      g.IsDocument,
                                      g.OwnerIDs,
                                      g.LockInPeriodDate,
                                      g.FileNO,
                                      g.PhysicalLocation,
                                      g.Taxes,
                                      g.ContractStatusID
                                  } into GCS
                                  select new Cont_GetMyReports()
                                  {
                                      ID = GCS.Key.ID, //ContractID
                                      CustomerID = GCS.Key.CustomerID,
                                      ContractNo = GCS.Key.ContractNo,
                                      ContractTitle = GCS.Key.ContractTitle,
                                      ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                      VendorIDs = GCS.Key.VendorIDs,
                                      VendorNames = GCS.Key.VendorNames,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      BranchName = GCS.Key.BranchName,
                                      DepartmentID = GCS.Key.DepartmentID,
                                      DeptName = GCS.Key.DeptName,
                                      ContractTypeID = GCS.Key.ContractTypeID,
                                      TypeName = GCS.Key.TypeName,
                                      ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                      SubTypeName = GCS.Key.SubTypeName,
                                      ProposalDate = GCS.Key.ProposalDate,
                                      AgreementDate = GCS.Key.AgreementDate,
                                      EffectiveDate = GCS.Key.EffectiveDate,
                                      ReviewDate = GCS.Key.ReviewDate,
                                      ExpirationDate = GCS.Key.ExpirationDate,
                                      CreatedOn = GCS.Key.CreatedOn,
                                      UpdatedOn = GCS.Key.UpdatedOn,
                                      StatusName = GCS.Key.StatusName,
                                      ContractAmt = GCS.Key.ContractAmt,
                                      ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                      PaymentType = GCS.Key.PaymentType,
                                      AddNewClause = GCS.Key.AddNewClause,
                                      Owner = GCS.Key.Owner,
                                      AssignedUserID = GCS.Key.AssignedUserID,
                                      IsDocument = GCS.Key.IsDocument,
                                      OwnerIDs = GCS.Key.OwnerIDs,
                                      LockInPeriodDate = GCS.Key.LockInPeriodDate,
                                      FileNO = GCS.Key.FileNO,
                                      PhysicalLocation = GCS.Key.PhysicalLocation,
                                      Taxes = GCS.Key.Taxes,
                                      ContractStatusID = GCS.Key.ContractStatusID
                                  }).ToList();

                        lstAssignedContracts = query1.Union(query2).Distinct().ToList();
                        lstAssignedContracts = lstAssignedContracts.Distinct().ToList();
                    }                   
                    else
                    {
                        lstAssignedContracts = query1.Distinct().ToList();
                    }


                    #region Filter Code
                    if (!string.IsNullOrEmpty(tbxFilter.Text))
                    {
                        if (CheckInt(tbxFilter.Text))
                        {
                            int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                            lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ID == a || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ContractTitle.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                            //lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ID == a || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                            //&& entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())
                        }
                        else
                        {
                            lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ContractTitle.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                        }
                    }
                    if (OwnerID != -1)
                    {
                        lstAssignedContracts = lstAssignedContracts.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();
                    }
                    #endregion

                    if (lstAssignedContracts.Count > 0)
                    {
                        flag = true;
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = lstAssignedContracts.Count;
                    }
                    else
                    {
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = null;
                    }

                    Session["grdDetailData"] = (lstAssignedContracts).ToDataTable(); //(grdContractList.DataSource as List<Cont_SP_GetAssignedContracts_All_Result>).ToDataTable();

                    if (lstAssignedContracts.Count > 0)
                        btnExportExcel.Enabled = true;
                    else
                        btnExportExcel.Enabled = false;

                    lstAssignedContracts.Clear();
                    lstAssignedContracts = null;

                }
            }            
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContractList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    if (ddlTypePage.SelectedValue == "1")
                        Response.Redirect("~/ContractProduct/aspxPages/ContractList.aspx", false);

                    else if (ddlTypePage.SelectedValue == "2")
                        Response.Redirect("~/ContractProduct/aspxPages/ContractTaskList.aspx", false);

                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton) (sender);
                if (btn != null)
                {
                    long contractInstanceID = Convert.ToInt64(btn.CommandArgument);

                    if (contractInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + contractInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdContractList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = AuthenticationHelper.UserID;                
                if (e.CommandName.Equals("DELETE_Contract"))
                {
                    int contractID = Convert.ToInt32(e.CommandArgument);
                    if (contractID != 0)
                    {
                        bool deleteSuccess = ContractManagement.DeleteContractByID(contractID, userID);

                        if (deleteSuccess)
                        {
                            BindGrid();
                            bindPageNumber();
                            ShowGridDetail();
                        }
                        if (deleteSuccess)
                        {
                            cvErrorContractListPage.IsValid = false;
                            cvErrorContractListPage.ErrorMessage = "Selected Contract Deleted Successfully";
                            vsContractListPage.CssClass = "alert alert-success fade-in";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContractList.PageIndex = chkSelectedPage - 1;

            grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
            ShowGridDetail();
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlDeptPage.ClearSelection();
                ddlVendorPage.ClearSelection();
                ddlContractStatus.ClearSelection();
                DropDownListOwner.ClearSelection();
                tbxFilter.Text = string.Empty;
                ClearTreeViewSelection(tvFilterLocation);

                tvFilterLocation_SelectedNodeChanged(sender, e);
                lnkBtnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Select Entity/Location";
                // tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdDetailData"] != null)
                    {
                        String FileName = String.Empty;

                        FileName = "ContractReport";

                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;

                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                        ExcelData = view.ToTable("Selected", false, "ContractNo", "StatusName", "ContractTitle", "ContractDetailDesc", "BranchName", "ContractAmt", "PaymentType", "DeptName", "ContactPersonOfDepartment", "TypeName", "SubTypeName", "VendorNames", "ProposalDate", "AgreementDate", "EffectiveDate", "ReviewDate", "ExpirationDate", "AddNewClause","Owner", "IsDocument", "LockInPeriodDate", "FileNO", "PhysicalLocation", "Taxes");

                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);

                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;

                                if (item["ProposalDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ProposalDate"])))
                                    {
                                        item["ProposalDate"] = Convert.ToDateTime(item["ProposalDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["AgreementDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["AgreementDate"])))
                                    {
                                        item["AgreementDate"] = Convert.ToDateTime(item["AgreementDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["EffectiveDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["EffectiveDate"])))
                                    {
                                        item["EffectiveDate"] = Convert.ToDateTime(item["EffectiveDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["ReviewDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ReviewDate"])))
                                    {
                                        item["ReviewDate"] = Convert.ToDateTime(item["ReviewDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["ExpirationDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ExpirationDate"])))
                                    {
                                        item["ExpirationDate"] = Convert.ToDateTime(item["ExpirationDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                                if (item["LockInPeriodDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["LockInPeriodDate"])))
                                    {
                                        item["LockInPeriodDate"] = Convert.ToDateTime(item["LockInPeriodDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                            }

                            var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = customer.Name;
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "Contract Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Contract No.";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "Status";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "Contract Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(50);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "Description";
                            exWorkSheet.Cells["E5"].AutoFitColumns(50);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Entity/Branch/Location";
                            exWorkSheet.Cells["F5"].AutoFitColumns(25);

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Contract Amount/Value";
                            exWorkSheet.Cells["G5"].AutoFitColumns(25);

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "Payment Type";
                            exWorkSheet.Cells["H5"].AutoFitColumns(25);

                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["I5"].Value = "Department";
                            exWorkSheet.Cells["I5"].AutoFitColumns(25);

                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["J5"].Value = "Contact Person Of Deapartment";
                            exWorkSheet.Cells["J5"].AutoFitColumns(25);

                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["K5"].Value = "Contract Type";
                            exWorkSheet.Cells["K5"].AutoFitColumns(25);

                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["L5"].Value = "Contract Sub-Type";
                            exWorkSheet.Cells["L5"].AutoFitColumns(25);

                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["M5"].Value = "Vendor";
                            exWorkSheet.Cells["M5"].AutoFitColumns(25);

                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["N5"].Value = "Proposal Date";
                            exWorkSheet.Cells["N5"].AutoFitColumns(15);

                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["O5"].Value = "Agreement Date";
                            exWorkSheet.Cells["O5"].AutoFitColumns(15);

                            exWorkSheet.Cells["P5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["P5"].Value = "Start Date";
                            exWorkSheet.Cells["P5"].AutoFitColumns(15);

                            exWorkSheet.Cells["Q5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["Q5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["Q5"].Value = "Review Date";
                            exWorkSheet.Cells["Q5"].AutoFitColumns(15);

                            exWorkSheet.Cells["R5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["R5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["R5"].Value = "End Date";
                            exWorkSheet.Cells["R5"].AutoFitColumns(15);

                            exWorkSheet.Cells["S5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["S5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["S5"].Value = "Remark";
                            exWorkSheet.Cells["S5"].AutoFitColumns(15);

                            exWorkSheet.Cells["T5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["T5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["T5"].Value = "Owner(s)";
                            exWorkSheet.Cells["T5"].AutoFitColumns(15);

                            exWorkSheet.Cells["u5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["u5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["u5"].Value = "Document Status";
                            exWorkSheet.Cells["u5"].AutoFitColumns(15);

                            exWorkSheet.Cells["v5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["v5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["v5"].Value = "Lock-In Period";
                            exWorkSheet.Cells["v5"].AutoFitColumns(15);

                            exWorkSheet.Cells["W5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["W5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["W5"].Value = "File NO";
                            exWorkSheet.Cells["W5"].AutoFitColumns(15);

                            exWorkSheet.Cells["X5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["X5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["X5"].Value = " File Physical Location";
                            exWorkSheet.Cells["X5"].AutoFitColumns(15);

                            exWorkSheet.Cells["Y5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["Y5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["Y5"].Value = "Taxes";
                            exWorkSheet.Cells["Y5"].AutoFitColumns(15);

                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 25])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 8, 5 + ExcelData.Rows.Count, 25])
                            {
                                col.Style.Numberformat.Format = "dd/MM/yyyy";
                            }

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            //Response.End();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        else
                        {
                            cvErrorContractListPage.IsValid = false;
                            cvErrorContractListPage.ErrorMessage = "No data available to export for current selection(s)";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdContractList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                bool IsOnlycontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (!IsOnlycontract)
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    int branchID = -1;
                    int vendorID = -1;
                    int deptID = -1;
                    long contractStatusID = -1;
                    long contractTypeID = -1;

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                    {
                        vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                    {
                        contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                    var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                        3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                    string SortExpr = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (SortExpr == e.SortExpression)
                        {
                            if (direction == SortDirection.Ascending)
                            {
                                direction = SortDirection.Descending;
                            }
                            else
                            {
                                direction = SortDirection.Ascending;
                            }
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }

                    if (direction == SortDirection.Ascending)
                    {
                        ViewState["Direction"] = "Ascending";
                        lstAssignedContracts = lstAssignedContracts.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    }
                    else
                    {
                        ViewState["Direction"] = "Descending";
                        lstAssignedContracts = lstAssignedContracts.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    }

                    ViewState["SortExpression"] = e.SortExpression;

                    foreach (DataControlField field in grdContractList.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["SortIndex"] = grdContractList.Columns.IndexOf(field);
                        }
                    }
                    if (lstAssignedContracts.Count > 0)
                    {
                        flag = true;
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = lstAssignedContracts.Count;
                    }
                    else
                    {
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = null;
                    }

                    Session["grdDetailData"] = (lstAssignedContracts).ToDataTable(); //(grdContractList.DataSource as List<Cont_SP_GetAssignedContracts_All_Result>).ToDataTable();
                    if (lstAssignedContracts.Count > 0)
                        btnExportExcel.Enabled = true;
                    else
                        btnExportExcel.Enabled = false;
                }
                else
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    int branchID = -1;
                    int vendorID = -1;
                    int deptID = -1;
                    long contractStatusID = -1;
                    long contractTypeID = -1;

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                    {
                        vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                    {
                        contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                    var lstAssignedContracts = ContractManagement.GetAssignedContractsListNew(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                        3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                    string SortExpr = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (SortExpr == e.SortExpression)
                        {
                            if (direction == SortDirection.Ascending)
                            {
                                direction = SortDirection.Descending;
                            }
                            else
                            {
                                direction = SortDirection.Ascending;
                            }
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }

                    if (direction == SortDirection.Ascending)
                    {
                        ViewState["Direction"] = "Ascending";
                        lstAssignedContracts = lstAssignedContracts.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    }
                    else
                    {
                        ViewState["Direction"] = "Descending";
                        lstAssignedContracts = lstAssignedContracts.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    }

                    ViewState["SortExpression"] = e.SortExpression;

                    foreach (DataControlField field in grdContractList.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["SortIndex"] = grdContractList.Columns.IndexOf(field);
                        }
                    }
                    if (lstAssignedContracts.Count > 0)
                    {
                        flag = true;
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = lstAssignedContracts.Count;
                    }
                    else
                    {
                        grdContractList.DataSource = lstAssignedContracts;
                        grdContractList.DataBind();
                        Session["TotalRows"] = null;
                    }

                    Session["grdDetailData"] = (lstAssignedContracts).ToDataTable(); //(grdContractList.DataSource as List<Cont_SP_GetAssignedContracts_All_Result>).ToDataTable();
                    if (lstAssignedContracts.Count > 0)
                        btnExportExcel.Enabled = true;
                    else
                        btnExportExcel.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        #region add Filter
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        #endregion

    }
}