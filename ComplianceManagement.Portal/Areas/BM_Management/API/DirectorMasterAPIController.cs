﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Setting;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/DirectorMasterAPI")]
    public class DirectorMasterAPIController : ApiController
    {
        IDirectorMaster objIDirectorMaster;
        ISettingService objISettingService;
        IEntityMaster objentity;

        public DirectorMasterAPIController(IDirectorMaster obj, ISettingService objISettingService, IEntityMaster objentity)
        {
            objIDirectorMaster = obj;
            this.objISettingService = objISettingService;
            this.objentity = objentity;
        }
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        [Route("GetDirector")]
        [HttpGet]
        public DataSourceResult GetDirector([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIDirectorMaster.GetDirectors(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetDirectorHistory")]
        [HttpGet]
        public DataSourceResult GetDirectorHistory([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long ID)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIDirectorMaster.GetDirectorsHistory(ID);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("CreateDirector")]
        [HttpPost]
        public HttpResponseMessage CreateDirector(Director_MasterVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            return response;

        }

        [Route("UpdateDirector")]
        [HttpPost]
        public HttpResponseMessage UpdateDirector(Director_MasterVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            return response;

        }


        [Route("GetRelativesList")]
        [HttpGet]
        public IEnumerable<Relatives> GetRelativesList(int Director_Id)
        {
            return objIDirectorMaster.GetRelativeForDropdown(Director_Id);
            //if(lstRelatives == null)
            //{
            //    lstRelatives = new List<Relatives>();
            //}
            //lstRelatives.Add(new Relatives() { Id = 0, Name = "Select" });
            //return lstRelatives;
        }

        [Route("DirectorList_ForEntity1")]
        [HttpGet]
        public IEnumerable<DirectorList_ForEntity> GetDirectorMasterList(int EntityId, int? CommitteeId, int? DetailsOfCommitteeId, bool? AddSelectOption)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objIDirectorMaster.GetDirectorForEntity(customerId, EntityId, CommitteeId, DetailsOfCommitteeId);
            if (AddSelectOption == true)
            {
                if (result == null)
                {
                    result = new List<DirectorList_ForEntity>() { new DirectorList_ForEntity() { Director_ID = 0, FullName = "Select", Entity_Id = EntityId } };
                }
                else
                {
                    result.Insert(0, new DirectorList_ForEntity() { Director_ID = 0, FullName = "Select", Entity_Id = EntityId });
                }
            }
            return result;
        }

        [Route("DirectorList_ForEntity")]
        [HttpGet]
        public DataSourceResult GetDirectorMasterList([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIDirectorMaster.GetDirectorListOfEntity(EntityId, SecretarialConst.MeetingTypeID.BOARD);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("BODforEntity")]
        [HttpGet]
        public DataSourceResult BODforEntity([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIDirectorMaster.BODforEntity(userID, EntityId, CustomerId, -1);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };

            return result;
        }

        [Route("GetCommitteeForEntity")]
        [HttpGet]
        public DataSourceResult GetCommitteeForEntity([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIDirectorMaster.GetCommitteeForEntity(customerId, EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetCommitteeMasterNew")]
        [HttpGet]
        public DataSourceResult GetCommitteeMasterNew([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIDirectorMaster.GetCommitteeMasterNew(customerId, EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count,
            };
            return result;
        }

        [Route("GetNatureOfInterestByEntityType")]
        [HttpGet]
        public IEnumerable<NatureOfInterestVM> GetNatureOfInterestByEntityType(int EntityType)
        {
            return objIDirectorMaster.GetNatureOfInterestByEntityType(EntityType);
        }

        #region Details of Intrest

        [Route("GetDetailsofIntrest")]
        [HttpGet]
        public DataSourceResult GetCommitteeDtls([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int DirectorId)
        {
            long DirectorIds = Convert.ToInt32(DirectorId);
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIDirectorMaster.DetailsOfIntrestList(DirectorIds, customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [Route("GetDropDownEntityForDetailsOfInterest")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetDropDownEntityForDetailsOfInterest(long Director_Id)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var item = objIDirectorMaster.GetAllEntityMaster(customerId, Director_Id);
            if (item == null)
            {
                item = new List<EntityMasterVM>();
            }
            item.Add(new EntityMasterVM() { Id = -1, EntityName = "Other", Entity_Type = 0 });
            return item;
            //return objEntityMaster.GetCompanyType();
        }

        [Route("CreateDetailsofIntrest")]
        [HttpPost]
        public IHttpActionResult CreateDetailsofIntrest(DetailsOfInterest obj)
        {
            DetailsOfInterest _objIntrest = new DetailsOfInterest();
            try
            {
                int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                string result = string.Empty;
                if (obj.Id == 0)
                {
                    _objIntrest = objIDirectorMaster.CreateDetailsOfInterest(obj, CustomerId, userID);

                    if (_objIntrest != null && _objIntrest.Response.Error == false)
                    {
                        if ((_objIntrest.NatureOfInterest == 2 || _objIntrest.NatureOfInterest == 10 || _objIntrest.NatureOfInterest == 9) && (_objIntrest.Director_ID > 0) && _objIntrest.Entity_Id > 0)
                        {
                            bool IsUserExist = (from y in entities.BM_DirectorMaster where y.UserID > 0 where y.Id == _objIntrest.Director_ID select y).Any();
                            if (IsUserExist == false)
                            {
                                CreateNewUser(_objIntrest);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _objIntrest.Response = new Response();
                _objIntrest.Response.Message = "Server error occured";
            }
            return Json(_objIntrest.Response.Message);
        }

        private void CreateNewUser(DetailsOfInterest _objIntrest)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                #region add User
                var createUserOfDirector = false;
                string IS_ICSI_MODE = ConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"].ToString();

                if (IS_ICSI_MODE != "1")
                {
                    createUserOfDirector = objISettingService.CheckCreateUserOfDirectorId(customerId);
                }

                //Create User with Director Role
                UserVM user = new UserVM();
                user.UserID = 0;

                if (createUserOfDirector == true)
                {
                    var getDirectorDetails = (from x in entities.BM_DirectorMaster where x.Id == _objIntrest.Director_ID && x.IsActive == true && x.Is_Deleted == false && x.Customer_Id == customerId select x).FirstOrDefault();
                    user.Email = getDirectorDetails.EmailId_Official;
                    user.FirstName = getDirectorDetails.FirstName;
                    user.LastName = getDirectorDetails.LastName;
                    user.CustomerID = customerId;
                    user.ContactNumber = getDirectorDetails.MobileNo;
                    user.Address = getDirectorDetails.Present_Address_Line1;

                    var tempUserId = objIDirectorMaster.GetUserIdForCreateNewDirector(getDirectorDetails.EmailId_Official, customerId);
                    if (tempUserId > 0)
                    {
                        user.UserID = (long)tempUserId;
                    }

                    if (user.UserID == 0)
                    {
                        user = CreateUser(user);
                    }
                    else
                    {
                        user = UpdateUser(user, getDirectorDetails.EmailId_Official);
                    }
                }
                //End
                #endregion
            }
            catch (Exception ex)
            {

            }
        }

        [NonAction]
        public UserVM CreateUser(UserVM obj, bool isActive = true)
        {
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                obj.Message = new Response();

                obj.CreatedBy = UserID;
                obj.CustomerID = CustomerID;
                obj.CreatedByText = AuthenticationHelper.User;

                var hasError = false;

                string Mailbody = getEmailMessage(obj, obj.Password, out hasError);
                if (hasError == true)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something went wrong, Please try again";
                }
                else
                {
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                    IUser objIUser = new BM_ManegmentServices.Services.Masters.User();
                    obj.SecretarialRoleId = objIUser.GetRoleIdFromCode(SecretarialConst.Roles.DRCTR);
                    var result = objIUser.Create(obj, SenderEmailAddress, isActive);

                    if (result.Message.Success)
                    {
                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Account Created.", Mailbody);
                    }

                    obj.Message.Success = result.Message.Success;
                    obj.Message.Message = result.Message.Message;
                    obj.Message.Error = result.Message.Error;
                }
            }
            else
            {
                obj.Message.Error = true;
            }
            return obj;
        }
        private string getEmailMessage(UserVM user, string passwordText, out bool hasError)
        {
            try
            {
                hasError = false;
                int customerID = -1;
                string ReplyEmailAddressName = "";
                int customerId = Convert.ToInt32(AuthenticationHelper.UserID);

                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalurl = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalurl = Urloutput.URL;
                }
                else
                {
                    portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                       .Replace("@Username", user.Email)
                                       .Replace("@User", username)
                                       .Replace("@PortalURL", Convert.ToString(portalurl))
                                       .Replace("@Password", passwordText)
                                       .Replace("@From", ReplyEmailAddressName)
                                       .Replace("@URL", Convert.ToString(portalurl));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                hasError = true;
                //cvUserPopup.ErrorMessage = "Something went wrong, Please try again";
                return null;
            }
        }

        [NonAction]
        public UserVM UpdateUser(UserVM obj, string Email_Old)
        {
            obj.Message = new BM_ManegmentServices.VM.Response();
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                obj.Message = new Response();

                obj.CreatedBy = UserID;
                obj.CustomerID = CustomerID;
                obj.CreatedByText = AuthenticationHelper.User;

                var hasError = false;

                string Mailbody = SendNotificationEmailChanged(obj, out hasError);
                if (hasError == true)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something went wrong, Please try again";
                }
                else
                {
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    IUser objIUser = new BM_ManegmentServices.Services.Masters.User();
                    var tempUser = objIUser.GetUser(CustomerID, Email_Old);

                    if (tempUser != null)
                    {
                        obj.UserID = tempUser.UserID;
                        obj.SecretarialRoleId = objIUser.GetRoleIdFromCode(SecretarialConst.Roles.DRCTR);
                        var result = objIUser.Update(obj, SenderEmailAddress, out Email_Old);

                        if (result.Message.Success)
                        {
                            if (Email_Old != result.Email)
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Email ID Changed.", Mailbody);

                            obj.Message.Success = true;
                        }
                        else
                        {
                            obj.Message.Error = true;
                            obj.Message.Message = result.Message.Message;
                        }

                    }
                    else
                    {
                        obj.Message.Error = true;
                        obj.Message.Message = "Something went wrong, Please try again";
                    }
                }
            }
            else
            {
                obj.Message.Error = true;
                obj.Message.Message = "Something went wrong, Please try again";
            }
            return obj;
        }

        private string SendNotificationEmailChanged(UserVM user, out bool hasError)
        {
            hasError = false;
            try
            {
                user.Message = new Response();
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    //customerID = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).CustomerID ?? 0;
                    customerID = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalurl = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalurl = Urloutput.URL;
                }
                else
                {
                    portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                       .Replace("@Username", user.Email)
                                       .Replace("@User", username)
                                       .Replace("@PortalURL", Convert.ToString(portalurl))
                                       .Replace("@From", ReplyEmailAddressName)
                                       .Replace("@URL", Convert.ToString(portalurl));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                hasError = true;
                user.Message.Error = true;
                user.Message.Message = "Something went wrong, Please try again";

            }
            return null;
        }

        [Route("DeleteDetailsOfInterest")]
        [HttpDelete]
        public IHttpActionResult DeleteDetailsOfInterest(int Id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string Message = string.Empty;
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                Message = objIDirectorMaster.DeleteDetailsOfInterest(Id, userID);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Json(Message);

        }
        #endregion

        #region Template Fields

        [Route("DirectorMaster")]
        [HttpGet]
        public IEnumerable<DirectorMasterListVM> DirectorMaster()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objIDirectorMaster.DirectorMasterList(customerId);
        }

        //BOD
        [Route("BOD")]
        [HttpGet]
        public IEnumerable<DirectorMasterListVM> BOD(int EntityId, bool? showAll)
        {
            var showAllInList = false;
            if(showAll != null)
            {
                showAllInList = (bool) showAll;
            }
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objIDirectorMaster.BOD(EntityId, customerId, showAllInList);
        }

        [Route("GetManagementEntityWise")]
        [HttpGet]
        public IEnumerable<DirectorMasterListVM> GetManagementEntityWise(int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objIDirectorMaster.GetManagementEntityWise(EntityId, customerId, true);
        }

        [Route("DirectorDetails")]
        [HttpGet]
        public IEnumerable<Directors> DirectorDetails(int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objIDirectorMaster.DirectorDetails(EntityId, customerId);
        }
        #endregion

        [Route("CountProfileUpdate")]
        [HttpPost]
        public IHttpActionResult CountProfileUpdate()
        {
            try
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);

                int result = objIDirectorMaster.CountProfileUpdate(userID);

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json("Server error occured");
            }
        }

        [Route("GetBodyCorporateName")]
        [HttpGet]
        public IEnumerable<Body_CorporateVM> GetBodyCorporateName()
        {
            int CustomerId=Convert.ToInt32(AuthenticationHelper.CustomerID);
            List<Body_CorporateVM> _objlist = objentity.GetBodyCorporateNomnee(CustomerId);
            return _objlist;
        }

        #region Appointment/Cessation/Change in designation
        [Route("DirectorListFor")]
        [HttpGet]
        public IEnumerable<DirectorMasterListVM> DirectorListForAppointmentCessation(int entityId, int? designationId, long? detailsId, string listFor)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objIDirectorMaster.DirectorListForAppointmentCessation(entityId, customerId, designationId, detailsId, listFor);
        }
        #endregion

        [Route("GetDirectorListCompanywise")]
        [HttpGet]
        public DataSourceResult GetDirectorListCompanywise([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, string filterText = "")
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = AuthenticationHelper.UserID;
            var gridData = objIDirectorMaster.GetDirectorListinCompany(customerId, userId);

            if (!String.IsNullOrEmpty(filterText))
            {
                filterText = filterText.Replace(",", "");
                gridData = gridData.Where(row => row.FullName.Trim().ToUpper().Equals(filterText.Trim().ToUpper())).ToList();
            }

            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}
