﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/CommitteeCompAPI")]
    public class CommitteeCompAPIController : ApiController
    {
        ICommitteeComp objICommitteeComp;

        public CommitteeCompAPIController(ICommitteeComp obj)
        {
            objICommitteeComp = obj;
        }

        [Route("GetAllCommittee")]
        [HttpGet]
        public DataSourceResult GetAllCommittee([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objICommitteeComp.GetAllCommitteeCompList(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetCommitteeCompEntitywise")]
        [HttpGet]
        public DataSourceResult GetCommitteeCompEntitywise([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long EntityId, int CustomerId)
        {

            var gridData = objICommitteeComp.GetCommitteeCompEntitywise(EntityId, CustomerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("CreateCommitteeComp")]
        [HttpPost]
        public IHttpActionResult CreateEntity(CommitteeCompVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                var result = objICommitteeComp.CreateOrUpdate(obj);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("DeleteCommitteeComp")]
        [HttpDelete]
        public HttpResponseMessage DeleteEntity(int Id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (Id > 0)
            {
                bool result = objICommitteeComp.Delete(Id);
            }

            return response;
        }

        [Route("UpdateCommitteeComp")]
        [HttpPut]
        public HttpResponseMessage UpdateEntity(CommitteeCompVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                var result = objICommitteeComp.Update(obj);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }

            return response;
        }

        [Route("GetCommitteeCompForDropDown")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetCommitteeCompForDropDown()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objICommitteeComp.GetAllCommitteeComp(customerId);
            List<CommitteeCompVM> lst = result.ToList();
            //lst.Insert(0, new CommitteeCompVM() { Id = -1, Name = "Select" });

            lst.Add(new CommitteeCompVM() { Id = 0, Name = "Other" });
            return lst;
        }

        [Route("GetCommitteeCompForCommitteeDropDown")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetCommitteeCompForCommitteeDropDown()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objICommitteeComp.GetAllCommitteeComp(customerId);
            List<CommitteeCompVM> lst = result.ToList();
            lst.Insert(0, new CommitteeCompVM() { Id = -1, Name = "Select" });

            return lst;
        }

        [Route("GetCommitteeMasterDropdown")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetCommitteeMasterDropdown()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objICommitteeComp.GetAllCommitteeComp(customerId);
        }

        [Route("GetCommitteeMasterForDropDown")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetCommitteeMasterForDropDown()
        {
            var result = objICommitteeComp.GetAllCommitteeComp(null);
            return result;
        }

        [Route("GetMeetingTypeForDropDown")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetMeetingTypeForDropDown()
        {
            var result = objICommitteeComp.GetAllMeetingType();
            return result;
        }

        [Route("GetMeetingTypeByEntity")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetMeetingTypeByEntity(string type, int entityID)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var result = objICommitteeComp.GetMeetingTypes(type, customerID, entityID, false);
            return result;
        }
        [Route("GetMeetingTypeByEntityWithGM")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetMeetingTypeByEntityWithGM(string type, int entityID)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var result = objICommitteeComp.GetMeetingTypes(type, customerID, entityID, true);
            return result;
        }

        [Route("GetDates")]
        [HttpGet]
        public IEnumerable<Days> GetDates(string Month_Id)
        {
            List<Days> lst = new List<Days>();
            lst.Add(new Days() { Day_ = 0, DayName_ = "Select" });
            for (int i = 1; i < 29; i++)
            {
                lst.Add(new Days() { Day_ = i, DayName_ = i.ToString() });
            }

            if (Month_Id == "4" || Month_Id == "6" || Month_Id == "9" || Month_Id == "11")
            {
                lst.Add(new Days() { Day_ = 29, DayName_ = "29" });
                lst.Add(new Days() { Day_ = 30, DayName_ = "30" });
            }
            else if (Month_Id == "1" || Month_Id == "3" || Month_Id == "5" || Month_Id == "7" || Month_Id == "8" || Month_Id == "10" || Month_Id == "12")
            {
                lst.Add(new Days() { Day_ = 29, DayName_ = "29" });
                lst.Add(new Days() { Day_ = 30, DayName_ = "30" });
                lst.Add(new Days() { Day_ = 31, DayName_ = "31" });
            }
            return lst;
        }

        [Route("GetMeetingTypeByCustomer")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetAllMeetingTypeByCustomer()
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var result = objICommitteeComp.GetAll_MeetingTypeByCustomer(customerID, false);
            return result;
        }

        [Route("GetMeetingTypeForAgendaMaster")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetMeetingTypeForAgendaMaster(bool showAll)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            string role = AuthenticationHelper.Role;
            var result = objICommitteeComp.GetAllMeetingType(customerID, showAll, role);
            return result;
        }
    }
}