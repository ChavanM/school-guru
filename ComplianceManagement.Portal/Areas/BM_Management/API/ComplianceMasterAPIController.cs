﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/ComplianceMasterAPI")]
    public class ComplianceMasterAPIController : ApiController
    {
        ICompliance objICompliance;

        public ComplianceMasterAPIController(ICompliance obj)
        {
            objICompliance = obj;
        }

        [Route("GetSecretarialCompliances")]
        [HttpGet]
        public DataSourceResult GetSecretarialCompliances([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objICompliance.GetAllSecretarialCompliances();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetSecretarialCompliancesDropdown")]
        [HttpGet]
        public IEnumerable<ComplianceVM> GetSecretarialCompliancesDropdown()
        {
            var item = objICompliance.GetAllSecretarialCompliances();
            if (item == null)
            {
                item = new List<ComplianceVM>();
            }
            return item;
        }

        [Route("GetComplianceStatusUpdateDropdown")]
        [HttpGet]
        public IEnumerable<ComplianceStatusVM> GetComplianceStatusUpdateDropdown(long scheduleOnID)
        {
            var userID = AuthenticationHelper.UserID;
            var item = objICompliance.GetComplianceUpdateStatus(scheduleOnID, userID);
            return item;
        }

    }
}