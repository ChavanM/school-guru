﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Net.Http.Headers;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using System.Text;
using System.Web.Script.Serialization;

using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Registers;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Services.Compliance;
using Newtonsoft.Json;

using Kendo.Mvc.UI;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/VideoMeeting")]
    public class VideoMeetingAPIController : ApiController,IDisposable
    {
        IVideoMeeting _objvideomeeting;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
        string Token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IlNzQ2ZoWXpkVGxpZUdZSGtSMC1BZmciLCJleHAiOjE2MDkxODM4MDAsImlhdCI6MTYwODI5NTEyMH0.jGf8lrqFnrnof4CZSPh3x3edMGrtLclz4lxSQX95MAE";
        

        public VideoMeetingAPIController(IVideoMeeting _objvideomeeting)
        {
            this._objvideomeeting = _objvideomeeting;
        }

        //string URL = "https://api.zoom.us/v2/users/me/meetings";
        [Route("CreateMeeting")]
        [HttpPost]
        public HttpResponseMessage CreateMeeting(long MeetingId)
        {
            HttpResponseMessage responses = Request.CreateResponse(HttpStatusCode.OK);
            VMeetingVM Data = new VMeetingVM();
            VideoMeetingVM objmeetingVM = new VideoMeetingVM();

            BM_VideoMeeting Value = new BM_VideoMeeting();
            try
            {
                Data = _objvideomeeting.GetMeetingDetail(CustomerId, MeetingId);


                var obj1 = new
                {

                    topic = Data.MeetingTitle,
                    type = " 2",
                    start_time = Data.MeetingDate + "T" + Data.MeetingTime,
                    duration = " 60",
                    timezone = " Asia/India",
                    password = " password",
                    agenda = Data.EntityName,
                    recurrence = new
                    {
                        type = 1,
                        repeat_interval = 1,
                        weekly_days = "1,2,3,4,5",
                        end_times = 120
                    },

                    settings = new
                    {
                        host_video = " true",
                        participant_video = "true",
                        cn_meeting = " false",
                        in_meeting = " true",
                        join_before_host = "false",
                        mute_upon_entry = " false",
                        watermark = " true",
                        use_pmi = " false",
                        approval_type = "0",
                        registration_type = "3",
                        audio = " both",
                        waiting_room = "true",
                        auto_recording = " Local",
                        enforce_login = " false",
                        alternative_hosts = " "
                    }
                };

                //"{\"topic\":\"Test Meeting\",\"type\":2,\"start_time\":\"2020-11-04T05:35:01\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"Agenda\",\"recurrence\":{\"type\":1,\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":true,\"mute_upon_entry\":false,\"watermark\":false,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}"
                var client = new RestSharp.RestClient("https://api.zoom.us/v2/users/me/meetings");
                var request = new RestSharp.RestRequest(RestSharp.Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer" + Token);
                request.AddParameter("application/json", "{\"topic\":\"" + obj1.topic + "\",\"type\":" + obj1.type + ",\"start_time\":\"" + obj1.start_time + "\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"" + obj1.agenda + "\",\"recurrence\":{\"type\":" + obj1.type + ",\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":true,\"mute_upon_entry\":false,\"watermark\":true,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}", RestSharp.ParameterType.RequestBody);
                //request.AddParameter("application/json",obj1, RestSharp.ParameterType.RequestBody);
                ServicePointManager.MaxServicePointIdleTime = 1000;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                       SecurityProtocolType.Tls11 |
                                       SecurityProtocolType.Tls12;
                RestSharp.IRestResponse response = client.Execute(request);
                string Check = obj1.ToJSON();
                if (response.StatusCode.ToString().ToLower() == "created")
                {
                    VideoMeetingVM account = JsonConvert.DeserializeObject<VideoMeetingVM>(response.Content);

                    if (account != null)
                    {
                      objmeetingVM = _objvideomeeting.SaveVideoMeeting(account, MeetingId, UserId);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            responses.Content = new StringContent("Video Conference created successfully");
            responses.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return responses;
        }

        [Route("UpdateMeeting")]
        [HttpPatch]
        public HttpResponseMessage UpdateMeeting(long MeetingId)
        {
            HttpResponseMessage responses = Request.CreateResponse(HttpStatusCode.OK);
            VMeetingVM Data = new VMeetingVM();
            VideoMeetingVM objmeetingVM = new VideoMeetingVM();
            string MeetingVId = _objvideomeeting.GetVMeetingId(MeetingId);

            BM_VideoMeeting Value = new BM_VideoMeeting();
            try
            {
                Data = _objvideomeeting.GetMeetingDetail(CustomerId, MeetingId);


                var obj1 = new
                {

                    topic = Data.MeetingTitle,
                    type = " 2",
                    start_time = Data.MeetingDate + "T" + Data.MeetingTime,
                    duration = " 60",
                    timezone = " Asia/India",
                    password = " password",
                    agenda = Data.EntityName,
                    recurrence = new
                    {
                        type = 1,
                        repeat_interval = 1,
                        weekly_days = "1,2,3,4,5",
                        end_times = 120
                    },

                    settings = new
                    {
                        host_video = " true",
                        participant_video = " true",
                        cn_meeting = " false",
                        in_meeting = " true",
                        join_before_host = " true",
                        mute_upon_entry = " false",
                        watermark = " false",
                        use_pmi = " false",
                        approval_type = " 0",
                        registration_type = " 3",
                        audio = " both",
                        waiting_room = "false",
                        auto_recording = " Local",
                        enforce_login = " false",
                        alternative_hosts = " "
                    }
                };

                //"{\"topic\":\"Test Meeting\",\"type\":2,\"start_time\":\"2020-11-04T05:35:01\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"Agenda\",\"recurrence\":{\"type\":1,\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":true,\"mute_upon_entry\":false,\"watermark\":false,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}"
                var client = new RestSharp.RestClient("https://api.zoom.us/v2/meetings/"+MeetingVId);
                var request = new RestSharp.RestRequest(RestSharp.Method.PATCH);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer" + Token);
                request.AddParameter("application/json", "{\"topic\":\"" + obj1.topic + "\",\"type\":" + obj1.type + ",\"start_time\":\"" + obj1.start_time + "\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"" + obj1.agenda + "\",\"recurrence\":{\"type\":" + obj1.type + ",\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":false,\"mute_upon_entry\":false,\"watermark\":true,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}", RestSharp.ParameterType.RequestBody);
                //request.AddParameter("application/json",obj1, RestSharp.ParameterType.RequestBody);
                ServicePointManager.MaxServicePointIdleTime = 1000;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                       SecurityProtocolType.Tls11 |
                                       SecurityProtocolType.Tls12;
                RestSharp.IRestResponse response = client.Execute(request);
                string Check = obj1.ToJSON();
                if (response.ErrorMessage != "BAD REQUEST")
                {
                    VideoMeetingVM account = JsonConvert.DeserializeObject<VideoMeetingVM>(response.Content);


                    if (account != null)

                    {
                       objmeetingVM = _objvideomeeting.SaveVideoMeeting(account, MeetingId, UserId);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            responses.Content = new StringContent("Video Conference updated successfully");
            responses.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return responses;
        }

        [Route("DeleteMeeting")]
        [HttpDelete]
        public HttpResponseMessage DeleteMeeting(long MeetingId)
        {
            HttpResponseMessage responses = Request.CreateResponse(HttpStatusCode.OK);
            VMeetingVM Data = new VMeetingVM();
            VideoMeetingVM objmeetingVM = new VideoMeetingVM();
           string MeetingVId = _objvideomeeting.GetVMeetingId(MeetingId);
            BM_VideoMeeting Value = new BM_VideoMeeting();
            try
            {
                Data = _objvideomeeting.GetMeetingDetail(CustomerId, MeetingId);


                var obj1 = new
                {

                    topic = Data.MeetingTitle,
                    type = " 2",
                    start_time = Data.MeetingDate + "T" + Data.MeetingTime,
                    duration = " 60",
                    timezone = " Asia/India",
                    password = " password",
                    agenda = Data.EntityName,
                    recurrence = new
                    {
                        type = 1,
                        repeat_interval = 1,
                        weekly_days = "1,2,3,4,5",
                        end_times = 120
                    },

                    settings = new
                    {
                        host_video = " true",
                        participant_video = " true",
                        cn_meeting = " false",
                        in_meeting = " true",
                        join_before_host = " true",
                        mute_upon_entry = " false",
                        watermark = " false",
                        use_pmi = " false",
                        approval_type = " 0",
                        registration_type = " 3",
                        audio = " both",
                        waiting_room = "false",
                        auto_recording = " Local",
                        enforce_login = " false",
                        alternative_hosts = " "
                    }
                };

                //"{\"topic\":\"Test Meeting\",\"type\":2,\"start_time\":\"2020-11-04T05:35:01\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"Agenda\",\"recurrence\":{\"type\":1,\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":true,\"mute_upon_entry\":false,\"watermark\":false,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}"
                var client = new RestSharp.RestClient("https://api.zoom.us/v2/meetings/"+ MeetingVId);
                var request = new RestSharp.RestRequest(RestSharp.Method.DELETE);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer" + Token);
               // request.AddParameter("application/json", "{\"topic\":\"" + obj1.topic + "\",\"type\":" + obj1.type + ",\"start_time\":\"" + obj1.start_time + "\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"" + obj1.agenda + "\",\"recurrence\":{\"type\":" + obj1.type + ",\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":true,\"mute_upon_entry\":false,\"watermark\":false,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}", RestSharp.ParameterType.RequestBody);
                //request.AddParameter("application/json",obj1, RestSharp.ParameterType.RequestBody);
                ServicePointManager.MaxServicePointIdleTime = 1000;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                       SecurityProtocolType.Tls11 |
                                       SecurityProtocolType.Tls12;
                RestSharp.IRestResponse response = client.Execute(request);
                string Check = obj1.ToJSON();
                if (response.ErrorMessage != "BAD REQUEST")
                {
                    VideoMeetingVM account = JsonConvert.DeserializeObject<VideoMeetingVM>(response.Content);


                    if (account != null)

                    {
                       objmeetingVM = _objvideomeeting.SaveVideoMeeting(account, MeetingId, UserId);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            responses.Content = new StringContent("Video Conference deleted successfully");
            responses.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return responses;
        }

        [Route("VCRecordingDetails")]
        [HttpGet]
        public DataSourceResult VCRecordingDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long MeetingID)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var gridData = _objvideomeeting.GetvideoConfrencingDetails(MeetingID, userID, customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _objvideomeeting.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}