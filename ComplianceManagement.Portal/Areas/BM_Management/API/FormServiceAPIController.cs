﻿using BM_ManegmentServices.Services.Forms;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/FormServiceAPI")]
    public class FormServiceAPIController : ApiController
    {
        IForms_Service objIForms_Service;
        public FormServiceAPIController(IForms_Service objForms_Service)
        {
            objIForms_Service = objForms_Service;
        }

        [Route("GetMeetingFormList")]
        [HttpGet]
        public DataSourceResult GetMeetingFormList([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long meetingId, long meetingAgendaMappingId, long complianceId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("FormName", System.ComponentModel.ListSortDirection.Ascending));
            }

            var gridData = objIForms_Service.GetMeetingFormList(meetingId, meetingAgendaMappingId, complianceId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [HttpGet]
        [Route("GenerateMeetingEForm")]
        public HttpResponseMessage Generate()
        {

            var stream = new MemoryStream();

            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/BM_Management/Documents/Forms");
            string filePath = Path.Combine(path, "Form_DIR-12.pdf");
            using (FileStream fs = File.OpenRead(filePath))
            {
                fs.CopyTo(stream);
            }
                
            // processing the stream.

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray()), 
            };
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = "Form_DIR-12.pdf"
                };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/force-download");

            return result;
        }


        [HttpGet]
        [Route("GenerateForm")]
        public HttpResponseMessage GenerateForm(long meetingFormMappingId, long meetingId)
        {
            var form = objIForms_Service.GenerateForms(meetingFormMappingId, meetingId);

            var result = new HttpResponseMessage(HttpStatusCode.OK);

            if(form.FormData != null)
            {
                result.Content = new ByteArrayContent(form.FormData);

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = form.FormName };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/force-download");
            }
            else
            {
                result = new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return result;
        }



        [HttpGet]
        [Route("GenerateFormforDirector")]
        public HttpResponseMessage GenerateFormforDirector(long DirectorId)
        {
            var form = objIForms_Service.GenerateFormsforDirector(DirectorId);

            var result = new HttpResponseMessage(HttpStatusCode.OK);

            if (form.FormData != null)
            {
                result.Content = new ByteArrayContent(form.FormData);

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = "Form_DIR-6.pdf" };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/force-download");
            }
            else
            {
                result = new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return result;
        }

        [HttpGet]
        [Route("GenerateFormforDirectorDIR3")]
        public HttpResponseMessage GenerateFormforDirectorDIR3(long DirectorId)
        {
            var form = objIForms_Service.GenerateFormforDirectorDIR3(DirectorId);

            var result = new HttpResponseMessage(HttpStatusCode.OK);

            if (form.FormData != null)
            {
                result.Content = new ByteArrayContent(form.FormData);

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = "Form_DIR-3_KYC.pdf" };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/force-download");
            }
            else
            {
                result = new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return result;
        }

    }
}