﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using BM_ManegmentServices.Services.DirectorMeetings;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/DirectorMeetings")]
    public class DirectorMeetingsAPIController : ApiController
    {
        IDirectorMeetings _objMeetings;

        public DirectorMeetingsAPIController(IDirectorMeetings _objMeetings)
        {
            this._objMeetings = _objMeetings;
        }


        [Route("GetMeeting")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetMeeting()
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var result = _objMeetings.GetMeeting(customerID);
            return result;
        }
    }
}
