﻿using BM_ManegmentServices.Services.Masters;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/AdditionalCompliances")]
    public class AdditionalComplianceAPIController : ApiController
    {

        IAdditionalComplianceMaster objadditional;
        
        public AdditionalComplianceAPIController(IAdditionalComplianceMaster obj)
        {
            objadditional = obj;
        }
        [Route("GetadditionalCompliances")]
        [HttpGet]
        public DataSourceResult GetadditionalCompliances([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {

            var gridData = objadditional.GetAdditionalCompliances();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

       
        [Route("DeleteAdditionalCompliances")]
        [HttpDelete]
        public HttpResponseMessage DeleteAdditionalCompliances(long Id)
        {
            int UserId = AuthenticationHelper.UserID;
            HttpResponseMessage response = null;
            if (Id > 0)
            {
                bool result = objadditional.DeleteAdditionalComp(Id, UserId);
                if (result)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return response;
        }
    }
}

