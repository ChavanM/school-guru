﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/ComplianceAPI")]
    public class ComplianceAPIController : ApiController
    {
        ICompliance_Service obj;
        public ComplianceAPIController(ICompliance_Service obj)
        {
            this.obj = obj;
        }

        [Route("GetCompliance")]
        [HttpGet]
        public DataSourceResult GetCompliance([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long? AgendaId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;
            List<VMCompliences> gridData = new List<VMCompliences>();
            gridData = obj.BindCompliancesNew(AgendaId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetActName")]
        [HttpGet]
        public IEnumerable<ActListViewVM> GetActName()
        {
            return obj.GetActName();
        }

        [Route("GetMyDocuments_Director")]
        [HttpGet]
        public List<BM_SP_DirectorComplianceDocuments_Result> GetMyDocuments_Director(int userID, int customerID, int statusID, int roleID)
        {
            return obj.GetMyDocuments_Director(userID, customerID, statusID, roleID);
        }

        [Route("GetMyDocuments")]
        [HttpGet]
        public List<BM_SP_MyComplianceDocuments_Result> GetMyDocuments(int userID, int customerID, int statusID, int roleID, string userRole)
        {
            return obj.GetMyDocuments(userID, customerID, statusID, roleID, userRole);
        }

        [Route("GetComplianceStatusReport_Director")]
        [HttpGet]
        public List<BM_SP_DirectorComplianceStatusReport_Result> GetComplianceStatusReport_Director(int userID, int customerID)
        {
            int? statusID = null;
            return obj.GetComplianceStatusReport_Director(userID, customerID, statusID, 2);
        }

        [Route("GetComplianceStatusReport")]
        [HttpGet]
        public List<BM_SP_ComplianceStatusReport_Result> GetComplianceStatusReport(int userID, int customerID, string userRole)
        {
            int? statusID = null;
            return obj.GetComplianceStatusReport(userID, customerID, statusID, 2, userRole);
        }

        [Route("GetAttendanceReport_Director")]
        [HttpGet]
        public List<BM_SP_DirectorAttendanceReport_Result> GetAttendanceReport_Director(int userID, int customerID)
        {            
            return obj.GetAttendanceReport_Director(userID, customerID);
        }

        [Route("GetAttendanceReport")]
        [HttpGet]
        public List<BM_SP_AttendanceReport_Result> GetAttendanceReport(int userID, int customerID, string userRole)
        {
            return obj.GetAttendanceReport(userID, customerID, userRole);
        }

        [Route("GetAttendanceReportCS")]
        [HttpGet]
        public List<BM_SP_AttendanceReportCS_Result> GetAttendanceReportCS(int userID, int customerID, string userRole)
        {
            return obj.GetAttendanceReportCS(userID, customerID, userRole);
        }

        [Route("GetComplianceRemark")]
        [HttpGet]
        public IHttpActionResult GetComplianceRemark(int UserId,int ComplianceID)
        {
            List<string> objRemark = new List<string>();


            objRemark = obj.getComplianceRemark(UserId, ComplianceID);
            
            return Json(objRemark);
        }
    }
}
