﻿using BM_ManegmentServices;
using BM_ManegmentServices.Services.Dashboard;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Dashboard;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/Dashboard")]
    public class DashboardAPIController : ApiController
    {
        IMeeting_Service objIMeeting_Service;
        IDashboardService objIDashboardService;

        public DashboardAPIController(IMeeting_Service obj, IDashboardService obj1)
        {
            objIMeeting_Service = obj;
            objIDashboardService = obj1;
        }

        [Route("Meeting")]
        [HttpGet]
        public IHttpActionResult Meeting()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            var result = objIMeeting_Service.GetMeetingsForDashboard(userId, customerId, AuthenticationHelper.Role);
            return Json(result);
        }

        [Route("DraftMeeting")]
        [HttpGet]
        public DataSourceResult DraftMeeting([DataSourceRequest] DataSourceRequest request, int customerId, int userId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            var gridData = objIMeeting_Service.GetDraftMeetingsForDirectorApproval(customerId, userId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("Count")]
        [HttpGet]
        public IHttpActionResult Count()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            var result = objIDashboardService.GetDashboardCount(customerId, userId, AuthenticationHelper.Role);

            return Json(result);
        }


        [Route("CountForDirector")]
        [HttpGet]
        public IHttpActionResult CountForDirector()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            var result = objIDashboardService.GetDashboardCountForDirector(customerId, userId);

            return Json(result);
        }


        [Route("ComplianceCount")]
        [HttpGet]
        public IHttpActionResult ComplianceCount()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            var result = objIDashboardService.GetComplianceCount(customerId, userId);

            return Json(result);
        }

        [Route("GetUsersDropdown")]
        [HttpGet]
        public IEnumerable<UserVM> GetUsersDropdown()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return null;
        }


        #region Compliance 
        [Route("MyCompliances_Director")]
        [HttpGet]
        public DataSourceResult MyCompliances_Director([DataSourceRequest] DataSourceRequest request, int roleId, string FY_Year)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            var gridData = objIDashboardService.MyCompliances_Director(customerId, userId, roleId, FY_Year);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("DashboardCompliances")]
        [HttpGet]
        public DataSourceResult DashboardCompliances([DataSourceRequest] DataSourceRequest request, string FY_Year)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            string userRole = AuthenticationHelper.Role;

            var gridData = objIDashboardService.GetDashboardCompliances(userId, customerId, FY_Year, userRole);            
            
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("MyCompliances_Dashboard")]
        [HttpGet]
        public DataSourceResult MyCompliances_Dashboard([DataSourceRequest] DataSourceRequest request, int roleId, string FY_Year)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            string role = AuthenticationHelper.Role;

            var gridData = objIDashboardService.MyCompliances_Dashboard(customerId, userId, roleId, FY_Year, role);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }



        [Route("MyCompliances_Director_JSON")]
        [HttpGet]
        public IHttpActionResult MyCompliances_Director_JSON()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            var result = objIDashboardService.MyCompliances_Director(customerId, userId, 3);

            return Json(result);
        }

        #endregion

        #region Meetings
        [Route("UpcomingMeetings")]
        [HttpGet]
        public DataSourceResult UpcomingMeetings([DataSourceRequest] DataSourceRequest request, int customerId, int userId, string role)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            var gridData = objIMeeting_Service.GetUpcomingMeetings(customerId, userId, role);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
        #endregion
    }
}