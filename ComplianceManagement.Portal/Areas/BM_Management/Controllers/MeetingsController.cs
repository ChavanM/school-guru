﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Fields;
using Telerik.Windows.Documents.Flow.Model.Styles;

using Telerik.Windows.Documents.Fixed.Model;
using Telerik.Windows.Documents.Fixed.Model.InteractiveForms;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;
using System.Reflection;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Compliance;
using System.Windows.Media;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using System.Windows;
using System.Net.Mail;
using BM_ManegmentServices.Services.Setting;
using Telerik.Windows.Documents.Flow.Model.Shapes;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf.Import;
using Syncfusion.DocIO.DLS;
using Syncfusion.Pdf.Parsing;
using Syncfusion.DocIO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MeetingsController : Controller
    {
        IMeeting_Service objIMeeting_Service;
        IMeeting_Compliances objIMeeting_Compliances;
        IComplianceTransaction_Service objIComplianceTransaction_Service;
        IFileData_Service objIFileData_Service;
        IMainMeeting objIMainMeeting;
        IMeetingInvitee objIMeetingInvitee;
        ICompliance_Service objICompliance_Service;
        ISettingService objISettingService;
        ITask objITask;
        IAgendaMinutesReviewService objIAgendaMinutesReviewService;
        IDocumentSetting_Service objIDocumentSetting_Service;
        IDocument_Service objIDocument_Service;

		IFileStorage_Service objIFileStorage_Service;
		IVideoMeeting _objvideomeeting;

        String key = "Authenticate" + AuthenticationHelper.UserID;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();

        #region Controller Constructor
        public MeetingsController(IMeeting_Service objMeeting_Service, IMeeting_Compliances objMeeting_Compliance, IComplianceTransaction_Service objComplianceTransaction_Service, IFileData_Service objFileData_Service, IMainMeeting objMainMeeting, IMeetingInvitee objMeetingInvitee, ICompliance_Service objCompliance_Service, ISettingService objSettingService, ITask objTask, IAgendaMinutesReviewService objAgendaMinutesReviewService, IDocumentSetting_Service objDocumentSetting_Service, IDocument_Service objDocument_Service,IFileStorage_Service objIFileStorage_Service,IVideoMeeting _objvideomeeting)
        {
            objIMeeting_Service = objMeeting_Service;
            objIMeeting_Compliances = objMeeting_Compliance;
            objIComplianceTransaction_Service = objComplianceTransaction_Service;
            objIFileData_Service = objFileData_Service;
            objIMainMeeting = objMainMeeting;
            objIMeetingInvitee = objMeetingInvitee;
            objICompliance_Service = objCompliance_Service;
            objISettingService = objSettingService;
            objITask = objTask;
            objIAgendaMinutesReviewService = objAgendaMinutesReviewService;
            objIDocumentSetting_Service = objDocumentSetting_Service;
            objIDocument_Service = objDocument_Service;
			this.objIFileStorage_Service = objIFileStorage_Service;
			this._objvideomeeting = _objvideomeeting;

        }
        #endregion        

        #region Get Meetings
        public ActionResult Meetings(string stage)
        {
            var model = new MeetingFilter() { Stage = stage };
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                model.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("My Meetings") select x.Addview).FirstOrDefault();
                model.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("My Meetings") select x.Editview).FirstOrDefault();
            }


            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR)
            {
                return View("Meetings_Director");
            }
            else
            {
                return View(model);
            }
        }
        public ActionResult Past()
        {
            return View("PastMeetings");
        }
        public ActionResult Meetings_Read([DataSourceRequest] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;
            return Json(objIMeeting_Service.GetMeetings(customerId, userID, role).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult MeetingsForDirector_Read([DataSourceRequest] DataSourceRequest request)
        {
            int UserId = AuthenticationHelper.UserID;
            return Json(objIMeeting_Service.GetMeetingsForParticipant(UserId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadTodaysMeeting([DataSourceRequest] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            string role = AuthenticationHelper.Role;
            int UserId = AuthenticationHelper.UserID;
            return Json(objIMeeting_Service.GetTodaysMeetingDirectorwise(customerId, UserId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create/Edit
        public ActionResult Meeting_Create()
        {
            bool isVideomeetingCustowise = false;
            isVideomeetingCustowise = objIMeeting_Service.IsVideoConferencingApplicable_CustomerWise(CustomerId);
            return PartialView("Meeting_Add", new Meeting_NewVM() { MeetingID = 0, IsShorter = false, isVideomeetingCusto = isVideomeetingCustowise });
        }
        public ActionResult Meeting_Edit(long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

            var model = objIMeeting_Service.GetMeetingDetails(customerId, MeetingId);

            if (model != null)
            {
                if (model.Type.Trim() == "M" && model.IsSeekAvailability == true)
                {
                    LoadSeekAvailability(model.MeetingID);
                }

                if (model.Type.Trim() == "C")
                {
                    model.CircularMeetingDetails.CircularMeetingId = model.MeetingID;
                    model.CircularMeetingDetails.EntityId = model.Entityt_Id;
                    model.CircularMeetingDetails.CircularNumber = model.MeetingSrNo;
                    model.CircularMeetingDetails.MeetingTypeId = model.MeetingTypeId;
                    model.CircularMeetingDetails.DueDate = model.CircularMeetingDetails.DueDate;
                    model.CircularMeetingDetails.CircularDate = model.CircularMeetingDetails.CircularDate;
                    model.CircularMeetingDetails.CircularDueTime = model.CircularMeetingDetails.CircularDueTime;
                    LoadCircularAgendaData(model.MeetingID);
                }

                if (model.IsVirtualMeeting == true)
                {
                    model.AttendanceNew = objIMainMeeting.checkforQuorum(model.MeetingID, customerId, model.Entityt_Id);
                    model.AttendanceNew.startstopeDateVM = objIMainMeeting.getMeetingstartstopDate(model.MeetingID,customerId);
                    model.AttendanceNew.MeetingId = MeetingId;
                }
                //else
                //{
                //model.agendaTaskReview = objITask.GetTaskDetails(model.MeetingID, model.Entityt_Id, 5);
                //if(model.agendaTaskReview != null)
                //{
                //    if(model.agendaTaskReview.Id == 0)
                //    {
                //        model.agendaTaskReview.TaskTitle = model.TypeName + "-" + model.MeetingTypeName +"[" + (model.MeetingSrNo == null ? "#" : Convert.ToString(model.MeetingSrNo) )+"]-"+ model.FY_CY;
                //    }
                //}
                //}
            }
            model.isVideomeetingCusto = objIMeeting_Service.IsVideoConferencingApplicable_CustomerWise(CustomerId);
            ModelState.Clear();
            return PartialView("Meeting_Edit", model);
        }
        [HttpPost]
        public ActionResult CreateMeeting(Meeting_NewVM obj)
        {
            if (ModelState.IsValid)
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                obj.CustomerId = customerId;
                obj.UserId = UserID;
                obj.MeetingSrNo = null;
                obj = objIMeeting_Service.Create(obj);
                obj.isVideomeetingCusto = objIMeeting_Service.IsVideoConferencingApplicable_CustomerWise(customerId);

                if (obj.Success)
                {
                    return Meeting_Edit(obj.MeetingID);
                }
                else
                {
                    return PartialView("Meeting_Add", obj);
                }
            }
            else
            {
                return PartialView("Meeting_Add", obj);
            }
        }
        [HttpPost]
        public ActionResult EditMeeting(MeetingVM obj)
        {
            if (ModelState.IsValid)
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                obj.UserId = UserID;
                obj.CustomerId = customerId;
                obj = objIMeeting_Service.Update(obj);
                obj.isVideomeetingCusto = objIMeeting_Service.IsVideoConferencingApplicable_CustomerWise(obj.CustomerId);
                ModelState.Clear();
            }

            if (obj.Type.Trim() == "M")
            {
                return PartialView("_EditMeetings", obj);
            }
            else
            {
                return PartialView("_EditCircular", obj);
            }
        }
        [HttpPost]
        public ActionResult RefreshMeeting(MeetingVM obj)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            obj = objIMeeting_Service.GetMeetingDetails(customerId, obj.MeetingID);
            ModelState.Clear();
            return PartialView("_EditMeetings", obj);
        }
        #endregion

        #region Delete Meeting
        public PartialViewResult GetDetailsCanMeetingDelete(long MeetingId)
        {
            var model = objIMeeting_Service.GetDetailsMeetingCanDelete(MeetingId);
            return PartialView("_DeleteMeeting", model);
        }
        [HttpPost]
        public PartialViewResult DeleteMeeting(DeleteMeetingVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            model = objIMeeting_Service.DeleteMeeting(model, userID, CustomerId);
            return PartialView("_DeleteMeeting", model);
        }
        #endregion

        #region Postpond
        [HttpPost]
        public ActionResult Postpond(long meetingId)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIMeeting_Service.Postpond(meetingId, userId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Meeting Participants
        public ActionResult CheckParticipantScheduledMeetings(long meetingId)
        {
            var result = objIMeeting_Service.GetParticipantOtherScheduledMeetings(meetingId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Pending Availability
        public ActionResult Availability()
        {
            return View();
        }
        public ActionResult GetPendingAvailability([DataSourceRequest] DataSourceRequest request)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            return Json(objIMeeting_Service.GetPedingAvailability(UserID).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Seek Availability
        [HttpPost]
        public ActionResult UpdateAvailabilityDueDate(long MeetingId, string date_)
        {
            var result = false;
            if (!String.IsNullOrEmpty(date_.Trim()))
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                DateTime dt = Convert.ToDateTime(date_);
                result = objIMeeting_Service.UpdateAvailabilityDueDate(MeetingId, dt, UserID);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public void LoadSeekAvailability(long ParentID)
        {
            ViewBag.DetailsOfSeekAvailability_Grid = objIMeeting_Service.GetAllSeekAvailability(ParentID);
        }

        public PartialViewResult GetSeekAvailability(long id)
        {
            var model = objIMeeting_Service.GetSeekAvailability(id);
            model.ShowCtrls = true;
            LoadSeekAvailability(model.MeetingID);
            LoadCircularAgendaData(model.MeetingID);
            return PartialView("_EditAvailability", model);
        }

        public PartialViewResult GetSeekAvailabilityNew(long ParentId)
        {
            var model = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = ParentId, ShowCtrls = true };
            LoadSeekAvailability(ParentId);
            return PartialView("_EditAvailability", model);
        }


        public PartialViewResult CreateUpdateSeekAvailabilityNew(long id, long meetingId)
        {
            var model = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = meetingId };
            if (id > 0)
            {
                model = objIMeeting_Service.GetSeekAvailability(id);
            }

            model.ShowCtrls = true;
            //LoadSeekAvailability(model.MeetingID);
            return PartialView("_EditAvailability_AddUpdate", model);
        }

        [HttpPost]
        public PartialViewResult CreateUpdateSeekAvailabilityNew(MeetingAvailabilityVM obj, string command_name)
        {
            #region Create/Update Seek Availability
            if (ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.UserId = UserId;
                if (obj.MeetingAvailabilityId == 0)
                {
                    obj = objIMeeting_Service.Create(obj);
                }
                else
                {
                    obj = objIMeeting_Service.Update(obj);
                }
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation fails.";
            }
            ModelState.Clear();

            return PartialView("_EditAvailability_AddUpdate", obj);
            #endregion
        }

        [HttpPost]
        public PartialViewResult CreateUpdateSeekAvailability(MeetingAvailabilityVM obj, string command_name)
        {
            #region Create/Update Seek Availability
            if (ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.UserId = UserId;
                if (obj.MeetingAvailabilityId == 0)
                {
                    obj = objIMeeting_Service.Create(obj);
                }
                else
                {
                    obj = objIMeeting_Service.Update(obj);
                }
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation fails.";
            }
            ModelState.Clear();

            if (obj.Success)
            {
                obj = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = obj.MeetingID, Message = obj.Message, Success = obj.Success, Error = obj.Error };
            }
            else
            {
                obj.ShowCtrls = true;
            }

            LoadSeekAvailability(obj.MeetingID);
            return PartialView("_EditAvailability", obj);
            #endregion
        }

        public PartialViewResult SeekAvailabilityRefresh(long MeetingId, string ViewType)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

            var meeting = objIMeeting_Service.GetMeeting(customerId, MeetingId);

            var model = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = meeting.MeetingID, ShowCtrls = false, IsSeekAvailabilitySent = meeting.IsSeekAvailabilitySent };
            model.CanMarkSeekAvailability_ = meeting.CanMarkSeekAvailability;
            model.CanReSendSeekAvailability_ = meeting.CanReSendSeekAvailability;

            if (ViewType == "Participant")
            {
                model.Participant_View = true;
                model.Participant_UserId = userID;
            }

            model.Participant_UserId = userID;

            LoadSeekAvailability(MeetingId);
            return PartialView("_EditAvailability", model);
        }

        [HttpPost]
        public PartialViewResult ReSendSeekAvailability(long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

            var meeting = objIMeeting_Service.ForceEditSeekAvailability(customerId, MeetingId);

            var model = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = meeting.MeetingID, ShowCtrls = true, IsSeekAvailabilitySent = meeting.IsSeekAvailabilitySent };
            model.CanMarkSeekAvailability_ = meeting.CanMarkSeekAvailability;
            model.CanReSendSeekAvailability_ = meeting.CanReSendSeekAvailability;
            model.RefreshMailFormat = true;

            model.Participant_UserId = userID;

            LoadSeekAvailability(MeetingId);
            return PartialView("_EditAvailability", model);
        }

        public PartialViewResult MarkSeekAvailability_Participant(long MeetingId)
        {
            MarkSeekAvailabilityVM model = new MarkSeekAvailabilityVM() { MeetingID = MeetingId, ViewType = "Participant" };
            return PartialView("_EditAvailability_Mark", model);
        }

        public PartialViewResult MarkSeekAvailability(long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var meeting = objIMeeting_Service.GetMeeting(customerId, MeetingId);

            MarkSeekAvailabilityVM model = new MarkSeekAvailabilityVM() { MeetingID = MeetingId };
            if (meeting != null)
            {
                model.CanMarkAvailability_ = meeting.CanMarkSeekAvailability;
            }
            return PartialView("_EditAvailability_Mark", model);
        }
        #endregion

        #region Seek Availability Mail
        [HttpPost]
        public ActionResult EditAvailabilityMail(MeetingAvailabilityMailVM obj, string command_name)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            obj.UserId = UserID;
            if (command_name == "send")
            {

                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                string mailFormat = "";
                obj = objIMeeting_Service.SendAvailabilityMail(obj, out mailFormat);
                var accessToDirector = objISettingService.GetAccessToDirector(customerID);
                if (accessToDirector)
                {
                    var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerID, obj.AvailabilityMailID);
                    var mailSubject = " Request Seeking availability for the Attending the " + meeting.MeetingTypeName + " meeting.";
                    var lstParticipants = objIMeeting_Service.GetMeetingParticipants(obj.AvailabilityMailID);
                    if (lstParticipants != null)
                    {
                        var mailSetting = EmailService.GetMailSettingByEntityId(meeting.Entityt_Id, CustomerId);
                        foreach (var item in lstParticipants)
                        {
                            string Mailbody = mailFormat.Replace("{{ Name of the Director }}", item.ParticipantName);
                            EmailManager.SendMail_Secretarial(mailSetting, SenderEmailAddress, new List<String>(new String[] { item.Email }), null, null, mailSubject, Mailbody, null);
                        }
                    }
                }
            }
            else
            {
                obj = objIMeeting_Service.UpdateAvailabilityMail(obj);
            }
            return PartialView("_EditAvailabilityFormat", obj);
        }

        [HttpPost]
        public ActionResult RefreshAvailabilityMail(long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var meeting = objIMeeting_Service.GetMeeting(customerId, MeetingId);
            var model = new MeetingAvailabilityMailVM();
            if (meeting != null)
            {
                if (meeting.AvailabilityMailFormat != null)
                {
                    model = meeting.AvailabilityMailFormat;
                }
            }
            return PartialView("_EditAvailabilityFormat", model);
        }

        [HttpPost]
        public ActionResult AvailabilityMailPreview(MeetingAvailabilityMailVM obj, string command_name)
        {

            obj = objIMeeting_Service.PreviewAvailabilityMail(obj);

            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Seek Availability Response

        [HttpGet]
        public ActionResult GetAvailabilityResponse([DataSourceRequest] DataSourceRequest request, long MeetingID, string ViewType)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int? userId = null;
            if (ViewType == "Participant")
            {
                userId = Convert.ToInt32(AuthenticationHelper.UserID);
            }
            var gridData = objIMeeting_Service.GetAvailabilityResponse(MeetingID, customerId, userId);

            return Json(gridData.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAvailabilityResponse([DataSourceRequest] DataSourceRequest request,
           [Bind(Prefix = "models")] IEnumerable<AvailabilityResponse_ResultVM> availabilityResponse)
        {
            if (availabilityResponse != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var item in availabilityResponse)
                {
                    var result = objIMeeting_Service.UpdateAvailabilityResponse(item, UserId);
                    item.AvailabilityResponseID = result.AvailabilityResponseID;
                }
            }
            return Json(availabilityResponse.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult GetAvailabilityResponseChartData(long MeetingID, long MeetingAvailabilityId)
        {
            var model = objIMeeting_Service.GetAvailabilityResponseChartData(MeetingID, MeetingAvailabilityId);
            return Json(model);
        }
        public PartialViewResult SelectSeekAvailability(long meetingID, long meetingAvailabilityId)
        {
            var model = objIMeeting_Service.GetSeekAvailability(meetingAvailabilityId);
            return PartialView("Availability_Click", model);
        }
        [HttpPost]
        public PartialViewResult SetMeetingDateFromAvailability(MeetingAvailabilityVM obj, string command_name)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.SelectSeekAvailability(obj, UserID);
            ModelState.Clear();
            return PartialView("Availability_Click", model);
        }
        #endregion

        #region Meeting Agenda
        //Returns List of Agenda Item 
        public PartialViewResult AgendaItem(long MeetingId)
        {
            return PartialView("_EditAgendaItems", MeetingId);
        }

        //Returns Agenda Items for select
        public PartialViewResult AgendaItemSelect(long MeetingId, string Part, string MeetingCircular, bool? showPendingAgenda)
        {
            if (String.IsNullOrEmpty(Part))
            {
                Part = "B";
            }

            if (Part != "A" && Part != "B" && Part != "C")
            {
                Part = "B";
            }

            if (showPendingAgenda == null)
            {
                showPendingAgenda = false;
            }
            dynamic expando = new ExpandoObject();
            var agendaItemSelectModel = expando as IDictionary<string, object>;

            agendaItemSelectModel.Add("MeetingId", MeetingId);
            agendaItemSelectModel.Add("Part", Part);
            agendaItemSelectModel.Add("MeetingCircular", MeetingCircular);

            agendaItemSelectModel.Add("showPendingAgenda", showPendingAgenda);

            return PartialView("_EditAgendaItemsSelect", agendaItemSelectModel);
        }

        public PartialViewResult MultistageAgendaItemHistory(long StartMeetingId, long StartAgendaId, int EntityId)
        {
            dynamic expando = new ExpandoObject();
            var agendaItemSelectModel = expando as IDictionary<string, object>;

            agendaItemSelectModel.Add("StartMeetingId", StartMeetingId);
            agendaItemSelectModel.Add("StartAgendaId", StartAgendaId);
            agendaItemSelectModel.Add("EntityIdForMultistage", EntityId);
            agendaItemSelectModel.Add("showPendingAgenda", true);

            return PartialView("_MultistageAgendaItemsHistory", agendaItemSelectModel);
        }
        //Add Agenda Items
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult save_checkedAgendaItems([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<AgendaItemSelect> agendaItems)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = new List<AgendaComplianceMappingPendingVM>();
            bool flag = false;
            bool addFlag = true;
            if (agendaItems != null && ModelState.IsValid)
            {
                foreach (var agendaItem in agendaItems)
                {
                    if (agendaItem.IsCheked)
                    {
                        var agenda = objIMeeting_Service.CheckComplianceAssignedOrNot(agendaItem.BM_AgendaMasterId, agendaItem.Meeting_Id, CustomerID, null);
                        if (agenda != null)
                        {
                            if (agenda.Count > 0)
                            {
                                addFlag = false;
                                result.Add(agenda.FirstOrDefault());
                            }
                        }
                    }
                }

                if(addFlag)
                {
                    foreach (var agendaItem in agendaItems)
                    {
                        if (agendaItem.IsCheked)
                        {
                            objIMeeting_Service.AddAgendaItem(agendaItem, UserID, CustomerID);
                            flag = true;
                        }
                    }
                }

                if (flag)
                {
                    var agenda = agendaItems.First();

                    objIMeeting_Service.SetDefaultItemNumbers(agenda.Meeting_Id);

                    objIMeeting_Compliances.GenerateScheduleOn(agenda.Meeting_Id, SecretarialConst.ComplianceMappingType.AGENDA, CustomerID, UserID);

                    #region Compliance Activation before Meeting
                    objIComplianceTransaction_Service.CreateTransactionOnNoticeSend(agenda.Meeting_Id, UserID);
                    #endregion

                    objIMeeting_Service.ReOpenVirtualMeeting(agenda.Meeting_Id, UserID);
                }
            }

            return Json(result.ToDataSourceResult(request, ModelState));
        }

        public ActionResult MeetingAgenda_Read([DataSourceRequest] DataSourceRequest request, long MeetingId, string Part, string Type)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("SrNo", System.ComponentModel.ListSortDirection.Ascending));
            }

            return Json(objIMeeting_Service.GetMeetingAgenda(MeetingId, Part, Type).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        //Save Agenda Item Order
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MeetingAgenda_SaveOrder([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<MeetingAgendaMappingVM> agendaItems)
        {
            if (agendaItems != null && ModelState.IsValid)
            {
                int SrNo = 0;
                foreach (var agendaItem in agendaItems)
                {
                    SrNo++;
                    objIMeeting_Service.SaveOrderAgendaItem(agendaItem, SrNo);
                    agendaItem.SrNo = SrNo;
                }

                if (SrNo > 0)
                {
                    var meetingId = agendaItems.FirstOrDefault().Meeting_Id;
                    objIMeeting_Service.SetDefaultItemNumbers(meetingId);
                }
            }
            return Json(agendaItems.ToDataSourceResult(request, ModelState));
        }

        //Agenda Item Format Methods
        public PartialViewResult AgendaItemFormat_Read(long MeetingAgendaMappingID)
        {
            var model = objIMeeting_Service.GetAgendaItem(MeetingAgendaMappingID);
            return PartialView("_EditAgendaItemFormat", model);
        }

        public PartialViewResult AgendaItemFormat_Save(MeetingAgendaMappingVM obj)
        {
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                obj = objIMeeting_Service.UpdateAgendaItem(obj, UserID);
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation Fails.";
            }
            ModelState.Clear();
            return PartialView("_EditAgendaItemFormat", obj);
        }
        //End
        public PartialViewResult AgendaPreview(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.PreviewAgenda(MeetingId, CustomerId);
            return PartialView("_Viewer_Agenda", model);
        }
        public ActionResult DownloadAgendaDocumnet(long MeetingId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                var fileName = GenerateAgendaDocumnet(MeetingId);

                #region Agenda Generation Telerik
                //using (MemoryStream stream = new MemoryStream())
                //{
                //    DocxFormatProvider provider = new DocxFormatProvider();
                //    RadFlowDocument document = CreateRadFixedDocumentNew(MeetingId, customerId);
                //    provider.Export(document, stream);
                //    return File(stream.ToArray(), "application/force-download", fileName);
                //}
                #endregion

                #region Agenda Generation Syncfusion
                //var bytes = GenerateAgendaDocumentSyncfusion(MeetingId, customerId);
                var bytes = objIDocument_Service.GenerateAgendaDocument(MeetingId, 0, userID, customerId, false);
                return File(bytes, "application/force-download", fileName);
                #endregion
            }
            catch (Exception e)
            {
                return PartialView("_FileNotFound");
            }
        }

        public FileDataVM GetNew_NoticeFileData(long meetingId, string noticeMail)
        {
            var result = new FileDataVM();
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, meetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + meetingId;
                //string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + meetingId;
                string fileName = "Notice-" + meeting.MeetingTypeName + " " + meeting.TypeName + " [ " + meeting.MeetingSrNo + " ] " + meeting.FY_CY + ".docx";

                DocxFormatProvider provider = new DocxFormatProvider();
                RadFlowDocument document = GetNoticeDocument(noticeMail);
                Byte[] bytes = provider.Export(document);

                result.FileName = fileName;
                result.FilePath = path;
                result.FileData = bytes;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public RadFlowDocument GetNoticeDocument(string noticeMail)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            try
            {
                #region Document header
                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                editor.MoveToParagraphStart(paragraph);
                #endregion

                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                var insertOptions = new InsertDocumentOptions
                {
                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                };

                RadFlowDocument htmlDocument = htmlProvider.Import(noticeMail);
                editor.InsertDocument(htmlDocument);

                foreach (var item in doc.Sections)
                {
                    double spacing = 1;
                    foreach (Paragraph para in doc.Sections[0].EnumerateChildrenOfType<Paragraph>())
                    {
                        para.Spacing.SpacingAfter = spacing;
                        para.Spacing.SpacingBefore = spacing;

                        para.Spacing.AutomaticSpacingAfter = false;
                        para.Spacing.AutomaticSpacingBefore = false;

                        para.Spacing.SpacingAfter = spacing;
                        para.Spacing.SpacingBefore = spacing;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }

        public FileDataVM NewAgendaFileData(long meetingId)
        {
            var result = new FileDataVM();
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, meetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + meetingId;

                //.Docx
                //string fileName = "Agenda-" + meeting.MeetingTypeName + " " + meeting.TypeName + " [ " + meeting.MeetingSrNo + " ] " + meeting.FY_CY + ".docx";
                //result.FileName = fileName;
                //result.FilePath = path;
                //result.FileData = objIDocument_Service.GenerateAgendaDocument(meetingId, 0, userId, customerId, false);
                //End

                //.PDF
                string fileName = "Agenda-" + meeting.MeetingTypeName + " " + meeting.TypeName + " [ " + meeting.MeetingSrNo + " ] " + meeting.FY_CY + ".pdf";
                result.FileName = fileName;
                result.FilePath = path;
                var tempFile = objIDocument_Service.GenerateAgendaDocument1(meetingId, 0, userId, customerId, false);
                if(!string.IsNullOrEmpty(tempFile))
                {
                    if (System.IO.File.Exists(Server.MapPath(tempFile)))
                    {
                        result.FileData = objIFileData_Service.ReadDocFiles(Server.MapPath(tempFile));
                    }
                }
                //End
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public string GenerateAgendaDocumnet(long MeetingId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, MeetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + MeetingId;
                //string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + MeetingId;
                string str = meeting.TypeName + "_" + meeting.MeetingTypeName + "_[" + meeting.MeetingSrNo + "]_" + meeting.FY_CY + ".docx";
                if (meeting.MeetingTypeName == "Meeting")
                {
                    if (meeting.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || meeting.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                    {
                        str = "Notice_" + str;
                    }
                    else
                    {
                        str = "Agenda_" + str;
                    }
                }
                //string fileName = Path.Combine(Server.MapPath(path), str);

                //bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                //if (!exists)
                //    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                //using (Stream output = System.IO.File.OpenWrite(fileName))
                //{
                //    DocxFormatProvider provider = new DocxFormatProvider();
                //    RadFlowDocument document = CreateRadFixedDocumentNew(MeetingId, customerId);

                //    provider.Export(document, output);
                //}

                // return fileName;
                return str;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public RadFlowDocument CreateRadFixedDocumentNew(long meetingId, int customerId)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            Telerik.Windows.Documents.Flow.Model.Fields.FieldInfo tocField = null;
            try
            {
                var agenda = objIMeeting_Service.PreviewAgenda(meetingId, customerId);
                var agendaItems = agenda.lstAgendaItems;

                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 

                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                #region meeting details
                var meetingSrNo = "";
                var meetingTypeName = "";
                if (agenda.MeetingSrNo.HasValue)
                {
                    var srNo = Convert.ToString(agenda.MeetingSrNo);
                    switch (srNo)
                    {
                        case "1":
                            meetingSrNo = srNo + "st";
                            break;
                        case "2":
                            meetingSrNo = srNo + "nd";
                            break;
                        case "3":
                            meetingSrNo = srNo + "rd";
                            break;
                        default:
                            meetingSrNo = srNo + "th";
                            break;
                    }
                }

                if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                {
                    meetingTypeName = "Annual General Meeting";
                }
                else if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    meetingTypeName = "Extra Ordinary General Meeting";
                }

                string mDate = string.Empty;
                string mDay = string.Empty;
                string mTime = string.Empty;
                string mVenue = string.Empty;
                string cDate = string.Empty;

                if (agenda.MeetingDate != null)
                {
                    var date = Convert.ToDateTime(agenda.MeetingDate);
                    mDate = date.ToString("MMMM dd, yyyy");
                    mDay = date.ToString("dddd");
                }
                if (agenda.CircularDate != null)
                {
                    cDate = Convert.ToDateTime(agenda.CircularDate).ToString("MMMM dd, yyyy");
                }
                if (!string.IsNullOrEmpty(agenda.MeetingTime))
                {
                    mTime = agenda.MeetingTime;
                }
                if (!string.IsNullOrEmpty(agenda.MeetingVenue))
                {
                    mVenue = agenda.MeetingVenue;
                }
                #endregion

                if (agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.AGM && agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.EGM)
                {
                    #region Document header

                    var section = editor.InsertSection();
                    var paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Center;
                    editor.MoveToParagraphStart(paragraph);

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                    var run1 = editor.InsertLine(agenda.EntityName);
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 24;

                    var run2 = editor.InsertLine(agenda.EntityAddressLine1);
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 14;

                    var run21 = editor.InsertLine(agenda.EntityAddressLine2);
                    run21.FontWeight = System.Windows.FontWeights.Bold;
                    run21.FontSize = 14;

                    var runCIN = editor.InsertLine(agenda.EntityCIN_LLPIN);
                    runCIN.FontWeight = System.Windows.FontWeights.Bold;
                    runCIN.FontSize = 12;

                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Center;

                    editor.InsertLine("");
                    var r = editor.InsertLine("AGENDA FOR THE " + meetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "") + " " + agenda.MeetingTypeName.ToUpper() + (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING ? " MEETING" : "CIRCULAR"));
                    r.FontWeight = System.Windows.FontWeights.Bold;
                    r.FontSize = 18;

                    if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING)
                    {
                        #region Table
                        var table = editor.InsertTable(4, 3);

                        table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                        table.Alignment = Alignment.Center;

                        table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 18);
                        table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 2);
                        table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 80);
                        Telerik.Windows.Documents.Flow.Model.Styles.Border tableBorderAll = new Telerik.Windows.Documents.Flow.Model.Styles.Border(0.5, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                        table.Borders = new TableBorders(tableBorderAll);

                        var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("DATE");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[0].Cells[2].Blocks.AddParagraph().Inlines.AddRun(mDate);
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        //Day
                        cellRun = table.Rows[1].Cells[0].Blocks.AddParagraph().Inlines.AddRun("DAY");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[1].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[1].Cells[2].Blocks.AddParagraph().Inlines.AddRun(mDay);
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        //End

                        //Time
                        cellRun = table.Rows[2].Cells[0].Blocks.AddParagraph().Inlines.AddRun("TIME");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[2].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[2].Cells[2].Blocks.AddParagraph().Inlines.AddRun(mTime);
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        //End

                        //Venue
                        cellRun = table.Rows[3].Cells[0].Blocks.AddParagraph().Inlines.AddRun("VENUE");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[3].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[3].Cells[2].Blocks.AddParagraph().Inlines.AddRun(mVenue);
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        //End
                        #endregion

                        editor.InsertLine("");
                        r = editor.InsertLine("Items of Agenda");
                        r.FontWeight = System.Windows.FontWeights.Bold;
                        r.FontSize = 14;

                        editor.ParagraphFormatting.SpacingAfter.LocalValue = 6;
                        editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;

                        //Telerik.Windows.Documents.Flow.Model.Fields.FieldInfo tocField = editor.InsertField("TOC \\o" + " \"1-1\" " + "\\h \\z \\u", "");
                        //tocField.IsDirty = true;
                        tocField = editor.InsertField("TOC \\o" + " \"1-1\" " + "\\h \\z \\u", "");
                        var bb = tocField.IsDirty;
                    }
                    else
                    {
                        #region Table
                        var table = editor.InsertTable(1, 4);

                        table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                        table.Alignment = Alignment.Center;

                        table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 18);
                        table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 2);
                        table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 80);
                        Telerik.Windows.Documents.Flow.Model.Styles.Border tableBorderAll = new Telerik.Windows.Documents.Flow.Model.Styles.Border(0.5, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                        table.Borders = new TableBorders(tableBorderAll);

                        var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("CIRCULAR DATE");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun(cDate);
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        //

                        cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("DUE DATE");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun(mDate);
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;

                        //End
                        #endregion
                    }
                    #endregion

                    Telerik.Windows.Documents.Flow.Model.Styles.Border b = new Telerik.Windows.Documents.Flow.Model.Styles.Border(1, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                    var h = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(1));
                    Telerik.Windows.Documents.Flow.Model.Styles.Border b_other = new Telerik.Windows.Documents.Flow.Model.Styles.Border(0, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.None, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(0, 0, 0)), false, false, 0);
                    h.ParagraphProperties.Borders.LocalValue = new ParagraphBorders(b_other, b_other, b_other, b);

                    h.CharacterProperties.FontSize.LocalValue = 16;
                    h.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                    h.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;

                    HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                    var insertOptions = new InsertDocumentOptions
                    {
                        ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                    };

                    if (agendaItems != null)
                    {
                        var SrNo = 0;
                        foreach (var item in agendaItems)
                        {
                            SrNo++;
                            string AgendaItem = "<h1>" + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper()) + "</h1>";
                            section = editor.InsertSection();
                            paragraph = section.Blocks.AddParagraph();
                            paragraph.TextAlignment = Alignment.Right;

                            editor.MoveToParagraphStart(paragraph);

                            var r1 = editor.InsertLine("Item No. " + SrNo);
                            r1.FontWeight = System.Windows.FontWeights.Bold;
                            r1.FontStyle = System.Windows.FontStyles.Italic;
                            r1.Underline.Pattern = UnderlinePattern.Single;

                            r1.FontSize = 24;
                            RadFlowDocument htmlDocument = htmlProvider.Import(AgendaItem);
                            editor.InsertDocument(htmlDocument, insertOptions);

                            AgendaItem = "<p>" + item.AgendaFormat + "</p>";
                            AgendaItem = item.AgendaFormat == null ? " " : item.AgendaFormat;
                            htmlDocument = htmlProvider.Import(AgendaItem);
                            editor.InsertDocument(htmlDocument, insertOptions);

                            #region Insert PageNo footer
                            section.Footers.Add();
                            Footer defaultFooter = section.Footers.Default;
                            Paragraph para = defaultFooter.Blocks.AddParagraph();
                            para.TextAlignment = Alignment.Center;
                            editor.MoveToParagraphStart(para);

                            var p1 = editor.InsertField("PAGE", "1");
                            p1.UpdateField();
                            #endregion

                            #region Insert Agenda Documents
                            /*
                            var docs = objIMeeting_Service.GetAgendaDocument(item.MeetingAgendaMappingID, customerId);
                            var isAnnuxureSectionAdded = false;
                            if (docs != null && isAnnuxureSectionAdded)
                            {
                                foreach (var document in docs)
                                {
                                    var actualFileName = objIFileData_Service.GetSecretarialFile(document.ID, userId, customerId);
                                    if(!string.IsNullOrEmpty(actualFileName))
                                    {
                                        if(isAnnuxureSectionAdded == false)
                                        {
                                            isAnnuxureSectionAdded = true;
                                            section = editor.InsertSection();
                                            paragraph = section.Blocks.AddParagraph();
                                            editor.MoveToParagraphStart(paragraph);
                                            editor.InsertLine("Item No "+ SrNo +" - Annexure ");
                                        }
                                        switch (Path.GetExtension(document.FileName))
                                        {
                                            case ".docx":
                                            case ".doc":
                                                Telerik.Windows.Documents.Flow.FormatProviders.Docx.DocxFormatProvider provider = new Telerik.Windows.Documents.Flow.FormatProviders.Docx.DocxFormatProvider();
                                                using (Stream input = System.IO.File.OpenRead(Server.MapPath(@"~\"+ actualFileName)))
                                                {
                                                    RadFlowDocument agendadoc = provider.Import(input);

                                                    var insertOptionsTest = new InsertDocumentOptions();
                                                    insertOptionsTest.ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle;
                                                    insertOptionsTest.InsertLastParagraphMarker = true;

                                                    //editor.InsertDocument(agendadoc, insertOptions);
                                                    editor.InsertDocument(agendadoc, insertOptionsTest);
                                                }
                                                break;
                                            case ".png":
                                            case ".jpeg":
                                                FloatingImage floatingImage = new FloatingImage(doc);

                                                var imageExtension = Path.GetExtension(actualFileName);
                                                // Assign a source to the image 
                                                floatingImage.Image.ImageSource = new Telerik.Windows.Documents.Media.ImageSource(System.IO.File.ReadAllBytes(Server.MapPath(@"~\" + actualFileName)), imageExtension);

                                                section = editor.InsertSection();
                                                paragraph = section.Blocks.AddParagraph();
                                                editor.MoveToParagraphStart(paragraph);

                                                // Insert the image at the desired position in a Paragraph 
                                                //paragraph.Inlines.Insert(0, floatingImage);
                                                paragraph.Inlines.Add(floatingImage);

                                                //using (Stream input = System.IO.File.OpenRead(Server.MapPath(@"~\" + actualFileName)))
                                                //{

                                                //    editor.InsertFloatingImage(input, imageExtension);
                                                //}


                                                break;
                                            case ".pdf":
                                                Telerik.Windows.Documents.Flow.FormatProviders.Pdf.PdfFormatProvider pdfFormatProvider = new Telerik.Windows.Documents.Flow.FormatProviders.Pdf.PdfFormatProvider();
                                                var lst = pdfFormatProvider.SupportedExtensions.ToList();
                                                PdfImportSettings settings = new PdfImportSettings();
                                                
                                                using (Stream input = System.IO.File.OpenRead(Server.MapPath(@"~\" + actualFileName)))
                                                {
                                                }
                                                //Telerik.Windows.Documents.Fixed.FormatProviders.Pdf.PdfFormatProvider pdfFormatProvider = new Telerik.Windows.Documents.Fixed.FormatProviders.Pdf.PdfFormatProvider();
                                                //using (Stream input = System.IO.File.OpenRead(Server.MapPath(@"~\" + actualFileName)))
                                                //{
                                                //    RadFixedDocument agendadoc = pdfFormatProvider.Import(input);


                                                //}

                                                #region
                                                //Telerik.Windows.Documents.Common.FormatProviders.IFormatProvider<RadFlowDocument> fileFormatProvider = new PdfFormatProvider();
                                                //RadFlowDocument documentToConvert = new RadFlowDocument();

                                                //// Read DOCX
                                                //using (FileStream input = new FileStream(Server.MapPath(@"~\" + actualFileName), FileMode.Open))
                                                //{
                                                //    //documentToConvert = fileFormatProvider.Import(input);
                                                //}

                                                //PdfFormatProvider provider1 = new PdfFormatProvider();
                                                //using (Stream stream1 = System.IO.File.OpenRead(Server.MapPath(@"~\" + actualFileName)))
                                                //{
                                                //    //RadFixedDocument document1 = provider1.Import(stream1);

                                                //    // Do your work with the document inside the using statement. 
                                                //}

                                                //// Write PDF
                                                //fileFormatProvider = new DocxFormatProvider(); // change format provider to PDF

                                                //using (Stream output = new FileStream(Server.MapPath(@"~\TempFiles\241220\Test.docx"), FileMode.OpenOrCreate))
                                                //{
                                                //    fileFormatProvider.Export(documentToConvert, output);
                                                //}

                                                //Telerik.Windows.Documents.Flow.FormatProviders.Docx.DocxFormatProvider provider1 = new Telerik.Windows.Documents.Flow.FormatProviders.Docx.DocxFormatProvider();
                                                //using (Stream input = System.IO.File.OpenRead(Server.MapPath(@"~\TempFiles\241220\Test.docx")))
                                                //{
                                                //    RadFlowDocument agendadoc = provider1.Import(input);
                                                //    editor.InsertDocument(agendadoc, insertOptions);
                                                //}
                                                #endregion

                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            } */
                            #endregion
                        }
                    }
                }
                else
                {
                    #region Document header
                    var section = editor.InsertSection();
                    var paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Center;
                    editor.MoveToParagraphStart(paragraph);

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                    var run1 = editor.InsertLine(agenda.EntityName);
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 24;

                    var run2 = editor.InsertLine(agenda.EntityAddressLine1);
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 14;

                    var run21 = editor.InsertLine(agenda.EntityAddressLine2);
                    run21.FontWeight = System.Windows.FontWeights.Bold;
                    run21.FontSize = 14;

                    var runCIN = editor.InsertLine(agenda.EntityCIN_LLPIN);
                    run21.FontWeight = System.Windows.FontWeights.Bold;
                    run21.FontSize = 12;

                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Center;

                    editor.InsertLine("");
                    run1 = editor.InsertLine("NOTICE OF " + meetingSrNo + " " + agenda.MeetingTypeName.ToUpper() + "");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 24;

                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Justified;

                    editor.InsertLine("");
                    var r = editor.InsertLine("Notice is hereby given that the " + meetingSrNo + " " + meetingTypeName + " of the members of " + agenda.EntityName + " will be held on " + mDay + " " + mDate + " at " + mTime + " at " + mVenue + " to transact following business:");
                    r.FontWeight = System.Windows.FontWeights.Bold;
                    r.FontSize = 18;

                    editor.InsertLine("");

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 6;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;

                    #endregion

                    #region Agenda
                    Telerik.Windows.Documents.Flow.Model.Styles.Border b = new Telerik.Windows.Documents.Flow.Model.Styles.Border(1, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                    var h = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(1));
                    Telerik.Windows.Documents.Flow.Model.Styles.Border b_other = new Telerik.Windows.Documents.Flow.Model.Styles.Border(0, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.None, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(0, 0, 0)), false, false, 0);
                    //h.ParagraphProperties.Borders.LocalValue = new ParagraphBorders(b_other, b_other, b_other, b);

                    h.CharacterProperties.FontSize.LocalValue = 16;
                    h.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                    h.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;

                    HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                    var insertOptions = new InsertDocumentOptions
                    {
                        ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                    };

                    if (agendaItems != null)
                    {
                        var SrNo = 0;
                        string business_ = "", businessLast_ = "";
                        foreach (var item in agendaItems)
                        {
                            SrNo++;
                            businessLast_ = item.IsOrdinaryBusiness ? "Ordinary Business" : "Special Business";

                            if (business_ != businessLast_)
                            {
                                business_ = businessLast_;

                                string businessType_ = "<h1>" + business_ + "</h1>";
                                RadFlowDocument htmlDocumentBusiness = htmlProvider.Import(businessType_);
                                editor.InsertDocument(htmlDocumentBusiness, insertOptions);
                            }

                            string AgendaItemGM = "<h1>" + SrNo + ". " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper()) + "</h1>";
                            RadFlowDocument htmlDocumentGM = htmlProvider.Import(AgendaItemGM);
                            editor.InsertDocument(htmlDocumentGM, insertOptions);

                            if (!item.IsOrdinaryBusiness)
                            {
                                AgendaItemGM = item.AgendaFormat == null ? " " : item.AgendaFormat;
                                htmlDocumentGM = htmlProvider.Import(AgendaItemGM);
                                editor.InsertDocument(htmlDocumentGM, insertOptions);
                            }
                        }
                    }
                    #endregion

                    #region Signing Authority
                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Left;

                    run2 = editor.InsertLine("");
                    run2 = editor.InsertLine("By Order of the Board of Director");
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 16;

                    run2 = editor.InsertLine("For " + agenda.EntityName);
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 16;

                    run2 = editor.InsertLine("");
                    run2 = editor.InsertLine("");
                    run2 = editor.InsertLine("");

                    if (agenda.MeetingSigningAuthorityForAGM != null)
                    {
                        run2 = editor.InsertLine(agenda.MeetingSigningAuthorityForAGM.AuthorityName);
                        run2.FontWeight = System.Windows.FontWeights.Bold;
                        run2.FontSize = 14;
                        run2 = editor.InsertLine("(" + agenda.MeetingSigningAuthorityForAGM.Designation + ")");
                        run2.FontWeight = System.Windows.FontWeights.Bold;
                        run2.FontSize = 14;
                        if (agenda.MeetingSigningAuthorityForAGM.IsCS == true)
                        {
                            run2 = editor.InsertLine("Membership No. " + agenda.MeetingSigningAuthorityForAGM.MembershipNo);
                            run2.FontWeight = System.Windows.FontWeights.Bold;
                            run2.FontSize = 14;
                        }
                        else
                        {
                            run2 = editor.InsertLine("DIN : " + agenda.MeetingSigningAuthorityForAGM.DIN_PAN);
                            run2.FontWeight = System.Windows.FontWeights.Bold;
                            run2.FontSize = 14;
                        }
                    }

                    run2 = editor.InsertLine("");
                    run2 = editor.InsertLine("");

                    run2 = editor.InsertLine("Date");
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 14;

                    run2 = editor.InsertLine("Place : " + agenda.EntityCity);
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 14;
                    #endregion

                    #region Notes
                    if (!string.IsNullOrEmpty(agenda.GM_Notes))
                    {
                        RadFlowDocument htmlDocument = htmlProvider.Import("<h1>Notes for Members' Attention</h1>");
                        editor.InsertDocument(htmlDocument, insertOptions);

                        var htmlDocumentNotes = htmlProvider.Import(agenda.GM_Notes);
                        editor.InsertDocument(htmlDocumentNotes, insertOptions);
                    }
                    #endregion

                    #region Explanatory statements
                    var hasExplanatorystatements = false;
                    if (agendaItems != null)
                    {
                        foreach (var item in agendaItems)
                        {
                            if (!string.IsNullOrEmpty(item.ExplanatoryStatement))
                            {
                                hasExplanatorystatements = true;
                                break;
                            }
                        }

                        if (hasExplanatorystatements)
                        {
                            var AgendaItemExState = "<h1>Explanatory Statement pursuant to Section 102 (1) of the companies act, 2013</h1>" +
                                            "The following statement sets out all material facts relating to special business mentioned in the notice:";

                            var htmlDocumentExState = htmlProvider.Import(AgendaItemExState);
                            editor.InsertDocument(htmlDocumentExState, insertOptions);

                            var SrNo = 0;
                            string resolutionType = "";
                            foreach (var item in agendaItems)
                            {
                                SrNo++;
                                if (!string.IsNullOrEmpty(item.ExplanatoryStatement))
                                {
                                    resolutionType = item.IsSpecialResolution ? "Special resolution" : "Ordinary resolution";
                                    string AgendaItem = "<h1>" + SrNo + ". " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper()) + "</h1>";
                                    RadFlowDocument htmlDocument = htmlProvider.Import(AgendaItem);
                                    editor.InsertDocument(htmlDocument, insertOptions);

                                    AgendaItemExState = item.ExplanatoryStatement;
                                    htmlDocumentExState = htmlProvider.Import(AgendaItemExState);
                                    editor.InsertDocument(htmlDocumentExState, insertOptions);

                                    AgendaItemExState = "<p></p>" +
                                                        "None of the directors of the Company, Key Managerial Personnel and their relatives is in any way concerned or interested financially or otherwise in the resolution." +
                                                        "<p></p>" +
                                                        "The Board of Directors accordingly recommends the <strong>" + resolutionType + "</strong> set out at ITEM NO. <strong>" + SrNo + "</strong> of the notice for the approval of the members.";
                                    htmlDocumentExState = htmlProvider.Import(AgendaItemExState);
                                    editor.InsertDocument(htmlDocumentExState, insertOptions);
                                }
                            }
                        }
                    }
                    #endregion

                    #region Signing Authority
                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Left;

                    run2 = editor.InsertLine("");

                    run2 = editor.InsertLine("By Order of the Board of Director");
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 16;

                    run2 = editor.InsertLine("For " + agenda.EntityName);
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 16;

                    run2 = editor.InsertLine("");
                    run2 = editor.InsertLine("");
                    run2 = editor.InsertLine("");

                    if (agenda.MeetingSigningAuthorityForAGM != null)
                    {
                        run2 = editor.InsertLine(agenda.MeetingSigningAuthorityForAGM.AuthorityName);
                        run2.FontWeight = System.Windows.FontWeights.Bold;
                        run2.FontSize = 14;
                        run2 = editor.InsertLine("(" + agenda.MeetingSigningAuthorityForAGM.Designation + ")");
                        run2.FontWeight = System.Windows.FontWeights.Bold;
                        run2.FontSize = 14;
                        if (agenda.MeetingSigningAuthorityForAGM.IsCS == true)
                        {
                            run2 = editor.InsertLine("Membership No. " + agenda.MeetingSigningAuthorityForAGM.MembershipNo);
                            run2.FontWeight = System.Windows.FontWeights.Bold;
                            run2.FontSize = 14;
                        }
                        else
                        {
                            run2 = editor.InsertLine("DIN : " + agenda.MeetingSigningAuthorityForAGM.DIN_PAN);
                            run2.FontWeight = System.Windows.FontWeights.Bold;
                            run2.FontSize = 14;
                        }
                    }

                    run2 = editor.InsertLine("");
                    run2 = editor.InsertLine("");

                    run2 = editor.InsertLine("Date");
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 14;

                    run2 = editor.InsertLine("Place : " + agenda.EntityCity);
                    run2.FontWeight = System.Windows.FontWeights.Bold;
                    run2.FontSize = 14;
                    #endregion
                }
                //Footer
                //Footer defaultFooter = section.Footers.Default;
                //Paragraph para = defaultFooter.Blocks.AddParagraph();

                //editor.MoveToParagraphStart(para);

                //editor.InsertText("Page ");
                //editor.InsertField("PAGE", "1");
                //editor.InsertText(" of ");
                //editor.InsertField("NUMPAGES", "1");
                //End Footer
                //tocField.UpdateField();
                //doc.UpdateFields();

                //var aaa = tocField.IsDirty;
                //tocField.UpdateField();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }

        protected byte[] GenerateAgendaDocumentSyncfusion(long meetingId, int customerId)
        {
            byte[] bytes = null;

            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            var agenda = objIMeeting_Service.PreviewAgenda(meetingId, customerId);
            var docSetting = objIDocumentSetting_Service.GetDocumentSetting(customerId, SecretarialConst.MeetingDocumentTypes.AGENDA);
            var agendaItems = agenda.lstAgendaItems;

            #region Generate Agenda Document
            #region meeting details
            var meetingSrNo = "";
            var meetingTypeName = "";
            if (agenda.MeetingSrNo.HasValue)
            {
                var srNo = Convert.ToString(agenda.MeetingSrNo);
                switch (srNo)
                {
                    case "1":
                        meetingSrNo = srNo + "st";
                        break;
                    case "2":
                        meetingSrNo = srNo + "nd";
                        break;
                    case "3":
                        meetingSrNo = srNo + "rd";
                        break;
                    default:
                        meetingSrNo = srNo + "th";
                        break;
                }
            }

            if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
            {
                meetingTypeName = "Annual General Meeting";
            }
            else if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
            {
                meetingTypeName = "Extra Ordinary General Meeting";
            }

            string mDate = string.Empty;
            string mDay = string.Empty;
            string mTime = string.Empty;
            string mVenue = string.Empty;
            string cDate = string.Empty;

            if (agenda.MeetingDate != null)
            {
                var date = Convert.ToDateTime(agenda.MeetingDate);
                mDate = date.ToString("MMMM dd, yyyy");
                mDay = date.ToString("dddd");
            }
            if (agenda.CircularDate != null)
            {
                cDate = Convert.ToDateTime(agenda.CircularDate).ToString("MMMM dd, yyyy");
            }
            if (!string.IsNullOrEmpty(agenda.MeetingTime))
            {
                mTime = agenda.MeetingTime;
            }
            if (!string.IsNullOrEmpty(agenda.MeetingVenue))
            {
                mVenue = agenda.MeetingVenue;
            }
            #endregion

            using (WordDocument doc = new WordDocument())
            {
                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();
                RadFlowDocument htmlDocument = new RadFlowDocument();

                doc.EnsureMinimal();

                WParagraphStyle myParagraphStyle = (WParagraphStyle)doc.AddParagraphStyle("MyParagraphStyle");
                myParagraphStyle.CharacterFormat.FontName = "Bookman Old Style";
                myParagraphStyle.CharacterFormat.FontSize = 12;
                var objStyleVM = new StyleVM();
                if (docSetting != null)
                {
                    if (docSetting.Styles != null)
                    {
                        objStyleVM = docSetting.Styles.Where(k => k.StyleName == "Paragraph").Select(k => k).FirstOrDefault();
                        if (objStyleVM != null)
                        {
                            myParagraphStyle.CharacterFormat.FontName = objStyleVM.FontName;
                            float fontSize = 12;
                            if (!float.TryParse(objStyleVM.FontSize, out fontSize))
                            {
                                fontSize = 12;
                            }
                            myParagraphStyle.CharacterFormat.FontSize = fontSize;
                        }
                    }
                }


                if (agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.AGM && agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.EGM)
                {
                    #region Agenda Document generation for Board & Committee
                    #region Header
                    var paragraph = doc.LastParagraph;
                    var headerText = paragraph.AppendText(agenda.EntityName) as WTextRange;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    headerText.CharacterFormat.FontSize = 18;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    headerText = paragraph.AppendText(agenda.EntityAddressLine1) as WTextRange;
                    headerText.CharacterFormat.FontSize = 10;
                    headerText.CharacterFormat.Bold = true;

                    headerText = paragraph.AppendText(agenda.EntityAddressLine2) as WTextRange;
                    headerText.CharacterFormat.FontSize = 10;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    headerText = paragraph.AppendText(agenda.EntityCIN_LLPIN) as WTextRange;
                    headerText.CharacterFormat.FontSize = 12;
                    headerText.CharacterFormat.Bold = true;

                    paragraph = doc.LastSection.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    paragraph.AppendText(Environment.NewLine);

                    var agendaMainHeading = "AGENDA FOR THE " + meetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "") + " " + agenda.MeetingTypeName.ToUpper() + (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING ? " MEETING" : " CIRCULAR");
                    headerText = paragraph.AppendText(agendaMainHeading) as WTextRange;
                    headerText.CharacterFormat.FontSize = 18;
                    headerText.CharacterFormat.Bold = true;
                    #endregion

                    if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING)
                    {
                        #region Table
                        paragraph = doc.LastSection.AddParagraph() as WParagraph;
                        IWTable table = doc.LastSection.AddTable();
                        table.ResetCells(4, 3);
                        float tableWidth = doc.LastSection.PageSetup.PageSize.Width - (doc.LastSection.PageSetup.Margins.Left + doc.LastSection.PageSetup.Margins.Right);

                        tableWidth = (tableWidth / 100) * 80;
                        //float individualCellWidth = tableWidth / 3;
                        float individualCellWidth = tableWidth / 100;

                        for (int i = 0; i < 4; i++)
                        {
                            table.Rows[i].Cells[0].Width = 18 * individualCellWidth;
                            table.Rows[i].Cells[1].Width = 2 * individualCellWidth;
                            table.Rows[i].Cells[2].Width = 80 * individualCellWidth;
                        }

                        var cellRun = table.Rows[0].Cells[0].AddParagraph().AppendText("DATE") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[0].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[0].Cells[2].AddParagraph().AppendText(mDate) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        //Day
                        cellRun = table.Rows[1].Cells[0].AddParagraph().AppendText("DAY") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[2].AddParagraph().AppendText(mDay) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;
                        //End

                        //Time
                        cellRun = table.Rows[2].Cells[0].AddParagraph().AppendText("TIME") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[2].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[2].Cells[2].AddParagraph().AppendText(mTime) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;
                        //End

                        //Venue
                        cellRun = table.Rows[3].Cells[0].AddParagraph().AppendText("VENUE") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[3].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[3].Cells[2].AddParagraph().AppendText(mVenue) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;
                        //End
                        #endregion
                    }
                    else
                    {
                        #region Table
                        paragraph = doc.LastSection.AddParagraph() as WParagraph;
                        IWTable table = doc.LastSection.AddTable();
                        table.ResetCells(2, 3);
                        float tableWidth = doc.LastSection.PageSetup.PageSize.Width - (doc.LastSection.PageSetup.Margins.Left + doc.LastSection.PageSetup.Margins.Right);

                        tableWidth = (tableWidth / 100) * 80;
                        //float individualCellWidth = tableWidth / 3;
                        float individualCellWidth = tableWidth / 100;

                        for (int i = 0; i < 2; i++)
                        {
                            table.Rows[i].Cells[0].Width = 22 * individualCellWidth;
                            table.Rows[i].Cells[1].Width = 2 * individualCellWidth;
                            table.Rows[i].Cells[2].Width = 76 * individualCellWidth;
                        }

                        var cellRun = table.Rows[0].Cells[0].AddParagraph().AppendText("CIRCULAR DATE") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[0].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[0].Cells[2].AddParagraph().AppendText(cDate) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[0].AddParagraph().AppendText("DUE DATE") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[2].AddParagraph().AppendText(mDate) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;
                        #endregion
                    }

                    #region TOC
                    WParagraph para = doc.LastSection.AddParagraph() as WParagraph;
                    //Insert TOC
                    TableOfContent toc = null;

                    if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING)
                    {
                        para.AppendText("Agenda Items");
                        para.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        para.ApplyStyle(BuiltinStyle.Heading1);

                        para = doc.LastSection.AddParagraph() as WParagraph;
                        para.AppendText(Environment.NewLine);

                        toc = para.AppendTOC(1, 3);
                    }
                    //para.ApplyStyle(BuiltinStyle.Heading4);
                    WSection section = doc.LastSection;
                    #endregion

                    #region Agenda Items
                    if (agendaItems != null)
                    {
                        WParagraph newPara = section.AddParagraph() as WParagraph;
                        var SrNo = 0;
                        foreach (var item in agendaItems)
                        {
                            SrNo++;

                            newPara = section.AddParagraph() as WParagraph;
                            //section.BreakCode = SectionBreakCode.NewPage;
                            newPara.AppendBreak(Syncfusion.DocIO.DLS.BreakType.PageBreak);

                            newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Right;
                            WTextRange text = newPara.AppendText("Item No. " + SrNo) as WTextRange;
                            text.CharacterFormat.UnderlineStyle = UnderlineStyle.Single;
                            text.CharacterFormat.Bold = true;
                            text.CharacterFormat.Italic = true;
                            text.CharacterFormat.FontSize = 12;

                            newPara = section.AddParagraph() as WParagraph;

                            #region Convert html format in XHTML 1.0
                            var htmlstring = (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper());
                            htmlstring = htmlstring.Replace("<B>", "<b>");
                            htmlstring = htmlstring.Replace("</B>", "</b>");
                            //htmlstring = htmlProvider.Export(htmlDocument);
                            #endregion

                            bool isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                            if (isValidHtml)
                            {
                                //section.Body.InsertXHTML(htmlstring);
                                newPara.AppendHTML(htmlstring);
                                newPara.ParagraphFormat.Borders.Bottom.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Single;
                                //newPara.ParagraphFormat.Borders.Bottom.LineWidth = 1;
                                newPara.ApplyStyle(BuiltinStyle.Heading2);
                            }

                            htmlstring = item.AgendaFormat;
                            #region Convert html format in XHTML 1.0
                            htmlDocument = htmlProvider.Import(htmlstring);
                            htmlstring = htmlProvider.Export(htmlDocument);
                            #endregion

                            newPara = section.AddParagraph() as WParagraph;

                            isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                            if (isValidHtml)
                            {
                                //section.Body.InsertXHTML(htmlstring);
                                newPara.AppendHTML(htmlstring);
                                var ss = newPara.StyleName;
                                newPara.ApplyStyle("MyParagraphStyle");
                            }
                            //newPara = section.AddParagraph() as WParagraph;
                            //                    newPara.AppendBreak(BreakType.PageBreak);

                            #region Page Numbers
                            //IWParagraph paragraphFooter = section.HeadersFooters.Footer.AddParagraph();
                            //paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                            //paragraphFooter.AppendField("Page", FieldType.FieldPage);
                            //paragraphFooter = section.AddParagraph();
                            #endregion

                            #region Agenda Documents
                            var docs = objIAgendaMinutesReviewService.GetAgendaDocument(item.MeetingAgendaMappingID, customerId);
                            var docCount = 1;
                            if (docs != null)
                            {
                                #region setting doc margin
                                //section = doc.AddSection() as WSection;
                                //// Setting document page margins.
                                //MarginsF pagemargins = new MarginsF();
                                //pagemargins.Bottom = 10;
                                //pagemargins.Top = 10;
                                //pagemargins.Left = 10;
                                //pagemargins.Right = 10;
                                //// Assigning document page margins to the current section.
                                //section.PageSetup.Margins = pagemargins;
                                #endregion

                                foreach (var annexure in docs)
                                {
                                    section.BreakCode = SectionBreakCode.NewPage;
                                    newPara = section.AddParagraph() as WParagraph;
                                    var actualFileName = objIFileData_Service.GetSecretarialDocs(annexure.ID);
                                    var fileName = annexure.FileName;
                                    var extension = System.IO.Path.GetExtension(fileName);
                                    switch (extension)
                                    {
                                        case ".pdf":
                                            newPara.AppendText("Annexure " + docCount);
                                            newPara.ApplyStyle(BuiltinStyle.Heading3);
                                            newPara = section.AddParagraph() as WParagraph;
                                            PdfLoadedDocument loadedDocument = new PdfLoadedDocument(actualFileName.FileData);
                                            //Get the PDF page count
                                            int count = loadedDocument.Pages.Count;
                                            //Convert each page to image file
                                            for (int p = 0; p < count; p++)
                                            {
                                                //System.Drawing.Image image = loadedDocument.ExportAsImage(p);
                                                //newPara.AppendPicture(image);
                                                System.Drawing.Image image = loadedDocument.ExportAsImage(p);
                                                var picture = newPara.AppendPicture(image) as WPicture;
                                                float clientWidth = section.PageSetup.ClientWidth;
                                                float clientHeight = section.PageSetup.PageSize.Height - section.PageSetup.Margins.Top - section.PageSetup.Margins.Bottom;
                                                float scalePer = 100;

                                                if (picture.Width > clientWidth)
                                                {
                                                    scalePer = clientWidth / image.Width * 100;
                                                }
                                                else if (picture.Height > clientHeight)
                                                {
                                                    scalePer = clientHeight / image.Height * 100;
                                                }
                                                // This will resizes the width and height.
                                                picture.WidthScale = scalePer;
                                                picture.HeightScale = scalePer;

                                            }
                                            loadedDocument.Close();
                                            docCount++;
                                            break;
                                        case ".docx":
                                        case ".doc":
                                            var tempFile = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                            if (!string.IsNullOrEmpty(tempFile))
                                            {
                                                newPara.AppendText("Annexure " + docCount);
                                                newPara.ApplyStyle(BuiltinStyle.Heading3);
                                                newPara = section.AddParagraph() as WParagraph;

                                                #region Word to image
                                                var formatType = FormatType.Docx;
                                                if (extension == ".doc")
                                                {
                                                    formatType = FormatType.Doc;
                                                }
                                                //Loads an existing Word document
                                                WordDocument wordDocument = new WordDocument(Server.MapPath(@"~\" + tempFile), formatType);
                                                //Initializes the ChartToImageConverter for converting charts during Word to image conversion
                                                //wordDocument.ChartToImageConverter = new ChartToImageConverter();
                                                //Sets the scaling mode for charts (Normal mode reduces the file size)
                                                //wordDocument.ChartToImageConverter.ScalingMode = ScalingMode.Normal;
                                                //Converts word document to image
                                                var images = wordDocument.RenderAsImages(ImageType.Bitmap);
                                                foreach (var image in images)
                                                {
                                                    var picture = newPara.AppendPicture(image) as WPicture;
                                                    float clientWidth = section.PageSetup.ClientWidth;
                                                    float clientHeight = section.PageSetup.PageSize.Height - section.PageSetup.Margins.Top - section.PageSetup.Margins.Bottom;
                                                    float scalePer = 100;

                                                    if (picture.Width > clientWidth)
                                                    {
                                                        scalePer = clientWidth / image.Width * 100;
                                                    }
                                                    else if (picture.Height > clientHeight)
                                                    {
                                                        scalePer = clientHeight / image.Height * 100;
                                                    }
                                                    // This will resizes the width and height.
                                                    picture.WidthScale = scalePer;
                                                    picture.HeightScale = scalePer;
                                                }
                                                //Closes the document
                                                wordDocument.Close();
                                                #endregion

                                                docCount++;
                                            }

                                            #region insert word
                                            //var tempFile = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                            //if(!string.IsNullOrEmpty(tempFile))
                                            //{
                                            //    newPara.AppendText("Annexure " + docCount);
                                            //    newPara.ApplyStyle(BuiltinStyle.Heading3);
                                            //    newPara = section.AddParagraph() as WParagraph;

                                            //    WordDocument wordDocument = new WordDocument(Server.MapPath(@"~\" + tempFile), FormatType.Docx);
                                            //    doc.ImportContent(wordDocument, ImportOptions.KeepSourceFormatting);
                                            //    //wordDocument.ChartToImageConverter = new ChartToImageConverter();
                                            //    wordDocument.RenderAsImages(ImageType.Metafile);

                                            //    wordDocument.Close();
                                            //    docCount++;
                                            //}
                                            #endregion
                                            break;
                                        //case ".doc":
                                        //    #region insert word
                                        //    var tempFiledoc = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                        //    if (!string.IsNullOrEmpty(tempFiledoc))
                                        //    {
                                        //        newPara.AppendText("Annexure " + docCount);
                                        //        newPara.ApplyStyle(BuiltinStyle.Heading3);
                                        //        newPara = section.AddParagraph() as WParagraph;
                                        //        WordDocument wordDocument = new WordDocument(Server.MapPath(@"~\" + tempFiledoc), FormatType.Doc);
                                        //        doc.ImportContent(wordDocument, ImportOptions.UseDestinationStyles);
                                        //        wordDocument.Close();

                                        //        docCount++;
                                        //    }
                                        //    #endregion
                                        //    break;
                                        case ".bmp":
                                        case ".jpeg":
                                        case ".jpg":
                                        case ".png":
                                        case ".tiff":
                                        case ".ico":
                                        case ".icon":
                                            var tempFileimg = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                            if (!string.IsNullOrEmpty(tempFileimg))
                                            {
                                                newPara.AppendText("Annexure " + docCount);
                                                newPara.ApplyStyle(BuiltinStyle.Heading3);
                                                newPara = section.AddParagraph() as WParagraph;

                                                System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath(@"~\" + tempFileimg));
                                                newPara.AppendPicture(img);

                                                docCount++;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            #endregion
                        }
                    }

                    #endregion

                    #region Page Numbers
                    IWParagraph paragraphFooter = section.HeadersFooters.Footer.AddParagraph();
                    paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    var pageNum = paragraphFooter.AppendField("Page", FieldType.FieldPage) as WField;
                    pageNum.CharacterFormat.FontSize = 10;
                    //paragraphFooter = section.AddParagraph();
                    #endregion

                    #region Page Numbers
                    ////Adds a footer paragraph text to the document
                    //IWParagraph paragraphFooter = section.HeadersFooters.Footer.AddParagraph();
                    ////paragraphFooter.ParagraphFormat.Tabs.AddTab(523f, TabJustification.Centered, TabLeader.NoLeader);

                    //paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    ////Adds text for the footer paragraph
                    ////paragraphFooter.AppendText("Copyright Northwind Inc. 2001 - 2015");
                    ////Adds page number field to the document
                    ////paragraphFooter.AppendText("\tPage ");
                    //paragraphFooter.AppendField("Page", FieldType.FieldPage);
                    ////Adds the paragraph
                    //paragraphFooter = section.AddParagraph();
                    #endregion

                    #region Update table of content
                    if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING && toc != null)
                    {
                        toc.IncludePageNumbers = true;
                        toc.RightAlignPageNumbers = true;
                        toc.UseHyperlinks = true;
                        toc.LowerHeadingLevel = Convert.ToInt32(1);
                        toc.UpperHeadingLevel = Convert.ToInt32(3);
                        toc.UseOutlineLevels = true;
                        toc.UseTableEntryFields = true;
                        doc.UpdateTableOfContents();
                    }
                    #endregion

                    #region Page Setup
                    if (docSetting != null)
                    {
                        if (docSetting.PageSetting != null)
                        {
                            switch (docSetting.PageSetting.PageSize)
                            {
                                case "A3":
                                    section.PageSetup.PageSize = PageSize.A3;
                                    break;
                                case "A4":
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                                case "A5":
                                    section.PageSetup.PageSize = PageSize.A5;
                                    break;
                                case "A6":
                                    section.PageSetup.PageSize = PageSize.A6;
                                    break;
                                case "Legal":
                                    section.PageSetup.PageSize = PageSize.Legal;
                                    break;
                                case "Letter":
                                    section.PageSetup.PageSize = PageSize.Letter;
                                    break;
                                default:
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                            }
                        }

                        section.PageSetup.Margins.Top = (float)docSetting.PageSetting.MarginTop;
                        section.PageSetup.Margins.Left = (float)docSetting.PageSetting.MarginLeft;
                        section.PageSetup.Margins.Right = (float)docSetting.PageSetting.MarginRight;
                        section.PageSetup.Margins.Bottom = (float)docSetting.PageSetting.MarginBottom;
                    }
                    #endregion
                    #endregion
                }
                else
                {
                    #region Agenda Document generation for AGM/EGM
                    #region Header
                    var paragraph = doc.LastParagraph;
                    var headerText = paragraph.AppendText(agenda.EntityName) as WTextRange;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    headerText.CharacterFormat.FontSize = 18;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    headerText = paragraph.AppendText(agenda.EntityAddressLine1) as WTextRange;
                    headerText.CharacterFormat.FontSize = 10;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    headerText = paragraph.AppendText(agenda.EntityAddressLine2) as WTextRange;
                    headerText.CharacterFormat.FontSize = 10;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    headerText = paragraph.AppendText(agenda.EntityCIN_LLPIN) as WTextRange;
                    headerText.CharacterFormat.FontSize = 11;

                    paragraph.AppendText(Environment.NewLine);

                    paragraph = doc.LastSection.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;

                    headerText = paragraph.AppendText("NOTICE OF " + meetingSrNo + " " + agenda.MeetingTypeName.ToUpper() + "") as WTextRange;
                    headerText.CharacterFormat.FontSize = 18;
                    headerText.CharacterFormat.Bold = true;

                    paragraph = doc.LastSection.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Justify;
                    var agendaMainHeading = "Notice is hereby given that the " + meetingSrNo + " " + meetingTypeName + " of the members of " + agenda.EntityName + " will be held on " + mDay + " " + mDate + " at " + mTime + " at " + mVenue + " to transact following business:";
                    headerText = paragraph.AppendText(agendaMainHeading) as WTextRange;
                    headerText.CharacterFormat.FontSize = 14;
                    //headerText.CharacterFormat.Bold = true;

                    WSection section = doc.LastSection;
                    #endregion

                    #region Agenda Items
                    if (agendaItems != null)
                    {
                        IWParagraph newPara;
                        var SrNo = 0;
                        string business_ = "", businessLast_ = "";
                        foreach (var item in agendaItems)
                        {
                            SrNo++;
                            businessLast_ = item.IsOrdinaryBusiness ? "Ordinary Business" : "Special Business";

                            if (business_ != businessLast_)
                            {
                                business_ = businessLast_;
                                newPara = section.AddParagraph() as WParagraph;
                                newPara.AppendText(business_);
                                newPara.ApplyStyle(BuiltinStyle.Heading1);
                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                            }

                            newPara = section.AddParagraph() as WParagraph;

                            #region Convert html format in XHTML 1.0
                            //var htmlstring = SrNo + ". " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper());
                            var htmlstring = SrNo + ". " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText);
                            htmlstring = htmlstring.Replace("<B>", "<b>");
                            htmlstring = htmlstring.Replace("</B>", "</b>");
                            #endregion

                            bool isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                            if (isValidHtml)
                            {
                                newPara.AppendHTML(htmlstring);
                                newPara.ApplyStyle(BuiltinStyle.Heading2);
                            }

                            if (!item.IsOrdinaryBusiness)
                            {
                                htmlstring = item.AgendaFormat == null ? " " : item.AgendaFormat;

                                htmlDocument = htmlProvider.Import(htmlstring);
                                htmlstring = htmlProvider.Export(htmlDocument);

                                newPara = section.AddParagraph() as WParagraph;

                                isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                                if (isValidHtml)
                                {
                                    newPara.AppendHTML(htmlstring);
                                }
                            }
                            #region Page Numbers
                            //IWParagraph paragraphFooter = section.HeadersFooters.Footer.AddParagraph();
                            //paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                            //paragraphFooter.AppendField("Page", FieldType.FieldPage);
                            //paragraphFooter = section.AddParagraph();
                            #endregion
                        }
                    }
                    #endregion

                    #region Page Numbers
                    IWParagraph paragraphFooter = section.HeadersFooters.Footer.AddParagraph();
                    paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    var pageNum = paragraphFooter.AppendField("Page", FieldType.FieldPage) as WField;
                    pageNum.CharacterFormat.FontSize = 10;
                    #endregion

                    #region Signing Authority
                    paragraph = section.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;

                    paragraph.AppendText(Environment.NewLine);
                    var textRange = paragraph.AppendText("By Order of the Board of Director") as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    textRange = paragraph.AppendText("For " + agenda.EntityName) as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;

                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    paragraph = section.AddParagraph() as WParagraph;
                    if (agenda.MeetingSigningAuthorityForAGM != null)
                    {
                        textRange = paragraph.AppendText(agenda.MeetingSigningAuthorityForAGM.AuthorityName) as WTextRange;
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                        textRange = paragraph.AppendText("(" + agenda.MeetingSigningAuthorityForAGM.Designation + ")") as WTextRange;
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                        if (agenda.MeetingSigningAuthorityForAGM.IsCS == true)
                        {
                            textRange = paragraph.AppendText("Membership No. " + agenda.MeetingSigningAuthorityForAGM.MembershipNo) as WTextRange;
                            textRange.CharacterFormat.FontSize = 12;
                            textRange.CharacterFormat.Bold = true;
                            paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                        }
                        else
                        {
                            textRange = paragraph.AppendText("DIN : " + agenda.MeetingSigningAuthorityForAGM.DIN_PAN) as WTextRange;
                            textRange.CharacterFormat.FontSize = 12;
                            textRange.CharacterFormat.Bold = true;
                            paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                        }
                    }

                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    textRange = paragraph.AppendText("Date") as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);


                    textRange = paragraph.AppendText("Place : " + agenda.EntityCity) as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    #endregion

                    #region Notes
                    if (!string.IsNullOrEmpty(agenda.GM_Notes))
                    {
                        paragraph = section.AddParagraph() as WParagraph;
                        paragraph.AppendText("Notes for Members' Attention");
                        paragraph.ApplyStyle(BuiltinStyle.Heading1);

                        var htmlstring = agenda.GM_Notes == null ? " " : agenda.GM_Notes;
                        htmlDocument = htmlProvider.Import(htmlstring);
                        htmlstring = htmlProvider.Export(htmlDocument);

                        paragraph = section.AddParagraph() as WParagraph;

                        var isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                        if (isValidHtml)
                        {
                            paragraph.AppendHTML(htmlstring);
                        }
                    }
                    #endregion

                    #region Explanatory statements
                    var hasExplanatorystatements = false;
                    if (agendaItems != null)
                    {
                        foreach (var item in agendaItems)
                        {
                            if (!string.IsNullOrEmpty(item.ExplanatoryStatement))
                            {
                                hasExplanatorystatements = true;
                                break;
                            }
                        }

                        if (hasExplanatorystatements)
                        {
                            paragraph = section.AddParagraph() as WParagraph;
                            paragraph.AppendText("Explanatory Statement pursuant to Section 102 (1) of the companies act, 2013");
                            paragraph.ApplyStyle(BuiltinStyle.Heading1);

                            paragraph = section.AddParagraph() as WParagraph;
                            paragraph.AppendText("The following statement sets out all material facts relating to special business mentioned in the notice:");

                            var SrNo = 0;
                            string resolutionType = "";
                            foreach (var item in agendaItems)
                            {
                                SrNo++;
                                if (!string.IsNullOrEmpty(item.ExplanatoryStatement))
                                {
                                    resolutionType = item.IsSpecialResolution ? "Special resolution" : "Ordinary resolution";
                                    string AgendaItem = SrNo + ". " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper());
                                    paragraph = section.AddParagraph() as WParagraph;
                                    paragraph.AppendText(AgendaItem);
                                    paragraph.ApplyStyle(BuiltinStyle.Heading2);

                                    paragraph = section.AddParagraph() as WParagraph;
                                    var htmlstring = item.ExplanatoryStatement == null ? " " : item.ExplanatoryStatement;
                                    htmlDocument = htmlProvider.Import(htmlstring);
                                    htmlstring = htmlProvider.Export(htmlDocument);

                                    var isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                                    if (isValidHtml)
                                    {
                                        paragraph.AppendHTML(htmlstring);
                                    }

                                    htmlstring = "<p></p>" +
                                                        "None of the directors of the Company, Key Managerial Personnel and their relatives is in any way concerned or interested financially or otherwise in the resolution." +
                                                        "<p></p>" +
                                                        "The Board of Directors accordingly recommends the <strong>" + resolutionType + "</strong> set out at ITEM NO. <strong>" + SrNo + "</strong> of the notice for the approval of the members.";
                                    htmlDocument = htmlProvider.Import(htmlstring);
                                    htmlstring = htmlProvider.Export(htmlDocument);

                                    isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                                    if (isValidHtml)
                                    {
                                        paragraph = section.AddParagraph() as WParagraph;
                                        paragraph.AppendHTML(htmlstring);
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region Signing Authority
                    paragraph = section.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;

                    paragraph.AppendText(Environment.NewLine);
                    textRange = paragraph.AppendText("By Order of the Board of Director") as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    textRange = paragraph.AppendText("For " + agenda.EntityName) as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;

                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    paragraph = section.AddParagraph() as WParagraph;
                    if (agenda.MeetingSigningAuthorityForAGM != null)
                    {
                        textRange = paragraph.AppendText(agenda.MeetingSigningAuthorityForAGM.AuthorityName) as WTextRange;
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                        textRange = paragraph.AppendText("(" + agenda.MeetingSigningAuthorityForAGM.Designation + ")") as WTextRange;
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                        if (agenda.MeetingSigningAuthorityForAGM.IsCS == true)
                        {
                            textRange = paragraph.AppendText("Membership No. " + agenda.MeetingSigningAuthorityForAGM.MembershipNo) as WTextRange;
                            textRange.CharacterFormat.FontSize = 12;
                            textRange.CharacterFormat.Bold = true;
                            paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                        }
                        else
                        {
                            textRange = paragraph.AppendText("DIN : " + agenda.MeetingSigningAuthorityForAGM.DIN_PAN) as WTextRange;
                            textRange.CharacterFormat.FontSize = 12;
                            textRange.CharacterFormat.Bold = true;
                            paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                        }
                    }

                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    textRange = paragraph.AppendText("Date") as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);


                    textRange = paragraph.AppendText("Place : " + agenda.EntityCity) as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    #endregion

                    #region Page Setup
                    if (docSetting != null)
                    {
                        if (docSetting.PageSetting != null)
                        {
                            switch (docSetting.PageSetting.PageSize)
                            {
                                case "A3":
                                    section.PageSetup.PageSize = PageSize.A3;
                                    break;
                                case "A4":
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                                case "A5":
                                    section.PageSetup.PageSize = PageSize.A5;
                                    break;
                                case "A6":
                                    section.PageSetup.PageSize = PageSize.A6;
                                    break;
                                case "Legal":
                                    section.PageSetup.PageSize = PageSize.Legal;
                                    break;
                                case "Letter":
                                    section.PageSetup.PageSize = PageSize.Letter;
                                    break;
                                default:
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                            }
                        }

                        section.PageSetup.Margins.Top = (float)docSetting.PageSetting.MarginTop;
                        section.PageSetup.Margins.Left = (float)docSetting.PageSetting.MarginLeft;
                        section.PageSetup.Margins.Right = (float)docSetting.PageSetting.MarginRight;
                        section.PageSetup.Margins.Bottom = (float)docSetting.PageSetting.MarginBottom;
                    }
                    #endregion
                    #endregion
                }

                #region Document style setting
                if (docSetting != null)
                {
                    if (docSetting.Styles != null)
                    {
                        foreach (var style in docSetting.Styles)
                        {
                            WParagraphStyle docStyle = doc.Styles.FindByName(style.StyleName, Syncfusion.DocIO.DLS.StyleType.ParagraphStyle) as WParagraphStyle;
                            if (docStyle != null)
                            {
                                if (!string.IsNullOrEmpty(style.FontName))
                                {
                                    docStyle.CharacterFormat.FontName = style.FontName;
                                }

                                float fontSize = 10;
                                if (!float.TryParse(style.FontSize, out fontSize))
                                {
                                    fontSize = 10;
                                }

                                if (fontSize > 0)
                                {
                                    docStyle.CharacterFormat.FontSize = fontSize;
                                }

                                //docStyle.CharacterFormat.TextColor = Color.FromRgb(20, 50,230);

                                docStyle.CharacterFormat.Bold = style.IsBold;
                                docStyle.CharacterFormat.Italic = style.IsItalic;
                                if (style.IsUnderline)
                                {
                                    docStyle.CharacterFormat.UnderlineStyle = UnderlineStyle.Single;
                                }
                            }
                        }
                    }
                }
                #endregion

                try
                {
                    //doccontrol.DocumentSource = new DocumentSource(di, bytes);
                    //doc.Save("Sample.docx", FormatType.Docx, Response, HttpContentDisposition.Attachment);

                    #region save temp File
                    //string Folder = "~/TempFiles";
                    //string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                    //string DateFolder = Folder + "/" + File;
                    //System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                    //if (!System.IO.Directory.Exists(DateFolder))
                    //{
                    //    System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                    //}

                    //string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                    //string User = userId + "" + customerId + "" + FileDate;
                    //string FileName = DateFolder + "/" + User + ".docx";
                    //doc.Save(Server.MapPath(FileName), FormatType.Docx);
                    ////doc.Close();
                    //doccontrol.Document = FileName;
                    #endregion

                    #region return byte[]
                    MemoryStream stream = new MemoryStream();
                    doc.Save(stream, FormatType.Docx);
                    bytes = stream.ToArray();
                    #endregion
                }
                catch (Exception ex)
                {
                    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            #endregion
            return bytes;
        }
        #endregion

        #region Delete Agenda Item
        public PartialViewResult GetDeleteAgendaItem(long MeetingId, long MeetingAgendaMappingId)
        {
            var model = objIMeeting_Service.GetAllowAgendaDelete(MeetingId, MeetingAgendaMappingId, false);
            return PartialView("_DeleteAgendaItem", model);
        }
        [HttpPost]
        public PartialViewResult DeleteAgendaItem(DeleteAgendaVM model)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            model = objIMeeting_Service.DeleteAgendaItem(model, userId);
            return PartialView("_DeleteAgendaItem", model);
        }
        #endregion

        #region Meeting Pre Committee Agenda
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CheckPendingPreCommitteeAgendaItems(long MeetingId)
        {
            var PreCommittePendingAgenda = objIMeeting_Service.GetPendingPreCommittee(MeetingId);

            return Json(PreCommittePendingAgenda, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPendingPreCommitteeAgendaItems(long MeetingId)
        {
            var PreCommittePendingAgenda = objIMeeting_Service.GetPendingPreCommittee(MeetingId);
            return PartialView("_PreCommitteeAgendaItems", PreCommittePendingAgenda);
        }
        [HttpPost]
        public ActionResult SavePreCommitteeAgendaItems(int MeetingTypeID, long SourceMeetingID, long SourceMeetingAgendaMappingID, long? PostAgendaID, long TargetMeetingID)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIMeeting_Service.SavePreCommitteeMeetingID(MeetingTypeID, SourceMeetingID, SourceMeetingAgendaMappingID, PostAgendaID, TargetMeetingID, UserID);
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Meeting Agenda Item Data Fields Template Generation
        public PartialViewResult AgendaItemTemplate_Read(long MeetingAgendaMappingID)
        {
            var model = objIMeeting_Service.GetAgendaItemTemplate(MeetingAgendaMappingID);
            return PartialView("_EditAgendaItemsTemplate", model);
        }
        public PartialViewResult AgendaItemTemplate_Save(MeetingAgendaTemplateVM obj)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.SaveUpdateAgendaItemTemplateList(obj, userID);
            ModelState.Clear();
            return PartialView("_EditAgendaItemsTemplate", model);
        }
        #endregion

        #region Meeting New Agenda Item Create/Update
        [HttpGet]
        public ActionResult CreateNewAgendaItem(long MeetingId, string Part)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.CreateNewAgendaItem(MeetingId, CustomerID);

            return PartialView("_AddNewAgendaItem", model);
        }

        [HttpPost]
        public ActionResult SaveUpdateNewAgendaItem(AgendaMasterVM obj)
        {
            var model = new AgendaMasterVM();
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            if (obj.BM_AgendaMasterId == 0)
            {
                model = objIMeeting_Service.CreateNewAgendaItem(obj, CustomerID);
            }
            else
            {
                model = objIMeeting_Service.Update(obj);
            }
            ModelState.Clear();
            return PartialView("_AddNewAgendaItem", model);
        }
        #endregion       

        #region Meeting Agenda change Items No
        public PartialViewResult ChangeItemNumber(long id)
        {
            return PartialView("_EditAgendaItemNumber", id);
        }
        public ActionResult CheckAgendaItemNumberIsValid(long id)
        {
            var result = objIMeeting_Service.CheckAgendaItemNumberIsValid(id);
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAgendaItemsForChangeNumber([DataSourceRequest] DataSourceRequest request, long id)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("ItemNo", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(objIMeeting_Service.GetAgendaItemForChangeNumber(id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeItemNumber([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<ChangeAgendaItemNumberVM> agendaItems)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            if (agendaItems != null && ModelState.IsValid)
            {
                objIMeeting_Service.SaveAgendaItemNumber(agendaItems, userId);
            }
            return Json(agendaItems.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Meeting Notice Mail
        [HttpPost]
        public ActionResult EditNoticeMail(MeetingNoticeMailVM obj, string command_name)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

            obj.UserId = UserID;
            obj.CustomerId = CustomerID;
            if (command_name == "send")
            {
                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                string mailFormat = "", mailSubject = "";

                long? fileId = null;
                obj = objIMeeting_Service.SendNoticeMail(obj, out mailFormat);

                if (obj.Success)
                {
                    if (obj.IsVirtualMeeting_ == true)
                    {
                        #region Generate Compliance document on Notice Send
                        if (obj.TemplateType == "AN" || obj.TemplateType == "N")
                        {
                            #region Update Schedule on Dates
                            objIMeeting_Compliances.CalculateScheduleDate(obj.NoticeMailID, obj.UserId);
                            #endregion

                            #region Compliance Activation before Meeting
                            objIComplianceTransaction_Service.CreateTransactionOnNoticeSend(obj.NoticeMailID, UserID);
                            #endregion

                            var objFileData = GetNew_NoticeFileData(obj.NoticeMailID, mailFormat);
                            //var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.NoticeMailID, "Notice");
                            var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.NoticeMailID, "Notice", false, 0, DateTime.Now, objFileData, UserID);
                            #region Commented on 19 Aug 2020
                            //long? complianceScheduleOnID = null, transactionID = null;
                            //if (objFileData != null)
                            //{
                            //    int fileVersion = 0;
                            //    if (lstClosedTransactions != null)
                            //    {
                            //        if (lstClosedTransactions.Count > 0)
                            //        {
                            //            complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                            //            transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                            //            fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                            //        }
                            //    }

                            //    fileVersion++;
                            //    objFileData.Version = Convert.ToString(fileVersion);
                            //    objFileData = objIFileData_Service.Save(objFileData, UserID);

                            //    if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                            //    {
                            //        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                            //    }
                            //}
                            #endregion
                        }
                        #endregion

                        #region Send Mail
                        var lstParticipants = objIMeeting_Service.GetMeetingParticipants(obj.NoticeMailID);
                        if (lstParticipants != null)
                        {
                            obj.IsSummit = true;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Generate Agenda Document
                        FileDataVM objFileData_Agenda = null;
                        if (obj.TemplateType == "AN" || obj.TemplateType == "A")
                        {
                            objFileData_Agenda = NewAgendaFileData(obj.NoticeMailID);

                            if (objFileData_Agenda != null)
                            {
                                int fileVersion = 0;
                                fileVersion++;

                                objFileData_Agenda.Version = Convert.ToString(fileVersion);
                                objFileData_Agenda = objIFileData_Service.Save(objFileData_Agenda, UserID);

                                fileId = objFileData_Agenda.FileID;
                            }
                        }
                        #endregion

                        #region Generate Compliance document on Notice Send
                        if (obj.TemplateType == "AN" || obj.TemplateType == "N")
                        {
                            #region Update Schedule on Dates
                            objIMeeting_Compliances.CalculateScheduleDate(obj.NoticeMailID, obj.UserId);
                            #endregion

                            #region Compliance Activation before Meeting
                            objIComplianceTransaction_Service.CreateTransactionOnNoticeSend(obj.NoticeMailID, UserID);
                            #endregion

                            var objFileData = GetNew_NoticeFileData(obj.NoticeMailID, mailFormat);
                            //var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.NoticeMailID, "Notice");
                            var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.NoticeMailID, "Notice", false, 0, DateTime.Now, objFileData, UserID);

                            #region Commented on 19 Aug 2020
                            //long? complianceScheduleOnID = null, transactionID = null;
                            //if (objFileData != null)
                            //{
                            //    int fileVersion = 0;
                            //    if (lstClosedTransactions != null)
                            //    {
                            //        if (lstClosedTransactions.Count > 0)
                            //        {
                            //            complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                            //            transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                            //            fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                            //        }
                            //    }

                            //    fileVersion++;
                            //    objFileData.Version = Convert.ToString(fileVersion);
                            //    objFileData = objIFileData_Service.Save(objFileData, UserID);

                            //    if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                            //    {
                            //        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                            //    }

                            //    if (objFileData_Agenda != null && complianceScheduleOnID > 0)
                            //    {
                            //        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData_Agenda.FileID, 1);
                            //    }
                            //}
                            #endregion
                        }
                        #endregion

                        #region Attachments
                        //List<Attachment> attachment = new List<Attachment>();

                        //if (objFileData_Agenda != null)
                        //{
                        //    attachment.Add(new Attachment(new MemoryStream(objFileData_Agenda.FileData), objFileData_Agenda.FileName));
                        //}

                        //var docs = objIFileData_Service.GetAllSecretarialDocs(obj.NoticeMailID, SecretarialConst.MeetingDocumentTypes.NOTICE);
                        //if(docs != null)
                        //{
                        //    if(docs.Count > 0)
                        //    {
                        //        foreach (var item in docs)
                        //        {
                        //            var files = objIFileData_Service.GetSecretarialDocs(item.FileID);
                        //            attachment.Add(new Attachment(new MemoryStream(files.FileData), files.FileName));
                        //        }
                        //    }
                        //}
                        #endregion

                        #region Send Mail
                        var lstParticipants = objIMeeting_Service.GetMeetingParticipants(obj.NoticeMailID);
                        if (lstParticipants != null)
                        {
                            var accessToDirector = objISettingService.GetAccessToDirector(CustomerID);
                            if (accessToDirector)
                            {
                                var meeting = objIMeeting_Service.GetMeetingForDocumentName(CustomerID, obj.NoticeMailID);
                                var meetingSrNo = "";
                                switch (meeting.MeetingSrNo)
                                {
                                    case 1:
                                        meetingSrNo = "1st";
                                        break;
                                    case 2:
                                        meetingSrNo = "2nd";
                                        break;
                                    case 3:
                                        meetingSrNo = "3rd";
                                        break;
                                    default:
                                        meetingSrNo = meeting.MeetingSrNo + "th";
                                        break;
                                }
                                mailSubject = (obj.TemplateType == "A" ? " Agenda of " : " Notice of ") + meetingSrNo + " " + meeting.MeetingTypeName + " " + meeting.TypeName + " to be held on " + Convert.ToDateTime(meeting.MeetingDate).ToString("MMMM dd, yyyy dddd") + ".";

                                var mailSetting = EmailService.GetMailSettingByEntityId(meeting.Entityt_Id, CustomerId);
                                long MeetingNoticeLogID = 0;
                                var senderEmail = mailSetting == null ? "" : mailSetting.Email_Entity;

                                if (obj.TemplateType == "AN" || obj.TemplateType == "A" || obj.TemplateType == "N")
                                {
                                    MeetingNoticeLogID = objIMeeting_Service.CreateNoticeLog(obj.NoticeMailID, obj.TemplateType, mailFormat, senderEmail, fileId, UserID);
                                }

                                foreach (var item in lstParticipants)
                                {
                                    string Mailbody = mailFormat.Replace("{{ Name of the Director }}", item.ParticipantName);

                                    #region Attachments
                                    List<Attachment> attachment = new List<Attachment>();

                                    if (objFileData_Agenda != null)
                                    {
                                        attachment.Add(new Attachment(new MemoryStream(objFileData_Agenda.FileData), objFileData_Agenda.FileName));
                                    }

                                    var docs = objIFileData_Service.GetAllSecretarialDocs(obj.NoticeMailID, SecretarialConst.MeetingDocumentTypes.NOTICE);
                                    if (docs != null)
                                    {
                                        if (docs.Count > 0)
                                        {
                                            foreach (var doc in docs)
                                            {
                                                var files = objIFileData_Service.GetSecretarialDocs(doc.FileID);
                                                if(files != null)
                                                {
                                                    if(files.Success)
                                                    {
                                                        attachment.Add(new Attachment(new MemoryStream(files.FileData), files.FileName));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                    var sendSuccess = EmailManager.SendMail_Secretarial(mailSetting, SenderEmailAddress, new List<String>(new String[] { item.Email }), null, null, mailSubject, Mailbody, attachment);

                                    if (MeetingNoticeLogID > 0)
                                    {
                                        objIMeeting_Service.CreateNoticeTransaction(MeetingNoticeLogID, item.MeetingParticipantId, DateTime.Now, sendSuccess);
                                    }
                                }
                            }
                            obj.IsSummit = true;
                        }
                        #endregion
                    }

                }
            }
            else
            {
                obj = objIMeeting_Service.UpdateNoticeMail(obj);
            }
            ModelState.Clear();
            return PartialView("_NoticeFormat", obj);
        }

        [HttpPost]
        public ActionResult NoticeMailPreview(MeetingNoticeMailVM obj, string command_name)
        {
            obj = objIMeeting_Service.PreviewNoticeMail(obj);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult NoticeAgendaTemplate(MeetingNoticeMailVM obj, string command_name)
        {
            obj = objIMeeting_Service.NoticeAgendaTemplate(obj);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public PartialViewResult NoticeAgendaLog(long meetingId, string templateType)
        {
            dynamic expando = new ExpandoObject();
            var agendaItemSelectModel = expando as IDictionary<string, object>;
            agendaItemSelectModel.Add("NoticeMeeting_Id", meetingId);
            agendaItemSelectModel.Add("TemplateType", string.IsNullOrEmpty(templateType) ? "N" : templateType);
            return PartialView("_MeetingNoticeAgendaLog", agendaItemSelectModel);
        }

        public ActionResult DownloadNoticeDocument(long meetingId)
        {
            try
            {
                var noticeMail = objIMeeting_Service.GetNoticeFormatByMeetingId(meetingId);
                DocxFormatProvider provider = new DocxFormatProvider();
                RadFlowDocument document = GetNoticeDocument(noticeMail);
                Byte[] bytes = provider.Export(document);

                return File(bytes, "application/force-download", "Notice.docx");
            }
            catch (Exception e)
            {
                return PartialView("_FileNotFound");
            }
        }
        #endregion

        #region Invitee Mail
        [HttpPost]
        public ActionResult EditInviteeMail(MeetingInviteeMailVM obj, int userid, string command_name)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            if (command_name == "send")
            {
                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                string mailFormat = "";

                var lstInvitees = objIMeetingInvitee.GetAll(obj.InviteeMailID, true);

                if (lstInvitees != null)
                {
                    var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerID, obj.InviteeMailID);
                    var meetingSrNo = "";
                    switch (meeting.MeetingSrNo)
                    {
                        case 1:
                            meetingSrNo = "1st";
                            break;
                        case 2:
                            meetingSrNo = "2nd";
                            break;
                        case 3:
                            meetingSrNo = "3rd";
                            break;
                        default:
                            meetingSrNo = meeting.MeetingSrNo + "th";
                            break;
                    }
                    string mailSubject = "Notice of " + meetingSrNo + " " + meeting.MeetingTypeName + " " + meeting.TypeName + " to be held on " + Convert.ToDateTime(meeting.MeetingDate).ToString("MMMM dd, yyyy dddd") + ".";

                    var mailSetting = EmailService.GetMailSettingByEntityId(meeting.Entityt_Id, CustomerId);

                    var senderEmail = mailSetting == null ? "" : mailSetting.Email_Entity;
                    obj = objIMeeting_Service.SendInviteeMail(obj, out mailFormat);
                    long MeetingNoticeLogID = objIMeeting_Service.CreateNoticeLog(obj.InviteeMailID, SecretarialConst.MeetingTemplatType.INVITEE_MAIL, mailFormat, senderEmail, null, UserID);

                    foreach (var item in lstInvitees)
                    {
                        if (!string.IsNullOrEmpty(mailFormat))
                        {
                            var table = "<Table border='1' style='width: 80%;text-align: center;margin-left: 50px;'>" +
                                        "<tr>" +
                                        "<th>SrNo</th>" +
                                        "<th>Agenda List</th>" +
                                        "</tr>";
                            int srNo = 0;
                            foreach (var agendaItem in item.lstAgendaItems)
                            {
                                srNo++;
                                table += "<tr>" +
                                        "<td>" + srNo + "</td>" +
                                        "<td>" + agendaItem.AgendaHeading + "</td>" +
                                        "</tr>";
                            }
                            table += "</Table>";

                            if (!string.IsNullOrEmpty(item.ParticipantEmail))
                            {
                                string Mailbody = mailFormat.Replace("{{ InviteeAgendaList }}", table);
                                //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { item.ParticipantEmail }), null, null, mailSubject, Mailbody);
                                var sendSuccess = EmailManager.SendMail_Secretarial(mailSetting, SenderEmailAddress, new List<String>(new String[] { item.ParticipantEmail }), null, null, mailSubject, Mailbody, null);
                                if (MeetingNoticeLogID > 0)
                                {
                                    objIMeeting_Service.CreateNoticeTransaction(MeetingNoticeLogID, item.MeetingParticipantId, DateTime.Now, sendSuccess);
                                }
                                obj.Success = true;
                                obj.Message = "Send mail successfully.";
                            }
                        }
                    }
                }
            }
            else
            {
                obj = objIMeeting_Service.UpdateInviteeMail(obj, UserID);
            }
            return PartialView("_inviteeMailFormat", obj);
        }
        [HttpPost]
        public ActionResult InviteeMailPreview(MeetingInviteeMailVM obj, string command_name)
        {
            obj = objIMeeting_Service.PreviewInviteeMail(obj);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Circular
        [NonAction]
        public void LoadCircularAgendaData(long ParentID)
        {
            ViewBag.DetailsOfAgenda_Grid = objIMeeting_Service.GetAllAgendaDetails(ParentID);
        }
        [HttpPost]
        public ActionResult GetAgendaResponseChartData(long MeetingID, long AgendaId, long MappingId)
        {
            int UserId = AuthenticationHelper.UserID;
            var model = objIMeeting_Service.GetAgendaResponseChartData(MeetingID, AgendaId, MappingId, UserId);
            return Json(model);
        }

        public ActionResult Agenda_SummitResponse([DataSourceRequest] DataSourceRequest request, long MeetingId, string ViewType)
        {
            Agenda_SummitResponse obj = new Agenda_SummitResponse();
            obj.MeetingId = MeetingId;
            obj.ViewType = "";
            return PartialView("_AgendaResponseSummite", obj);
        }

        [HttpGet]
        public ActionResult GetAgendaResponse([DataSourceRequest] DataSourceRequest request, long MeetingID, string ViewType)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int? userId = null;
            string Role = null;
            int? RoleId = null;
            //if (ViewType == "Participant")
            //{
            userId = Convert.ToInt32(AuthenticationHelper.UserID);
            Role = AuthenticationHelper.Role;
            if (Role == "CS")
            {
                RoleId = 20;
            }
            else
            {
                RoleId = 0;
            }
            //}
            var gridData = objIMeeting_Service.GetAgendaResponse(MeetingID, customerId, userId, RoleId);
            LoadCircularAgendaData(MeetingID);

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("Castingvote", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(gridData.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public PartialViewResult FinalizeResolution(FinalizeResolutionVM model, string finalizeResolutionCommand)
        {
            if (ModelState.IsValid)
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                objIMeeting_Service.FinalizeResolution(model, userId, customerId);
            }
            return PartialView("_FinalizeResolution", model);
        }
        #endregion

        #region Agenda Documents
        [HttpPost]
        public ActionResult UpdateAgendaResponse([DataSourceRequest] DataSourceRequest request,
        [Bind(Prefix = "models")] IEnumerable<Agenda_SummitResponse> agendaResponse)
        {
            if (agendaResponse != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var item in agendaResponse)
                {
                    var result = objIMeeting_Service.UpdateAgendaResponse(item, UserId);
                    //item.AvailabilityResponseID = result.AvailabilityResponseID;
                }
            }
            return Json(agendaResponse.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult UploadAgendaFile(VM_AgendaDocument _vmagendaDocument)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                bool Savesuccess = objIMeeting_Service.AgendaFileUpload(_vmagendaDocument, userID, customerID);
                if (Savesuccess)
                {
                    _vmagendaDocument.Success = true;
                    _vmagendaDocument.Message = "File Upload successfully";
                }
                else
                {
                    _vmagendaDocument.Error = true;
                    _vmagendaDocument.Message = "Some error occured";
                }
            }
            return PartialView("_AgendaFileUpload", _vmagendaDocument);
        }

        [HttpGet]
        public ActionResult ReloadPieChart(long MeetingId)
        {
            LoadCircularAgendaData(MeetingId);
            return PartialView("_CircularAgendaResponsesPiachart", MeetingId);
        }

        public ActionResult AgendaDocumentDownload(long Id)
        {
            bool downloadFile = objIMeeting_Service.agendaDocumentDownload(Id);
            return View();
        }

        public ActionResult AgendaDocumentsList(long MeetingAgendaMappingID)
        {
            VM_AgendaDocument _vm_agendadocument = new VM_AgendaDocument();
            _vm_agendadocument.MeetingAggendaMappingId = MeetingAgendaMappingID;
            return PartialView("_Agenda_wize_Document", _vm_agendadocument);
        }

        public ActionResult AgendaDocumentsListNew(long MeetingAgendaMappingID, long AgendaDocumentMeetingID)
        {
            var _vm_agendadocument = new VM_AgendaDocument()
            {
                AgendaDocumentMeetingId = AgendaDocumentMeetingID,
                MeetingAggendaMappingId = MeetingAgendaMappingID
            };
            return PartialView("_Agenda_wize_Document", _vm_agendadocument);
        }
        public ActionResult AgendaDocumentsListForReview(long MeetingAgendaMappingID, long AgendaDocumentMeetingID)
        {
            var _vm_agendadocument = new VM_AgendaDocument()
            {
                AgendaDocumentMeetingId = AgendaDocumentMeetingID,
                MeetingAggendaMappingId = MeetingAgendaMappingID
            };
            return PartialView("_Agenda_Documents_Review", _vm_agendadocument);
        }

        public ActionResult AgendaDocuments(long MeetingAgendaMappingID)
        {
            VM_AgendaDocument _vm_agendadocument = new VM_AgendaDocument();
            _vm_agendadocument.MeetingAggendaMappingId = MeetingAgendaMappingID;
            return PartialView("_Agenda_Documents", _vm_agendadocument);
        }
        #endregion

        #region Circular Voting Noting
        public ActionResult MeetingVootingNoting(long MeetingId)
        {
            MeetingAttendance_VM _meetingattendance = new MeetingAttendance_VM();
            _meetingattendance.MeetingId = MeetingId;
            return PartialView("_Meeting", _meetingattendance);
        }
        #endregion

        #region Minutes of Meeting
        public PartialViewResult MOMPreview(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewMOM(MeetingId, CustomerId);
            if (model != null)
            {
                model.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                //if(model.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || model.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                //{
                model.lstOtherParticipantAttendance = objIMainMeeting.GetOtherParticipentAttendence(MeetingId);
                //}
                model.meetingMinutesDetails = objIMeeting_Service.GetMinutesDetails(MeetingId);
            }
            return PartialView("_Viewer_Minutes", model);
        }

        public PartialViewResult MOMPreviewNewOld(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewMOM(MeetingId, CustomerId);
            if (model != null)
            {
                model.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                //if (model.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || model.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                //{
                model.lstOtherParticipantAttendance = objIMainMeeting.GetOtherParticipentAttendence(MeetingId);
                //}
                model.meetingMinutesDetails = objIMeeting_Service.GetMinutesDetails(MeetingId);
            }
            return PartialView("_Viewer_Draft", model);
        }

        public PartialViewResult MOMPreviewNew(long MeetingId)
        {
            var model = objIMeeting_Service.GetDraftMinutesDetails(MeetingId);
            return PartialView("_Viewer_DraftNew", model);
        }

        //Used in virtual meeting
        public PartialViewResult MinutesPreview(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewMOM(MeetingId, CustomerId);
            if (model != null)
            {
                model.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                //if (model.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || model.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                //{
                model.lstOtherParticipantAttendance = objIMainMeeting.GetOtherParticipentAttendence(MeetingId);
                //}
                model.meetingMinutesDetails = objIMeeting_Service.GetMinutesDetails(MeetingId);
            }
            return PartialView("_Viewer_Minutes", model);
        }

        public ActionResult DownloadMinutesDocumnet(long MeetingId)
        {
            try
            {
                #region Minutes Generation Telerik
                //var fileName = GenerateMinutesDocument(MeetingId, 0);
                //return File(fileName, "application/force-download", Path.GetFileName(fileName));
                #endregion

                #region Minutes Generation Syncfusion
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                var fileName = GenerateMinutesDocument(MeetingId, 0);
                var bytes = objIDocument_Service.GetMinutesDocumentData(MeetingId, 0, userID, customerId, false, false, string.Empty);
                return File(bytes, "application/force-download", fileName);
                #endregion
            }
            catch (Exception e)
            {
                return PartialView("_FileNotFound");
            }
        }

        public string GenerateMinutesDocument(long MeetingId, long NoticeLogId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, MeetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + MeetingId;
                string str = "Minutes_" + meeting.TypeName + "_" + meeting.MeetingTypeName + "_[" + meeting.MeetingSrNo + "]_" + meeting.FY_CY + ".docx";
                #region Old Code
                //string fileName = Path.Combine(Server.MapPath(path), str);

                //if (NoticeLogId > 0)
                //{
                //    fileName = Path.Combine(Server.MapPath(path), "Minutes_" + NoticeLogId + ".docx");
                //}

                //bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                //if (!exists)
                //    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                //using (Stream output = System.IO.File.OpenWrite(fileName))
                //{
                //    DocxFormatProvider provider = new DocxFormatProvider();
                //    RadFlowDocument document = CreateMinutesDocument(MeetingId, customerId, string.Empty);

                //    provider.Export(document, output);
                //}

                //return fileName;
                #endregion
                return str;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public RadFlowDocument CreateMinutesDocument(long meetingId, int customerId, string generateFor)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            try
            {
                var agenda = objIMeeting_Service.PreviewMOM(meetingId, customerId);
                var agendaItems = agenda.lstAgendaItems;
                if (agenda != null)
                {
                    agenda.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(meetingId, customerId, null);
                    //if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                    //{
                    agenda.lstOtherParticipantAttendance = objIMainMeeting.GetOtherParticipentAttendence(meetingId);
                    //}
                }


                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 


                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                Telerik.Windows.Documents.Flow.Model.Styles.Border b = new Telerik.Windows.Documents.Flow.Model.Styles.Border(1, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                var h = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(1));
                Telerik.Windows.Documents.Flow.Model.Styles.Border b_other = new Telerik.Windows.Documents.Flow.Model.Styles.Border(0, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.None, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(0, 0, 0)), false, false, 0);
                h.ParagraphProperties.Borders.LocalValue = new ParagraphBorders(b_other, b_other, b_other, b);

                h.CharacterProperties.FontSize.LocalValue = 15;
                h.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                h.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;



                var h2 = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(2));
                h2.CharacterProperties.FontSize.LocalValue = 15;
                h2.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                h2.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;

                if (agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.AGM && agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.EGM)
                {
                    #region Document header

                    var section = editor.InsertSection();
                    var paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Left;
                    editor.MoveToParagraphStart(paragraph);

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Justified;

                    #region Table
                    var meetingSrNo = "";
                    if (agenda.MeetingSrNo.HasValue)
                    {
                        var srNo = Convert.ToString(agenda.MeetingSrNo);
                        switch (srNo)
                        {
                            case "1":
                                meetingSrNo = srNo + "st";
                                break;
                            case "2":
                                meetingSrNo = srNo + "nd";
                                break;
                            case "3":
                                meetingSrNo = srNo + "rd";
                                break;
                            default:
                                meetingSrNo = srNo + "th";
                                break;
                        }
                    }

                    string mDate = string.Empty;
                    string mDay = string.Empty;
                    string mTime = string.Empty;
                    string mVenue = string.Empty;

                    if (agenda.MeetingDate != null)
                    {
                        var date = Convert.ToDateTime(agenda.MeetingDate);
                        mDate = date.ToString("MMMM dd, yyyy");
                        mDay = date.ToString("dddd");
                    }
                    if (!string.IsNullOrEmpty(agenda.MeetingTime))
                    {
                        mTime = agenda.MeetingTime;
                    }
                    if (!string.IsNullOrEmpty(agenda.MeetingVenue))
                    {
                        mVenue = agenda.MeetingVenue;
                    }

                    #endregion

                    HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                    var insertOptions = new InsertDocumentOptions
                    {
                        ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                    };

                    var meetingTitle = "<h1>" + "MINUTES OF THE " + meetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "") + " MEETING OF THE " + agenda.MeetingTypeName.ToUpper() + " OF " + agenda.EntityName.ToUpper() + " HELD ON " + mDate.ToUpper() + ", AT " + mTime.ToUpper() + " AT " + mVenue.ToUpper() + "</h1>";
                    RadFlowDocument htmlDocument = htmlProvider.Import(meetingTitle);
                    editor.InsertDocument(htmlDocument, insertOptions);


                    var run1 = editor.InsertLine("");
                    //run1.FontSize = 12;

                    if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER)).Count() > 0)
                    {
                        run1 = editor.InsertLine("PRESENT");
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 15;

                        foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER)))
                        {
                            var run2 = editor.InsertLine(item.MeetingParticipant_Name + "   " + item.Designation);
                            run2.FontWeight = System.Windows.FontWeights.Bold;
                            run2.FontSize = 15;
                        }

                        run1 = editor.InsertLine("");
                        //run1.FontSize = 12;

                        if (agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.INVITEE).Count() > 0)
                        {
                            run1 = editor.InsertLine("Special Invitees");
                            run1.FontWeight = System.Windows.FontWeights.Bold;
                            run1.FontSize = 15;

                            foreach (var item in agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.INVITEE))
                            {
                                var run2 = editor.InsertLine(item.MeetingParticipant_Name + "  " + item.Designation);
                                run2.FontSize = 15;
                            }
                            run1 = editor.InsertLine("");
                        }

                        var run21 = editor.InsertLine("The meeting commenced at " + agenda.MeetingStartTime);
                        run21.FontSize = 14;

                        run1 = editor.InsertLine("");
                        //run1.FontSize = 12;
                        var runCIN = editor.InsertLine("Chairman");
                        runCIN.FontWeight = System.Windows.FontWeights.Bold;
                        runCIN.FontSize = 14;

                        run1 = editor.InsertLine("");
                        //run1.FontSize = 12;

                        if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && k.Position == 1 && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR).Count() > 0)
                        {
                            var chairman = agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && k.Position == 1).FirstOrDefault();
                            runCIN = editor.InsertLine(chairman.MeetingParticipant_Name + " presided as the Chairman of the meeting.");
                            runCIN.FontSize = 14;
                        }
                    }
                    //paragraph = section.Blocks.AddParagraph();
                    //paragraph.TextAlignment = Alignment.Center;

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 6;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;

                    #endregion

                    bool anyOtherBusinessFlag = false;
                    if (agendaItems != null)
                    {
                        var SrNo = 0;
                        foreach (var item in agendaItems)
                        {
                            if (!string.IsNullOrEmpty(item.AgendaItemText))
                            {
                                SrNo++;

                                if (item.PartId == 3)
                                {
                                    if (anyOtherBusinessFlag == false)
                                    {
                                        var anyOtherHeading = "<h2>Any other business item</h2>";

                                        paragraph = section.Blocks.AddParagraph();
                                        editor.MoveToParagraphStart(paragraph);

                                        htmlDocument = htmlProvider.Import(anyOtherHeading);
                                        editor.InsertDocument(htmlDocument, insertOptions);


                                        var anyOtherText = "<p>The Chairman informed the board with the consultation of majority of members present at the meeting to consider the following agenda</p>";
                                        htmlDocument = htmlProvider.Import(anyOtherText);

                                        editor.InsertDocument(htmlDocument, insertOptions);

                                        anyOtherBusinessFlag = true;
                                    }
                                }
                                string AgendaItem = "<h2>" + SrNo + ". " + item.AgendaItemText + "</h2>";
                                paragraph = section.Blocks.AddParagraph();

                                editor.MoveToParagraphStart(paragraph);

                                htmlDocument = htmlProvider.Import(AgendaItem);
                                editor.InsertDocument(htmlDocument, insertOptions);

                                AgendaItem = string.IsNullOrEmpty(item.AgendaFormat) ? "" : item.AgendaFormat;
                                htmlDocument = htmlProvider.Import(AgendaItem);

                                editor.InsertDocument(htmlDocument, insertOptions);
                            }
                        }
                    }

                    var voteForThanks = "<h2>Votes of Thanks</h2>";
                    paragraph = section.Blocks.AddParagraph();

                    editor.MoveToParagraphStart(paragraph);

                    htmlDocument = htmlProvider.Import(voteForThanks);
                    editor.InsertDocument(htmlDocument, insertOptions);

                    var voteForThanksBody = "<p>There being no other matter to be transacted, the meeting concluded at " + agenda.MeetingEndTime + ". with a vote of thanks to the Chair.</p>";
                    htmlDocument = htmlProvider.Import(voteForThanksBody);

                    htmlDocument = htmlProvider.Import(voteForThanksBody);
                    editor.InsertDocument(htmlDocument, insertOptions);

                    var signTable = "<p>" +
                        "<table style ='width:100%'>" +
                             "<tr>" +
                                 "<td style = 'width:50%' > Place </td>" +
                                  "<td style = 'width:50%' ><b> Chairman </b></td>" +
                               "</tr>" +
                               "<tr>" +
                                   "<td> Date of entering minutes </td>" +
                                      "<td> Date of signing minutes </td> " +
                                     "</tr > " +
                                 "</table> " +
                        "</p>";

                    if (generateFor != "Draft Minutes")
                    {
                        signTable = "<p>" +
                        "<table style ='width:100%'>";
                        var minutesDetails = objIMeeting_Service.GetMinutesDetails(meetingId);

                        if (minutesDetails != null)
                        {
                            if (!string.IsNullOrEmpty(minutesDetails.Place))
                            {
                                signTable += "<tr>" +
                                 "<td style = 'width:50%' > Place : " + minutesDetails.Place + "</td>" +
                                  "<td style = 'width:50%' ><b> Chairman </b></td>" +
                               "</tr>";
                            }
                            else
                            {
                                signTable += "<tr>" +
                                "<td style = 'width:50%' > Place </td>" +
                                 "<td style = 'width:50%' ><b> Chairman </b></td>" +
                              "</tr>";
                            }
                            signTable += "<tr>";
                            if (minutesDetails.DateOfEntering != null)
                            {
                                signTable += "<td> Date of entering minutes : " + Convert.ToDateTime(minutesDetails.DateOfEntering).ToString("MMM dd, yyyy") + "</td>";
                            }
                            else
                            {
                                signTable += "<td> Date of entering minutes </td> ";
                            }

                            if (minutesDetails.DateOfSigning != null)
                            {
                                signTable += "<td> Date of signing minutes : " + Convert.ToDateTime(minutesDetails.DateOfSigning).ToString("MMM dd, yyyy") + "</td>";
                            }
                            else
                            {
                                signTable += "<td> Date of signing minutes </td> ";
                            }

                            signTable += "</tr>";
                        }
                        else
                        {
                            signTable += "<tr>" +
                                 "<td style = 'width:50%' > Place </td>" +
                                  "<td style = 'width:50%' ><b> Chairman </b></td>" +
                               "</tr>" +
                               "<tr>" +
                                   "<td> Date of entering minutes </td>" +
                                      "<td> Date of signing minutes </td>" +
                                     "</tr> ";
                        }
                        signTable += "</table></p>";
                    }

                    htmlDocument = htmlProvider.Import(signTable);
                    editor.InsertDocument(htmlDocument, insertOptions);
                }
                else
                {
                    var meetingSrNo = "";
                    var meetingTypeName = "";
                    var chairman = new MeetingAttendance_VM();

                    if (agenda.MeetingSrNo.HasValue)
                    {
                        var srNo = Convert.ToString(agenda.MeetingSrNo);
                        switch (srNo)
                        {
                            case "1":
                                meetingSrNo = srNo + "st";
                                break;
                            case "2":
                                meetingSrNo = srNo + "nd";
                                break;
                            case "3":
                                meetingSrNo = srNo + "rd";
                                break;
                            default:
                                meetingSrNo = srNo + "th";
                                break;
                        }
                    }

                    #region Document header

                    var section = editor.InsertSection();
                    var paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Left;
                    editor.MoveToParagraphStart(paragraph);

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Justified;

                    #region Table
                    string mDate = string.Empty;
                    string mDay = string.Empty;
                    string mTime = string.Empty;
                    string mVenue = string.Empty;

                    if (agenda.MeetingDate != null)
                    {
                        var date = Convert.ToDateTime(agenda.MeetingDate);
                        mDate = date.ToString("MMMM dd, yyyy");
                        mDay = date.ToString("dddd");
                    }
                    if (!string.IsNullOrEmpty(agenda.MeetingTime))
                    {
                        mTime = agenda.MeetingTime;
                    }
                    if (!string.IsNullOrEmpty(agenda.MeetingVenue))
                    {
                        mVenue = agenda.MeetingVenue;
                    }

                    #endregion

                    HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                    var insertOptions = new InsertDocumentOptions
                    {
                        ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                    };

                    var meetingTitle = "<h1>" + "MINUTES OF THE " + meetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "") + " " + agenda.MeetingTypeName.ToUpper() + " OF THE MEMBERS OF " + agenda.EntityName.ToUpper() + " HELD ON " + mDate.ToUpper() + ", AT " + mTime.ToUpper() + " AT " + mVenue.ToUpper() + "</h1>";
                    RadFlowDocument htmlDocument = htmlProvider.Import(meetingTitle);
                    editor.InsertDocument(htmlDocument, insertOptions);

                    var run1 = editor.InsertLine("");
                    #endregion

                    #region Attendance
                    var isLeave = false;
                    if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "P").Count() > 0)
                    {
                        chairman = agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && k.Position == 1 && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR).FirstOrDefault();

                        run1 = editor.InsertLine("PRESENT");
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 15;

                        run1 = editor.InsertLine(" " + (agenda.TotalMemberPresentInAGM != null ? Convert.ToString(agenda.TotalMemberPresentInAGM) : "{{TOTAL_NUMBER_OF_MEMBERS_PRESENT}}") + " members were present in person or by proxy.");
                        run1.FontSize = 15;

                        run1 = editor.InsertLine("The following directors of the company were also present");
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 15;

                        foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR))
                        {
                            var run2 = editor.InsertLine(item.MeetingParticipant_Name + "   " + item.Designation);
                            run2.FontSize = 15;
                        }

                        run1 = editor.InsertLine("");

                        if (agenda.lstOtherParticipantAttendance != null)
                        {
                            if (agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP).Count() > 0)
                            {
                                run1 = editor.InsertLine("Officers in Presence");
                                run1.FontWeight = System.Windows.FontWeights.Bold;
                                run1.FontSize = 15;

                                foreach (var item in agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP))
                                {
                                    var run2 = editor.InsertLine(item.MeetingParticipant_Name + "   " + item.Designation);
                                    run2.FontSize = 15;
                                }
                            }

                            if (agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.AUDITOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.INVITEE)).Count() > 0)
                            {
                                run1 = editor.InsertLine("Special Invitees");
                                run1.FontWeight = System.Windows.FontWeights.Bold;
                                run1.FontSize = 15;

                                foreach (var item in agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.AUDITOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.INVITEE)))
                                {
                                    if (string.IsNullOrEmpty(item.Representative))
                                    {
                                        var run2 = editor.InsertLine(item.MeetingParticipant_Name + "  " + item.Designation);
                                        run2.FontSize = 15;
                                    }
                                    else
                                    {
                                        var run2 = editor.InsertLine(item.Representative + " representing " + item.MeetingParticipant_Name + ", the Company’s " + item.Designation);
                                        run2.FontSize = 15;
                                    }
                                }
                            }

                            if (agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "A" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP).Count() > 0)
                            {
                                isLeave = true;
                            }
                        }

                        #region Leave of absens
                        if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "A" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR).Count() > 0)
                        {
                            isLeave = true;
                        }

                        if (isLeave)
                        {
                            run1 = editor.InsertLine("Leave of Absences");
                            run1.FontWeight = System.Windows.FontWeights.Bold;
                            run1.FontSize = 15;

                            run1 = editor.InsertLine("");

                            run1 = editor.InsertLine("Leave of absence was granted to");
                            run1.FontSize = 15;

                            foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "A" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR))
                            {
                                var run2 = editor.InsertLine(item.MeetingParticipant_Name + "   " + item.Designation);
                                run2.FontSize = 15;
                            }

                            if (agenda.lstOtherParticipantAttendance != null)
                            {
                                foreach (var item in agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "A" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP))
                                {
                                    var run2 = editor.InsertLine(item.MeetingParticipant_Name + "   " + item.Designation);
                                    run2.FontSize = 15;
                                }
                            }
                        }
                        #endregion

                        if (chairman != null)
                        {
                            run1 = editor.InsertLine("CHAIRMAN");
                            run1.FontWeight = System.Windows.FontWeights.Bold;
                            run1.FontSize = 15;

                            run1 = editor.InsertLine(chairman.PartnerName + ", in his capacity as the chairman of the board of directors of the company, occupied the chair and presided over the meeting.");
                            run1.FontWeight = System.Windows.FontWeights.Bold;
                            run1.FontSize = 15;

                            run1 = editor.InsertLine("");
                        }
                    }
                    #endregion

                    #region Register. Documents & Report
                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Left;
                    editor.MoveToParagraphStart(paragraph);

                    run1 = editor.InsertLine("REGISTER, DOCUMENTS, REPORTS ");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 15;

                    run1 = editor.InsertLine("");

                    run1 = editor.InsertLine("1. The Chairman informed the Members that the following documents and registers as required under the Companies Act, 2013 and other applicable laws were open for inspection by the Members at the Meeting:");
                    run1.FontSize = 14;

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("2. Notice convening the " + meetingSrNo + " " + meetingTypeName);
                    run1.FontSize = 14;

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("3. Directors’ Report of the company for the financial year " + agenda.FYText + " along with its annexures. ");
                    run1.FontSize = 14;

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("4. Audited Financial Statements (Standalone) " + (agenda.HasConsolidatedFinancial ? "and Audited Consolidated Financial Statements " : "") + "for the financial year " + agenda.FYText + " along with respective Auditor’s Reports. ");
                    run1.FontSize = 14;

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("5. Secretarial Audit Report for the financial year " + agenda.FYText);
                    run1.FontSize = 14;

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("6. The Register of Proxies with the proxies lodged with the Company in connection with " + meetingSrNo + " " + agenda.MeetingTypeName);
                    run1.FontSize = 14;

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("7. Register of Directors and Key Managerial Personnel and their Shareholding.");
                    run1.FontSize = 14;

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("8. Register of Contracts or Arrangements in which the Directors were interested.");
                    run1.FontSize = 14;
                    #endregion

                    #region  Quorum
                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Left;
                    editor.MoveToParagraphStart(paragraph);

                    run1 = editor.InsertLine("Quorum");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 14;

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("At " + agenda.MeetingStartTime + ", the Chairman welcomed the Members present at the venue of the AGM. Thereafter, the Chairman announced that the requisite quorum was present and called the Meeting to order. ");

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("The Chairman requested the members present to take the notice convening the AGM, the auditor’s Report and the annexure thereto as read, to which the members agreed. As there were, no qualifications in the auditor’s report as also in Secretarial Audit report, the same were not required to read.");

                    run1 = editor.InsertLine("");
                    run1 = editor.InsertLine("With the permission of the members, the Chairman then commenced reading out the summary of the resolution to provide sufficient opportunity to the members to propose and second the same and vote on them. He drew attention of members to the Explanatory Statement forming part of Notice convening the " + agenda.MeetingTypeName + ", which explained the objectives and implications of the resolutions.");
                    #endregion

                    #region Agenda Items
                    if (agendaItems != null)
                    {
                        var SrNo = 0;
                        foreach (var item in agendaItems)
                        {
                            if (!string.IsNullOrEmpty(item.AgendaItemText))
                            {
                                SrNo++;
                                paragraph = section.Blocks.AddParagraph();
                                editor.MoveToParagraphStart(paragraph);

                                string AgendaItem = "ITEM NO. <strong>" + SrNo + "</strong>";
                                htmlDocument = htmlProvider.Import(AgendaItem);
                                editor.InsertDocument(htmlDocument, insertOptions);

                                AgendaItem = "<h2>" + item.AgendaItemText + "</h2>";
                                htmlDocument = htmlProvider.Import(AgendaItem);
                                editor.InsertDocument(htmlDocument, insertOptions);

                                AgendaItem = string.IsNullOrEmpty(item.AgendaFormat) ? "" : item.AgendaFormat;
                                htmlDocument = htmlProvider.Import(AgendaItem);

                                editor.InsertDocument(htmlDocument, insertOptions);
                            }
                        }
                    }
                    #endregion

                    #region Invited Questions
                    paragraph = section.Blocks.AddParagraph();
                    editor.MoveToParagraphStart(paragraph);

                    var invitedQuestions = "The chairman then invited questions from the members. The following members offered comments/raised queries.";

                    htmlDocument = htmlProvider.Import(invitedQuestions);
                    editor.InsertDocument(htmlDocument, insertOptions);

                    var invitedQuestionsBody = "<p>The Chairman thanked the members for all questions and concerned raised.</p>";
                    htmlDocument = htmlProvider.Import(invitedQuestionsBody);
                    editor.InsertDocument(htmlDocument, insertOptions);

                    invitedQuestionsBody = "<p>The chairman then authorized the company secretary top carry out the voting process.";
                    if (agenda.IsEVoting)
                    {
                        invitedQuestionsBody += "<br/>He further stated that the combined results of the remote e-voting and the voting at the meeting would be announced by Company Secretary and the same would be displayed on the Company’s website, the website of NSDL/CDSL, will be uploaded on the stock exchanges in within two days.";
                    }
                    invitedQuestionsBody += "</p>";
                    htmlDocument = htmlProvider.Import(invitedQuestionsBody);
                    editor.InsertDocument(htmlDocument, insertOptions);


                    invitedQuestionsBody = "<p>The Chairman thanked the members and extended his good wishes for supporting the company. </p>";
                    htmlDocument = htmlProvider.Import(invitedQuestionsBody);
                    editor.InsertDocument(htmlDocument, insertOptions);

                    invitedQuestionsBody = "<p>On behalf of the members, one of them proposed a vote of thanks to the chair.</p>";
                    htmlDocument = htmlProvider.Import(invitedQuestionsBody);
                    editor.InsertDocument(htmlDocument, insertOptions);
                    #endregion

                    #region Sign table
                    var signTable = "<p>" +
                        "<table style ='width:100%'>" +
                             "<tr>" +
                                 "<td style = 'width:50%' > DATE </td>" +
                                  "<td style = 'width:50%' ></td>" +
                               "</tr>" +
                               "<tr>" +
                                    "<td> " + (chairman != null ? chairman.MeetingParticipant_Name : "{{NAME_OF_CHAIRMAN}}") + " </td> " +
                                    "<td> &nbsp; </td>" +
                                "</tr> " +
                                "<tr>" +
                                    "<td> PLACE </td> " +
                                    "<td> Chairman </td>" +
                                "</tr> " +
                                 "</table> " +
                        "</p>";

                    if (generateFor != "Draft Minutes")
                    {
                        var minutesDetails = objIMeeting_Service.GetMinutesDetails(meetingId);

                        if (minutesDetails != null)
                        {
                            signTable = "<p>" +
                            "<table style ='width:100%'>" +
                                 "<tr>" +
                                     "<td style = 'width:50%' > DATE : " + (minutesDetails.DateOfSigning != null ? Convert.ToDateTime(minutesDetails.DateOfSigning).ToString("dd/MM/yyyy") : "") + "</td>" +
                                      "<td style = 'width:50%' ></td>" +
                                   "</tr>" +
                                   "<tr>" +
                                        "<td> " + (chairman != null ? chairman.MeetingParticipant_Name : "{{NAME_OF_CHAIRMAN}}") + " </td> " +
                                        "<td> &nbsp; </td>" +
                                    "</tr> " +
                                    "<tr>" +
                                        "<td> PLACE : " + (minutesDetails.Place != null ? minutesDetails.Place : "") + "</td> " +
                                        "<td> Chairman </td>" +
                                    "</tr> " +
                                     "</table> " +
                            "</p>";
                        }
                    }

                    htmlDocument = htmlProvider.Import(signTable);
                    editor.InsertDocument(htmlDocument, insertOptions);
                    #endregion
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }

        public ActionResult DownloadProceedingOfMeetingDocument(long MeetingId)
        {
            try
            {
                var fileName = GenerateProceedingOfMeetingDocument(MeetingId, 0);
                return File(fileName, "application/force-download", Path.GetFileName(fileName));
            }
            catch (Exception e)
            {
                return PartialView("_FileNotFound");
            }
        }
        public string GenerateProceedingOfMeetingDocument(long MeetingId, long NoticeLogId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, MeetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + MeetingId;
                //string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + MeetingId;
                string str = "ProceedingOfMeeting_" + meeting.TypeName + "_" + meeting.MeetingTypeName + "_[" + meeting.MeetingSrNo + "]_" + meeting.FY_CY + ".docx";
                string fileName = Path.Combine(Server.MapPath(path), str);

                if (NoticeLogId > 0)
                {
                    fileName = Path.Combine(Server.MapPath(path), "ProceedingOfMeeting_" + NoticeLogId + ".docx");
                }

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                using (Stream output = System.IO.File.OpenWrite(fileName))
                {
                    DocxFormatProvider provider = new DocxFormatProvider();
                    RadFlowDocument document = CreateProceedingOfMeetingDocument(MeetingId, customerId, string.Empty);

                    provider.Export(document, output);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public RadFlowDocument CreateProceedingOfMeetingDocument(long meetingId, int customerId, string generateFor)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            try
            {
                var agenda = objIMeeting_Service.PreviewAgenda(meetingId, customerId);
                var agendaItems = agenda.lstAgendaItems;
                if (agenda != null)
                {
                    agenda.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(meetingId, customerId, null);
                }

                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 

                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                Telerik.Windows.Documents.Flow.Model.Styles.Border b = new Telerik.Windows.Documents.Flow.Model.Styles.Border(1, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                var h = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(1));
                Telerik.Windows.Documents.Flow.Model.Styles.Border b_other = new Telerik.Windows.Documents.Flow.Model.Styles.Border(0, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.None, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(0, 0, 0)), false, false, 0);
                h.ParagraphProperties.Borders.LocalValue = new ParagraphBorders(b_other, b_other, b_other, b);

                h.CharacterProperties.FontSize.LocalValue = 15;
                h.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                h.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;

                var h2 = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(2));
                h2.CharacterProperties.FontSize.LocalValue = 15;
                h2.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                h2.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;

                #region Document header

                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Center;
                editor.MoveToParagraphStart(paragraph);

                editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                var run1 = editor.InsertLine(agenda.EntityName);
                run1.FontWeight = System.Windows.FontWeights.Bold;
                run1.FontSize = 24;

                var run2 = editor.InsertLine(agenda.EntityAddressLine1);
                run2.FontWeight = System.Windows.FontWeights.Bold;
                run2.FontSize = 14;

                var run21 = editor.InsertLine(agenda.EntityAddressLine2);
                run21.FontWeight = System.Windows.FontWeights.Bold;
                run21.FontSize = 14;

                var runCIN = editor.InsertLine(agenda.EntityCIN_LLPIN);
                run21.FontWeight = System.Windows.FontWeights.Bold;
                run21.FontSize = 12;

                paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Left;

                editor.InsertLine("");

                editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Justified;

                #region Table
                var meetingSrNo = "";
                var meetingTypeName = "";
                if (agenda.MeetingSrNo.HasValue)
                {
                    var srNo = Convert.ToString(agenda.MeetingSrNo);
                    switch (srNo)
                    {
                        case "1":
                            meetingSrNo = srNo + "st";
                            break;
                        case "2":
                            meetingSrNo = srNo + "nd";
                            break;
                        case "3":
                            meetingSrNo = srNo + "rd";
                            break;
                        default:
                            meetingSrNo = srNo + "th";
                            break;
                    }
                }

                if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                {
                    meetingTypeName = "Annual General Meeting";
                }
                else if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    meetingTypeName = "Extra Ordinary General Meeting";
                }

                string nDate = string.Empty;
                string mDate = string.Empty;
                string mDay = string.Empty;
                string mTime = string.Empty;
                string mVenue = string.Empty;

                if (agenda.NoticeDate != null)
                {
                    nDate = Convert.ToDateTime(agenda.NoticeDate).ToString("MMMM dd, yyyy");
                }

                if (agenda.MeetingDate != null)
                {
                    var date = Convert.ToDateTime(agenda.MeetingDate);
                    mDate = date.ToString("MMMM dd, yyyy");
                    mDay = date.ToString("dddd");
                }
                if (!string.IsNullOrEmpty(agenda.MeetingTime))
                {
                    mTime = agenda.MeetingTime;
                }
                if (!string.IsNullOrEmpty(agenda.MeetingVenue))
                {
                    mVenue = agenda.MeetingVenue;
                }

                #endregion

                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                var insertOptions = new InsertDocumentOptions
                {
                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                };

                var meetingTitle = "<h1>" + "SUMMARY OF PROCEEDINGS OF " + meetingSrNo + " " + meetingTypeName + " " + "</h1>";
                RadFlowDocument htmlDocument = htmlProvider.Import(meetingTitle);
                editor.InsertDocument(htmlDocument, insertOptions);


                run1 = editor.InsertLine("");
                var details = "The " + meetingSrNo + " " + agenda.MeetingTypeName.ToUpper() + " of " + agenda.EntityName + " was held on " +
                    mDay + "" + mDay + " at " + mTime + " at " + mVenue;
                var r = editor.InsertLine(details);
                r.FontWeight = System.Windows.FontWeights.Bold;
                r.FontSize = 16;

                editor.InsertLine("");

                r = editor.InsertLine("Time of Commencement " + agenda.MeetingStartTime);
                r.FontWeight = System.Windows.FontWeights.Bold;
                r.FontSize = 14;

                r = editor.InsertLine("Time of Conclusion " + agenda.MeetingEndTime);
                r.FontWeight = System.Windows.FontWeights.Bold;
                r.FontSize = 14;

                editor.InsertLine("");

                if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "P").Count() > 0)
                {
                    run1 = editor.InsertLine("Directors");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 15;

                    foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "P"))
                    {
                        run2 = editor.InsertLine(item.MeetingParticipant_Name + "   " + item.Designation);
                        run2.FontSize = 15;
                    }

                    run1 = editor.InsertLine("");
                }

                if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "A").Count() > 0)
                {
                    run1 = editor.InsertLine("Leave of Absences");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 15;

                    run1 = editor.InsertLine("Leave of absence was granted to");
                    run1.FontSize = 14;

                    foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "A"))
                    {
                        run2 = editor.InsertLine(item.MeetingParticipant_Name + "   " + item.Designation);
                        run2.FontSize = 15;
                    }

                    run1 = editor.InsertLine("");
                }

                var paraStr = (agenda.TotalMemberPresentInAGM == null ? Convert.ToString(agenda.TotalMemberPresentInAGM) : "{{TOTAL_NUMBER_OF_MEMBERS_PRESENT}}") + " members were present in person or by proxy.";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                run1 = editor.InsertLine("");

                if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && k.Position == 1).Count() > 0)
                {
                    var chairman = agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && k.Position == 1).FirstOrDefault();
                    var chairmanPara = chairman.MeetingParticipant_Name + " chaired the meeting. After declaring the quorum to be present, the chairman called the meeting to order. With the consent of the shareholders, the notice convening the " + agenda.MeetingTypeName + " and the auditor’s report were taken as read";
                    run1 = editor.InsertLine(chairmanPara);
                    run1.FontSize = 14;

                    editor.InsertLine("");
                }

                paraStr = "The Chairman then commenced his speech and gave an overview with respect to Company’s performance, financial outlook and company’s governance during the " + agenda.FYText + ".";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                editor.InsertLine("");

                paraStr = "The Chairman informed the members that in compliance with the provisions of the Companies act, 2013 and the Securities and Exchange Board of India (Listing Obligations and Disclosure Requirements) Regulations, 2015, the Company has provided to the members the facility to cast their vote through remote e-voting means. Further, the Chairman informed the members that the facility of e-voting is made available at the venue of the meeting for members who have not cast their vote through the remote e-voting means.";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                editor.InsertLine("");

                paraStr = "The Chairman informed the members, that the Board of Directors appointed {{NAME_OF_THE_SCRUTINISER_OR_FIRM}} as the Scrutinizer to scrutinize the remote e-voting process as well as the e-voting to be conducted at the venue of the " + agenda.MeetingTypeName + ".";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                editor.InsertLine("");

                paraStr = "The Chairman concluded his speech by placing on record his appreciation and gratitude for all the stakeholders for having reposed their trust and confidence in the Company.";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                editor.InsertLine("");

                paraStr = "The Chairman informed the members with respect to the business to be transacted at the meeting. as per the notice dated " + nDate + " convening the " + meetingSrNo + " " + meetingTypeName + " of the company, the following business was transacted at the meeting.";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                paraStr = "";
                run1 = editor.InsertLine(paraStr);
                //run1.FontSize = 14;

                #region Agenda table
                int ordinaryCount = 0, specialCount = 0;
                if (agendaItems != null)
                {
                    foreach (var item in agendaItems)
                    {
                        if (item.IsOrdinaryBusiness)
                        {
                            ordinaryCount++;
                        }
                        else
                        {
                            specialCount++;
                        }
                    }
                }

                if (ordinaryCount > 0)
                {
                    editor.InsertLine("");

                    run1 = editor.InsertLine("Ordinary Business");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 14;

                    var table = editor.InsertTable((ordinaryCount + 1), 3);
                    table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                    table.Alignment = Alignment.Center;

                    table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 15);
                    table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 65);
                    table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 20);

                    Telerik.Windows.Documents.Flow.Model.Styles.Border tableBorderAll = new Telerik.Windows.Documents.Flow.Model.Styles.Border(0.5, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                    table.Borders = new TableBorders(tableBorderAll);

                    var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("Resolution No.");
                    cellRun.FontWeight = System.Windows.FontWeights.Bold;

                    cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun("Resolutions");
                    cellRun.FontWeight = System.Windows.FontWeights.Bold;

                    cellRun = table.Rows[0].Cells[2].Blocks.AddParagraph().Inlines.AddRun("Type of Resolution");
                    cellRun.FontWeight = System.Windows.FontWeights.Bold;

                    int rowCount = 0, SrNo = 0;
                    foreach (var item in agendaItems)
                    {
                        if (item.IsOrdinaryBusiness)
                        {
                            SrNo++;
                            rowCount++;

                            table.Rows[rowCount].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 15);
                            table.Rows[rowCount].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 65);
                            table.Rows[rowCount].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 20);

                            cellRun = table.Rows[rowCount].Cells[0].Blocks.AddParagraph().Inlines.AddRun(Convert.ToString(SrNo));
                            cellRun = table.Rows[rowCount].Cells[1].Blocks.AddParagraph().Inlines.AddRun(item.AgendaItemText);
                            cellRun = table.Rows[rowCount].Cells[2].Blocks.AddParagraph().Inlines.AddRun((item.IsSpecialResolution ? "Special Resolution" : "Ordinary Resolution"));
                        }
                    }

                }

                if (specialCount > 0)
                {
                    editor.InsertLine("");

                    run1 = editor.InsertLine("Special Business");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 14;

                    var table = editor.InsertTable((specialCount + 1), 3);
                    table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                    table.Alignment = Alignment.Center;

                    table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 15);
                    table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 65);
                    table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 20);

                    Telerik.Windows.Documents.Flow.Model.Styles.Border tableBorderAll = new Telerik.Windows.Documents.Flow.Model.Styles.Border(0.5, Telerik.Windows.Documents.Flow.Model.Styles.BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                    table.Borders = new TableBorders(tableBorderAll);

                    var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("Resolution No.");
                    cellRun.FontWeight = System.Windows.FontWeights.Bold;

                    cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun("Resolutions");
                    cellRun.FontWeight = System.Windows.FontWeights.Bold;

                    cellRun = table.Rows[0].Cells[2].Blocks.AddParagraph().Inlines.AddRun("Type of Resolution");
                    cellRun.FontWeight = System.Windows.FontWeights.Bold;

                    int rowCount = 0, SrNo = 0;
                    foreach (var item in agendaItems)
                    {
                        if (!item.IsOrdinaryBusiness)
                        {
                            SrNo++;
                            rowCount++;
                            table.Rows[rowCount].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 15);
                            table.Rows[rowCount].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 65);
                            table.Rows[rowCount].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 20);

                            cellRun = table.Rows[rowCount].Cells[0].Blocks.AddParagraph().Inlines.AddRun(Convert.ToString(SrNo));
                            cellRun = table.Rows[rowCount].Cells[1].Blocks.AddParagraph().Inlines.AddRun(item.AgendaItemText);
                            cellRun = table.Rows[rowCount].Cells[2].Blocks.AddParagraph().Inlines.AddRun((item.IsSpecialResolution ? "Special Resolution" : "Ordinary Resolution"));
                        }
                    }

                }
                #endregion

                editor.InsertLine("");
                paraStr = "Members present were given opportunity to ask questions and seek clarification(s). The Chairman appropriately responded to the questions raised.";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                editor.InsertLine("");
                paraStr = "Post the question and answer session the chairman thanked all the Board members, Members and Invitees present at the meeting and then conducted the meeting by authorising the Company Secretary to carry out the voting process and declare the voting results. He informed the members that the voting results will be made available on the websites of the company within 48 hours of the conclusion of the meeting.";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                editor.InsertLine("");
                paraStr = "This is for your information and records.";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                editor.InsertLine("");
                paraStr = "Yours faithfully";
                run1 = editor.InsertLine(paraStr);
                run1.FontSize = 14;

                editor.InsertLine("");
                run1 = editor.InsertLine(agenda.EntityName);
                run1.FontWeight = System.Windows.FontWeights.Bold;
                run1.FontSize = 14;

                paraStr = "";
                run1 = editor.InsertLine(paraStr);

                paraStr = "";
                run1 = editor.InsertLine(paraStr);


                run1 = editor.InsertLine("{{NAME_OF_SIGNING_AUTHORITY}}");
                run1.FontWeight = System.Windows.FontWeights.Bold;
                run1.FontSize = 14;

                run1 = editor.InsertLine("({{DESIGNATION_OF_SIGNING_AUTHORITY}})");
                run1.FontWeight = System.Windows.FontWeights.Bold;
                run1.FontSize = 14;

                run1 = editor.InsertLine("DIN: {{DIN_OF_SIGNING_AUTHORITY}}");
                run1.FontWeight = System.Windows.FontWeights.Bold;
                run1.FontSize = 14;

                editor.ParagraphFormatting.SpacingAfter.LocalValue = 6;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;

                #endregion

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }

        public FileDataVM NewDraftMinutesFileData(long meetingId, string generateFor)
        {
            var result = new FileDataVM();
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, meetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + meetingId;
                string fileName = generateFor + "-" + meeting.MeetingTypeName + " " + meeting.TypeName + " [ " + meeting.MeetingSrNo + " ] " + meeting.FY_CY + ".docx";

                #region Telerik
                //DocxFormatProvider provider = new DocxFormatProvider();
                //RadFlowDocument document = CreateMinutesDocument(meetingId, customerId, generateFor);
                //Byte[] bytes = provider.Export(document);
                #endregion

                #region Syncfusion
                Byte[] bytes = objIDocument_Service.GetMinutesDocumentData(meetingId, 0, userID, customerId, true, false, generateFor);
                #endregion

                result.FileName = fileName;
                result.FilePath = path;
                result.FileData = bytes;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public PartialViewResult Edit_Minutes(long MeetingId, long MappingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.GetMOM(MeetingId, MappingId, CustomerId);
            return PartialView("_MOM_Edit", model);
        }

        public PartialViewResult View_Minutes(long MeetingId, long MappingId)
        {
            var model = objIMeeting_Service.PreviewMOM(MappingId);
            return PartialView("_MOM_View", model);
        }
        [HttpPost]
        public PartialViewResult Save_Minutes(MeetingAgendaMappingMOMVM model)
        {
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                model = objIMeeting_Service.SaveMOM(model, UserID);
                ModelState.Clear();
            }
            else
            {
                model.Error = true;
                model.Message = "Validation Fails.";
            }

            return PartialView("_MOM_Edit", model);
        }

        [HttpPost]
        public JsonResult Get_Minutes(long MeetingId, long MappingId)
        {
            return Json(new { result = objIMainMeeting.GetAgendabyIDforMainContent(MappingId) }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetScheduleOnIDForMeetingEvent(long meetingId, string meetingEvent)
        {
            var scheduleOnID = objICompliance_Service.GetScheduleOnIDForMeetingEvent(meetingId, meetingEvent);
            return Json(new { result = scheduleOnID }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Minutes Details (date of entering etc)
        public PartialViewResult GetMinutesDetails(long meetingId)
        {
            var model = objIMeeting_Service.GetMinutesDetails(meetingId);

            if (model == null)
            {
                model = new MeetingMinutesDetailsVM() { MOM_MeetingId = meetingId };
            }
            return PartialView("_MOM_Details", model);
        }
        [HttpPost]
        public PartialViewResult SaveMinutesDetails(MeetingMinutesDetailsVM model, string commandName)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                string mailFormat = "";
                if (commandName == "circulateDraft")
                {
                    model = objIMeeting_Service.CirculateMinutes(model, userID, out mailFormat);

                    if (model.Success == true)
                    {
                        #region Generate Compliance document on circulate Draft
                        //#region Update Schedule on Dates
                        //objIMeeting_Compliances.CalculateScheduleDate(model.MOM_MeetingId, userID);
                        //#endregion

                        var objFileData = NewDraftMinutesFileData(model.MOM_MeetingId, "Draft Minutes");
                        //var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "CirculateDraft");
                        var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "CirculateDraft", true, 0, DateTime.Now, objFileData, userID);

                        #region Commented on 19 Aug 2020
                        //long? complianceScheduleOnID = null, transactionID = null;
                        //if (objFileData != null)
                        //{
                        //    int fileVersion = 0;
                        //    if (lstClosedTransactions != null)
                        //    {
                        //        if (lstClosedTransactions.Count > 0)
                        //        {
                        //            complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                        //            transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                        //            fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                        //        }
                        //    }

                        //    fileVersion++;
                        //    objFileData.Version = Convert.ToString(fileVersion);
                        //    objFileData = objIFileData_Service.Save(objFileData, userID);

                        //    if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                        //    {
                        //        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                        //    }
                        //}
                        #endregion

                        #endregion

                        #region Send Mail
                        var lstParticipants = objIMeeting_Service.GetMeetingParticipants(model.MOM_MeetingId);
                        if (lstParticipants != null && !string.IsNullOrEmpty(mailFormat))
                        {
                            var accessToDirector = objISettingService.GetAccessToDirector(customerID);
                            if (accessToDirector)
                            {
                                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerID, model.MOM_MeetingId);
                                var meetingSrNo = "";
                                switch (meeting.MeetingSrNo)
                                {
                                    case 1:
                                        meetingSrNo = "1st";
                                        break;
                                    case 2:
                                        meetingSrNo = "2nd";
                                        break;
                                    case 3:
                                        meetingSrNo = "3rd";
                                        break;
                                    default:
                                        meetingSrNo = meeting.MeetingSrNo + "th";
                                        break;
                                }
                                var mailSubject = "Draft Minutes of " + meetingSrNo + " " + meeting.MeetingTypeName + " " + meeting.TypeName + " held on " + Convert.ToDateTime(meeting.MeetingDate).ToString("MMMM dd, yyyy dddd") + ".";

                                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                var mailSetting = EmailService.GetMailSettingByEntityId(meeting.Entityt_Id, CustomerId);
                                foreach (var item in lstParticipants)
                                {
                                    string Mailbody = mailFormat.Replace("{{ Name of the Director }}", item.ParticipantName);

                                    #region Attachments
                                    List<Attachment> attachment = new List<Attachment>();

                                    if (objFileData != null)
                                    {
                                        attachment.Add(new Attachment(new MemoryStream(objFileData.FileData), objFileData.FileName));
                                    }
                                    #endregion
                                    EmailManager.SendMail_Secretarial(mailSetting, SenderEmailAddress, new List<String>(new String[] { item.Email }), null, null, mailSubject, Mailbody, attachment);

                                    //if (MeetingNoticeLogID > 0)
                                    //{
                                    //    objIMeeting_Service.CreateNoticeTransaction(MeetingNoticeLogID, item.MeetingParticipantId, DateTime.Now);
                                    //}
                                }
                            }
                        }
                        #endregion
                    }
                }
                else if (commandName == "finalizeDraft")
                {
                    model = objIMeeting_Service.FinalizedMinutes(model, userID);

                    if (model.Success == true)
                    {
                        #region Generate Compliance document on Finalized

                        var objFileData = NewDraftMinutesFileData(model.MOM_MeetingId, "Minutes");

                        //var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "FinalizedMinutes");
                        var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "FinalizedMinutes", false, 0, DateTime.Now, objFileData, userID);

                        #region Commented on 19 Aug 2020
                        long? complianceScheduleOnID = null, transactionID = null;
                        if (objFileData != null)
                        {
                            int fileVersion = 0;
                            if (lstClosedTransactions != null)
                            {
                                if (lstClosedTransactions.Count > 0)
                                {
                                    complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                                    transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                                    fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                                }
                            }

                            fileVersion++;
                            objFileData.Version = Convert.ToString(fileVersion);
                            objFileData = objIFileData_Service.Save(objFileData, userID);

                            if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                            {
                                objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                            }
                        }
                        #endregion

                        #endregion
                    }
                }
                else
                {
                    model = objIMeeting_Service.SaveMinutesDetails(model, userID);
                }
            }
            return PartialView("_MOM_Details", model);
        }
        //Used in virtual meeting
        [HttpPost]
        public PartialViewResult SaveMinutesDetailsVirtual(MeetingMinutesDetailsVM model, string commandName)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                if (commandName == "finalizeDraft")
                {
                    //var objFileDataDraft = NewDraftMinutesFileData(model.MOM_MeetingId, "Draft Minutes");
                    var objFileDataMinutes = NewDraftMinutesFileData(model.MOM_MeetingId, "Minutes");
                    model = objIMeeting_Service.FinalizedMinutesOfVirtualMeeting(model, objFileDataMinutes, userID);
                }
                else
                {
                    model = objIMeeting_Service.SaveMinutesDetails(model, userID);
                }
            }
            return PartialView("_MOM_DetailsVirtual", model);
        }
        //
        #endregion
        public ActionResult Meetings_Read_Participants([DataSourceRequest] DataSourceRequest request, long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return null;//Json(objIMeeting_Service.GetMeetingParticipants(MeetingId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #region Meeting Details
        public PartialViewResult SigningAuthority(long id, int entityId)
        {
            var model = objIMeeting_Service.GetMeetingOtherDetails(id, entityId);
            return PartialView("_SigningAuthority", model);
        }
        [HttpPost]
        public PartialViewResult SaveMeetingOtherDetails(MeetingDetailsVM obj)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.SaveMeetingOtherDetails(obj, userID);
            return PartialView("_SigningAuthority", model);
        }
        #endregion

        #region Notes
        [HttpPost]
        public ActionResult SaveNotes(MeetingNotesVM obj)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            objIMeeting_Service.SaveNotes(obj, userID);
            ModelState.Clear();
            return PartialView("_Notes", obj);
        }
        #endregion

        #region Notice Attachments
        public ActionResult MeetingDocuments(long meetingId, string docs)
        {
            VM_AgendaDocument model = new VM_AgendaDocument()
            {
                FileUpload_MeetingId = meetingId,
                MeetingDocs = docs,
                MeetingAggendaMappingId = 0
            };
            return PartialView("_NoticeAttachments", model);
        }
        [HttpPost]
        public ActionResult UploadMeetingDocuments(VM_AgendaDocument obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var result = objIMeeting_Service.UploadMeetingDocuments(obj, userID, customerID);

                obj.Success = result.Success;
                obj.Error = result.Error;
                obj.Message = result.Message;
            }
            ModelState.Clear();
            return PartialView("_NoticeAttachments_FileUpload", obj);
        }
        #endregion

        #region Meeting Agenda/Minutes review
		public ActionResult AddEditAgendaMinutesTask(long meetingId, int entityId, int taskId, int taskType)
        {
            var model = objITask.GetTaskDetails(meetingId, entityId, taskId, taskType);
            if (taskId == 0 && model != null)
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerID, meetingId);
                model.TaskTitle = meeting.TypeName + "-" + meeting.MeetingTypeName + "-[" + (meeting.MeetingSrNo == null ? "#" : Convert.ToString(meeting.MeetingSrNo)) + "]-" + meeting.FY_CY;
            }
            return PartialView("_TaskAgendaMinutesAddEdit", model);
        }

        public ActionResult TaskDetails(long meetingId, int entityId, int taskType)
        {
            var model = objITask.GetTaskDetails(meetingId, entityId, taskType);
            return PartialView("_TaskAgendaMinutes", model);
        }

        [HttpPost]
        public ActionResult SaveTaskDetails(VMTask obj)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var mailFormat = "";
            obj = objITask.SaveTask(obj, userID, customerID, out mailFormat);
            if (!string.IsNullOrEmpty(mailFormat) && obj.successMessage)
            {
                var accessToDirector = objISettingService.GetAccessToDirector(customerID);
                if (accessToDirector)
                {
                    var mailSubject = "Regarding Review of the Draft Agenda";
                    var lstUser = objITask.GetUserListByTaskId(obj.Id);
                    if (lstUser != null)
                    {
                        var performer = lstUser.Where(k => k.RoleId == SecretarialConst.RoleID.PERFORMER).FirstOrDefault();
                        if (performer != null)
                        {
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            foreach (var item in lstUser.Where(k => k.RoleId == SecretarialConst.RoleID.REVIEWER))
                            {
                                string Mailbody = mailFormat.Replace("{{ Name of the Reviewer }}", item.UserName);
                                Mailbody = Mailbody.Replace("{{ Name of the Performer }}", performer.UserName);
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { item.EmailId }), null, null, mailSubject, Mailbody);
                            }
                        }
                    }
                }
            }
            return PartialView("_TaskAgendaMinutesAddEdit", obj);
        }

        [HttpPost]
        public ActionResult CloseAgendaMinutesReview(long taskId, long meetingId)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var obj = objITask.CloseAgendaMinutesReview(taskId, meetingId, userID);
            return Json(new { result = obj }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DownloadRecordingToLocalFile(int MeetingId)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            string ProviderName = _objvideomeeting.GetProviderName(customerID);
            string obj = "";
            if (!string.IsNullOrEmpty(ProviderName))
            {
                if (ProviderName.ToLower() == "zoom")
                {
                    obj = _objvideomeeting.GetZoomMeetingRecording(MeetingId, customerID);
                }
                else if(ProviderName.ToLower() == "webex")
                {
                   obj = _objvideomeeting.GetCiscoMeetingRecording(MeetingId, customerID);
                }
            }
            //return PartialView("_VideoConfrencingRecordings", MeetingId);
            return Json(new { result = obj }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpGet]
        public ActionResult DownloadFilefromCloude(int FileId)
        {

            var obj = objIFileStorage_Service.GetFileDetails(FileId);

            if (obj != null)
            {
                string fileExtension = Path.GetExtension(obj.FileName);
                string fileName = obj.FileName;
                string path = obj.FilePath;
                string fileNameNew = obj.FileKey + fileExtension;

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference(path);
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileNameNew);
                MemoryStream memStream = new MemoryStream();
                blockBlob.DownloadToStream(memStream);
                return File(memStream.ToArray(), blockBlob.Properties.ContentType.ToString(), fileName);
            }

            return Json("File not found", JsonRequestBehavior.AllowGet);
        }

        public ActionResult RecordingList(int MeetingId)
        {
            return PartialView("_VideoConfrencingRecordings", MeetingId);
        }
        #region Copy Meeting
        public ActionResult CopyMeeting(long meetingId)
        {
            var model = new CopyMeetingVM();
            if(meetingId > 0)
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var result = objIMeeting_Service.GetMeetingForDocumentName(customerID, meetingId);
                if (result != null)
                {
                    model.SourceMeetingID = result.MeetingID;
                    model.EntityTypeId_ = result.EntityTypeId_;
                    model.SourceMeetingEntityID = result.Entityt_Id;
                    model.MeetingTitleTemp = result.TypeName +" - "+ result.MeetingTypeName +" [ "+(result.MeetingSrNo == null ? "#" : result.MeetingSrNo.ToString())+" ] - "+ result.FY_CY;
                }
            }
            return PartialView("_CopyMeeting", model);
        }
        public ActionResult SaveCopyMeeting(CopyMeetingVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            model = objIMeeting_Service.SaveCopyMeetings(model, customerID, userID);
            return PartialView("_CopyMeeting", model);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CopyMeetingCheckComplianceAssigned(CopyMeetingCheckComplianceAssignedVM model)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            string userName = AuthenticationHelper.User;
            var result = objIMeeting_Service.CopyMeetingCheckComplianceAssigned(model, userID, customerID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}