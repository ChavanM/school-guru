﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class TaskController : Controller
    {
        // GET: BM_Management/Task
        ITask obj;

        public TaskController(ITask obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MyTask()
        {
            return View();
        }

        public PartialViewResult MyTaskForDashboard()
        {
            return PartialView("_MyTask");
        }

        public ActionResult AddEditTask(int Id, int? MeetingId, int EntityId, int? AgendaId)
        {
            VMTask vmtask = new VMTask();
            int userId = AuthenticationHelper.UserID;
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            string role = AuthenticationHelper.Role;
            if (Id > 0)
            {
                vmtask = obj.gettaskbyId(Id, userId, customerId, role);
                GettaskTypeDetails(Id);
                GettaskDetails(Id);
                GetTaskFile(Id);
                vmtask.Tasklevel = "T";

                return PartialView(vmtask.View, vmtask);
            }
            else
            {
                if (MeetingId > 0 && EntityId > 0 && AgendaId > 0)
                {
                    vmtask.MeetingType = MeetingId;
                    vmtask.EntityId = EntityId;
                    vmtask.AgendaId = AgendaId;
                    vmtask.TaskType = 2;
                    vmtask.Tasklevel = "M";
                    GettaskTypeDetails(Id);
                    GettaskDetails(Id);
                    GetTaskFile(Id);
                    string getagendaTitle = obj.getAgendatitle(AgendaId);
                    if (getagendaTitle != null)
                    {
                        vmtask.TaskTitle = "ATR-" + getagendaTitle;
                    }
                }
                else
                {
                    GettaskTypeDetails(Id);
                    GettaskDetails(Id);
                    GetTaskFile(Id);
                    vmtask.Tasklevel = "T";
                }
                return PartialView("_AddEditTasknew", vmtask);
            }


        }

        public ActionResult Templates_Save(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath("~/Areas/BM_Management/TempFolder"), fileName);
                    file.SaveAs(physicalPath);
                }
            }
            return Content("");
        }

        public ActionResult Templates_Remove(string[] fileNames)
        {

            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/Areas/BM_Management/TempFolder"), fileName);
                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }
            return Content("");
        }

        [HttpPost]
        public ActionResult CreateUpdateTask(VMTask vmtask)
        {
            if (ModelState.IsValid)
            {
                vmtask.CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                vmtask.Createby = AuthenticationHelper.UserID;
                vmtask.RoleName = AuthenticationHelper.Role;
                if (vmtask.Id > 0)
                {
                    vmtask = obj.UpdateTask(vmtask);
                    GetTaskFile(vmtask.Id);
                    GettaskTypeDetails(Convert.ToInt32(vmtask.Id));
                    GettaskDetails(Convert.ToInt32(vmtask.Id));
                   
                }
                else
                {
                    vmtask = obj.CreateTask(vmtask);
                    GettaskTypeDetails(Convert.ToInt32(vmtask.Id));
                    GettaskDetails(Convert.ToInt32(vmtask.Id));
                    GetTaskFile(Convert.ToInt32(vmtask.Id));
                }
                if (vmtask.Id > 0)
                {
                    GetTaskFile(vmtask.Id);
                    GettaskTypeDetails(Convert.ToInt32(vmtask.Id));
                    GettaskDetails(Convert.ToInt32(vmtask.Id));
                    GetTaskFile(Convert.ToInt32(vmtask.Id));
                }
                return PartialView("_AddEditTasknew", vmtask);

            }
            else
            {
                ModelState.AddModelError("", "Validation error ocured");
                return PartialView("_AddEditTasknew", vmtask);
            }


        }

        [HttpPost]
        public ActionResult CreateUpdateTaskPerformer(VMTask vmtask)
        {
            if (ModelState.IsValid)
            {
                vmtask.CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                vmtask.UserId = AuthenticationHelper.UserID;
                if (vmtask.Role == 3)
                {
                    vmtask.status = "Submitted";
                }

                if (vmtask.TaskAssinmentId > 0)
                {
                    vmtask = obj.UpdateTaskPerformer(vmtask);
                }
                else
                {
                    vmtask = obj.CreateTaskPerformer(vmtask);
                }
                if (vmtask.Id > 0)
                {
                    GettaskTypeDetails(Convert.ToInt32(vmtask.Id));
                    GettaskDetails(Convert.ToInt32(vmtask.Id));
                }
                if (vmtask.Role == 4)
                {
                    GettaskTypeDetails(Convert.ToInt32(vmtask.Id));
                    GettaskDetails(Convert.ToInt32(vmtask.Id));
                    return PartialView("_EditAssignTask", vmtask);
                }
                else
                {
                    GettaskTypeDetails(Convert.ToInt32(vmtask.Id));
                    GettaskDetails(Convert.ToInt32(vmtask.Id));
                    return PartialView("_EditTaskAssignmentPerformer", vmtask);
                }
            }
            else
            {
                ModelState.AddModelError("", "Validation error occurred");
                if (vmtask.Role == 4)
                {
                    return PartialView("_EditAssignTask", vmtask);
                }
                else
                {
                    return PartialView("_EditTaskAssignmentPerformer", vmtask);
                }
            }
        }

        private List<string> GetTaskFile(long id)
        {
            return ViewBag.lstTaskFile = obj.GetTaskFile(id);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingInline_Create([DataSourceRequest] DataSourceRequest request, VMTask vmtask)
        {
            vmtask.UserId = AuthenticationHelper.UserID;
            if (vmtask != null && ModelState.IsValid)
            {
                vmtask = obj.CreateTaskAssinment(vmtask);
            }

            return Json(new[] { vmtask }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult TaskFileDownload(int Id)
        {
            if (Id > 0)
            {
                bool downloadfile = obj.DownloadTaskFile(Id);
            }
            return View();
        }

        public ActionResult TaskFileDownload1(int Id)
        {
            if (Id > 0)
            {
                bool downloadfile = obj.DownloadTaskFile1(Id);
            }
            return View();
        }

        public ActionResult TaskFileView(int Id)
        {
            if (Id > 0)
            {
                bool viewfile = obj.ViewTaskFile(Id);
            }
            return View();
        }

        public ActionResult ExportTask()
        {
            #region Sheet 1   
            using (ExcelPackage exportPackge = new ExcelPackage())
            {

                string TaskType = string.Empty;
                string CreatedBy = string.Empty;
                string AssignTo = string.Empty;
                string Entity = string.Empty;
                string Meeting = string.Empty;
                string Agenda = string.Empty;
                DateTime DueDate;


                string NameNoticeCase = string.Empty;
                try
                {
                    int userId = AuthenticationHelper.UserID;
                    List<BM_SP_Task_Result> Tasktable = new List<BM_SP_Task_Result>();

                    DataView view1 = new DataView();

                    DataTable table1 = new DataTable();
                    int P = 0;


                    #region With Litigation Tax
                    {
                        table1.Columns.Add("SrNo", typeof(string));
                        table1.Columns.Add("Status", typeof(string));
                        table1.Columns.Add("TaskDesc", typeof(string));
                        table1.Columns.Add("TaskTitle", typeof(string));
                        table1.Columns.Add("TaskType", typeof(string));
                        table1.Columns.Add("CreatedBy", typeof(string));
                        table1.Columns.Add("AssignTo", typeof(string));
                        table1.Columns.Add("Entity", typeof(string));
                        table1.Columns.Add("Meeting", typeof(string));
                        table1.Columns.Add("Agenda", typeof(string));

                        #region Case

                        using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                        {
                            Tasktable = (from row in entities.BM_SP_Task(userId, true, false)
                                         select row).ToList();

                            Tasktable = (from g in Tasktable
                                         group g by new
                                         {
                                             g.ID,
                                             g.TaskTitle,
                                             g.TaskType,
                                             g.Createdby,
                                             g.UserID,
                                             g.Status,
                                             g.TaskDesc,
                                             g.MeetingId,
                                             g.EntityId,
                                             g.AgendaId

                                         } into GCS
                                         select new BM_SP_Task_Result
                                         {

                                             ID = GCS.Key.ID,
                                             TaskTitle = GCS.Key.TaskTitle,
                                             TaskType = GCS.Key.TaskType,
                                             Status = GCS.Key.Status,
                                             TaskDesc = GCS.Key.TaskDesc,
                                             UserID = GCS.Key.UserID,
                                             Createdby = GCS.Key.Createdby,
                                             EntityId = GCS.Key.EntityId,
                                             MeetingId = GCS.Key.MeetingId,
                                             AgendaId = GCS.Key.AgendaId
                                         }).ToList();

                            Tasktable = Tasktable.OrderBy(entry => entry.ID).ToList();

                            foreach (var item in Tasktable)
                            {
                                P++;
                                CreatedBy = (from row in entities.Users where row.ID == item.Createdby select row.FirstName + "" + row.LastName).FirstOrDefault();
                                AssignTo = (from row in entities.Users where row.ID == item.UserID select row.FirstName + "" + row.LastName).FirstOrDefault();
                                Meeting = (from row in entities.BM_Meetings where row.MeetingID == item.MeetingId select row.MeetingTitle).FirstOrDefault();
                                Agenda = (from row in entities.BM_MeetingAgendaMapping where row.AgendaID == item.AgendaId select row.AgendaItemText).FirstOrDefault();
                                Entity = (from row in entities.BM_EntityMaster where row.Id == item.EntityId select row.CompanyName).FirstOrDefault();
                                table1.Rows.Add(P, item.Status, item.TaskDesc, item.TaskTitle, item.TaskType, CreatedBy, AssignTo, Meeting, Agenda, Entity);

                            }
                        }

                        #endregion

                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("TaskReport");

                        #region Second Sheet

                        view1 = new System.Data.DataView(table1);

                        DataTable ExcelData = null;

                        ExcelData = view1.ToTable("Selected", false, "SrNo", "TaskTitle", "TaskDesc", "TaskType", "Createdby", "AssignTo", "Meeting", "Agenda", "Entity", "Status");



                        exWorkSheet1.Cells["A1"].Value = "SrNo";
                        exWorkSheet1.Cells["A1"].AutoFitColumns(8);
                        exWorkSheet1.Cells["A1:A2"].Merge = true;
                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["A1"].Style.WrapText = true;



                        exWorkSheet1.Cells["B1"].Value = "TaskDesc";
                        exWorkSheet1.Cells["B1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["B1:B2"].Merge = true;
                        exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["B1"].Style.WrapText = true;

                        exWorkSheet1.Cells["C1"].Value = "TaskTitle";
                        exWorkSheet1.Cells["C1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["C1:C2"].Merge = true;
                        exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["C1"].Style.WrapText = true;

                        exWorkSheet1.Cells["D1"].Value = "TaskType";
                        exWorkSheet1.Cells["D1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["D1:D2"].Merge = true;
                        exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["D1"].Style.WrapText = true;

                        exWorkSheet1.Cells["E1"].Value = "Createdby";
                        exWorkSheet1.Cells["E1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["E1:E2"].Merge = true;
                        exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["E1"].Style.WrapText = true;

                        exWorkSheet1.Cells["F1"].Value = "AssignTo";
                        exWorkSheet1.Cells["F1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["F1:F2"].Merge = true;
                        exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["F1"].Style.WrapText = true;

                        exWorkSheet1.Cells["G1"].Value = "Meeting";
                        exWorkSheet1.Cells["G1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["G1:G2"].Merge = true;
                        exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["G1"].Style.WrapText = true;

                        exWorkSheet1.Cells["H1"].Value = "Entity";
                        exWorkSheet1.Cells["H1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["H1:H2"].Merge = true;
                        exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["H1"].Style.WrapText = true;

                        exWorkSheet1.Cells["I1"].Value = "Agenda";
                        exWorkSheet1.Cells["I1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["I1:I2"].Merge = true;
                        exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["I1"].Style.WrapText = true;

                        exWorkSheet1.Cells["J1"].Value = "Status";
                        exWorkSheet1.Cells["J1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["J1:J2"].Merge = true;
                        exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        exWorkSheet1.Cells["J1"].Style.WrapText = true;
                        if (ExcelData.Rows.Count > 0)
                        {
                            exWorkSheet1.Cells["A3"].LoadFromDataTable(ExcelData, false);
                        }


                        string ActulValue = string.Empty;
                        string NewValue = string.Empty;
                        int j = 3;
                        int k = 3; ;
                        for (int i = 3; i <= 2 + ExcelData.Rows.Count; i++)
                        {

                            string cOpenDate = "c" + i;
                            exWorkSheet1.Cells[cOpenDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            string CloseDate = "D" + i;
                            exWorkSheet1.Cells[CloseDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            string CStatus = "E" + i;
                            exWorkSheet1.Cells[CStatus].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            string cResponceDate = "F" + i;
                            exWorkSheet1.Cells[cResponceDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                            string chke = "B" + i;
                            string Checkcell = exWorkSheet1.Cells[chke].Value.ToString();
                            if (i > 3)
                            {
                                string cOpenDate1 = "c" + i;
                                exWorkSheet1.Cells[cOpenDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CloseDate1 = "D" + i;
                                exWorkSheet1.Cells[CloseDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CStatus1 = "E" + i;
                                exWorkSheet1.Cells[CStatus1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string cResponceDate1 = "F" + i;
                                exWorkSheet1.Cells[cResponceDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                if (string.IsNullOrEmpty(Checkcell))
                                {
                                    j++;
                                }
                                else
                                {
                                    string checknow4 = "A" + k + ":A" + j;
                                    string chekcnow = "B" + k + ":B" + j;
                                    string chekcnow1 = "C" + k + ":C" + j;
                                    string chekcnow2 = "D" + k + ":D" + j;
                                    string chekcnow3 = "E" + k + ":E" + j;
                                    string chekcnow4 = "F" + k + ":F" + j;
                                    string chekcnow5 = "G" + k + ":G" + j;
                                    string chekcnow6 = "H" + k + ":H" + j;
                                    exWorkSheet1.Cells[checknow4].Merge = true;
                                    exWorkSheet1.Cells[chekcnow].Merge = true;
                                    exWorkSheet1.Cells[chekcnow1].Merge = true;
                                    exWorkSheet1.Cells[chekcnow2].Merge = true;
                                    exWorkSheet1.Cells[chekcnow3].Merge = true;
                                    exWorkSheet1.Cells[chekcnow4].Merge = true;
                                    exWorkSheet1.Cells[chekcnow5].Merge = true;
                                    exWorkSheet1.Cells[chekcnow6].Merge = true;
                                    k = i;
                                    j = i;
                                }
                                if (j == (2 + ExcelData.Rows.Count))
                                {
                                    string checknow4 = "A" + k + ":A" + j;
                                    string chekcnow = "B" + k + ":B" + j;
                                    string chekcnow1 = "C" + k + ":C" + j;
                                    string chekcnow2 = "D" + k + ":D" + j;
                                    string chekcnow3 = "E" + k + ":E" + j;
                                    string chekcnow4 = "F" + k + ":F" + j;
                                    string chekcnow5 = "G" + k + ":G" + j;
                                    string chekcnow6 = "H" + k + ":H" + j;
                                    exWorkSheet1.Cells[checknow4].Merge = true;
                                    exWorkSheet1.Cells[chekcnow].Merge = true;
                                    exWorkSheet1.Cells[chekcnow1].Merge = true;
                                    exWorkSheet1.Cells[chekcnow2].Merge = true;
                                    exWorkSheet1.Cells[chekcnow3].Merge = true;
                                    exWorkSheet1.Cells[chekcnow4].Merge = true;
                                    exWorkSheet1.Cells[chekcnow5].Merge = true;
                                    exWorkSheet1.Cells[chekcnow6].Merge = true;
                                }
                            }
                        }

                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, 2 + ExcelData.Rows.Count, 17])
                        {
                            col.Style.WrapText = true;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                    }
                    #endregion

                    #endregion
                }

                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                byte[] data = exportPackge.GetAsByteArray();
                return File(data, "application/octet-stream", DateTime.Now + "TaskReport.xlsx");
            }
            #endregion

        }

        [NonAction]
        public void GettaskTypeDetails(int TaskId)
        {
            ViewBag.lstTaskType = obj.getTaskTypeDetails(TaskId);

        }
        //GetTask(int taskId)
        public void GettaskDetails(int taskId)
        {
            ViewBag.Taskdetails = obj.GetTask(taskId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TaskDelete(int taskId)
        {
            var userId = AuthenticationHelper.UserID;
            var result = obj.DeleteTask(taskId, userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}