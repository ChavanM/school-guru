﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class KMPMasterController : Controller
    {
        // GET: BM_Management/KMPMaster
        IKMP_Master _objkmp;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public KMPMasterController(IKMP_Master _objkmp)
        {
            this._objkmp = _objkmp;
        }

        public ActionResult Index()
        {
            Director _objdirector = new Director();
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                _objdirector.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Key Managerial Personnel (KMP)") select x.Addview).FirstOrDefault();
                _objdirector.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Key Managerial Personnel (KMP)") select x.Editview).FirstOrDefault();
                _objdirector.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Key Managerial Personnel (KMP)") select x.DeleteView).FirstOrDefault();
            }
            return View(_objdirector);

           
        }

        public PartialViewResult Create(int Id)
        {
            if (Id == 0)
            {
                //Director_KMP _objKMP = new Director_KMP();
                //_objKMP._objSecurities = new Director_MasterKMPUploadSecurity();
                //_objKMP.objDirector_MasterVM = new Director_MasterVM_KMP();
                //_objKMP.objcompanysecurity = new Director_Master_KMPCompanySecurity();
                Director_MasterVM_KMP _objKMP = new Director_MasterVM_KMP();
                return PartialView("_KMPMaster", _objKMP);
            }
            else
            {
                Director_KMP _objKMP = new Director_KMP();
                _objKMP._objSecurities = new Director_MasterKMPUploadSecurity();
                _objKMP.objDirector_MasterVM = _objkmp.GetKMP_Dtls(Id, CustomerId);
                if (_objKMP.objDirector_MasterVM == null)
                {
                    _objKMP.objDirector_MasterVM = new Director_MasterVM_KMP();
                }
                _objKMP.objcompanysecurity = _objkmp.GetKMPCompanyDtls(Id, CustomerId);

                if (_objKMP.objcompanysecurity == null)
                {
                    _objKMP.objcompanysecurity = new Director_Master_KMPCompanySecurity();
                    _objKMP.objcompanysecurity.KMP_Id = _objKMP.objDirector_MasterVM.ID;
                    _objKMP._objSecurities.KMP_ID= _objKMP.objDirector_MasterVM.ID;

                }
                else
                {
                    _objKMP.objcompanysecurity.KMP_Id = _objKMP.objDirector_MasterVM.ID;
                    _objKMP._objSecurities.KMP_ID = _objKMP.objDirector_MasterVM.ID;
                }

                return PartialView("CreateKMP", _objKMP);
            }
        }

        public ActionResult SaveKMP(Director_MasterVM_KMP _objDirector_MasterVM_KMP)
        {
            if (ModelState.IsValid)
            {
                if (_objDirector_MasterVM_KMP != null)
                {
                    if (_objDirector_MasterVM_KMP.ID == 0)
                    {
                        _objDirector_MasterVM_KMP = _objkmp.CreateKMP(_objDirector_MasterVM_KMP, CustomerId);
                    }
                    else
                    {
                        _objDirector_MasterVM_KMP = _objkmp.UpdateKMP(_objDirector_MasterVM_KMP, CustomerId);
                    }
                }

                //    if (_objDirector_MasterVM_KMP.ID > 0)
                //    {
                //        Director_KMP objdirectorkmp = new Director_KMP();
                //        objdirectorkmp.objDirector_MasterVM = _objDirector_MasterVM_KMP;
                //        objdirectorkmp._objSecurities = new Director_MasterKMPUploadSecurity() { KMP_ID = _objDirector_MasterVM_KMP.ID };
                //        objdirectorkmp.objcompanysecurity = new Director_Master_KMPCompanySecurity() { KMP_Id = _objDirector_MasterVM_KMP.ID };
                //        return PartialView("CreateKMP", objdirectorkmp);
                //    }
                //    else
                //    {
                //        return PartialView("_KMPMaster", _objDirector_MasterVM_KMP);
                //    }
                //}
                //else
                //{
                //    return PartialView("_KMPMaster", _objDirector_MasterVM_KMP);
                //}
            }

            return PartialView("_KMPMaster", _objDirector_MasterVM_KMP);

        }

        public ActionResult SaveKMP_CompanySecurity(Director_Master_KMPCompanySecurity _obj_KMP)
        {
            if (ModelState.IsValid)
            {

                if (_obj_KMP.Id_sec == 0)
                {
                    var objkempdtls = _objkmp.CreateKMP_CompanySecurity(_obj_KMP, CustomerId);
                    return PartialView("_PartialKMPCompanyDtls", objkempdtls);
                }
                else
                {
                    var objkempdtls = _objkmp.UpdateKMP_CompanySecurity(_obj_KMP, CustomerId);
                    return PartialView("_PartialKMPCompanyDtls", objkempdtls);
                }

            }
            else
            {
                return PartialView("_PartialKMPCompanyDtls", _obj_KMP);
            }
        }
        public ActionResult Delete(Director_MasterVM_KMP _objDirector_MasterVM_KMP)
        {
            if (ModelState.IsValid)
            {
                var objDelete = _objkmp.DeleteKMP(_objDirector_MasterVM_KMP, CustomerId);
                return PartialView(objDelete);
            }
            else
            {
                return PartialView("CreateKMP", _objDirector_MasterVM_KMP);
            }
        }
        public ActionResult Delete_companySecurity(Director_Master_KMPCompanySecurity _objDirector_MasterVM_KMP)
        {
            if (ModelState.IsValid)
            {
                var objDelete = _objkmp.DeleteKMPdtls(_objDirector_MasterVM_KMP, CustomerId);
                return PartialView(objDelete);
            }
            else
            {
                return PartialView("CreateKMP", _objDirector_MasterVM_KMP);
            }
        }

        [HttpPost]
        public ActionResult UploadKMP_SecuritiesDtls(Director_MasterKMPUploadSecurity _objSecurities)
        {
            if (_objSecurities.File != null && _objSecurities.Id > 0)
            {
                if (_objSecurities.File.ContentLength > 0)
                {
                    int UserId = AuthenticationHelper.UserID;

                    string path = "~/Areas/BM_Management/Documents/" + AuthenticationHelper.CustomerID + "/KMP_SecurityDetails/" + UserId + "/";
                    string _file_Name = Path.GetFileName(_objSecurities.File.FileName);
                    string _path = Path.Combine(Server.MapPath(path), _file_Name);
                    bool exists = System.IO.Directory.Exists(Server.MapPath(path));
                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(path));

                    _objSecurities.File.SaveAs(_path);
                    FileInfo excelfile = new FileInfo(_path);
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = KMPSheetsExitsts(xlWorkbook, "KMPForm");
                            if (flag == true)
                            {

                                KMP_SecuritiesDetails(xlWorkbook, _objSecurities.Id, _objSecurities.Current_Date);
                            }
                            else
                            {
                                ViewBag.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'DmatForm'.";
                            }
                        }
                    }
                }
            }
            return PartialView("_SecurityUpload");
        }

        private bool KMPSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("KMPForm"))
                    {
                        if (sheet.Name.Trim().Equals("KMPForm"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }

        private void KMP_SecuritiesDetails(ExcelPackage xlWorkbook, long KMPID, string Current_date)
        {
            List<string> errorMessage = new List<string>();
            try
            {
                bool saveSuccess = false;
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["KMPForm"];
                if (xlWorksheet != null)
                {

                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string CompanyName = string.Empty;
                    int Securities_Number = -1;
                    string Securities_Description = string.Empty;
                    long Securities_NominalValue = -1;
                    DateTime? acquisition_Date;
                    decimal acquisitionsecurities_PricePaid = -1;
                    long Other_acquisition_considerationpaid = -1;
                    DateTime? Disposal_Date;
                    decimal Disposal_RecivedPrice = -1;
                    decimal Other_considerationRecivedPrice = -1;
                    string acquisition_securities_Mode = string.Empty;
                    long CBal_SecuritiesNO = -1;
                    string Mode = string.Empty;
                    string Any_encumbrance_or_pledged = string.Empty;


                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        CompanyName = string.Empty;
                        Securities_Number = -1;
                        Securities_Description = string.Empty;
                        Securities_NominalValue = -1;
                        acquisition_Date = null;
                        acquisitionsecurities_PricePaid = -1;
                        Other_acquisition_considerationpaid = -1;
                        Disposal_Date = null;
                        Disposal_RecivedPrice = -1;
                        Other_considerationRecivedPrice = -1;
                        acquisition_securities_Mode = string.Empty;
                        CBal_SecuritiesNO = -1;
                        Mode = string.Empty;
                        Any_encumbrance_or_pledged = string.Empty;

                        #region 1 CompanyName
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                        {
                            CompanyName = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            if (string.IsNullOrEmpty(CompanyName))
                            {
                                errorMessage.Add("Please Check the CompanyName can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 2 Securities Number
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                        {
                            Securities_Number = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(Securities_Number)))
                        {
                            errorMessage.Add("Please Check the Securities Number can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 3 Securities Description
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                        {
                            Securities_Description = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Securities_Description))
                        {
                            errorMessage.Add("Required Securities Description at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 4 Securities NominalValue
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim()))
                        {
                            Securities_NominalValue = Convert.ToInt64((xlWorksheet.Cells[i, 4].Text).Trim());
                        }
                        if (Securities_NominalValue <= 0)
                        {
                            errorMessage.Add("Required Securities NominalValue at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 5 Acquisition_Date
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim()))
                        {
                            acquisition_Date = Convert.ToDateTime(xlWorksheet.Cells[i, 5].Text);
                        }
                        if (acquisition_Date == null)
                        {
                            errorMessage.Add("Required Acquisition_Date at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region  6 Acquisition Securities PricePaid
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim())))
                        {
                            acquisitionsecurities_PricePaid = Convert.ToUInt32(xlWorksheet.Cells[i, 6].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Convert.ToString(acquisitionsecurities_PricePaid)))
                        {
                            errorMessage.Add("Required Acquisition Securities PricePaid at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 7 Other Acquisition Consideration Paid
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim())))
                        {
                            Other_acquisition_considerationpaid = Convert.ToInt64((xlWorksheet.Cells[i, 7].Text.Trim()));
                        }
                        //if (Other_acquisition_considerationpaid <= 0)
                        //{
                        //    errorMessage.Add("Required Other Acquisition Consideration Paid at row number - " + (count + 1) + "");
                        //}
                        #endregion

                        #region 8 Disposal Date                        
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim())))
                        {
                            Disposal_Date = (Convert.ToDateTime(Convert.ToString(xlWorksheet.Cells[i, 8].Text)));
                        }
                        //if (string.IsNullOrEmpty(Disposal_Date))
                        //{
                        //    errorMessage.Add("Required Disposal Date  at row - " + (count + 1) + "");
                        //}
                        #endregion

                        #region 9 Disposal RecivedPrice
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim())))
                        {
                            Disposal_RecivedPrice = Convert.ToInt32(xlWorksheet.Cells[i, 9].Text.Trim());
                        }
                        //if (Disposal_RecivedPrice < 0)
                        //{
                        //    errorMessage.Add("Required Disposal Recived Price at row number - " + (count + 1) + "");
                        //}
                        #endregion

                        #region 10 Other_considerationRecivedPrice   
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim())))
                        {
                            Other_considerationRecivedPrice = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text.Trim());
                        }
                        //if (Other_considerationRecivedPrice < 0)
                        //{
                        //    errorMessage.Add("Required Other consideration Recived Price at row number - " + (count + 1) + "");
                        //}
                        #endregion

                        #region 11 Acquisition Securities Mode
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                        {
                            acquisition_securities_Mode = Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(acquisition_securities_Mode))
                        {
                            errorMessage.Add("Required Acquisition Securities Mode at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 12 CBal_SecuritiesNO
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim())))
                        {
                            CBal_SecuritiesNO = Convert.ToInt32(xlWorksheet.Cells[i, 12].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Convert.ToString(CBal_SecuritiesNO)))
                        {
                            errorMessage.Add("Required Cumulative balance and number of securities held after each transaction at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 13 Mode
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                        {
                            Mode = Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Mode))
                        {
                            errorMessage.Add("Required Mode of Security at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 14 Any_encumbrance_or_pledged
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                        {
                            Any_encumbrance_or_pledged = Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Convert.ToString(Any_encumbrance_or_pledged)))
                        {
                            errorMessage.Add("Required Securities have been pledged or any encumbrance has been created at row number - " + (count + 1) + "");
                        }
                        #endregion

                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            CompanyName = string.Empty;
                            Securities_Number = -1;
                            Securities_Description = string.Empty;
                            Securities_NominalValue = -1;
                            acquisition_Date = null;
                            acquisitionsecurities_PricePaid = -1;
                            Other_acquisition_considerationpaid = -1;
                            Disposal_Date = null;
                            Disposal_RecivedPrice = -1;
                            Other_considerationRecivedPrice = -1;
                            acquisition_securities_Mode = string.Empty;
                            CBal_SecuritiesNO = -1;
                            Mode = string.Empty;
                            Any_encumbrance_or_pledged = string.Empty;

                            #region 1 CompanyName
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                            {
                                CompanyName = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Securities_Number
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                            {
                                Securities_Number = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                            }
                            #endregion

                            #region 3 Securities_Description
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                            {
                                Securities_Description = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                            }
                            #endregion

                            #region 4 Securities_NominalValue
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim()))
                            {
                                Securities_NominalValue = Convert.ToInt64((xlWorksheet.Cells[i, 4].Text).Trim());
                            }
                            #endregion

                            #region 5 Acquisition Date
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim()))
                            {
                                acquisition_Date = Convert.ToDateTime(xlWorksheet.Cells[i, 5].Text);
                            }
                            #endregion

                            #region  6 Acquisition Securities PricePaid
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim())))
                            {
                                acquisitionsecurities_PricePaid = Convert.ToInt32(xlWorksheet.Cells[i, 6].Text.Trim());
                            }
                            #endregion

                            #region 7 Other Acquisition Considerationpaid
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim())))
                            {
                                Other_acquisition_considerationpaid = Convert.ToInt64((xlWorksheet.Cells[i, 7].Text.Trim()));
                            }
                            #endregion

                            #region 8 Disposal_Date                       
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim())))
                            {
                                Disposal_Date = Convert.ToDateTime(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim()));
                            }
                            #endregion

                            #region 9 Disposal Recived Price
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim())))
                            {
                                Disposal_RecivedPrice = Convert.ToInt32(xlWorksheet.Cells[i, 9].Text.Trim());
                            }
                            #endregion

                            #region 10 Other_considerationRecivedPrice   
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim())))
                            {
                                Other_considerationRecivedPrice = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text.Trim());
                            }
                            #endregion

                            #region 11 acquisition_securities_Mode
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                            {
                                acquisition_securities_Mode = Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim());
                            }
                            #endregion

                            #region 12 CBal_SecuritiesNO
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim())))
                            {
                                CBal_SecuritiesNO = Convert.ToInt32(xlWorksheet.Cells[i, 12].Text.Trim());
                            }
                            #endregion

                            #region 13 Mode
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                            {
                                Mode = Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim());
                            }
                            #endregion

                            #region 14 Any_encumbrance_or_pledged
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                            {
                                Any_encumbrance_or_pledged = Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim());
                            }
                            #endregion

                            BM_SecurityDetail Obj_Kmpsecurity = new BM_SecurityDetail();
                            if (KMPID > 0)
                                Obj_Kmpsecurity.Director_KmpId = Convert.ToInt32(KMPID);
                            Obj_Kmpsecurity.CustomerId = CustomerId;
                            Obj_Kmpsecurity.CompanyName = CompanyName;
                            Obj_Kmpsecurity.securities_No = Securities_Number;
                            Obj_Kmpsecurity.Dis_of_securities = Securities_Description;
                            Obj_Kmpsecurity.Nominam_valSecurities = Securities_NominalValue;
                            if (acquisition_Date != null)
                                Obj_Kmpsecurity.acquisition_Date = acquisition_Date;
                            Obj_Kmpsecurity.acquisition_pricePaid = acquisitionsecurities_PricePaid;
                            Obj_Kmpsecurity.Other_ConsiPaid_acquisition = Other_acquisition_considerationpaid;
                            Obj_Kmpsecurity.disposal_RescPrice = Disposal_RecivedPrice;
                            Obj_Kmpsecurity.otherconsideration_disposal_RescPrice = Other_considerationRecivedPrice;
                            if (Disposal_Date != null)
                                Obj_Kmpsecurity.Date_disposal = Disposal_Date;
                            Obj_Kmpsecurity.Price_Received_disposal = Disposal_RecivedPrice;
                            Obj_Kmpsecurity.Cbal_No_securities_afterTransaction = CBal_SecuritiesNO;
                            Obj_Kmpsecurity.Mode_of_acquisitionSecurities = Mode;
                            Obj_Kmpsecurity.Securities_encumbrance = Any_encumbrance_or_pledged;

                            saveSuccess = _objkmp.CreateKMP_SecurityDetails(Obj_Kmpsecurity);
                            if (!saveSuccess)
                            {
                                ViewBag.Error = "KMP Security Data allready exist";
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ViewBag.ErrorMessage = errorMessage;
                        }
                    }

                    if (saveSuccess)
                    {
                        ViewBag.SuccessMessage = "KMP Securities Details Uploaded Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                ViewBag.ErrorMessage = errorMessage;
            }
        }

        public ActionResult CreateSecurity(long KmpID, int SecurityId)
        {
            Director_Master_KMPCompanySecurity objkmpsecuritydetails = new Director_Master_KMPCompanySecurity();
            objkmpsecuritydetails.KMP_Id = KmpID;
            return PartialView("_PartialKMPCompanyDtls", objkmpsecuritydetails);
        }

        public ActionResult DownloadSampleFormateforKMPSecurity()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\KMP\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "KMPSecurity.xlsx");
            string fileName = "KMPSecurity.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}