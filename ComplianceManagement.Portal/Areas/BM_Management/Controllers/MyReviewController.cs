﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.Services.Setting;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MyReviewController : Controller
    {
        ITask objITask;
        IMeeting_Service objIMeeting_Service;
        IAgendaMinutesReviewService objIAgendaMinutesReviewService;
        ISettingService objISettingService;
        public MyReviewController(ITask obj, IMeeting_Service objMeeting_Service, IAgendaMinutesReviewService objAgendaMinutesReviewService, ISettingService objSettingService)
        {
            objITask = obj;
            objIMeeting_Service = objMeeting_Service;
            objIAgendaMinutesReviewService = objAgendaMinutesReviewService;
            objISettingService = objSettingService;
        }
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult AgendaMinutesReview(long taskId, long assignmentId)
        //{
        //    int UserId = AuthenticationHelper.UserID;
        //    var model = objITask.GetAgendaMinutesReview(taskId, assignmentId, UserId);
        //    return PartialView("_AgendaMinutesReviewDetails", model);
        //}
        //[HttpPost]
        //public ActionResult SaveAgendaMinutesReview(ReviewerRemark model)
        //{
        //    int UserId = AuthenticationHelper.UserID;
        //    var mailFormat = "";
        //    model = objITask.SaveAgendaMinutesReview(model, UserId, out mailFormat);
        //    return PartialView("_AgendaMinutesReviewDetails", model);
        //}

        #region Review Agenda
        public PartialViewResult AgendaPreview(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.PreviewAgenda(MeetingId, CustomerId);
            return PartialView("_Viewer_Agenda", model);
        }

        public PartialViewResult AgendaReview(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.PreviewAgenda(MeetingId, CustomerId);

            return PartialView("_Viewer_Agenda", model);
        }
        #endregion

        #region Agenda, Documents Review
        public ActionResult ReviewAgendaDocument(long taskId, long assignmentId, long meetingId, bool canAcceptChanges, bool allowFinalizeReview)
        {
            int userId = AuthenticationHelper.UserID;
            var model = objITask.GetAgendaMinutesReview(taskId, assignmentId, userId, canAcceptChanges, allowFinalizeReview);
            return PartialView("_AgendaReview", model);
        }

        [HttpPost]
        public ActionResult SaveAgendaDocumentReview(ReviewerRemark model)
        {
            int userId = AuthenticationHelper.UserID;
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var mailFormat = "";
            model = objITask.SaveAgendaMinutesReview(model, userId, out mailFormat);
            if(!string.IsNullOrEmpty(mailFormat) && model.Success)
            {
                var accessToDirector = objISettingService.GetAccessToDirector(customerID);
                if(accessToDirector)
                {
                    var mailSubject = "Regarding Draft Agenda Reviewed.";
                    if (model.StatusId == SecretarialConst.TaskStaus.REJECTED)
                    {
                        mailSubject = "Regarding Draft Agenda Rejected.";
                    }
                    var lstReviewer = objITask.GetReviewerListByTaskAssignmentId(model.Id);
                    var performer = objITask.GetPerformerByTaskAssignmentId(model.Id);
                    if (lstReviewer != null && performer != null)
                    {
                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                        foreach (var item in lstReviewer)
                        {
                            string Mailbody = mailFormat.Replace("{{ Name of the Reviewer }}", item.UserName);
                            Mailbody = Mailbody.Replace("{{ Name of the Performer }}", performer.UserName);
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { performer.EmailId }), null, null, mailSubject, Mailbody);
                        }
                    }
                }
            }

            return Json( new { Result = model }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReviewAgendaDocumentData([DataSourceRequest] DataSourceRequest request, long meetingId, long taskId, long assignmentId)
        {
            return Json(objIAgendaMinutesReviewService.AgendaDocumentReview(meetingId, taskId, assignmentId), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ReviewMinutesDocumentData([DataSourceRequest] DataSourceRequest request, long meetingId, long taskId, long assignmentId)
        {
            return Json(objIAgendaMinutesReviewService.DraftMinutesReview(meetingId, taskId, assignmentId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetDocumentOrder(List<FileSrNoVM> model, long meetingId)
        {
            int userId = AuthenticationHelper.UserID;
            return Json( new { Result = objIAgendaMinutesReviewService.SetDocumentOrder(model, meetingId, userId) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MergeReviewChanges(long taskId, long taskAssignmentId, string remark)
        {
            int userId = AuthenticationHelper.UserID;
            var result = objIAgendaMinutesReviewService.MergeChanges(taskId, taskAssignmentId, remark, userId);
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReSendChangesForReview(ReviewerRemark model)
        {
            int userId = AuthenticationHelper.UserID;
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var mailFormat = "";
            var result = objIAgendaMinutesReviewService.ReSendChangesForReview((long) model.TaskId, model.Id, model.Remark, userId, out mailFormat);
            if (!string.IsNullOrEmpty(mailFormat) && result.Success)
            {
                var accessToDirector = objISettingService.GetAccessToDirector(customerID);
                if (accessToDirector)
                {
                    var mailSubject = "Regarding review of the Draft Agenda.";
                    var lstReviewer = objITask.GetReviewerListByTaskAssignmentId(model.Id);
                    var performer = objITask.GetPerformerByTaskAssignmentId(model.Id);
                    if (lstReviewer != null && performer != null)
                    {
                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                        foreach (var item in lstReviewer.Where(k => k.RoleId == SecretarialConst.RoleID.REVIEWER))
                        {
                            string Mailbody = mailFormat.Replace("{{ Name of the Reviewer }}", item.UserName);
                            Mailbody = Mailbody.Replace("{{ Name of the Performer }}", performer.UserName);
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { item.EmailId }), null, null, mailSubject, Mailbody);
                        }
                    }
                }
            }
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}