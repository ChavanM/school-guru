﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Drawing;
using BM_ManegmentServices.Services.Meetings;
using System.Configuration;
//using GleamTech.DocumentUltimate;
//using GleamTech.DocumentUltimate.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MeetingInviteeController : Controller
    {
        IMeetingInvitee objIMeetingInvitee;
        public MeetingInviteeController(IMeetingInvitee obj)
        {
            objIMeetingInvitee = obj;
        }
        public ActionResult Editing_Custom(long? meetingID)
        {
            return PartialView("_Invitee",(long)meetingID);
        }

        public ActionResult Invitee_Read([DataSourceRequest] DataSourceRequest request, long? meetingID)
        {
            return Json(objIMeetingInvitee.GetAll((long)meetingID,true).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Invitee_Update([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<MeetingInviteeVM> products)
        {
            if (products != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var product in products)
                {
                    objIMeetingInvitee.Update(product, UserId);
                }
            }

            return Json(products.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Invitee_Create([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<MeetingInviteeVM> products)
        {
            var results = new List<MeetingInviteeVM>();

            if (products != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var product in products)
                {
                    objIMeetingInvitee.Create(product, UserId);

                    results.Add(product);
                }
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Invitee_Destroy([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<MeetingInviteeVM> products)
        {
            foreach (var product in products)
            {
                //productService.Destroy(product);
            }

            return Json(products.ToDataSourceResult(request, ModelState));
        }

        [HttpDelete]
        public ActionResult DeleteInvitee(int participantID)
        {
            MeetingInviteeVM objmeeting = new MeetingInviteeVM();
            objmeeting.MeetingParticipantId = participantID;
            int UserId = Convert.ToInt32(AuthenticationHelper.UserID);

            objmeeting = objIMeetingInvitee.DestroyInvitee(objmeeting, UserId);


            return Json(objmeeting, JsonRequestBehavior.AllowGet);
        }

    }
}