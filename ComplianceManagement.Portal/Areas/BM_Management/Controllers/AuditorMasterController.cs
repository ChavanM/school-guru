﻿
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Windows.Media;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Styles;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using BM_ManegmentServices.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class AuditorMasterController : Controller
    {
        IAuditorMaster obj;
        int Customer_Id = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = AuthenticationHelper.UserID;
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public AuditorMasterController(IAuditorMaster obj)
        {
            this.obj = obj;
        }

        // GET: BM_Management/AuditorMaster
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AuditorMaster()
        {
            VMAuditor objAuditor = new VMAuditor();
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                objAuditor.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Auditor") select x.Addview).FirstOrDefault();
                objAuditor.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Auditor") select x.Editview).FirstOrDefault();
                objAuditor.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Auditor") select x.DeleteView).FirstOrDefault();
            }

            return View(objAuditor);
        }

        public ActionResult GetAuditorMaster(int EntityId)
        {
            VMAuditor obj = new VMAuditor();
            if (EntityId > 0)
            {
                obj.Entity_Id = EntityId;
            }
            return PartialView("_partialviewAuditorDetails", obj);
        }

        public ActionResult Add_EditAuditorMaster(int Id, int StautoryAuditor, string ViewName)
        {
            VMAuditor objvmauditor = new VMAuditor();

            if (Id > 0 && StautoryAuditor == 0)
            {
                //objvmauditor = obj.GetAuditorbyId(Id,Customer_Id);
                objvmauditor = obj.GetAuditorbyId(Id);
            }
            else if (Id > 0 && StautoryAuditor == 1)
            {
                objvmauditor = obj.GetAuditorbyId(Id);
                //objvmauditor = obj.GetAuditorbyId(Id, Customer_Id);
                objvmauditor.forStatutory = 1;
            }
            //else if(Id == 0 && ViewName == "AuditorAppointment")
            //{
            //    objvmauditor.Auditor_Type = 2;
            //}

            if (ViewName == "AuditorAppointment")
            {
                objvmauditor.ViewName = "AuditorAppointment";
            }
            if (objvmauditor.Id == 0)
            {
                objvmauditor.IsMapped = true;
            }
            return PartialView("_PartialViewForAuditPopUp", objvmauditor);
        }

        public ActionResult getAuditordetails(int Id)
        {
            VMAuditor objvmauditor = new VMAuditor();

            if (Id > 0)
            {
                
                objvmauditor = obj.GetAuditorbyId(Id);
                objvmauditor.Id = 0;
                objvmauditor.IsMapped = true;
                objvmauditor.Isexisting = false;
                objvmauditor.AuditorId = Id;
                ModelState.Clear();
            }
           
            return PartialView("_PartialViewForAuditPopUp", objvmauditor);
        }

        public ActionResult Add_EditAuditorNumberMaster(int Id)
        {
            VMAuditor obj = new VMAuditor();
            return PartialView("_PartialViewforAuditNumber", obj);
        }

        [HttpPost]
        public ActionResult Add_UpdateAudit(VMAuditor _vmauditor)
        {
            if (ModelState.IsValid)
            {
                _vmauditor.CustomerId = Customer_Id;
                _vmauditor.logedinuserId = UserId;
                if (_vmauditor.Id == 0)
                {
                   
                    _vmauditor = obj.AddAuditData(_vmauditor);
                }
                else
                {
                    _vmauditor = obj.UpdateAuditData(_vmauditor);
                }
            }
            return PartialView("_partialviewAddEditAuditor", _vmauditor);
        }

        [HttpGet]
        public ActionResult AuditDetails(int Entity_ID)
        {
            VMStatutory_Auditor _objstatutory = new VMStatutory_Auditor();
            _objstatutory.EntityId = Entity_ID;
            ViewBag.EntityID = Entity_ID;
            string EntityName = string.Empty;
            if (Entity_ID > 0)
            {
                EntityName = obj.getEntityName(Entity_ID);
            }
            _objstatutory.EntityName = EntityName;
            return View(_objstatutory);
        }
        public ActionResult Open_popforStatutoryAudit(int Id, int EntityID)
        {
            VMStatutory_Auditor _objstatutory_auditor = new VMStatutory_Auditor();
            _objstatutory_auditor.AuditDeatilsList = new List<VMAuditorDetails> { new VMAuditorDetails() };

            if (Id > 0)
            {
                _objstatutory_auditor = obj.GetStatutorybyId(Id, Customer_Id);
            }
            if (_objstatutory_auditor.AuditDeatilsList.Count > 0)
            {
                _objstatutory_auditor.Auditor_Number = _objstatutory_auditor.AuditDeatilsList.Count;
            }
            _objstatutory_auditor.EntityId = EntityID;
            return PartialView("_AddEditStatutoryAuditor", _objstatutory_auditor);

        }

        [HttpPost]
        public ActionResult Add_UpdateStaturory_Audit(VMStatutory_Auditor _objstatutory_auditor, string submitButton)
        {
            if (submitButton == null)
            {
                if (_objstatutory_auditor.AuditDeatilsList == null)
                {
                    _objstatutory_auditor.AuditDeatilsList = new List<VMAuditorDetails>();
                }

                if (_objstatutory_auditor.Auditor_Number < _objstatutory_auditor.AuditDeatilsList.Count)
                {
                    List<VMAuditorDetails> lstTemp = new List<VMAuditorDetails>();
                    for (int i = 0; i < _objstatutory_auditor.Auditor_Number; i++)
                    {
                        lstTemp.Add(_objstatutory_auditor.AuditDeatilsList[i]);
                    }
                    _objstatutory_auditor.AuditDeatilsList = lstTemp;
                }
                else
                {

                    for (int i = _objstatutory_auditor.AuditDeatilsList.Count; i < _objstatutory_auditor.Auditor_Number; i++)
                    {
                        _objstatutory_auditor.AuditDeatilsList.Add(new VMAuditorDetails());
                    }

                }
            }

            else if (submitButton == "")
            {
                if (ModelState.IsValid)
                {

                    _objstatutory_auditor.CustomerId = Customer_Id;
                    if (_objstatutory_auditor.Id == 0 && _objstatutory_auditor.EntityId > 0)
                    {
                        _objstatutory_auditor = obj.AddStatutoryAuditor(_objstatutory_auditor);
                    }
                    else
                    {
                        _objstatutory_auditor = obj.UpdateStatutoryAuditor(_objstatutory_auditor);
                    }
                }
            }
            return PartialView("_AddEditStatutoryAuditor", _objstatutory_auditor);
        }

        [HttpPost]
        public ActionResult Add_UpdateInternalAuditor(VMInternalAuditor _objinternal_auditor)
        {

            if (ModelState.IsValid)
            {
                _objinternal_auditor.UserId = AuthenticationHelper.UserID;
                _objinternal_auditor.CustomerId = (int)AuthenticationHelper.CustomerID;
                _objinternal_auditor.CustomerId = Customer_Id;
                if (_objinternal_auditor.Id == 0 && _objinternal_auditor.EntityId > 0)
                {
                    _objinternal_auditor = obj.AddInternalAuditor(_objinternal_auditor);
                }
                else
                {
                    _objinternal_auditor = obj.UpdateInternalAuditor(_objinternal_auditor);
                }
            }
            return PartialView("_partialviewAddEditInternalAuditor", _objinternal_auditor);
        }


        public ActionResult Open_PopUpForResignation(int Id, int AuditorId, int EntityId, string AU)
        {
            var objstatutory = new Auditor_ResignationVM();
            ViewBag.EntityId = EntityId;
            if (AuditorId > 0)
            {
                objstatutory = obj.GetAuditorResignationDetails(Id, AuditorId, EntityId, AU);
            }
            return PartialView("_Resignation", objstatutory);
        }

        [HttpPost]
        public ActionResult Add_UpdateResignation(Auditor_ResignationVM obj_regin)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (obj_regin.SA_ID > 0)
                {
                    obj_regin = obj.UpdateAuditorResignation(obj_regin);
                }
                else
                {
                    obj_regin = obj.SaveAuditorResignation(obj_regin, customerId, userId);
                }
            }
            return PartialView("_Resignation", obj_regin);
        }

        [HttpPost]
        public ActionResult RevertResignation(int Id, int AuditorId, int EntityId, string AU)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = obj.RevertAuditorResignation(Id, AuditorId, userId);
            var model = obj.GetAuditorResignationDetails(Id, AuditorId, EntityId, AU);
            model.Error = result.Error;
            model.Message = result.Message;
            return PartialView("_Resignation", model);
        }

        [HttpPost]
        public ActionResult Resignation(long AuditorMappingId)
        {
            var model = obj.ResignationLetter(AuditorMappingId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadResignationLetter(long AuditorMappingId)
        {
            try
            {
                DocxFormatProvider provider = new DocxFormatProvider();
                RadFlowDocument document = CreateRadFixedDocumentNew(AuditorMappingId);
                Byte[] bytes = provider.Export(document);

                return File(bytes, "application/force-download", "Resignation Letter.docx");
            }
            catch (Exception e)
            {
                return PartialView("_FileNotFound");
            }
        }

        public RadFlowDocument CreateRadFixedDocumentNew(long AuditorMappingId)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            try
            {
                var ResignationFormat = obj.ResignationLetter(AuditorMappingId);

                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 


                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                #region Document header

                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Center;
                editor.MoveToParagraphStart(paragraph);

                editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                var insertOptions = new InsertDocumentOptions
                {
                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                };
                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();
                RadFlowDocument htmlDocument = htmlProvider.Import(ResignationFormat.ResignationFormat);
                editor.InsertDocument(htmlDocument);
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }

        public ActionResult Open_PopUpForInternalAuditor(long Id, int EntityId)
        {
            VMInternalAuditor objinternal = new VMInternalAuditor();
            objinternal.EntityId = EntityId;
            ViewBag.EntityId = EntityId;
            objinternal.Number_OF_InternalAuditor = 1;
            //objinternal.InternalAuditorDetails = new List<InternalAuditor> { new InternalAuditor() };
            if (Id > 0 && EntityId > 0)
            {
                objinternal = obj.GetInternalAuditorbyId(Id, EntityId);
            }
            //objinternal.Number_OF_InternalAuditor = objinternal.InternalAuditorDetails.Count();
            return PartialView("_partialviewAddEditInternalAuditor", objinternal);
        }

        public ActionResult Add_UpdateSecreterialAuditor(VM_SecreterialAuditor _objsecreterial)

        {
            _objsecreterial.CustomerId = Customer_Id;
            if (_objsecreterial.Id > 0 && _objsecreterial.EntityId > 0)
            {

                _objsecreterial = obj.updateSecreterialDate(_objsecreterial);
            }
            else
            {
                _objsecreterial = obj.AddSecreterialDate(_objsecreterial);
            }
            return PartialView("_partialviewAddEditSecreterialAuditor", _objsecreterial);
        }

        public ActionResult Open_PopUpForsacreterialAuditor(long Id, int EntityId)
        {
            VM_SecreterialAuditor objsecreterial = new VM_SecreterialAuditor();
            ViewBag.EntityId = EntityId;
            objsecreterial.EntityId = EntityId;
            if (Id > 0 && EntityId > 0)
            {
                objsecreterial = obj.getSecreterialAuditorbyId(Id, EntityId);
            }

            return PartialView("_partialviewAddEditSecreterialAuditor", objsecreterial);
        }

        public ActionResult Open_PopUpForCostAuditor(int Id, int EntityId)
        {
            VM_CostAuditor objCost = new VM_CostAuditor();
            ViewBag.EntityId = EntityId;
            objCost.EntityId = EntityId;
            if (Id > 0 && EntityId > 0)
            {
                objCost = obj.getCostAuditorbyId(Id, EntityId);
            }

            return PartialView("_AddEditCostAuditor", objCost);
        }

        public ActionResult Add_UpdateCostAuditor(VM_CostAuditor _objcost)

        {
            _objcost.CustomerId = Customer_Id;
            if (_objcost.Id > 0 && _objcost.EntityId > 0)
            {

                _objcost = obj.updateCostDate(_objcost);
            }
            else
            {
                _objcost = obj.AddCostDate(_objcost);
            }
            return PartialView("_AddEditCostAuditor", _objcost);
        }

        [HttpPost]
        public ActionResult UploadAuditors(VM_AuditUpload _objAuditor)
        {
            if (_objAuditor.File != null)
            {
                if (_objAuditor.File.ContentLength > 0)
                {
                    string excelfileName = string.Empty;
                    string path = "~/Areas/BM_Management/Documents/" + AuthenticationHelper.CustomerID + "/AuditorMaster/";
                    string _file_Name = Path.GetFileName(_objAuditor.File.FileName);
                    string _path = Path.Combine(Server.MapPath(path), _file_Name);
                    bool exists = System.IO.Directory.Exists(Server.MapPath(path));
                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(path));
                    _objAuditor.File.SaveAs(_path);

                    FileInfo excelfile = new FileInfo(_path);

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flagPublic = CheckAuditorsWorkSheet(xlWorkbook, "AuditorSheet");

                            if (flagPublic == true)
                            {
                                UploadAuditorData(xlWorkbook);
                            }

                            else
                            {
                                ViewBag.Error = "No Data Found in Excel Document or Sheet Name must be 'AuditorSheet'";
                            }
                        }
                    }
                }
            }

            return PartialView("_AuditorsUpload");
        }

        private void UploadAuditorData(ExcelPackage xlWorkbook)
        {
            List<string> errorMessage = new List<string>();

            try
            {
                bool saveSuccess = false;
                ExcelWorksheet excelAuditor = xlWorkbook.Workbook.Worksheets["AuditorSheet"];

                if (excelAuditor != null)
                {
                    //#region Save Auditor Details

                    int count = 0;
                    int xlrow2 = excelAuditor.Dimension.End.Row;

                    #region Auditor/Firm  Data
                    string Type = string.Empty;
                    string Category = string.Empty;
                    string NameofAuditor_AuditorFirms = string.Empty;
                    string MembershipNo_RegistrationNo = string.Empty;
                    string AddressLine1 = string.Empty;
                    string AdressLine2 = string.Empty;
                    string Country = string.Empty;
                    int CountryId = 0;
                    int StateId = 0;
                    int CityId = 0;
                    string State = string.Empty;
                    string city = string.Empty;
                    string Pin = string.Empty;
                    string Email = string.Empty;
                    long MobileNumber = 0;
                    string Pan = string.Empty;

                    #endregion

                    #region Validations for Auditor's or Auditor's Firm
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        #region 1 Auditor Type
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 1].Text.Trim())))
                        {
                            Type = Convert.ToString(excelAuditor.Cells[i, 1].Text).Trim();
                        }
                        else if (string.IsNullOrEmpty(Type))
                        {
                            errorMessage.Add("Please Check the Auditor Type can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 2 Auditor Category
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 2].Text.Trim())))
                        {
                            Category = Convert.ToString(excelAuditor.Cells[i, 2].Text).Trim();
                            if (string.IsNullOrEmpty(Category))
                            {
                                errorMessage.Add("Please Check the Auditor Category can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 3 Auditor Name or Firm Name
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 3].Text.Trim())))
                        {
                            NameofAuditor_AuditorFirms = Convert.ToString(excelAuditor.Cells[i, 3].Text).Trim();
                            if (string.IsNullOrEmpty(Category))
                            {
                                errorMessage.Add("Please Check the Auditor Name or Firm Name can not be empty at row - " + i + "");
                            }
                        }
                        #endregion


                        #region 4 Emil Id

                        if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 4].Text.Trim())))
                        {
                            Email = Convert.ToString(excelAuditor.Cells[i, 4].Text).Trim();
                        }

                        if (string.IsNullOrEmpty(Email))
                        {
                            errorMessage.Add("Please Check the Emil Id can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 5 Mobile Number

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 5].Text.Trim())))
                        {
                            MobileNumber = Convert.ToInt64(excelAuditor.Cells[i, 5].Text.Trim());
                        }
                        #endregion

                        #region 5 PAN
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim())))
                        {
                            Pan = Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Pan))
                        {
                            errorMessage.Add("Please Check the PAN can not be empty at row" + (count + 1) + "");
                        }
                        #endregion

                        #region 6 Membership Number or Registration Number


                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim())))
                        {
                            MembershipNo_RegistrationNo = Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim());

                        }
                        if (String.IsNullOrEmpty(MembershipNo_RegistrationNo))
                        {
                            errorMessage.Add("Please Check the Registration Number can not be empty at row " + (count + 1) + "");
                        }

                        #endregion

                        #region 7 Address Line 1
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim())))
                        {
                            AddressLine1 = Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(AddressLine1))
                        {
                            errorMessage.Add("Please Check the Address Line 1 can not be empty at row " + (count + 1) + "");
                        }
                        #endregion

                        #region 8 Addresss Line 2
                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 9].Text.Trim())))
                        {
                            AdressLine2 = Convert.ToString(excelAuditor.Cells[i, 9].Text.Trim());
                        }

                        #endregion

                        #region 9 Country

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim())))
                        {
                            Country = Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim());
                            if (Country != "")
                            {
                                CountryId = obj.Getcountry(Country);
                            }

                        }

                        #endregion

                        #region 10 State

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim())))
                        {
                            State = Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim());
                            if (State != "")
                            {
                                StateId = obj.getStateId(State);
                            }
                            if (StateId == 0)
                            {
                                errorMessage.Add("Please Check the state not found" + (count + 1) + "");
                            }
                        }
                        if (String.IsNullOrEmpty(State))
                        {
                            errorMessage.Add("Please Check the State can not be empty at row" + (count + 1) + "");
                        }
                        #endregion

                        #region 11 City

                        if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim())))
                        {
                            city = Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim());
                            if (State != "")
                            {
                                CityId = obj.getCityId(StateId, city);
                            }
                            //if (CityId == 0)
                            //{
                            //    errorMessage.Add("Please Check the city not found" + (count + 1) + "");
                            //}
                        }

                        #endregion

                        #region 12 Pin code
                        Pin = Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim());

                        if (!String.IsNullOrEmpty(Convert.ToString(Pin)))
                        {
                            try
                            {
                                if (Pin.Trim().Length > 6)
                                {
                                    errorMessage.Add("PIN should not be more than 6 digits, Check at Row - " + count);
                                }
                                else
                                {
                                    if (Convert.ToInt32(Pin) >= 0)
                                    {
                                       
                                    }
                                    else
                                        errorMessage.Add("Only positive number allowed, check at Row - " + count);
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("PIN must be number, check at Row-" + count);
                            }
                        }
                        else
                        {
                            errorMessage.Add("PIN can not be empty, check at row - " + (count + 1) + "");
                        }
                        #endregion
                    }
                    #endregion
                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            Type = string.Empty;
                            Category = string.Empty;
                            NameofAuditor_AuditorFirms = string.Empty;
                            MembershipNo_RegistrationNo = string.Empty;
                            AddressLine1 = string.Empty;
                            AdressLine2 = string.Empty;
                            Country = string.Empty;
                            State = string.Empty;
                            city = string.Empty;
                            Pin = string.Empty;
                            Email = string.Empty;
                            MobileNumber = 0;
                            Pan = string.Empty;

                            #region 1 Auditor Type

                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 1].Text.Trim())))
                            {
                                Type = Convert.ToString(excelAuditor.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Auditor Category

                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 2].Text.Trim())))
                            {
                                Category = Convert.ToString(excelAuditor.Cells[i, 2].Text).Trim();
                            }
                            #endregion

                            #region 3 Email Id
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 3].Text.Trim())))
                            {
                                NameofAuditor_AuditorFirms = Convert.ToString(excelAuditor.Cells[i, 3].Text.Trim());
                            }
                            #endregion

                            #region 4 Auditor Name or Firm Name
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 4].Text.Trim())))
                            {
                                Email = Convert.ToString(excelAuditor.Cells[i, 4].Text).Trim();
                                if (string.IsNullOrEmpty(Category))
                                {
                                    errorMessage.Add("Please Check the Auditor Name or Firm Name can not be empty at row - " + i + "");
                                }
                            }
                            #endregion

                            #region 5 Mobile Number
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 5].Text.Trim())))
                            {
                                MobileNumber = Convert.ToInt64(excelAuditor.Cells[i, 5].Text.Trim());
                            }
                            #endregion

                            #region 6 Pan Number
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim())))
                            {
                                Pan = Convert.ToString(excelAuditor.Cells[i, 6].Text.Trim());
                            }
                            #endregion

                            #region 7 Membership Number or Registration Number
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim())))
                            {
                                MembershipNo_RegistrationNo = Convert.ToString(excelAuditor.Cells[i, 7].Text.Trim());
                            }
                            #endregion

                            #region 8 Address Line 1
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim())))
                            {
                                AddressLine1 = Convert.ToString(excelAuditor.Cells[i, 8].Text.Trim());
                            }
                            #endregion

                            #region 9 Address Line 2
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 9].Text.Trim())))
                            {
                                AdressLine2 = Convert.ToString(excelAuditor.Cells[i, 9].Text.Trim());
                            }
                            #endregion

                            #region 10 country
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim())))
                            {
                                Country = Convert.ToString(excelAuditor.Cells[i, 10].Text.Trim());

                            }
                            #endregion

                            #region 11 State
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim())))
                            {
                                State = Convert.ToString(excelAuditor.Cells[i, 11].Text.Trim());
                                if (State != "")
                                {
                                    StateId = obj.getStateId(State);
                                }
                            }
                            #endregion

                            #region 12 City
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim())))
                            {
                                city = Convert.ToString(excelAuditor.Cells[i, 12].Text.Trim());
                                if (State != "")
                                {
                                    CityId = obj.getCityId(StateId, city);
                                }
                            }
                            #endregion

                            #region 13 Pin Number
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim())))
                            {
                                Pin = Convert.ToString(excelAuditor.Cells[i, 13].Text.Trim());
                            }
                            #endregion

                            #endregion

                            BM_Auditor_Master objAuditor = new BM_Auditor_Master();

                            int AuditorTye = 0;
                            int Catergory = 0;
                            if (Category.ToUpper() == "INDIVIDUAL")
                            {
                                Catergory = 1;
                            }
                            else if (Category.ToUpper() == "FIRM")
                            {
                                Catergory = 2;
                            }

                            if (Type.ToLower() == "statutory")
                            {
                                AuditorTye = 1;
                            }
                            else if (Type.ToLower() == "internal")
                            {
                                AuditorTye = 2;
                            }
                            else if (Type.ToLower() == "secretarial")
                            {
                                AuditorTye = 3;
                            }
                            else if (Type.ToLower() == "cost")
                            {
                                AuditorTye = 4;
                            }

                            BM_Auditor_Master objauditor = new BM_Auditor_Master();
                            objauditor.Audit_Type = AuditorTye;
                            objauditor.FullName_FirmName = NameofAuditor_AuditorFirms;
                            objauditor.Category_Id = Catergory;
                            objauditor.Email_Id = Email;
                            objauditor.MobileNo = MobileNumber;
                            objauditor.PAN = Pan;
                            objauditor.Reg_MemberNumber = MembershipNo_RegistrationNo;
                            objauditor.AddressLine1 = AddressLine1;
                            objauditor.AddressLine2 = AdressLine2;
                            objauditor.Country_Id = CountryId;
                            objauditor.state_Id = StateId;
                            objauditor.city_Id = CityId;

                            if (!string.IsNullOrEmpty(Pin))
                                objauditor.Pin_Number = Convert.ToInt32(Pin);

                            objauditor.CustomerId = Customer_Id;
                            objauditor.IsActive = true;
                            objauditor.CreatedOn = DateTime.Now;
                            objauditor.Createdby = UserId;

                            saveSuccess = obj.ChecksaveUpdateForAuditorMaster(objauditor);
                        }
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ViewBag.ErrorMessage = errorMessage;
                        }
                    }

                    if (saveSuccess)
                    {
                        ViewBag.SuccessMessage = "Auditor Details Uploaded Successfully";
                    }
                    else
                    {
                        ViewBag.Error = "No data in excel File";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                ViewBag.ErrorMessage = errorMessage;
            }
        }

        private bool CheckAuditorsWorkSheet(ExcelPackage PrivatePublicexcel, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in PrivatePublicexcel.Workbook.Worksheets)
                {
                    if (data.Equals("AuditorSheet"))
                    {
                        if (sheet.Name.Trim().Equals("AuditorSheet"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }

        public ActionResult DownloadSampleFormateforAuditorMaster()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AuditorUploadDocument\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "Auditor.xlsx");
            string fileName = "Auditor.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult UploadAuditorPopup()
        {
            VM_AuditUpload objAuditorUpload = new VM_AuditUpload();
            return PartialView("_AuditorsUpload", objAuditorUpload);
        }
    }
}