﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using BM_ManegmentServices;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MyComplianceDocumentController : Controller
    {
        // GET: BM_Management/MyComplianceDocument
        public ActionResult Index()
        {
            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR || AuthenticationHelper.Role == SecretarialConst.Roles.SMNGT)
            {
                return View();
            }
            else
            {
                return View("MyDocumentsCS");
            }
        }

        public ActionResult MyDrive()
        {
            return View("MyDrive");
        }
    }
}