﻿using GleamTech.DocumentUltimate.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class Document_ViewerController : Controller
    {
        // GET: BM_Management/Document_Viewer
        public ActionResult Viewer()
        {
            var documentViewer = new DocumentViewer
            {
                Width = 800,
                Height = 600,
                Resizable = true,
                Document = @"~\Areas\BM_Management\Documents\Sample_5.docx"
            };
            return PartialView("_Viewer", documentViewer);
        }
    }
}