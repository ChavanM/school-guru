﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MeetingComplianceAssignController : Controller
    {

        IMeeting_Compliances objIMeeting_Compliances;
        IUser objIUser;
        public MeetingComplianceAssignController(IMeeting_Compliances obj, IUser objUser)
        {
            objIMeeting_Compliances = obj;
            objIUser = objUser;
        }
        // GET: BM_Management/Meeting_Compliances/Compliances
        public ActionResult MeetingComplianceAssignment(long meetingId)
        {
            dynamic model = new ExpandoObject();
            model.MeetingID = meetingId;
            PopulateUsers();
            return PartialView("MeetingComplianceAssignment", model);
        }
        public ActionResult ComplianceAssignment_Read([DataSourceRequest] DataSourceRequest request, long meetingId, string mappingType)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("MappingType", System.ComponentModel.ListSortDirection.Descending));
                request.Sorts.Add(new SortDescriptor("AssignmentID", System.ComponentModel.ListSortDirection.Ascending));
            }

            return Json(objIMeeting_Compliances.GetMeetingComplianceScheduleAssignment(meetingId, mappingType).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingCustom_Create([DataSourceRequest] DataSourceRequest request,
    [Bind(Prefix = "models")]IEnumerable<MeetingComplianceScheduleDetails_ResultVM> lstComplianceAssignments)
        {
            var results = new List<MeetingComplianceScheduleDetails_ResultVM>();

            if (lstComplianceAssignments != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                objIMeeting_Compliances.MeetingComplianceScheduleReAssignment(lstComplianceAssignments.ToList(), UserId);
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }
        private void PopulateUsers()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var categories = objIUser.GetUsers(customerId);
            ViewData["categories"] = categories;
            ViewData["defaultCategory"] = categories.First();
        }


        public ActionResult MeetingComplianceSchedule(long meetingID)
        {
            return RedirectToAction("MeetingComplianceSchedule", "Meeting_Compliances", new { meetingId  = meetingID });
        }
    }
}