﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Annotation;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class AnnotationController : Controller
    {
        IAnnotationService objIAnnotationService;
        public AnnotationController(IAnnotationService objAnnotationService)
        {
            objIAnnotationService = objAnnotationService;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }

        public ActionResult AgendaAnnotate(long meetingId, long meetingParticipantId, string docType)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var role = AuthenticationHelper.Role;
            var model = objIAnnotationService.GetAnnotation(meetingId, meetingParticipantId, docType, userId, role, customerId);
            return View(model);
        }
    }
}