﻿
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class PageAuthenticationMasterController : Controller
    {
        IPageAuthentication obj;
        IUser objIUser;

        public PageAuthenticationMasterController(IPageAuthentication obj, IUser objIUser)
        {
            this.obj = obj;
            this.objIUser = objIUser;
        }
        // GET: BM_Management/PageAuthenticationMaster
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AuthenticationMaster()
        {
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;
            VM_pageAuthentication obj = new VM_pageAuthentication();
            if (authRecord != null)
            {
                obj.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Page Authorization") select x.Addview).FirstOrDefault();
                obj.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Page Authorization") select x.Editview).FirstOrDefault();
            }
            return View(obj);
        }
        //    [AcceptVerbs(HttpVerbs.Post)]
        //     public ActionResult UpdateCreateDelete(List<VM_pageAuthentication> updatedComponentTypes)
        // //[Bind(Prefix = "updated")]List<VM_pageAuthentication> updatedComponentTypes,
        //// [Bind(Prefix = "new")]List<VM_pageAuthentication> newComponentTypes)

        //     {
        //         int CustomerId = (int)AuthenticationHelper.CustomerID;
        //         if (updatedComponentTypes != null)
        //         {
        //             updatedComponentTypes = obj.UpdateAuthentication(updatedComponentTypes, CustomerId);
        //         }
        //         //if (newComponentTypes != null)
        //         //{
        //         //    newComponentTypes = obj.UpdateAuthentication(newComponentTypes, CustomerId);
        //         //}

        //         return Json(updatedComponentTypes.ToDataSourceResult(request, ModelState, product => new VM_pageAuthentication
        //         {

        //         }));
        //     }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateCreateDelete([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] List<VM_pageAuthentication> updatedComponentTypes)
        {

            int UserId = 0;
            List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
            // Will keep the updated entitites here. Used to return the result later.
            int CustomerId = (int)AuthenticationHelper.CustomerID;
            int loggedInUserID = AuthenticationHelper.UserID;

            updatedComponentTypes = obj.UpdateAuthentication(updatedComponentTypes, CustomerId, loggedInUserID);
            if (updatedComponentTypes.Count > 0)
            {
                foreach (var items in updatedComponentTypes)
                {
                    UserId = items.UserId;
                }
            }
            String key = "Authenticate" + UserId;
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;
            if (authRecord != null)
            {
                HttpContext.Cache.Remove(key + "authrecord");
                HttpContext.Cache.Remove(key + "Page");
            }
            return Json(new[] { updatedComponentTypes }.ToTreeDataSourceResult(request, ModelState));
        }

        public JsonResult All([DataSourceRequest] DataSourceRequest request, int UserId, int EntityId, string Role)
        {
            int CustomerId = (int)AuthenticationHelper.CustomerID;
            var assets = obj.GetPageAuthentication(UserId, CustomerId, EntityId, Role);

            var result = assets.Select(e => new VM_pageAuthentication
            {
                Id = e.Id,
                PageName = e.PageName,
                UserId = UserId,
                parentId = e.parentId,
                DeleteView = e.DeleteView,
                Addview = e.Addview,
                Editview = e.Editview,
                Pageview = e.Pageview,
                hasChildren = assets.Where(s => s.parentId == e.Id).Count() > 0
            }).ToTreeDataSourceResult(request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public IEnumerable<RoleVM> GetRoleofsecretarial()
        {
            var objrole = new List<RoleVM>();
            objrole = objIUser.GetSecretarialRoles();
            return objrole;
        }


        [HttpGet]
        public IEnumerable<UserVM> GetUserRolewise(string Role, int CustomerId)
        {
            var objrole = new List<UserVM>();
            objrole = objIUser.GetUserRolewise(Role, CustomerId);
            return objrole;
        }
    }
}