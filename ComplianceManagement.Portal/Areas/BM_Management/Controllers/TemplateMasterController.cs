﻿using BM_ManegmentServices.VM;
using System;
using BM_ManegmentServices.Services.Masters;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using BM_ManegmentServices.Services.Meetings;
using BM_Manegment;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class TemplateMasterController : Controller
    {
        ITemplateservice objTemplateService;
        IMeeting_Compliances objTemplateCompliances;

        public TemplateMasterController(ITemplateservice objTemplateService, IMeeting_Compliances objTemplateCompliances)
        {
            this.objTemplateService = objTemplateService;
            this.objTemplateCompliances = objTemplateCompliances;
        }

        public ActionResult TemplateMaster()
        {
            var obj = new Template_MasterVM();
            obj.IsShorter = false;
            return View(obj);
        }
        public ActionResult MeetingTemplateMaster()
        {
            var obj = new Template_MasterVM();
            obj.MeetingTypeId = -1;
            obj.IsShorter = false;
            return View(obj);
        }

        [HttpPost]
        public ActionResult SaveMeetingMailTemplateMaster(Template_MasterVM _objTemplate)
        {
            if (ModelState.IsValid)
            {
                _objTemplate.userId = AuthenticationHelper.UserID;
                _objTemplate = objTemplateService.Add(_objTemplate);
            }
            return PartialView("_AddEditMeetingMailTemplateMaster", _objTemplate);
        }

        public ActionResult GetTemplatemaster(Template_MasterVM obj, string name)
        {
           obj = objTemplateService.GetTemplatemaster(obj);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult saveDetailsmaster(Template_MasterVM _objTemplate)
        {
            if (ModelState.IsValid)
            {
                    _objTemplate.userId = AuthenticationHelper.UserID;
                    _objTemplate = objTemplateService.Add(_objTemplate);
            }
            return PartialView("_AddEditTemplateMaster", _objTemplate);
        }
     }
}