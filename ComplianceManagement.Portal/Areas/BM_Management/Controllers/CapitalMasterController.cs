﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;

using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using static BM_ManegmentServices.VM.VMCapitalData;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class CapitalMasterController : Controller
    {
        ICapitalMaster objCapitalMaster;
        IShareholdingMaster objshares;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public CapitalMasterController(ICapitalMaster obj,IShareholdingMaster objshares)
        {
            objCapitalMaster = obj;
            this.objshares = objshares;
        }
        //// GET: CapitalMaster
        [PageViewFilter]
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        [PageViewFilter]
        public ActionResult GetCapitalMasterDtls(int EntityId)
        {
            long CapitalID = 0;
            Equity obj = new Equity();
            obj.AuthorizedCapital = new AuthorizedCapital();
            obj.AuthorizedCapital.Entity_Id = EntityId;
            obj.VMCapitalData = new VMCapitalData();
            obj.VMCapitalData.shares = new List<Shares>();
            obj.VMPrifrenscShare = new VMPrifrenscShare();
            obj.VMPrifrenscShare.pshares = new pShares();

            obj.VMUnclassified = new VMUnclassified();
            obj.VMDebentures = new VMDebentures();
            try
            {
                obj.AuthorizedCapital = objCapitalMaster.GetAuthorisedCapital(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId);
                if (obj.AuthorizedCapital != null)
                {

                    CapitalID = obj.AuthorizedCapital.AuthorisedId;
                    obj.VMCapitalData.Id = obj.AuthorizedCapital.AuthorisedId;
                    obj.VMPrifrenscShare.Id = obj.AuthorizedCapital.AuthorisedId;
                    obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                    obj.VMDebentures.Id = obj.AuthorizedCapital.AuthorisedId;

                    obj.VMCapitalData = objCapitalMaster.GetEquityCapital(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, obj.AuthorizedCapital.AuthorisedId);
                    if (obj.VMCapitalData != null)
                    {
                        if (obj.VMCapitalData.Id > 0)
                        {
                            var _objEquietysub_Dtls = objCapitalMaster.GetEquityCapital_subdtls(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), obj.VMCapitalData.Id, EntityId);

                            if (_objEquietysub_Dtls.Count > 0)
                            {
                                obj.VMCapitalData.shares = new List<Shares>();
                                obj.VMCapitalData.count = _objEquietysub_Dtls.Count;
                                obj.VMCapitalData.shares.AddRange(_objEquietysub_Dtls);
                            }
                            else
                            {
                                obj.VMCapitalData.shares = new List<Shares>();
                                obj.VMCapitalData.shares.Add(new Shares());
                            }
                        }
                        else
                        {
                            obj.VMCapitalData = new VMCapitalData();
                            obj.VMCapitalData.shares = new List<Shares>();
                            obj.VMCapitalData.shares.Add(new Shares());
                        }

                        obj.VMPrifrenscShare = objCapitalMaster.GetPrifrenceCapital(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, CapitalID);
                        if (obj.VMPrifrenscShare != null)
                        {
                            var _objPrifrencelist = objCapitalMaster.GetPrifrenceCapitalList(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, CapitalID);

                            if (_objPrifrencelist != null)
                            {
                                obj.VMPrifrenscShare.pshares = new pShares();

                                obj.VMPrifrenscShare.pshares = _objPrifrencelist;
                            }
                            else
                            {
                                obj.VMPrifrenscShare.pshares = new pShares();


                            }
                        }
                        else
                        {
                            obj.VMPrifrenscShare = new VMPrifrenscShare();
                            obj.VMPrifrenscShare.pshares = new pShares();

                        }
                        obj.VMUnclassified = objCapitalMaster.GetUnclassified(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, obj.AuthorizedCapital.AuthorisedId);



                        if (obj.VMUnclassified != null)
                        {
                            obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                        }
                        else
                        {
                            obj.VMUnclassified = new VMUnclassified();
                            obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                        }

                        obj.VMDebentures = objCapitalMaster.GetDebenger(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, obj.AuthorizedCapital.AuthorisedId);


                        if (obj.VMDebentures == null)
                        {

                            obj.VMDebentures = new VMDebentures();
                            obj.VMDebentures.Id = obj.AuthorizedCapital.AuthorisedId;
                        }
                    }
                    else
                    {
                        obj.VMCapitalData = new VMCapitalData();
                    }
                }
                else
                {
                    obj.AuthorizedCapital = new AuthorizedCapital();
                    obj.AuthorizedCapital.Entity_Id = EntityId;

                    obj.VMPrifrenscShare = new VMPrifrenscShare();
                    obj.VMPrifrenscShare.pshares = new pShares();

                }
                string EntityName = string.Empty;
                if (EntityId > 0)
                {
                    EntityName = objCapitalMaster.getEntityName(EntityId);
                }
                obj.EntityName = EntityName;
                obj.EntityId = EntityId;
                //obj.AuthorizedCapital.paidupCapita= objshares.gettotalprefrenceCapital(EntityId, CustomerId);
				obj.AuthorizedCapital.paidupCapita= objshares.gettotalpaidupcapitalinamCapital(EntityId, CustomerId);
                return View(obj);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return View(obj);
            }
        }
        [HttpGet]
        public ActionResult GetPaidupCapital(int EntityId)
        {
			decimal paidupcapital= objshares.gettotalpaidupcapitalinamCapital(EntityId, CustomerId);
            //decimal paidupcapital= objshares.gettotalprefrenceCapital(EntityId, CustomerId);
            return Json(paidupcapital, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [PageViewFilter]
        public ActionResult ADDEquiety(string submitButton, VMCapitalData objEquity)
        {

            VMCapitalData _objEquiety = new VMCapitalData();
            try
            {
                if (submitButton == null)
                {
                    if (objEquity.shares == null)
                    {
                        objEquity.shares = new List<Shares>();
                    }

                    if (objEquity.count < objEquity.shares.Count)
                    {
                        List<Shares> lstTemp = new List<Shares>();
                        for (int i = 0; i < objEquity.count; i++)
                        {
                            lstTemp.Add(objEquity.shares[i]);
                        }
                        objEquity.shares = lstTemp;
                    }
                    else
                    {
                        for (int i = objEquity.shares.Count; i < objEquity.count; i++)
                        {

                            objEquity.shares.Add(new Shares());
                        }
                    }
                    return PartialView("_EquietySharesCapital", objEquity);
                }


                else if (submitButton == "Save")
                {

                    _objEquiety = objCapitalMaster.AddEquietydtls(objEquity, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                    ModelState.Clear();
                    return PartialView("_EquietySharesCapital", _objEquiety);
                }
                else
                {
                    return PartialView("_EquietySharesCapital", objEquity);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return View(_objEquiety);
            }
        }

        [HttpPost]
        [PageViewFilter]
        public PartialViewResult ADDprifrence(string submitButton, VMPrifrenscShare objPrefrence)
        {
            VMPrifrenscShare _objPrefrence = new VMPrifrenscShare();
            try
            {
                if (submitButton == "Save")
                {
                    _objPrefrence = objCapitalMaster.AddPrefrence(objPrefrence, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                    ModelState.Clear();
                    return PartialView("_PreferenceShareCapital", _objPrefrence);
                }
                else
                {
                    return PartialView("_PreferenceShareCapital", objPrefrence);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_PreferenceShareCapital", objPrefrence);
            }
        }

        [HttpPost]
        [PageViewFilter]
        public PartialViewResult AddAuthorizedCapital(string submitButton, AuthorizedCapital objauhorizedCapital)
        {
            AuthorizedCapital objAuthorized = new AuthorizedCapital();
            if (objauhorizedCapital.AuthorizedCapval >= 0)
            {

                if (objauhorizedCapital.Entity_Id > 0)
                {
                    objAuthorized = objCapitalMaster.SaveAuthorisedCapital(objauhorizedCapital, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                    ModelState.Clear();
                    return PartialView("_AuthorizedCapital", objAuthorized);
                }
                else
                {
                    return PartialView("_AuthorizedCapital", objauhorizedCapital);
                }

            }
            else
            {
                return PartialView("_AuthorizedCapital", objauhorizedCapital);
            }

        }

        [HttpPost]
        [PageViewFilter]
        public PartialViewResult AddUnclassifiedCapital(string submitButton, VMUnclassified objUnclassified)
        {


            VMUnclassified UnAuhorizeddtls = new VMUnclassified();
            try
            {
                if (objUnclassified != null)
                {

                    UnAuhorizeddtls = objCapitalMaster.SaveUnClassified(objUnclassified, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                    ModelState.Clear();
                    return PartialView("_UnclassifiedSharesCapital", UnAuhorizeddtls);
                }
                else
                {
                    return PartialView("_UnclassifiedSharesCapital", objUnclassified);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView(objUnclassified);
            }

        }

        [HttpPost]
        [PageViewFilter]
        public PartialViewResult AddDebenger(string submitButton, VMDebentures objDebenture)
        {
            VMDebentures debengerdtls = new VMDebentures();
            try
            {
                if (objDebenture != null)
                {

                    debengerdtls = objCapitalMaster.SaveDebengerdtls(objDebenture, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                    ModelState.Clear();
                    return PartialView("_DebenturesCapital", debengerdtls);
                }
                else
                {
                    return PartialView("_DebenturesCapital", objDebenture);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_DebenturesCapital", objDebenture);
            }
        }

        [HttpPost]
        [PageViewFilter]
        public JsonResult IsGreaterThenAuthorized(decimal TnAuthorisedCapital)
        {
            bool isGreterThenAuthorized = false;

            try

            {
                decimal? getAuthorizedCapital = (from row in entities.BM_CapitalMaster
                                                 where row.CustomerId == CustomerId
                                                 && row.IsActive == true
                                                 select row.AuthorizedCapital).FirstOrDefault();

                if (TnAuthorisedCapital <= getAuthorizedCapital)
                {
                    isGreterThenAuthorized = true;
                }
                else
                {
                    isGreterThenAuthorized = false;
                }


                return Json(isGreterThenAuthorized, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }
        }


        //for LLp Action Method
        [HttpGet]
        [PageViewFilter]
        public ActionResult GetLLpCapitalMaster(int EntityId)
        {
            VMllpCapital obj = new VMllpCapital();
            string EntityName = string.Empty;
            if (EntityId > 0)
            {
                EntityName = objCapitalMaster.getEntityName(EntityId);
            }
            obj.EntityName = EntityName;
            obj.Entity_Id = EntityId;
            ViewBag.EntityId = EntityId;
            return View(obj);
        }

        //for BoadyCorporate
        [HttpGet]
        [PageViewFilter]
        public ActionResult GetBodyCorporateCapitalMaster(int EntityId)
        {
            VMBodyCorporate obj = new VMBodyCorporate();
            string EntityName = string.Empty;
            if (EntityId > 0)
            {
                EntityName = objCapitalMaster.getEntityName(EntityId);
            }
            obj.EntityName = EntityName;
            obj.EntityId = EntityId;
            ViewBag.EntityId = EntityId;
            return View(obj);
        }
        [PageViewFilter]
        public ActionResult Edit(int id, int EntityId)
        {
            VMllpCapital getCapitalLLp = new VMllpCapital();
            try
            {
                if (id > 0 && EntityId > 0)
                {
                    getCapitalLLp = objCapitalMaster.EditCapitalLLP(id, EntityId);
                    getCapitalLLp.Entity_Id = EntityId;
                    return PartialView("_PartialViewLLpCapital", getCapitalLLp);
                }
                else
                {
                    getCapitalLLp.Entity_Id = EntityId;
                    return PartialView("_PartialViewLLpCapital", getCapitalLLp);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_PartialViewLLpCapital", getCapitalLLp);
            }
        }

        [HttpPost]
        [PageViewFilter]
        public ActionResult CreateLLPCapitalMaster(VMllpCapital _ObjllpCapita)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    VMllpCapital objgetllp = new VMllpCapital();
                    if (_ObjllpCapita.Id == 0)
                    {
                        objgetllp = objCapitalMaster.AddLLpCapitalMaster(_ObjllpCapita, CustomerId);

                    }
                    else
                    {
                        objgetllp = objCapitalMaster.UpdateLLpCapitalMaster(_ObjllpCapita, CustomerId);
                    }
                    return PartialView("_PartialViewLLpCapital", objgetllp);
                }
                else
                {
                    return PartialView("_PartialViewLLpCapital", _ObjllpCapita);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_PartialViewLLpCapital", _ObjllpCapita);
            }
        }

        [HttpGet]
        [PageViewFilter]
        public ActionResult EditCreateBodyCorporate(int id, int EntityId)
        {
            VMBodyCorporate getCapitalbodyCorporate = new VMBodyCorporate();
            try
            {
                if (id > 0 && EntityId > 0)
                {
                    getCapitalbodyCorporate = objCapitalMaster.EditCapitalBodyCorporate(id, EntityId);
                    getCapitalbodyCorporate.EntityId = EntityId;
                    return PartialView("_PartialViewBodyCorporate", getCapitalbodyCorporate);
                }
                else
                {
                    getCapitalbodyCorporate.EntityId = EntityId;
                    return PartialView("_PartialViewBodyCorporate", getCapitalbodyCorporate);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_PartialViewBodyCorporate", getCapitalbodyCorporate);
            }
        }


        [HttpPost]
        [PageViewFilter]
        public ActionResult CreateBodyCorporateCapitalMaster(VMBodyCorporate _objbodycorporate)
        {
            VMBodyCorporate obj = new VMBodyCorporate();
            try
            {
                if (ModelState.IsValid)
                {
                    if (_objbodycorporate.EntityId > 0 && _objbodycorporate != null)
                    {
                        if (_objbodycorporate.Id == 0)
                        {
                            obj = objCapitalMaster.AddBodyCorporateCapitalMaster(_objbodycorporate, CustomerId);
                        }
                        else
                        {
                            obj = objCapitalMaster.Update_BodyCorporateCapitalMaster(_objbodycorporate, CustomerId);
                        }

                    }
                    return PartialView("_PartialViewBodyCorporate", obj);
                }
                else
                {
                    return PartialView("_PartialViewBodyCorporate", _objbodycorporate);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_PartialViewBodyCorporate", _objbodycorporate);
            }

        }

    }
}