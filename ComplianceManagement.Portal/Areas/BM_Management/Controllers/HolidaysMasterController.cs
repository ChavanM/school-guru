﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class HolidaysMasterController : Controller
    {
        IHoliday obj;

        public HolidaysMasterController(IHoliday obj)
        {
            this.obj = obj;
        }
        // GET: BM_Management/HolidaysMaster
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HolidayMaster()
        {
            return View();
        }


        [PageViewFilter]
        public ActionResult Edit_AddHolidays(int id)
        {
            VMHolidayMaster _objholiday = new VMHolidayMaster();
            try
            {
                if (id > 0)
                {
                    _objholiday = obj.GetHolidaybyId(id);
                }
                return PartialView("_Add_EditHodiday", _objholiday);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_Add_EditHodiday", _objholiday);
            }
        }

        [HttpPost]
        public ActionResult CreateUpdateHolidays(VMHolidayMaster _objholiday)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_objholiday.Id == 0)
                    {
                        _objholiday = obj.CreateHoliday(_objholiday);
                    }
                    else
                    {
                        _objholiday = obj.UpdateHoliday(_objholiday);
                    }
                }
                return PartialView("_PartialviewHoliday", _objholiday);
            }
            catch (Exception ex)
            {
                return PartialView("_PartialviewHoliday", _objholiday);
            }
        }

        [HttpPost]
        public ActionResult UploadHolidaydata(VMHolidayMaster _objHoliday)
        {
            if (_objHoliday.File != null)
            {
                if (_objHoliday.File.ContentLength > 0)
                {
                    string excelfileName = string.Empty;
                    //string path = "~/Areas/BM_Management/Documents/" + AuthenticationHelper.CustomerID + "/HolidayDetails/";
                    string path = "~/Areas/BM_Management/Documents/HolidayDetails/";
                    string _file_Name = Path.GetFileName(_objHoliday.File.FileName);
                    string _path = Path.Combine(Server.MapPath(path), _file_Name);
                    bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(path));

                    _objHoliday.File.SaveAs(_path);

                    FileInfo excelfile = new FileInfo(_path);
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = TransactionSheetsforStateExitsts(xlWorkbook, "HolidayStateWise");
                            bool flags = TransactionSheetsforStockExchangeExitsts(xlWorkbook, "HolidayStockExchangeWise");
                            if (flag == true)
                            {
                                StateWiseUpload(xlWorkbook);
                            }
                            else if (flags == true)
                            {
                                StockExchangeUpload(xlWorkbook);
                            }
                            else
                            {
                                if (flag == false)
                                {
                                    ViewBag.Error = "No Data Found in Excel Document or Sheet Name must be 'HolidayStateWise'.";
                                }
                                else if (flags == false)
                                {
                                    ViewBag.Error = "No Data Found in Excel Document or Sheet Name must be 'HolidayStockExchangeWise'.";
                                }
                            }
                        }
                    }
                    else
                    {
                        ViewBag.Error = "Please Upload File'.";
                    }
                }
                else
                {
                    ViewBag.Error = "Please Upload File'.";
                }
            }
            else
            {
                ViewBag.Error = "Please Upload File'.";
            }

            return PartialView("_partialviewforHolidayUpload");
        }

        private void StockExchangeUpload(ExcelPackage xlWorkbook)
        {
            List<string> errorMessage = new List<string>();
            DateTime holidaydate = DateTime.Now;
            try
            {
                bool saveSuccess = false;

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["HolidayStockExchangeWise"];
                if (xlWorksheet != null)
                {
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string StockExchange = string.Empty;
                    string HolidayDate = string.Empty;
                    string Holiday_Discription = string.Empty;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        StockExchange = string.Empty;
                        HolidayDate = string.Empty;
                        Holiday_Discription = string.Empty;

                        #region 1 StockExchange
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                        {
                            StockExchange = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            if (string.IsNullOrEmpty(StockExchange))
                            {
                                errorMessage.Add("Please Check the Stock Exchange can not be empty at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 2 Holiday Date
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                        {
                            HolidayDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                        }
                        if (string.IsNullOrEmpty(HolidayDate))
                        {
                            errorMessage.Add("Please Check the Holiday Date can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 3 Holiday Description
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                        {
                            Holiday_Discription = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Holiday_Discription))
                        {
                            errorMessage.Add("Required Holiday Description at row number - " + (count + 1) + "");
                        }
                        #endregion
                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            StockExchange = string.Empty;
                            HolidayDate = string.Empty;
                            Holiday_Discription = string.Empty;

                            #region 1 StockExchange
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                            {
                                StockExchange = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 HolidayDate
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                            {
                                HolidayDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            }
                            #endregion

                            #region 3 Holiday_Discription
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                            {
                                Holiday_Discription = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                            }
                            #endregion

                            if (HolidayDate != null)
                            {
                                holidaydate = Convert.ToDateTime(HolidayDate);
                            }

                            BM_HolidayMaster objholiday = new BM_HolidayMaster();

                            int getStockExchangeval = obj.GetStockExchangevalue(StockExchange.Trim());
                            if (getStockExchangeval > 0)
                            {
                                BM_HolidayMaster objholidays = new BM_HolidayMaster();
                                objholiday.Stockexchange = getStockExchangeval;
                                objholiday.HolidaysDate = holidaydate;
                                objholiday.Holiday_Discription = Holiday_Discription;
                                objholiday.IsActive = true;
                                objholiday.IsSataeorSX = false;

                                saveSuccess = obj.CheckSaveorUpdateforstockexchange(objholiday);
                            }
                            else
                            {
                                errorMessage.Add("Stock Exchange Name Not Found.");
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ViewBag.ErrorMessage = errorMessage;
                        }
                    }

                    if (saveSuccess)
                    {
                        ViewBag.SuccessMessage = "Stock Exchange Details Uploaded Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                ViewBag.ErrorMessage = errorMessage;
            }
        }

        private void StateWiseUpload(ExcelPackage xlWorkbook)
        {
            List<string> errorMessage = new List<string>();
            DateTime holidaydate = DateTime.Now;
            try
            {
                bool saveSuccess = false;


                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["HolidayStateWise"];
                if (xlWorksheet != null)
                {

                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;


                    string State = string.Empty;
                    string HolidayDate = string.Empty;
                    string Holiday_Discription = string.Empty;



                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        State = string.Empty;
                        HolidayDate = string.Empty;
                        Holiday_Discription = string.Empty;


                        #region 1 IsIn_No
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                        {
                            State = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            if (string.IsNullOrEmpty(State))
                            {
                                errorMessage.Add("Please Check the State can not be empty at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 2 Holiday Date
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                        {
                            HolidayDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                        }
                        if (string.IsNullOrEmpty(HolidayDate))
                        {
                            errorMessage.Add("Please Check Holiday Date can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 3 Holiday Discription
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                        {
                            Holiday_Discription = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Holiday_Discription))
                        {
                            errorMessage.Add("Required Holiday Discription at row number - " + (count + 1) + "");
                        }
                        #endregion
                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            State = string.Empty;
                            HolidayDate = string.Empty;
                            Holiday_Discription = string.Empty;

                            #region 1 IsIn_No
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                            {
                                State = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Dmat Account No
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                            {
                                HolidayDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            }
                            #endregion

                            #region 3 DP_ID
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                            {
                                Holiday_Discription = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                            }
                            #endregion


                            #endregion
                            if (HolidayDate != null)
                            {
                                holidaydate = Convert.ToDateTime(HolidayDate);
                            }
                            int getstateval = obj.GetStatevalue(State.Trim());
                            if (getstateval > 0)
                            {
                                BM_HolidayMaster objholiday = new BM_HolidayMaster();
                                objholiday.State = getstateval;
                                objholiday.HolidaysDate = holidaydate;
                                objholiday.Holiday_Discription = Holiday_Discription;
                                objholiday.IsActive = true;
                                objholiday.IsSataeorSX = true;

                                saveSuccess = obj.CheckSaveorUpdate(objholiday);
                            }
                            else
                            {
                                errorMessage.Add("State Name Not Found Please Check");
                            }
                        }
                    }
                }
                else
                {
                    if (errorMessage.Count > 0)
                    {
                        ViewBag.ErrorMessage = errorMessage;
                    }
                }

                if (saveSuccess)
                {
                    ViewBag.SuccessMessage = "State Holiday  Uploaded Successfully";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                ViewBag.ErrorMessage = errorMessage;
            }
        }

        private bool TransactionSheetsforStateExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("HolidayStateWise"))
                    {
                        if (sheet.Name.Trim().Equals("HolidayStateWise"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }

        private bool TransactionSheetsforStockExchangeExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("HolidayStockExchangeWise"))
                    {
                        if (sheet.Name.Trim().Equals("HolidayStockExchangeWise"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }


    }
}