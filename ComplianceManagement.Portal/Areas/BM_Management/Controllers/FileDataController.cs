﻿using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class FileDataController : Controller
    {
        IFileData_Service objIFileData_Service;
        IFileStorage_Service objIFileStorage_Service;

        public FileDataController(IFileData_Service objFileData_Service, IFileStorage_Service objFileStorage_Service)
        {
            objIFileData_Service = objFileData_Service;
            objIFileStorage_Service = objFileStorage_Service;

        }
        // GET: BM_Management/Document_Viewer
        public ActionResult Viewer()
        {
            return View();
        }

        public ActionResult DownloadFile(long id)
        {
            try
            {
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                var result = objIFileData_Service.GetFile(id, userId);
                if (result.FileData != null)
                {
                    return File(result.FileData, "application/force-download", result.FileName);
                }
                else
                {
                    return PartialView("_FileNotFound");
                }
            }
            catch (Exception ex)
            {
                return PartialView("_FileNotFound");
            }
        }

        public ActionResult DownloadSecretarialFile(long id)
        {
            try
            {
                var result = objIFileData_Service.GetSecretarialDocs(id);
                if (result.FileData != null)
                {
                    return File(result.FileData, "application/force-download", result.FileName);
                }
                else
                {
                    return PartialView("_FileNotFound");
                }
            }
            catch (Exception ex)
            {
                return PartialView("_FileNotFound");
            }
        }
        [HttpPost]
        public ActionResult DeleteSecretarialFile(long id, long meetingId, string docs)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIFileData_Service.DeleteSecretarialDocs(id, meetingId, docs, userID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSecretarialFileByMappingId(long id, long meetingAgendaMappingId, long meetingId)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIFileData_Service.DeleteSecretarialDocs(id, meetingAgendaMappingId, meetingId, userID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadFileOnFileStorage(long meetingId)
        {
            var model = new FileStorageDataVM();
            model.MeetingId = meetingId;
            return PartialView("_UploadFileOnFileStorage", model);
        }
        [HttpPost]
        public ActionResult UploadFile(FileStorageDataVM obj, long meetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            objIFileStorage_Service.UploadFile(obj, meetingId, customerId);
            return PartialView("_UploadFileOnFileStorage", meetingId);
        }
        public ActionResult GetUploadedFilesByMeetingId([DataSourceRequest] DataSourceRequest request, long meetingId)
        {
            return Json(objIFileStorage_Service.GetUploadedFilesByMeetingId(meetingId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadFileFromStorage(long id)
        {
            try
            {
                var obj = objIFileStorage_Service.GetFileDetails(id);
                if(obj != null)
                {
                    string fileExtension = Path.GetExtension(obj.FileName);
                    string fileName = obj.FileName;
                    string path = obj.FilePath;
                    string fileNameNew = obj.FileKey + fileExtension;

                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    CloudBlobContainer container = blobClient.GetContainerReference(path);
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileNameNew);
                    MemoryStream memStream = new MemoryStream();
                    blockBlob.DownloadToStream(memStream);
                    return File(memStream.ToArray(), blockBlob.Properties.ContentType.ToString(), fileName);
                }
                else
                {
                    return PartialView("_FileNotFound");
                }
            }
            catch (Exception ex)
            {
                return PartialView("_FileNotFound");
            }
        }
    }
}