﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.DirectorMeetings;
using BM_ManegmentServices.Services.Googleservices;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Dashboard;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Calendar.v3;
using Google.Apis.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class DirectorMeetingsController : Controller
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        IDirectorMeetings _objdirectorMeeting;
        IMainMeeting obj;
       //Prashant
        IMeeting_Service objIMeeting_Service;

        public DirectorMeetingsController(IDirectorMeetings _objdirectorMeeting, IMainMeeting obj, IMeeting_Service objIMeeting_Service)
        {
            this._objdirectorMeeting = _objdirectorMeeting;
            this.obj = obj;
            //Prashant
            this.objIMeeting_Service = objIMeeting_Service;
        }

        public static string[] Scopes =
        {
                                CalendarService.Scope.Calendar,
                                CalendarService.Scope.CalendarReadonly
                            };
        // GET: BM_Management/DirectorMeetings
        int userId = AuthenticationHelper.UserID;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UpcomingMeetings()
        {
            return View();
        }

        public ActionResult SeekAvailibility()
        {
            return View();
        }
        public ActionResult CircularResolution()
        {
            return View();
        }
        public ActionResult PastCircularResolution()
        {
            return View();
        }
        public ActionResult Meetings_Read_Participants([DataSourceRequest] DataSourceRequest request, long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return null;//Json(_objdirectorMeeting.GetMeetingParticipants(MeetingId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #region Created by Ruchi on 5th of May 2020 SeekAvability

        public ActionResult UpdateAvailability(string meetingAviability)
        {
            if (meetingAviability != null)
            {
                var model = JsonConvert.DeserializeObject<List<Aviability>>(meetingAviability);

                IEnumerable<MeetingVM> objmeetingvm = new List<MeetingVM>();
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                if (model.Count > 0)
                {
                    foreach (var item in model)
                    {
                        var obj = _objdirectorMeeting.UpdateSeekaviabilityResponses(item, UserID);
                    }
                }
            }
            return Json(meetingAviability, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Getdetailsofseekava([DataSourceRequest] DataSourceRequest request)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            return Json(_objdirectorMeeting.GetPedingAvailability(UserID).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion Created by Ruchi on 5th of May 2020

        #region Created by Ruchi on 5th of May 2020 Upcomming Meeting
        public ActionResult upcommingMeeting()
        {
            return View();
        }

        public ActionResult UpcommingMetting([DataSourceRequest] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;
            var result = _objdirectorMeeting.GetMeetingsForUpcommingMeeting(userID, customerId, AuthenticationHelper.Role);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        public ActionResult updateUpcommingresponse(MeetingListVM _objmeetingvm)
        {
            var result = _objdirectorMeeting.saveupdateupcomingMeetingdtlsbydir(_objmeetingvm, userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult MarkAttendence(long MeetingId, long ParticipantId)
        {
            int UserId = AuthenticationHelper.UserID;
            bool result = false;
            if (MeetingId > 0 && ParticipantId > 0)
            {
                result = _objdirectorMeeting.MarkAttendence(MeetingId, ParticipantId, UserId);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion Created by Ruchi on 5th of May 2020 Upcomming Meeting

        #region Created by Ruchi on 22nd of May 2020 Circural Resolution
        public ActionResult GetCircularResolution([DataSourceRequest] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;
            var result = _objdirectorMeeting.GetCircularResolution(userID, customerId, AuthenticationHelper.Role);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPastCircularresolution([DataSourceRequest] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;
            var result = _objdirectorMeeting.GetPastCircularResolution(userID, customerId, AuthenticationHelper.Role);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult AgendaDetailswithvotingnoting([DataSourceRequest] DataSourceRequest request, long MeetingId)
        {
            List<MeetingAgendaSummary> _objagendadeals = new List<MeetingAgendaSummary>();

            _objagendadeals = obj.GetAgendaMeetingwise(MeetingId, userId);

            return Json(_objagendadeals.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddUpdateCircularResponses([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<MeetingAgendaSummary> _objagendaresponse)
        {
            var results = new MeetingAgendaSummary();

            if (_objagendaresponse != null && ModelState.IsValid)
            {
                foreach (var vottings in _objagendaresponse)
                {
                    vottings.userID = AuthenticationHelper.UserID;
                    results = _objdirectorMeeting.AddCircularVotting(vottings);
                }
            }

            return Json("success", JsonRequestBehavior.AllowGet);
        }

        #endregion Created by Ruchi on 22nd of May 2020 Circural Resolution

        public ActionResult Createcalenderresponse(long Id)
        {
            var getMeetingDate = (from row in entities.BM_Meetings
                                  where row.MeetingID == Id
                                  select new MeetingDetailsforCal
                                  {
                                      MeetingDate = row.MeetingDate,
                                      MeetingId = row.MeetingID,
                                      MeetingType = row.BM_CommitteeComp.MeetingTypeName,
                                      venu = row.MeetingVenue,
                                      MeetingTitle = row.MeetingTime,
                                  }).FirstOrDefault();
            TempData["MeetingDetails"] = new MeetingDetailsforCal { MeetingDate = getMeetingDate.MeetingDate, MeetingType = getMeetingDate.MeetingType, venu = getMeetingDate.venu };
            if (string.IsNullOrWhiteSpace(GoogleOauthTokenService.OauthToken))
            {
                var redirectUri = GoogleCalendarSyncer.GetOauthTokenUri(this);
                return Redirect(redirectUri);
            }
            else
            {
                var success = GoogleCalendarSyncer.SyncToGoogleCalendar(this);
                if (!success)
                {
                    return Json("Token was revoked. Try again.");
                }
            }
            //return Redirect("BM_Management/DirectorMeetings/UpcomingMeetings");
            return Redirect(Request.UrlReferrer.ToString());
        }


        #region Meeting Agenda View  Prashant 22 Oct 2020
        public PartialViewResult CircularAgendaView(long MeetingId, long ParticipantId)
        {
           
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewAgenda(MeetingId, CustomerId);
            if (model != null)
            {
                model.ParticipantID_ = ParticipantId;
            }
            return PartialView("_CircularAgendaViewer", model);
        }
        #endregion

    }
}