﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MeetingInterestedPartyController : Controller
    {
        IMeetingAgendaInterestedParty objIInterestedParty;
        IDirectorMaster objIDirectorMaster;
        public MeetingInterestedPartyController(IMeetingAgendaInterestedParty obj, IDirectorMaster objIDirector)
        {
            objIInterestedParty = obj;
            objIDirectorMaster = objIDirector;
        }
        public ActionResult Editing_Custom(long? MeetingAgendaMappingID, int Entityt_Id)
        {
            if (MeetingAgendaMappingID == null)
            {
                MeetingAgendaMappingID = 0;
            }
            PopulateCategories(Entityt_Id);
            return PartialView("Editing_Custom", (long)MeetingAgendaMappingID);
        }
        public ActionResult EditingCustom_Read([DataSourceRequest] DataSourceRequest request, long? MeetingAgendaMappingID)
        {
            if (MeetingAgendaMappingID == null)
            {
                MeetingAgendaMappingID = 0;
            }
            return Json(objIInterestedParty.GetAll((long)MeetingAgendaMappingID).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingCustom_Update([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<MeetingAgendaInterestedPartyVM> products)
        {
            if (products != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var product in products)
                {
                    objIInterestedParty.Update(product, UserId);
                }
            }
            return Json(products.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingCustom_Create([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<MeetingAgendaInterestedPartyVM> products)
        {
            var results = new List<MeetingAgendaInterestedPartyVM>();

            if (products != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var product in products)
                {
                    objIInterestedParty.Create(product, UserId);
                    results.Add(product);
                }
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingCustom_Destroy([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<MeetingAgendaInterestedPartyVM> products)
        {
            int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            foreach (var product in products)
            {
                objIInterestedParty.Delete(product, UserId);
            }

            return Json(products.ToDataSourceResult(request, ModelState));
        }

        private void PopulateCategories(int Entityt_Id)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //BOD
            var categories = objIDirectorMaster.BOD(Entityt_Id, customerId, false);

            ViewData["categories"] = categories;
            ViewData["defaultCategory"] = categories.First();
        }
    }
}