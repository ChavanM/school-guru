﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.MeetingsHistory;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
  //  [CustomExceptionFilter]
    public class HistoricalDataController : Controller
    {

        IHistorical _objHistorical;
        public HistoricalDataController(IHistorical _objHistorical)
        {
            this._objHistorical = _objHistorical;
        }

        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
        // GET: BM_Management/HistoricalData

        //[CustomExceptionFilter]
        //[OutputCache(Duration = 20)]
        public ActionResult Index()
        {
            return View();
        }

        //[CustomExceptionFilter]
        //[OutputCache(Duration = 20)]
        public ActionResult GetHistoricalData([DataSourceRequest] DataSourceRequest request)
        {
            int userID = AuthenticationHelper.UserID;
            string userRole = AuthenticationHelper.Role;

            List<VM_HistoricalMeetings> _objgetdata = new List<VM_HistoricalMeetings>();
            _objgetdata = _objHistorical.GetHistoricalData(CustomerId, userID, userRole);
            return Json(_objgetdata.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

       // [CustomExceptionFilter]
        public ActionResult CreateHistoricalMeeting(VM_HistoricalMeetings _objHistoricalMeetings)
        {
            string view = string.Empty;
            if (ModelState.IsValid)
            {
                if (_objHistoricalMeetings.ID == 0)
                {
                    _objHistoricalMeetings = _objHistorical.CreateHistoricalData(_objHistoricalMeetings, CustomerId, UserId);
                    view = "_AddEditHistoricalMeetings";
                }
                else
                {
                    _objHistoricalMeetings = _objHistorical.UpdateHistoricalData(_objHistoricalMeetings, CustomerId, UserId);
                    view = "EditHistoricalData";
                }               
            }

            return PartialView(view, _objHistoricalMeetings);
        }

       // [CustomExceptionFilter]
        public ActionResult OpenPopUPForHistorialdata(int ID)
        {
            string view = string.Empty;
            VM_HistoricalMeetings _objHistoricaldata = new VM_HistoricalMeetings();
            if (ID > 0)
            {
                _objHistoricaldata = _objHistorical.GetHistoricalDataByID(ID);
                view = "EditHistoricalData";
            }
            else
            {
                view = "_AddEditHistoricalMeetings";
            }
            return PartialView(view, _objHistoricaldata);
        }

        //[CustomExceptionFilter]
        //[OutputCache(Duration = 20)]
        public ActionResult GetHistoricalFileData([DataSourceRequest] DataSourceRequest request,int HistoricalID,string status)
        {
            List<BM_SP_GetHistoricalFileData_Result> _objgetdata = new List<BM_SP_GetHistoricalFileData_Result>();
            _objgetdata = _objHistorical.GetHistoricalFileData(CustomerId, HistoricalID, status);
            return Json(_objgetdata.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Templates_Remove(string[] fileNames)
        {

            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/Areas/BM_Management/TempFolder"), fileName);
                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }
            return Content("");
        }

        public ActionResult ViewHistoricalData(long Id)
        {
            string view = _objHistorical.Viewdocument(Id);
            return Json(view);
        }

        public ActionResult DownloadHistoricalData(long Id)
        {
            bool view = _objHistorical.Downloadrecords(Id);
            return Json("");
        }

        [HttpGet]
        public JsonResult BindFYear()
        {
            List<FYearVM> _objFY = new List<FYearVM>();
            _objFY = _objHistorical.BindFY();
            return Json(_objFY, JsonRequestBehavior.AllowGet);
        }
        [HttpDelete]
        public ActionResult DeleteHistoricalData(int ID)
        {
            int userId = AuthenticationHelper.UserID;
            bool deletehisdata= _objHistorical.DeleteHisData(ID, userId);
            return Json(deletehisdata,JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DeleteHistoricalFile(int ID)
        {
            int userId = AuthenticationHelper.UserID;
            bool deletehisdata = _objHistorical.DeleteHisFile(ID, userId);
            return Json(deletehisdata, JsonRequestBehavior.AllowGet);
        }
    }
}