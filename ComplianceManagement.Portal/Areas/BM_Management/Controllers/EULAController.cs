﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.EULA;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.Services.Setting;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.EULA;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class EULAController : Controller
    {
        IEULA_Service objIEULA_Service;
        IFileData_Service objIFileData_Service;
        public EULAController(IEULA_Service objEULA_Service, IFileData_Service objFileData_Service)
        {
            objIEULA_Service = objEULA_Service;
            objIFileData_Service = objFileData_Service;
        }
        public ActionResult Details()
        {
            return View("EULA_Details");
        }
        public virtual JsonResult GetEULA_Details([DataSourceRequest] DataSourceRequest request)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIEULA_Service.GetEULADetails(userID);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditEULA_Details(int id, string type)
        {
            var model = objIEULA_Service.GetEULADetailsById(id, type);
            return PartialView("_ViewEULA_Details", model);
        }
        public ActionResult ViewEULA_Details()
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIEULA_Service.GetEULADetailsByUserId(userID);
            return PartialView("_ViewEULA_Details", model);
        }

        [HttpPost]
        public ActionResult SaveEULA_Details(EULA_DetailsVM model, string command_name)
        {
            if(model != null)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                if (command_name == "Approve")
                {
                    model = objIEULA_Service.SaveEULAStatus(model, 2, userID);
                }
                else if (command_name == "Reject")
                {
                    model = objIEULA_Service.SaveEULAStatus(model, 3, userID);
                }
                else if (command_name == "Upload")
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    model.CustomerID = customerID;
                    model.UserID = userID;
                    model = objIFileData_Service.SaveEULAFile(model);
                }
            }
            return PartialView("_ViewEULA_Details", model);
        }

        public ActionResult DownloadEULA(int id)
        {
            var fileName = Server.MapPath(objIEULA_Service.GetEULADocById(id));
            var bytes = objIFileData_Service.GetEULAFile(fileName);
            return File(bytes, "application/force-download", Path.GetFileName(fileName));
        }
    }
}