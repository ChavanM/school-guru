﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class ConfigurationController : Controller
    {
        // GET: BM_Management/Configuration
        IConfiguration obj;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        public ConfigurationController(IConfiguration obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Configuration()
        {
            VMConfiguration _objmeeting = new VMConfiguration();
            GetAllRules(_objmeeting.EntityId);
            return View();
        }

        public ActionResult PopForAddEdit(int? Id, int? EntityId)
        {
            VMConfiguration _objmeeting = new VMConfiguration();
            if (EntityId > 0)
            {
                _objmeeting.EntityId = (int)EntityId;
                //_objmeeting.Id = (int)Id;
                if (EntityId != null)
                    _objmeeting.DefaultVirtualMeeting = obj.GetIsDefaultVirtualMeetingByEntityId(Convert.ToInt32(EntityId));
                GetAllRules((int)EntityId);
            }
            return PartialView("_AddEditConfiguration", _objmeeting);
        }

        public ActionResult PopForAddEditCircularMeeting(int Id, int EntityId, string FY,string flag)
        {
            VMConfiguration _objmeeting = new VMConfiguration();
          
            if (Id > 0)
            {
                _objmeeting = obj.GetMeetingbyId(Id);
            }
            _objmeeting.EntityId = EntityId;
            _objmeeting.YearType = FY;
            if (EntityId > 0)
            {
                _objmeeting.DefaultVirtualMeeting = obj.GetIsDefaultVirtualMeetingByEntityId(EntityId);
                GetAllRules(EntityId);
            }
            return PartialView("_AddEditConfiguration", _objmeeting);
        }

        [HttpPost]
        public ActionResult AddEditMeeting(VMConfiguration _objmeeting)
        {
            if (ModelState.IsValid)
            {
                _objmeeting.CustomerId = CustomerId;
                if (_objmeeting.conId == 0)
                {
                    _objmeeting = obj.CreateMeeting(_objmeeting);
                }
                else
                {
                    _objmeeting = obj.UpdateMeeting(_objmeeting);
                }
            }
            GetAllRules(_objmeeting.EntityId);
            return PartialView("_AddEditConfiguration", _objmeeting);
        }
        public ActionResult ReloadConfiList(int EntityId)
        {
            GetAllRules(EntityId);
            return PartialView("_ConfiMeetingDetailsList");
        }
        
        public void GetAllRules(int EntityId)
        {
            ViewBag.lstMeetings = obj.getConfiguration(EntityId);
            //ViewBag.lastCircular = obj.lastCircular(EntityId);
        }

       [HttpPost]
        public ActionResult EditMeeting(int EntityId,int Id)
        {
            VMConfiguration _objcon = new VMConfiguration();
            _objcon=obj.GetMeetingbyId(Id);

            if (EntityId > 0 && _objcon != null)
            {
                _objcon.DefaultVirtualMeeting = obj.GetIsDefaultVirtualMeetingByEntityId(EntityId);
            }

            GetAllRules(EntityId);
            return PartialView("_AddEditConfiguration", _objcon);
        }
    }
}