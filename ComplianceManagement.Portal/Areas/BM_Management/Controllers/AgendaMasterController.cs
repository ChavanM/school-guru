﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Kendo.Mvc.Export;
using System.IO;
using System.Dynamic;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Services.Masters;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using BM_ManegmentServices;
using Kendo.Mvc.Extensions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class AgendaMasterController : Controller
    {
        IAgendaMaster objIAgendaMaster;
        public AgendaMasterController(IAgendaMaster obj)
        {
            objIAgendaMaster = obj;
        }

        public ActionResult Agenda()
        {
            long? CustomerID = AuthenticationHelper.CustomerID;
            if (AuthenticationHelper.Role == BM_ManegmentServices.SecretarialConst.Roles.CSIMP)
            {
                CustomerID = null;
                return View(CustomerID);
            }
            else
            {
                return View(CustomerID);
            }
        }
        public PartialViewResult GetAgenda(long id)
        {
            var role = AuthenticationHelper.Role;

            var showDefaultAgendaCtrl = role == SecretarialConst.Roles.CSIMP ? true : false;

            if (id == 0)
            {
                var model = new AgendaMasterVM() { BM_AgendaMasterId = 0, IsMultiStageAgenda = false, CanPassedByCircularResolution = false, IsPostBallotRequired = false, IsVoting = false, CanMultiple = false };
                model.ShowDefaultAgendaCtrl = showDefaultAgendaCtrl;
                return PartialView("_AddAgenda", model);
            }
            else
            {
                var model = objIAgendaMaster.GetAgenda(id);
                model.ShowDefaultAgendaCtrl = showDefaultAgendaCtrl;
                return PartialView("_EditAgenda", model);
            }
        }

        #region Customer-wise Standard Agenda Formates
        public PartialViewResult GetStandardAgenda(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAgendaMaster.GetStadardAgenda(customerID, id);
            return PartialView("_EditStandardAgenda", model);
        }
        [HttpPost]
        public PartialViewResult EditAgendaFormates(StandardAgendaFormatesVM obj)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                obj.CustomerID = customerId;
                obj = objIAgendaMaster.SaveAgendaFormates(obj, userId);
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation fails.";
            }
            ModelState.Clear();
            return PartialView("_EditStandardAgendaFormates", obj);
        }

        public PartialViewResult GetPreCommitteeStandardAgendaFormate(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAgendaMaster.GetPreCommitteeStadardAgenda(customerID, id);

            return PartialView("_PreCommitteeStandardAgendaFormates", model);
        }
        [HttpPost]
        public PartialViewResult EditPreCommitteeStandardAgendaFormate(StandardAgendaFormates_SubVM obj)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                obj.CustomerID = customerId;
                obj = objIAgendaMaster.SaveAgendaFormates(obj, userId);
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation fails.";
            }
            ModelState.Clear();
            return PartialView("_PreCommitteeStandardAgendaFormates", obj);
        }
        #endregion

        [NonAction]
        public void LoadSubAgenda(long ParentID)
        {
            //ViewBag.DetailsOfSubAgenda_Grid = objIAgendaMaster.GetAllSubAgendas(ParentID);
        }
        [NonAction]
        public void LoadSubAgendaNew(long ParentID)
        {
            ViewBag.DetailsOfSubAgenda_GridNew = objIAgendaMaster.GetAllSubAgendasNew(ParentID);
        }

        [HttpPost]
        public PartialViewResult CreateAgenda(AgendaMasterVM obj, string command_name)
        {
            obj.Message = new BM_ManegmentServices.VM.Response();
            string partialView = "_AddAgenda";
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var role = AuthenticationHelper.Role;

                obj.UserId = userID;

                var model = objIAgendaMaster.Create(obj, customerID, role);
                obj.BM_AgendaMasterId = model.BM_AgendaMasterId;
            }
            else
            {
                obj.Message.Error = true;
                obj.Message.Message = "Validation Fails.";
            }

            if (obj.BM_AgendaMasterId > 0)
            {
                partialView = "_EditAgenda";
                //obj.subAgendaMaster = new SubAgendaMasterVM() { BM_SubAgendaMasterId = 0, BM_AgendaMasterId = obj.BM_AgendaMasterId };
            }
            ModelState.Clear();
            return PartialView(partialView, obj);
        }

        [HttpPost]
        public PartialViewResult EditAgenda(AgendaMasterVM obj, string command_name)
        {
            obj.Message = new BM_ManegmentServices.VM.Response();
            if (ModelState.IsValid)
            {
                var role = AuthenticationHelper.Role;

                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.UserId = UserId;

                var model = objIAgendaMaster.Update(obj, role);
                obj.Message = model.Message;
            }
            else
            {
                obj.Message.Error = true;
                obj.Message.Message = "Validation fails.";
            }
            ModelState.Clear();
            return PartialView("_EditParentAgenda", obj);
        }

        #region Agenda Info
        [HttpPost]
        public PartialViewResult AgendaInfo(AgendaInfoVM obj)
        {
            if (ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                var model = objIAgendaMaster.SaveInfomativeText(obj, UserId);
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation fails.";
            }
            ModelState.Clear();
            return PartialView("_AgendaInfo", obj);
        }
        #endregion

        #region Pre Committee
        public PartialViewResult GetPreCommitteeAgenda(long id)
        {
            var model = objIAgendaMaster.GetSubAgenda(id);
            return PartialView("_PreCommitteeAgendaItemEdit", model);
        }

        public PartialViewResult GetPreCommitteeAgendaNew(long ParentId)
        {
            var model = new SubAgendaMasterVM() { BM_SubAgendaMasterId = 0, BM_AgendaMasterId = ParentId, CanPassedByCircularResolutionSub = false, IsPostBallotRequiredSub = false, IsVotingSub = false, SubAgendaInfomative = "" };
            return PartialView("_PreCommitteeAgendaItemEdit", model);
        }

        [HttpPost]
        public PartialViewResult CreateUpdatePreCommitteeAgendaItem(SubAgendaMasterVM obj, string command_name)
        {
            obj.Message = new BM_ManegmentServices.VM.Response();
            if (ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.UserId = UserId;
                if (obj.BM_SubAgendaMasterId == 0)
                {
                    obj = objIAgendaMaster.Create(obj);
                }
                else
                {
                    obj = objIAgendaMaster.Update(obj);
                }
            }
            else
            {
                obj.Message.Error = true;
                obj.Message.Message = "Validation fails.";
            }

            ModelState.Clear();
            return PartialView("_PreCommitteeAgendaItemEdit", obj);
        }

        #endregion

        #region Sub Agenda New
        public PartialViewResult GetSubAgendaMapping(long id)
        {
            var model = objIAgendaMaster.GetSubAgendaNew(id);
            model.ShowCtrl = true;
            return PartialView("_EditSubAgendaNew", model);
        }

        public PartialViewResult GetSubAgendaMappingNew(long ParentId)
        {
            var model = new AgendaMaster_SubAgendaMappging() { Agenda_SubAgendaMappgingID = 0, Parent_AgendaMasterId = ParentId };
            model.ShowCtrl = true;
            return PartialView("_EditSubAgendaNew", model);
        }

        [HttpPost]
        public PartialViewResult CreateUpdateSubAgendaMapping(AgendaMaster_SubAgendaMappging obj, string command_name)
        {
            obj.Message = new BM_ManegmentServices.VM.Response();
            if (ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.UserId = UserId;
                if (obj.Agenda_SubAgendaMappgingID == 0)
                {
                    obj = objIAgendaMaster.Create(obj);
                }
                else
                {
                    obj = objIAgendaMaster.Update(obj);
                }
            }
            else
            {
                obj.Message.Error = true;
                obj.Message.Message = "Validation fails.";
            }

            obj.ShowCtrl = true;

            ModelState.Clear();
            return PartialView("_EditSubAgendaNew", obj);
        }
        #endregion



        //[HttpPost]
        //public ActionResult Export(EditorExportData data)
        //{
        //    var settings = new EditorDocumentsSettings();
        //    data.ExportType = EditorExportType.Pdf;
        //    data.Value = "Hello";
        //    settings.HtmlImportSettings.LoadFromUri += HtmlImportSettings_LoadFromUri;
        //    try
        //    {
        //        return EditorExport.Export(data, settings);
        //    }
        //    catch (Exception ex)
        //    {
        //        return RedirectToAction("exportas", "editor");
        //    }
        //}

        private void HtmlImportSettings_LoadFromUri(object sender, Telerik.Windows.Documents.Flow.FormatProviders.Html.LoadFromUriEventArgs e)
        {
            var uri = e.Uri;
            var absoluteUrl = uri.StartsWith("http://") || uri.StartsWith("www.");
            if (!absoluteUrl)
            {

                var filePath = Server.MapPath(uri);
                using (var fileStream = System.IO.File.OpenRead(filePath))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        fileStream.CopyTo(memoryStream);
                        e.SetData(memoryStream.ToArray());
                    }
                }
            }
        }

        #region Add New Compliances No added By Ruchi
        public ActionResult AddNewCompliances()
        {
            NewCompliances_VM _objnewCompliances = new NewCompliances_VM();
            return PartialView("_AddNewComplianceNo", _objnewCompliances);
        }
        #endregion

        public ActionResult AgendaDetails()
        {
            string Role = AuthenticationHelper.Role;

            if (Role == "CSIMP")
            {
                PopulateCategories();
                return View();
            }

            return RedirectToAction("~/ComplianceManagement.Portal/Login.aspx");
        }


        private void PopulateCategories()
        {
            var listDocumentType = new List<DocumentTypeViewdata>
            {

             new DocumentTypeViewdata {Type="ALL",TypeName="ALL" },
             new DocumentTypeViewdata {Type="Partial",TypeName="Partial" },
             new DocumentTypeViewdata {Type="None",TypeName="None" },
             new DocumentTypeViewdata {Type="No Document",TypeName="No Document" }
            };
            ViewData["listDocumentTypeViewdata"] = listDocumentType;
            ViewData["defaultDocumentType"] = listDocumentType.First();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateAgendaDetails([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<AgendaMasterVM> AgendatDetails)
        {
            int? CustomerID = null;
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            bool result = false;
            string Role = AuthenticationHelper.Role;

            if (Role == "CSIMP")
            {
                CustomerID = null;
            }
            else
            {
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            if (AgendatDetails != null)
            {
                foreach (var agendadoctypeItem in AgendatDetails)
                {
                    result = objIAgendaMaster.UpdateAgendaDetails(agendadoctypeItem, UserID, CustomerID);

                    if (!result)
                    {
                        ModelState.AddModelError("", SecretarialConst.Messages.serverError);
                    }
                }
            }

            return Json(AgendatDetails.ToDataSourceResult(request, ModelState));
        }

        public ActionResult GetAgendaDetails([DataSourceRequest] DataSourceRequest request)
        {
            PopulateCategories();

            string Role = AuthenticationHelper.Role;

            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            List<AgendaMasterVM> objagendacompliance = new List<AgendaMasterVM>();


            objagendacompliance = objIAgendaMaster.GetAll_CustomerEntityTypeWise(CustomerId, Role, AuthenticationHelper.ProductApplicableLogin);

            return Json(objagendacompliance.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}