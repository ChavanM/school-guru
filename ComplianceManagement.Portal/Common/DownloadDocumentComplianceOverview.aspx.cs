﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class DownloadDocumentComplianceOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["FileID"]) && !string.IsNullOrEmpty(Request.QueryString["Version"]))
                {
                    int ScheduleOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);

                    string Version = Request.QueryString["Version"].ToString();
                    int FileID = Convert.ToInt32(Request.QueryString["FileID"]);

                    List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                    List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                    ComplianceDocument = GetFileData1(Convert.ToInt32(ScheduleOnID), Convert.ToInt32(FileID)).ToList();

                    if (Version.Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == Version).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == Version).ToList();
                    }
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        var compliancetype = DocumentManagement.GetComplianceTypeID(Convert.ToInt32(ScheduleOnID));
                        if (compliancetype == 1)
                        {
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(ScheduleOnID));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + Version);
                                if (ComplianceFileData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + Version + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                             i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(ScheduleOnID));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + Version);
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + Version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + Version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }

                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                #endregion
                            }
                        }
                        else
                        { int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(ScheduleOnID));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + Version);
                                if (ComplianceFileData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;

                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + Version + "/" + str, DocumentManagement.ReadDocFiles(filePath));

                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Region
                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(ScheduleOnID));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + Version);
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + Version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + Version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                #endregion
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<GetComplianceDocumentsView> GetFileData1(int ScheduledOnID,int fileid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetComplianceDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID
                                && row.FileID == fileid
                                select row).ToList();

                return fileData;
            }
        }
    }
}