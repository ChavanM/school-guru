﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ConfigurationCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {              
                BindCustomers();
                BindFrequencyDetail();
                BindComplianceTypes();
            }
        }
        private void BindFrequencyDetail()
        {
            try
            {
                var data = CustomerManagement.FrequencyDetails();
                ddlFrequencyList.DataTextField = "Frequency_Name";
                ddlFrequencyList.DataValueField = "FID";

                ddlFrequencyList.DataSource = data;
                ddlFrequencyList.DataBind();

                ddlFrequencyList.Items.Insert(0, new ListItem("Select Frequency", "-1"));

                ddlFrequencyList1.DataTextField = "Frequency_Name";
                ddlFrequencyList1.DataValueField = "FID";

                ddlFrequencyList1.DataSource = data;
                ddlFrequencyList1.DataBind();

                ddlFrequencyList1.Items.Insert(0, new ListItem("Select Frequency", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers()
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                ddlCustomerList.DataSource = CustomerManagement.GetAll("");
                ddlCustomerList.DataBind();

                //ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                //ddlCustomerList.SelectedItem(Convert.ToInt32(AuthenticationHelper.CustomerID));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindComplianceTypes()
        {
            try
            {
                long Frequency = -1;
                string FlagType = string.Empty;
                string ComplianceType = "S";

                if (ddlFrequencyList1.SelectedValue != "-1")
                {
                    Frequency = Convert.ToInt32(ddlFrequencyList1.SelectedValue);
                }
                if (ddlFrequencyType.SelectedValue != "-1")
                {
                    FlagType = ddlFrequencyType.SelectedValue;
                }
                if (ddlComplianceType.SelectedValue != "-1")
                {
                    ComplianceType = ddlComplianceType.SelectedValue;
                }

                var data = ConfigurationManagement.GetAllReminderTemplate(Convert.ToInt32(AuthenticationHelper.CustomerID), ComplianceType);
                if (data.Count > 0)
                {
                    if (Frequency != -1)
                    {
                        data = data.Where(x => x.Frequency == Frequency).ToList();
                    }
                    if (!string.IsNullOrEmpty(FlagType))
                    {
                        data = data.Where(x => x.Flag == FlagType).ToList();
                    }
                }

                grdFrequency.DataSource = data;
                grdFrequency.DataBind();
                upFinancialYearList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnManageReminders_Click(object sender, EventArgs e)
        {

            try
            {
                var reminderTemplates = ConfigurationManagement.GetAllReminderCustomerTemplates();

               
                cblMonthly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.Monthly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsActive }).ToList().ForEach(item =>
                {
                    cblMonthly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });

                cblQuarterly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.Quarterly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsActive }).ToList().ForEach(item =>
                {
                    cblQuarterly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });
                //added by rahul on 14 Jan 2015
                cblFourMonthly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.FourMonthly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsActive }).ToList().ForEach(item =>
                {
                    cblFourMonthly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });
                cblHalfYearly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.HalfYearly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsActive }).ToList().ForEach(item =>
                {
                    cblHalfYearly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });

                cblYearly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.Annual).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsActive }).ToList().ForEach(item =>
                {
                    cblYearly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });
                //added by rahul on 14 Jan 2015
                cblTwoYearly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.TwoYearly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsActive }).ToList().ForEach(item =>
                {
                    cblTwoYearly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });

                //added by rahul on 14 Jan 2015
                cblSevenYearly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.SevenYearly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsActive }).ToList().ForEach(item =>
                {
                    cblSevenYearly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });

               
                upReminderConfiguration.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divReminderConfiguration\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddFinancialYear_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxTimeInDays.Text = string.Empty;
                tbxTimeInDays.Text = string.Empty;
                txtRepeatDay.Text = string.Empty;

                upComplianceType.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceTypeDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            grdFrequency.PageIndex = 0;
            BindComplianceTypes();
        }
        protected void btnSaveReminderConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                List<ReminderTemplate> reminders = new List<ReminderTemplate>();

                foreach (ListItem item in cblMonthly.Items)
                {
                    reminders.Add(new ReminderTemplate()
                    {
                        ID = Convert.ToInt32(item.Value),
                        IsSubscribed = item.Selected
                    });
                }

                foreach (ListItem item in cblQuarterly.Items)
                {
                    reminders.Add(new ReminderTemplate()
                    {
                        ID = Convert.ToInt32(item.Value),
                        IsSubscribed = item.Selected
                    });
                }

                foreach (ListItem item in cblHalfYearly.Items)
                {
                    reminders.Add(new ReminderTemplate()
                    {
                        ID = Convert.ToInt32(item.Value),
                        IsSubscribed = item.Selected
                    });
                }

                foreach (ListItem item in cblYearly.Items)
                {
                    reminders.Add(new ReminderTemplate()
                    {
                        ID = Convert.ToInt32(item.Value),
                        IsSubscribed = item.Selected
                    });
                }

                ConfigurationManagement.UpdateReminderTemplates(reminders);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divReminderConfiguration\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool validation = false;
                if (DropDownList2.SelectedValue == "S" || DropDownList2.SelectedValue == "I")
                {
                    if (ddlFrequencyList.SelectedValue != "-1")
                    {
                        validation = true;
                    }
                    else
                    {
                        validation = false;
                    }
                }
                else
                {
                    validation = true;
                }

                if (validation)
                {
                    ReminderTemplate_CustomerMapping obj = new ReminderTemplate_CustomerMapping()
                    {                        
                        TimeInDays = Convert.ToInt16(tbxTimeInDays.Text),
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        IsActive = true,
                        Flag = DropDownList1.SelectedValue,
                        Type = DropDownList2.SelectedValue,
                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                        CreatedOn = DateTime.Now
                    };
                    if (ddlFrequencyList.SelectedValue != "-1")
                    {
                        obj.Frequency = Convert.ToByte(ddlFrequencyList.SelectedValue);
                    }
                    
                    if (!string.IsNullOrEmpty(txtRepeatDay.Text))
                    {
                        obj.RepeatEveryDays = Convert.ToInt32(txtRepeatDay.Text);
                    }
                    if (ConfigurationManagement.ReminderTemplate_CustomerMappingExist(obj))
                    {
                        cvDuplicateEntry.ErrorMessage = "Frequency already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        ConfigurationManagement.CreateReminder(obj);
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        ConfigurationManagement.UpdateReminder(obj);
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenAlert();", true);
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceTypeDialog\").dialog('close')", true);
                    BindComplianceTypes();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select Frequency.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

            //try
            //{
            //    FinancialYear financialYear = new FinancialYear()
            //    {
            //        Name = tbxName.Text,
            //    };

            //    if ((int)ViewState["Mode"] == 1)
            //    {
            //        financialYear.ID = Convert.ToInt32(ViewState["FinancialYearID"]);
            //    }

            //    if (ConfigurationManagement.Exists(financialYear))
            //    {
            //        cvDuplicateEntry.ErrorMessage = "Financial year already exists.";
            //        cvDuplicateEntry.IsValid = false;
            //        return;
            //    }

            //    if ((int)ViewState["Mode"] == 0)
            //    {
            //        ConfigurationManagement.Create(financialYear);
            //    }
            //    else if ((int)ViewState["Mode"] == 1)
            //    {
            //        ConfigurationManagement.Update(financialYear);
            //    }

            //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceTypeDialog\").dialog('close')", true);
            //    BindComplianceTypes();
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }
      
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
       
        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdFrequency_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int typeID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_FINANCIAL_YEAR"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["FinancialYearID"] = typeID;

                    FinancialYear userParameter = ConfigurationManagement.GetByID(typeID);
                    
                    upComplianceType.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceTypeDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_FREQUENCY"))
                {
                    ConfigurationManagement.DeleteFreqDays(typeID,Convert.ToInt32(AuthenticationHelper.UserID));
                    BindComplianceTypes();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdFrequency_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long Frequency = -1;
                string FlagType = string.Empty;
                string ComplianceType = "S";

                if (ddlFrequencyList1.SelectedValue != "-1")
                {
                    Frequency = Convert.ToInt32(ddlFrequencyList1.SelectedValue);
                }
                if (ddlFrequencyType.SelectedValue != "-1")
                {
                    FlagType = ddlFrequencyType.SelectedValue;
                }
                if (ddlComplianceType.SelectedValue != "-1")
                {
                    ComplianceType = ddlComplianceType.SelectedValue;
                }

                var data = ConfigurationManagement.GetAllReminderTemplate(Convert.ToInt32(AuthenticationHelper.CustomerID), ComplianceType);
                if (data.Count > 0)
                {
                    if (Frequency != -1)
                    {
                        data = data.Where(x => x.Frequency == Frequency).ToList();
                    }
                    if (!string.IsNullOrEmpty(FlagType))
                    {
                        data = data.Where(x => x.Flag == FlagType).ToList();
                    }
                }

                //var financialYearList = ConfigurationManagement.GetAllReminderTemplate(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (direction == SortDirection.Ascending)
                {
                    data = data.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    data = data.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdFrequency.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdFrequency.Columns.IndexOf(field);
                    }
                }

                grdFrequency.DataSource = data;
                grdFrequency.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdFrequency_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdFrequency_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdFrequency.PageIndex = e.NewPageIndex;
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFrequencyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceTypes();
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceTypes();
        }

        protected void ddlFrequencyList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceTypes();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                ddlFrequencyList.SelectedValue = "-1";
                tbxTimeInDays.Text = string.Empty;
                txtRepeatDay.Text = string.Empty;

                upComplianceType.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceTypeDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList2.SelectedValue != "-1")
            {
                if (DropDownList2.SelectedValue == "S" || DropDownList2.SelectedValue == "I")
                {
                    frequencydisplay.Visible = true;
                }
                else
                {
                    frequencydisplay.Visible = false;
                }
            }
        }
    }
}