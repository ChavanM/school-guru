﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class CertificateOverview : System.Web.UI.Page
    {
        static string sampleFormPath1 = "";
        public static string CompDocReviewPath = "";
        protected static string ActDocString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                {
                    int ActID = Convert.ToInt32(Request.QueryString["ActID"]);

                    if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["ID"]);

                        if (string.IsNullOrEmpty(Request.QueryString["ISfromActDoc"]))
                        {
                            BindTree(ActID);
                            BindActDocumentSecond(ActID, ID);
                        }
                        else
                        {
                            BindActDocumentSecond(ActID, ID);
                        }
                    }
                    else
                    {
                        BindActDocumentFirst(ActID);
                    }
                }
            }
        }
        
        public void BindActDocumentSecond(long ActID,long ID)
        {
            List<Sp_Act_Document_Result> CMPDocuments = ActManagement.getFileNameID(Convert.ToInt32(ActID),ID).ToList();
            if (CMPDocuments != null)
            {
                if (CMPDocuments.Count > 0)
                {
                    foreach (var file in CMPDocuments)
                    {
                        string filePath = Server.MapPath(file.FilePath);

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                lblMessage.Text = "";
                                lblMessage.Text = "Zip file can't view please download it";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                            }
                            else
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                bw.Write(DocumentManagement.ReadDocFiles(filePath));
                                bw.Close();

                                FileName = FileName.Substring(2, FileName.Length - 2);
                                 ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + FileName + "');", true);
                                lblMessage.Text = "";
                            }
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                        }
                        //upDocuemt.Update();
                    }
                }
            }
        }
        public void BindActDocumentFirst(long ActID)
        {

            var MasterQuery = ActManagement.getFileNamebyID(Convert.ToInt32(ActID));
            var DistinctTransQuery = MasterQuery;
            DistinctTransQuery = DistinctTransQuery.GroupBy(x => x.DocumentTypeID).Select(x => x.FirstOrDefault()).ToList();
            if (DistinctTransQuery != null)
            {
                if (DistinctTransQuery.Count > 0)
                {
                    System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
                    stringbuilder.Append(@"<table id='basic' width='100%'>");

                    foreach (var item in DistinctTransQuery)
                    {
                        stringbuilder.Append("<tr data-node-id=" + Convert.ToString(item.DocumentTypeID) + ">" +
                                    " <td class='locationheadbg'>" + Convert.ToString(item.DocumentType) + "</td>");
                        stringbuilder.Append("</tr>");

                        var actDocData1 = MasterQuery.Where(entry => entry.Act_ID == ActID && entry.DocumentTypeID == item.DocumentTypeID).ToList();
                        int i = 0;
                        foreach (var item1 in actDocData1)
                        {
                            i += 1;
                            string id = Convert.ToString(item.DocumentTypeID) + "." + i;
                            string FileName = "";
                            string[] filename = item1.FileName.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (filename[0].Length > 10)
                            {
                                FileName = filename[0].Substring(0,8) + "." + filename[1];
                            }
                            else
                            {
                                FileName = item1.FileName;
                            }

                            if (item.Act_TypeVersionDate != null)
                            {
                                FileName = FileName + " on " + Convert.ToDateTime(item1.Act_TypeVersionDate).ToString("MMM-yy");
                            }

                            stringbuilder.Append("<tr data-node-id=" + Convert.ToString(id) + " data-node-pid=" + Convert.ToString(item.DocumentTypeID) + " >" +
                            " <td class='locationheadLocationbg'   ><a href='../Common/ActOverview.aspx?ActID=" + ActID + "&ID=" + item1.ID + "&ISfromActDoc=" + 1 +"'>" + Convert.ToString(FileName) + "</a> </td>");
                            stringbuilder.Append("</tr>");
                        }
                    }
                    stringbuilder.Append("</table>");
                    ActDocString = stringbuilder.ToString().Trim();
                    foreach (var file in MasterQuery)
                    {
                        string filePath = Server.MapPath(file.FilePath);

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                lblMessage.Text = "";
                                lblMessage.Text = "Zip file can't view please download it";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                            }
                            else
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                bw.Write(DocumentManagement.ReadDocFiles(filePath));
                                bw.Close();

                                FileName = FileName.Substring(2, FileName.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + FileName + "');", true);
                                lblMessage.Text = "";
                            }
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                        }
                        break;
                    }
                }
            }
        }

        public void BindTree(long ActID)
        {

            var MasterQuery = ActManagement.getFileNamebyID(Convert.ToInt32(ActID));
            var DistinctTransQuery = MasterQuery;
            DistinctTransQuery = DistinctTransQuery.GroupBy(x => x.DocumentTypeID).Select(x => x.FirstOrDefault()).ToList();
            if (DistinctTransQuery != null)
            {
                if (DistinctTransQuery.Count > 0)
                {
                    System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
                    stringbuilder.Append(@"<table id='basic' width='100%'>");

                    foreach (var item in DistinctTransQuery)
                    {
                        stringbuilder.Append("<tr data-node-id=" + Convert.ToString(item.DocumentTypeID) + ">" +
                                    " <td class='locationheadbg'>" + Convert.ToString(item.DocumentType) + "</td>");
                        stringbuilder.Append("</tr>");

                        var actDocData1 = MasterQuery.Where(entry => entry.Act_ID == ActID && entry.DocumentTypeID == item.DocumentTypeID).ToList();
                        int i = 0;
                        foreach (var item1 in actDocData1)
                        {
                            i += 1;
                            string id = Convert.ToString(item.DocumentTypeID) + "." + i;
                            string FileName = "";
                            string OriginalFileName = "";
                            string[] filename = item1.FileName.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (filename[0].Length > 10)
                            {
                                FileName = filename[0].Substring(0, 8) + "." + filename[1];
                            }
                            else
                            {
                                FileName = item1.FileName;
                            }

                            if (item.Act_TypeVersionDate != null)
                            {
                                FileName = FileName + " on " + Convert.ToDateTime(item1.Act_TypeVersionDate).ToString("MMM-yy");
                            }

                            stringbuilder.Append("<tr data-node-id=" + Convert.ToString(id) + " data-node-pid=" + Convert.ToString(item.DocumentTypeID) + " >" +
                            //" <td class='locationheadLocationbg' onclick ='OpenDocumentOverviewpup(" + ActID + "," + item1.ID + ")' >" + Convert.ToString(FileName) + " </td>");
                            " <td class='locationheadLocationbg'><a href='../Common/ActOverview.aspx?ActID=" + ActID + "&ID=" + item1.ID + "&ISfromActDoc=" + 1 + "'>" + Convert.ToString(FileName) + "</a> </td>");
                            stringbuilder.Append("</tr>");
                        }
                    }
                    stringbuilder.Append("</table>");
                    ActDocString = stringbuilder.ToString().Trim();
                }
            }
        }
    }
}