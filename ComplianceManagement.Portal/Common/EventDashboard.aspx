﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="EventDashboard.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.EventDashboard" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/EventDashboards.ascx" TagName="EventDashboards" TagPrefix="vit" %>
<%@ Register Src="~/Controls/ActiveEventDashboards.ascx" TagName="ActiveEventDashboards" TagPrefix="vit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function initializeDatePicker11() { }
        function initializeDatePickerGrid() { }
        function initializeConfirmDatePickerActivation() { }
        
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <%-- <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                      <header class="panel-heading tab-bg-primary ">
                           
                                          <ul id="rblRole1" class="nav nav-tabs">
                                                   <li class="dummyval">                                                     
                                                        <asp:LinkButton ID="liAssignedEvents" OnClick="liAssignedEvents_Click"  runat="server">Assigned Events</asp:LinkButton>
                                                    </li>
                                                  <li class="dummyval">                                                      
                                                           <asp:LinkButton ID="liActiveEvents" OnClick="liActiveEvents_Click"  runat="server">Activated Events</asp:LinkButton>
                                                    </li>
                                          </ul>
  
                        </header>
                       <div class="panel-body">
                                    <div class="tab-content ">
                                       <div>
                                        <asp:UpdatePanel ID="upComplianceDashboard" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>                                                 
                                                <vit:EventDashboards ID="ucEventDashboards" runat="server" Visible="false"></vit:EventDashboards>
                                                <vit:ActiveEventDashboards ID="ucActiveEventDashboards" runat="server" Visible="false"></vit:ActiveEventDashboards>
                                            </ContentTemplate>                                           
                                        </asp:UpdatePanel>
                                    </div>
                                             </div>
                                     </div>
                              </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="liAssignedEvents"/>
            <asp:PostBackTrigger ControlID="liActiveEvents" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:HiddenField ID="HiddenFieldType" runat="server" />
    <script type="text/javascript">
        $(document).ready(function () {
            var hv = document.getElementById('<%= HiddenFieldType.ClientID%>').value;
            if (hv != "" && hv != undefined) {
                fhead(hv);
            }
            else {
                var filterbytype = ReadQuerySt('type');
                if (filterbytype == '') {
                    fhead('Events');
                } else {
                    $('#pagetype').css("font-size", "25px")
                    if (filterbytype == 'active') {
                        filterbytype = 'Activated Events';
                    } else if (filterbytype == 'assigned') {
                        filterbytype = 'Assigned Events';
                    }
                    fhead(filterbytype);
                }
            }
        });

    </script>
</asp:Content>


