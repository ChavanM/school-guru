﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ReviewerLockingDays.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ReviewerLockingDays" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Reports / Locking Details');
            setactivemenu('Myreport');
            fmaters1();
            $("#txtSearchComplianceID").on('input', function (e) {
                FilterGrid();
            });
            var startDate = new Date();
            $(function () {
                $('input[id*=txtUpdateDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    minDate: startDate,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        });
    </script>

    <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 0px 0px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        input[class=k-textbox], input[type=radio] {
            width: 99%;
            margin-left: 1px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            padding-right: 3px;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 180px !important;
        }

        #grid1 .k-grid-content {
            min-height: 380px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #EBEBEB;
            border-color: #a6a6ad;
            color: #535b6a;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#txtSearchComplianceID").on('input', function (e) {
                FilterGrid();
            });
            ComplianceType();
            BindGrid();
            BindAct();
            BindCompliance();
        });

        function FilterGrid() {
            var finalSelectedfilter = { logic: "and", filters: [] };
            if ($("#txtSearchComplianceID").val() != "" || $("#dropdownlistComplianceType").val() == "S" || $("#dropdownlistComplianceType").val() == "SC" || $("#dropdownlistComplianceType").val() == "EB" || $("#dropdownlistComplianceType").val() == "EBC") {
                if ($("#dropdownACT").val() != undefined && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "ActID", operator: "eq", value: $("#dropdownACT").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }
            }
            if ($("#dropdownCompliance").val() != undefined && $("#dropdownCompliance").val() != null && $("#dropdownCompliance").val() != "") {
                var ComplinceIDFilter = { logic: "or", filters: [] };
                ComplinceIDFilter.filters.push({
                    field: "ComplianceID", operator: "eq", value: $("#dropdownCompliance").val()
                });
                finalSelectedfilter.filters.push(ComplinceIDFilter);
            }
            if ($("#txtSearchComplianceID").val() != "") {
                var RiskFilter = { logic: "or", filters: [] };
                RiskFilter.filters.push({
                    field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
                });
                finalSelectedfilter.filters.push(RiskFilter);
            }
            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        function ClearAllFilterMain(e) {
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#txtSearchComplianceID").val('');
            $("#dropdownCompliance").data("kendoDropDownList").select(0);
            $("#dropdownlistComplianceType").data("kendoDropDownList").select(0);
            $("#dropdownACT").data("kendoDropDownList").enable(true);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fopenAlert() {
            alert('Record Save Successfully.');
            return false;
        }

        function CloseClearPopup() {
            window.location.reload();
            return false;
        }

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        function ComplianceType() {
            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "S" },
                    { text: "Internal", value: "I" },
                    { text: "Event Based", value: "EB" },
                ],
                index: 0,
                change: function (e) {
                    if ($("#dropdownlistComplianceType").val() == "I") {
                        $("#dropdownACT").data("kendoDropDownList").select(0);
                        $("#dropdownACT").data("kendoDropDownList").enable(false);
                    }
                    else {
                        $("#dropdownACT").data("kendoDropDownList").enable(true);
                    }
                    BindGrid();
                    BindCompliance();
                    BindAct();
                }
            });
        }

        function BindAct() {
            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActRevLockingComplianceList?UserID=<% =UId%>&CustID=<% =CustId%>&Type=' + $("#dropdownlistComplianceType").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("400");
                }
            });

        }

        function BindCompliance() {

            $("#dropdownCompliance").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Compliance",
                change: function (e) {
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindRevLockingComplianceList?UserID=<% =UId%>&CustID=<% =CustId%>&Type=' + $("#dropdownlistComplianceType").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("330");
                }
            });
        }

        function editAll() {
            var theGrid = $("#grid").data("kendoGrid");
            $("#grid tbody").find('tr').each(function () {
                var model = theGrid.dataItem(this);
                kendo.bind(this, model);
            });
            $("#grid").focus();
        }

        function calculateDates(first, second) {
            var diff = Math.round((second - first) / 1000 / 60 / 60 / 24); //Difference in days
            return diff;
        }

        function updatedMinDates() {
            var month = kendo.date.today().getMonth() + 1;
            var getmonth = month < 10 ? '0' + month : '' + month;

            var getday = kendo.date.today().getDate();
            var day = getday < 10 ? '0' + getday : '' + getday;
            return kendo.date.today().getFullYear() + "-" + getmonth + "-" + day;
        }

        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetLockingDaysDetail_Reviewer?CustID=<%=CustId%>&Type=' + $("#dropdownlistComplianceType").val() +'&RerID=<%=UId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduleOn: { type: "date" },
                                remark: { validation: { required: true, } },
                                ActivateDate: { type: "date", validation: { required: true } },
                                UserID: { type: "text" }
                            }
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                dataBound: function (e) {
                    editAll();
                },
                sortable: true,
                filterable: true,
                groupable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                persistSelection: true,
                multi: true,
                editable: false,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                columns: [
                    {
                        selectable: true,
                        width: "30px"
                    },
                    //{
                    //    hidden: true, field: "ID", title: "ID", width: "8%",
                    //    attributes: {
                    //        style: 'white-space: nowrap;'
                    //    }, filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        width: "12%;",
                        field: "BranchName", title: 'Location',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "10%" },
                    {
                        width: "15%;",
                        field: "ShortDescription", title: 'Compliance',

                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "12%;",
                        field: "ActName", title: 'Act Name',
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "15%;",
                        field: "Remarks", title: 'Performer Remarks',
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "13%;",
                        field: "PerformerName", title: 'Performer',
                        //hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduleOn", title: "Due Date",
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        width: "10%;",
                        field: "ForMonth", title: 'Period',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "10%;", field: "DueDay",
                        template: "#= calculateDates(kendo.parseDate(ScheduleOn, 'yyyy-MM-dd'), kendo.parseDate(new Date(), 'yyyy-MM-dd')) #",
                        title: "Due Day",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: false,
                    },

                    {
                        template: "<input data-bind='value:remark'  class='k-textbox' />",
                        title: "Remark",
                        width: "12%",
                    },
                    {
                        template: "<input id='ActivateDate' data-bind='value:ActivateDate' min='#= updatedMinDates() #' name='ActivateDate' type='date' class='k-textbox'/>",
                        format: "{0:yyyy-MM-dd}",
                        title: "Unlock Date",
                        width: "200px"
                    },

                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-edit", className: "ob-overviewMain" }
                        ], title: "Action", lock: true, width: "6%", headerAttributes: {
                            style: "text-align: center;"
                        }

                    }
                ]
            });

            function dateTimeEditor1(container, options) {
                $('<input type="text" id="ActivateDate"  name="ActivateDate" required/>')
                    .appendTo(container)
                    .kendoDatePicker({
                        format: "dd-MM-yyyy",
                        value: kendo.toString(new Date(options.model.ActivateDate), 'dd-MM-yyyy'),
                        min: kendo.date.today(),
                    });
            }
            $(document).on("click", "#grid tbody tr .ob-overviewMain", function (e) {
                
                document.getElementById("<%=SelectedID.ClientID%>").value = "";
                document.getElementById("<%=txtDueDate.ClientID%>").value = "";
                document.getElementById("<%=txtPerformerName.ClientID%>").value = "";
                document.getElementById("<%=txtRemark.ClientID%>").value = "";
                document.getElementById("<%=txtUpdateDate.ClientID%>").value = "";

                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#divOverView1').modal('show');
                $('.modal-dialog').css('width', '947px');
                
                document.getElementById("<%=SelectedID.ClientID%>").value = item.ID;
                document.getElementById("<%=txtDueDate.ClientID%>").value = kendo.toString(kendo.parseDate(item.ScheduleOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy');
                document.getElementById("<%=txtPerformerName.ClientID%>").value = item.PerformerName;
                BindAuditlogGrid(item.ScheduledOn);
                return true;
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Unlock Compliance";
                }
            });

      
            $("#grid").kendoTooltip({
                filter: ("th:nth-child(n+2)"),
                //filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid").kendoTooltip({
                filter: ("td:nth-child(n+2)"),
                //filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindAuditlogGrid(DetailID) {
            var gridexist = $('#gridauditlog').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#gridauditlog').empty();

            var grid = $("#gridauditlog").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetSubLocakingDaysDetailRev?UserID=<% =UId%>&CustID=<%=CustId%>&ScheduleOnID=' + DetailID,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                remark: { validation: { required: true, } },
                                ActivateDate: { type: "date", validation: { required: true } },
                                UserID: { type: "text" }
                            }
                        },
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 5
                },
                excel: {
                    allPages: true,
                },
                sortable: false,
                groupable: false,
                filterable: false,
                columnMenu: false,
                pageable: true,
                reorderable: false,
                resizable: false,
                multi: true,
                selectable: false,
                columns: [
                    {
                        width: "5%;", field: "DueDate", title: "Log Date",
                        type: "date",
                        template: "#= kendo.toString(kendo.parseDate(CreatedOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    },
                    { width: "5%;", field: "DueDay", title: "Due Day" },
                    {
                        width: "15%;",
                        field: "Remark", title: 'Remark',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                ]
            });
        }

        function BindGridApply(e) {
            ComplianceType();
            BindGrid();
            FilterGrid();
            e.preventDefault();
        }

        function exportReportGenerate(e) {
            
            var ReportName = "Locking Details";
            var todayDate = moment().format('DD-MMM-YYYY');
            var grid = $("#grid").getKendoGrid();

            var rows = [
                {
                    cells: [
                        { value: "Report Name:", bold: true },
                        { value: ReportName }
                    ]
                },
                {
                    cells: [
                        { value: "Report Generated On:", bold: true },
                        { value: todayDate }
                    ]
                },
                {
                    cells: [
                        { value: "" }
                    ]
                },
                {
                    cells: [
                        { value: "Location", bold: true, textAlign: "left" },
                        { value: "Compliance", bold: true, textAlign: "left" },
                        { value: "Performer Remarks", bold: true, textAlign: "left" },
                        { value: "Performer", bold: true, textAlign: "left" },
                        { value: "Due Date", bold: true, textAlign: "left" },
                        { value: "Period", bold: true, textAlign: "left" },
                        { value: "Due Days", bold: true, textAlign: "left" },
                    ]
                }
            ];

            var trs = grid.dataSource;
            var filteredDataSource = new kendo.data.DataSource({
                data: trs.data(),
                filter: trs.filter()
            });

            filteredDataSource.read();
            var data = filteredDataSource.view();
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                var rowHeight = 0;
                if (dataItem.ShortDescription != null) {
                    rowHeight = dataItem.ShortDescription.length > 27 ? Math.ceil(dataItem.ShortDescription.length / 27) * 20 : 20;
                }
                rows.push({
                    cells: [
                        { value: dataItem.BranchName },
                        { value: dataItem.ShortDescription },
                        { value: dataItem.Remarks },
                        { value: dataItem.PerformerName },
                        { value: dataItem.ScheduleOn, format: "DD-MMM-YYYY" },
                        { value: dataItem.ForMonth},
                        { value: dataItem.DueDay }
                    ],
                    height: rowHeight
                });
            }
            for (var i = 3; i < rows.length; i++) {
                for (var j = 0; j < 7; j++) {
                    rows[i].cells[j].borderBottom = "#000000";
                    rows[i].cells[j].borderLeft = "#000000";
                    rows[i].cells[j].borderRight = "#000000";
                    rows[i].cells[j].borderTop = "#000000";
                    rows[i].cells[j].hAlign = "left";
                    rows[i].cells[j].vAlign = "top";
                    rows[i].cells[j].wrap = true;

                    if ((j == 4) && i > 3) {
                        var value = rows[i].cells[4].value;
                        if (value != null) {
                            
                            var valueNew = new Date(value);
                            valueNew.format = "dd-MM-yyyy";
                            var valueNew1 = new Date();
                            valueNew1.format = "dd-MM-yyyy";
                            var diff = Math.round((valueNew1 - valueNew) / (1000 * 60 * 60 * 24)); //Difference in days
                            rows[i].cells[6].value = diff;
                        }
                    }
                }
            }
            excelExport(rows, ReportName);
            e.preventDefault();
        }

        function excelExport(rows, ReportName) {

            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        columns: [
                            { width: 150 },
                            { width: 250 },
                            { width: 180 },
                            { width: 120 },
                            { width: 100 },
                            { width: 100 },
                            { width: 100 },
                        ],
                        title: "Report",
                        rows: rows
                    },
                ]
            });

            var nameOfPage = $("#dropdownlistComplianceType").data("kendoDropDownList").text() + "_Locking Details"
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
        }

        function btnUnlockCompliance_click(e) {
            e.preventDefault();
            var rows = $('#grid :checkbox:checked');
            var grid = $('#grid').data("kendoGrid");

            if (rows.length == 0) {
                alert('Please select at least one row with data');
                return;
            }
            if (rows.length > 0) {
                var validatopnFlag = 0;
                var things = [];
                $.each(rows, function () {
                    var item = grid.dataItem($(this).closest("tr"));
                    if (item.ActivateDate == undefined || item.ActivateDate == null
                        || item.ActivateDate == "") {
                        validatopnFlag = 1;
                    }
                    item.UserID =<% =UId%>
                        things.push(item);
                });

                if (validatopnFlag == 0) {
                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '<% =Path%>/Data/MultipleUpdateRevLocking',
                        data: JSON.stringify(things),
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                        success: function (data) {
                            if (data[0].Message == "Save") {
                                alert('Record Save Successfully.');
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                $('#grid').data('kendoGrid')._selectedIds = {};
                                $('#grid').data('kendoGrid').clearSelection();
                            }
                        },
                        failure: function (response) {
                            $('#result').html(response);
                        }
                    });
                }
                else {
                    alert('Please select unlock date on selected row');
                    return;
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div style="margin: 0.5% 0 0.5%;">
            <input id="dropdownlistComplianceType" style="width: 12%; margin-right: 0.8%;" />
            <input id="dropdownCompliance" style="width: 20%; margin-right: 0.8%;" />
            <input id="txtSearchComplianceID" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 15%; margin-right: 0.8%;" />
            <input id="dropdownACT" style="width: 23%; margin-right: 0.8%;" />
            <button id="ClearfilterMain" style="float: right; height: 30px; margin-right: 0.8%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            <button id="exportReport" style="height: 28px;float: right; margin-right: 0.8%;" onclick="exportReportGenerate(event)"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
            <button id="btnUnlockCompliance" style="float: right; height: 28px; margin-right: 0.8%;" onclick="btnUnlockCompliance_click(event)">Submit</button>
        </div>

        <div id="grid" style="width:99%;"></div>
    </div>
    <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 1150px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="height: 650px;">

                    <div class="col-md-12 colpadding0" style="min-height: 0px; max-height: 240px; overflow-y: auto;">
                        <asp:Panel ID="vdpanel1" runat="server" ScrollBars="Auto">
                            <asp:ValidationSummary ID="VSCasePopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceValidationGroup1" />
                            <asp:CustomValidator ID="cvCasePopUp" runat="server" EnableClientScript="true"
                                ValidationGroup="ComplianceValidationGroup1" Display="none" class="alert alert-block alert-danger fade in" />
                        </asp:Panel>
                    </div>
                    <div style="margin-bottom: 7px">
                        <h2>Unlock Compliance</h2>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                        <label style="font-weight: bold; vertical-align: text-top;">Performer Name</label>
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                    <td style="width: 73%;">
                                        <asp:TextBox runat="server" ID="txtPerformerName" class="form-control" Style="width: 167px;" ReadOnly="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                        <label style="font-weight: bold; vertical-align: text-top;">Due Date</label>
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                    <td style="width: 73%;">
                                        <asp:TextBox runat="server" ID="txtDueDate" placeholder="DD-MM-YYYY"
                                            class="form-control" Style="width: 167px;" ReadOnly="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="font-weight: bold; vertical-align: text-top;">Update Date for Unlock</label>
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                    <td style="width: 73%;">
                                        <asp:TextBox runat="server" ID="txtUpdateDate" placeholder="DD-MM-YYYY"
                                            class="form-control" Style="width: 167px;" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Enter Date." ControlToValidate="txtUpdateDate"
                                            runat="server" ID="RequiredFieldValidator4" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                        <label style="font-weight: bold; vertical-align: text-top;">Remark</label>
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                    <td style="width: 73%;">
                                        <asp:TextBox runat="server" TextMode="MultiLine" ID="txtRemark" CssClass="form-control" autocomplete="off" Width="280px" />
                                        <%--  <asp:RequiredFieldValidator ErrorMessage="Please Enter Remark." ControlToValidate="txtRemark"
                                                    runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup1" Display="None" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="SelectedID" Style="display: none;" />
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;"></td>
                                    <td>
                                        <asp:Button Text="Submit" runat="server" ID="btnSave"
                                            OnClick="btnSave_Click" ValidationGroup="ComplianceValidationGroup1" CssClass="btn btn-search" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div>
                        <h2>Audit Log</h2>
                    </div>
                    <div id="gridauditlog"></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
