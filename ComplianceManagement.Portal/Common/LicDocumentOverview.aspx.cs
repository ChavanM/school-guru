﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class LicDocumentOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["IsFlag"]))
            {
                int ScheduledOnID = Convert.ToInt32(Request.QueryString["ScheduleID"]);
                string IsStatutory = Convert.ToString(Request.QueryString["IsFlag"]);
                BindFiles(ScheduledOnID);
            }
        }
        public static List<GetLicenseInternalComplianceDocumentsView> GetDocumnetsInternal(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DocumentList = (from row in entities.GetLicenseInternalComplianceDocumentsViews
                                    where row.ScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return DocumentList;
            }
        }
        public class GetTaskDocumentData
        {
            public Nullable<int> ID { get; set; }
            public int FileID { get; set; }
            public Nullable<long> TaskScheduleOnID { get; set; }
            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string Version { get; set; }
            public Nullable<System.DateTime> VersionDate { get; set; }
            public Nullable<bool> ISLink { get; set; }
        }
        private void BindFiles(int scheduledOnID)
        {
            if (true)//Statutory or internal
            {
                List<GetComplianceDocumentsView> taskDocumentsoutput = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(scheduledOnID), 0);
                if (CMPDocuments != null)
                {
                    if (CMPDocuments.Count > 0)
                    {
                        taskDocumentsoutput.Clear();
                        var entitiesData1 = CMPDocuments.Where(x => x.ISLink == false).ToList();
                        var documentVersionData = entitiesData1.GroupBy(entry => new { entry.Version }).Select(entry => entry.FirstOrDefault()).ToList();
                        var documentVersionData1 = CMPDocuments.Where(x => x.ISLink == true).ToList();
                        foreach (var item in documentVersionData)
                        {
                            taskDocumentsoutput.Add(new GetComplianceDocumentsView
                            {
                                ID = item.ID,
                                ScheduledOnID = item.ScheduledOnID,
                                Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                VersionDate = item.VersionDate,
                                FileID = item.FileID,
                                ISLink = item.ISLink,
                                FilePath = item.FilePath,
                                FileName = item.FileName
                            });
                        }
                        foreach (var item in documentVersionData1)
                        {
                            taskDocumentsoutput.Add(new GetComplianceDocumentsView
                            {
                                ID = item.ID,
                                ScheduledOnID = item.ScheduledOnID,
                                Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                VersionDate = item.VersionDate,
                                FileID = item.FileID,
                                ISLink = item.ISLink,
                                FilePath = item.FilePath,
                                FileName = item.FileName
                            });
                        }
                        //rptDownloadTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                        rptDownloadTaskVersion.DataSource = taskDocumentsoutput;
                        rptDownloadTaskVersion.DataBind();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
                    }
                }
            }
            else
            {


            }
        }

        protected void rptDownloadTaskVersion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {                
                Label lblpathIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectLink = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnDownloadTaskVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDownloadTaskDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

                if (lblpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectLink.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectLink.Visible = false;
                }
            }
        }
        protected void rptDownloadTaskVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetComplianceDocumentsView> taskDocument = new List<GetComplianceDocumentsView>();

                    taskDocument = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(taskScheduleOnID), 0);

                    if (commandArgs[1].Equals("1.0"))
                    {
                        taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();

                        if (taskDocument.Count <= 0)
                        {
                            taskDocument = taskDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {

                            if (taskDocument.Count > 0)
                            {
                                List<GetComplianceDocumentsView> taskDocumentsoutput = new List<GetComplianceDocumentsView>();
                                taskDocumentsoutput.Clear();
                                var entitiesData1 = taskDocument.Where(x => x.ISLink == false).ToList();
                                var documentVersionData = entitiesData1.GroupBy(entry => new { entry.Version }).Select(entry => entry.FirstOrDefault()).ToList();
                                var documentVersionData1 = taskDocument.Where(x => x.ISLink == true).ToList();
                                foreach (var item in documentVersionData)
                                {
                                    taskDocumentsoutput.Add(new GetComplianceDocumentsView
                                    {
                                        ID = item.ID,
                                        ScheduledOnID = item.ScheduledOnID,
                                        Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                        VersionDate = item.VersionDate,
                                        FileID = item.FileID,
                                        ISLink = item.ISLink,
                                        FilePath = item.FilePath,
                                        FileName = item.FileName
                                    });
                                }
                                foreach (var item in documentVersionData1)
                                {
                                    taskDocumentsoutput.Add(new GetComplianceDocumentsView
                                    {
                                        ID = item.ID,
                                        ScheduledOnID = item.ScheduledOnID,
                                        Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                        VersionDate = item.VersionDate,
                                        FileID = item.FileID,
                                        ISLink = item.ISLink,
                                        FilePath = item.FilePath,
                                        FileName = item.FileName
                                    });
                                }
                                rptDownloadTaskVersion.DataSource = taskDocumentsoutput;
                                rptDownloadTaskVersion.DataBind();
                            }
                          
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();

                            if (taskDocument.Count > 0)
                            {
                                taskDocument = taskDocument.Where(x => x.ISLink == false).ToList();
                                ComplianceZip.AddDirectoryByName(commandArgs[1]);

                                int i = 0;
                                foreach (var eachFile in taskDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                    if (eachFile.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(eachFile.FileName);
                                        string[] filename = eachFile.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (eachFile.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }                  
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}