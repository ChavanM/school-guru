﻿<%@ Page Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceDashboardDisplay.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ComplianceDashboardDisplay" EnableEventValidation="false" %>


<%@ Register Src="~/Controls/PerformanceSummaryforReviewer.ascx" TagName="PerformanceSummaryforReviewer"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/PerformanceSummaryforPerformer.ascx" TagName="PerformanceSummaryforPerformer"
    TagPrefix="vit" %>

<%@ Register Src="~/Controls/InternalPerformance_SummaryforReviewer.ascx" TagName="InternalPerformanceSummaryforReviewer"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalPerformance_SummaryforPerformer.ascx" TagName="InternalPerformanceSummaryforPerformer"
    TagPrefix="vit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('title').text(document.getElementById('<%= hdnTitle.ClientID %>').value);
         });

         function initializeComboboxUpcoming() {
         }

         function initializeDatePickerforPerformer(date) {
         }

         function initializeDatePicker(date) {
         }
         function initializeJQueryUI(date) {
         }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div>
        <table width="100%" style="height: 35px">
            <tr>
                <td>
                    <div style="margin-bottom: 4px">
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:PlaceHolder runat="server" ID="phSummaryControl" />
        <vit:PerformanceSummaryforPerformer ID="ucPerformanceSummaryforPerformer" runat="server" Visible="false"></vit:PerformanceSummaryforPerformer>
        <vit:PerformanceSummaryforReviewer ID="ucPerformanceSummaryforReviewer" runat="server" Visible="false"></vit:PerformanceSummaryforReviewer>        
        <vit:InternalPerformanceSummaryforPerformer ID="UcInternalPerformanceSummaryforPerformer" runat="server" Visible="false"></vit:InternalPerformanceSummaryforPerformer>
        <vit:InternalPerformanceSummaryforReviewer ID="UcInternalPerformanceSummaryforReviewer" runat="server" Visible="false"></vit:InternalPerformanceSummaryforReviewer>
    </div>
    <asp:HiddenField ID="hdnTitle" runat="server" />
</asp:Content>
