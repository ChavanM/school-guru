﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateOverview.aspx.cs" EnableEventValidation="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.CertificateOverview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="../tree/jquerysctipttop.css" rel="stylesheet" />
    <script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
    <link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />

    <style type="text/css">
        table#basic > tr, td {
            border-radius: 5px;
        }

        table#basic {
            border-collapse: unset;
            border-spacing: 3px;
        }

        .locationheadbg {
            background-color: #fff;
        }

        .locationheadLocationbg {
            background-color: #fff;
            color: blue;
            cursor: pointer;
        }

        /*.locationheadbg {
            background-color: #fff;
        }*/

        td.locationheadLocationbg > span.tree-icon {
            background-color: #1976d2 !important;
            padding-right: 12px;
            color: white;
        }

        .GradingRating1 {
            background-color: #8fc156;
        }

        .GradingRating2 {
            background-color: #ffc107;
        }

        .GradingRating3 {
            background-color: #ef9a9a;
        }

        .Viewcss {
            width: 50px;
            color: blue;
            text-align: center;
            cursor: pointer;
        }

        .downloadcss {
            width: 100px;
            color: blue;
            text-align: center;
            cursor: pointer;
        }
    </style>

    <script type="text/javascript">
        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };

        function fopendocfileReview(file) {
            debugger;
            $('#divViewDocument').modal('show');
            $('#docViewerStatutory').removeAttr('src');
            $('#docViewerStatutory').attr('src', "../docviewer.aspx?docurl=" + file + "&m=" + Math.random());
            
        }

        function OpenDocumentOverviewpup(ActID, ID) {
            $('#OverViews').removeAttr('src');
            $('#OverViews').attr('src', "../Common/ActOverview.aspx?ActID=" + ActID + "&ID=" + ID + "&ISfromActDoc=" + 1 + "&m=" + Math.random());
            
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#basic').simpleTreeTable({
                collapsed: false
            });
        });

    </script>


</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div id="divViewDocument" class="modal fade" style="width: 100%; background: #fff;">


                <div style="float: left; width: 20%">
                    <asp:Panel runat="server" Height="530px" ID="pnl" ScrollBars="Auto">
                        <div style="width: 90%; height: 300px; margin-top: 15px;">
                            <%=ActDocString%>
                        </div>
                    </asp:Panel>

                </div>
                <div style="float: left; width: 80%">
          
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 0px; width: 100%;">
                            </fieldset>
                          
                 
                      <iframe src="about:blank" id="docViewerStatutory" runat="server" width="100%" height="510px"></iframe>
                </div>
            </div>
        </div>

        <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
