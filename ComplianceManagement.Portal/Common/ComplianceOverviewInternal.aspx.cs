﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ComplianceOverviewInternal : System.Web.UI.Page
    {
        static string sampleFormPath1 = "";
        public static string CompDocReviewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["ComplainceInstatnceID"]))
            {
                int ScheduledOnID =Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                int complianceInstanceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);

                ComplianceDBEntities entities = new ComplianceDBEntities();

                var ParentEvent = entities.SP_GetAuditLogDateInternal(ScheduledOnID).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();

                licomplianceoverview1.Attributes.Add("class", "active");
                lidocuments1.Attributes.Add("class", "");
                liAudit1.Attributes.Add("class", "");

                complianceoverview.Attributes.Remove("class");
                complianceoverview.Attributes.Add("class", "tab-pane active");
                documents.Attributes.Remove("class");
                documents.Attributes.Add("class", "tab-pane");
                audits.Attributes.Remove("class");
                audits.Attributes.Add("class", "tab-pane");

                divMgmtRemrks.Visible = false;
                if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.ComplianceProductType == 3)
                {
                    divMgmtRemrks.Visible = true;
                }
            }
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                lblRisk.Text = string.Empty;
                var complianceType = Business.InternalComplianceManagement.GetInternalComplianceType(complianceInstanceID).IComplianceType;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var masterquery = (from row in entities.InternalComplianceAssignments
                                         join row1 in entities.Users
                                         on row.UserID equals row1.ID
                                         where row.InternalComplianceInstanceID == complianceInstanceID                                         
                                         select new {
                                            name= row1.FirstName + " " + row1.LastName ,
                                            roleid = row.RoleID
                                         }).ToList();

                    var PerformerName = masterquery.Where(en => en.roleid == 3).Select(ent=>ent.name).FirstOrDefault();
                    var ReviewerName = masterquery.Where(en => en.roleid == 4).Select(ent => ent.name).FirstOrDefault();
                    lblPerformer.Text = PerformerName;
                    lblreviewer.Text = ReviewerName;
                }
                  
                if (complianceType == 1)  //CheckList
                    {
                        int customerbranchID = -1;
                        var AllComData = Business.ComplianceManagement.GetPeriodLocationPerformerInternalCheckList(ScheduledOnID, complianceInstanceID);
                        if (AllComData != null)
                        {
                            customerbranchID = AllComData.CustomerBranchID;
                            if (AllComData.Status.ToLower() == "in progress")
                            {
                                lblStatus.Text = AllComData.Status;
                                divRiskType.Attributes["style"] = "background-color:red;";
                            }
                            else if (Convert.ToDateTime(AllComData.InternalScheduledOn).Date < DateTime.Today.Date)
                            {
                                if (AllComData.Status.ToLower() == "open" || AllComData.Status.ToLower() == "not  complied" || AllComData.Status.ToLower() == "complied but pending review" || AllComData.Status.ToLower() == "complied delayed but pending review" || AllComData.Status.ToLower() == "rejected" || AllComData.Status.ToLower() == "not applicable" || AllComData.Status.ToLower() == "complied  but document pending")
                                {
                                    lblStatus.Text = "Overdue";
                                    divRiskType.Attributes["style"] = "background-color:red;";

                                }
                                if (AllComData.Status.ToLower() == "not complied")
                                {
                                    lblStatus.Text = "Not Complied";
                                    divRiskType.Attributes["style"] = "background-color:#2195f2;";

                                }
                                else if (AllComData.Status.ToLower() == "closed-timely" || AllComData.Status.ToLower() == "closed-delayed" || AllComData.Status.ToLower() == "approved" || AllComData.Status.ToLower() == "revise compliance" || AllComData.Status.ToLower() == "not applicable" || AllComData.Status.ToLower() == "complied but document pending")
                                {
                                    lblStatus.Text = "Completed";
                                    divRiskType.Attributes["style"] = "background-color:green;";
                                }
                            }

                            else if (Convert.ToDateTime(AllComData.InternalScheduledOn).Date >= DateTime.Today.Date)
                            {
                                if (AllComData.Status.ToLower() == "open" || AllComData.Status.ToLower() == "complied but pending review" || AllComData.Status.ToLower() == "complied delayed but pending review" || AllComData.Status.ToLower() == "rejected")
                                {
                                    lblStatus.Text = "Upcoming";
                                    divRiskType.Attributes["style"] = "background-color:#ffbf00;";
                                }
                                if (AllComData.Status.ToLower() == "not complied")
                                {
                                    lblStatus.Text = "Not Complied";
                                    divRiskType.Attributes["style"] = "background-color:#2195f2;";

                                }
                                else if (AllComData.Status.ToLower() == "closed-timely" || AllComData.Status.ToLower() == "closed-delayed" || AllComData.Status.ToLower() == "revise compliance" || AllComData.Status.ToLower() == "not applicable" || AllComData.Status.ToLower() == "complied but document pending")
                                {
                                    lblStatus.Text = "Completed";
                                    divRiskType.Attributes["style"] = "background-color:green;";
                                }
                            }
                            lblLocation.Text = AllComData.Branch;
                            lblDueDate.Text = Convert.ToDateTime(AllComData.InternalScheduledOn).ToString("dd-MMM-yyyy");
                            lblPeriod.Text = AllComData.ForMonth;

                        }
                    }
                    else
                    {
                        int customerbranchID = -1;
                        var AllComData = Business.ComplianceManagement.GetPeriodLocationPerformerInternal(ScheduledOnID, complianceInstanceID);
                        if (AllComData != null)
                        {
                            customerbranchID = AllComData.CustomerBranchID;
                            if (AllComData.Status.ToLower() == "in progress")
                            {
                                lblStatus.Text = AllComData.Status.ToUpper();
                                divRiskType.Attributes["style"] = "background-color:red;";
                            }
                            else if (Convert.ToDateTime(AllComData.InternalScheduledOn).Date < DateTime.Today.Date)
                            {
                                if (AllComData.Status.ToLower() == "open" || AllComData.Status.ToLower() == "not  complied" || AllComData.Status.ToLower() == "complied but pending review" || AllComData.Status.ToLower() == "complied delayed but pending review" || AllComData.Status.ToLower() == "rejected" || AllComData.Status.ToLower() == "not  applicable" || AllComData.Status.ToLower() == "complied  but document pending")
                                {
                                    lblStatus.Text = "Overdue";
                                    divRiskType.Attributes["style"] = "background-color:red;";

                                }
                                if (AllComData.Status.ToLower() == "not complied")
                                {
                                    lblStatus.Text = "Not Complied";
                                    divRiskType.Attributes["style"] = "background-color:#2195f2;";

                                }
                                else if (AllComData.Status.ToLower() == "closed-timely" || AllComData.Status.ToLower() == "closed-delayed" || AllComData.Status.ToLower() == "approved" || AllComData.Status.ToLower() == "revise compliance" || AllComData.Status.ToLower() == "not applicable" || AllComData.Status.ToLower() == "complied but document pending")
                                {
                                    lblStatus.Text = "Completed";
                                    divRiskType.Attributes["style"] = "background-color:green;";
                                }
                            }

                            else if (Convert.ToDateTime(AllComData.InternalScheduledOn).Date >= DateTime.Today.Date)
                            {
                                if (AllComData.Status.ToLower() == "open" || AllComData.Status.ToLower() == "not  complied" || AllComData.Status.ToLower() == "complied but pending review" || AllComData.Status.ToLower() == "complied delayed but pending review" || AllComData.Status.ToLower() == "rejected")
                                {
                                    lblStatus.Text = "Upcoming";
                                    divRiskType.Attributes["style"] = "background-color:#ffbf00;";
                                }
                                if (AllComData.Status.ToLower() == "not complied")
                                {
                                    lblStatus.Text = "Not Complied";
                                    divRiskType.Attributes["style"] = "background-color:#2195f2;";

                                }
                                else if (AllComData.Status.ToLower() == "closed-timely" || AllComData.Status.ToLower() == "closed-delayed" || AllComData.Status.ToLower() == "revise compliance" || AllComData.Status.ToLower() == "not applicable" || AllComData.Status.ToLower() == "complied but document pending")
                                {
                                    lblStatus.Text = "Completed";
                                    divRiskType.Attributes["style"] = "background-color:green;";
                                }
                            }
                            lblLocation.Text = AllComData.Branch;
                            lblDueDate.Text = Convert.ToDateTime(AllComData.InternalScheduledOn).ToString("dd-MMM-yyyy");
                            lblPeriod.Text = AllComData.ForMonth;  
                        }
                    }
                var RecentComplianceTransaction = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID(ScheduledOnID);                              
                var complianceInfo = Business.InternalComplianceManagement.GetInternalComplianceByInstanceID(ScheduledOnID);               
                if (complianceInfo != null)
                {
                    lblComplianceDiscription.Text = complianceInfo.IShortDescription.Replace("\n", "<br>");
                    lblShortForm.Text = complianceInfo.IShortForm;
                    lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency != null ? (int)complianceInfo.IFrequency : -1));
                    string risk = Business.InternalComplianceManagement.GetRiskType(complianceInfo);
                    lblRisk.Text = risk;
                    hdnComplianceId.Value = Convert.ToString(complianceInfo.ID);
                    lblcomplianceID.Text = Convert.ToString(complianceInfo.ID);

                    if (risk == "HIGH")
                    {
                        lblRisk.Attributes["style"] = "color:red;font-weight: bold;";
                    }
                    else if (risk == "MEDIUM")
                    {
                        lblRisk.Attributes["style"] = "color:#ffbf00;font-weight: bold;";
                    }
                    else if (risk == "LOW")
                    {
                        lblRisk.Attributes["style"] = "color:green;font-weight: bold;";
                    }
                    else if (risk == "CRITICAL")
                    {
                        lblRisk.Attributes["style"] = "color:green;font-weight: bold;";
                    }
                }

                var documentVersionData = DocumentManagement.GetFileDataInternal(Convert.ToInt32(RecentComplianceTransaction.InternalComplianceScheduledOnID)).Select(x => new
                {
                    ID = x.ID,
                    InternalComplianceScheduledOnID = x.InternalComplianceScheduledOnID,
                    FileID = x.FileID,
                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                    VersionDate = x.VersionDate,
                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                    ISLink = x.ISLink,
                    FilePath = x.FilePath,
                    FileName = x.FileName
                }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                rptComplianceVersion.DataSource = documentVersionData;
                rptComplianceVersion.DataBind();

                rptComplianceDocumnets.DataSource = null;
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = null;
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                    List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                    ComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(commandArgs[0])).ToList();

                    if (commandArgs[1].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                       
                            var ComplianceDocumentFileData = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            var documentComplianceFileData = ComplianceDocumentFileData.Select(x => new
                            {
                                ID = x.ID,
                                InternalComplianceScheduledOnID = x.InternalComplianceScheduledOnID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).ToList();
                            rptComplianceDocumnets.DataSource = documentComplianceFileData;
                            //rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptComplianceDocumnets.DataBind();


                            var WorkingDocumentFileData = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            var documentWorkingFileData = WorkingDocumentFileData.Select(x => new
                            {
                                ID = x.ID,
                                InternalComplianceScheduledOnID = x.InternalComplianceScheduledOnID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).ToList();

                            rptWorkingFiles.DataSource = documentWorkingFileData;
                            //rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptWorkingFiles.DataBind();

                        rptComplianceVersion.DataSource = null;
                        rptComplianceVersion.DataBind();
                        var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                InternalComplianceScheduledOnID = x.InternalComplianceScheduledOnID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptComplianceVersion.DataSource = documentVersionData;
                            rptComplianceVersion.DataBind();
                            UpDetailView.Update();
                        
                        //if (e.CommandName.Equals("version"))
                        //{
                        //    rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        //    rptComplianceDocumnets.DataBind();

                        //    rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                        //    rptWorkingFiles.DataBind();

                        //    var documentVersionData = ComplianceDocument.Select(x => new
                        //    {
                        //        ID = x.ID,
                        //        InternalComplianceScheduledOnID = x.InternalComplianceScheduledOnID,
                        //        Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                        //        VersionDate = x.VersionDate,
                        //        VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                        //    }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                        //    rptComplianceVersion.DataSource = documentVersionData;
                        //    rptComplianceVersion.DataBind();
                        //    UpDetailView.Update();
                        //}
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var compliancetype = DocumentManagement.GetInternalComplianceTypeID(Convert.ToInt32(commandArgs[0]));
                                if (compliancetype == 1)
                                {
                                    var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(commandArgs[0]));
                                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
                                    if (ComplianceFileData.Count > 0)
                                    {
                                        ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                        int i = 0;
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;

                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        foreach (var item in ComplianceFileData)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                                request.Key = item.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                            }
                                        }
                                        foreach (var file in ComplianceFileData)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                string str = filename[0] + i + "." + ext;
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                              
                                                i++;
                                            }
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(commandArgs[0]));
                                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
                                    if (ComplianceFileData.Count > 0)
                                    {
                                        ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                        int i = 0;
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;

                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        foreach (var item in ComplianceFileData)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                                request.Key = item.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                            }
                                        }

                                        foreach (var file in ComplianceFileData)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;
                                                
                                                 ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                                
                                                i++;
                                            }
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var compliancetype = DocumentManagement.GetInternalComplianceTypeID(Convert.ToInt32(commandArgs[0]));
                                if (compliancetype == 1)
                                {
                                    var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(commandArgs[0]));
                                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
                                    if (ComplianceFileData.Count > 0)
                                    {
                                        ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                        int i = 0;
                                        foreach (var file in ComplianceFileData)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;
                                                if (file.EnType == "M")
                                                {
                                                    ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(commandArgs[0]));
                                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
                                    if (ComplianceFileData.Count > 0)
                                    {
                                        ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                        int i = 0;
                                        foreach (var file in ComplianceFileData)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;
                                                if (file.EnType == "M")
                                                {
                                                    ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            if (ComplianceFileData.Count > 0)
                            {
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                int i = 0;

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }


                                foreach (var file in ComplianceFileData)
                                {
                                    rptComplianceVersionView.DataSource = ComplianceFileData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionView.DataBind();

                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            i++;
                                            lblMessage.Text = "";
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            if (ComplianceFileData.Count > 0)
                            {
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    rptComplianceVersionView.DataSource = ComplianceFileData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionView.DataBind();

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                            string FileName = DateFolder + "/" + User + "" + extension;
                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            i++;
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            lblMessage.Text = "";
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReview");
                Label lblpathIlink = (Label)e.Item.FindControl("lblpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblpathDownload");

                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
            //   scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            //}
        }
        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                }
                else
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                }
                else
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblWorkCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetInternalComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.InternalComplianceScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.SecretKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }
                                            
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }

                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.InternalComplianceManagement.GetFile(fileId);
                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblUserName = (Label)e.Row.FindControl("lblUserName");
                string UserName = Convert.ToString(lblUserName.Text);

                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                string Status = Convert.ToString(lblStatus.Text);

                Label lblRemarks = (Label)e.Row.FindControl("lblRemarks");
                string Remarks = Convert.ToString(lblRemarks.Text);

                Label lblDocumentVersionView = (Label)e.Row.FindControl("lblcustomlabel");
                if (Status.ToLower() == "open" && Remarks.ToLower() == "new internal compliance assigned.")
                {
                    lblDocumentVersionView.Text = "Compliance assinged to " + UserName;
                }
                if (Status.ToLower() == "not  complied")
                {
                    lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName;
                }
                if (Status.ToLower() == "not complied")
                {
                    lblDocumentVersionView.Text = "Compliance has been not complied by " + UserName;
                }
                if (Status.ToLower() == "Rejected")
                {
                    lblDocumentVersionView.Text = "Compliance has been rejected by " + UserName;
                }
                if (Status.ToLower() == "complied but pending review")
                {
                    if (Remarks != "")
                        lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " in time with remark " + Remarks;
                    else
                        lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " in time ";
                }
                if (Status.ToLower() == "complied delayed but pending review")
                {
                    if (Remarks != "")
                        lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date time with remark " + Remarks;
                    else
                        lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date ";
                }
                if (Status.ToLower() == "complied delayed but pending review")
                {
                    if (Remarks != "")
                        lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date time with remark " + Remarks;
                    else
                        lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date ";
                }

                if (Status.ToLower() == "closed-timely")
                {
                    if (Remarks != "")
                        lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " in time with remark " + Remarks;
                    else
                        lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " in time ";
                }

                if (Status.ToLower() == "closed-delayed")
                {
                    if (Remarks != "")
                        lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " after due date with remark" + Remarks;
                    else
                        lblDocumentVersionView.Text = "Compliance has been  reviewed and closed by " + UserName + " after due date ";
                }

                if (Status.ToLower() == "approved")
                {
                    lblDocumentVersionView.Text = "Compliance has been approved and closed by " + UserName;
                }

                if (Status.ToLower() == "revise compliance")
                {
                    lblDocumentVersionView.Text = "Compliance has been revised by " + UserName;
                }

                //if (Status.ToLower() == "submitted for interim review")
                //{
                //    lblDocumentVersionView.Text = "Interim compliance has been reviewed by " + UserName;
                //}
                //if (Status.ToLower() == "interim review approved")
                //{
                //    lblDocumentVersionView.Text = "Interim compliance has been submitted for review approved by " + UserName;
                //}
                //if (Status.ToLower() == "interim rejected")
                //{
                //    lblDocumentVersionView.Text = "Interim compliance has been rejected by " + UserName;
                //}

                if (Status.ToLower() == "reminder")
                {
                    if (Remarks.ToLower() == "upcoming")
                        lblDocumentVersionView.Text = "Upcoming compliance reminder has been sent " + UserName;
                    if (Remarks.ToLower() == "overdue")
                        lblDocumentVersionView.Text = "Overdue compliance reminder has been sent " + UserName;
                    if (Remarks.ToLower() == "escalation notification")
                        lblDocumentVersionView.Text = "Escalation notification compliance reminder has been sent " + UserName;
                    if (Remarks.ToLower() == "pending for review")
                        lblDocumentVersionView.Text = "Pending for review compliance reminder has been sent " + UserName;
                    if (Remarks.ToLower() == "upcoming perofrmer monthly")
                        lblDocumentVersionView.Text = "Upcoming perofrmer monthly reminder has been sent " + UserName;
                    if (Remarks.ToLower() == "upcoming reviewer monthly")
                        lblDocumentVersionView.Text = "Upcoming reviewer monthly reminder has been sent " + UserName;
                    if (Remarks.ToLower() == "approver notification")
                        lblDocumentVersionView.Text = "Approver notification reminder has been sent " + UserName;
                }
                if (Status.ToLower() == "managementremark")
                {
                    lblDocumentVersionView.Text = UserName + " has added remark " + Remarks;
                }
            }
        }
        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]))
                    {
                        if (gvParentGrid.DataKeys[e.Row.RowIndex]["SendDate"] != null)
                        {
                            DateTime? Date = Convert.ToDateTime(gvParentGrid.DataKeys[e.Row.RowIndex]["SendDate"]);

                            //DateTime? Date = Convert.ToDateTime(e.Row.Cells[1].Text);
                            int? ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);

                            //GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                            //string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                            ComplianceDBEntities entities = new ComplianceDBEntities();
                            GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                            var compliance = entities.SP_GetAuditLogDetailInternal(ScheduledOnID, Date).ToList();
                            gv1.DataSource = compliance;
                            gv1.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveRemarks_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtManagementRemarks.Text != "")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        ManagementCommentsInternal cm = new ManagementCommentsInternal();
                        cm.remarks = txtManagementRemarks.Text;
                        cm.createdBy = AuthenticationHelper.UserID;
                        cm.createdDate = DateTime.UtcNow;
                        cm.ComplainceInstatnceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                        cm.ComplianceScheduleID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                        entities.ManagementCommentsInternals.Add(cm);
                        entities.SaveChanges();

                        int instanceId = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                        var compliance = (from row in entities.InternalComplianceAssignments
                                          where row.InternalComplianceInstanceID == instanceId
                                          select row).ToList();

                        var perf = (from row in compliance
                                    where row.RoleID == 3
                                    select row.UserID).FirstOrDefault();

                        var rev = (from row in compliance
                                   where row.RoleID == 4
                                   select row.UserID).FirstOrDefault();

                        try
                        {
                            string portalurl = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalurl = Urloutput.URL;
                            }
                            else
                            {
                                portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }

                            if (perf != 0)
                            {
                                var User = UserManagement.GetByID(Convert.ToInt32(perf));
                                var MgmtUser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;
                                var ShortDescription = Business.InternalComplianceManagement.GetCompliance(Convert.ToInt32(hdnComplianceId.Value)).IShortDescription;
                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                                  .Replace("@User", User.FirstName + " " + User.LastName)
                                                  .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                                  .Replace("@ShortDescription", ShortDescription)
                                                  .Replace("@Period", lblPeriod.Text)
                                                  .Replace("@Remarks", txtManagementRemarks.Text)
                                                  .Replace("@URL", Convert.ToString(portalurl))
                                                  .Replace("@From", ReplyEmailAddressName);

                                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                //                    .Replace("@User", User.FirstName + " " + User.LastName)
                                //                    .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                //                    .Replace("@ShortDescription", ShortDescription)
                                //                    .Replace("@Period", lblPeriod.Text)
                                //                    .Replace("@Remarks", txtManagementRemarks.Text)
                                //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                //                    .Replace("@From", ReplyEmailAddressName);

                                new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { User.Email }).ToList(), null, null, "AVACOM Notification for Management Remarks/Comment.", message); }).Start();
                            }

                            if (rev != 0)
                            {
                                var User = UserManagement.GetByID(Convert.ToInt32(rev));
                                var MgmtUser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;
                                var ShortDescription = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(hdnComplianceId.Value)).ShortDescription;
                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                                .Replace("@User", User.FirstName + " " + User.LastName)
                                                .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                                .Replace("@ShortDescription", ShortDescription)
                                                .Replace("@Period", lblPeriod.Text)
                                                .Replace("@Remarks", txtManagementRemarks.Text)
                                                .Replace("@URL", Convert.ToString(portalurl))
                                                .Replace("@From", ReplyEmailAddressName);

                                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                //                    .Replace("@User", User.FirstName + " " + User.LastName)
                                //                    .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                //                    .Replace("@ShortDescription", ShortDescription)
                                //                    .Replace("@Period", lblPeriod.Text)
                                //                    .Replace("@Remarks", txtManagementRemarks.Text)
                                //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                //                    .Replace("@From", ReplyEmailAddressName);

                                new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { User.Email }).ToList(), null, null, "AVACOM Notification for Management Remarks/Comment.", message); }).Start();
                            }
                        }
                        catch (Exception)
                        {
                        }

                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Comment Saved Successfully.";


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
    }
}