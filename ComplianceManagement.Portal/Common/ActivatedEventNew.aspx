﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ActivatedEventNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ActivatedEventNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-checkbox-wrapper {
            display: inline-block;
            vertical-align: middle;
            margin-left: -13px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 310px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden !important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>

    <script type="text/javascript">

        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;

            var FilterForAllSearch = document.getElementById('txtSearchfilter').value;

            if (locationlist.length > 0
                || ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != -1 && $("#dropdownlistComplianceType1").val() != "")
                || FilterForAllSearch != "") {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != -1 && $("#dropdownlistComplianceType1").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "EventClassificationID", operator: "eq", value: parseInt($("#dropdownlistComplianceType1").val())
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
                if (FilterForAllSearch != "" && FilterForAllSearch != undefined) {
                    var AllFilter = { logic: "or", filters: [] };

                    AllFilter.filters.push({
                        field: "Name", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "UserName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "EventDescription", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "CustomerBranchName", operator: "contains", value: FilterForAllSearch
                    });

                    finalSelectedfilter.filters.push(AllFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                dataSource.pageSize(total);
            }
        }

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.pageSize(10);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }
        function CloseMyReminderPopup() {
            $('#divShowReminderDialog').modal('hide');
            BindGrid();
        }
        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:4px;vertical-align: baseline;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

    </script>

    <script type="text/javascript">
        var record = 0;
        var total = 0;

        $("form.k-filter-menu button[type='reset']").trigger("click");

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Activated Events');
            BindGrid();

            $("#txtSearchfilter").on('input', function (e) {
                FilterAll();
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({
                placeholder: "Select Type",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Type",
                autoClose: true,
                dataSource: [
                    { text: "Secretarial", value: "1" },
                    { text: "Non Secretarial", value: "2" },
                ],
                index: 0,
                change: function (e) {
                    FilterAll();

                }
            });

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        <%--                        read: "<% =Path%>Data/GetLocationList?UserId=<% =UId%>"--%>
                        read: {
                            url: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S",
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    //},
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

        });


        var record = 0;
        var Details = "";
        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(11)').remove();
                },
                dataSource: {
                    transport: {
                        read: {
                            url: "<% =Path%>Data/ActivatedEventData?eventRoleID=10&UserID=<%=UId%>&CustomerID=<% =CustId%>",
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/ActivatedEventData?eventRoleID=10&UserID=<%=UId%>&CustomerID=<% =CustId%>"
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                groupable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    pageSizes: ['All', 5, 10, 20],
                    filterable: true,
                    buttonCount: 3,
                    numeric: true,
                    pageSize: 10
                },
                dataBinding: function () {
                    record = 0;
                    total = this.dataSource._total;

                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        else if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }
                    //if (this.dataSource.pageSize() < 10 && this.dataSource.pageSize() != total) {
                    //    this.dataSource.pageSize(10)
                    //}
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                },
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        title: "Sr.No.",
                        template: "#= ++record #",
                        width: "60px",
                    },
                    {
                        field: "Name",
                        title: "Event Name",
                        width: "400px;",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CustomerBranchName",
                        title: "Location",
                        width: "200px",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "UserName",
                        title: "User",
                        width: "150px",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EventDescription",
                        title: "Nature Of Event",
                        width: "180px",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" },
                        ], title: "Action", width: "8%", lock: true,
                        headerAttributes: {
                            style: "text-align: center;"
                        },

                    }
                ]
            });
            $('#grid').kendoTooltip({
                filter: "td[role=gridcell]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[role=columnheader]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[data-role=droptarget]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=7]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=8]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=9]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=10]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=11]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                width: 80,
                content: function (e) {
                    return "Edit Event";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "View Event";
                }
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                debugger;
                kendo.ui.progress($(".chart-loading"), true);

                setTimeout(function () { kendo.ui.progress($(".chart-loading"), false); }, 3000);
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                Details = 'EventDetailKendo.aspx?eventId=' + item.ID + '&CustomerBranchID=' + item.CustomerBranchID + '&eventType=AciveEvent';
                OpenActivatedCompliancePopup();
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                kendo.ui.progress($(".chart-loading"), true);
                setTimeout(function () { kendo.ui.progress($(".chart-loading"), false); }, 3000);
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../Event/ViewPageActivated.aspx?ID=" + item.ID + '&Type=' + item.Type + '&EventScheduledOnID=' + item.EventScheduledOnId + '&CustomerBranchID=' + item.CustomerBranchID + '&EventClassificationID=' + item.EventClassificationID + '&EventDescription=' + item.EventDescription);
                $('#divShowReminderDialog').modal('show');

            });

        }

        function BindGridApply(e) {
            BindGrid();
            e.preventDefault();
        }

        function OpenActivatedCompliancePopup() {

            $("#divwindow").kendoWindow({
                modal: true,
                width: "97%",
                height: "600px",
                title: "Compliance Details",
                visible: false,
                draggable: false,
                content: Details,
                refresh: true,
                pinned: true,
                actions: [
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divwindow" style="display: none;"></div>

    <div class="row Dashboard-white-widget">
        <div class="col-lg-12 col-md-12 ">
            <section class="panel">
                      <header class="panel-heading tab-bg-primary ">
                           
                                          <ul id="rblRole1" class="nav nav-tabs">
                                                   <li class="dummyval">                                                     
                                                        <asp:LinkButton ID="liAssignedEvents" OnClick="liAssignedEvents_Click"  runat="server">Assigned Events</asp:LinkButton>
                                                    </li>
                                                  <li class="dummyval">                                                      
                                                           <asp:LinkButton ID="liActiveEvents" CssClass="active"  OnClick="liActiveEvents_Click"  runat="server">Activated Events</asp:LinkButton>
                                                    </li>
                                          </ul>
  
                        </header>
                </section>
        </div>
    </div>
    <div id="example">

        <div class="row" style="margin-bottom: 0.5%">
            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 200px;" />
            <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 200px; margin-left: 10px" />
            <input id='txtSearchfilter' class='k-textbox' placeholder="Type to Filter" style="width: 250px; margin-left: 10px" />

            <button id="Applyfilter" style="float: right; margin-left: 1%; display: none;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Apply Filter</button>
            <button id="ClearfilterMain" style="float: right; margin-left: 1%; height: 30px; display: block; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
        </div>
    </div>
    <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>

    <div id="grid"></div>
    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 936px; margin-left: -172px; height: 690px;">
                <div class="modal-header" style="height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" scrolling="no" frameborder="0" runat="server" width="100%" height="300px" style="height: 631px;"></iframe>
                </div>

            </div>
        </div>
    </div>
    <div class="chart-loading"></div>
</asp:Content>
