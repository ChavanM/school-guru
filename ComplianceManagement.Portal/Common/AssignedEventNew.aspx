﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="AssignedEventNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.AssignedEventNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
<style type="text/css">
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 390px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-checkbox-wrapper {
            display: inline-block;
            vertical-align: middle;
            margin-left: -13px;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden !important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-grid tbody .k-button
         {
            min-width: 21px;
            min-height: 30px;
            background: white;
            border: none;
         }

         .k-multiselect-wrap .k-input {
               /*padding-top:6px;*/
               display: inherit !important;
           }
    </style>

    <script type="text/javascript">

        var record = 0;
        var total = 0;

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Assigned Events');
            BindGrid();
            $("#txtSearchfilter").on('input', function (e) {
                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;
                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field == "Name" || x.field == "CustomerBranchName") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);
            });
            $("#dropdownlist").kendoDropDownList({
                placeholder: "Select Type",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Type",
                autoClose: true,
                dataSource: [
                    { text: "Secretarial", value: "1" },
                    { text: "Non Secretarial", value: "2" }
                ],
                index: 0,
                change: function (e) {
                    FilterAll();
                }
            });
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    //placeholder: "Entity/Sub-Entity/Location",
                        fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        });

        
        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;
            if (locationlist.length > 0
                || ($("#dropdownlist").val() != 0 && $("#dropdownlist").val() != -1 && $("#dropdownlist").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownlist").val() != 0 && $("#dropdownlist").val() != -1 && $("#dropdownlist").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "EventClassificationID", operator: "eq", value: parseInt($("#dropdownlist").val())
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                dataSource.pageSize(total);
            }
        }
        var Details = "";

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            var dataSource = $("#grid").data("kendoGrid").dataSource;
                dataSource.pageSize(10);           
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')

            CheckFilterClearorNotMain();
        };
        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="vertical-align:baseline;background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:4px;vertical-align: baseline;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

        function OpenActivatedCompliancePopup() {
            $("#divwindow").kendoWindow({
                modal: true,
                width: "97%",
                height: "600px",
                title: "Compliance Details",
                visible: false,
                draggable: false,
                content: Details,
                refresh: true,
                pinned: true,
                actions: [
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            return false;
        }

        var products = "";
        var EventID = [];
        var record = 0;

        function BindAssignedComplianceListFilters() {

            $("#tbxStartDate").kendoDatePicker({
                format: "dd-MM-yyyy"
            });

            $("#ddlFilterPerformer").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Performer",
                autoClose: true,
                change: function (e) {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Litigation/UserList?CutomerID=<% =CustId%>&Flags=" + false
                        },
                    }
                });

                $("#ddlFilterReviewer").kendoDropDownList({
                    dataTextField: "Name",
                    dataValueField: "ID",
                    optionLabel: "Select Reveiwer",
                    autoClose: true,
                    change: function (e) {
                        //Bindgrid();
                    },
                    dataSource: {
                        severFiltering: true,
                        transport: {
                            read: "<% =Path%>Litigation/UserList?CutomerID=<% =CustId%>&Flags=" + false
                        },
                    }
                });

                $("#ddlFilterApprover").kendoDropDownList({
                    dataTextField: "Name",
                    dataValueField: "ID",
                    optionLabel: "Select Approver",
                    autoClose: true,
                    change: function (e) {
                        //Bindgrid();
                    },
                    dataSource: {
                        severFiltering: true,
                        transport: {
                            read: "<% =Path%>Litigation/UserList?CutomerID=<% =CustId%>&Flags=" + true
                    },
                }
            });

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: '/Event/GetCheckdEvent?EventID=' + EventID.toString(),
                success: function (json) {
                    if (json.message == "No NotComplianceAssigned") {
                        $("#ddlEvent").kendoDropDownList({
                            dataTextField: "Name",
                            dataValueField: "ID",
                            optionLabel: "Select Event",
                            autoClose: true,
                            change: function (e) {
                            },
                            dataSource: json.status
                        });
                    }
                },
                failure: function (json) {
                }
            });

        }

        var dataSource = new kendo.data.DataSource({

            transport: {

                read: {
                    url: "<% =Path%>Data/AssignedEventData?eventRoleID=10&UserId=<%=UId%>&CustID=<% =CustId%>",
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                },

                update: {
                    url: "/Event/SaveAllActivatedEvent",
                },
                create: {
                },
                destroy: {
                }
            },
            pageSize: 10,
            schema: {
                model: {
                    id: "EventInstanceID",
                    fields: {
                        CustomerBranchName: { editable: false },
                        Name: { editable: false },
                        ActivateDate: { type: "date" },
                        NatureOfEventID: { type: "text" },
                    }
                }
            },
            requestEnd: onRequestEnd
        });

        function onRequestEnd(e) {

            if (e.type == "create") {
                e.sender.read();
            }
            else if (e.type == "update") {
                e.sender.read();
            }
        }
        function editAll() {
            var theGrid = $("#grid").data("kendoGrid");
            $("#grid tbody").find('tr').each(function () {
                var model = theGrid.dataItem(this);
                kendo.bind(this, model);
            });
            $("#grid").focus();
        }
        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(11)').remove();
                },
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                groupable: true,
                reorderable: true,
                resizable: true,
                persistSelection: true,
                multi: true,
                pageable: {
                    pageSizes: ['All', 5, 10, 20],
                    buttonCount: 3,
                    numeric: true,
                    pageSize: 10
                },
                dataBinding: function () {
                    record = 0;
                    total = this.dataSource._total;

                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        else if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }
                    //if (this.dataSource.pageSize() < 10 && this.dataSource.pageSize() != total) {
                    //    this.dataSource.pageSize(10)
                    //}
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                },
                dataBound: function () {
                    editAll();
                },
                columns: [
                    {
                        selectable: true,
                        width: "50px",

                    },
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "60px",
                    },
                    {
                        field: "Name",
                        title: "Event Name",
                        width: "22%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },

                    },
                    {
                        field: "CustomerBranchName",
                        type: { editable: "false" },
                        title: "Location",
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        template: "<input data-bind='value:NatureOfEventID'  type='text' class='k-textbox' onclick='this.value=removeSpaces(this.value)' />",
                      title: "Nature Of Event", width: "16%", },
                    {
                        template: "<input data-bind='value:ActivateDate'  type='date' class='k-textbox'/>",
                        format: "{0:yyyy-dd-MM}",
                        title: "Activation Date", width: "16%"
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit" }, //iconClass: "k-icon k-i-edit",
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" },
                        ], title: "Action", width: "8%", lock: true,
                        headerAttributes: {
                            style: "text-align: center;"
                        },

                    },
                ],

            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                if (item.ActivateDate != undefined &&
                    item.ActivateDate != null
                    && item.NatureOfEventID != undefined &&
                    item.NatureOfEventID != null) {
                    kendo.ui.progress($(".chart-loading"), true);
                    var items = [];
                    items.push(item);

                    items = JSON.stringify({ 'ActivatedEvents': items });

                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '/Event/CheckAllNotAssignedComplinceListForEvent',
                        data: items,
                        success: function (json) {
                            if (json.message == "NotComplianceAssigned") {

                                kendo.ui.progress($(".chart-loading"), true);
                                OpenNoAssignedCompliancePopup();
                                products = json.status;
                                EventID = json.EventID;
                                BindNotAssignedComplianceList();

                            }
                            else {
                                kendo.ui.progress($(".chart-loading"), true);
                                $.ajax({
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    type: 'POST',
                                    url: '/Event/SaveAllActivatedEventNew',
                                    data: items,
                                    success: function (json) {
                                        kendo.ui.progress($(".chart-loading"), false);
                                        if (json.message == "Successfully Activated") {
                                            alert("Event Assigned Successfully.");
                                        }
                                        $('#grid').data('kendoGrid').dataSource.read();

                                        $('#grid').data('kendoGrid').refresh();
                                        $('#grid').data('kendoGrid')._selectedIds = {};
                                        $('#grid').data('kendoGrid').clearSelection();
                                    },
                                    failure: function (json) {
                                    }
                                });
                            }

                        },
                        failure: function (json) {
                        }
                    });
                }
                else {
                    alert('Please select activate date and nature of event.');
                }
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {

                kendo.ui.progress($(".chart-loading"), true);

                setTimeout(function () { kendo.ui.progress($(".chart-loading"), false); }, 3000);
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                Details = 'EventDetailKendo.aspx?eventId=' + item.ID + '&CustomerBranchID=' + item.CustomerBranchID + '&eventType=AssignedEvent';
                OpenActivatedCompliancePopup();
            });

            $('#ApplyAll').kendoTooltip({
                position: "bottom",
                content: function (e) {
                    return "Save";
                }
            });
            
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Activate";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "View Event";
                }
            });
            $("#grid").kendoTooltip({
                filter: ("th:nth-child(n+2)"),
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid").kendoTooltip({
                filter: ("td:nth-child(n+2)"),
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=8]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=9]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

        }

        function close_windows() {
            $('div.k-window').each(function (index) {
                $(this).hide();
            });
        };

        function btn_ApplyAll() {
            debugger
            var grid = $('#grid').data("kendoGrid");
            var rows = $('#grid :checkbox:checked');

            var items = [];
            var validatopnFlag = 0;
            $.each(rows, function () {
                debugger;
                var item = grid.dataItem($(this).closest("tr"));
                if (item.ActivateDate == undefined || item.ActivateDate == null
                    && item.NatureOfEventID == undefined || item.NatureOfEventID == null || item.NatureOfEventID.trim() == "") {
                    validatopnFlag = 1;
                }
                
                    items.push(item);
                 
            });

            if (validatopnFlag == 0) {
                if (items.length > 0) {
                    kendo.ui.progress($(".chart-loading"), true);

                    items = JSON.stringify({ 'ActivatedEvents': items });

                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '/Event/CheckAllNotAssignedComplinceListForEvent',
                        data: items,
                        success: function (json) {
                            if (json.message == "NotComplianceAssigned") {
                                kendo.ui.progress($(".chart-loading"), true);
                                OpenNoAssignedCompliancePopup();
                                products = json.status;
                                EventID = json.EventID;
                                BindNotAssignedComplianceList();
                                kendo.ui.progress($(".chart-loading"), false);
                            }
                            else {
                                kendo.ui.progress($(".chart-loading"), true);
                                $.ajax({
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    type: 'POST',
                                    url: '/Event/SaveAllActivatedEventNew',
                                    data: items,
                                    success: function (json) {
                                      
                                        kendo.ui.progress($(".chart-loading"), false);
                                        alert("Event Assigned Successfully.");
                                        $('#grid').data('kendoGrid').dataSource.read();

                                        $('#grid').data('kendoGrid').refresh();
                                        $('#grid').data('kendoGrid')._selectedIds = {};
                                        $('#grid').data('kendoGrid').clearSelection();
                                    },
                                    failure: function (json) {
                                    }
                                });
                            }

                        },
                        failure: function (json) {
                        }
                    });
                }
                else {
                    alert("Please select at least one event.");
                    return;
                }
            }
            else {
                alert('Please select activation date and nature of event on selected row.');
                return;
            }
        }

        function OpenNoAssignedCompliancePopup() {

            $("#divCompliancewindow").kendoWindow({
                modal: true,
                width: "97%",
                height: "600px",
                title: "Compliance",
                visible: false,
                draggable: false,
                refresh: true,
                pinned: true,
                actions: [
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            return false;

        }

        function BindNotAssignedComplianceList() {

            var grid = $('#gridNotAssigned').data("kendoGrid");

            if (grid != undefined || grid != null)
                $('#gridNotAssigned').empty();

            var gridNotAssigned = $("#gridNotAssigned").kendoGrid({
                dataSource: {
                    data: products,
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                },
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        title: "ID",
                        field: "ID",
                        width: "8%",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "EvnetName",
                        title: "Event Name",
                        //width: "22%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },

                    },
                    {
                        field: "CustomerBranchName",
                        title: "Location",
                        editable: false,
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "ComplianceName",
                        title: "Compliance Name",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    }
                ]

            });
            $("#gridNotAssigned").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });
            $("#gridNotAssigned").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
            kendo.ui.progress($(".chart-loading"), false);
        }
        function btnComplianceList_Click(e) {
            $("#divAssignedComplianceList").kendoWindow({
                modal: true,
                width: "97%",
                height: "615px",
                title: "Assign Complaince",
                visible: false,
                position: {
                    top: 10,
                    left: 20,
                },
                draggable: false,
                refresh: true,
                actions: [
                    "Close"
                ]

            }).data("kendoWindow").open();

            BindAssignedComplianceListFilters();
            BindAssignedComplianceList();
        }

        function BindAssignedComplianceList() {

            var record = 0;
            var gridNew = $('#AssignedComplianceListgrid').data("kendoGrid");
            $('#AssignedComplianceListgrid').empty();
            if (gridNew != undefined || gridNew != null)
                $('#AssignedComplianceListgrid').empty();

            var gridNew = $("#AssignedComplianceListgrid").kendoGrid({
                dataSource: products,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                columns: [
                    {
                        title: "Assign",
                        selectable: true,
                        width: "10%"
                    },
                    {
                        title: "Sr No.",
                        template: "#= ++record #",
                        width: "7%",
                        style: "text-align:center;"
                    },
                    {
                        title: "ID",
                        field: "ID",
                        width: "8%",
                    },
                    {
                        field: "EvnetName",
                        title: "Event Name",
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CustomerBranchName",
                        title: "Location",
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ComplianceName", title: "Compliance Name",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                ]

            });
            $("#AssignedComplianceListgrid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#AssignedComplianceListgrid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

        }

        function SaveComplianceList_Click() {

            if ($("#tbxStartDate").val() == "" || $("#ddlFilterPerformer").val() == ""
                || $("#ddlFilterReviewer").val() == "") {
                if ($("#tbxStartDate").val() == "") {
                    alert("Please select start date.");
                }
                if ($("#ddlFilterPerformer").val() == "") {
                    alert("Please select performer.");
                }
                if ($("#ddlFilterReviewer").val() == "") {
                    alert("Please select Reviewer.");
                }
            }
            else {
                var grid = $('#AssignedComplianceListgrid').data("kendoGrid");
                var rows = $('#AssignedComplianceListgrid :checkbox:checked');
                var items = [];
                $.each(rows, function () {
                    var item = grid.dataItem($(this).closest("tr"));

                    item.tbxStartDate = $("#tbxStartDate").val();
                    item.ddlFilterPerformer = $("#ddlFilterPerformer").val();
                    item.ddlFilterReviewer = $("#ddlFilterReviewer").val();
                    item.ddlFilterApprover = $("#ddlFilterApprover").val();
                    items.push(item);

                });
                if (items.length > 0) {
                    kendo.ui.progress($(".chart-loading"), true);
                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '/Event/SaveComplianceList',
                        data: JSON.stringify(items),
                        success: function (json) {
                            kendo.ui.progress($(".chart-loading"), false);
                            if (json.message == "ComplianceList Saved") {
                                alert("Compliance Assign Successfully.");
                                $("#btncloseall").click();
                                $("#ApplyAll").click();
                                $("#tbxStartDate").val('');
                                $("#ddlFilterPerformer").data("kendoDropDownList").select(0);
                                $("#ddlFilterReviewer").data("kendoDropDownList").select(0);
                                $("#ddlFilterApprover").data("kendoDropDownList").select(0);
                            }
                            else {
                                alert("Error in Assigning Compliance");
                            }
                        },
                        failure: function (json) {
                        }
                    });
                }
                else {
                    alert('please select at least one event.');
                }
            }
        }

        $(document).ready(function () {
            var divAssignedComplianceList = $("#divAssignedComplianceList").kendoValidator().data("kendoValidator");

            $("#SaveComplianceList").click(function () {

                var ddlFilterPerformer = $('#ddlFilterPerformer').data("kendoDropDownList");

                var ddlFilterReviewer = $('#ddlFilterReviewer').data("kendoDropDownTree");

                var ddlFilterApprover = $('#ddlFilterApprover').data("kendoDropDownList");

                var ddlEvent = $('#dropdownCaseType').data("kendoDropDownList");

                if (ddlFilterPerformer.value() == "") {
                    ddlFilterPerformer.wrapper.find(".k-input").css("border", "1px solid red");
                }
                if (ddlFilterReviewer.value() == "") {
                    ddlFilterReviewer.wrapper.find(".k-input").css("border", "1px solid red");
                }
                if (ddlFilterApprover.value() == "0") {
                    ddlFilterApprover.wrapper.find(".k-input").css("border", "1px solid red");
                }
                if (ddlEvent.value() == "0") {
                    ddlEvent.wrapper.find(".k-input").css("border", "1px solid red");
                }

                if (divAssignedComplianceList.validate() && ddlFilterPerformer.value() != ""
                    && ddlFilterReviewer.value() != ""
                    && ddlFilterApprover.value() != "0" && ddlEvent.value() != "0") {
                    SaveComplianceList_Click();
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <div id="divwindow"></div>
    <div id="btncloseall" onclick="close_windows()" style="display: none;"></div>
    <div class="row Dashboard-white-widget">
        <div class="col-lg-12 col-md-12 ">
            <section class="panel">
                      <header class="panel-heading tab-bg-primary ">
                           
                                          <ul id="rblRole1" class="nav nav-tabs">
                                                   <li class="dummyval">                                                     
                                                        <asp:LinkButton ID="liAssignedEvents" CssClass="active" OnClick="liAssignedEvents_Click"  runat="server">Assigned Events</asp:LinkButton>
                                                    </li>
                                                  <li class="dummyval">                                                      
                                                           <asp:LinkButton ID="liActiveEvents" OnClick="liActiveEvents_Click"  runat="server">Activated Events</asp:LinkButton>
                                                    </li>
                                          </ul>
                        </header>
                </section>
        </div>
    </div>

    <input type="text" id="products" runat="server" style="display: none;" />
    <div id="divCompliancewindow" style="display: none;">
        <div style="margin: 5px; color: red; font-size: 17px; font-weight: bold;">
            Listed compliance are not assigned to performer and reviewer, kindly assign to process further for activation. Once assignmemt completed please click save at main page.
        </div>
        <div id="gridNotAssigned" style="border: none;"></div>
        <button style="width: 166px; margin-top: 1%;" id="btnComplianceList" class="btn btn-search" onclick="btnComplianceList_Click()">Assign Compliance</button>
    </div>
    <div id="example">
        <div class="row">
            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 200px;" />
            <input id="dropdownlist" data-placeholder="Type" style="width: 150px; margin-left: 10px" />
            <input id='txtSearchfilter' type="text" class='k-textbox' placeholder="Type to Filter" style="width: 220px; margin-left: 10px" onkeydown="return (event.keyCode!=13);" />
            <button id="ApplyAll" style="float: right;height: 27px;width: 7%;margin-top: 6px;" onclick="btn_ApplyAll(event)"><span onclick="javascript:return false;"></span>Save</button>

        </div>
        <div class="row" style="margin-bottom: 5px; margin-top: 5px;">
            <button id="ClearfilterMain" style="float: right; margin-left: 1%; display: none; height: 27px" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
        </div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
        <div id="grid"></div>

    </div>
    <div id="divAssignedComplianceList" style="display: none;">
        <div class="row" style="margin-bottom: 0.5%">
            <input id="ddlFilterPerformer" required data-placeholder="Performer" style="width: 200px;" />
            <input id="ddlFilterReviewer" required data-placeholder="Reviewer" style="width: 150px;" />
            <input id="ddlFilterApprover" required placeholder="Approver" style="width: 220px" />
            <input id="tbxStartDate" required placeholder="Start Date" style="width: 220px" />
            <input id="ddlEvent" required placeholder="Event Name" style="width: 220px" />
        </div>

        <div id="AssignedComplianceListgrid"></div>
        <button style="width: 170px; margin-top: 1%;" id="SaveComplianceList" class="btn btn-search" onclick="SaveComplianceList_Click()">Save</button>
    </div>
    <div class="chart-loading"></div>
</asp:Content>