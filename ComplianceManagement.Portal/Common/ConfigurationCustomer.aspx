﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ConfigurationCustomer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ConfigurationCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
          function fopenAlert() {
            alert("Frequency add successfully.");
            //$('#divComplianceTypeDialog').modal('hide');
            $("#divComplianceTypeDialog").dialog('close');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upFinancialYearList" runat="server" UpdateMode="Conditional" style="padding-top: 21px;">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center" style="width: 20%;">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" style="display:none;"/>
                        <asp:DropDownList runat="server" ID="ddlFrequencyList1" OnSelectedIndexChanged="ddlFrequencyList1_SelectedIndexChanged"
                        Style="padding: 0px; margin: 0px; height: 22px; width: 238px;padding-right: 24px;"
                            CssClass="txtbox" AutoPostBack="true" />
                        <asp:DropDownList runat="server" ID="ddlFrequencyType" OnSelectedIndexChanged="ddlFrequencyType_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 246px;"
                            CssClass="txtbox" AutoPostBack="true">
                            <asp:ListItem Text="Upcoming Notification" value="UN" Selected="true"/>
                            <asp:ListItem Text="Overdue Notification" value="ON"/>
                            <asp:ListItem Text="Pending for Review Notification" value="PRN"/>
                        </asp:DropDownList>
                        <asp:DropDownList runat="server" ID="ddlComplianceType" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 246px;"
                            CssClass="txtbox" AutoPostBack="true">
                            <asp:ListItem Text="Statutory" value="S"  Selected="true"/>
                            <asp:ListItem Text="Internal" value="I"/>
                            <asp:ListItem Text="Statutory Event" Value="SE" />
                            <asp:ListItem Text="Internal Event" Value="IE" />
                        </asp:DropDownList>
                    </td>
                    <td align="center" class="newlink">  
                        <asp:Button Text="Add New" runat="server" ID="btnAddNew" OnClick="btnAddNew_Click" CssClass="button"
                             />                      
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddFinancialYear"
                            OnClick="btnAddFinancialYear_Click"  style="padding-right: 43px;display:none;"/>
                        <asp:LinkButton Text="Escalation Detail" runat="server" ID="btnManageReminders"
                            OnClick="btnManageReminders_Click" style="display:none;" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdFrequency" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="60%" OnSorting="grdFrequency_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdFrequency_RowCommand" OnRowCreated="grdFrequency_RowCreated"
                OnPageIndexChanging="grdFrequency_PageIndexChanging" style="margin-left: 244px;margin-top: 32px;">
                <Columns>
                    <asp:BoundField DataField="Frequency_Name" HeaderText="Frequency" HeaderStyle-Height="20px" SortExpression="Frequency_Name" />
                    <asp:BoundField DataField="TimeInDays" HeaderText="Days" HeaderStyle-Height="20px" SortExpression="TimeInDays" />
                    <asp:BoundField DataField="RepeatEveryDays" HeaderText="Repeat Every Days" HeaderStyle-Height="20px" SortExpression="RepeatEveryDays" />
                    <asp:BoundField DataField="FlagName" HeaderText="Status" HeaderStyle-Height="20px" SortExpression="Flag" />
                    <asp:BoundField DataField="TypeName" HeaderText="Type" HeaderStyle-Height="20px" SortExpression="Flag" />
                    <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" style="display:none;"  runat="server" CommandName="EDIT_FINANCIAL_YEAR"
                                CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Financial Year" title="Edit Financial Year" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_FREQUENCY"
                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Frequency Days?');"><img src="../Images/delete_icon.png" alt="Delete" title="Delete" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divComplianceTypeDialog">
        <asp:UpdatePanel ID="upComplianceType" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceTypeValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceTypeValidationGroup" Display="None" />
                    </div>
                     <div style="margin-bottom: 7px;display:none;">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer Name</label>
                          <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 287px;"
                                    CssClass="txtbox" AutoPostBack="true" />
                    </div>
                       
                       </div>
                         <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Type</label>
                        <asp:DropDownList runat="server" ID="DropDownList2" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 287px;"
                            CssClass="txtbox" AutoPostBack="true">
                            <asp:ListItem Text="Statutory" Value="S" Selected="true" />
                            <asp:ListItem Text="Internal" Value="I" />
                            <asp:ListItem Text="Statutory Event" Value="SE" />
                            <asp:ListItem Text="Internal Event" Value="IE" />
                        </asp:DropDownList>
                    </div>
                         <div style="margin-bottom: 7px" id="frequencydisplay" runat="server">
                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Frequency</label>
                         <asp:DropDownList runat="server" ID="ddlFrequencyList" Style="padding: 0px; margin: 0px; height: 22px; width: 287px;"
                                    CssClass="txtbox" AutoPostBack="true" />
                          <%--  <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Frequency." ControlToValidate="ddlFrequencyList"
            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceTypeValidationGroup"
            Display="None" />--%>
                     
                    </div>
                    <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:DropDownList runat="server" ID="DropDownList1" OnSelectedIndexChanged="ddlFrequencyType_SelectedIndexChanged" 
                        Style="padding: 0px; margin: 0px; height: 22px; width: 287px;"
                            CssClass="txtbox" AutoPostBack="true">
                            <asp:ListItem Text="Upcoming Notification" Value="UN" Selected="true" />
                            <asp:ListItem Text="Overdue Notification" Value="ON" />
                            <asp:ListItem Text="Pending for Review Notification" Value="PRN" />
                        </asp:DropDownList>
                    </div>
                      
                    <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Days</label>
                        <asp:TextBox runat="server" onkeypress="return isNumber(event)" ID="tbxTimeInDays" Style="height: 16px; width: 200px;" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Days can not be empty."
                            ControlToValidate="tbxTimeInDays" runat="server" ValidationGroup="ComplianceTypeValidationGroup"
                            Display="None" />                       
                    </div>
                     <div style="margin-bottom: 7px">
                     <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Repeat Every(Days)</label>
                        <asp:TextBox runat="server" ID="txtRepeatDay" onkeypress="return isNumber(event)" Style="height: 16px; width: 200px;" MaxLength="100" />                      
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 66px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceTypeValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divComplianceTypeDialog').dialog('close');" />
                    </div>
                <%--</div>--%>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divReminderConfiguration">
        <asp:UpdatePanel ID="upReminderConfiguration" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Monthly:</label>
                        <asp:CheckBoxList runat="server" ID="cblMonthly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Quarterly:</label>
                        <asp:CheckBoxList runat="server" ID="cblQuarterly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            FourMonthly:</label>
                        <asp:CheckBoxList runat="server" ID="cblFourMonthly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Half Yearly:</label>
                        <asp:CheckBoxList runat="server" ID="cblHalfYearly" RepeatDirection="Horizontal"
                            RepeatLayout="Table" CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Annual:</label>
                        <asp:CheckBoxList runat="server" ID="cblYearly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Two Yearly:</label>
                        <asp:CheckBoxList runat="server" ID="cblTwoYearly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Seven Yearly:</label>
                        <asp:CheckBoxList runat="server" ID="cblSevenYearly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>

                    <div style="margin-bottom: 7px; float: right;">
                        <asp:Button Text="Save" runat="server" ID="btnSaveReminderConfiguration" OnClick="btnSaveReminderConfiguration_Click"
                            CssClass="button" />
                        <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divReminderConfiguration').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#divComplianceTypeDialog').dialog({
                height: 300,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Frequency Update Detail",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divReminderConfiguration').dialog({
                height: 390,
                width: 530,
                autoOpen: false,
                draggable: true,
                title: "Reminders Configuration",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>

