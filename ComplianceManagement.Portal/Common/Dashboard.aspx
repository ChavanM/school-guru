﻿<%@ Page Title="My Dashboard" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <meta name="author" content="AVANTIS - Development Team">

    <link href='<%# ResolveUrl("../NewCSS/responsive-calendar.css")%>' rel="stylesheet" type="text/css" />
    <style type="text/css">
        /*dailyupdates*/
          
        .tags > * {
                display: inline-block;
                list-style: none;
            }
            .card-text-sm {
                margin-bottom: -1rem !important;
                -webkit-margin-before: -30px;
                display: block;
                margin-block-start: 0;
                margin-block-end: 0;
                margin-inline-start: 0;
                margin-inline-end: 0;
                font-weight: bold;
            }
            .tags {
                margin: 0;
                padding: 0;
                margin-bottom: 0;
            }
            .h-100 {
                height: 100% !important;
            }
            .text-black-50 {
                font-size: 10px;
                color: rgba(0,0,0,.5) !important;
                margin-top: 15px;
                font-weight: 420;
            }
            .box {
                border: 1px solid transparent;
                padding: 10px;
                border-radius: 5px;
                background-color: #FFFFFF;
                box-shadow: 0 7px 20px 0 rgba(210,210,210,0.5);
            }
            .a-tag {
                color: #37A8F1;
                background-color: rgba(55, 168, 241, 0.20);
                font-size: 12px;
                font-weight: bold;
                padding: 6px 10px 7px;
                height: 28px;
                border-radius: 14px;
                display: inline-block;
                margin: 0 4px 10px;
                max-width: 130px;
                min-width: 104px;

                width: 100px;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
                text-align: center;
            }
            /*end dailyupdates*/ 

        .clscircle {
            margin-right: 7px !important;
        }

        .fixwidth {
            width: 20% !important;
        }

        .badge {
            font-size: 10px !important;
            font-weight: 200 !important;
        }

        .responsive-calendar .day {
            width: 13.7% !important;
            height: 50px;
        }

            .responsive-calendar .day.cal-header {
                border-bottom: none !important;
                width: 13.9% !important;
                font-size: 17px;
                height: 25px;
            }

        #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        .bx-viewport {
              height: 285px !important;
            margin-left: -41px;
            width: 110% !important;
        }

        #dailyupdates .bx-viewport {
            height: 326px !important;
        }

        .graphcmp {
            margin-left: 36%;
            font-size: 16px;
            margin-top: -3%;
            color: #666666;
            font-family: 'Roboto';
        }

        .days > div.day {
            margin: 1px;
            background: #eee;
        }

        .overdue ~ div > a {
            background: red;
        }

        .info-box {
            min-height: 105px !important;
        }

        .dashboardProgressbar {
            display: none;
        }

        #reviewersummary {
            height: 150px;
        }

        #performersummary {
            height: 150px;
        }

        #eventownersummary {
            height: 150px;
        }

        #performersummarytask {
            height: 150px;
        }

        #reviewersummarytask {
            height: 150px;
        }

        .info-box {
            margin-bottom: 0px !important;
            border: 1px solid #ADD8E6;
        }

        div.panel {
            margin-bottom: 12px;
        }

        .panel .panel-heading .panel-actions {
            height: 25px !important;
        }

        hr {
            margin-bottom: 8px;
        }

        .panel .panel-heading h2 {
            font-size: 20px;
        }

          .clsDashboardtab{
              padding-left: 5px; 
              padding-right: 5px; 
              margin-right: 6px !important;
              background-color: #fff !important; 
              border-top-left-radius: 5px; 
              border-top-right-radius: 5px;               
          }

        .panel-heading .nav > li > a:hover {
            border-top-left-radius: 5px; 
              border-top-right-radius: 5px;
        }
    </style>
    <script>
        function ClosePop()
        {
            $('#divreports').modal('hide');
        }

        function KendoShowProgress() {
            debugger;
            kendo.ui.progress($(document.body), true);

            //setTimeout(function(){
            //    kendo.ui.progress($(document.body), false);
            //}, 2000); 
            //kendo.ui.progress(e.sender.element, true);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="clearfix" style="height: 0px"></div>
    <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ComplianceProductType == 3 && IsTLEntityAssignment){%>       
         <header class="panel-heading tab-bg-primary" style="background: transparent; padding-left: 0px;">
            <ul id="rblDashboard" class="nav nav-tabs">
                <li class="active clsDashboardtab" id="liAVACOMDashboard" runat="server" style="margin-right:5px !important;">
                    <asp:LinkButton ID="lnkShowAVACOMDashboard" runat="server" Style="font-size: 16px; line-height: 1.0;">
                        <i class="fa fa-users" style="padding-left: 0.5em; padding-right: 0.5em;"></i>Self</asp:LinkButton>
                </li>
                <li class="" id="liTLDashboard" runat="server" style="padding-left: 5px; padding-right: 6px;">
                    <asp:LinkButton ID="lnkShowTLDashboard" runat="server" Style="font-size: 16px; line-height: 1.0;" OnClientClick="KendoShowProgress()" OnClick="lnkShowTLDashboard_Click">
                        <i class="fa fa-sitemap" style="padding-left: 0.5em; padding-right: 0.5em;"></i>Managed</asp:LinkButton> <%--PostBackUrl="~/RLCS/RLCS_HRMDashboardNew.aspx">--%>
                </li>
            </ul>
          </header>
        <%}%>
    <div class="clearfix" style="height: 15px"></div>
     <% if (user_Roles.Contains("MGMT") || DeptHead) {%>
        <header class="panel-heading tab-bg-primary" style="background: transparent; padding-left: 0px;">
            <ul id="rblRole1" class="nav nav-tabs" >                    
                 <% if (DeptHead){%>                                     
                     <li id="li1" runat="server" style="padding-left: 5px;padding-right: 6px;margin-right: 6px!important;">
                        <asp:LinkButton ID="LinkButton1" PostBackUrl="~/Management/MgmtDashboardDeptHead.aspx"  runat="server"  Style="font-size: 16px;line-height: 1.0;">Department Head</asp:LinkButton>                                           
                    </li>
                <%}else if (user_Roles.Contains("MGMT")){%>
                  <li id="li2" runat="server" style="padding-left: 5px;padding-right: 6px;margin-right: 6px!important;">
                        <asp:LinkButton ID="LinkButton3" PostBackUrl="~/Management/ManagementDashboardNew.aspx"  runat="server"  Style="font-size: 16px;line-height: 1.0;">Management</asp:LinkButton>                                           
                    </li>
                <%}else{%>            
                    <li  class="active" id="li3" runat="server" style="padding-left: 5px;padding-right: 6px;">
                        <asp:LinkButton ID="LinkButton2"  PostBackUrl="~/common/Dashboard.aspx" Style="font-size: 16px;line-height: 1.0;" runat="server">Performer/Reviewer</asp:LinkButton>                                        
                    </li>
                <%}%> 
            </ul>      
        </header>
     <%}%>
    <div style="clear:both;height: 9px;"></div>
    <section class="wrapper" style="width: 98%; margin: 0;">
                <div class="row">
                    <div class="dashboard">                       
                        <%----%>
                          <% if (user_Roles.Contains("EXCT") || user_Roles.Contains("CADMN") || user_Roles.Contains("MGMT") ||com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ComplianceProductType==3)%>
                            <% { %>                       
                               <% if (roles.Contains(3) && roles.Contains(4) && roles.Contains(10))%>
                                 <% { %>
                                        <!--Performer Summary-->
                                        <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Performer Summary</h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>                              
                                                <div id="collapsePerformer" class="panel-collapse collapse in">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                            <div class="info-box white-bg">
                                                                <div class="title">Upcoming</div>                                                                                                                                                                                                                                                                                                                                                                    
                                                              <% if (checkInternalapplicable.Equals(1))%>
                                                              <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                              <% }
                                                                  else
                                                                  { %> 
                                                                    <div class="col-md-12">
                                                               <% }%>              
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Upcoming&role=Performer&lflag=<% =PerLicenseUpcoming%>" onclick="settracknew('Dashboard','Summary','Upcoming','');">                                                                                                                                                                                                                                                                                         
                                                                              <div class="count" runat="server" id="divupcomingPREOcount">0</div>
                                                                         </a>
                                                                     <div class="desc">Statutory</div>
                                                                     <div class="progress thin dashboardProgressbar">
                                                                         <% var guage = GetPercentage("PUS", "PREO"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                               </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                  <div class="col-md-6">
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Upcoming&role=Performer&lflag=<% =PerInternalLicenseUpcoming%>" onclick="settracknew('Dashboard','Summary','Upcoming','');">   
                                                                        <div class="count" runat="server" id="divupcomingInternalPREOcount">0</div>
                                                                         </a>
                                                                        <div class="desc">Internal</div>
                                                                         <div class="progress thin dashboardProgressbar">
                                                                              <%  guage = GetPercentage("PUI", "PREO"); %>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>      
                                                                <% }%>                                                               
                                                                <div class="clearfix"></div>                                                                                                                           
                                                             </div>                                                            
                                                        </div>
                                                        
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Overdue</div>                                                                                                                                                                                         
                                                              <% if (checkInternalapplicable.Equals(1))%>
                                                              <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                              <% }
                                                                  else
                                                                  { %> 
                                                                    <div class="col-md-12">
                                                               <% }%>         
                                                                    <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Overdue&role=Performer&lflag=<% =PerLicenseOverdue%>" onclick="settracknew('Dashboard','Summary','Overdue','');">                                                                                                                                                                                            
                                                                        <div class="count" runat="server" id="divOverduePREOcount">0</div>
                                                                        </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("POS", "PREO"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>   
                                                                      <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                     
                                                                <div class="col-md-6">
                                                                    <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Overdue&role=Performer&lflag=<% =PerInternalLicenseOverdue%>" onclick="settracknew('Dashboard','Summary','Overdue','');">   
                                                                    <div class="count" runat="server" id="divOverdueInternalPREOcount">0</div>
                                                                          </a>
                                                                    <div class="desc">Internal</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("POI", "PREO"); %>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% }%>
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>

                                                        <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != 63)
                                                            {%>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Checklist</div>                                                                                                                                                                                           
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                                <% }
                                                                    else
                                                                    { %> 
                                                                    <div class="col-md-12">
                                                                <% }%>                         
                                                                        <a href="../Compliances/CheckListKendo.aspx?type=Statutory&filter=Overdue&role=Performer" onclick="settracknew('Dashboard','Summary','Checklist','');">                                
                                                                        <div class="count" runat="server" id="divPerformerChecklistPREOcount">0</div>
                                                                            </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("PCS", "PREO"); %>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                       </div>
                                                                </div>   
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                            
                                                                    <div class="col-md-6">
                                                                        
                                                                        <a href="../Compliances/CheckListKendo.aspx?type=Internal&filter=Overdue&role=Performer" onclick="settracknew('Dashboard','Summary','Checklist','');">
                                                                        <div class="count" runat="server" id="divPerformerChecklistInternalPREOcount">0</div>
                                                                            </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("PCI", "PREO"); %>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                                <% }%>                                                                   
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                        <% }%> 


                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Rejected</div>                                                                                                                                                                                           
                                                              <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                                <% }
                                                                    else
                                                                    { %> 
                                                                    <div class="col-md-12">
                                                                <% }%>            
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Rejected&role=Performer" onclick="settracknew('Dashboard','Summary','Rejected','');">                                                     
                                                                    <div class="count" runat="server" id="divPerformerRejectedPREOcount">0</div>
                                                                             </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                          <%  guage = GetPercentage("PRS", "PREO"); %>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                         </div>
                                                                         <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                         </div>
                                                                   </div>
                                                                </div>    
                                                               <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                           
                                                                    <div class="col-md-6">
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Rejected&role=Performer" onclick="settracknew('Dashboard','Summary','Rejected','');"> 
                                                                        <div class="count" runat="server" id="divPerformerRejectedInternalPREOcount">0</div>
                                                                             </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                              <%  guage = GetPercentage("PRI", "PREO"); %>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%>  
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Pending For Review</div>                                                                                                                                                                                           
                                                              <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                                <% }
                                                                    else
                                                                    { %> 
                                                                    <div class="col-md-12">
                                                                <% }%>                  
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=PendingForReview&role=Performer&lflag=<% =PerLicensePendingForReview%>" onclick="settracknew('Dashboard','Summary','Pending for review','');">                                                 
                                                                    <div class="count" runat="server" id="divPerformerPendingforReviewPREOcount">0</div>
                                                                            </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <%  guage = GetPercentage("PPRS", "PREO"); %>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                         </div>
                                                                         <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                         </div>
                                                                   </div>
                                                                </div>    
                                                               <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                           
                                                                    <div class="col-md-6">
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=PendingForReview&role=Performer&lflag=<% =PerInternalLicensePendingForReview%>" onclick="settracknew('Dashboard','Summary','Pending for review','');">  
                                                                        <div class="count" runat="server" id="divPerformerPendingforRevieweInternalPREOcount">0</div>
                                                                            </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <%  guage = GetPercentage("PPRI", "PREO"); %>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%>  
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->    
                                               </div>                                         
                                          </div>

                                        <!--Reviewer summary-->
                                        <div id="reviewersummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Reviewer Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseReviewer"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('reviewersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseReviewer" class="panel-collapse collapse in">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Due But Not Submitted</div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=DueButNotSubmitted&role=Reviewer&lflag=<% =RevLicenseDueButNotSubmitted%>">  
                                                                    <div class="count" runat="server" id="divDueButNotSubmittedPREOcount">0</div>
                                                                               </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <% var guageReviewer = GetPercentage("RDS", "PREO"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                            <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=DueButNotSubmitted&role=Reviewer&lflag=<% =RevInternalLicenseDueButNotSubmitted%>"> 
                                                                        <div class="count" runat="server" id="divDueButNotSubmittedInternalPREOcount">0</div>
                                                                             </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                                <% guageReviewer = GetPercentage("RDI", "PREO"); %>
                                                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:  <%=guageReviewer%>%">
                                                                                </div>
                                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                            <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Pending For Review</div>
                                                             <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=PendingForReview&role=Reviewer&lflag=<% =RevLicensePendingForReview%>">  
                                                                        <div class="count" runat="server" id="divReviewerPendingforReviewePREOcount">0</div>
                                                                              </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <% guageReviewer = GetPercentage("RPRS", "PREO"); %>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:  <%=guageReviewer%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                        <div class="col-md-6">
                                                                           
                                                                             <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=PendingForReview&role=Reviewer&lflag=<% =RevInternalLicensePendingForReview%>">  
                                                                            <div class="count"  runat="server" id="divReviewerPendingforRevieweInternalPREOcount">0</div>
                                                                                 </a>
                                                                            <div class="desc">Internal</div>
                                                                            <div class="progress thin dashboardProgressbar">
                                                                                <% guageReviewer = GetPercentage("RPRI", "PREO"); %>
                                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:  <%=guageReviewer%>%">
                                                                                    </div>
                                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                              <% }%> 
                                                            <div class="clearfix"></div>
                                                            
                                                        </div>
                                                    </div>

                                                     <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != 63)
                                                         {%>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Checklist</div>
                                                             <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                     <a href="../Compliances/CheckListKendo.aspx?type=Statutory&filter=Overdue&role=Reviewer">                                
                                                                    <div class="count" runat="server" id="divReviewerChecklistPREOcount">0</div>
                                                                          <%--    </a>--%>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <% guageReviewer = GetPercentage("RCS", "PREO"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:  <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                   </div>
                                                                </div>
                                                           <% if (checkInternalapplicable.Equals(1))%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                <a href="../Compliances/CheckListKendo.aspx?type=Internal&filter=Overdue&role=Reviewer">
                                                                <div class="count" runat="server" id="divReviewerChecklistInternalPREOcount">0</div>
                                                                     </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                    <% guageReviewer = GetPercentage("RCI", "PREO"); %>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:  <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>
                                                        <% }%>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Rejected</div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Rejected&role=Reviewer"> 
                                                                <div class="count"  runat="server" id="divReviewerRejectedPREOcount">0</div>
                                                                              </a>
                                                                <div class="desc">Statutory</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                    <% guageReviewer = GetPercentage("RRS", "PREO"); %>
                                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:  <%=guageReviewer%>%">
                                                                    </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                    </div>
                                                               </div>
                                                             </div>
                                                           <% if (checkInternalapplicable.Equals(1))%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                 <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Rejected&role=Reviewer"> 
                                                                <div class="count" runat="server" id="divReviewerRejectedInternalPREOcount">0</div>
                                                                     </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                    <% guageReviewer = GetPercentage("RRI", "PREO"); %>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:  <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>
                                                </div><!--/.row-->
                                            </div>
                                        </div>
                                        <!--Event Owner -->
                                         <div id="eventownersummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Event Owner Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseeventowner"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('eventownersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div> 
                                              <div id="collapseeventowner" class="panel-collapse collapse in">
                                                  <div class="row">
                                                      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Assigned Events</div>                                                           
                                                             <div class="col-md-12">
                                                               <a href="../Common/AssignedEventNew.aspx?type=assigned"> 
                                                                   <div class="count" runat="server" id="divAssignedEventPREOcount">0</div>
                                                               </a>
                                                                <div class="desc">Statutory</div>
                                                            </div>
                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                 <% var guageEvent = GetPercentage("AES", "PREO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Activated Events</div>                                                          
                                                            <div class="col-md-12">
                                                               <a href="../Common/ActivatedEventNew.aspx?type=active"> 
                                                                   <div class="count" runat="server" id="divActivatedEventPREOcount">0</div>
                                                               </a>
                                                                <div class="desc">Statutory</div>
                                                            </div>                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                 <% guageEvent = GetPercentage("ACES", "PREO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Closed Events</div>                                                          
                                                            <div class="col-md-12">
                                                               <a href="../Event/EventClose.aspx"> 
                                                                   <div class="count" runat="server" id="divClosedEventPREOcount">0</div>
                                                               </a>
                                                                <div class="desc">Statutory</div>
                                                            </div>                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                 <% guageEvent = GetPercentage("CES", "PREO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div><!--/.row-->
                                              </div>
                                        </div>
                                 <% } %>   
                                <% else if (roles.Contains(3) && roles.Contains(4))%>
                                 <% { %>  
                                      <!--Performer Summary If Performer And Reviewer-->
                                        <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Performer Summary</h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>                              
                                                <div id="collapsePerformer" class="panel-collapse collapse in">
                                                    <div class="row ">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Upcoming</div>                                                                                                                                                                                        
                                                                      <% if (checkInternalapplicable.Equals(1))%>
                                                                      <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                      <% }
                                                                          else
                                                                          { %> 
                                                                       <div class="col-md-12">
                                                                      <% }%>              
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Upcoming&role=Performer&lflag=<% =PerLicenseUpcoming%>">                                                                                                                                                                          
                                                                     <div class="count" runat="server" id="divupcomingPRcount">0</div>
                                                                               </a>
                                                                     <div class="desc">Statutory</div>
                                                                     <div class="progress thin dashboardProgressbar">
                                                                           <% var guage = GetPercentage("PUS", "PREO");%>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Upcoming&role=Performer&lflag=<% =PerInternalLicenseUpcoming%>">     
                                                                        <div class="count" runat="server" id="divupcomingInternalPRcount">0</div>
                                                                             </a>
                                                                        <div class="desc">Internal</div>
                                                                         <div class="progress thin dashboardProgressbar">
                                                                              <%  guage = GetPercentage("PUI", "PR");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% }%> 
                                                                <div class="clearfix"></div>                                                                                                                           
                                                              </div>
                                                           </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Overdue</div>
                                                                 <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Overdue&role=Performer&lflag=<% =PerLicenseOverdue%>"> 
                                                                    <div class="count" runat="server" id="divOverduePRcount">0</div>
                                                                             </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <%  guage = GetPercentage("POS", "PR");%>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                <div class="col-md-6">
                                                                    <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Overdue&role=Performer&lflag=<% =PerInternalLicenseOverdue%>">   
                                                                    <div class="count" runat="server" id="divOverdueInternalPRcount">0</div>
                                                                        </a>
                                                                    <div class="desc">Internal</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <%  guage = GetPercentage("POI", "PR");%>
                                                                       <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% } %>  
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                         <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != 63)
                                                             {%>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Checklist</div>
                                                                 <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                     <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Compliances/CheckListKendo.aspx?type=Statutory&filter=Overdue&role=Performer">                                
                                                                        <div class="count" runat="server" id="divPerformerChecklistPRcount">0</div>
                                                                             </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <%  guage = GetPercentage("PCS", "PR");%>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                       </div>
                                                                </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                        <a href="../Compliances/CheckListKendo.aspx?type=Internal&filter=Overdue&role=Performer">
                                                                        <div class="count" runat="server" id="divPerformerChecklistInternalPRcount">0</div>
                                                                         </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <%  guage = GetPercentage("PCI", "PR");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                         <%}%>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Rejected</div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Rejected&role=Performer">   
                                                                    <div class="count" runat="server" id="divPerformerRejectedPRcount">0</div>
                                                                               </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("PRS", "PR");%>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                         </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                         </div>
                                                                   </div>
                                                                </div>
                                                               <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Rejected&role=Performer"> 
                                                                        <div class="count" runat="server" id="divPerformerRejectedInternalPRcount">0</div>
                                                                             </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("PRI", "PR");%>
                                                                           <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>

                                                         <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Pending For Review</div>                                                                                                                                                                                           
                                                              <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                                <% }
                                                                    else
                                                                    { %> 
                                                                    <div class="col-md-12">
                                                                <% }%>                       
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=PendingForReview&role=Performer&lflag=<% =PerLicensePendingForReview%>">                                          
                                                                             <div class="count" runat="server" id="divPerformerPendingforReviewPRcount">0</div>
                                                                             </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("PPRS", "PR");%>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                   </div>
                                                                </div>    
                                                               <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                           
                                                                    <div class="col-md-6">
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=PendingForReview&role=Performer&lflag=<% =PerInternalLicensePendingForReview%>">
                                                                             <div class="count" runat="server" id="divPerformerPendingforRevieweInternalPRcount">0</div>
                                                                            </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("PPRI", "PR");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%>  
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->
                                                </div>
                                            </div>
                                        <!--Reviewer summary Performer And Reviewer-->
                                        <div id="reviewersummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Reviewer Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseReviewer"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('reviewersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseReviewer" class="panel-collapse collapse in">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Due But Not Submitted</div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=DueButNotSubmitted&role=Reviewer&lflag=<% =RevLicenseDueButNotSubmitted%>">  
                                                                    <div class="count" runat="server" id="divDueButNotSubmittedPRcount">0</div>
                                                                              </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <% var guageReviewer = GetPercentage("RDS", "PR"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                            <% { %>
                                                                    <div class="col-md-6">
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=DueButNotSubmitted&role=Reviewer&lflag=<% =RevInternalLicenseDueButNotSubmitted%>"> 
                                                                        <div class="count" runat="server" id="divDueButNotSubmittedInternalPRcount">0</div>
                                                                              </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <% guageReviewer = GetPercentage("RDI", "PR"); %>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                            <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Pending For Review</div>
                                                             <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=PendingForReview&role=Reviewer&lflag=<% =RevLicensePendingForReview%>">  
                                                                        <div class="count" runat="server" id="divReviewerPendingforReviewerPRcount">0</div>
                                                                               </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <% guageReviewer = GetPercentage("RPRS", "PR"); %>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                        <div class="col-md-6">
                                                                             <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=PendingForReview&role=Reviewer&lflag=<% =RevInternalLicensePendingForReview%>">  
                                                                            <div class="count"  runat="server" id="divReviewerPendingforReviewerInternalPRcount">0</div>
                                                                                 </a>
                                                                            <div class="desc">Internal</div>
                                                                            <div class="progress thin dashboardProgressbar">
                                                                                 <% guageReviewer = GetPercentage("RPRI", "PR"); %>
                                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                    </div>
                                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                              <% }%> 
                                                            <div class="clearfix"></div>
                                                            
                                                        </div>
                                                    </div>
                                                     <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != 63)
                                                         {%>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Checklist</div>
                                                             <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Compliances/CheckListKendo.aspx?type=Statutory&filter=Overdue&role=Reviewer">                                
                                                                    <div class="count" runat="server" id="divReviewerChecklistPRcount">0</div>
                                                                         </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <% guageReviewer = GetPercentage("RCS", "PR"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                   </div>
                                                                </div>
                                                           <% if (checkInternalapplicable.Equals(1))%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                <a href="../Compliances/CheckListKendo.aspx?type=Internal&filter=Overdue&role=Reviewer">
                                                                <div class="count" runat="server" id="divReviewerChecklistInternalPRcount">0</div>
                                                                 </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("RCI", "PR"); %>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>
                                                    <% }%> 
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Rejected</div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Rejected&role=Reviewer"> 
                                                                <div class="count"  runat="server" id="divReviewerRejectedPRcount">0</div>
                                                                                </a>
                                                                <div class="desc">Statutory</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("RRS", "PR"); %>
                                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                    </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                    </div>
                                                               </div>
                                                             </div>
                                                           <% if (checkInternalapplicable.Equals(1))%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                 <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Rejected&role=Reviewer"> 
                                                                <div class="count" runat="server" id="divReviewerRejectedInternalPRcount">0</div>
                                                                     </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                    <% guageReviewer = GetPercentage("RRI", "PR"); %>
                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>
                                                </div><!--/.row-->
                                            </div>
                                        </div>
                                       
                                 <% } %>                                                                          
                                <% else if (roles.Contains(3) && roles.Contains(10))%>
                                 <% { %>
                                    <!--Performer summary If Performer And Event Owner-->
                                        <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Performer Summary</h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>                              
                                                <div id="collapsePerformer" class="panel-collapse collapse in">
                                                    <div class="row ">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Upcoming</div>                                                                                                                                                                                        
                                                                      <% if (checkInternalapplicable.Equals(1))%>
                                                                      <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                      <% }
                                                                          else
                                                                          { %> 
                                                                       <div class="col-md-12">
                                                                      <% }%>                 
                                                                           <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Upcoming&role=Performer&lflag=<% =PerLicenseUpcoming%>">                                                                                                                                                                   
                                                                              <div class="count" runat="server" id="divupcomingPEOcount">0</div>
                                                                            </a>
                                                                     <div class="desc">Statutory</div>
                                                                     <div class="progress thin dashboardProgressbar">
                                                                         <% var guage = GetPercentage("PUS", "PEO");%>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                  </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Upcoming&role=Performer&lflag=<% =PerInternalLicenseUpcoming%>"> 
                                                                        <div class="count" runat="server" id="divupcomingInternalPEOcount">0</div>
                                                                             </a>
                                                                        <div class="desc">Internal</div>
                                                                         <div class="progress thin dashboardProgressbar">
                                                                              <%  guage = GetPercentage("PUI", "PEO");%>
                                                                           <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                                
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Overdue</div>
                                                                 <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Overdue&role=Performer&lflag=<% =PerLicenseOverdue%>">
                                                                    <div class="count" runat="server" id="divOverduePEOcount">0</div>
                                                                               </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("POS", "PEO");%>
                                                                       <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                <div class="col-md-6">
                                                                     <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Overdue&role=Performer&lflag=<% =PerInternalLicenseOverdue%>">
                                                                    <div class="count" runat="server" id="divOverdueInternalPEOcount">0</div>
                                                                         </a>
                                                                    <div class="desc">Internal</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("POI", "PEO");%>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% } %>  
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                        <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != 63)
                                                            {%>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Checklist</div>
                                                                 <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                     <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Compliances/CheckListKendo.aspx?type=Statutory&filter=Overdue&role=Performer">                                
                                                                        <div class="count" runat="server" id="divPerformerChecklistPEOcount">0</div>
                                                                              </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("PCS", "PEO");%>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                       </div>
                                                                </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Compliances/CheckListKendo.aspx?type=Internal&filter=Overdue&role=Performer">
                                                                        <div class="count" runat="server" id="divPerformerChecklistInternalPEOcount">0</div>
                                                                         <%--</a> --%>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("PCI", "PEO");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                             <% }%> 
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Rejected</div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Rejected&role=Performer">  
                                                                    <div class="count" runat="server" id="divPerformerRejectedPEOcount">0</div></a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("PRS", "PEO");%>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                   </div>
                                                                </div>
                                                               <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Rejected&role=Performer"> 
                                                                        <div class="count" runat="server" id="divPerformerRejectedInternalPEOcount">0</div>
                                                                             </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("PRI", "PEO");%>
                                                                           <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                            
                                                         <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Pending For Review</div>                                                                                                                                                                                           
                                                              <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                                <% }
                                                                    else
                                                                    { %> 
                                                                    <div class="col-md-12">
                                                                <% }%>                     
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=PendingForReview&role=Performer&lflag=<% =PerLicensePendingForReview%>">                                                                                            
                                                                    <div class="count" runat="server" id="divPerformerPendingforReviewPEOcount">0</div>
                                                                            </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("PPRS", "PEO");%>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                         </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                         </div>
                                                                   </div>
                                                                </div>
                                                               <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=PendingForReview&role=Performer&lflag=<% =PerInternalLicensePendingForReview%>">  
                                                                        <div class="count" runat="server" id="divPerformerPendingforRevieweInternalPEOcount">0</div>
                                                                            </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("PPRI", "PEO");%>
                                                                           <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->
                                                </div>
                                            </div>                                       
                                        <!--Event Owner If Performer And Event Owner -->
                                         <div id="eventownersummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Event Owner Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseeventowner"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('eventownersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div> 
                                              <div id="collapseeventowner" class="panel-collapse collapse in">
                                                  <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Assigned Events</div>                                                           
                                                             <div class="col-md-12">
                                                                <a href="../Common/EventDashboard.aspx?type=assigned"><div class="count" runat="server" id="divAssignedEventPEOcount">0</div></a>
                                                                <div class="desc">Statutory</div>
                                                            </div>
                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                <% var guageEvent = GetPercentage("AES", "PEO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Activated Events</div>                                                          
                                                            <div class="col-md-12">
                                                                 <a href="../Common/EventDashboard.aspx?type=active"><div class="count" runat="server" id="divActivatedEventPEOcount">0</div></a>
                                                                <div class="desc">Statutory</div>
                                                            </div>                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                 <% guageEvent = GetPercentage("ACES", "PEO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Closed Events</div>                                                          
                                                            <div class="col-md-12">
                                                               <a href="../Event/ClosedEvents.aspx"> 
                                                                   <div class="count" runat="server" id="divClosedEventPEOcount">0</div>
                                                               </a>
                                                                <div class="desc">Statutory</div>
                                                            </div>                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                 <% guageEvent = GetPercentage("CES", "PEO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div><!--/.row-->
                                              </div>
                                        </div>
                                 <% } %>                               
                                <% else if (roles.Contains(4) && roles.Contains(10))%>
                                 <% { %>                                      
                                        <!--Reviewer summary  If Reviewer And Event Owner-->
                                        <div id="reviewersummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Reviewer Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseReviewer"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('reviewersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseReviewer" class="panel-collapse collapse in">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Due But Not Submitted</div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=DueButNotSubmitted&role=Reviewer&lflag=<% =RevLicenseDueButNotSubmitted%>"> 
 
                                                                    <div class="count" runat="server" id="divDueButNotSubmittedREOcount">0</div>
                                                                             </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <% var guageReviewer = GetPercentage("RDS", "REO"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                            <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=DueButNotSubmitted&role=Reviewer&lflag=<% =RevInternalLicenseDueButNotSubmitted%>"> 
                                                                        <div class="count" runat="server" id="divDueButNotSubmittedInternalREOcount">0</div>
                                                                             </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                               <% guageReviewer = GetPercentage("RDI", "REO"); %>
                                                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                </div>
                                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                            <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Pending For Review</div>
                                                             <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=PendingForReview&role=Reviewer&lflag=<% =RevLicensePendingForReview%>"> 
                                                                        <div class="count" runat="server" id="divReviewerPendingforReviewerREOcount">0</div>
                                                                              </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <% guageReviewer = GetPercentage("RPRS", "REO"); %>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                        <div class="col-md-6">
                                                                              <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=PendingForReview&role=Reviewer&lflag=<% =RevInternalLicensePendingForReview%>">  
                                                                            <div class="count"  runat="server" id="divReviewerPendingforReviewerInternalREOcount">0</div>
                                                                                  </a>
                                                                            <div class="desc">Internal</div>
                                                                            <div class="progress thin dashboardProgressbar">
                                                                                 <% guageReviewer = GetPercentage("RPRI", "REO"); %>
                                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                    </div>
                                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                              <% }%> 
                                                            <div class="clearfix"></div>
                                                            
                                                        </div>
                                                    </div>

                                                     <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != 63)
                                                         {%>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Checklist</div>
                                                             <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                    <a href="../Compliances/CheckListKendo.aspx?type=Statutory&filter=Overdue&role=Reviewer">                                
                                                                    <div class="count" runat="server" id="divReviewerChecklistREOcount">0</div>
                                                                             <%-- </a>--%>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <% guageReviewer = GetPercentage("RCS", "REO"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                   </div>
                                                                </div>
                                                           <% if (checkInternalapplicable.Equals(1))%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                 <a href="../Compliances/CheckListKendo.aspx?type=Internal&filter=Overdue&role=Reviewer">
                                                                <div class="count" runat="server" id="divReviewerChecklistInternalREOcount">0</div>
                                                               <%-- </a>--%>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("RCI", "REO"); %>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>
                                                        <% }%> 
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Rejected</div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Rejected&role=Reviewer"> 
                                                                <div class="count"  runat="server" id="divReviewerRejectedREOcount">0</div>
                                                                              </a>
                                                                <div class="desc">Statutory</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("RRS", "REO"); %>
                                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                    </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                    </div>
                                                               </div>
                                                             </div>
                                                           <% if (checkInternalapplicable.Equals(1))%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                 <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Rejected&role=Reviewer">
                                                                <div class="count" runat="server" id="divReviewerRejectedInternalREOcount">0</div>
                                                                     </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("RRI", "REO"); %>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>
                                                </div><!--/.row-->
                                            </div>
                                        </div>
                                        <!--Event Owner If Reviewer And Event Owner -->
                                         <div id="eventownersummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Event Owner Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseeventowner"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('eventownersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div> 
                                              <div id="collapseeventowner" class="panel-collapse collapse in">
                                                  <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Assigned Events</div>                                                           
                                                             <div class="col-md-12">
                                                                <a href="../Common/EventDashboard.aspx?type=assigned"> 
                                                                <div class="count" runat="server" id="divAssignedEventREOcount">0</div></a>
                                                                <div class="desc">Statutory</div>
                                                            </div>
                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                <% var guageEvent = GetPercentage("AES", "REO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Activated Events</div>                                                          
                                                            <div class="col-md-12">
                                                                <a href="../Common/EventDashboard.aspx?type=active">
                                                                <div class="count" runat="server" id="divActivatedEventREOcount">0</div></a>
                                                                <div class="desc">Statutory</div>
                                                            </div>                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                <% guageEvent = GetPercentage("ACES", "REO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                       <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Closed Events</div>                                                          
                                                            <div class="col-md-12">
                                                               <a href="../Event/ClosedEvents.aspx"> 
                                                                   <div class="count" runat="server" id="divClosedEventREOcount">0</div>
                                                               </a>
                                                                <div class="desc">Statutory</div>
                                                            </div>                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                 <% guageEvent = GetPercentage("CES", "REO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div><!--/.row-->
                                              </div>
                                        </div>
                                 <% } %>                                                                                        
                                <%else if (roles.Contains(3))%>
                                 <% { %>
                                      <!--Performer summary-->
                                        <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Performer Summary</h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>                              
                                                <div id="collapsePerformer" class="panel-collapse collapse in">
                                                    <div class="row ">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Upcoming</div>                                                                                                                                                                                        
                                                                      <% if (checkInternalapplicable.Equals(1))%>
                                                                      <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                      <% }
                                                                          else
                                                                          { %> 
                                                                       <div class="col-md-12">
                                                                      <% }%>                      
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Upcoming&role=Performer&lflag=<% =PerLicenseUpcoming%>">                                                                                                                                                                 
                                                                                <div class="count" runat="server" id="divPerformerOnlyupcomingcount">0</div>
                                                                            </a>
                                                                     <div class="desc">Statutory</div>
                                                                     <div class="progress thin dashboardProgressbar">
                                                                         <% var guage = GetPercentage("PUS", "P");%>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Upcoming&role=Performer&lflag=<% =PerInternalLicenseUpcoming%>">  
                                                                        <div class="count" runat="server" id="divPerformerOnlyupcomingInternalcount">0</div>
                                                                             </a>
                                                                        <div class="desc">Internal</div>
                                                                         <div class="progress thin dashboardProgressbar">
                                                                              <% guage = GetPercentage("PUI", "P");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                                
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Overdue</div>
                                                                 <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Overdue&role=Performer&lflag=<% =PerLicenseOverdue%>">  
                                                                    <div class="count" runat="server" id="divPerformerOnlyOverduecount">0</div>
                                                                              </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%   guage = GetPercentage("POS", "P");%>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                <div class="col-md-6">
                                                                     <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Overdue&role=Performer&lflag=<% =PerInternalLicenseOverdue%>">   
                                                                    <div class="count" runat="server" id="divPerformerOnlyOverdueInternalcount">0</div>
                                                                         </a>
                                                                    <div class="desc">Internal</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%   guage = GetPercentage("POI", "P");%>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% } %>  
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                        <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != 63)
                                                            {%>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Checklist</div>
                                                                 <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                     <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Compliances/CheckListKendo.aspx?type=Statutory&filter=Overdue&role=Performer">                                
                                                                        <div class="count" runat="server" id="divPerformerOnlyChecklistcount">0</div>
                                                                      <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%   guage = GetPercentage("PCS", "P");%>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                       </div>
                                                                </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Compliances/CheckListKendo.aspx?type=Internal&filter=Overdue&role=Performer">
                                                                        <div class="count" runat="server" id="divPerformerOnlyChecklistInternalcount">0</div>
                                                                        </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%   guage = GetPercentage("PCI", "P");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                         <%}%> 
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Rejected</div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Rejected&role=Performer">     
                                                                    <div class="count" runat="server" id="divPerformerOnlyRejectedcount">0</div>
                                                                              </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%   guage = GetPercentage("PRS", "P");%>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                         </div>
                                                                         <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                         </div>
                                                                   </div>
                                                                </div>
                                                               <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Rejected&role=Performer"> 
                                                                        <div class="count" runat="server" id="divPerformerOnlyRejectedInternalcount">0</div>
                                                                            </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%   guage = GetPercentage("PRI", "P");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Pending For Review</div>                                                                                                                                                                                           
                                                              <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                                <% }
                                                                    else
                                                                    { %> 
                                                                    <div class="col-md-12">
                                                                <% }%>  
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=PendingForReview&role=Performer&lflag=<% =PerLicensePendingForReview%>">                                                              
                                                                    <div class="count" runat="server" id="divPerformerOnlyPendingforReviewcount">0</div>
                                                                              </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%   guage = GetPercentage("PPRS", "P");%>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                         </div>
                                                                         <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                         </div>
                                                                   </div>
                                                                </div>
                                                               <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=PendingForReview&role=Performer&lflag=<% =PerInternalLicensePendingForReview%>">  
                                                                        <div class="count" runat="server" id="divPerformerOnlyPendingforRevieweInternalcount">0</div>
                                                                             </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <%   guage = GetPercentage("PPRI", "P");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->
                                                </div>
                                            </div>
                                       
                                <%}%>
                                <%else if (roles.Contains(4))%>
                                 <% { %>
                                              
                                        <!--Reviewer summary-->
                                        <div id="reviewersummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Reviewer Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseReviewer"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('reviewersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseReviewer" class="panel-collapse collapse in">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Due But Not Submitted</div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=DueButNotSubmitted&role=Reviewer&lflag=<% =RevLicenseDueButNotSubmitted%>">  
                                                                           <div class="count" runat="server" id="divRevieweronlyDueButNotSubmittedcount">0</div>
                                                                             </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <% var guageReviewer = GetPercentage("RDS", "R"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                            <% { %>
                                                                    <div class="col-md-6">
                                                                        <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=DueButNotSubmitted&role=Reviewer&lflag=<% =RevInternalLicenseDueButNotSubmitted%>"> 
                                                                        <div class="count" runat="server" id="divRevieweronlyDueButNotSubmittedInternalcount">0</div>
                                                                            </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                                <% guageReviewer = GetPercentage("RDI", "R"); %>
                                                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                </div>
                                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                            <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Pending For Review</div>
                                                             <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=PendingForReview&role=Reviewer&lflag=<% =RevLicensePendingForReview%>">
                                                                        <div class="count" runat="server" id="divRevieweronlyReviewerPendingforReviewercount">0</div>
                                                                              </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <% guageReviewer = GetPercentage("RPRS", "R"); %>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% if (checkInternalapplicable.Equals(1))%>
                                                                <% { %>
                                                                        <div class="col-md-6">
                                                                             <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=PendingForReview&role=Reviewer&lflag=<% =RevInternalLicensePendingForReview%>">  
                                                                            <div class="count"  runat="server" id="divRevieweronlyReviewerPendingforReviewerInternalcount">0</div>
                                                                                 </a>
                                                                            <div class="desc">Internal</div>
                                                                            <div class="progress thin dashboardProgressbar">
                                                                                 <% guageReviewer = GetPercentage("RPRI", "R"); %>
                                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                    </div>
                                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                              <% }%> 
                                                            <div class="clearfix"></div>
                                                            
                                                        </div>
                                                    </div>
                                                           <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != 63)
                                                               {%>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Checklist</div>
                                                             <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Compliances/CheckListKendo.aspx?type=Statutory&filter=Overdue&role=Reviewer">                                
                                                                    <div class="count" runat="server" id="divRevieweronlyChecklistcount">0</div>
                                                                           <%--   </a>--%>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <% guageReviewer = GetPercentage("RCS", "R"); %>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                   </div>
                                                                </div>
                                                           <% if (checkInternalapplicable.Equals(1))%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                  <a href="../Compliances/CheckListKendo.aspx?type=Internal&filter=Overdue&role=Reviewer">
                                                                <div class="count" runat="server" id="divRevieweronlyChecklistInternalcount">0</div>
                                                                 </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("RCI", "R"); %>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>

                                                        <% }%> 
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Rejected</div>
                                                            <% if (checkInternalapplicable.Equals(1))%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Statutory&filter=Rejected&role=Reviewer">
                                                                <div class="count"  runat="server" id="divRevieweronlyRejectedcount">0</div>
                                                                                </a>
                                                                <div class="desc">Statutory</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("RRS", "R"); %>
                                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                    </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                    </div>
                                                               </div>
                                                             </div>
                                                           <% if (checkInternalapplicable.Equals(1))%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                 <a href="../Controls/frmUpcomingCompliancessNew.aspx?type=Internal&filter=Rejected&role=Reviewer">
                                                                <div class="count" runat="server" id="divRevieweronlyRejectedInternalcount">0</div>
                                                                     </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                        <% guageReviewer = GetPercentage("RRI", "R"); %>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>
                                                </div><!--/.row-->
                                            </div>
                                        </div>
                                                     </div>
                                          </div>          
                                  <%}%>
                                <%else if (roles.Contains(10))%>
                                 <% { %>                                       
                                        <!--Event Owner -->
                                         <div id="eventownersummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Event Owner Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseeventowner"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('eventownersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div> 
                                              <div id="collapseeventowner" class="panel-collapse collapse in">
                                                  <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Assigned Events</div>                                                           
                                                             <div class="col-md-12">
                                                                 <a href="../Common/EventDashboard.aspx?type=assigned"> 
                                                                <div class="count" runat="server" id="divEventOwnerAssignedEventOnlycount">0</div></a>
                                                                <div class="desc">Statutory</div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                <% var guageEvent = GetPercentage("AES", "EO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Activated Events</div>                                                          
                                                            <div class="col-md-12">
                                                                                <a href="../Common/EventDashboard.aspx?type=active">
                                                                <div class="count" runat="server" id="divEventOwnerActivatedEventOnlycount">0</div></a>
                                                                <div class="desc">Statutory</div>
                                                            </div>                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                <% guageEvent = GetPercentage("ACES", "EO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Closed Events</div>                                                          
                                                            <div class="col-md-12">
                                                               <a href="../Event/ClosedEvents.aspx"> 
                                                                   <div class="count" runat="server" id="divClosedEventOnlycount">0</div>
                                                               </a>
                                                                <div class="desc">Statutory</div>
                                                            </div>                                                          
                                                            <div class="clearfix"></div>
                                                            <div class="progress thin dashboardProgressbar">
                                                                 <% guageEvent = GetPercentage("CES", "EO"); %>
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageEvent%>%">
                                                                </div>
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageEvent%>%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div><!--/.row-->
                                              </div>
                                        </div>
                                 <% } %>
                          <% } %>                       
                   
                      <%--Task management start--%>     
                          <% if (checkTaskapplicable.Equals(1))%>
                           <%{%>    
                                  <%if (TaskRoles.Contains(3) && TaskRoles.Contains(4))%>
                                    <%{%>    
                                                
                                       <!--Performer Summary If Performer And Reviewer-->
                                        <div id="performersummarytask" class="col-lg-12 col-md-12 colpadding0">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Performer Task Summary</h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer1"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performersummarytask')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>                              
                                                <div id="collapsePerformer1" class="panel-collapse collapse in">
                                                    <div class="row ">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Upcoming</div>                                                                                                                                                                                        
                                                                      <% if (IsApplicableInternal)%>
                                                                      <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                      <% }
                                                                          else
                                                                          { %> 
                                                                       <div class="col-md-12">
                                                                      <% }%>              
                                                                          <a href="../Task/myTask.aspx?type=Statutory&filter=Upcoming&role=Performer">                                                                                                                                                                                                          
                                                                        <div class="count" runat="server" id="DivPerformerUpcomingStatBoth">0</div>
                                                                               </a>

                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <% var guage = GetPercentage("TPUS", "Performer");%>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>                                                                   
                                                                    </div>
                                                                 </div>
                                                                <% if (IsApplicableInternal)%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Task/myTask.aspx?type=Internal&filter=Upcoming&role=Performer">     
                                                                        <div class="count" runat="server" id="DivPerformerUpcomingIntBoth">0</div>
                                                                                </a>
                                                                        <div class="desc">Internal</div>
                                                                            <div class="progress thin dashboardProgressbar">
                                                                                <%  guage = GetPercentage("TPUI", "Performer");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% }%> 
                                                                <div class="clearfix"></div>                                                                                                                           
                                                              </div>
                                                           </div>

                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Overdue</div>
                                                                 <% if (IsApplicableInternal)%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                        
                                                                    <a href="../Task/myTask.aspx?type=Statutory&filter=Overdue&role=Performer"> 
                                                                    <div class="count" runat="server" id="DivPerformerOverdueStatBoth">0</div>
                                                                                </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <%  guage = GetPercentage("TPOS", "Performer");%>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% if (IsApplicableInternal)%>
                                                                <% { %>
                                                                <div class="col-md-6">
                                                                    <a href="../Task/myTask.aspx?type=Internal&filter=Overdue&role=Performer">   
                                                                    <div class="count" runat="server" id="DivPerformerOverdueIntBoth">0</div>
                                                                        </a>
                                                                    <div class="desc">Internal</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <%  guage = GetPercentage("TPOI", "Performer");%>
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% } %>  
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>                                                      
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Rejected</div>
                                                                <% if (IsApplicableInternal)%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Task/myTask.aspx?type=Statutory&filter=Rejected&role=Performer">   
                                                                    <div class="count" runat="server" id="DivPerformerRejectedStatBoth">0</div>
                                                                                </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                            <%  guage = GetPercentage("TPRS", "Performer");%>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                    </div>
                                                                   </div>                                                                
                                                               <% if (IsApplicableInternal)%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                           <a href="../Task/myTask.aspx?type=Internal&filter=Rejected&role=Performer"> 
                                                                        <div class="count" runat="server" id="DivPerformerRejectedIntBoth">0</div>
                                                                                </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                                <%  guage = GetPercentage("TPRI", "Performer");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>

                                                         <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Pending For Review</div>                                                                                                                                                                                           
                                                              <% if (IsApplicableInternal)%>
                                                                <% { %>                                                                                                                             
                                                                    <div class="col-md-6 borderright">
                                                                <% }
                                                                    else
                                                                    { %> 
                                                                    <div class="col-md-12">
                                                                <% }%>                       
                                                                    <a href="../Task/myTask.aspx?type=Statutory&filter=PendingForReview&role=Performer">                      

                                                                        <div class="count" runat="server" id="DivPerformerPendingReviewStatBoth">0</div>
                                                                        </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                            <%  guage = GetPercentage("TPPS", "Performer");%>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                    </div>
                                                                </div>    
                                                               <% if (IsApplicableInternal)%>
                                                                <% { %>                                                           
                                                                    <div class="col-md-6">
                                                                       <a href="../Task/myTask.aspx?type=Internal&filter=PendingForReview&role=Performer">
                                                                                <div class="count" runat="server" id="DivPerformerPendingReviewIntBoth">0</div>
                                                                            </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                                <%  guage = GetPercentage("TPPI", "Performer");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%>  
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->
                                                </div>
                                            </div>
                                                
                                                
                                                
                                                
                                             <!--Reviewer summary Performer And Reviewer-->
                                        <div id="reviewersummarytask" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                     <h2>Reviewer Task Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseReviewer1"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('reviewersummarytask')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseReviewer1" class="panel-collapse collapse in">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Due But Not Submitted</div>
                                                            <% if (IsApplicableInternal)%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Task/myTask.aspx?type=Statutory&filter=DueButNotSubmitted&role=Reviewer">  
                                                                    <div class="count" runat="server" id="DivReviewerDueNotSubStatBoth">0</div>
                                                                              </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <% var guageReviewer = GetPercentage("TRDS", "Reviewer");%>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <% if (IsApplicableInternal)%>
                                                            <% { %>
                                                                    <div class="col-md-6">
                                                                          <a href="../Task/myTask.aspx?type=Internal&filter=DueButNotSubmitted&role=Reviewer"> 
                                                                        <div class="count" runat="server" id="DivReviewerDueNotSubIntBoth">0</div>
                                                                              </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <% guageReviewer = GetPercentage("TRDI", "Reviewer");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                            <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Pending For Review</div>
                                                             <% if (IsApplicableInternal)%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Task/myTask.aspx?type=Statutory&filter=PendingForReview&role=Reviewer">  
                                                                        <div class="count" runat="server" id="DivReviewerPendingReviewStatBoth">0</div>
                                                                               </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <% guageReviewer = GetPercentage("TRPS", "Reviewer"); %>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% if (IsApplicableInternal)%>
                                                                <% { %>
                                                                        <div class="col-md-6">
                                                                            <a href="../Task/myTask.aspx?type=Internal&filter=PendingForReview&role=Reviewer">  
                                                                            <div class="count"  runat="server" id="DivReviewerPendingReviewIntBoth">0</div>
                                                                                 </a>
                                                                            <div class="desc">Internal</div>
                                                                            <div class="progress thin dashboardProgressbar">
                                                                                 <% guageReviewer = GetPercentage("TRPI", "Reviewer"); %>
                                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                    </div>
                                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                              <% }%> 
                                                            <div class="clearfix"></div>
                                                            
                                                        </div>
                                                    </div>

                                                   

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Rejected</div>
                                                            <% if (IsApplicableInternal)%>
                                                                 <% { %>                                                                                                                             
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                                  <a href="../Task/myTask.aspx?type=Statutory&filter=Rejected&role=Reviewer"> 
                                                                <div class="count"  runat="server" id="DivReviewerRejectedStatBoth">0</div>
                                                                                </a>
                                                                <div class="desc">Statutory</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("TRRS", "Reviewer"); %>
                                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                    </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                    </div>
                                                               </div>
                                                             </div>
                                                           <% if (IsApplicableInternal)%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                 <a href="../Task/myTask.aspx?type=Internal&filter=Rejected&role=Reviewer"> 
                                                                <div class="count" runat="server" id="DivReviewerRejectedIntBoth">0</div>
                                                                     </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                    <% guageReviewer = GetPercentage("TRRI", "Reviewer"); %>
                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>                                                            
                                                        </div>
                                                    </div>
                                                </div><!--/.row-->
                                            </div>
                                       </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                                  <%}%>     
                                  <%else if (TaskRoles.Contains(3))%>
                                  <%{%>  
                                      <!--Performer Summary If Performer And Reviewer-->
                                       <div id="performersummarytask" class="col-lg-12 col-md-12 colpadding0">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Performer Task Summary</h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer1"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performersummarytask')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>                              
                                                <div id="collapsePerformer1" class="panel-collapse collapse in">
                                                    <div class="row ">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Upcoming</div>                           
                                                                    <% if (IsApplicableInternal)%>
                                                                      <%{%>  
                                                                        <div class="col-md-6 borderright">
                                                                      <% }
                                                                          else
                                                                          { %> 
                                                                            <div class="col-md-12">
                                                                        <%}%>              
                                                                                <a href="../Task/myTask.aspx?type=Statutory&filter=Upcoming&role=Performer">                                                                                                                                                                                                          
                                                                                    <div class="count" runat="server" id="DivPerformerUpcomingStat">0</div>
                                                                               </a>
                                                                                 <div class="desc">Statutory</div>
                                                                                 <div class="progress thin dashboardProgressbar">
                                                                                       <% var guage = GetPercentage("TPUS", "Performer");%>
                                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                                    </div>
                                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                <% if (IsApplicableInternal)%>
                                                                <%{%>
                                                                    <div class="col-md-6">
                                                                         <a href="../Task/myTask.aspx?type=Internal&filter=Upcoming&role=Performer">     
                                                                        <div class="count" runat="server" id="DivPerformerUpcomingInt">0</div>
                                                                             </a>
                                                                        <div class="desc">Internal</div>
                                                                         <div class="progress thin dashboardProgressbar">
                                                                              <%  guage = GetPercentage("TPUI", "Performer");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% }%> 
                                                                <div class="clearfix"></div>                                                
                                                              </div>
                                                           </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Overdue</div>
                                                                 <% if (IsApplicableInternal)%>
                                                                 <% { %>  
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                         <a href="../Task/myTask.aspx?type=Statutory&filter=Overdue&role=Performer"> 
                                                                    <div class="count" runat="server" id="DivPerformerOverdueStat">0</div>
                                                                             </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <%  guage = GetPercentage("TPOS", "Performer");%>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% if (IsApplicableInternal)%>
                                                                <% { %>
                                                                <div class="col-md-6">
                                                                    <a href="../Task/myTask.aspx?type=Internal&filter=Overdue&role=Performer">   
                                                                    <div class="count" runat="server" id="DivPerformerOverdueInt">0</div>
                                                                        </a>
                                                                    <div class="desc">Internal</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                        <%  guage = GetPercentage("TPOI", "Performer");%>
                                                                       <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                        </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <% } %>  
                                                                <div class="clearfix"></div>                                
                                                            </div>
                                                        </div>
                                                        

                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Rejected</div>
                                                                <% if (IsApplicableInternal)%>
                                                                 <% { %>           
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Task/myTask.aspx?type=Statutory&filter=Rejected&role=Performer">   
                                                                    <div class="count" runat="server" id="DivPerformerRejectedStat">0</div>
                                                                               </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("TPRS", "Performer");%>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                         </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                         </div>
                                                                   </div>
                                                                </div>
                                                               <% if (IsApplicableInternal)%>
                                                                <% { %>
                                                                    <div class="col-md-6">
                                                                         <a href="../Task/myTask.aspx?type=Internal&filter=Rejected&role=Performer"> 
                                                                        <div class="count" runat="server" id="DivPerformerRejectedInt">0</div>
                                                                             </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("TPRI", "Performer");%>
                                                                           <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%> 
                                                                <div class="clearfix"></div> 
                                                            </div>
                                                        </div>
                                                         <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Pending For Review</div>                 

                                                              <% if (IsApplicableInternal)%>
                                                                <% { %>              
                                                                    <div class="col-md-6 borderright">
                                                                <% }
                                                                    else
                                                                    { %> 
                                                                    <div class="col-md-12">
                                                                <% }%>                       
                                                                         <a href="../Task/myTask.aspx?type=Statutory&filter=PendingForReview&role=Performer">                      

                                                                             <div class="count" runat="server" id="DivPerformerPendingReviewStat">0</div>
                                                                             </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <%  guage = GetPercentage("TPPS", "Performer");%>
                                                                         <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                   </div>
                                                                </div>    
                                                               <% if (IsApplicableInternal)%>
                                                                <% { %>       
                                                                    <div class="col-md-6">
                                                                        <a href="../Task/myTask.aspx?type=Internal&filter=PendingForReview&role=Performer">
                                                                             <div class="count" runat="server" id="DivPerformerPendingReviewInt">0</div>
                                                                            </a>
                                                                        <div class="desc" >Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <%  guage = GetPercentage("TPPI", "Performer");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guage%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guage%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               <% }%>  
                                                                <div class="clearfix"></div>  
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->
                                                </div>
                                            </div>
                                                            </div></div>
                                                    </div>
                                           <%--</div>--%>
                                 <%}%> 
                                 <% else if (TaskRoles.Contains(4))%>
                                 <%{%>    
                                     <!--Reviewer summary Performer And Reviewer-->
                                        <div id="reviewersummarytask" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2>Reviewer Task Summary</h2>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseReviewer1"><i class="fa fa-chevron-up"></i></a>
                                                        <a href="javascript:closeDiv('reviewersummarytask')" class="btn-close"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseReviewer1" class="panel-collapse collapse in">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Due But Not Submitted</div>
                                                            <% if (IsApplicableInternal)%>
                                                                 <% { %>    
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                          <a href="../Task/myTask.aspx?type=Statutory&filter=DueButNotSubmitted&role=Reviewer">  
                                                                    <div class="count" runat="server" id="DivReviewerDueNotSubStat">0</div>
                                                                              </a>
                                                                    <div class="desc">Statutory</div>
                                                                    <div class="progress thin dashboardProgressbar">
                                                                         <% var guageReviewer = GetPercentage("TRDS", "Reviewer");%>
                                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <% if (IsApplicableInternal)%>
                                                            <% { %>
                                                                    <div class="col-md-6">
                                                                          <a href="../Task/myTask.aspx?type=Internal&filter=DueButNotSubmitted&role=Reviewer"> 
                                                                        <div class="count" runat="server" id="DivReviewerDueNotSubInt">0</div>
                                                                              </a>
                                                                        <div class="desc">Internal</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                            <% guageReviewer = GetPercentage("TRDI", "Reviewer");%>
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                            <% }%> 
                                                            <div class="clearfix"></div>   
                                                        </div>
                                                    </div>
                                                        

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Pending For Review</div>
                                                             <% if (IsApplicableInternal)%>
                                                                 <% { %>  
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Task/myTask.aspx?type=Statutory&filter=PendingForReview&role=Reviewer">  
                                                                        <div class="count" runat="server" id="DivReviewerPendingReviewStat">0</div>
                                                                               </a>
                                                                        <div class="desc">Statutory</div>
                                                                        <div class="progress thin dashboardProgressbar">
                                                                             <% guageReviewer = GetPercentage("TRPS", "Reviewer"); %>
                                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <% if (IsApplicableInternal)%>
                                                                <% { %>
                                                                        <div class="col-md-6">
                                                                             <a href="../Task/myTask.aspx?type=Internal&filter=PendingForReview&role=Reviewer">  
                                                                            <div class="count"  runat="server" id="DivReviewerPendingReviewInt">0</div>
                                                                                 </a>
                                                                            <div class="desc">Internal</div>
                                                                            <div class="progress thin dashboardProgressbar">
                                                                                 <% guageReviewer = GetPercentage("TRPI", "Reviewer"); %>
                                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                                    </div>
                                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                              <% }%> 
                                                            <div class="clearfix"></div>
                                                            
                                                        </div>
                                                    </div>
                                                       
                                                    
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                                        <div class="info-box white-bg">
                                                            <div class="title">Rejected</div>
                                                            <% if (IsApplicableInternal)%>
                                                                 <% { %>         
                                                                        <div class="col-md-6 borderright">
                                                                 <% }
                                                                     else
                                                                     { %> 
                                                                     <div class="col-md-12">
                                                                  <% }%> 
                                                                           <a href="../Task/myTask.aspx?type=Statutory&filter=Rejected&role=Reviewer"> 
                                                                <div class="count"  runat="server" id="DivReviewerRejectedStat">0</div>
                                                                                </a>
                                                                <div class="desc">Statutory</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                     <% guageReviewer = GetPercentage("TRRS", "Reviewer"); %>
                                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                    </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                    </div>
                                                               </div>
                                                             </div>
                                                           <% if (IsApplicableInternal)%>
                                                           <% { %>
                                                            <div class="col-md-6">
                                                                 <a href="../Task/myTask.aspx?type=Internal&filter=Rejected&role=Reviewer"> 
                                                                <div class="count" runat="server" id="DivReviewerRejectedInt">0</div>
                                                                     </a>
                                                                <div class="desc">Internal</div>
                                                                <div class="progress thin dashboardProgressbar">
                                                                    <% guageReviewer = GetPercentage("TRRI", "Reviewer"); %>
                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=guageReviewer%>%">
                                                                        </div>
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-guageReviewer%>%">
                                                                        </div>
                                                                </div>
                                                            </div>
                                                          <% }%> 
                                                            <div class="clearfix"></div>    
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div></div> </div>
                                                </div><!--/.row-->
                                         <%--   </div>--%>
                                  <%}%> 
                             
                           <%}%>                
                          <%--Task Management End--%>
                
                <% if (user_Roles.Contains("EXCT") || user_Roles.Contains("CADMN") || user_Roles.Contains("MGMT"))%>
                <% { %>
                           <% if (roles.Contains(3) || roles.Contains(4))%>
                           <% { %>
                                    <div class="row Dashboard-white-widget" id="ComplianceCalender" style="margin-top: 10px;">
                                        <div class="dashboard">
                                            
                                            <div  class="col-lg-12 col-md-12 ">

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>My Compliance Calendar</h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformerCalender"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('ComplianceCalender')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>                                                        
                                                    </div>
                                                    
                                                </div>
                                                <div id="collapsePerformerCalender" class="panel-collapse collapse in">
                                                    <div class="row">
                                                     <div class="col-md-5 colpadding0">
                                         <!-- Responsive calendar - START -->
                                           <img class="imgDynamic_cal"  src="../images/loader.gif" style="display: none;position: fixed;top: 375px;z-index: 103;left: 447px;">
    	                                    <div class="responsive-calendar" style="width:95%">
                                                 
                                                
                                               <img src="../images/loader.gif">
                                              
                                            </div>
                                        <!-- Responsive calendar - END -->
                                                         <div><asp:linkbutton runat="server" ID="btnCreateICS" OnClick="btnCreateICS_Click"  OnClientClick="settracknew('Dashboard','Calendar','Download','');" ToolTip="Download Calendar (Outlook, Google)" data-toggle="tooltip" style="float:right; margin-right:76px">
                                                        <img src="../Images/Calendar_ICS.png" style="position: absolute;"/>
                                                         </asp:linkbutton></div>
                                        </div>
                                             <div  class="col-md-7">
                                                <div ><span style="float:left;height: 20px;font-family: 'Roboto',sans-serif;color: #666;font-size:16px;" id="clsdatel"></span>
                                                    <br /> <i style="font-family: 'Roboto',sans-serif;color: #666;">Select a date from calendar to view details</i>
                                                    
                                                </div>
                                                 <div class="clearfix" style="height:0px;"></div>
                                                   <div id="datacal">
                                                             <img src="../images/loader.gif" id="imgcaldate" style="position: absolute;">
                                                       <iframe  id="calframe" src="about:blank"  scrolling="no" frameborder="0" width="100%" height="350px"></iframe>
                                                   </div>
                                              </div>
                                              <div class="clearfix" style="height:10px"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        </div>
                          <% } %>
                <% } %>

                <div class="row Dashboard-white-widget" id="dailyupdates" >
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Daily Updates</h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseDailyUpdates"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('dailyupdates')" class="btn-close" ><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseDailyUpdates" class="panel-collapse collapse in">
                                <div class="row dailyupdates">
                                    <ul id="DailyUpdatesrow" class="bxslider bxdaily" style="width: 100%; max-width: none;">                                        
                                    </ul>
                                </div><!--/.row-->
                                <div class="clearfix" style="height:10px"></div>
                            </div>
                              <div class="modal fade" id="NewsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		                            <div class="modal-dialog" style="width:900px">
                                        <div class="modal-content" >
                                            <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                   
                        </div>
                                                    <div class="modal-body" id="dailyupdatecontent">
                                                         <h2 id="dailyupdatedate" style="margin-top:1px">Daily Updates: November 3, 2016</h2>
                                                            <h3 id="dailyupdatedateInner">
                                                             Daily Updates: November 3,
                                                            </h3>
                                                        <p id="dailytitle"></p>
                                                                <div id="contents" style="    color: #666666;">
                    </div>
                </div>
                                            </div>
                                        </div>
                            </div>
                                <div class="col-md-12">
                        <a class="btn btn-search" style="float: right;" onclick="settracknew('Dashboard','LegalUpdate','ViewMore','');" href="../Users/DailyUpdateList.aspx" title="View">View All</a>
                                <div class="clearfix" style="height:30px"></div>
                        </div>   
                        </div>
                    </div>
                </div>

                <div class="row Dashboard-white-widget" id="newsletter">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>News Letter</h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseNewsletter"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('newsletter')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseNewsletter" class="panel-collapse collapse in">
                                <div class="row dailyupdates">
                                    <ul id="Newslettersrow" class="bxslider bxnews" style="width: 100%; max-width: none;">
                                 </ul>
                                </div><!--/.row-->
                                 <div class="modal fade" id="Newslettermodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="width:550px">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                               
                                            </div>
                                            <div class="modal-body">
                                                 <img id="newsImg" src="../Images/xyz.png" style="border-bottom-left-radius:10px; border-bottom-right-radius:10px; width:100%" />
                                                 <h2  id="newsTitle"></h2>
                                                 <div class="clearfix" style="height:10px;"></div>
                                                 <div id="newsDesc"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                               <%-- <div class="clearfix" style="height:10px"></div>--%>
                                  <div class="col-md-12" >
                              <a class="btn btn-search" style="float: right;" href="../Users/NewsLetterList.aspx" onclick="settracknew('Dashboard','Newsletter','ViewMore','');" title="View">View All</a>
                                    <div class="clearfix" style="height:30px"></div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>

             <%--   //#region 27 FEB 2020
                <% if (user_Roles.Contains("EXCT") || user_Roles.Contains("CADMN") || user_Roles.Contains("MGMT"))%>
                <% { %>
                           <% if (roles.Contains(3) && roles.Contains(4))%>
                           <% { %>
                                    <div class="row Dashboard-white-widget" id="performerlocation">
                                        <div class="dashboard">
                                            <div  class="col-lg-12 col-md-12 ">

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Performer Location
                                                        </h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformerLoc"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performerlocation')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div id="collapsePerformerLoc" class="panel-collapse collapse in">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 colpadding0">
                                                            <div class="panel-body-map">
                                                                <div id="map" style="height:380px;"></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div style="text-align:center;padding-top:40px">
                                                                <ul class="bxslider-map" id="PerformerGraph" style="width: 100%; max-width: none;">
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->
                                                    <div class="clearfix" style="height:10px"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row Dashboard-white-widget" id="reviewerlocation">
                                        <div class="dashboard">
                                            <div  class="col-lg-12 col-md-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Reviewer Location</h2>
                                                        <div class="panel-actions">

                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePreviewerLoc"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('reviewerlocation')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="collapsePreviewerLoc" class="panel-collapse collapse in">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 colpadding0">
                                                            <div class="panel-body-map">
                                                                <div id="map2" style="height:380px;"></div>
                                                            </div>
                                                        </div>                                    
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div style="text-align:center;padding-top:40px">
                                                                <ul class="bxslider-map" id="ReviewerGraph" style="width: 100%; max-width: none;">
                                                                   
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->
                                                    <div class="clearfix" style="height:10px"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>              
                          <% } %>
                         <%else if (roles.Contains(3))%>
                            <% { %>
                                <div class="row Dashboard-white-widget" id="performerlocation">
                                        <div class="dashboard">
                                            <div  class="col-lg-12 col-md-12 ">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Performer Location</h2>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformerLoc"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performerlocation')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="collapsePerformerLoc" class="panel-collapse collapse in">
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 colpadding0">
                                                            <div class="panel-body-map">
                                                                <div id="map" style="height:380px;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            <div style="text-align:center;padding-top:40px">
                                                                <ul class="bxslider-map" id="PerformerGraph" style="width: 100%; max-width: none;">                                                                   
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div><!--/.row-->
                                                    <div class="clearfix" style="height:10px"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <% } %>
                         <%else if (roles.Contains(4))%>
                         <% { %>
                                <div class="row Dashboard-white-widget" id="reviewerlocation">
                                <div class="dashboard">
                                     <div  class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h2>Reviewer Location</h2>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePreviewerLoc"><i class="fa fa-chevron-up"></i></a>
                                                <a href="javascript:closeDiv('reviewerlocation')" class="btn-close"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="collapsePreviewerLoc" class="panel-collapse collapse in">
                                        <div class="row">
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 colpadding0">
                                                <div class="panel-body-map">
                                                    <div id="map2" style="height:380px;"></div>
                                                </div>
                                            </div>                                    
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div style="text-align:center;padding-top:40px">
                                                    <ul class="bxslider-map" id="ReviewerGraph" style="width: 100%; max-width: none;">
                                               
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!--/.row-->
                                        <div class="clearfix" style="height:10px"></div>
                                  </div>
                                </div>
                                </div>
                        </div>
                         <% } %>
                <% } %>
              //endregion--%>
      </section>

    <div class="modal fade" id="divNotification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <%--style="width: 750px;"--%>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">
                    <h3 style="text-align: center">Notifications</h3>
                    <br />
                    <%-- <div style="margin-bottom: 10px; display: flex; justify-content: center;">
                        <label style="display: block; float: left; font-size: 14px; font-weight: bold; color: #333; ">
                            You have Some Important Notifications, Please Check in Notification Center.</label>
                        
                    </div>--%>
                    <div id="divNotificationData"></div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="modal fade" id="divreports" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1200px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 670px; border-radius: 6px;">

                        <iframe id="showdetails" src="about:blank" width="1150px" height="100%" frameborder="0"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">

                        <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="hiddendiv" style="display: none"></div>
    <script type="text/javascript" src="../Newjs/responsive-calendar1.js"></script>
    <script type="text/javascript">
        var dailyupdateJson;
        var NewsLetterJson;
        function bindnewsdata() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',
                url: '/dailyupdateservice.svc/DailyNewsLetter',
                content: 'application/json;charset=utf-8',
                processData: true,
                success: function (result) {
                    NewsLetterJson = result;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {
                        var objDate = new Date(timeconvert(result[step].NewsDate));
                        str += '<li><img style="width:250px;height:166px" src="../NewsLetterImages/' + result[step].FileName + '" /><h3>' + result[step].Title + '</h3><p><a class="view-pdf" data-toggle="modal" style="cursor:pointer" onclick="viewpdf(this)" data-title="' + result[step].Title + '"  data-href="../NewsLetterImages/' + result[step].DocFileName + '"> Issue ' + getmonths(objDate) + ' ' + objDate.getFullYear() + '</a></p></li>'
                    }
                    $("#Newslettersrow").html(str);
                    var ker = $('.bxnews').bxSlider({
                        minSlides: 3,
                        maxSlides: 3,
                        slideWidth: 250,
                        slideMargin: 50,
                        moveSlides: 1,
                        auto: false,
                    });
                },
                error: function (e, t) { }
            })
        }
        function binddailyupdatedata()
        {
            var data = { Email: '<%=UserInformation%>' };
            var k = 0;
            $.ajax({
                async: true,
                type: 'post',
                //url: "https://cors-anywhere.herokuapp.com/https://api.avantis.co.in/api/v2/legalupdates/params/",
                //url: "https://tunnel2.avantisregtec.in/api/v2/legalupdates/params/",
                url: "https://api.avantis.co.in/api/v2/legalupdates/params/",
                content: 'application/json;charset=utf-8',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Token 04f7a4e33a692196c39ef9ba54c53459c9a9b25b'
                },
                processData: true,
                body: JSON.stringify(data),
                success: function (result) {
                    dailyupdateJson = result.data;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.data.length; step++) {
                        var titledaily = '';
                        var Descriptiondaily = '';
                        if (result.data[step].title.length > 35) {
                            titledaily = result.data[step].title.substring(0, 35) + '...'
                        } else {
                            titledaily = result.data[step].title;
                        }
                        if (result.data[step].description.length > 150) {
                            if (result.data[step].description.indexOf('<div') == -1 || result.data[step].description.indexOf('<span') == -1) {
                                Descriptiondaily = result.data[step].description.substring(0, 150) + '...'
                            }
                        } else {
                            if (result.data[step].description.indexOf('<div') == -1 || result.data[step].description.indexOf('<span') == -1) {
                                Descriptiondaily = result.data[step].description;
                            }
                        }
                        var category = '';
                        if (result.data[step].Categories.length > 12) {
                            category = result.data[step].Categories.substring(0, 12) + '...'
                        } else {
                            category = result.data[step].Categories;
                        }

                        var objDate = new Date(result.data[step].date);
                        var GetDateDetail = getmonths(objDate) + ' ' + objDate.getDate() + ', ' + objDate.getFullYear();
                        
                        str += '<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 c-col mb-2" style="padding-top: 12px;"><div class="box h-100 focusable" style="min-height: 270px!important;max-height:270px!important;"><ul class="tags"><li><a class="a-tag" title=' + result.data[step].Categories + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?cat=' + result.data[step].Categories + '" target="_blank">' + category + '</a></li><li><a class="a-tag" title=' + result.data[step].Types + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?state=' + result.data[step].Types + '" target="_blank">' + result.data[step].Types + '</a></li></ul><li><a href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update" target="_blank"><h4 class="card-text-sm"> ' + titledaily + ' </h4></a><a href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update" target="_blank"><p class="text-black-50">' + GetDateDetail + '</p><p>' + Descriptiondaily + '</p><p></a><br /><a data-toggle="modal" onclick="SettracknewLU();" href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update" target="_blank">Read More</a></p></li></div></div>'
                        //str += '<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 c-col mb-2" style="padding-top: 12px;"><div class="box h-100 focusable" style="min-height: 270px!important;max-height:270px!important;"><ul class="tags"><li><a class="a-tag" title=' + result.data[step].Categories + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?cat=' + result.data[step].Categories + '" target="_blank">' + category + '</a></li><li><a class="a-tag" title=' + result.data[step].Types + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?state=' + result.data[step].Types + '" target="_blank">' + result.data[step].Types + '</a></li></ul><li><h4 class="card-text-sm"> ' + titledaily + ' </h4><p class="text-black-50">' + GetDateDetail + '</p><p>' + Descriptiondaily + '</p><p><br /><a data-toggle="modal" href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update" target="_blank">Read More</a></p></li></div></div>'
                        //str += '<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 c-col mb-2" style="padding-top: 12px;"><div class="box h-100 focusable" style="min-height: 270px!important;max-height:270px!important;"><ul class="tags"><li><a class="a-tag" title=' + result.data[step].Categories + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?cat=' + result.data[step].Categories + '">' + category + '</a></li><li><a class="a-tag" title=' + result.data[step].Types + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?state=' + result.data[step].Types + '">' + result.data[step].Types + '</a></li></ul><li><h4 class="card-text-sm"> ' + titledaily + ' </h4><p class="text-black-50">' + GetDateDetail + '</p><p>' + Descriptiondaily + '</p><p><br /><a data-toggle="modal" href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update">Read More</a></p></li></div></div>'
                                               
                    }
                    $("#DailyUpdatesrow").append(str);
                    var ite = $('.bxdaily').bxSlider({
                        minSlides: 3,
                        maxSlides: 3,
                        slideWidth: 300,
                        slideMargin: 0,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'dailyupdatenext',
                        nextclickFunction: 'fdailyclick()',
                    });
                },
                error: function (e, t) {
                    //alert('false');
                }
            });

            function SettracknewLU() {
                settracknew('Dashboard', 'LegalUpdate', 'Detailedview', '')	
            }

            //var k = 0;
            //$.ajax({
            //    async: true,
            //    type: 'Get',
            //    url: '/dailyupdateservice.svc/DailyUpdate',
            //    content: 'application/json;charset=utf-8',
            //    processData: true,
            //    success: function (result) {
            //        dailyupdateJson = result;
            //        var step = 0;
            //        var str = '';
            //        for (step = 0; step < result.length; step++) {
            //            var titledaily = '';
            //            var Descriptiondaily = '';
            //            if (result[step].Title.length > 150) {
            //                titledaily = result[step].Title.substring(0, 150) + '...'
            //            } else {
            //                titledaily = result[step].Title;
            //            }
            //            if (result[step].Description.length > 150) {
            //                if (result[step].Description.indexOf('<div') == -1 || result[step].Description.indexOf('<span') == -1) {
            //                    Descriptiondaily = result[step].Description.substring(0, result[step].Description) + '...'
            //                }
            //            } else {
            //                if (result[step].Description.indexOf('<div') == -1 || result[step].Description.indexOf('<span') == -1) {
            //                    Descriptiondaily = result[step].Description;
            //                }
            //            }
            //            str += '<li><h3 style="height: 70px;"> ' + titledaily + ' </h3> <p><br />   <a data-toggle="modal" onclick="Binddailyupdatepopup(' + result[step].ID + ');" href="#NewsModal">Read More</a></p></li>'
            //        }
            //        $("#DailyUpdatesrow").html(str);
            //        var ite = $('.bxdaily').bxSlider({
            //            minSlides: 3,
            //            maxSlides: 3,
            //            slideWidth: 250,
            //            slideMargin: 50,
            //            moveSlides: 1,
            //            auto: false,
            //        });
            //    },
            //    error: function (e, t) { }
            //})
        }
        function timeconvert(ds) {
            var D, dtime, T, tz, off,
            dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
            T = parseInt(dobj[0]);
            tz = dobj[1];
            off = dobj[2];
            if (off) {
                off = (parseInt(off.substring(0, 2), 10) * 3600000) +
                (parseInt(off.substring(2), 10) * 60000);
                if (tz == '-') off *= -1;
            }
            else off = 0;
            return new Date(T += off).toUTCString();
        }
        function getmonths(mon) {
            try {
                return mon.toString().split(' ')[1];
            } catch (e) { }
        }
        function Binddailyupdatepopup(DID) {
            for (var i = 0; i < dailyupdateJson.length; i++) {
                if (DID == dailyupdateJson[i].ID) {
                    var objDate = new Date(timeconvert(dailyupdateJson[i].CreatedDate));
                    if (timeconvert(dailyupdateJson[i].CreatedDate) == "Invalid Date") {
                        objDate = new Date(dailyupdateJson[i].CreatedDate);
                    }
                    var mon_I = getmonths(objDate);
                    $("#dailyupdatedate").html('Daily Updates:' + mon_I + ' ' + objDate.getDate() + ', ' + objDate.getFullYear());
                    $("#dailyupdatedateInner").html('Daily Updates:' + mon_I + ' ' + objDate.getDate());
                    $("#contents").html(dailyupdateJson[i].Description);
                    $("#dailytitle").html(dailyupdateJson[i].Title);
                    break;
                }
            }
        }
        function BindNewsLetterpopup(DID) {
            for (var i = 0; i < NewsLetterJson.length; i++) {
                if (DID == NewsLetterJson[i].ID) {
                    $("#newsImg").attr('src', '../Images/' + NewsLetterJson[i].FileName);
                    $("#newsTitle").html(NewsLetterJson[i].Title);
                    $("#newsDesc").html(NewsLetterJson[i].Description);
                    break;
                }
            }
        }

        function openModal() {
         return;
            if (sessionStorage.getItem("Notify") == null) {
                $('#divNotification').modal('show');
               
                settracknew('Dashboard', 'Notification', 'View details, Mark read ', '');
                SetNotifyKey();
                return true;
            }
        }
        function BindNewNotifications() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',
                url: '/dailyupdateservice.svc/BindNotifications?userid=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>',
                content: 'application/json;charset=utf-8',
                processData: true,
                success: function (result) {
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {
                        str += '<li><p><a href="../DailyUpdates/Notification.aspx">' + result[step] + '</a></p></li>'
                    }
                   
                if (sessionStorage.getItem("Notify") == null && result.length>0) {
               	 	$('#divNotification').modal('show');
              	 	SetNotifyKey();
                  	$("#divNotificationData").html(str);
           	  }else if(result.length>0){
			$('#divNotification').modal('hide');
			}
                },
                error: function (e, t) { }
            })
        }
        function SetNotifyKey() {
            sessionStorage.setItem("Notify", "1");
        }
        BindNewNotifications();

     <%-- Change on 6 June 2019 Rahul 
        
          function openModal() {
            if (sessionStorage.getItem("Notify") == null) {
                $('#divNotification').modal('show');
                SetNotifyKey();
                return true;
            }
        }
        function BindNewNotifications() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',
                url: '/dailyupdateservice.svc/BindNotifications?userid=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>',
                content: 'application/json;charset=utf-8',
                processData: true,
                success: function (result) {
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {
                        str += '<li><p><a href="../DailyUpdates/Notification.aspx">' + result[step] + '</a></p></li>'
                    }
                    $("#divNotificationData").html(str);
                },
                error: function (e, t) { }
            })
        }
        function SetNotifyKey() {
            sessionStorage.setItem("Notify", "1");
        }
        BindNewNotifications();--%>
    </script>

    <script type="text/javascript">
        var Isprocessing = true;
        $(document).ready(function () {
           
            $.get("/controls/calendarstandalone.aspx?m=3&type=1", function (data) {
                $(".responsive-calendar").html(data);
                frendercalendar();
            });

            setInterval(setcolor, 1000);
        });

        function fcallcalwithMonth(month, year) {
            var dat = month.split('-');
            var mn = parseInt(dat[1]) - 1;
            var yr = parseInt(dat[0]);
            if (mn - 1 < 0) {
                mn = 12;
                yr = yr - 1;
            }
            Isprocessing = false;
            $('.imgDynamic_cal').show();
            $.get("/controls/calendarstandalone.aspx?m=3&type=1&month=" + mn + "&year=" + yr, function (data) {

                $(".responsive-calendar").html('');
                $(".responsive-calendar").html(data);
                Isprocessing = true;
                frendercalendar();
                $('.imgDynamic_cal').hide();
            });
        }
        function fcallcalwithMonth1(month, year) {
            
            var dat = month.split('-');
            var mn = parseInt(dat[1]) + 1;
            var yr = parseInt(dat[0]);
            if (mn >12) {
                mn = 1;
                yr = yr + 1;
            }
            Isprocessing = false;
            $('.imgDynamic_cal').show();
            $.get("/controls/calendarstandalone.aspx?m=3&type=1&month=" + mn + "&year=" + yr, function (data) {

                $(".responsive-calendar").html('');
                $(".responsive-calendar").html(data);
                Isprocessing = true;
                frendercalendar();
                $('.imgDynamic_cal').hide();
            });

        }
        function setcolor() {
            $('.overdue').closest('div').find('a').css('background-color', '#FF0000');
            $('.complete').closest('div').find('a').css('background-color', '#006500');
            $('.pending').closest('div').find('a').css('background-color', '#00008d');
            $('.delayed').closest('div').find('a').css('background-color', '#ffcd70');

        }
        $(document).ready(function (ClassName) {
            $("a").addClass(ClassName);
        });


        //$(document).ready(function () {
        //    $('#calframe').load(function () {
        //        $('#calframe')
        //            .contents().find("head")
        //              .append($('<link rel="stylesheet" type="text/css" href="../NewCSS/bootstrap-theme.css" rel="stylesheet">'),
        //              $('<link rel="stylesheet" type="text/css" href="../NewCSS/stylenew.css" rel="stylesheet">')
        //        );
        //    });
        //});
        function formatDate(date) {
            var monthNames = [
              "January", "February", "March",
              "April", "May", "June", "July",
              "August", "September", "October",
              "November", "December"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
        function fclosepopcal(dt) {
            $('#divreports').modal('hide');
            fcal(dt)
        }
        function hideloader() {
            $('#imgcaldate').hide();
        }
        function fcal(dt) {
            try {
                settracknew('Dashboard', 'Calendar', 'Date Selection', '');

                var ddata = new Date(dt);
                $('#clsdatel').html('Compliance items for date ' + formatDate(ddata));
            } catch (e) { }
           $('#imgcaldate').show();
           // $('#imgcaldate').hide();
            $('#calframe').attr('src', '/controls/calendardataAPI.aspx?m=3&date=' + dt + '&type=' + 1)
            return;
        }

        function OpenOverViewpup(scheduledonid, instanceid, CType) {
            settracknew('Dashboard', 'Compliace view', 'Perform', '');

            if (CType == 'Statutory' || CType == 'Statutory CheckList' || CType == 'Event Based') {
                $('#divOverView').modal('show');
                $('#OverViews').attr('width', '1150px');
                $('#OverViews').attr('height', '600px');
                $('.modal-dialog').css('width', '1200px');
                $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
            else {
                $('#divOverView').modal('show');
                $('#OverViews').attr('width', '1050px');
                $('#OverViews').attr('height', '600px');
                $('.modal-dialog').css('width', '1100px');
                $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
        }
        function OpenPerrevpopup(scheduledonid, instanceid, Interimdays, CType, RoleID, dtdate, SLType) {

            $('#divreports').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '660px');
            $('.modal-dialog').css('width', '85%');
            $('#showdetails').attr('src', "../Compliances/CalenderPopup.aspx?scheduleonId=" + scheduledonid + "&InstanceId=" + instanceid + "&Interimdays=" + Interimdays + "&CType=" + CType + "&RoleID=" + RoleID + "&dt=" + dtdate + "&SLType=" + SLType);

        }

        //region Rahul Comment on 27 FEB 2020
        //Map code  
        //{
        //    var geocoder, geocoder1;
        //    var map, map2;
        //    var bounds = new google.maps.LatLngBounds();
        //    var bounds1 = new google.maps.LatLngBounds();
        //    function initialize() {
        //        if (document.getElementById("map") != null && document.getElementById("map") != undefined && typeof (locations) !== 'undefined' && locations != null && locations != undefined) {
        //            map = new google.maps.Map(
        //            document.getElementById("map"), {
        //                center: new google.maps.LatLng(37.4419, -122.1419),
        //                zoom: 13,
        //                mapTypeId: google.maps.MapTypeId.ROADMAP
        //            });
        //            geocoder = new google.maps.Geocoder();
        //            if (locations.length > 1) {
        //                for (i = 0; i < locations.length; i++) {
        //                    geocodeAddress(locations, i);
        //                }
        //            } else if (locations.length == 1) {
        //                if (locations[0][1] == '') {
        //                    for (i = 0; i < locations.length; i++) {
        //                        geocodeAddress(locationsInternal, i);
        //                    }
        //                }
        //            }
        //        }
        //        if (document.getElementById("map2") != null && document.getElementById("map2") != undefined && typeof (locations1) !== 'undefined' && locations1 != null && locations1 != undefined) {
        //            map2 = new google.maps.Map(
        //            document.getElementById("map2"), {
        //                center: new google.maps.LatLng(37.4419, -122.1419),
        //                zoom: 13,
        //                mapTypeId: google.maps.MapTypeId.ROADMAP
        //            });

        //            geocoder1 = new google.maps.Geocoder();
        //            for (i = 0; i < locations1.length; i++) {
        //                geocodeAddress1(locations1, i);
        //            }
        //        }
        //    }
        //    google.maps.event.addDomListener(window, "load", initialize);
        //    function geocodeAddress(locations, i) {
        //        var title = locations[i][0];
        //        var address = locations[i][1];
        //        var url = locations[i][2];
        //        geocoder.geocode({
        //            'address': locations[i][1]
        //        },
        //        function (results, status) {
        //            if (status == google.maps.GeocoderStatus.OK) {
        //                var marker = new google.maps.Marker({
        //                    icon: 'https://maps.google.com/mapfiles/ms/icons/blue.png',
        //                    map: map,
        //                    position: results[0].geometry.location,
        //                    title: title,
        //                    animation: google.maps.Animation.DROP,
        //                    address: address,
        //                    url: url
        //                })
        //                bounds.extend(marker.getPosition());
        //                map.fitBounds(bounds);
        //                window.google.maps.event.addListener(marker, 'click', function () {
        //                    $('#PerformerGraph > li').each(function (index) {
        //                        var thechosenone = 'p' + address;
        //                        if ($(this).attr("id") == thechosenone) {
        //                            $("#PerformerGraph").css('-webkit-transform', 'translate3d(-' + $(this).attr("scroll-val") + 'px,0px,0px)');
        //                        }
        //                    });
        //                });
        //            } else {
        //                //alert("geocode of " + address + " failed:" + status);
        //            }
        //        });
        //    }

        //    function geocodeAddress1(locations1, i) {
        //        var title1 = locations1[i][0];
        //        var address1 = locations1[i][1];
        //        var url1 = locations1[i][2];
        //        geocoder1.geocode({
        //            'address': locations1[i][1]
        //        },
        //        function (results1, status1) {
        //            if (status1 == google.maps.GeocoderStatus.OK) {
        //                var marker1 = new google.maps.Marker({
        //                    icon: 'https://maps.google.com/mapfiles/ms/icons/blue.png',
        //                    map: map2,
        //                    position: results1[0].geometry.location,
        //                    title: title1,
        //                    animation: google.maps.Animation.DROP,
        //                    address: address1,
        //                    url: url1
        //                })
        //                window.google.maps.event.addListener(marker1, 'click', function () {
        //                    $('#ReviewerGraph > li').each(function (index) {
        //                        var thechosenone = 'r' + address1;
        //                        if ($(this).attr("id") == thechosenone) {
        //                            $("#ReviewerGraph").css('-webkit-transform', 'translate3d(-' + $(this).attr("scroll-val") + 'px,0px,0px)');
        //                        }
        //                    });
        //                });
        //                bounds1.extend(marker1.getPosition());
        //                map2.fitBounds(bounds1);
        //            } else {
        //                //alert("geocode of " + address1 + " failed:" + status1);
        //            }
        //        });
        //    }

        //    $("#PerformerGraph").css('-webkit-transform', 'translate3d(-340px,0px,0px)');
        //    $("#ReviewerGraph").css('-webkit-transform', 'translate3d(-340px,0px,0px)');
        //    function infoWindow(marker, map, title, address, url) {
        //        google.maps.event.addListener(marker, 'click', function () {
        //            var html = "<div><h3>" + title + "</h3><p>" + address + "<br></div><a href='" + url + "'>View location</a></p></div>";
        //            iw = new google.maps.InfoWindow({
        //                content: html,
        //                maxWidth: 350
        //            });
        //            iw.open(map, marker);
        //        });
        //    }
        //    function createMarker(results) {
        //        var marker = new google.maps.Marker({
        //            icon: 'https://maps.google.com/mapfiles/ms/icons/blue.png',
        //            map: map,
        //            position: results[0].geometry.location,
        //            title: title,
        //            animation: google.maps.Animation.DROP,
        //            address: address,
        //            url: url
        //        })
        //        bounds.extend(marker.getPosition());
        //        map.fitBounds(bounds);
        //        infoWindow(marker, map, title, address, url);
        //        return marker;
        //    }
        //    function infoWindow1(marker1, map1, title1, address1, url1) {
        //        google.maps.event.addListener(marker1, 'click', function () {
        //            var html1 = "<div><h3>" + title1 + "</h3><p>" + address1 + "<br></div><a href='" + url1 + "'>View location</a></p></div>";
        //            iw = new google.maps.InfoWindow({
        //                content: html1,
        //                maxWidth: 350
        //            });
        //            iw.open(map1, marker1);
        //        });
        //    }
        //    function createMarker1(results1) {
        //        var marker1 = new google.maps.Marker({
        //            icon: 'https://maps.google.com/mapfiles/ms/icons/blue.png',
        //            map: map2,
        //            position: results1[0].geometry.location,
        //            title: title1,
        //            animation: google.maps.Animation.DROP,
        //            address: address1,
        //            url: url1
        //        })
        //        bounds.extend(marker1.getPosition());
        //        map.fitBounds(bounds1);
        //        infoWindow(marker1, map2, title1, address1, url1);
        //        return marker1;
        //    }
        //}

        //function fgraph() {
        //    var graph = "";
        //    var scroll = 340;
        //    if (typeof (locations) == 'undefined' && typeof (locationsInternal) !== 'undefined') {
        //        locations = [['Location', '', '0']];
        //    }
        //    if (typeof (locations) !== 'undefined') {
        //        for (var i = 0; i < locations.length; i++) {
        //            if (i > 0)
        //                scroll = scroll + 300;
        //            cities = locations[i][1];
        //            var Internalval = 0;
        //            if (typeof (locationsInternal) !== 'undefined' && locationsInternal != null && locationsInternal != undefined) {
        //                for (var ii = 0; ii < locationsInternal.length; ii++) {
        //                    if (locations[i][1] == locationsInternal[ii][1]) {
        //                        Internalval = locationsInternal[ii][2];
        //                    }
        //                    else if (locations[i][1] == '' && locationsInternal[ii][1] != '') {
        //                        cities = locationsInternal[i][1];
        //                        Internalval = locationsInternal[ii][2];
        //                    }
        //                }
        //            }
        //            if (cities != null) {
        //                var newPerformercities = cities.split(',');

        //            }
        //            if (Internalval == 0 && 1 == 2) {
        //                graph += '<li style="height:260px" id="p' + locations[i][1] + '" scroll-val="' + scroll + '"><input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="' + locations[i][2] + '" data-thickness=".1" data-fgcolor="#008000"><br /><h3>' + locations[i][1] + '</h3></li>'
        //            } else {
        //                graph += '<li   id="p' + locations[i][1] + '" scroll-val="' + scroll + '" style="height: 260px; float: left; list-style: outside none none; position: relative; width: 250px;"  class="clscircle"><div style="width:100%;height:110px"><div style="display:inline;width:100px;height:200px;"><canvas width="100" height="200px"></canvas><input class="knob" data-width="100" data-min="0" data-displayprevious="true" data-readonly="true" value="' + locations[i][2] + '" data-thickness=".1" data-fgcolor="#008000" readonly="readonly" style="width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(255, 116, 115); padding: 0px;"></div></div><div class="graphcmp">Statutory</div><div style="width:100%;height:110px;margin-top: 5%;font-size: 18px;"><div style="display:inline;width:100px;height:200px;" data-toggle="tooltip" data-placement="left" title="% Internal Compliance completion"><canvas width="100" height="200px"></canvas><input class="knob" data-width="100" data-min="0" data-displayprevious="true" data-readonly="true" value="' + Internalval + '" data-thickness=".1" data-fgcolor="#008000" readonly="readonly" style="width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(255, 116, 115); padding: 0px;"></div></div><div class="graphcmp">Internal</div><h3 style="margin-top: 15px;margin-bottom: 10px;margin-left: 97px;font-size: 18px;">' + newPerformercities[0] + '</h3></li>';
        //            }
        //        }
        //        $("#PerformerGraph").html(graph);
        //    }

        //    if (typeof (locations1) == 'undefined' && typeof (locationsInternal1) !== 'undefined') {
        //        var locations1 = [['Location', '', '0']];
        //    }
        //    if (typeof (locations1) !== 'undefined') {
        //        var graph1 = ''; scroll = 340;
        //        for (var i = 0; i < locations1.length; i++) {
        //            if (i > 0)
        //                scroll = scroll + 300;
        //            reviewercities = locations1[i][1];
        //            var Internalval1 = 0;
        //            if (typeof (locationsInternal1) !== 'undefined' && locationsInternal1 != null && locationsInternal1 != undefined) {
        //                for (var ii = 0; ii < locationsInternal1.length; ii++) {
        //                    if (locations1[i][1] == locationsInternal1[ii][1]) {
        //                        Internalval1 = locationsInternal1[ii][2];
        //                    }
        //                    else if (locations1[i][1] == '' && locationsInternal1[ii][1] != '') {
        //                        reviewercities = locationsInternal1[i][1];
        //                        Internalval1 = locationsInternal1[ii][2];
        //                    }
        //                }
        //            }
        //            if (reviewercities != null) {
        //                var newReviewercities = reviewercities.split(',');

        //            }
        //            if (Internalval1 == 0 && 1 == 2) {
        //                graph1 += '<li style="height:260px" id="r' + locations1[i][1] + '" scroll-val="' + scroll + '"><input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="' + locations1[i][2] + '" data-thickness=".1" data-fgcolor="#008000"><br /><h3>' + locations1[i][1] + '</h3></li>';
        //            }
        //            else {
        //                if (typeof (Internalval) == 'undefined') { Internalval = 0; }
        //                graph1 += '<li id="r' + locations1[i][1] + '" scroll-val="' + scroll + '" style="height: 260px; float: left; list-style: outside none none; position: relative; width: 250px; margin-right: 7px!important;" class="clscircle"><div style="width:100%;height:110px" ><div style="display:inline;width:100px;height:200px;"   ><canvas width="100" height="200px" ></canvas><input class="knob" data-width="100" data-min="0" data-displayprevious="true" data-readonly="true" value="' + locations1[i][2] + '" data-thickness=".1" data-fgcolor="#008000" readonly="readonly" style="width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(255, 116, 115); padding: 0px;"></div></div><div class="graphcmp">Statutory</div><div style="width:100%;height:110px;margin-top:5%;font-size: 18px;"  ><div style="display:inline;width:100px;height:200px;"><canvas width="100" height="200px"  ></canvas><input class="knob" data-width="100" data-min="0" data-displayprevious="true" data-readonly="true" value="' + Internalval + '" data-thickness=".1" data-fgcolor="#008000" readonly="readonly" style="width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(255, 116, 115); padding: 0px;"></div></div><div class="graphcmp">Internal</div><h3 style="margin-top: 15px;margin-bottom: 10px;margin-left: 97px;font-size: 18px;">' + newReviewercities[0] + '</h3></li>';
        //            }
        //        }
        //        $("#ReviewerGraph").html(graph1);
        //    }
        //}
        //fgraph();
        //endregion

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
            binddailyupdatedata();
            bindnewsdata();
            $(".knob").knob({
                'change': function (v) { console.log(v); },
                draw: function () {
                    $(this.i).val(this.cv + '%');
                }
            });
        });

    </script>

    <script type="text/javascript">
        /*
        * This is the plugin
        */
        (function (a) { a.createModal = function (b) { defaults = { title: "", message: "Your Message Goes Here!", closeButton: true, scrollable: false }; var b = a.extend({}, defaults, b); var c = (b.scrollable === true) ? 'style="max-height: 420px;overflow-y: auto;"' : ""; html = '<div class="modal fade" id="myModal">'; html += '<div class="modal-dialog" style="width:1000px;">'; html += '<div class="modal-content" style="width:1100px;">'; html += '<div class="modal-header">'; html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'; if (b.title.length > 0) { html += '<h4 class="modal-title">' + b.title + "</h4>" } html += "</div>"; html += '<div class="modal-body" ' + c + ">"; html += b.message; html += "</div>"; html += '<div class="modal-footer">'; if (b.closeButton === true) { html += '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>' } html += "</div>"; html += "</div>"; html += "</div>"; html += "</div>"; a("body").prepend(html); a("#myModal").modal().on("hidden.bs.modal", function () { a(this).remove() }) } })(jQuery);

        /*
        * Here is how you use it
        */

        function viewpdf(obj) {

            var pdf_link = $(obj).attr('data-href');
            var pdf_title = $(obj).attr('data-title');
            var iframe = '<object type="application/pdf" data="' + pdf_link + '"#toolbar=1&navpanes=0&statusbar=0&messages=0 width="100%" height="500">No Support</object>'
            $.createModal({
                title: pdf_title,
                message: iframe,
                closeButton: true,
                scrollable: false
            });
            return false;
        }
        function instructionsDashboard() {
            var enjoyhint_instance = new EnjoyHint({});

            //simple config. 
            //Only one step - highlighting(with description) "New" button 
            //hide EnjoyHint after a click on the button.
            var enjoyhint_script_steps = [
              {
                  "next .count": "Click number to Complete/Review Compliances/tasks",
                  showSkip: true,
              }, {
                  "next #ContentPlaceHolder1_DivPerformerUpcomingInt": "Click number to Complete tasks/subtasks",
                  showSkip: true,
              }, {
                  "next .icon_briefcase": "Click to go to My Workspace",
                  showSkip: true,
              }
              , {
                  "next .icon_datareport": "Click to go to My Reports",
                  showSkip: true,
              }
               , {
                   "next .icon_documents_alt": "Click to go to My Documents",
                   showSkip: true,
               }, {
                   "next .icon_documents_alt": "Click to go to My Reminders",
                   showSkip: true,
               }
               , {
                   "next #ProfilePicTop": "Click to log out menu",
                   showSkip: true,
               }
            ];


            //set script config
            enjoyhint_instance.set(enjoyhint_script_steps);

            //run Enjoyhint script
            enjoyhint_instance.run();
        }

    </script>
   

</asp:Content>
