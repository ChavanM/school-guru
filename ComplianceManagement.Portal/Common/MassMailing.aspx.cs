﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.IO;
using System.Net.Mail;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class MassMailing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRoles();
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxSubject.Text != "")
                {

                    int roleID = -1;

                    if(rdMailOption.SelectedValue.Equals("rolewise"))
                       roleID = Convert.ToInt32(ddlRole.SelectedValue);

                    string  ReplyEmailAddressName = "Avantis";

                    List<Tuple<Stream, string>> attachment = new List<Tuple<Stream, string>>();
                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection[i];
                            attachment.Add(new Tuple<Stream,string>(uploadfile.InputStream,uploadfile.FileName));
                        }

                    }
                    string sub = tbxSubject.Text;
                    string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                     + txtMessagebody.Text.Replace("\n", "<br />") + "<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                    new Thread(() => { ConfigurationManagement.SendMassEmail(sub, message,roleID, attachment); }).Start();
                    
                    ddlRole.SelectedIndex = -1;
                    txtMessagebody.Text = string.Empty;
                    tbxSubject.Text = string.Empty;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please enter your query.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdMailOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdMailOption.SelectedValue.Equals("mass"))
                {
                    divRole.Visible = false;
                    CompareValidator2.Visible = false;
                }
                else
                {
                    divRole.Visible = true;
                    CompareValidator2.Visible = true;
                }
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "initializeRadioButtonsList", "initializeRadioButtonsList();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upMassMailList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "initializeRadioButtonsList", "initializeRadioButtonsList();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void BindRoles()
        {
            try
            {
                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "ID";

                ddlRole.DataSource = RoleManagement.GetAll(false).Where(entry => entry.Code != "EO" && entry.Code != "ER" && entry.Code != "SADMN").ToList();
                ddlRole.DataBind();

                ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ddlRole.SelectedIndex = -1;
                txtMessagebody.Text = string.Empty;
                tbxSubject.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        
    }
}