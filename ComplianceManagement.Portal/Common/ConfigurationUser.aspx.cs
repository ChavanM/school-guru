﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.License;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ConfigurationUser : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        private string RoleId = AuthenticationHelper.Role;
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            divsuccessmsgCusrlst.Visible = false;
            vsUserPopup.CssClass = "alert alert-danger";
            if (!IsPostBack)
            {
                flag = false;
                BindFrequencyDetail();
                BindUsers();
                
                bindPageNumber();
                ShowGridDetail();
            }
        }

        private void BindFrequencyDetail()
        {
            try
            {
                var data = CustomerManagement.FrequencyDetails();
                ddlFrequencyList.DataTextField = "Frequency_Name";
                ddlFrequencyList.DataValueField = "FID";

                ddlFrequencyList.DataSource = data;
                ddlFrequencyList.DataBind();

                ddlFrequencyList.Items.Insert(0, new ListItem("Select Frequency", "-1"));

                ddlFrequencyList1.DataTextField = "Frequency_Name";
                ddlFrequencyList1.DataValueField = "FID";

                ddlFrequencyList1.DataSource = data;
                ddlFrequencyList1.DataBind();

                ddlFrequencyList1.Items.Insert(0, new ListItem("Select Frequency", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }


                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }

                DropDownListPageNo.DataBind();

                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Session["TotalRows"].ToString())) /*&& Session["TotalRows"].ToString() != "0"*/
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        
        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long Frequency = -1;
                string FlagType = string.Empty;
                string ComplianceType = "S";

                if (ddlFrequencyList1.SelectedValue != "-1" && ddlFrequencyList1.SelectedValue != "")
                {
                    Frequency = Convert.ToInt32(ddlFrequencyList1.SelectedValue);
                }
                if (ddlFrequencyType.SelectedValue != "-1" && ddlFrequencyType.SelectedValue != "")
                {
                    FlagType = ddlFrequencyType.SelectedValue;
                }
                if (ddlComplianceType.SelectedValue != "-1" && ddlComplianceType.SelectedValue != "")
                {
                    ComplianceType = ddlComplianceType.SelectedValue;
                }

                var data = ConfigurationManagement.GetAllUserReminderTemplate(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), ComplianceType);
                if (data.Count > 0)
                {
                    if (Frequency != -1)
                    {
                        data = data.Where(x => x.Frequency == Frequency).ToList();
                    }
                    if (!string.IsNullOrEmpty(FlagType))
                    {
                        data = data.Where(x => x.Flag == FlagType).ToList();
                    }
                }

                grdUser.DataSource = data;
                grdUser.DataBind();
                Session["TotalRows"] = data.Count();
                upUserList.Update();
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgCusrlst.Visible = false;
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            ViewState["Mode"] = 0;
            ddlFrequencyList.SelectedValue = "-1";
            tbxTimeInDays.Text = string.Empty;
            txtRepeatDay.Text = string.Empty;
            upUsersPopup.Update();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //Reload the Grid
                BindUsers();

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdUser.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgCusrlst.Visible = false;
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindUsers();
            bindPageNumber();
            ShowGridDetail();
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdUser.PageIndex = chkSelectedPage - 1;
                                
                grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                
                BindUsers(); ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgCusrlst.Visible = false;
                
            }
        }

        protected void ddlCustomerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindUsers(); bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long Frequency = -1;
                string FlagType = string.Empty;
                string ComplianceType = "S";

                if (ddlFrequencyList1.SelectedValue != "-1" && ddlFrequencyList1.SelectedValue != "")
                {
                    Frequency = Convert.ToInt32(ddlFrequencyList1.SelectedValue);
                }
                if (ddlFrequencyType.SelectedValue != "-1" && ddlFrequencyType.SelectedValue != "")
                {
                    FlagType = ddlFrequencyType.SelectedValue;
                }
                if (ddlComplianceType.SelectedValue != "-1" && ddlComplianceType.SelectedValue != "")
                {
                    ComplianceType = ddlComplianceType.SelectedValue;
                }

                var data = ConfigurationManagement.GetAllUserReminderTemplate(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), ComplianceType);
                if (data.Count > 0)
                {
                    if (Frequency != -1)
                    {
                        data = data.Where(x => x.Frequency == Frequency).ToList();
                    }
                    if (!string.IsNullOrEmpty(FlagType))
                    {
                        data = data.Where(x => x.Flag == FlagType).ToList();
                    }
                }

                grdUser.DataSource = data;
                grdUser.DataBind();
                upUserList.Update();
                Session["TotalRows"] = data.Count();
                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}

                //if (ddlCustomerPage.SelectedValue != "-1")
                //{
                //    customerID = Convert.ToInt32(ddlCustomerPage.SelectedValue);
                //}
                //var userList = LicenseMastersManagement.GetAllUser(customerID, tbxFilter.Text);

                //List<object> dataSource = new List<object>();
                //foreach (User user in userList)
                //{
                //    dataSource.Add(new
                //    {
                //        user.ID,
                //        user.FirstName,
                //        user.LastName,
                //        user.Email,
                //        user.ContactNumber,
                //        Role = user.LicenseRoleID != null ? RoleManagement.GetByID(Convert.ToInt32(user.LicenseRoleID)).Name : "",
                //        user.IsActive,
                //    });
                //}
                //string SortExpr = string.Empty;
                //if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                //{
                //    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                //    if (SortExpr == e.SortExpression)
                //    {
                //        if (direction == SortDirection.Ascending)
                //        {
                //            direction = SortDirection.Descending;
                //        }
                //        else
                //        {
                //            direction = SortDirection.Ascending;
                //        }
                //    }
                //    else
                //    {
                //        direction = SortDirection.Ascending;
                //    }
                //}

                //if (direction == SortDirection.Ascending)
                //{
                //    ViewState["Direction"] = "Ascending";
                //    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //}
                //else
                //{
                //    ViewState["Direction"] = "Descending";
                //    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //}

                //ViewState["SortExpression"] = e.SortExpression;

                //foreach (DataControlField field in grdUser.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["SortIndex"] = grdUser.Columns.IndexOf(field);
                //    }
                //}
                //flag = true;
                //grdUser.DataSource = dataSource;
                //Session["TotalRows"] = dataSource.Count();
                //grdUser.DataBind();
                //upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgCusrlst.Visible = false;
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void grdUser_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int typeID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("DELETE"))
                {
                    ConfigurationManagement.DeleteUserFreqDays(typeID, Convert.ToInt32(AuthenticationHelper.UserID));
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenAlert1();", true);
                    BindUsers();
                    bindPageNumber();
                    ShowGridDetail();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenAlert1();", true);
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgCusrlst.Visible = false;
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        #region User Detail--Popup
               

        protected void upUsersPopup_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
               // ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgCusrlst.Visible = false;
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool validation = false;
                if (DropDownList2.SelectedValue == "S" || DropDownList2.SelectedValue == "I")
                {
                    if (ddlFrequencyList.SelectedValue != "-1")
                    {
                        validation = true;
                    }
                    else
                    {
                        validation = false;
                    }
                }
                else
                {
                    validation = true;
                }

                if (validation)
                {
                    ReminderTemplate_UserMapping obj = new ReminderTemplate_UserMapping()
                    {
                        //Frequency = Convert.ToByte(ddlFrequencyList.SelectedValue),
                        TimeInDays = Convert.ToInt16(tbxTimeInDays.Text),
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        UserID = Convert.ToInt32(AuthenticationHelper.UserID),
                        IsActive = true,
                        Flag = DropDownList1.SelectedValue,
                        Type = DropDownList2.SelectedValue,
                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                        CreatedOn = DateTime.Now
                    };
                    if (ddlFrequencyList.SelectedValue != "-1")
                    {
                        obj.Frequency = Convert.ToByte(ddlFrequencyList.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(txtRepeatDay.Text))
                    {
                        obj.RepeatEveryDays = Convert.ToInt32(txtRepeatDay.Text);
                    }
                    if (ConfigurationManagement.ReminderTemplate_UserMappingExist(obj))
                    {
                        cvUserPopup.ErrorMessage = "Frequency already exists.";
                        cvUserPopup.IsValid = false;
                        return;
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        ConfigurationManagement.CreateUserReminder(obj);
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        //ConfigurationManagement.UpdateReminder(obj);
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenAlert();", true);
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ClosePopUser()", true);
                    BindUsers();
                }
                else
                {
                    cvUserPopup.IsValid = false;
                    cvUserPopup.ErrorMessage = "Please select Frequency.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgCusrlst.Visible = false;
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

     
      
        #endregion
        protected void lnkApply_Click(object sender, EventArgs e)
        {
            BindUsers();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void ddlFrequencyList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindUsers();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void ddlFrequencyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindUsers();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindUsers();
            bindPageNumber();
            ShowGridDetail();
        }

        protected void grdUser_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //BindUsers();
            //bindPageNumber();
            //ShowGridDetail();
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList2.SelectedValue != "-1")
            {
                if (DropDownList2.SelectedValue == "S" || DropDownList2.SelectedValue == "I")
                {
                    frequencydisplay.Visible = true;
                }
                else
                {
                    frequencydisplay.Visible = false;
                }
            }
        }
    }
}