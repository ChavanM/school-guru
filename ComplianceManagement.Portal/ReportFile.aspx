﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportFile.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ReportFile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link href="NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

    <style>
        /* Three image containers (use 25% for four, and 50% for two, etc) */
        /* Clear floats after image containers */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .spanDescription {
            text-align: center;
            font-weight: 700;
            font-size: 16px;
        }

        .col-md-4.img-set {
            border-right: 1px solid #aaa;
            padding: 10px;
            text-align: center;
            /*line-height: 100px;*/
            min-height: 159px; /*190*/
        }

        .keychallenge {
            border: 1px solid silver;
            /*border-radius: 10px;
            padding: 10px;*/
            margin-bottom: 24px;
        }

            .keychallenge .title {
                background-color: #1fd9e1;
                text-align: left;
                line-height: 25px;
                padding: 5px;
                border-bottom: 1px solid #aaa;
                margin: 0;
            }

        .col-md-8.span-desc {
            padding: 10px;
        }

        .k-button {
            float: right;
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .headerdiv {
            background-color: #1fd9e1;
            height: 63px;
            padding-top: 9px;
            font-weight: 600;
            font-size: 16px;
            color: white;
        }

        .welcomeheaderdiv {
            float: right;
            padding-top: 9px;
            background-color: #1fd9e1;
            color: white;
            font-size: 14px;
        }

        .profile-ava img {
            border-radius: 50%;
            -webkit-border-radius: 50%;
            display: inline-block;
        }

        .rowheight {
            /*max-height: 144px;*/
        }
    </style>
    <script type="text/javascript">
        function NoDcoumentAlert() {
            alert("No Document available");
            return false;
        }
        function myFunctionShowInternal()
        {
            var x = document.getElementById("divStatutory");
            x.style.display = "none";

            var y = document.getElementById("divInternal");
            y.style.display = "block";

              var z = document.getElementById("lblStatusInternalReport");
            z.style.display = "block";

            var a = document.getElementById("lblStatusStatutoryReport");
            a.style.display = "none";
        }

        function myFunctionShowStatutory()
        {
            var x = document.getElementById("divStatutory");
            x.style.display = "block";

            var y = document.getElementById("divInternal");
            y.style.display = "none";

            var z = document.getElementById("lblStatusInternalReport");
            z.style.display = "none";

            var a = document.getElementById("lblStatusStatutoryReport");
            a.style.display = "block";
        }                
    </script>
    <title>My Report</title>
</head>
<body>
    <form runat="server" class="login-form" name="Report">
        <div>
            <div class="col-md-1 col-sm-1 col-xs-1" style="height: 63px;">
                <img src="../Images/avantil-logo.png" style="height: 60px;">
            </div>
            <div class="col-md-11 col-sm-11 col-xs-11 headerdiv">
                <div style="float: right">
                    <span class="profile-ava">
                        <img src="UserPhotos/DefaultImage.png" id="ProfilePicTop" style="height: 50px; width: 50px;">
                    </span>
                    <span class="username">
                        <asp:Label ID="LabelUserName" runat="server" Style="color: white"></asp:Label></span>
                </div>
            </div>
        </div>
        <div class="row" style="background: #fff; height: 50px;">
            <section class="wrapper ">
                <div class="col-lg-5 col-md-5" id="lblStatusStatutoryReport">
                    <h1 id="pagetype" style="margin-top: 9px; font-size: 24px;">Weekly Statutory Compliance Status Report </h1>
                </div>
                 <div class="col-lg-5 col-md-5" style="display:none;" id="lblStatusInternalReport">
                    <h1 id="pagetype1" style="margin-top: 9px; font-size: 24px;">Weekly Internal Compliance Status Report </h1>
                </div>
                <div class="col-lg-5 col-md-5 colpadding0" style="margin-top: 9px;">
                </div>
                <div class="col-lg-2 col-md-2 colpadding0" style="margin-top: 9px;">
                    <button type="button" class="btn btn-search" style="background-color: #1fd9e1;" onclick="myFunctionShowStatutory()" id="btnStatutory">Statutory</button>
                    <button type="button" class="btn btn-search" style="background-color: #1fd9e1;" onclick="myFunctionShowInternal()" id="btnInternal">Internal</button>              
                </div>
            </section>
        </div>
        <div class="clearfix"></div>
        <%-- <div style="clear:both;height:1px"></div> --%>
        <div class="container" style="padding-top: 15px; max-width: 1344px;">
            <div class="row">

                <div id="divStatutory">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Overall Summary Report</p>
                            <div class="row rowheight" style="max-height: 144px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 144px;">
                                    <asp:ImageButton ID="header1" ImageUrl="../Images/Comprehensive_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>This dynamic report shows the overall summary of the status of your compliances across locations, categories, users, risk profiles, along with details of overdue compliances</p>
                                    <a href="#" id="ComprehensiveReport" onserverclick="ComprehensiveReport_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Location Summary Report</p>
                            <div class="row rowheight" style="max-height: 144px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 144px;">
                                    <asp:ImageButton ID="header4" ImageUrl="../Images/Location_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>View the status of your compliances segregated for each of your locations across categories, risk profiles, and users, along with details of ageing of overdue compliances by location</p>
                                    <a href="#" id="LocationReport" onserverclick="LocationReport_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Risk Summary Report</p>
                            <div class="row rowheight" style="max-height: 159px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 159px;">
                                    <asp:ImageButton ID="header3" ImageUrl="../Images/Risk_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>This dynamic report shows the status of your compliances segregated by risk profiles across locations, categories, and users, along with details of ageing of overdue compliances by risk</p>
                                    <a href="#" id="RiskSummaryReport" onserverclick="RiskSummaryReport_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">User Summary Report</p>
                            <div class="row" style="max-height: 159px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 159px;">
                                    <asp:ImageButton ID="header2" ImageUrl="../Images/User_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>This report shows the status of your compliances managed by each one of your performers and reviewers across categories, risk profiles, and locations, along with details of ageing of overdue compliances</p>
                                    <a href="#" id="UserReport" onserverclick="UserReport_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Category Summary Report</p>
                            <div class="row" style="max-height: 155px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 155px;">
                                    <asp:ImageButton ID="header5" ImageUrl="../Images/Category_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>View the status of your compliances segregated by category across locations, risk profiles, and users, along with details of ageing of overdue compliances by category</p>
                                    <a href="#" id="CategoryReport" onserverclick="CategoryReport_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Detailed Summary Report</p>
                            <div class="row" style="max-height: 155px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 155px;">
                                    <asp:ImageButton ID="header6" ImageUrl="../Images/DetailReport_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>Store a contract forever with keyword tagging for easy search and retrieval with role-based access</p>
                                    <br />
                                    <a href="#" id="DetailedReport" onserverclick="DetailedReport_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                       <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Critical Risk Summary Report</p>
                            <div class="row" style="max-height: 155px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 155px;">
                                    <asp:ImageButton ID="ImageButton7" ImageUrl="../Images/DetailReport_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>With this report view the status of your compliances segregated across locations, categories and users, along with details of ageing of overdue compliances by Critical risk</p>
                                    <br />
                                    <a href="#" id="CriticalReport" onserverclick="CriticalReport_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="divInternal" style="display:none;">
                       <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Overall Summary Report</p>
                            <div class="row rowheight" style="max-height: 144px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 144px;">
                                    <asp:ImageButton ID="ImageButton1" ImageUrl="../Images/Comprehensive_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>This dynamic report shows the overall summary of the status of your compliances across locations, categories, users, risk profiles, along with details of overdue compliances</p>
                                    <a href="#" id="InternalOverall" onserverclick="InternalOverall_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Location Summary Report</p>
                            <div class="row rowheight" style="max-height: 144px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 144px;">
                                    <asp:ImageButton ID="ImageButton2" ImageUrl="../Images/Location_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>View the status of your compliances segregated for each of your locations across categories, risk profiles, and users, along with details of ageing of overdue compliances by location</p>
                                    <a href="#" id="InternalLocation" onserverclick="InternalLocation_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Risk Summary Report</p>
                            <div class="row rowheight" style="max-height: 159px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 159px;">
                                    <asp:ImageButton ID="ImageButton3" ImageUrl="../Images/Risk_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>This dynamic report shows the status of your compliances segregated by risk profiles across locations, categories, and users, along with details of ageing of overdue compliances by risk</p>
                                    <a href="#" id="InternalRisk" onserverclick="InternalRisk_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">User Summary Report</p>
                            <div class="row" style="max-height: 159px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 159px;">
                                    <asp:ImageButton ID="ImageButton4" ImageUrl="../Images/User_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>This report shows the status of your compliances managed by each one of your performers and reviewers across categories, risk profiles, and locations, along with details of ageing of overdue compliances</p>
                                    <a href="#" id="InternalUser" onserverclick="InternalUser_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Category Summary Report</p>
                            <div class="row" style="max-height: 155px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 155px;">
                                    <asp:ImageButton ID="ImageButton5" ImageUrl="../Images/Category_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>View the status of your compliances segregated by category across locations, risk profiles, and users, along with details of ageing of overdue compliances by category</p>
                                    <a href="#" id="InternalCategory" onserverclick="InternalCategory_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Detailed Summary Report</p>
                            <div class="row" style="max-height: 155px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 155px;">
                                    <asp:ImageButton ID="ImageButton6" ImageUrl="../Images/DetailReport_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>Store a contract forever with keyword tagging for easy search and retrieval with role-based access</p>
                                    <br />
                                    <a href="#" id="InternalDetail" onserverclick="InternalDetail_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="keychallenge">
                            <p class="title">Critical Risk Summary Report</p>
                            <div class="row" style="max-height: 155px;">
                                <div class="col-md-4 col-sm-4 col-xs-4 img-set" style="min-height: 155px;">
                                    <asp:ImageButton ID="ImageButton8" ImageUrl="../Images/DetailReport_Report.PNG" runat="server" />
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8 span-desc">
                                    <span class="spanDescription">Description</span>
                                    <p>With this report view the status of your compliances segregated across locations, categories and users, along with details of ageing of overdue compliances by Critical risk</p>
                                    <br />
                                    <a href="#" id="InternalCriticalRiskReport" onserverclick="InternalCriticalRiskReport_ServerClick" runat="server" class="k-button"><span class="k-icon k-i-download"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>


                <div class="col-md-6 col-sm-12 col-xs-12"></div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <asp:LinkButton ID="lbgotoReport" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                        Text="Go to reports" OnClick="lbgotoReport_Click" Style="float: right; font-size: 18px; font-weight: 700; color: #1fd9e1" Font-Underline="true" Visible="false"></asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <asp:TextBox runat="server" ID="UserId" Style="display: none;"></asp:TextBox>
                <asp:TextBox runat="server" ID="DateReportDate" Style="display: none;"></asp:TextBox>
            </div>
        </div>
    </form>
</body>
</html>
