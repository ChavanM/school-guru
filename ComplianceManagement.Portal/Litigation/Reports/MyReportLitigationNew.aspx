﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="MyReportLitigationNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Reports.MyReportLitigationNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    
    <style type="text/css">
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 180px !important;
            overflow-y: auto !important;
        }
        #grid1 .k-grid-content {
            min-height: 380px !important;
             overflow-y: auto !important;
             overflow-x:scroll;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        /*.k-auto-scrollable {
            overflow: hidden;
        }*/

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

       .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

          .k-auto-scrollable k-grid-content{
            overflow-y: auto;
            overflow-x: scroll;
        }


    </style>

     <%--   <%if(NewColumnsLitigation==false){%>
    <style type="text/css">

        .k-grid-content {
            min-height: 180px !important;
            overflow: hidden;
        }
                       
        </style>
     <%}%>--%>
    <title></title>
    <script type="text/javascript">

        setTimeout(printSomething, 1000);

        function printSomething() {
            window.scrollTo(0, document.body.scrollHeight);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            bindFY();
            bindCalendarYear();
            BindStatus();
            BindType();
            if (document.getElementById('CustomerId').value == "89")//
            {
                $("#dropdownType").data("kendoDropDownList").value('2');
                $("#dropdownType1").data("kendoDropDownList").value('2');
            }
            BindLawyers();
            BindPeriod();
            BindCategory();
            BindOpponent();
            BindBranch();
            BindCourt();
            BindWinLoseType();
            BindDepartment();
            bindNoticeType();
            Bindgrid();
            BindgridPopup();


            $(document).on("click", "#grid1 tbody tr .ob-edit1", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                if (item.TypeCaseNotice != '') {
                    $('#divShowDialog1').modal('show');
                    $('#showdetails1').attr('width', '100%');
                    $('#showdetails1').attr('height', '550px');
                    $('.modal-dialog').css('width', '98%');

                    if (item.TypeCaseNotice == 'C') {
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&HistoryFlag=True");
                    }
                    else if (item.TypeCaseNotice == 'N') {
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&HistoryFlag=True");
                    }
                }
                return false;

            });
            $(document).on("click", "#grid1 tbody tr .ob-hearing1", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                if (item.TypeCaseNotice != '') {
                    $('#divShowDialog1').modal('show');
                    $('#showdetails1').attr('width', '100%');
                    $('#showdetails1').attr('height', '550px');
                    $('.modal-dialog').css('width', '98%');

                    if (item.TypeCaseNotice == 'C') {
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&HistoryFlag=True" + '&Flagtab=H&HistoryFlag=True');
                        // $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID);

                    }
                    else if (item.TypeCaseNotice == 'N') {
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&HistoryFlag=True" + '&Flagtab=H&HistoryFlag=True');
                        //    $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID);
                    }
                }
                return true;
            });

            var CourtDropDown = $("#dropdownCourtWise").data("kendoDropDownTree");
            if ($("#dropdownType1").val() == "1") {
                CourtDropDown.wrapper.hide();
            }
            else {
                CourtDropDown.wrapper.show();
            }

            $("#Startdatepicker").kendoDatePicker({
                change: onChange,
                format: "dd/MM/yyyy",
            });
            $("#Lastdatepicker").kendoDatePicker({
                change: onChange,
                format: "dd/MM/yyyy",
            });

        });

        function BindLawyers() {
            $("#dropdownLawyers").kendoDropDownTree({
                placeholder: "Opposition Lawyer",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "LawyerName",
                dataValueField: "LawyertID",
                change: function () {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownLawyers', 'filtersstoryboardLawyer', 'Lawyer');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoLawyerList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                            //read: "<% =Path%>Litigation/KendoLawyerList?CustId=<% =CustId%>"
                    },
                }
            });
        }

        function onChange() {
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != undefined && $("#Startdatepicker").val() != "") {
                $("#dropdownFY1").data("kendoDropDownList").select(0);
                $("#dropdownPastData1").data("kendoDropDownList").select(4);
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != undefined && $("#Lastdatepicker").val() != "") {
                $("#dropdownFY1").data("kendoDropDownList").select(0);
                $("#dropdownPastData1").data("kendoDropDownList").select(4);

            }

        }

        function BindWinLoseType() {
            $("#dropdownWinLose").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "TypeName",
                dataValueField: "ID",
                optionLabel: "Result",
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/TypeList?CutomerID=<% =CustId%>&type=' + $("#dropdownType1").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/TypeList?CutomerID=<% =CustId%>"
                    },
                }
            });
        }

        function bindNoticeType() {
            $("#NoticeType").kendoDropDownTree({
                placeholder: "Type",
                autoClose: true,
                autoWidth: true,
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('NoticeType', 'filtersstoryboardNoticeType', 'Type');
                },
                dataSource: [
                    { text: "Inward/Defendant", value: "I" },//Inward/Defendant
                    { text: "Outward/Plaintiff", value: "O" }//Outward/Plaintiff
                ]
            });
            $("#NoticeType1").kendoDropDownTree({
                placeholder: "Type",
                autoClose: true,
                autoWidth: true,
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('NoticeType1', 'filtersstoryboardNoticeType1', 'Type1');
                },
                dataSource: [
                    { text: "Inward/Defendant", value: "I" },//Inward/Defendant
                    { text: "Outward/Plaintiff", value: "O" }//Outward/Plaintiff
                ]
            });
        }

        function BindDepartment() {
            $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                //dataValueField: "DepartmentID",
                change: function () {
                    FilterAll();
                    fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>"
                    },
                }
            });

            $("#dropdownDept1").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                change: function () {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownDept1', 'filtersstoryboardDept1', 'Dept1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>"
                    },
                }
            });
        }

        function BindBranch() {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                //checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/GetAssignedLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',
                          <%--  url: '<% =Path%>Litigation/GetAssignedEntitiesLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',--%>
                         //url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                //checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdowntree1', 'filtersstoryboard1', 'loc1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                             url: '<% =Path%>Litigation/GetAssignedLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',
                           <%--  url: '<% =Path%>Litigation/GetAssignedEntitiesLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',--%>
                         //url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }

        function BindStatus() {
            $("#dropdownStatus").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Pending/Open", value: "1" },
                    { text: "Disposed/Closed", value: "3" }
                ]
            });

            $("#dropdownStatus1").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //BindgridPopup();
                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Pending/Open", value: "1" },
                    { text: "Disposed/Closed", value: "3" }
                ]
            });
        }

        function BindType() {
            $("#dropdownType").kendoDropDownList({
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownType").val() == "3") {
                        window.location.href = "../Reports/TaskReportNew.aspx";
                    }
                    else {
                        Bindgrid();
                    }
                },
                dataSource: [
                    { text: "Notice", value: "1" },
                    { text: "Case", value: "2" },
                    { text: "Task", value: "3" }
                ]
            });

            $("#dropdownType1").kendoDropDownList({
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    var CourtDropDown = $("#dropdownCourtWise").data("kendoDropDownTree");
                    if ($("#dropdownType1").val() == "1") {
                        CourtDropDown.wrapper.hide();
                    }
                    else {
                        CourtDropDown.wrapper.show();
                    }
                    if ($("#dropdownType1").val() == "3") {
                        window.location.href = "../Reports/TaskReportNew.aspx";
                    }
                    else {
                        BindgridPopup();
                        BindWinLoseType();
                    }
                },
                dataSource: [
                    { text: "Notice", value: "1" },
                    { text: "Case", value: "2" },
                    { text: "Task", value: "3" }
                ]
            });
        }

        function BindOpponent() {
            $("#dropdownOpponent").kendoDropDownTree({
                placeholder: "Opponent",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function () {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownOpponent', 'filtersstoryboardOpponent', 'Opponent');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/LegalCaseParty?CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/LegalCaseParty?CustomerID=<% =CustId%>"
                    },
                }
            });
        }

        function BindCourt() {
            $("#dropdownCourtWise").kendoDropDownTree({
                placeholder: "Court",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "CourtName",
                dataValueField: "ID",
                change: function (e) {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownCourtWise', 'filtersstoryCourtboard', 'Court');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCourtList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCourtList?CustId=<% =CustId%>"
                    }
                }
            });
        }

        function BindCategory() {
            $("#dropdownCategory").kendoDropDownTree({
                placeholder: "Category of Cases",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: false,
                checkAllTemplate: "Select All",
                dataTextField: "CaseType",
                //dataValueField: "ID",
                dataValueField: "CaseType",
                autoClose: false,
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdownCategory', 'filtersstoryCategoryboard', 'Category');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>"
                    }
                }
            });


            $("#dropdownCategory1").kendoDropDownTree({
                placeholder: "Category of Cases",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "CaseType",
                //dataValueField: "ID",
                dataValueField: "CaseType",
                change: function (e) {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownCategory1', 'filtersstoryCategoryboard1', 'Category1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>"
                    }
                }
            });

        }

        function BindPeriod() {

            $("#dropdownPastData1").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    // BindgridPopup();
                    $("#dropdownFY1").data("kendoDropDownList").select(0);
                    $('#Startdatepicker').val('');
                    $('#Lastdatepicker').val('');
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
        }

        function bindCalendarYear() {
            $("#dropdownCalYear").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownCalYear").val() != 0) {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                    }
                },
                dataSource: [
                    { text: "Calendar Year", value: "0" },
                    { text: "2021", value: "2021" },
                    { text: "2020", value: "2020" },
                    { text: "2019", value: "2019" },
                    { text: "2018", value: "2018" },
                    { text: "2017", value: "2017" },
                    { text: "2016", value: "2016" },
                    { text: "2015", value: "2015" },
                    { text: "2014", value: "2014" },
                    { text: "2013", value: "2013" },
                    { text: "2012", value: "2012" }

                ]
            });

            $("#dropdownCalYear1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownCalYear1").val() != 0) {
                        $("#dropdownFY1").data("kendoDropDownList").select(0);
                    }
                },
                dataSource: [
                    { text: "Calendar Year", value: "0" },
                    { text: "2021", value: "2021" },
                    { text: "2020", value: "2020" },
                    { text: "2019", value: "2019" },
                    { text: "2018", value: "2018" },
                    { text: "2017", value: "2017" },
                    { text: "2016", value: "2016" },
                    { text: "2015", value: "2015" },
                    { text: "2014", value: "2014" },
                    { text: "2013", value: "2013" },
                    { text: "2012", value: "2012" }

                ]
            });
        }



        function bindFY() {
            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    $("#dropdownCalYear").data("kendoDropDownList").select(0);
                },
                dataSource: [
                        <%if(FYYear == true) {%>
                        { text: "Disputed Financial Year", value: "0" },
                    <%}%>
                        <%else
                    {%> 
                        { text: "Financial Year", value: "0" },
                        <%}%>
                   
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },
                    { text: "2015-2016", value: "2015-2016" },
                    { text: "2014-2015", value: "2014-2015" },
                    { text: "2013-2014", value: "2013-2014" },
                    { text: "2012-2013", value: "2012-2013" }

                ]
            });

            $("#dropdownFY1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY1").val() != 0) {
                        $("#dropdownPastData1").data("kendoDropDownList").select(4);
                        $('#Startdatepicker').val('');
                        $('#Lastdatepicker').val('');
                        $("#dropdownCalYear1").data("kendoDropDownList").select(0);
                    }

                },
                dataSource: [
                             <%if(FYYear == true) {%>
                        { text: "Disputed Financial Year", value: "0" },
                    <%}%>
                        <%else
                    {%> 
                        { text: "Financial Year", value: "0" },
                        <%}%>
                   
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },
                    { text: "2015-2016", value: "2015-2016" },
                    { text: "2014-2015", value: "2014-2015" },
                    { text: "2013-2014", value: "2013-2014" },
                    { text: "2012-2013", value: "2012-2013" }

                ]
            });
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Location
            }
            if (div == 'filtersstoryboardDept') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:&nbsp');//Department
            }
            if (div == 'filtersstoryboardNoticeType') {
                $('#' + div).append('Party Type &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Notice Type
            }
            if (div == 'filtersstoryCategoryboard') {
                $('#' + div).append('Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Category Type
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
            fCreateStoryBoard('NoticeType', 'filtersstoryboardNoticeType', 'Type');
            fCreateStoryBoard('dropdownCategory', 'filtersstoryCategoryboard', 'Category');
        };

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownTree").value([]);
            $("#NoticeType").data("kendoDropDownTree").value([]);
            $("#dropdownCategory").data("kendoDropDownTree").value([]);
            $("#dropdownFY").data("kendoDropDownList").select(0);
            $("#dropdownCalYear").data("kendoDropDownList").select(0);
            Bindgrid();
            e.preventDefault();
        }

        function fCreateStoryBoard1(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Location
            }
            if (div == 'filtersstoryboardDept1') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:&nbsp');//Department
            }
            if (div == 'filtersstoryboardNoticeType1') {
                $('#' + div).append('Party Type &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Notice Type
            }
            if (div == 'filtersstoryCategoryboard1') {
                $('#' + div).append('Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Category Type
            }
            if (div == 'filtersstoryCourtboard') {
                $('#' + div).append('Court &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Court
            }
            if (div == 'filtersstoryboardOpponent') {
                $('#' + div).append('Opponent &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//opponent
            }
            if (div == 'filtersstoryboardLawyer') {
                $('#' + div).append('Lawyer &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Lawyer
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory1(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory1(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function fcloseStory1(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard1('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard1('dropdownDept1', 'filtersstoryboardDept1', 'Dept1');
            fCreateStoryBoard1('NoticeType1', 'filtersstoryboardNoticeType1', 'Type1');
            fCreateStoryBoard1('dropdownCategory1', 'filtersstoryCategoryboard1', 'Category1');
            fCreateStoryBoard1('dropdownCourtWise', 'filtersstoryCourtboard', 'Court');
            fCreateStoryBoard1('dropdownOpponent', 'filtersstoryboardOpponent', 'Opponent');
            fCreateStoryBoard1('dropdownLawyers', 'filtersstoryboardLawyer', 'Lawyer');
        };

        function ClearAllFilter() {
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownDept1").data("kendoDropDownTree").value([]);
            $("#NoticeType1").data("kendoDropDownTree").value([]);
            $("#dropdownCategory1").data("kendoDropDownTree").value([]);
            $("#dropdownCourtWise").data("kendoDropDownTree").value([]);
            $("#dropdownOpponent").data("kendoDropDownTree").value([]);
            $("#dropdownLawyers").data("kendoDropDownTree").value([]);
            $("#dropdownWinLose").data("kendoDropDownList").select(0);
            $("#dropdownPastData1").data("kendoDropDownList").select(4);
            $("#dropdownFY1").data("kendoDropDownList").select(0);
            $("#dropdownCalYear1").data("kendoDropDownList").select(0);
            document.getElementById('txtSearch').value = '';
            $('#Startdatepicker').val('');
            $('#Lastdatepicker').val('');
            BindgridPopup();
        }

        function FilterAllAdvanced() {
            var locationlist = $("#dropdowntree1").data("kendoDropDownTree")._values;

            //Department details
            var Departmentlist = $("#dropdownDept1").data("kendoDropDownTree")._values;

            var NoticeTypedetails = $("#NoticeType1").data("kendoDropDownTree")._values;

            //Category details
            var Categorylist8 = $("#dropdownCategory1").data("kendoDropDownTree")._values;

            //Court details
            var Courtlist7 = $("#dropdownCourtWise").data("kendoDropDownTree")._values;

            //Oppnent details
            var Oppnentlist4 = $("#dropdownOpponent").data("kendoDropDownTree")._values;

            //Lawyers details
            var Lawyerlist = $("#dropdownLawyers").data("kendoDropDownTree")._values;

            var FilterForAllSearch = document.getElementById('txtSearch').value;

            if (locationlist.length > 0
                || Departmentlist.length > 0
                || NoticeTypedetails.length > 0
                || Categorylist8.length > 0
                || Courtlist7.length > 0
                || Oppnentlist4.length > 0
                || Lawyerlist.length > 0
                || FilterForAllSearch != ""
                || ($("#dropdownFY1").val() != 0 && $("#dropdownFY1").val() != -1 && $("#dropdownFY1").val() != "")
                || ($("#dropdownWinLose").val() != 0 && $("#dropdownWinLose").val() != -1 && $("#dropdownWinLose").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownFY1").val() != 0 && $("#dropdownFY1").val() != -1 && $("#dropdownFY1").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "FYName", operator: "contains", value: ($("#dropdownFY1").val() + ',')
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if ($("#dropdownWinLose").val() != 0 && $("#dropdownWinLose").val() != -1 && $("#dropdownWinLose").val() != "") {
                    var WLFilter = { logic: "or", filters: [] };

                    WLFilter.filters.push({
                        field: "WLResult", operator: "eq", value: $("#dropdownWinLose").val()
                    });

                    finalSelectedfilter.filters.push(WLFilter);
                }

                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if (Departmentlist.length > 0) {
                    var DeptFilter = { logic: "or", filters: [] };

                    $.each(Departmentlist, function (i, v) {
                        DeptFilter.filters.push({
                            field: "DepartmentID", operator: "eq", value: parseInt(v)
                            //field: "DepartmentID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(DeptFilter);
                }

                if (NoticeTypedetails.length > 0) {

                    var TypeFilter = { logic: "or", filters: [] };
                    if ($("#dropdownType1").val() == "2") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Defendant" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Plaintiff" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }
                    if ($("#dropdownType1").val() == "1") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Inward" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Outward" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }

                }

                if (Categorylist8.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Categorylist8, function (i, v) {
                        CategoryFilter.filters.push({
                            field: "Category", operator: "contains", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }

                if (Courtlist7.length > 0) {
                    if ($("#dropdownType1").val() == "2") {
                        var CourtFilter = { logic: "or", filters: [] };

                        $.each(Courtlist7, function (i, v) {
                            CourtFilter.filters.push({
                                field: "CourtID", operator: "eq", value: parseInt(v)
                            });
                        });

                        finalSelectedfilter.filters.push(CourtFilter);
                    }
                }


                if (Oppnentlist4.length > 0) {
                    var OppnentFilter = { logic: "or", filters: [] };

                    $.each(Oppnentlist4, function (i, v) {
                        OppnentFilter.filters.push({
                            field: "PartyID", operator: "Contains", value: v + ','
                        });
                    });
                    finalSelectedfilter.filters.push(OppnentFilter);
                }

                if (Lawyerlist.length > 0) {
                    var lawyerFilter = { logic: "or", filters: [] };

                    $.each(Lawyerlist, function (i, v) {
                        lawyerFilter.filters.push({
                            field: " Opposition", operator: "Contains", value: v + ','
                        });
                    });
                    finalSelectedfilter.filters.push(lawyerFilter);
                }
                if (FilterForAllSearch != "" && FilterForAllSearch != undefined) {
                    var AllFilter = { logic: "or", filters: [] };

                    AllFilter.filters.push({
                        field: "Department", operator: "contains", value: FilterForAllSearch
                    });

                    AllFilter.filters.push({
                        field: "BranchName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "Category", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "NoticeCaseDesc", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "TypeName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "CaseRefNo", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "Title", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "PartyName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "RefNo", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "CaseStage", operator: "contains", value: FilterForAllSearch
                    });
                    finalSelectedfilter.filters.push(AllFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid1").data("kendoGrid").dataSource.filter({});
            }

        }

        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;

            //Department details
            var Departmentlist = $("#dropdownDept").data("kendoDropDownTree")._values;


            var NoticeTypedetails = $("#NoticeType").data("kendoDropDownTree")._values;

            var Categorylist8 = $("#dropdownCategory").data("kendoDropDownTree")._values;

            if (locationlist.length > 0
                || Departmentlist.length > 0
                || NoticeTypedetails.length > 0
                || Categorylist8.length > 0
                || ($("#dropdownFY").val() != 0 && $("#dropdownFY").val() != -1 && $("#dropdownFY").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownFY").val() != 0 && $("#dropdownFY").val() != -1 && $("#dropdownFY").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "FYName", operator: "contains", value: ($("#dropdownFY").val() + ',')
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
                if (Departmentlist.length > 0) {
                    var DeptFilter = { logic: "or", filters: [] };

                    $.each(Departmentlist, function (i, v) {
                        DeptFilter.filters.push({
                            field: "DepartmentID", operator: "eq", value: parseInt(v)
                            //field: "DepartmentID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(DeptFilter);
                }
                if (NoticeTypedetails.length > 0) {

                    var TypeFilter = { logic: "or", filters: [] };
                    if ($("#dropdownType").val() == "2") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Defendant" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Plaintiff" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }
                    if ($("#dropdownType").val() == "1") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Inward" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Outward" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }

                }
                if (Categorylist8.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Categorylist8, function (i, v) {
                        CategoryFilter.filters.push({
                            field: "Category", operator: "contains", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

        var record = 0;
        var caserefno = 0;

        var LitigationType = 0;
        var trueorfalse = false;
        function Bindgrid() {
            if ($("#dropdownType").val() == 1) {
                caserefno = "RefNo";
                LitigationType = "Notice";
                trueorfalse = true;
            }
            else {
                caserefno = "CaseRefNo";
                LitigationType = "Case";
                trueorfalse = false;
            }


            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/MyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=All&StatusFlag=' + $("#dropdownType").val() + '&FY=0&StatusType=' + $("#dropdownStatus").val() + '&CourtId=&CategoryID=&StartDateDetail=&EndDateDetail=&CY=' + $("#dropdownCalYear").val() + '&WLResult=-1&AssingnedLawyer=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/MyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=All&StatusFlag=' + $("#dropdownType").val() + '&FY=0&StatusType=' + $("#dropdownStatus").val() + '&CourtId=&CategoryID=&StartDateDetail=&EndDateDetail=&CY=' + $("#dropdownCalYear").val() + '&WLResult=-1&AssingnedLawyer=-1',
                    },
                    schema: {

                        data: function (response) {
                            if ($("#dropdownType").val() == 1) {
                                return response[0].Notice;
                            }
                            else if ($("#dropdownType").val() == 2) {
                                return response[0].Case;
                            }
                        },
                        total: function (response) {
                            if ($("#dropdownType").val() == 1) {
                                return response[0].Notice.length;
                            }
                            else if ($("#dropdownType").val() == 2) {
                                return response[0].Case.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All',5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "70px",
                        locked: true,
                        lockable: false,
                    },
                    <% if (NewColumnsLitigation==false){%>
                    {
                        field: "BranchName", title: 'Location',
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },width:"140px"
                    },
                    <%}%>
                    <%else if (NewColumnsLitigation==true){%>
                    {
                        field: "CBU", title: 'CBU',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                     {
                         field: "Zone", title: 'Zone',
                         width: "120px;",
                         locked: true,
                         lockable: false,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                      {
                          field: "Region", title: 'Region',
                          width: "120px;",
                          locked: true,
                          lockable: false,
                          type: "string",
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                      {
                          field: "Territory", title: 'Territory',
                          width: "120px;",
                          locked: true,
                          lockable: false,
                          type: "string",
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                   <%}%>
                    {
                        field: "TypeName", title: LitigationType + ' Type',
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "150px"
                    },
                    {
                        field: caserefno, title: LitigationType + ' No.',
                        locked: true,
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "150px"
                    },
                    {
                        field: "Title", title: 'Title',
                        locked: true,
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "280px"
                    },
                    {
                        field: "PartyName", title: 'Opponents',
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                        }, width: "180px"
                    },
                    {
                        field: "CaseStage", title: 'Case Stage',
                        lockable: false,
                        width: "130px;",
                        hidden: trueorfalse,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "NoticeStageName", title: 'Notice Stage',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "Department", title: 'Department',
                        lockable: false,
                        type: "string",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "110px"
                    },
                    {
                        field: "NoticeCaseDesc", title: 'Description',
                        lockable: false,
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "200px"
                    },
                    { field: "Status", title: "Status(Open/Closed)", lockable: false,filterable: { multi: true, search: true }, width: "120px" },
                    { field: "Jurisdiction", title: "Jurisdiction", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    { field: "ClaimAmt", title: "Claim Amount", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    { field: "ProbableAmt", title: "Probable Amount", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    { field: "Provisionalamt", title: "Provisional Amount", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    { field: "ProtestMoney", title: "Protest Money", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    {
                        <%if(FYYear == true) {%>
                        title: "Disputed Financial Year",
                    <%}%>
                        <%else
                    {%> 
                        title: "Financial Year",
                        <%}%>
                        field: "FYName", hidden: true,
                        lockable: false,
                        template: "#if (FYName != null) {# #= FYNameEnd(FYName) # #} else {##}#",
                        filterable: { multi: true, search: true }, width: "120px"
                    },
                { field: "BankGurantee", title: "Bank Guarantee", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                { field: "StateName", title: "State", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    {
                        field: "OpenDate",
                        title: "Open Date",
                        lockable: false,
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                      <% if (IsNoticeDate  == customerID)
        {%>
                      {
                         field: "NoticeDate",
                         title: "Notice Date",
                         lockable: false,
                         hidden: true,
                         width: "100px",
                         type: "date",
                         format: "{0:dd-MM-yyyy}",
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     type: "date",
                                     format: "{0:dd-MM-yyyy}",
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             },
                         }
                      },
                        <%}%>
                    {
                        field: "CloseDate",
                        title: "Close Date",
                        lockable: false,
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                { field: "CNResult", title: "Result", hidden: true, lockable: false, filterable: { multi: true, search: true }, width: "100px" },
              
              <% if (NewColumnsLitigation  == true){%>
                     {
                         field: "CaseCreator", title: 'Case/Notice Creator',
                         hidden: true,
                         type: "string",
                         lockable: false,
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }, width: "160px",
                     },
                       {
                           field: "EmailId", title: 'Email Id',
                           hidden: true,
                           lockable: false,
                           type: "string",
                           attributes: {
                               style: 'white-space: nowrap;'
                           },
                           filterable: {
                               multi: true,
                               extra: false,
                               search: true,
                               operators: {
                                   string: {
                                       eq: "Is equal to",
                                       neq: "Is not equal to",
                                       contains: "Contains"
                                   }
                               }
                           }, width: "140px",
                       },
                        
                        {
                            field: "ContactNumber", title: 'Contact Number',
                            hidden: true,
                            lockable: false,
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }, width: "120px",
                        },
                     <%}%>

                    {
                        command: [
                            { name: "hearing", text: "", iconClass: "k-icon k-i-calendar-date", className: "ob-hearing" },
                            { name: "edit", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit" }

                        ], title: "Action", lock: true, width: 100, headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "" && content != "  ") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                width: "150px",
                content: function (e) {
                    return "View " + LitigationType + " Details";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-hearing",
                width: "150px",
                content: function (e) {
                    if (LitigationType == "Case") {
                        return "Show Hearing Details";
                    }
                    else {
                        return "Show Response Details";
                    }
                }
            });


            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {

                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                if (item.TypeCaseNotice != '') {
                    $('#divShowDialog').modal('show');
                    $('#showdetails').attr('width', '100%');
                    $('#showdetails').attr('height', '550px');
                    $('.modal-dialog').css('width', '100%');

                    if (item.TypeCaseNotice == 'C') {
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&HistoryFlag=True");

                    }
                    else if (item.TypeCaseNotice == 'N') {
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&HistoryFlag=True");
                    }
                }
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-hearing", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                if (item.TypeCaseNotice != '') {
                    $('#divShowDialog').modal('show');
                    $('#showdetails').attr('width', '100%');
                    $('#showdetails').attr('height', '550px');
                    $('.modal-dialog').css('width', '100%');

                    if (item.TypeCaseNotice == 'C') {
                        //                        $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&HistoryFlag=True");
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + '&Flagtab=H&HistoryFlag=True');

                    }
                    else if (item.TypeCaseNotice == 'N') {
                        //    $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&HistoryFlag=True");
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + '&Flagtab=H&HistoryFlag=True');
                    }
                }
                return true;
            });
        }

        function BindgridPopup() {
            if ($("#dropdownType1").val() == 1) {
                caserefno = "RefNo";
                LitigationType = "Notice";
            }
            else {
                caserefno = "CaseRefNo";
                LitigationType = "Case";
            }

            var grid = $('#grid1').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid1').empty();

            var setStartDate = '';
            var setEndDate = '';

            var list4 = $("#dropdownLawyers").data("kendoDropDownTree")._values;
            var Lawyerdetails = [];
            if (list4.length > 0) {
                $.each(list4, function (i, v) {
                    Lawyerdetails.push(v);
                });
            }

            if ($("#Startdatepicker").val() != undefined) {
                //setStartDate = kendo.toString($("#Startdatepicker").val(), "dd/MM/yyyy");
                setStartDate = $("#Startdatepicker").val();
            }
            if ($("#Lastdatepicker").val() != undefined) {
                //setEndDate = kendo.toString($("#Lastdatepicker").val(), "dd/MM/yyyy");
                setEndDate = $("#Lastdatepicker").val();
            }

            var grid = $("#grid1").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/MyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=' + $("#dropdownPastData1").val() + '&StatusFlag=' + $("#dropdownType1").val() + '&FY=0&StatusType=' + $("#dropdownStatus1").val() + '&StartDateDetail=' + setStartDate + '&EndDateDetail=' + setEndDate + '&CY=' + $("#dropdownCalYear1").val() + '&WLResult=-1&AssingnedLawyer=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/MyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=' + $("#dropdownPastData1").val() + '&StatusFlag=' + $("#dropdownType1").val() + '&FY=0&StatusType=' + $("#dropdownStatus1").val() + '&StartDateDetail=' + setStartDate + '&EndDateDetail=' + setEndDate + '&CY=' + $("#dropdownCalYear1").val() + '&WLResult=-1&AssingnedLawyer=-1',
                    },
                    schema: {
                        data: function (response) {
                            if ($("#dropdownType1").val() == 1) {
                                return response[0].Notice;
                            }
                            else if ($("#dropdownType1").val() == 2) {
                                return response[0].Case;
                            }
                        },
                        total: function (response) {
                            if ($("#dropdownType1").val() == 1) {
                                return response[0].Notice.length;
                            }
                            else if ($("#dropdownType1").val() == 2) {
                                return response[0].Case.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "70px",
                        locked: true,
                        lockable: false,
                    },
                    <%if (NewColumnsLitigation == false) {%>
                    {
                        field: "BranchName", title: 'Location',
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },width:"180px"
                    },
                    <%}%>
                    <%else if (NewColumnsLitigation==true){%>
                    {
                        field: "CBU", title: 'CBU',
                        width: "130px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                     },
                     {
                         field: "Zone", title: 'Zone',
                         width: "130px;",
                         locked: true,
                         lockable: false,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                      {
                          field: "Region", title: 'Region',
                          width: "130px;",
                          locked: true,
                          lockable: false,
                          type: "string",
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                      {
                          field: "Territory", title: 'Territory',
                          width: "130px;",
                          locked: true,
                          lockable: false,
                          type: "string",
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                   <%}%>
                    {
                        field: "TypeName", title: LitigationType + ' Type',
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "150px"
                    },
                    {
                        field: caserefno, title: LitigationType + ' No.',
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "180px"
                    },
                    {
                        field: "Title", title: 'Title',
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "320px"
                    },
                    {
                        field: "PartyName", title: 'Opponents',
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "260px"
                    },
                    {
                        field: "CaseStage", title: 'Case Stage',
                        lockable: false,
                        width: "150px;",
                        hidden: trueorfalse,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "NoticeStageName", title: 'Notice Stage',
                        lockable: false,
                        width: "150px;",
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Department", title: 'Department',
                        lockable: false,
                        type: "string",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },width:"150px"
                    },
                    {
                        field: "NoticeCaseDesc", title: 'Description',
                        lockable: false,
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "200px"
                    },
                    { field: "Status", title: "Status(Open/Closed)", lockable: false,filterable: { multi: true, search: true }, width: "130px" },
                    { field: "Jurisdiction", title: "Jurisdiction", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    { field: "ClaimAmt", title: "Claim Amount", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    { field: "ProbableAmt", title: "Probable Amount", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    { field: "Provisionalamt", title: "Provisional Amount", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    { field: "ProtestMoney", title: "Protest Money", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                    {
                           <%if(FYYear == true) {%>
                        title: "Disputed Financial Year",
                    <%}%>
                        <%else
                    {%> 
                        title: "Financial Year",
                        <%}%>
                        field: "FYName", hidden: true,
                        lockable: false,
                        template: "#if (FYName != null) {# #= FYNameEnd(FYName) # #} else {##}#",
                        filterable: { multi: true, search: true }, lockable: false, width: "130px"
                    },
                { field: "BankGurantee", title: "Bank Guarantee", hidden: true, lockable: false, filterable: { multi: true, search: true }, width: "100px" },
                { field: "StateName", title: "State", hidden: true, lockable: false, filterable: { multi: true, search: true }, width: "100px" },
                    {
                        field: "OpenDate",
                        title: "Open Date",
                        lockable: false,
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                      <% if (IsNoticeDate  == customerID)
        {%>
                      {
                          field: "NoticeDate",
                          lockable: false,
                         title: "Notice Date",
                         hidden: true,
                         width: "100px",
                         type: "date",
                         format: "{0:dd-MM-yyyy}",
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     type: "date",
                                     format: "{0:dd-MM-yyyy}",
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             },
                         }
                      },
                        <%}%>
                    {
                        field: "CloseDate",
                        title: "Close Date",
                        lockable: false,
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                    { field: "CNResult", title: "Result", lockable: false, hidden: true, filterable: { multi: true, search: true }, width: "100px" },
                     <% if (NewColumnsLitigation  == true){%>
                     {
                         field: "CaseCreator", title: 'Case/Notice Creator',
                         hidden: true,
                         type: "string",
                         lockable: false,
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }, width: "160px",
                     },
                       {
                           field: "EmailId", title: 'Email Id',
                           hidden: true,
                           lockable: false,
                           type: "string",
                           attributes: {
                               style: 'white-space: nowrap;'
                           },
                           filterable: {
                               multi: true,
                               extra: false,
                               search: true,
                               operators: {
                                   string: {
                                       eq: "Is equal to",
                                       neq: "Is not equal to",
                                       contains: "Contains"
                                   }
                               }
                           }, width: "140px",
                       },
                        
                        {
                            field: "ContactNumber", title: 'Contact Number',
                            hidden: true,
                            lockable: false,
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }, width: "120px",
                        },
                     <%}%>

                    {
                        command: [
                            { name: "hearing1", text: "", iconClass: "k-icon k-i-calendar-date", className: "ob-hearing1" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit1" }
                        ], title: "Action", lock: true, width: 100, headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });

            $("#grid1").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "" && content != "  ") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
       
            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit1",
                width:"150px",
                content: function (e) {
                    return "View " + LitigationType + " Details";
                }
            });
            $("#grid1").kendoTooltip({
                filter: ".k-grid-hearing1",
                width: "150px",
                content: function (e) {
                    if (LitigationType == "Case") {
                        return "Show Hearing Details";
                    }
                    else {
                        return "Show Response Details";
                    }
                }
            });


        }
        function ClosePopNoticeDetialPage() {
            $('#divShowDialog').modal('hide');
            $('#showdetails').attr('src', "../../Common/blank.html");
            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();
        }

        function FYNameEnd(FYName) {
            FYName = FYName.toString();
            FYName = FYName.replace(/,\s*$/, "");
            return FYName;
        }

        $(document).ready(function () {
            setactivemenu('leftreportsmenu');
            fhead('My Reports');
        });

        function OpenAdvanceSearch(e) {
            $("#divAdvanceSearchModel").kendoWindow({
                modal: true,
                width: "97%",
                height: "92%",
                title: "Advanced Search",
                visible: false,
                draggable: true,
                refresh: true,
                pinned: true,
                actions: [
                    "Close"
                ], close: ClearAllFilter

            }).data("kendoWindow").open().center();
            e.preventDefault();
            return false;

        }

        function ApplyBtnMainFilter(e) {
            Bindgrid();
            FilterAll();
            e.preventDefault();
        }

        function ApplyBtnAdvancedFilter(e) {
            BindgridPopup();
            FilterAllAdvanced();
        }
        function OpenMoreReport(e) {
            window.location.href = "../Reports/litigationreports.aspx";
            e.preventDefault();
            return false;
        }

        function exportReportMain(e) {
            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Department details
            var list2 = $("#dropdownDept").data("kendoDropDownTree")._values;
            var Deptdetails = [];
            $.each(list2, function (i, v) {
                Deptdetails.push(v);
            });

            //NoticeType details
            var list3 = $("#NoticeType").data("kendoDropDownTree")._values;
            var NoticeTypedetails = [];
            $.each(list3, function (i, v) {
                NoticeTypedetails.push(v);
            });

            //Category details
            var Categorylist = $("#dropdownCategory").data("kendoDropDownTree")._values;
            var CategoryID = [];
            $.each(Categorylist, function (i, v) {
                CategoryID.push(v);
            });


            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var FlagIsApp = document.getElementById('FlagDetail').value;
            var GetReportCustId = document.getElementById('GetCustomerID').value;

            if (customerId == GetReportCustId) {
                $.ajax({
                    type: "GET",
                    url: '' + PathName + '//LitigationExportReport/GetAllNoticeCaseReport',
                    data: {
                        UserId: UId,
                        CustomerID: customerId,
                        FlagIsApp: FlagIsApp,
                        MonthId: 'All',
                        StatusFlag: $("#dropdownType").val(),
                        FY: $("#dropdownFY").val(),
                        StatusType: $("#dropdownStatus").val(),
                        StartDateDetail: '',
                        EndDateDetail: '',
                        Dept: JSON.stringify(Deptdetails),
                        NoticeType: JSON.stringify(NoticeTypedetails),
                        location: JSON.stringify(locationsdetails),
                        Category: JSON.stringify(CategoryID),
                        CY: $("#dropdownCalYear").val(),
                        CourtID: "-1",
                        Oppnent: "-1",
                        Lawyers: "-1",
                        Result: "-1",
                        AllSearch: ""
                    },
                    success: function (response) {
                        if (response != "Error" && response != "No Record Found" && response != "") {
                            window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                        }
                        if (response == "No Record Found") {
                            alert("No Record Found");
                        }
                    }
                });
            }
            else {
                $.ajax({
                    type: "GET",
                    url: '' + PathName + '//LitigationExportReport/NoticeCaseReport',
                    data: {
                        UserId: UId,
                        CustomerID: customerId,
                        FlagIsApp: FlagIsApp,
                        MonthId: 'All',
                        StatusFlag: $("#dropdownType").val(),
                        FY: $("#dropdownFY").val(),
                        StatusType: $("#dropdownStatus").val(),
                        StartDateDetail: '',
                        EndDateDetail: '',
                        Dept: JSON.stringify(Deptdetails),
                        NoticeType: JSON.stringify(NoticeTypedetails),
                        location: JSON.stringify(locationsdetails),
                        Category: JSON.stringify(CategoryID),
                        CY: $("#dropdownCalYear").val(),
                        CourtID: "-1",
                        Oppnent: "-1",
                        Lawyers: "-1",
                        Result: "-1",
                        AllSearch: ""
                    },
                    success: function (response) {
                        if (response != "Error" && response != "No Record Found" && response != "") {
                            window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                        }
                        if (response == "No Record Found") {
                            alert("No Record Found");
                        }
                    }
                });
            }
            e.preventDefault();
            return false;
        }

        function exportReportAdvanced(e) {
            //location details
            var list1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Department details
            var list2 = $("#dropdownDept1").data("kendoDropDownTree")._values;
            var Deptdetails = [];
            $.each(list2, function (i, v) {
                Deptdetails.push(v);
            });

            //NoticeType details
            var list3 = $("#NoticeType1").data("kendoDropDownTree")._values;
            var NoticeTypedetails = [];
            $.each(list3, function (i, v) {
                NoticeTypedetails.push(v);
            });

            //Category details
            var Categorylist = $("#dropdownCategory1").data("kendoDropDownTree")._values;
            var CategoryID = [];
            $.each(Categorylist, function (i, v) {
                CategoryID.push(v);
            });

            //CourtID details
            var Courtlist = $("#dropdownCourtWise").data("kendoDropDownTree")._values;
            var CourtDetailID = [];
            $.each(Courtlist, function (i, v) {
                CourtDetailID.push(v);
            });

            //Oppnent details
            var list4 = $("#dropdownOpponent").data("kendoDropDownTree")._values;
            var Oppnentdetails = [];
            $.each(list4, function (i, v) {
                Oppnentdetails.push(v);
            });

            //Lawyer details
            var list4 = $("#dropdownLawyers").data("kendoDropDownTree")._values;
            var Lawyerdetails = [];
            $.each(list4, function (i, v) {
                Lawyerdetails.push(v);
            });

            var WlResult = $("#dropdownWinLose").val();
            if (WlResult == "") {
                WlResult = -1;
            }
            else {
                WlResult = $("#dropdownWinLose").val();
            }


            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var FlagIsApp = document.getElementById('FlagDetail').value;
            var FilterForAll = document.getElementById('txtSearch').value;

            var GetReportCustId = document.getElementById('GetCustomerID').value;

            if (customerId == GetReportCustId) {
                $.ajax({
                    type: "GET",
                    url: '' + PathName + '//LitigationExportReport/GetAllNoticeCaseReport',
                    data: {
                        UserId: UId,
                        CustomerID: customerId,
                        FlagIsApp: FlagIsApp,
                        MonthId: $("#dropdownPastData1").val(),
                        StatusFlag: $("#dropdownType1").val(),
                        FY: $("#dropdownFY1").val(),
                        StatusType: $("#dropdownStatus1").val(),
                        StartDateDetail: $("#Startdatepicker").val(),
                        EndDateDetail: $("#Lastdatepicker").val(),
                        Dept: JSON.stringify(Deptdetails),
                        NoticeType: JSON.stringify(NoticeTypedetails),
                        location: JSON.stringify(locationsdetails),
                        Category: JSON.stringify(CategoryID),
                        CY: $("#dropdownCalYear1").val(),
                        CourtID: JSON.stringify(CourtDetailID),
                        Oppnent: JSON.stringify(Oppnentdetails),
                        Lawyers: JSON.stringify(Lawyerdetails),
                        Result: WlResult,
                        AllSearch: FilterForAll,
                    },
                    success: function (response) {
                        if (response != "Error" && response != "No Record Found" && response != "") {
                            window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                        }
                        if (response == "No Record Found") {
                            alert("No Record Found");
                        }
                    }
                });
            }
            else {
                $.ajax({
                    type: "GET",
                    url: '' + PathName + '//LitigationExportReport/NoticeCaseReport',
                    data: {
                        UserId: UId,
                        CustomerID: customerId,
                        FlagIsApp: FlagIsApp,
                        MonthId: $("#dropdownPastData1").val(),
                        StatusFlag: $("#dropdownType1").val(),
                        FY: $("#dropdownFY1").val(),
                        StatusType: $("#dropdownStatus1").val(),
                        StartDateDetail: $("#Startdatepicker").val(),
                        EndDateDetail: $("#Lastdatepicker").val(),
                        Dept: JSON.stringify(Deptdetails),
                        NoticeType: JSON.stringify(NoticeTypedetails),
                        location: JSON.stringify(locationsdetails),
                        Category: JSON.stringify(CategoryID),
                        CY: $("#dropdownCalYear1").val(),
                        CourtID: JSON.stringify(CourtDetailID),
                        Oppnent: JSON.stringify(Oppnentdetails),
                        Lawyers: JSON.stringify(Lawyerdetails),
                        Result: WlResult,
                        AllSearch: FilterForAll,
                    },
                    success: function (response) {
                        if (response != "Error" && response != "No Record Found" && response != "") {
                            window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                        }
                        if (response == "No Record Found") {
                            alert("No Record Found");
                        }
                    }
                });
            }
            e.preventDefault();
            return false;
        }

        function ClosePopNoticeDetialPageAdvance() {
            $('#divShowDialog1').modal('hide');
            $('#showdetails1').attr('src', "../../Common/blank.html");
            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <div class="row">
        <input id="Path" type="hidden" value="<% =Path%>" />
        <input id="CustomerId" type="hidden" value="<% =CustId%>" />
        <input id="UId" type="hidden" value="<% =UId%>" />
        <input id="FlagDetail" type="hidden" value="<% =FlagIsApp%>" />
          <input id="GetCustomerID" type="hidden" value="<% =GetReportCustId%>" />

    </div>

    <div style="margin: 0.4% 0.9% 0; width: 99%;">
        <input id="dropdownType" data-placeholder="Type" style="width: 15%; margin-right: 0.8%;" />
        <input id="dropdownStatus" data-placeholder="Status" style="width: 15%; margin-right: 0.8%;" />
        <input id="dropdownDept" style="width: 15%; margin-right: 0.8%;" />
        <input id="NoticeType" style="width: 15%; margin-right: 0.8%;" />
        <input id="dropdownCategory" style="width: 19%; margin-right: 0.8%;" />
        <button id="MoreReport" style="height: 32px; width: 14.5%;" onclick="OpenMoreReport(event)">More Reports</button>
    </div>

    <div style="margin: 0.4% 0.9% 0; width: 99%;">
        <input id="dropdowntree" style="width: 31.2%; margin-right: 0.8%;" />
        <input id="dropdownFY" style="width: 15%; margin-right: 0.8%;" />
        <input id="dropdownCalYear" style="width: 15%; margin-right: 0.8%;" />
        <button id="AdavanceSearch" style="height: 30px; margin-right: 0.8%;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
        <button id="exportReport"  style="height:30px;" onclick="exportReportMain(event)" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
        <button id="ApplyBtnMain" style="height: 32px; margin-left: 0.8%;" onclick="ApplyBtnMainFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
        <button id="ClearfilterMain" style="height: 32px; margin-left: 0.5%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
    </div>

    <div class="row" style="margin-left: 10px; margin-top: 5px">
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryboard">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryboardDept">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryboardNoticeType">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryCategoryboard">&nbsp;&nbsp;</div>
    </div>

    <div class="row" style="padding-top: 2px;">
        <div id="grid" style="margin-left: 10px; margin-right: 10px;"></div>
    </div>

    <div id="divAdvanceSearchModel" style="padding-top: 3px;display: none;">

        <div style="margin: 0.4% 0.5% 0.5%;">
            <input id="dropdownType1" data-placeholder="Type" style="width: 14.5%; margin-right: 0.8%;" />
            <input id="dropdownStatus1" data-placeholder="Status" style="width: 14.5%; margin-right: 0.8%;" />
            <input id="dropdownWinLose" style="width: 15%; margin-right: 0.8%;" />
            <input id="NoticeType1" style="width: 15%; margin-right: 0.8%;" />
            <input id="dropdownFY1" data-placeholder="Finance Year" style="width: 17.5%; margin-right: 0.8%;" />
            <input id="dropdownPastData1" style="width: 17.5%;" />
        </div>
        <div style="margin: 0.4% 0.5% 0.5%;">
            <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 30%; margin-right: 0.8%;" />
            <input id="dropdownDept1" data-placeholder="Department" style="width: 15%; margin-right: 0.8%;" />
            <input id="dropdownOpponent" style="width: 15%; margin-right: 0.8%;" />
            <input id="dropdownCalYear1" style="width: 14%; margin-right: 0.8%;" />
            <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 10%; margin-right: 0.8%;" />
            <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 10%;" />
        </div>
        <div style="margin: 0.4% 0.5% 5px;">
            <input id="dropdownCourtWise" style="width: 30%; margin-right: 0.8%;" />
            <input id="dropdownLawyers" style="width: 15%; margin-right: 0.8%;" />
            <input id="txtSearch" type="text" style="width: 15%; margin-right: 0.8%;" onkeydown="return (event.keyCode!=13);" class="k-textbox" placeholder="Type to Search " />
            <input id="dropdownCategory1" data-placeholder="Category Of Case" style="width: 14%; margin-right: 0.8%;" />
            <button id="exportAdvanced"  style="height:26px;" onclick="exportReportAdvanced(event)" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
            <button id="ApplyBtnAdvanced" style="height: 26px; width: 6%; margin-left: 3px;" onclick="ApplyBtnAdvancedFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
            <button id="Clearfilter" style="height: 26px; width: 5.7%; margin-left: 2px;" onclick="ClearAllFilter()"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
        </div>

        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryboard1">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryboardDept1">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryboardNoticeType1">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryCategoryboard1">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryCourtboard">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryboardOpponent">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px;font-weight: bold; color: black;" id="filtersstoryboardLawyer">&nbsp;&nbsp;</div>

        <div class="row" style="padding-top: 8px;">
            <div id="grid1" style="margin-left: 7px; margin-right: 7px;"></div>
        </div>
            <div class="modal fade" id="divShowDialog1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                            <button id="btnAddEditcase1" type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopNoticeDetialPageAdvance();">&times;</button>
                        </div>

                        <div class="modal-body" style="background-color: #f7f7f7;">
                            <iframe id="showdetails1" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
    </div>

     <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                      <button id="btnAddEditcase" type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopNoticeDetialPage();">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #f7f7f7;">
                        <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        </div>
</asp:Content>

