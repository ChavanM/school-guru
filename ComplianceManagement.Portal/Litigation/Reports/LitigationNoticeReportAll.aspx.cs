﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Reports
{
    public partial class LitigationNoticeReportAll : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected string IsCaseNoticeReport;
        protected string CustID;
        public bool NewColumnsLitigation;
        public bool FYCustID;
        public bool RiskType;
        protected void Page_Load(object sender, EventArgs e)
        {
            IsCaseNoticeReport = Convert.ToString(ConfigurationManager.AppSettings["IsCaseNoticeReport"]);
            CustID = Convert.ToString(AuthenticationHelper.CustomerID);
            NewColumnsLitigation = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "LitigationNewColumn");
             FYCustID = CaseManagement.CheckForClient(Convert.ToInt32(CustID), "CaseNoticeLabel");
            RiskType = CaseManagement.CheckForClient(Convert.ToInt32(CustID), "RiskType");

            if (!IsPostBack)
            {
                Branchlist.Clear();
                BindCustomerBranches();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            #region Sheet 1   
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                String WithLititgationTax = "WithLititgationTax";
                string PageType = string.Empty;
                string PageName = string.Empty;
                string check = string.Empty;
                string DeskTitile = string.Empty;
                string RespDate = string.Empty;
                string caseStatus = string.Empty;
                int totalColumnOfCaseAndNotice = 0;
                DataTable tableCaseCustomField = new DataTable();
                DataTable tableNoticeCustomField = new DataTable();
                List<string> CaseCustomValueList = new List<string>();
                List<string> NoticeCustomValueList = new List<string>();
                List<ResponseDetailReport> ResponseDetailsObj = new List<ResponseDetailReport>();
                try
                {
                    //if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                    //{
                    //    if (ddlTypePage.SelectedValue != "-1")
                    //    {
                    //        PageType = ddlTypePage.SelectedValue;
                    //    }
                    //}
                    List<int> CaseInstanceIDDynamic = new List<int>();
                    List<int> NoticeInstanceIDDynamic = new List<int>();
                    List<SP_LitigationCaseReportWithCustomParameter_Result> Casetable = new List<SP_LitigationCaseReportWithCustomParameter_Result>();
                    List<sp_LitigationNoticeReportWithCustomParameter_Result> Noticetable = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();
                    List<HearingDetailReport> HearingDataobj = new List<HearingDetailReport>();
                    DataView view1 = new DataView();
                    DataView view2 = new DataView();
                    DataTable table = new DataTable();
                    int P = 0;
                    tableCaseCustomField.Columns.Add("NoticecaseInstanceID", typeof(string));
                    tableCaseCustomField.Columns.Add("ResponseDate", typeof(string));
                    tableCaseCustomField.Columns.Add("Description", typeof(string));
                    tableCaseCustomField.Columns.Add("Remark", typeof(string));
                    tableCaseCustomField.Columns.Add("CreatedByText", typeof(string));

                    #region With Litigation Tax
                    if (WithLititgationTax == "WithLititgationTax")
                    {
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("NoticeCaseInstanceID", typeof(string));
                        table.Columns.Add("Title", typeof(string));
                        table.Columns.Add("OpenDate", typeof(string));
                        table.Columns.Add("CloseDate", typeof(string));
                        table.Columns.Add("Status", typeof(string));
                        table.Columns.Add("Label", typeof(string));
                        table.Columns.Add("LabelValue", typeof(string));
                        table.Columns.Add("Interest", typeof(string));
                        table.Columns.Add("Penalty", typeof(string));
                        table.Columns.Add("ProvisionInBook", typeof(string));
                        table.Columns.Add("Total", typeof(string));
                        table.Columns.Add("SettlementValue", typeof(string));
                        table.Columns.Add("IsAllowed", typeof(string));
                        table.Columns.Add("ResponseDate", typeof(string));
                        table.Columns.Add("Description", typeof(string));
                        table.Columns.Add("Remark", typeof(string));
                        table.Columns.Add("CreatedByText", typeof(string));

                        long NoticeIDOld = 0;
                        if (PageType == "N")
                        {
                            #region Notice
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Noticetable = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                               select row).ToList();


                                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.CustomerBranchID == Convert.ToInt32(tvFilterLocation.SelectedValue)).ToList();
                                }

                                foreach (var item in Noticetable)
                                {
                                    long NoticeID = item.NoticeCaseInstanceID;
                                    if (NoticeID != NoticeIDOld)
                                    {
                                        NoticeIDOld = item.NoticeCaseInstanceID;
                                        List<ResponseDetailReport> HearingData = CaseManagement.GetResponseDetailNoticeWise(item.NoticeCaseInstanceID);

                                        foreach (var itemValues in HearingData)
                                        {
                                            ResponseDetailsObj.Add(itemValues);
                                            tableCaseCustomField.Rows.Add(itemValues.CaseInstanceID, itemValues.ResponseDate, itemValues.Description, itemValues.Remark, itemValues.CreatedByText);
                                        }
                                    }
                                }

                                int beforecondition = 0;
                                for (int i = 0; i < Noticetable.Count; i++)
                                {
                                    if (ResponseDetailsObj.Count > 0)
                                    {
                                        beforecondition = Convert.ToInt32(Noticetable[i].NoticeCaseInstanceID);//1
                                        for (int j = 0; j <= i; j++)
                                        {
                                            if (ResponseDetailsObj.Count > j)
                                            {
                                                if (ResponseDetailsObj.Count == 1)
                                                {
                                                    if (ResponseDetailsObj[0].CaseInstanceID == Noticetable[i].NoticeCaseInstanceID)
                                                    {
                                                        Noticetable[i].Remark = ResponseDetailsObj[j].Remark;
                                                        if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[j].ResponseDate)))
                                                        {
                                                            Noticetable[i].ResponseDate = Convert.ToDateTime(ResponseDetailsObj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                        }
                                                        Noticetable[i].Description = ResponseDetailsObj[j].Description;
                                                        Noticetable[i].CreatedBy = ResponseDetailsObj[j].CreatedByText;
                                                        ResponseDetailsObj.RemoveAt(j);
                                                    }
                                                    break;
                                                }
                                                else
                                                {
                                                    if (ResponseDetailsObj[j].CaseInstanceID == Noticetable[i].NoticeCaseInstanceID)
                                                    {
                                                        Noticetable[i].Remark = ResponseDetailsObj[j].Remark;
                                                        if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[j].ResponseDate)))
                                                        {
                                                            Noticetable[i].ResponseDate = Convert.ToDateTime(ResponseDetailsObj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                        }
                                                        Noticetable[i].Description = ResponseDetailsObj[j].Description;
                                                        Noticetable[i].CreatedBy = ResponseDetailsObj[j].CreatedByText;
                                                        ResponseDetailsObj.RemoveAt(j);
                                                        j = i;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (ResponseDetailsObj.Count > 0)
                                {
                                    for (int k = 0; k < ResponseDetailsObj.Count; k++)
                                    {
                                        long IDRemCheck = Convert.ToInt32(ResponseDetailsObj[k].CaseInstanceID);
                                        string RespDates = string.Empty;
                                        if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[k].ResponseDate)))
                                        {
                                            RespDates = Convert.ToDateTime(ResponseDetailsObj[k].ResponseDate).ToString("dd-MMM-yyyy");
                                        }
                                        Noticetable.Add(new sp_LitigationNoticeReportWithCustomParameter_Result { NoticeCaseInstanceID = IDRemCheck, Remark = ResponseDetailsObj[k].Remark, ResponseDate = RespDates, Description = ResponseDetailsObj[k].Description, CreatedBy = ResponseDetailsObj[k].CreatedByText });
                                    }
                                }


                                Noticetable = Noticetable.OrderBy(entry => entry.NoticeCaseInstanceID).ToList();

                                foreach (var item in Noticetable)
                                {
                                    long NoticeID = item.NoticeCaseInstanceID;
                                    if (NoticeID != NoticeIDOld)
                                    {
                                        ++P;
                                        CaseInstanceIDDynamic.Add(Convert.ToInt32(item.NoticeCaseInstanceID));
                                        NoticeIDOld = item.NoticeCaseInstanceID;
                                        table.Rows.Add(P, item.NoticeCaseInstanceID, item.Title, item.OpenDate, item.CloseDate, item.Status, item.Label, item.LabelValue, item.Interest, item.Penalty, item.ProvisionInBook, item.Total, item.SettlementValue, item.IsAllowed, item.ResponseDate, item.Description, item.Remark, item.CreatedBy);
                                    }
                                    else
                                    {
                                        CaseInstanceIDDynamic.Add(0);
                                        table.Rows.Add("", "", "", "", "", "", item.Label, item.LabelValue, item.Interest, item.Penalty, item.ProvisionInBook, item.Total, item.SettlementValue, item.IsAllowed, item.ResponseDate, item.Description, item.Remark, item.CreatedBy);
                                    }
                                }

                                PageName = "Notice";
                                DeskTitile = "Response";
                                RespDate = "Response Date";
                            }
                            #endregion
                        }

                        if (PageType == "C")
                        {
                            #region Case

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Casetable = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                             select row).ToList();

                                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                                {
                                    Casetable = Casetable.Where(entry => entry.CustomerBranchID == Convert.ToInt32(tvFilterLocation.SelectedValue)).ToList();
                                }

                                foreach (var item in Casetable)
                                {
                                    long NoticeID = item.NoticeCaseInstanceID;
                                    if (NoticeID != NoticeIDOld)
                                    {
                                        NoticeIDOld = item.NoticeCaseInstanceID;
                                        List<HearingDetailReport> HearingData = CaseManagement.GetHearingDetailCaseWise(item.NoticeCaseInstanceID);

                                        foreach (var itemValues in HearingData)
                                        {
                                            HearingDataobj.Add(itemValues);
                                            tableCaseCustomField.Rows.Add(itemValues.CaseInstanceID, itemValues.ResponseDate, itemValues.Description, itemValues.Remark, itemValues.CreatedByText);
                                        }
                                    }
                                }

                                int beforecondition = 0;
                                for (int i = 0; i < Casetable.Count; i++)
                                {
                                    if (HearingDataobj.Count > 0)
                                    {
                                        beforecondition = Convert.ToInt32(Casetable[i].NoticeCaseInstanceID);//1
                                        for (int j = 0; j <= i; j++)
                                        {
                                            if (HearingDataobj.Count > j)
                                            {
                                                if (HearingDataobj.Count == 1)
                                                {
                                                    if (HearingDataobj[0].CaseInstanceID == Casetable[i].NoticeCaseInstanceID)
                                                    {
                                                        Casetable[i].Remark = HearingDataobj[j].Remark;
                                                        if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].ResponseDate)))
                                                        {
                                                            Casetable[i].ResponseDate = Convert.ToDateTime(HearingDataobj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                        }
                                                        Casetable[i].Description = HearingDataobj[j].Description;
                                                        Casetable[i].CreatedBy = HearingDataobj[j].CreatedByText;
                                                        HearingDataobj.RemoveAt(j);
                                                    }
                                                    break;
                                                }
                                                else
                                                {
                                                    if (HearingDataobj[j].CaseInstanceID == Casetable[i].NoticeCaseInstanceID)
                                                    {
                                                        Casetable[i].Remark = HearingDataobj[j].Remark;
                                                        if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].ResponseDate)))
                                                        {
                                                            Casetable[i].ResponseDate = Convert.ToDateTime(HearingDataobj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                        }
                                                        Casetable[i].Description = HearingDataobj[j].Description;
                                                        Casetable[i].CreatedBy = HearingDataobj[j].CreatedByText;
                                                        HearingDataobj.RemoveAt(j);
                                                        j = i;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (HearingDataobj.Count > 0)
                                {
                                    for (int k = 0; k < HearingDataobj.Count; k++)
                                    {
                                        long IDRemCheck = Convert.ToInt32(HearingDataobj[k].CaseInstanceID);
                                        string RespDates = string.Empty;
                                        if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[k].ResponseDate)))
                                        {
                                            RespDates = Convert.ToDateTime(HearingDataobj[k].ResponseDate).ToString("dd-MMM-yyyy");
                                        }
                                        Casetable.Add(new SP_LitigationCaseReportWithCustomParameter_Result { NoticeCaseInstanceID = IDRemCheck, Remark = HearingDataobj[k].Remark, ResponseDate = RespDates, Description = HearingDataobj[k].Description, CreatedBy = HearingDataobj[k].CreatedByText });
                                    }
                                }

                                Casetable = Casetable.OrderBy(entry => entry.NoticeCaseInstanceID).ToList();

                                foreach (var item in Casetable)
                                {
                                    long NoticeID = item.NoticeCaseInstanceID;
                                    if (NoticeID != NoticeIDOld)
                                    {
                                        ++P;
                                        CaseInstanceIDDynamic.Add(Convert.ToInt32(item.NoticeCaseInstanceID));
                                        NoticeIDOld = item.NoticeCaseInstanceID;
                                        table.Rows.Add(P, item.NoticeCaseInstanceID, item.Title, item.OpenDate, item.CloseDate, item.Status, item.Label, item.LabelValue, item.Interest, item.Penalty, item.ProvisionInBook, item.Total, item.SettlementValue, item.IsAllowed, item.ResponseDate, item.Description, item.Remark, item.CreatedBy);
                                    }
                                    else
                                    {
                                        CaseInstanceIDDynamic.Add(0);
                                        table.Rows.Add("", "", "", "", "", "", item.Label, item.LabelValue, item.Interest, item.Penalty, item.ProvisionInBook, item.Total, item.SettlementValue, item.IsAllowed, item.ResponseDate, item.Description, item.Remark, item.CreatedBy);
                                    }
                                }
                            }
                            PageName = "Case";
                            DeskTitile = "Hearing";
                            RespDate = "Case Date";
                            #endregion
                        }

                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add(PageName + " Report");


                        #region First Sheet
                        if (PageName == "Notice" || PageName == "Case")
                        {
                            view1 = new System.Data.DataView(table);

                            DataTable ExcelData = null;
                            ExcelData = view1.ToTable("Selected", false, "SrNo", "Title", "OpenDate", "CloseDate", "Status", "Label", "LabelValue", "Interest", "Penalty", "ProvisionInBook", "Total", "SettlementValue", "IsAllowed", "ResponseDate", "Description", "Remark", "CreatedByText");

                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["OpenDate"])))
                                {
                                    item["OpenDate"] = Convert.ToDateTime(item["OpenDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["CloseDate"])))
                                {
                                    item["CloseDate"] = Convert.ToDateTime(item["CloseDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
                                {
                                    item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            exWorkSheet.Cells["A1"].Value = "SrNo";
                            exWorkSheet.Cells["A1"].AutoFitColumns(8);
                            exWorkSheet.Cells["A1:A2"].Merge = true;
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["A1"].Style.WrapText = true;

                            exWorkSheet.Cells["B1"].Value = PageName + " Title";
                            exWorkSheet.Cells["B1"].AutoFitColumns(20);
                            exWorkSheet.Cells["B1:B2"].Merge = true;
                            exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["B1"].Style.WrapText = true;

                            exWorkSheet.Cells["C1"].Value = "Open Date";
                            exWorkSheet.Cells["C1"].AutoFitColumns(15);
                            exWorkSheet.Cells["C1:C2"].Merge = true;
                            exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C1:C2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C1:C2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["C1"].Style.WrapText = true;

                            exWorkSheet.Cells["D1"].Value = "Close Date";
                            exWorkSheet.Cells["D1"].AutoFitColumns(15);
                            exWorkSheet.Cells["D1:D2"].Merge = true;
                            exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D1:D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D1:D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["D1"].Style.WrapText = true;

                            exWorkSheet.Cells["E1"].Value = "Status";
                            exWorkSheet.Cells["E1"].AutoFitColumns(15);
                            exWorkSheet.Cells["E1:E2"].Merge = true;
                            exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E1:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E1:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["E1"].Style.WrapText = true;

                            exWorkSheet.Cells["F1"].Value = "Additional Tracking Parameter(s)";
                            exWorkSheet.Cells["F1:M1"].Merge = true;
                            exWorkSheet.Cells["F1"].AutoFitColumns(100);
                            exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F1:M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["F1"].Style.WrapText = true;

                            exWorkSheet.Cells["F2"].Value = "Ground Of Appeal";
                            exWorkSheet.Cells["F2"].AutoFitColumns(15);
                            exWorkSheet.Cells["F2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["F2"].Style.WrapText = true;

                            exWorkSheet.Cells["G2"].Value = "Tax demand";
                            exWorkSheet.Cells["G2"].AutoFitColumns(35);
                            exWorkSheet.Cells["G2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["G2"].Style.WrapText = true;

                            exWorkSheet.Cells["H2"].Value = "Interest";
                            exWorkSheet.Cells["H2"].AutoFitColumns(25);
                            exWorkSheet.Cells["H2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["H2"].Style.WrapText = true;

                            exWorkSheet.Cells["I2"].Value = "Penalty";
                            exWorkSheet.Cells["I2"].AutoFitColumns(25);
                            exWorkSheet.Cells["I2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["I2"].Style.WrapText = true;

                            exWorkSheet.Cells["J2"].Value = "Provision In Book";
                            exWorkSheet.Cells["J2"].AutoFitColumns(25);
                            exWorkSheet.Cells["J2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["J2"].Style.WrapText = true;

                            exWorkSheet.Cells["K2"].Value = "Total";
                            exWorkSheet.Cells["K2"].AutoFitColumns(25);
                            exWorkSheet.Cells["K2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["K2"].Style.WrapText = true;

                            exWorkSheet.Cells["L2"].Value = "Settlement Value";
                            exWorkSheet.Cells["L2"].AutoFitColumns(25);
                            exWorkSheet.Cells["L2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["L2"].Style.WrapText = true;

                            exWorkSheet.Cells["M2"].Value = "Result";
                            exWorkSheet.Cells["M2"].AutoFitColumns(25);
                            exWorkSheet.Cells["M2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["M2"].Style.WrapText = true;

                            exWorkSheet.Cells["N1"].Value = "Hearing Detail";
                            exWorkSheet.Cells["N1:Q1"].Merge = true;
                            exWorkSheet.Cells["N1"].AutoFitColumns(100);
                            exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N1:Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["N1"].Style.WrapText = true;

                            exWorkSheet.Cells["N2"].Value = "Response Date";
                            exWorkSheet.Cells["N2"].AutoFitColumns(25);
                            exWorkSheet.Cells["N2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["N2"].Style.WrapText = true;

                            exWorkSheet.Cells["O2"].Value = "Description";
                            exWorkSheet.Cells["O2"].AutoFitColumns(25);
                            exWorkSheet.Cells["O2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["O2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["O2"].Style.WrapText = true;

                            exWorkSheet.Cells["P2"].Value = "Remark";
                            exWorkSheet.Cells["P2"].AutoFitColumns(25);
                            exWorkSheet.Cells["P2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["P2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["P2"].Style.WrapText = true;

                            exWorkSheet.Cells["Q2"].Value = "Created By";
                            exWorkSheet.Cells["Q2"].AutoFitColumns(25);
                            exWorkSheet.Cells["Q2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["Q2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["Q2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["Q2"].Style.WrapText = true;

                            #region Start Dynamic Column of Notice


                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A3"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion

                            string ActulValue = string.Empty;
                            string NewValue = string.Empty;
                            int j = 3;
                            int k = 3; ;
                            for (int i = 3; i <= 2 + ExcelData.Rows.Count; i++)
                            {

                                string cOpenDate = "c" + i;
                                exWorkSheet.Cells[cOpenDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CloseDate = "D" + i;
                                exWorkSheet.Cells[CloseDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CStatus = "E" + i;
                                exWorkSheet.Cells[CStatus].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string cResponceDate = "F" + i;
                                exWorkSheet.Cells[cResponceDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                string chke = "B" + i;
                                string Checkcell = exWorkSheet.Cells[chke].Value.ToString();
                                if (i > 3)
                                {
                                    string cOpenDate1 = "c" + i;
                                    exWorkSheet.Cells[cOpenDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string CloseDate1 = "D" + i;
                                    exWorkSheet.Cells[CloseDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string CStatus1 = "E" + i;
                                    exWorkSheet.Cells[CStatus1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string cResponceDate1 = "F" + i;
                                    exWorkSheet.Cells[cResponceDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                    if (string.IsNullOrEmpty(Checkcell))
                                    {
                                        j++;
                                    }
                                    else
                                    {
                                        string checknow4 = "A" + k + ":A" + j;
                                        string chekcnow = "B" + k + ":B" + j;
                                        string chekcnow1 = "C" + k + ":C" + j;
                                        string chekcnow2 = "D" + k + ":D" + j;
                                        string chekcnow3 = "E" + k + ":E" + j;
                                        exWorkSheet.Cells[checknow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow].Merge = true;
                                        exWorkSheet.Cells[chekcnow1].Merge = true;
                                        exWorkSheet.Cells[chekcnow2].Merge = true;
                                        exWorkSheet.Cells[chekcnow3].Merge = true;
                                        k = i;
                                        j = i;
                                    }
                                    if (j == (2 + ExcelData.Rows.Count))
                                    {
                                        string checknow4 = "A" + k + ":A" + j;
                                        string chekcnow = "B" + k + ":B" + j;
                                        string chekcnow1 = "C" + k + ":C" + j;
                                        string chekcnow2 = "D" + k + ":D" + j;
                                        string chekcnow3 = "E" + k + ":E" + j;
                                        exWorkSheet.Cells[checknow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow].Merge = true;
                                        exWorkSheet.Cells[chekcnow1].Merge = true;
                                        exWorkSheet.Cells[chekcnow2].Merge = true;
                                        exWorkSheet.Cells[chekcnow3].Merge = true;
                                    }
                                }
                            }

                            using (ExcelRange col = exWorkSheet.Cells[1, 1, 2 + ExcelData.Rows.Count, 17 + totalColumnOfCaseAndNotice])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region Without Litigation Tax
                    if (WithLititgationTax == "WithoutLititgationTax")
                    {
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("NoticeCaseInstanceID", typeof(string));
                        table.Columns.Add("Title", typeof(string));
                        table.Columns.Add("OpenDate", typeof(string));
                        table.Columns.Add("CloseDate", typeof(string));
                        table.Columns.Add("Status", typeof(string));
                        table.Columns.Add("Label", typeof(string));
                        table.Columns.Add("LabelValue", typeof(string));
                        table.Columns.Add("ResponseDate", typeof(string));
                        table.Columns.Add("Description", typeof(string));
                        table.Columns.Add("Remark", typeof(string));
                        table.Columns.Add("CreatedByText", typeof(string));

                        long NoticeIDOld = 0;

                        #region Case

                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            Casetable = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                         select row).ToList();

                            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                            {
                                Casetable = Casetable.Where(entry => entry.CustomerBranchID == Convert.ToInt32(tvFilterLocation.SelectedValue)).ToList();
                            }

                            foreach (var item in Casetable)
                            {
                                long NoticeID = item.NoticeCaseInstanceID;
                                if (NoticeID != NoticeIDOld)
                                {
                                    NoticeIDOld = item.NoticeCaseInstanceID;
                                    List<HearingDetailReport> HearingData = CaseManagement.GetHearingDetailCaseWise(item.NoticeCaseInstanceID);

                                    foreach (var itemValues in HearingData)
                                    {
                                        HearingDataobj.Add(itemValues);
                                        tableCaseCustomField.Rows.Add(itemValues.CaseInstanceID, itemValues.ResponseDate, itemValues.Description, itemValues.Remark, itemValues.CreatedByText);
                                    }
                                }
                            }

                            int beforecondition = 0;
                            for (int i = 0; i < Casetable.Count; i++)
                            {
                                if (HearingDataobj.Count > 0)
                                {
                                    beforecondition = Convert.ToInt32(Casetable[i].NoticeCaseInstanceID);//1
                                    for (int j = 0; j <= i; j++)
                                    {
                                        if (HearingDataobj.Count > j)
                                        {
                                            if (HearingDataobj.Count == 1)
                                            {
                                                if (HearingDataobj[0].CaseInstanceID == Casetable[i].NoticeCaseInstanceID)
                                                {
                                                    Casetable[i].Remark = HearingDataobj[j].Remark;
                                                    if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].ResponseDate)))
                                                    {
                                                        Casetable[i].ResponseDate = Convert.ToDateTime(HearingDataobj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                    }
                                                    Casetable[i].Description = HearingDataobj[j].Description;
                                                    Casetable[i].CreatedBy = HearingDataobj[j].CreatedByText;
                                                    HearingDataobj.RemoveAt(j);
                                                }
                                                break;
                                            }
                                            else
                                            {
                                                if (HearingDataobj[j].CaseInstanceID == Casetable[i].NoticeCaseInstanceID)
                                                {
                                                    Casetable[i].Remark = HearingDataobj[j].Remark;
                                                    if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].ResponseDate)))
                                                    {
                                                        Casetable[i].ResponseDate = Convert.ToDateTime(HearingDataobj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                    }
                                                    Casetable[i].Description = HearingDataobj[j].Description;
                                                    Casetable[i].CreatedBy = HearingDataobj[j].CreatedByText;
                                                    HearingDataobj.RemoveAt(j);
                                                    j = i;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (HearingDataobj.Count > 0)
                            {
                                for (int k = 0; k < HearingDataobj.Count; k++)
                                {
                                    long IDRemCheck = Convert.ToInt32(HearingDataobj[k].CaseInstanceID);
                                    string RespDates = string.Empty;
                                    if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[k].ResponseDate)))
                                    {
                                        RespDates = Convert.ToDateTime(HearingDataobj[k].ResponseDate).ToString("dd-MMM-yyyy");
                                    }
                                    Casetable.Add(new SP_LitigationCaseReportWithCustomParameter_Result { NoticeCaseInstanceID = IDRemCheck, Remark = HearingDataobj[k].Remark, ResponseDate = RespDates, Description = HearingDataobj[k].Description, CreatedBy = HearingDataobj[k].CreatedByText });
                                }
                            }

                            Casetable = Casetable.OrderBy(entry => entry.NoticeCaseInstanceID).ToList();

                            foreach (var item in Casetable)
                            {
                                long NoticeID = item.NoticeCaseInstanceID;
                                if (NoticeID != NoticeIDOld)
                                {
                                    ++P;
                                    CaseInstanceIDDynamic.Add(Convert.ToInt32(item.NoticeCaseInstanceID));
                                    NoticeIDOld = item.NoticeCaseInstanceID;
                                    table.Rows.Add(P, item.NoticeCaseInstanceID, item.Title, item.OpenDate, item.CloseDate, item.Status, item.Label, item.LabelValue, item.ResponseDate, item.Description, item.Remark, item.CreatedBy);
                                }
                                else
                                {
                                    CaseInstanceIDDynamic.Add(0);
                                    table.Rows.Add("", "", "", "", "", "", item.Label, item.LabelValue, item.ResponseDate, item.Description, item.Remark, item.CreatedBy);
                                }
                            }
                        }
                        PageName = "Case";
                        DeskTitile = "Hearing";
                        RespDate = "Case Date";
                        #endregion


                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add(PageName + " Report");


                        #region First Sheet
                        if (PageName == "Notice" || PageName == "Case")
                        {
                            view1 = new System.Data.DataView(table);

                            DataTable ExcelData = null;
                            ExcelData = view1.ToTable("Selected", false, "SrNo", "Title", "OpenDate", "CloseDate", "Status", "Label", "LabelValue", "ResponseDate", "Description", "Remark", "CreatedByText");

                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["OpenDate"])))
                                {
                                    item["OpenDate"] = Convert.ToDateTime(item["OpenDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["CloseDate"])))
                                {
                                    item["CloseDate"] = Convert.ToDateTime(item["CloseDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
                                {
                                    item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            exWorkSheet.Cells["A1"].Value = "SrNo";
                            exWorkSheet.Cells["A1"].AutoFitColumns(8);
                            exWorkSheet.Cells["A1:A2"].Merge = true;
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["A1"].Style.WrapText = true;

                            exWorkSheet.Cells["B1"].Value = PageName + " Title";
                            exWorkSheet.Cells["B1"].AutoFitColumns(20);
                            exWorkSheet.Cells["B1:B2"].Merge = true;
                            exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["B1"].Style.WrapText = true;

                            exWorkSheet.Cells["C1"].Value = "Open Date";
                            exWorkSheet.Cells["C1"].AutoFitColumns(15);
                            exWorkSheet.Cells["C1:C2"].Merge = true;
                            exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C1:C2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["C1:C2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["C1"].Style.WrapText = true;

                            exWorkSheet.Cells["D1"].Value = "Close Date";
                            exWorkSheet.Cells["D1"].AutoFitColumns(15);
                            exWorkSheet.Cells["D1:D2"].Merge = true;
                            exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D1:D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["D1:D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["D1"].Style.WrapText = true;

                            exWorkSheet.Cells["E1"].Value = "Status";
                            exWorkSheet.Cells["E1"].AutoFitColumns(15);
                            exWorkSheet.Cells["E1:E2"].Merge = true;
                            exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E1:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["E1:E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["E1"].Style.WrapText = true;

                            exWorkSheet.Cells["F1"].Value = "Additional Tracking Parameter(s)";
                            exWorkSheet.Cells["F1:G1"].Merge = true;
                            exWorkSheet.Cells["F1"].AutoFitColumns(100);
                            exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F1:G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["F1"].Style.WrapText = true;

                            exWorkSheet.Cells["F2"].Value = "Name";
                            exWorkSheet.Cells["F2"].AutoFitColumns(15);
                            exWorkSheet.Cells["F2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["F2"].Style.WrapText = true;

                            exWorkSheet.Cells["G2"].Value = "Value";
                            exWorkSheet.Cells["G2"].AutoFitColumns(35);
                            exWorkSheet.Cells["G2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["G2"].Style.WrapText = true;

                            exWorkSheet.Cells["H1"].Value = "Hearing Detail";
                            exWorkSheet.Cells["H1:K1"].Merge = true;
                            exWorkSheet.Cells["H1"].AutoFitColumns(100);
                            exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H1:K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["H1"].Style.WrapText = true;

                            exWorkSheet.Cells["H2"].Value = "Response Date";
                            exWorkSheet.Cells["H2"].AutoFitColumns(25);
                            exWorkSheet.Cells["H2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["H2"].Style.WrapText = true;

                            exWorkSheet.Cells["I2"].Value = "Description";
                            exWorkSheet.Cells["I2"].AutoFitColumns(25);
                            exWorkSheet.Cells["I2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["I2"].Style.WrapText = true;

                            exWorkSheet.Cells["J2"].Value = "Remark";
                            exWorkSheet.Cells["J2"].AutoFitColumns(25);
                            exWorkSheet.Cells["J2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["J2"].Style.WrapText = true;

                            exWorkSheet.Cells["K2"].Value = "Created By";
                            exWorkSheet.Cells["K2"].AutoFitColumns(25);
                            exWorkSheet.Cells["K2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["K2"].Style.WrapText = true;

                            #region Start Dynamic Column of Notice


                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A3"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion

                            string ActulValue = string.Empty;
                            string NewValue = string.Empty;
                            int j = 3;
                            int k = 3; ;
                            for (int i = 3; i <= 2 + ExcelData.Rows.Count; i++)
                            {

                                string cOpenDate = "c" + i;
                                exWorkSheet.Cells[cOpenDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CloseDate = "D" + i;
                                exWorkSheet.Cells[CloseDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CStatus = "E" + i;
                                exWorkSheet.Cells[CStatus].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string cResponceDate = "F" + i;
                                exWorkSheet.Cells[cResponceDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                string chke = "B" + i;
                                string Checkcell = exWorkSheet.Cells[chke].Value.ToString();
                                if (i > 3)
                                {
                                    string cOpenDate1 = "c" + i;
                                    exWorkSheet.Cells[cOpenDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string CloseDate1 = "D" + i;
                                    exWorkSheet.Cells[CloseDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string CStatus1 = "E" + i;
                                    exWorkSheet.Cells[CStatus1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string cResponceDate1 = "F" + i;
                                    exWorkSheet.Cells[cResponceDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                    if (string.IsNullOrEmpty(Checkcell))
                                    {
                                        j++;
                                    }
                                    else
                                    {
                                        string checknow4 = "A" + k + ":A" + j;
                                        string chekcnow = "B" + k + ":B" + j;
                                        string chekcnow1 = "C" + k + ":C" + j;
                                        string chekcnow2 = "D" + k + ":D" + j;
                                        string chekcnow3 = "E" + k + ":E" + j;
                                        exWorkSheet.Cells[checknow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow].Merge = true;
                                        exWorkSheet.Cells[chekcnow1].Merge = true;
                                        exWorkSheet.Cells[chekcnow2].Merge = true;
                                        exWorkSheet.Cells[chekcnow3].Merge = true;
                                        k = i;
                                        j = i;
                                    }
                                    if (j == (2 + ExcelData.Rows.Count))
                                    {
                                        string checknow4 = "A" + k + ":A" + j;
                                        string chekcnow = "B" + k + ":B" + j;
                                        string chekcnow1 = "C" + k + ":C" + j;
                                        string chekcnow2 = "D" + k + ":D" + j;
                                        string chekcnow3 = "E" + k + ":E" + j;
                                        exWorkSheet.Cells[checknow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow].Merge = true;
                                        exWorkSheet.Cells[chekcnow1].Merge = true;
                                        exWorkSheet.Cells[chekcnow2].Merge = true;
                                        exWorkSheet.Cells[chekcnow3].Merge = true;
                                    }
                                }
                            }

                            using (ExcelRange col = exWorkSheet.Cells[1, 1, 2 + ExcelData.Rows.Count, 11])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                        }
                        #endregion
                    }
                    #endregion

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LitigationReport.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }


                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            #endregion
        }

        //Mis Report filter brach and Open date

        #region New Mis 
        //Mis Report filter brach and Open date
        //protected void btnMis_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string check = string.Empty;
        //        string DeskTitile = string.Empty;
        //        string RespDate = string.Empty;
        //        string caseStatus = string.Empty;
        //        String WithLititgationTax = "WithLititgationTax";
        //        DataTable tableCaseCustomField = new DataTable();
        //        List<string> CaseCustomValueList = new List<string>();

        //        int P = 0;
        //        long NoticeIDOld = 0;
        //        tableCaseCustomField.Columns.Add("NoticecaseInstanceID", typeof(string));
        //        tableCaseCustomField.Columns.Add("ResponseDate", typeof(string));
        //        tableCaseCustomField.Columns.Add("Description", typeof(string));
        //        tableCaseCustomField.Columns.Add("Remark", typeof(string));
        //        tableCaseCustomField.Columns.Add("NexthearingDate", typeof(string));
        //        //sp_GetMisReports
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            #region MIS
        //            using (ExcelPackage exportPackge = new ExcelPackage())
        //            {
        //                try
        //                {
        //                    #region Without Lititgation Tax
        //                    {
        //                        entities.Database.CommandTimeout = 180;
        //                        var MisReport = (entities.sp_GetMisReports(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

        //                        if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
        //                        {
        //                            MisReport = MisReport.Where(entry => entry.Name == tvFilterLocation.SelectedNode.Text).ToList();
        //                        }
        //                        if (!string.IsNullOrEmpty(txtFromDate.Text))
        //                        {

        //                            MisReport = MisReport.Where(entry => entry.OpenDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
        //                        }
        //                        if (!string.IsNullOrEmpty(txtToDate.Text))
        //                        {
        //                            MisReport = MisReport.Where(entry => entry.OpenDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
        //                        }

        //                        List<HearingDetailReport> HearingDataobj = new List<HearingDetailReport>();
        //                        DataTable table = new DataTable();
        //                        table.Columns.Add("SLNO", typeof(string));
        //                        table.Columns.Add("CaseRefNo", typeof(string));
        //                        table.Columns.Add("CaseTitle", typeof(string));
        //                        table.Columns.Add("CaseType", typeof(string));
        //                        table.Columns.Add("OpenDate", typeof(string));
        //                        table.Columns.Add("CaseType1", typeof(string));
        //                        table.Columns.Add("Party", typeof(string));
        //                        table.Columns.Add("CourtName", typeof(string));
        //                        table.Columns.Add("Judge", typeof(string));
        //                        table.Columns.Add("CaseDetailDesc", typeof(string));
        //                        table.Columns.Add("Name", typeof(string));
        //                        table.Columns.Add("ClaimAmt", typeof(string));
        //                        table.Columns.Add("ExpenseTillDate", typeof(string));
        //                        table.Columns.Add("Owner", typeof(string));
        //                        table.Columns.Add("Lawfirm", typeof(string));
        //                        table.Columns.Add("Label", typeof(string));
        //                        table.Columns.Add("LabelValue", typeof(string));
        //                        table.Columns.Add("ResponseDate", typeof(string));
        //                        table.Columns.Add("Description", typeof(string));
        //                        table.Columns.Add("Remark", typeof(string));
        //                        table.Columns.Add("NextHearingDate", typeof(string));
        //                        table.Columns.Add("CreatedByText", typeof(string));

        //                        //Filter for Tax Litigation Category
        //                        MisReport = MisReport.Where(entry => entry.CaseCategoryID != 47).ToList();

        //                        foreach (var item in MisReport)
        //                        {
        //                            long NoticeID = item.CaseInstanceID;
        //                            if (NoticeID != NoticeIDOld)
        //                            {
        //                                NoticeIDOld = item.CaseInstanceID;
        //                                List<HearingDetailReport> HearingData = CaseManagement.GetHearingDetailCaseWise(item.CaseInstanceID);

        //                                foreach (var itemValues in HearingData)
        //                                {
        //                                    HearingDataobj.Add(itemValues);
        //                                    tableCaseCustomField.Rows.Add(itemValues.CaseInstanceID, itemValues.ResponseDate, itemValues.Description, itemValues.Remark, itemValues.CreatedByText);
        //                                }
        //                            }
        //                        }

        //                        int beforecondition = 0;
        //                        for (int i = 0; i < MisReport.Count; i++)
        //                        {
        //                            if (HearingDataobj.Count > 0)
        //                            {
        //                                beforecondition = Convert.ToInt32(MisReport[i].CaseInstanceID);//1
        //                                for (int j = 0; j <= i; j++)
        //                                {
        //                                    if (HearingDataobj.Count > j)
        //                                    {
        //                                        if (HearingDataobj.Count == 1)
        //                                        {
        //                                            if (HearingDataobj[0].CaseInstanceID == MisReport[i].CaseInstanceID)
        //                                            {
        //                                                MisReport[i].Remark = HearingDataobj[j].Remark;
        //                                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].ResponseDate)))
        //                                                {
        //                                                    MisReport[i].ResponseDate = Convert.ToDateTime(HearingDataobj[j].ResponseDate).ToString("dd-MMM-yyyy");
        //                                                }
        //                                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].NextHearingDate)))
        //                                                {
        //                                                    MisReport[i].NextHearingDate = Convert.ToDateTime(HearingDataobj[j].NextHearingDate).ToString("dd-MMM-yyyy");
        //                                                }
        //                                                MisReport[i].Description = HearingDataobj[j].Description;
        //                                                MisReport[i].CreatedBy = HearingDataobj[j].CreatedByText;
        //                                                HearingDataobj.RemoveAt(j);
        //                                            }
        //                                            break;
        //                                        }
        //                                        else
        //                                        {
        //                                            if (HearingDataobj[j].CaseInstanceID == MisReport[i].CaseInstanceID)
        //                                            {
        //                                                MisReport[i].Remark = HearingDataobj[j].Remark;
        //                                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].ResponseDate)))
        //                                                {
        //                                                    MisReport[i].ResponseDate = Convert.ToDateTime(HearingDataobj[j].ResponseDate).ToString("dd-MMM-yyyy");
        //                                                }
        //                                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].NextHearingDate)))
        //                                                {
        //                                                    MisReport[i].NextHearingDate = Convert.ToDateTime(HearingDataobj[j].NextHearingDate).ToString("dd-MMM-yyyy");
        //                                                }
        //                                                MisReport[i].Description = HearingDataobj[j].Description;
        //                                                MisReport[i].CreatedBy = HearingDataobj[j].CreatedByText;
        //                                                HearingDataobj.RemoveAt(j);
        //                                                j = i;
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        if (HearingDataobj.Count > 0)
        //                        {
        //                            for (int k = 0; k < HearingDataobj.Count; k++)
        //                            {
        //                                long IDRemCheck = Convert.ToInt32(HearingDataobj[k].CaseInstanceID);
        //                                string RespDates = string.Empty;
        //                                string NextHDate = string.Empty;
        //                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[k].ResponseDate)))
        //                                {
        //                                    RespDates = Convert.ToDateTime(HearingDataobj[k].ResponseDate).ToString("dd-MMM-yyyy");
        //                                }
        //                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[k].NextHearingDate)))
        //                                {
        //                                    NextHDate = Convert.ToDateTime(HearingDataobj[k].NextHearingDate).ToString("dd-MMM-yyyy");
        //                                }
        //                                MisReport.Add(new sp_GetMisReports_Result { CaseInstanceID = IDRemCheck, Remark = HearingDataobj[k].Remark, ResponseDate = RespDates, Description = HearingDataobj[k].Description, NextHearingDate = NextHDate, CreatedBy = HearingDataobj[k].CreatedByText });
        //                            }
        //                        }


        //                        MisReport = MisReport.OrderBy(entry => entry.CaseInstanceID).ToList();
        //                        long CheckIDOld = 0;
        //                        foreach (var item in MisReport)
        //                        {
        //                            long NoticeID = item.CaseInstanceID;
        //                            if (NoticeID != CheckIDOld)
        //                            {
        //                                ++P;
        //                                CheckIDOld = item.CaseInstanceID;
        //                                table.Rows.Add(P, item.CaseRefNo, item.CaseTitle, item.CaseType, item.OpenDate, item.CaseType1, item.Party, item.CourtName, item.Judge, item.CaseDetailDesc, item.Name, item.ClaimAmt, item.ExpenseTillDate, item.Owner, item.Lawfirm, item.Label, item.LabelValue, item.ResponseDate, item.Description, item.Remark, item.NextHearingDate, item.CreatedBy);
        //                            }
        //                            else
        //                            {
        //                                table.Rows.Add("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", item.Label, item.LabelValue, item.ResponseDate, item.Description, item.Remark, item.NextHearingDate, item.CreatedBy);
        //                            }
        //                        }


        //                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MIS");
        //                        DataView view = new System.Data.DataView(table);
        //                        DataTable ExcelData = null;

        //                        ExcelData = view.ToTable("Selected", false, "SLNO", "CaseRefNo", "CaseTitle", "CaseType", "OpenDate", "CaseType1", "Party", "CourtName", "Judge", "CaseDetailDesc", "Name", "ClaimAmt", "ExpenseTillDate", "Owner", "Lawfirm", "Label", "LabelValue", "ResponseDate", "Description", "Remark", "NextHearingDate", "CreatedByText");

        //                        foreach (DataRow item in ExcelData.Rows)
        //                        {
        //                            if (!string.IsNullOrEmpty(Convert.ToString(item["OpenDate"])))
        //                            {
        //                                var Opendate = Convert.ToDateTime(item["OpenDate"]).ToString("dd-MMM-yyyy");
        //                                item["OpenDate"] = Opendate;
        //                            }
        //                            if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
        //                            {
        //                                item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
        //                            }
        //                            if (!string.IsNullOrEmpty(Convert.ToString(item["NextHearingDate"])))
        //                            {
        //                                item["NextHearingDate"] = Convert.ToDateTime(item["NextHearingDate"]).ToString("dd-MMM-yyyy");
        //                            }
        //                        }


        //                        exWorkSheet1.Cells["A1"].Value = "SL NO";
        //                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["A1:A2"].Merge = true;
        //                        exWorkSheet1.Cells["A1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["B1"].Value = "Case No";
        //                        exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["B1:B2"].Merge = true;
        //                        exWorkSheet1.Cells["B1"].AutoFitColumns(15);
        //                        exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["C1"].Value = "Case Title";
        //                        exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["C1"].AutoFitColumns(20);
        //                        exWorkSheet1.Cells["C1:C2"].Merge = true;
        //                        exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["D1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["D1:D2"].Merge = true;
        //                        exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["D1"].Value = "Type";
        //                        exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["E1"].Value = "Open Date";
        //                        exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["E1"].AutoFitColumns(15);
        //                        exWorkSheet1.Cells["E1:E2"].Merge = true;
        //                        exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["F1"].Value = "Type of Case";
        //                        exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["F1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["F1:F2"].Merge = true;
        //                        exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["G1"].Value = "Party";
        //                        exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["G1"].AutoFitColumns(14);
        //                        exWorkSheet1.Cells["G1:G2"].Merge = true;
        //                        exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["H1"].Value = "Court Name";
        //                        exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["H1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["H1:H2"].Merge = true;
        //                        exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["I1"].Value = "Judge";
        //                        exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["I1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["I1:I2"].Merge = true;
        //                        exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["J1"].Value = "Case Detail Desc";
        //                        exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["J1"].AutoFitColumns(40);
        //                        exWorkSheet1.Cells["J1:J2"].Merge = true;
        //                        exWorkSheet1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["K1"].Value = "Entity/Location/Branch";
        //                        exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["K1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["K1:K2"].Merge = true;
        //                        exWorkSheet1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["L1"].Value = "Claim Amt";
        //                        exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["L1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["L1:L2"].Merge = true;
        //                        exWorkSheet1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["M1"].Value = "Expense Till Date";
        //                        exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["M1:M2"].Merge = true;
        //                        exWorkSheet1.Cells["M1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["N1"].Value = "Owner";
        //                        exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["N1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["N1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["N1:N2"].Merge = true;
        //                        exWorkSheet1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["O1"].Value = "Law Firm";
        //                        exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["O1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["O1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["O1:O2"].Merge = true;
        //                        exWorkSheet1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["P1"].Value = "Additional Tracking Parameter(s)";
        //                        exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["P1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["P1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["P1:Q1"].Merge = true;
        //                        exWorkSheet1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["P2"].Value = "Name";
        //                        exWorkSheet1.Cells["P2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["P2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["P2"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["P2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["P2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["P2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["P2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["Q2"].Value = "Value";
        //                        exWorkSheet1.Cells["Q2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["Q2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["Q2"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["Q2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["Q2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["Q2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["Q2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["R1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["R1"].Value = "Hearing Detail";
        //                        exWorkSheet1.Cells["R1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["R1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["R1:V1"].Merge = true;
        //                        exWorkSheet1.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["R1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["R1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["R2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["R2"].Value = "Response Date";
        //                        exWorkSheet1.Cells["R2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["R2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["R2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["R2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["R2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["R2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["S2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["S2"].Value = "Description";
        //                        exWorkSheet1.Cells["S2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["S2"].AutoFitColumns(40);
        //                        exWorkSheet1.Cells["S2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["S2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["S2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["S2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["T2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["T2"].Value = "Remark";
        //                        exWorkSheet1.Cells["T2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["T2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["T2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["T2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["T2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["T2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["U2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["U2"].Value = "Next Hearing Date";
        //                        exWorkSheet1.Cells["U2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["U2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["U2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["U2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["U2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["U2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["V2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["V2"].Value = "Created By";
        //                        exWorkSheet1.Cells["V2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["V2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["V2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["V2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["V2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["V2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        if (ExcelData.Rows.Count > 0)
        //                        {
        //                            exWorkSheet1.Cells["A3"].LoadFromDataTable(ExcelData, false);
        //                        }

        //                        #region Merging
        //                        string ActulValue = string.Empty;
        //                        string NewValue = string.Empty;
        //                        int RowValj = 3;
        //                        int RowVal = 3; ;
        //                        for (int i = 3; i <= 2 + ExcelData.Rows.Count; i++)
        //                        {

        //                            string cOpenDate = "c" + i;
        //                            exWorkSheet1.Cells[cOpenDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                            string CloseDate = "D" + i;
        //                            exWorkSheet1.Cells[CloseDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                            string CStatus = "E" + i;
        //                            exWorkSheet1.Cells[CStatus].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                            string cResponceDate = "F" + i;
        //                            exWorkSheet1.Cells[cResponceDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


        //                            string chke = "B" + i;
        //                            string Checkcell = exWorkSheet1.Cells[chke].Value.ToString();
        //                            if (i > 3)
        //                            {
        //                                string cOpenDate1 = "c" + i;
        //                                exWorkSheet1.Cells[cOpenDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                                string CloseDate1 = "D" + i;
        //                                exWorkSheet1.Cells[CloseDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                                string CStatus1 = "E" + i;
        //                                exWorkSheet1.Cells[CStatus1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                                string cResponceDate1 = "F" + i;
        //                                exWorkSheet1.Cells[cResponceDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                                if (string.IsNullOrEmpty(Checkcell))
        //                                {
        //                                    RowValj++;
        //                                }
        //                                else
        //                                {
        //                                    string checknow4 = "A" + RowVal + ":A" + RowValj;
        //                                    string chekcnow = "B" + RowVal + ":B" + RowValj;
        //                                    string chekcnow1 = "C" + RowVal + ":C" + RowValj;
        //                                    string chekcnow2 = "D" + RowVal + ":D" + RowValj;
        //                                    string chekcnow3 = "E" + RowVal + ":E" + RowValj;

        //                                    string chekcnow5 = "F" + RowVal + ":F" + RowValj;
        //                                    string chekcnow6 = "G" + RowVal + ":G" + RowValj;
        //                                    string chekcnow7 = "H" + RowVal + ":H" + RowValj;
        //                                    string chekcnow8 = "I" + RowVal + ":I" + RowValj;
        //                                    string chekcnow9 = "J" + RowVal + ":J" + RowValj;
        //                                    string chekcnow10 = "K" + RowVal + ":K" + RowValj;
        //                                    string chekcnow11 = "L" + RowVal + ":L" + RowValj;
        //                                    string chekcnow12 = "M" + RowVal + ":M" + RowValj;
        //                                    string chekcnow13 = "N" + RowVal + ":N" + RowValj;
        //                                    string chekcnow14 = "O" + RowVal + ":O" + RowValj;

        //                                    exWorkSheet1.Cells[checknow4].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow1].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow2].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow3].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow5].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow6].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow7].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow8].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow9].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow10].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow11].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow12].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow13].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow14].Merge = true;

        //                                    RowVal = i;
        //                                    RowValj = i;
        //                                }
        //                                if (RowValj == (2 + ExcelData.Rows.Count))
        //                                {
        //                                    string checknow4 = "A" + RowVal + ":A" + RowValj;
        //                                    string chekcnow = "B" + RowVal + ":B" + RowValj;
        //                                    string chekcnow1 = "C" + RowVal + ":C" + RowValj;
        //                                    string chekcnow2 = "D" + RowVal + ":D" + RowValj;
        //                                    string chekcnow3 = "E" + RowVal + ":E" + RowValj;
        //                                    string chekcnow5 = "F" + RowVal + ":F" + RowValj;
        //                                    string chekcnow6 = "G" + RowVal + ":G" + RowValj;
        //                                    string chekcnow7 = "H" + RowVal + ":H" + RowValj;
        //                                    string chekcnow8 = "I" + RowVal + ":I" + RowValj;
        //                                    string chekcnow9 = "J" + RowVal + ":J" + RowValj;
        //                                    string chekcnow10 = "K" + RowVal + ":K" + RowValj;
        //                                    string chekcnow11 = "L" + RowVal + ":L" + RowValj;
        //                                    string chekcnow12 = "M" + RowVal + ":M" + RowValj;
        //                                    string chekcnow13 = "N" + RowVal + ":N" + RowValj;
        //                                    string chekcnow14 = "O" + RowVal + ":O" + RowValj;

        //                                    exWorkSheet1.Cells[checknow4].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow1].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow2].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow3].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow5].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow6].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow7].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow8].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow9].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow10].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow11].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow12].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow13].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow14].Merge = true;
        //                                }
        //                            }
        //                        }

        //                        #endregion End Merging

        //                        int count = Convert.ToInt32(ExcelData.Rows.Count) + 2;
        //                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 22])
        //                        {
        //                            col.Style.WrapText = true;
        //                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
        //                        }
        //                    }
        //                    #endregion

        //                    #region With Lititgation Tax
        //                    if (WithLititgationTax == "WithLititgationTax")
        //                    {
        //                        entities.Database.CommandTimeout = 180;
        //                        var MisReport = (entities.sp_GetMisReports(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

        //                        if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
        //                        {
        //                            MisReport = MisReport.Where(entry => entry.Name == tvFilterLocation.SelectedNode.Text).ToList();
        //                        }
        //                        //HearingDate
        //                        if (!string.IsNullOrEmpty(txtFromDate.Text))
        //                        {
        //                            MisReport = MisReport.Where(entry => entry.OpenDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
        //                        }
        //                        if (!string.IsNullOrEmpty(txtToDate.Text))
        //                        {
        //                            MisReport = MisReport.Where(entry => entry.OpenDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
        //                        }

        //                        List<HearingDetailReport> HearingDataobj = new List<HearingDetailReport>();
        //                        DataTable table = new DataTable();
        //                        table.Columns.Add("SLNO", typeof(string));
        //                        table.Columns.Add("CaseRefNo", typeof(string));
        //                        table.Columns.Add("CaseTitle", typeof(string));
        //                        table.Columns.Add("CaseType", typeof(string));
        //                        table.Columns.Add("OpenDate", typeof(string));
        //                        table.Columns.Add("CaseType1", typeof(string));
        //                        table.Columns.Add("Party", typeof(string));
        //                        table.Columns.Add("CourtName", typeof(string));
        //                        table.Columns.Add("Judge", typeof(string));
        //                        table.Columns.Add("CaseDetailDesc", typeof(string));
        //                        table.Columns.Add("Name", typeof(string));
        //                        table.Columns.Add("ClaimAmt", typeof(string));
        //                        table.Columns.Add("ExpenseTillDate", typeof(string));
        //                        table.Columns.Add("Owner", typeof(string));
        //                        table.Columns.Add("Lawfirm", typeof(string));
        //                        table.Columns.Add("Label", typeof(string));
        //                        table.Columns.Add("LabelValue", typeof(string));
        //                        table.Columns.Add("Interest", typeof(string));
        //                        table.Columns.Add("Penalty", typeof(string));
        //                        table.Columns.Add("ProvisionInBook", typeof(string));
        //                        table.Columns.Add("Total", typeof(string));
        //                        table.Columns.Add("SettlementValue", typeof(string));
        //                        table.Columns.Add("IsAllowed", typeof(string));
        //                        table.Columns.Add("ResponseDate", typeof(string));
        //                        table.Columns.Add("Description", typeof(string));
        //                        table.Columns.Add("Remark", typeof(string));
        //                        table.Columns.Add("NextHearingDate", typeof(string));
        //                        table.Columns.Add("CreatedByText", typeof(string));

        //                        //Filter for Tax Litigation Category
        //                        MisReport = MisReport.Where(entry => entry.CaseCategoryID == 47).ToList();

        //                        foreach (var item in MisReport)
        //                        {
        //                            long NoticeID = item.CaseInstanceID;
        //                            if (NoticeID != NoticeIDOld)
        //                            {
        //                                NoticeIDOld = item.CaseInstanceID;
        //                                List<HearingDetailReport> HearingData = CaseManagement.GetHearingDetailCaseWise(item.CaseInstanceID);

        //                                foreach (var itemValues in HearingData)
        //                                {
        //                                    HearingDataobj.Add(itemValues);
        //                                    tableCaseCustomField.Rows.Add(itemValues.CaseInstanceID, itemValues.ResponseDate, itemValues.Description, itemValues.Remark, itemValues.CreatedByText);
        //                                }
        //                            }
        //                        }

        //                        int beforecondition = 0;
        //                        for (int i = 0; i < MisReport.Count; i++)
        //                        {
        //                            if (HearingDataobj.Count > 0)
        //                            {
        //                                beforecondition = Convert.ToInt32(MisReport[i].CaseInstanceID);//1
        //                                for (int j = 0; j <= i; j++)
        //                                {
        //                                    if (HearingDataobj.Count > j)
        //                                    {
        //                                        if (HearingDataobj.Count == 1)
        //                                        {
        //                                            if (HearingDataobj[0].CaseInstanceID == MisReport[i].CaseInstanceID)
        //                                            {
        //                                                MisReport[i].Remark = HearingDataobj[j].Remark;
        //                                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].ResponseDate)))
        //                                                {
        //                                                    MisReport[i].ResponseDate = Convert.ToDateTime(HearingDataobj[j].ResponseDate).ToString("dd-MMM-yyyy");
        //                                                }
        //                                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].NextHearingDate)))
        //                                                {
        //                                                    MisReport[i].NextHearingDate = Convert.ToDateTime(HearingDataobj[j].NextHearingDate).ToString("dd-MMM-yyyy");
        //                                                }
        //                                                MisReport[i].Description = HearingDataobj[j].Description;
        //                                                MisReport[i].CreatedBy = HearingDataobj[j].CreatedByText;
        //                                                HearingDataobj.RemoveAt(j);
        //                                            }
        //                                            break;
        //                                        }
        //                                        else
        //                                        {
        //                                            if (HearingDataobj[j].CaseInstanceID == MisReport[i].CaseInstanceID)
        //                                            {
        //                                                MisReport[i].Remark = HearingDataobj[j].Remark;
        //                                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].ResponseDate)))
        //                                                {
        //                                                    MisReport[i].ResponseDate = Convert.ToDateTime(HearingDataobj[j].ResponseDate).ToString("dd-MMM-yyyy");
        //                                                }
        //                                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[j].NextHearingDate)))
        //                                                {
        //                                                    MisReport[i].NextHearingDate = Convert.ToDateTime(HearingDataobj[j].NextHearingDate).ToString("dd-MMM-yyyy");
        //                                                }
        //                                                MisReport[i].Description = HearingDataobj[j].Description;
        //                                                MisReport[i].CreatedBy = HearingDataobj[j].CreatedByText;
        //                                                HearingDataobj.RemoveAt(j);
        //                                                j = i;
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        if (HearingDataobj.Count > 0)
        //                        {
        //                            for (int k = 0; k < HearingDataobj.Count; k++)
        //                            {
        //                                long IDRemCheck = Convert.ToInt32(HearingDataobj[k].CaseInstanceID);
        //                                string RespDates = string.Empty;
        //                                string NextHDate = string.Empty;
        //                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[k].ResponseDate)))
        //                                {
        //                                    RespDates = Convert.ToDateTime(HearingDataobj[k].ResponseDate).ToString("dd-MMM-yyyy");
        //                                }
        //                                if (!string.IsNullOrEmpty(Convert.ToString(HearingDataobj[k].NextHearingDate)))
        //                                {
        //                                    NextHDate = Convert.ToDateTime(HearingDataobj[k].NextHearingDate).ToString("dd-MMM-yyyy");
        //                                }
        //                                MisReport.Add(new sp_GetMisReports_Result { CaseInstanceID = IDRemCheck, Remark = HearingDataobj[k].Remark, ResponseDate = RespDates, Description = HearingDataobj[k].Description, NextHearingDate = NextHDate, CreatedBy = HearingDataobj[k].CreatedByText });
        //                            }
        //                        }


        //                        MisReport = MisReport.OrderBy(entry => entry.CaseInstanceID).ToList();
        //                        long CheckIDOld = 0;
        //                        foreach (var item in MisReport)
        //                        {
        //                            long NoticeID = item.CaseInstanceID;
        //                            if (NoticeID != CheckIDOld)
        //                            {
        //                                ++P;
        //                                CheckIDOld = item.CaseInstanceID;
        //                                table.Rows.Add(P, item.CaseRefNo, item.CaseTitle, item.CaseType, item.OpenDate, item.CaseType1, item.Party, item.CourtName, item.Judge, item.CaseDetailDesc, item.Name, item.ClaimAmt, item.ExpenseTillDate, item.Owner, item.Lawfirm, item.Label, item.LabelValue, item.Interest, item.Penalty, item.ProvisionInBook, item.SettlementValue, item.Total, item.IsAllowed, item.ResponseDate, item.Description, item.Remark, item.NextHearingDate, item.CreatedBy);
        //                            }
        //                            else
        //                            {
        //                                table.Rows.Add("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", item.Label, item.LabelValue, item.Interest, item.Penalty, item.ProvisionInBook, item.SettlementValue, item.Total, item.IsAllowed, item.ResponseDate, item.Description, item.Remark, item.NextHearingDate, item.CreatedBy);
        //                            }
        //                        }


        //                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MIS Tax Litigation");
        //                        DataView view = new System.Data.DataView(table);
        //                        DataTable ExcelData = null;

        //                        ExcelData = view.ToTable("Selected", false, "SLNO", "CaseRefNo", "CaseTitle", "CaseType", "OpenDate", "CaseType1", "Party", "CourtName", "Judge", "CaseDetailDesc", "Name", "ClaimAmt", "ExpenseTillDate", "Owner", "Lawfirm", "Label", "LabelValue", "Interest", "Penalty", "ProvisionInBook", "SettlementValue", "Total", "IsAllowed", "ResponseDate", "Description", "Remark", "NextHearingDate", "CreatedByText");

        //                        foreach (DataRow item in ExcelData.Rows)
        //                        {
        //                            if (!string.IsNullOrEmpty(Convert.ToString(item["OpenDate"])))
        //                            {
        //                                var Opendate = Convert.ToDateTime(item["OpenDate"]).ToString("dd-MMM-yyyy");
        //                                item["OpenDate"] = Opendate;
        //                            }
        //                            if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
        //                            {
        //                                item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
        //                            }
        //                            if (!string.IsNullOrEmpty(Convert.ToString(item["NextHearingDate"])))
        //                            {
        //                                item["NextHearingDate"] = Convert.ToDateTime(item["NextHearingDate"]).ToString("dd-MMM-yyyy");
        //                            }
        //                        }


        //                        exWorkSheet1.Cells["A1"].Value = "SL NO";
        //                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["A1:A2"].Merge = true;
        //                        exWorkSheet1.Cells["A1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["B1"].Value = "Case No";
        //                        exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["B1:B2"].Merge = true;
        //                        exWorkSheet1.Cells["B1"].AutoFitColumns(15);
        //                        exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["C1"].Value = "Case Title";
        //                        exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["C1"].AutoFitColumns(20);
        //                        exWorkSheet1.Cells["C1:C2"].Merge = true;
        //                        exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["D1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["D1:D2"].Merge = true;
        //                        exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["D1"].Value = "Type";
        //                        exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["E1"].Value = "Open Date";
        //                        exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["E1"].AutoFitColumns(15);
        //                        exWorkSheet1.Cells["E1:E2"].Merge = true;
        //                        exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["F1"].Value = "Type of Case";
        //                        exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["F1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["F1:F2"].Merge = true;
        //                        exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["G1"].Value = "Party";
        //                        exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["G1"].AutoFitColumns(14);
        //                        exWorkSheet1.Cells["G1:G2"].Merge = true;
        //                        exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["H1"].Value = "Court Name";
        //                        exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["H1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["H1:H2"].Merge = true;
        //                        exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["I1"].Value = "Judge";
        //                        exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["I1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["I1:I2"].Merge = true;
        //                        exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["J1"].Value = "Case Detail Desc";
        //                        exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["J1"].AutoFitColumns(40);
        //                        exWorkSheet1.Cells["J1:J2"].Merge = true;
        //                        exWorkSheet1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["K1"].Value = "Entity/Location/Branch";
        //                        exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["K1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["K1:K2"].Merge = true;
        //                        exWorkSheet1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["L1"].Value = "Claim Amt";
        //                        exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["L1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["L1:L2"].Merge = true;
        //                        exWorkSheet1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["M1"].Value = "Expense Till Date";
        //                        exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["M1:M2"].Merge = true;
        //                        exWorkSheet1.Cells["M1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["N1"].Value = "Owner";
        //                        exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["N1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["N1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["N1:N2"].Merge = true;
        //                        exWorkSheet1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["O1"].Value = "Law Firm";
        //                        exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["O1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["O1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["O1:O2"].Merge = true;
        //                        exWorkSheet1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["P1"].Value = "Additional Tracking Parameter(s)";
        //                        exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["P1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["P1"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["P1:W1"].Merge = true;
        //                        exWorkSheet1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["P2"].Value = "Ground Of Appeal";
        //                        exWorkSheet1.Cells["P2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["P2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["P2"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["P2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["P2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["P2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["P2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["Q2"].Value = "Tax demand";
        //                        exWorkSheet1.Cells["Q2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["Q2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["Q2"].AutoFitColumns(10);
        //                        exWorkSheet1.Cells["Q2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["Q2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["Q2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["Q2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["R2"].Value = "Interest";
        //                        exWorkSheet1.Cells["R2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["R2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["R2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["R2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["R2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["R2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["R2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["S2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["S2"].Value = "Penalty";
        //                        exWorkSheet1.Cells["S2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["S2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["S2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["S2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["S2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["S2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["T2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["T2"].Value = "Provision In Book";
        //                        exWorkSheet1.Cells["T2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["T2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["T2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["T2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["T2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["T2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["U2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["U2"].Value = "Total";
        //                        exWorkSheet1.Cells["U2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["U2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["U2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["U2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["U2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["U2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["V2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["V2"].Value = "Settlement Value";
        //                        exWorkSheet1.Cells["V2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["V2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["V2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["V2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["V2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["V2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["W2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["W2"].Value = "Result";
        //                        exWorkSheet1.Cells["W2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["W2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["W2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["W2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["W2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["W2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["X1"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["X1"].Value = "Hearing Detail";
        //                        exWorkSheet1.Cells["X1"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["X1"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["X1:AB1"].Merge = true;
        //                        exWorkSheet1.Cells["X1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["X1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["X1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["X1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


        //                        exWorkSheet1.Cells["X2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["X2"].Value = "Response Date";
        //                        exWorkSheet1.Cells["X2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["X2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["X2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["X2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["X2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["X2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["Y2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["Y2"].Value = "Description";
        //                        exWorkSheet1.Cells["Y2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["Y2"].AutoFitColumns(40);
        //                        exWorkSheet1.Cells["Y2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["Y2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["Y2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["Y2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["Z2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["Z2"].Value = "Remark";
        //                        exWorkSheet1.Cells["Z2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["Z2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["Z2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["Z2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["Z2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["Z2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["AA2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["AA2"].Value = "Next Hearing Date";
        //                        exWorkSheet1.Cells["AA2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["AA2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["AA2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["AA2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["AA2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["AA2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        exWorkSheet1.Cells["AB2"].Style.Font.Bold = true;
        //                        exWorkSheet1.Cells["AB2"].Value = "Created By";
        //                        exWorkSheet1.Cells["AB2"].Style.Font.Size = 12;
        //                        exWorkSheet1.Cells["AB2"].AutoFitColumns(12);
        //                        exWorkSheet1.Cells["AB2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        exWorkSheet1.Cells["AB2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //                        exWorkSheet1.Cells["AB2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        exWorkSheet1.Cells["AB2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

        //                        if (ExcelData.Rows.Count > 0)
        //                        {
        //                            exWorkSheet1.Cells["A3"].LoadFromDataTable(ExcelData, false);
        //                        }

        //                        #region Merging
        //                        string ActulValue = string.Empty;
        //                        string NewValue = string.Empty;
        //                        int RowValj = 3;
        //                        int RowVal = 3; ;
        //                        for (int i = 3; i <= 2 + ExcelData.Rows.Count; i++)
        //                        {

        //                            string cOpenDate = "c" + i;
        //                            exWorkSheet1.Cells[cOpenDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                            string CloseDate = "D" + i;
        //                            exWorkSheet1.Cells[CloseDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                            string CStatus = "E" + i;
        //                            exWorkSheet1.Cells[CStatus].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                            string cResponceDate = "F" + i;
        //                            exWorkSheet1.Cells[cResponceDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


        //                            string chke = "B" + i;
        //                            string Checkcell = exWorkSheet1.Cells[chke].Value.ToString();
        //                            if (i > 3)
        //                            {
        //                                string cOpenDate1 = "c" + i;
        //                                exWorkSheet1.Cells[cOpenDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                                string CloseDate1 = "D" + i;
        //                                exWorkSheet1.Cells[CloseDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                                string CStatus1 = "E" + i;
        //                                exWorkSheet1.Cells[CStatus1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                                string cResponceDate1 = "F" + i;
        //                                exWorkSheet1.Cells[cResponceDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                                if (string.IsNullOrEmpty(Checkcell))
        //                                {
        //                                    RowValj++;
        //                                }
        //                                else
        //                                {
        //                                    string checknow4 = "A" + RowVal + ":A" + RowValj;
        //                                    string chekcnow = "B" + RowVal + ":B" + RowValj;
        //                                    string chekcnow1 = "C" + RowVal + ":C" + RowValj;
        //                                    string chekcnow2 = "D" + RowVal + ":D" + RowValj;
        //                                    string chekcnow3 = "E" + RowVal + ":E" + RowValj;

        //                                    string chekcnow5 = "F" + RowVal + ":F" + RowValj;
        //                                    string chekcnow6 = "G" + RowVal + ":G" + RowValj;
        //                                    string chekcnow7 = "H" + RowVal + ":H" + RowValj;
        //                                    string chekcnow8 = "I" + RowVal + ":I" + RowValj;
        //                                    string chekcnow9 = "J" + RowVal + ":J" + RowValj;
        //                                    string chekcnow10 = "K" + RowVal + ":K" + RowValj;
        //                                    string chekcnow11 = "L" + RowVal + ":L" + RowValj;
        //                                    string chekcnow12 = "M" + RowVal + ":M" + RowValj;
        //                                    string chekcnow13 = "N" + RowVal + ":N" + RowValj;
        //                                    string chekcnow14 = "O" + RowVal + ":O" + RowValj;

        //                                    exWorkSheet1.Cells[checknow4].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow1].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow2].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow3].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow5].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow6].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow7].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow8].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow9].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow10].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow11].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow12].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow13].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow14].Merge = true;

        //                                    RowVal = i;
        //                                    RowValj = i;
        //                                }
        //                                if (RowValj == (2 + ExcelData.Rows.Count))
        //                                {
        //                                    string checknow4 = "A" + RowVal + ":A" + RowValj;
        //                                    string chekcnow = "B" + RowVal + ":B" + RowValj;
        //                                    string chekcnow1 = "C" + RowVal + ":C" + RowValj;
        //                                    string chekcnow2 = "D" + RowVal + ":D" + RowValj;
        //                                    string chekcnow3 = "E" + RowVal + ":E" + RowValj;
        //                                    string chekcnow5 = "F" + RowVal + ":F" + RowValj;
        //                                    string chekcnow6 = "G" + RowVal + ":G" + RowValj;
        //                                    string chekcnow7 = "H" + RowVal + ":H" + RowValj;
        //                                    string chekcnow8 = "I" + RowVal + ":I" + RowValj;
        //                                    string chekcnow9 = "J" + RowVal + ":J" + RowValj;
        //                                    string chekcnow10 = "K" + RowVal + ":K" + RowValj;
        //                                    string chekcnow11 = "L" + RowVal + ":L" + RowValj;
        //                                    string chekcnow12 = "M" + RowVal + ":M" + RowValj;
        //                                    string chekcnow13 = "N" + RowVal + ":N" + RowValj;
        //                                    string chekcnow14 = "O" + RowVal + ":O" + RowValj;

        //                                    exWorkSheet1.Cells[checknow4].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow1].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow2].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow3].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow5].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow6].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow7].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow8].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow9].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow10].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow11].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow12].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow13].Merge = true;
        //                                    exWorkSheet1.Cells[chekcnow14].Merge = true;
        //                                }
        //                            }
        //                        }

        //                        #endregion End Merging

        //                        int count = Convert.ToInt32(ExcelData.Rows.Count) + 2;
        //                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 28])
        //                        {
        //                            col.Style.WrapText = true;
        //                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
        //                        }
        //                    }
        //                    #endregion


        //                    Byte[] fileBytes = exportPackge.GetAsByteArray();
        //                    Response.ClearContent();
        //                    Response.Buffer = true;
        //                    string locatioName = "MIS.xlsx";
        //                    Response.AddHeader("content-disposition", "attachment;filename=report" + locatioName);//locatioName
        //                                                                                                          //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
        //                    Response.Charset = "";
        //                    Response.ContentType = "application/vnd.ms-excel";
        //                    StringWriter sw = new StringWriter();
        //                    Response.BinaryWrite(fileBytes);
        //                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
        //                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
        //                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

        //                }
        //                catch (Exception ex)
        //                {
        //                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //                }
        //            }
        //            #endregion
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        #endregion

        protected void btnMis_Click(object sender, EventArgs e)
        {
            try
            {
                //sp_GetMisReports
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MisReport = (entities.sp_GetMisReports_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                    if (Branchlist.Count > 0)
                    {
                        MisReport = MisReport.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                  
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if ( IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        MisReport = MisReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                             MisReport = MisReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticecaseInstanceId))).ToList();
                            
                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            MisReport = MisReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            MisReport = MisReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (MisReport.Count > 0)
                    {
                        MisReport = (from g in MisReport
                                     group g by new
                                     {
                                         g.NoticeType,
                                         g.NoticeDate,
                                         g.RefNo,
                                         g.NoticeTitle,
                                         g.NoticeTerm,
                                         g.CaseType1,
                                         g.Party,
                                         g.NoticeDetailDesc,
                                         g.Name,
                                         g.Jurisdiction,
                                         g.ClaimAmt,
                                         g.ResponseDate,
                                         g.Responsedescription,
                                         g.ResponseRemark,
                                         g.NoticeDueDate,
                                         g.ExpenseTillDate,
                                         g.Owner,
                                         g.Lawfirm,
                                         g.State,
                                         g.FYName,
                                         g.Monetory,
                                         g.Provisionalamt,
                                         g.BankGurantee,
                                         g.ProtestMoney,
                                         g.ProbableAmt,
                                         g.AssignedUser,
                                         g.RecoveryAmount,
                                         g.CBU,
                                         g.Zone,
                                         g.Region,
                                         g.Territory,
                                         g.CaseCreator,
                                         g.EmailId,
                                         g.ContactNumber,
                                         g.RiskType

                                     } into GCS
                                     select new sp_GetMisReports_Notice_Result()
                                     {
                                         NoticeType = GCS.Key.NoticeType,
                                         NoticeDate = GCS.Key.NoticeDate,
                                         RefNo = GCS.Key.RefNo,
                                         NoticeTitle = GCS.Key.NoticeTitle,
                                         NoticeTerm = GCS.Key.NoticeTerm,
                                         CaseType1 = GCS.Key.CaseType1,
                                         Party = GCS.Key.Party,
                                         NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                         Name = GCS.Key.Name,
                                         Jurisdiction = GCS.Key.Jurisdiction,
                                         ClaimAmt = GCS.Key.ClaimAmt,
                                         ResponseDate = GCS.Key.ResponseDate,
                                         Responsedescription = GCS.Key.Responsedescription,
                                         ResponseRemark = GCS.Key.ResponseRemark,
                                         NoticeDueDate = GCS.Key.NoticeDueDate,
                                         ExpenseTillDate = GCS.Key.ExpenseTillDate,
                                         Owner = GCS.Key.Owner,
                                         Lawfirm = GCS.Key.Lawfirm,
                                         State = GCS.Key.State,
                                         FYName = GCS.Key.FYName,
                                         Monetory = GCS.Key.Monetory,
                                         Provisionalamt = GCS.Key.Provisionalamt,
                                         BankGurantee = GCS.Key.BankGurantee,
                                         ProtestMoney = GCS.Key.ProtestMoney,
                                         ProbableAmt = GCS.Key.ProbableAmt,
                                         AssignedUser = GCS.Key.AssignedUser,
                                         RecoveryAmount = GCS.Key.RecoveryAmount,
                                         CBU = GCS.Key.CBU,
                                         Zone = GCS.Key.Zone,
                                         Region = GCS.Key.Region,
                                         Territory = GCS.Key.Territory,
                                         CaseCreator = GCS.Key.CaseCreator,
                                         EmailId = GCS.Key.EmailId,
                                         ContactNumber = GCS.Key.ContactNumber,
                                         RiskType = GCS.Key.RiskType

                                     }).ToList();
                    }
                    //if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    //{
                    //    MisReport = MisReport.Where(entry => entry.Name == tvFilterLocation.SelectedNode.Text).ToList();
                    //}
                    //HearingDate
                    if (!string.IsNullOrEmpty(txtFromDate.Text))
                    {
                        MisReport = MisReport.Where(entry => entry.NoticeDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
                    }
                    if (!string.IsNullOrEmpty(txtToDate.Text))
                    {
                        MisReport = MisReport.Where(entry => entry.NoticeDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
                    }
                   
                    #region MIS
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            DataTable table = new DataTable();
                            table.Columns.Add("SLNO", typeof(string));
                            table.Columns.Add("NoticeType", typeof(string));
                            table.Columns.Add("NoticeDate", typeof(string));
                            table.Columns.Add("RefNo", typeof(string));
                            table.Columns.Add("NoticeTitle", typeof(string));
                            table.Columns.Add("NoticeTerm", typeof(string));
                            table.Columns.Add("CaseType1", typeof(string));
                            table.Columns.Add("Party", typeof(string));
                            table.Columns.Add("NoticeDetailDesc", typeof(string));
                            table.Columns.Add("Name", typeof(string));
                            table.Columns.Add("Jurisdiction", typeof(string));
                            table.Columns.Add("ResponseDate", typeof(string));
                            table.Columns.Add("Responsedescription", typeof(string));
                            table.Columns.Add("ResponseRemark", typeof(string));
                            table.Columns.Add("NoticeDueDate", typeof(string));
                            table.Columns.Add("Owner", typeof(string));
                            table.Columns.Add("Lawfirm", typeof(string));
                            table.Columns.Add("AssignedUser", typeof(string));
                            table.Columns.Add("State", typeof(string));
                            table.Columns.Add("FYName", typeof(string));
                            table.Columns.Add("Monetory", typeof(string));
                            table.Columns.Add("BankGurantee", typeof(string));
                            table.Columns.Add("RecoveryAmount", typeof(decimal));
                            table.Columns.Add("ClaimAmt", typeof(decimal));
                            table.Columns.Add("Provisionalamt", typeof(decimal));
                            table.Columns.Add("ProtestMoney", typeof(decimal));
                            table.Columns.Add("ProbableAmt", typeof(decimal));
                            table.Columns.Add("ExpenseTillDate", typeof(int));
                            if (NewColumnsLitigation == true)
                            {
                                table.Columns.Add("CBU", typeof(string));
                                table.Columns.Add("Zone", typeof(string));
                                table.Columns.Add("Region", typeof(string));
                                table.Columns.Add("Territory", typeof(string));
                                table.Columns.Add("CaseCreator", typeof(string));
                                table.Columns.Add("EmailId", typeof(string));
                                table.Columns.Add("ContactNumber", typeof(string));
                            }
                            table.Columns.Add("RiskType", typeof(string));


                            int Count = 0;

                            if (NewColumnsLitigation == true)
                            {
                                foreach (var item in MisReport)
                                {
                                    Count++;
                                    table.Rows.Add(Count, item.NoticeType, item.NoticeDate, item.RefNo, item.NoticeTitle, item.NoticeTerm, item.CaseType1,
                                        item.Party.TrimEnd(','), item.NoticeDetailDesc, item.Name, item.Jurisdiction, item.ResponseDate,
                                        item.Responsedescription, item.ResponseRemark, item.NoticeDueDate,
                                        item.Owner, item.Lawfirm.TrimEnd(','), item.AssignedUser.TrimEnd(','), item.State, item.FYName.TrimEnd(','),
                                        item.Monetory, item.BankGurantee, item.RecoveryAmount, item.ClaimAmt, item.Provisionalamt, item.ProtestMoney, item.ProbableAmt, item.ExpenseTillDate,
                                        item.CBU, item.Zone, item.Region, item.Territory, item.CaseCreator, item.EmailId, item.ContactNumber);

                                }
                            }
                            else
                            {
                                foreach (var item in MisReport)
                                {
                                    Count++;
                                    table.Rows.Add(Count, item.NoticeType, item.NoticeDate, item.RefNo, item.NoticeTitle, item.NoticeTerm, item.CaseType1,
                                        item.Party.TrimEnd(','), item.NoticeDetailDesc, item.Name, item.Jurisdiction, item.ResponseDate,
                                        item.Responsedescription, item.ResponseRemark, item.NoticeDueDate,
                                        item.Owner, item.Lawfirm.TrimEnd(','), item.AssignedUser.TrimEnd(','), item.State, item.FYName.TrimEnd(','),
                                        item.Monetory, item.BankGurantee, item.RecoveryAmount, item.ClaimAmt, item.Provisionalamt, item.ProtestMoney, item.ProbableAmt, item.ExpenseTillDate ,item.RiskType
                                       );

                                }
                            }
                               

                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MIS");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            if (NewColumnsLitigation == true)
                            {
                                ExcelData = view.ToTable("Selected", false, "SLNO", "NoticeType", "NoticeDate", "RefNo", "NoticeTitle", "NoticeTerm", "CaseType1", "Party", "NoticeDetailDesc", "Name", "Jurisdiction", "ResponseDate", "Responsedescription", "ResponseRemark", "NoticeDueDate",
                                                          "Owner", "Lawfirm", "AssignedUser", "State", "FYName", "Monetory", "BankGurantee", "RecoveryAmount", "ClaimAmt", "Provisionalamt", "ProtestMoney", "ProbableAmt", "ExpenseTillDate", "CBU", "Zone", "Region", "Territory", "CaseCreator", "EmailId", "ContactNumber");

                            }
                            else
                            {
                                ExcelData = view.ToTable("Selected", false, "SLNO", "NoticeType", "NoticeDate", "RefNo", "NoticeTitle", "NoticeTerm", "CaseType1", "Party", "NoticeDetailDesc", "Name", "Jurisdiction", "ResponseDate", "Responsedescription", "ResponseRemark", "NoticeDueDate",
                                                          "Owner", "Lawfirm", "AssignedUser", "State", "FYName", "Monetory", "BankGurantee", "RecoveryAmount", "ClaimAmt", "Provisionalamt", "ProtestMoney", "ProbableAmt", "ExpenseTillDate","RiskType");

                            }

                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["NoticeDate"])))
                                {
                                    var Opendate = Convert.ToDateTime(item["NoticeDate"]).ToString("dd-MMM-yyyy");
                                    item["NoticeDate"] = Opendate;
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
                                {
                                    item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["NoticeDueDate"])))
                                {
                                    item["NoticeDueDate"] = Convert.ToDateTime(item["NoticeDueDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            int count3 = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            int count2 = Convert.ToInt32(ExcelData.Rows.Count) + 2;

                            exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet1.Cells["A1"].Value = "Sr NO";
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A1"].AutoFitColumns(10);
                            exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["B1"].Value = "Type";
                            exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["B1"].AutoFitColumns(10);
                            exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["C1"].Value = "Notice Open Date";
                            exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C1"].AutoFitColumns(10);
                            //exWorkSheet1.Cells[1A3"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["D1"].Value = "Notice No";
                            exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["E1"].Value = "Notice Title";
                            exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["F1"].Value = "Notice Term";
                            exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["G1"].Value = "Type of Notice";
                            exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            exWorkSheet1.Cells["H1"].Value = "Opponent";
                            exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H1"].AutoFitColumns(20);
                            //exWorkSheet1.Cells1C5"].Style.Numberformat.Format = "dd-MM-yyyy";
                            exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I1"].Value = "Notice Detail Description";
                            exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I1"].AutoFitColumns(40);
                            exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J1"].Value = "Entity/Location/Branch";
                            exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K1"].Value = "Jurisdiction";
                            exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["K1"].AutoFitColumns(10);
                            exWorkSheet1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["L1"].Value = "Response Date";
                            exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["L1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["M1"].Value = "Response Description";
                            exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["M1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["N1"].Value = "Response Remark";
                            exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["N1"].AutoFitColumns(10);
                            exWorkSheet1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["O1"].Value = "Notice Due Date";
                            exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["O1"].AutoFitColumns(10);
                            //exWorkSheet1.CellsO"M5"].Style.Numberformat.Format = "dd-MM-yyyy";
                            exWorkSheet1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P1"].Value = "Owner";
                            exWorkSheet1.Cells["P1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["P1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["Q1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q1"].Value = "Law Firm";
                            exWorkSheet1.Cells["Q1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Q1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Q1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["R1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R1"].Value = "Name of Lawyer's";
                            exWorkSheet1.Cells["R1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["R1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["R1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["R1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["S1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S1"].Value = "State";
                            exWorkSheet1.Cells["S1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["S1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["S1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["S1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            if(FYCustID == true)
                            {
                                exWorkSheet1.Cells["T1"].Value = "Disputed Financial Year";
                            }
                            else
                            {
                                exWorkSheet1.Cells["T1"].Value = "Financial Year";
                            }
                            exWorkSheet1.Cells["T1"].Style.Font.Bold = true;  
                            exWorkSheet1.Cells["T1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["T1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["T1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["T1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["U1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["U1"].Value = "Monetory";
                            exWorkSheet1.Cells["U1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["U1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["U1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["U1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["U1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["U1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            exWorkSheet1.Cells["V1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["V1"].Value = "Bank Guarantee";
                            exWorkSheet1.Cells["V1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["V1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["V1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["V1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            exWorkSheet1.Cells["W1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["W1"].Value = "Recovery Amount";
                            exWorkSheet1.Cells["W1:W" + count3].Style.Numberformat.Format = "#,##0.00";
                            exWorkSheet1.Cells["W1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["W1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["W1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["W1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["W1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["W1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["X1"].Value = "Claim Amount";
                            exWorkSheet1.Cells["X1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["X1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["X1:X" + count3].Style.Numberformat.Format = "#,##0.00";
                            exWorkSheet1.Cells["X1"].AutoFitColumns(10);
                            exWorkSheet1.Cells["X1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["X1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["X1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["X1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["Y1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Y1"].Value = "Provisional Amount";
                            exWorkSheet1.Cells["Y1:Y" + count3].Style.Numberformat.Format = "#,##0.00";
                            exWorkSheet1.Cells["Y1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Y1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["Y1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["Y1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["Y1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Y1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["Z1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Z1"].Value = "Protest Money";
                            exWorkSheet1.Cells["Z1:Z" + count3].Style.Numberformat.Format = "#,##0.00";
                            exWorkSheet1.Cells["Z1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Z1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["Z1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["Z1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["Z1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Z1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["AA1"].Value = "Probable Amount";
                            exWorkSheet1.Cells["AA1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AA1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AA1:AA" + count3].Style.Numberformat.Format = "#,##0.00";
                            exWorkSheet1.Cells["AA1"].AutoFitColumns(10);
                            exWorkSheet1.Cells["AA1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["AA1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["AA1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["AA1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["AB1"].Value = "Expense Till Date";
                            exWorkSheet1.Cells["AB1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["AB1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["AB1:AB" + count3].Style.Numberformat.Format = "#,##0.00";
                            exWorkSheet1.Cells["AB1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["AB1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["AB1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["AB1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["AB1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            if (RiskType == true)
                            {
                                exWorkSheet1.Cells["AC1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AC1"].Value = "Risk";
                                exWorkSheet1.Cells["AC1:AC" + count3].Style.Numberformat.Format = "#,##0.00";
                                exWorkSheet1.Cells["AC1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AC1"].AutoFitColumns(25);
                                exWorkSheet1.Cells["AC1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["AC1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["AC1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["AC1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            }

                                if (NewColumnsLitigation == true)
                            {
                                exWorkSheet1.Cells["AC1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AC1"].Value = "CBU";
                                exWorkSheet1.Cells["AC1:AC" + count3].Style.Numberformat.Format = "#,##0.00";
                                exWorkSheet1.Cells["AC1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AC1"].AutoFitColumns(25);
                                exWorkSheet1.Cells["AC1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["AC1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["AC1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["AC1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet1.Cells["AD1"].Value = "Zone";
                                exWorkSheet1.Cells["AD1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AD1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AD1:AD" + count3].Style.Numberformat.Format = "#,##0.00";
                                exWorkSheet1.Cells["AD1"].AutoFitColumns(20);
                                exWorkSheet1.Cells["AD1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["AD1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["AD1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["AD1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet1.Cells["AE1"].Value = "Region";
                                exWorkSheet1.Cells["AE1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AE1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AE1:AE" + count3].Style.Numberformat.Format = "#,##0.00";
                                exWorkSheet1.Cells["AE1"].AutoFitColumns(35);
                                exWorkSheet1.Cells["AE1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["AE1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["AE1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["AE1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet1.Cells["AF1"].Value = "Territory";
                                exWorkSheet1.Cells["AF1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AF1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AF1:AF" + count3].Style.Numberformat.Format = "#,##0.00";
                                exWorkSheet1.Cells["AF1"].AutoFitColumns(20);
                                exWorkSheet1.Cells["AF1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["AF1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["AF1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["AF1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheet1.Cells["AG1"].Value = "Notice Creator";
                                exWorkSheet1.Cells["AG1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AG1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AG1"].AutoFitColumns(15);
                                exWorkSheet1.Cells["AG1:AG" + count3].Style.Numberformat.Format = "#,##0.00";
                                exWorkSheet1.Cells["AG1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["AG1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["AG1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["AG1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet1.Cells["AH1"].Value = "Email ID";
                                exWorkSheet1.Cells["AH1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AH1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AH1"].AutoFitColumns(15);
                                exWorkSheet1.Cells["AH1:AH" + count3].Style.Numberformat.Format = "#,##0.00";
                                exWorkSheet1.Cells["AH1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["AH1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["AH1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["AH1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet1.Cells["AI1"].Value = "Contact Number";
                                exWorkSheet1.Cells["AI1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["AI1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["AI1"].AutoFitColumns(15);
                                exWorkSheet1.Cells["AI1:AI" + count3].Style.Numberformat.Format = "#,##0.00";
                                exWorkSheet1.Cells["AI1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["AI1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["AI1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["AI1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            }

                            exWorkSheet1.Cells["A" + count2].Value = "Total";
                            exWorkSheet1.Cells["A" + count2].Style.Font.Bold = true;

                            exWorkSheet1.Cells["W" + count2].Formula = "=SUM(W2:W" + count3 + ")";
                            exWorkSheet1.Cells["W" + count2].Style.Numberformat.Format = "#,##0.00";

                            exWorkSheet1.Cells["X" + count2].Formula = "=SUM(X2:X" + count3 + ")";
                            exWorkSheet1.Cells["X" + count2].Style.Numberformat.Format = "#,##0.00";

                            exWorkSheet1.Cells["Y" + count2].Formula = "=SUM(Y2:Y" + count3 + ")";
                            exWorkSheet1.Cells["Y" + count2].Style.Numberformat.Format = "#,##0.00";

                            exWorkSheet1.Cells["Z" + count2].Formula = "=SUM(Z2:Z" + count3 + ")";
                            exWorkSheet1.Cells["Z" + count2].Style.Numberformat.Format = "#,##0.00";

                            exWorkSheet1.Cells["AA" + count2].Formula = "=SUM(AA2:AA" + count3 + ")";
                            exWorkSheet1.Cells["AA" + count2].Style.Numberformat.Format = "#,##0.00";

                            exWorkSheet1.Cells["AB" + count2].Formula = "=SUM(AB2:AB" + count3 + ")";
                            exWorkSheet1.Cells["AB" + count2].Style.Numberformat.Format = "#,##0.00";

                             if (NewColumnsLitigation == true)
                            {
                                int count = Convert.ToInt32(ExcelData.Rows.Count) + 2;
                                using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 36])
                                {
                                    col.Style.WrapText = true;
                                    //col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                }
                            }
                             else
                            {
                                int count = Convert.ToInt32(ExcelData.Rows.Count) + 2;
                                using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 29])
                                {
                                    col.Style.WrapText = true;
                                    //col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                }
                            }
                                


                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "Notice MIS-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                            //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnMisCloseReport_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MisReportClosedNotice = (entities.sp_GetMisReportsClosed_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                    if (Branchlist.Count > 0)
                    {
                        MisReportClosedNotice = MisReportClosedNotice.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();
                    }
                   
                    ClientCustomization isexist= UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    if (isexist != null) //(IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    {
                        MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                         
                            MisReportClosedNotice = MisReportClosedNotice.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticecaseInstanceId))).ToList();
                       
                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            MisReportClosedNotice = MisReportClosedNotice.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (MisReportClosedNotice.Count > 0)
                    {
                        MisReportClosedNotice = (from g in MisReportClosedNotice
                                                 group g by new
                                                 {
                                                     g.NoticeType,
                                                     g.NoticeDate,
                                                     g.CloseDate,
                                                     g.RefNo,
                                                     g.NoticeTitle,
                                                     g.NoticeTerm,
                                                     g.CaseType1,
                                                     g.Party,
                                                     g.NoticeDetailDesc,
                                                     g.Jurisdiction,
                                                     g.Name,
                                                     g.ClaimAmt,
                                                     g.ResponseDate,
                                                     g.Responsedescription,
                                                     g.ResponseRemark,
                                                     g.NoticeDueDate,
                                                     g.ExpenseTillDate,
                                                     g.Owner,
                                                     g.Lawfirm,
                                                     g.State,
                                                     g.Monetory,
                                                     g.FYName,
                                                     g.NoticeResult,
                                                     g.Provisionalamt,
                                                     g.BankGurantee,
                                                     g.ProtestMoney,
                                                     g.RiskType
                                                 } into GCS
                                                 select new sp_GetMisReportsClosed_Notice_Result()
                                                 {
                                                     NoticeType = GCS.Key.NoticeType,
                                                     NoticeDate = GCS.Key.NoticeDate,
                                                     CloseDate = GCS.Key.CloseDate,
                                                     RefNo = GCS.Key.RefNo,
                                                     NoticeTitle = GCS.Key.NoticeTitle,
                                                     NoticeTerm = GCS.Key.NoticeTerm,
                                                     CaseType1 = GCS.Key.CaseType1,
                                                     Party = GCS.Key.Party,
                                                     NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                                     Name = GCS.Key.Name,
                                                     Jurisdiction = GCS.Key.Jurisdiction,
                                                     ClaimAmt = GCS.Key.ClaimAmt,
                                                     ResponseDate = GCS.Key.ResponseDate,
                                                     Responsedescription = GCS.Key.Responsedescription,
                                                     ResponseRemark = GCS.Key.ResponseRemark,
                                                     NoticeDueDate = GCS.Key.NoticeDueDate,
                                                     ExpenseTillDate = GCS.Key.ExpenseTillDate,
                                                     Owner = GCS.Key.Owner,
                                                     Lawfirm = GCS.Key.Lawfirm,
                                                     State = GCS.Key.State,
                                                     Monetory = GCS.Key.Monetory,
                                                     FYName = GCS.Key.FYName,
                                                     NoticeResult = GCS.Key.NoticeResult,
                                                     Provisionalamt = GCS.Key.Provisionalamt,
                                                     BankGurantee = GCS.Key.BankGurantee,
                                                     ProtestMoney = GCS.Key.ProtestMoney,
                                                     RiskType = GCS.Key.RiskType
                                                 }).ToList();
                    }
                    //if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    //{
                    //    MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.Name == tvFilterLocation.SelectedNode.Text).ToList();
                    //}
                    if (!string.IsNullOrEmpty(txtFromDate.Text))
                    {
                        MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.CloseDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
                    }
                    if (!string.IsNullOrEmpty(txtToDate.Text))
                    {
                        MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.CloseDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
                    }
                   
                    #region MIS Closed
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            {
                                DataTable table = new DataTable();
                                table.Columns.Add("SLNO", typeof(string));
                                table.Columns.Add("NoticeType", typeof(string));
                                table.Columns.Add("NoticeDate", typeof(string));
                                table.Columns.Add("CloseDate", typeof(string));
                                table.Columns.Add("RefNo", typeof(string));
                                table.Columns.Add("NoticeTitle", typeof(string));
                                table.Columns.Add("NoticeTerm", typeof(string));
                                table.Columns.Add("CaseType1", typeof(string));
                                table.Columns.Add("Party", typeof(string));
                                table.Columns.Add("NoticeDetailDesc", typeof(string));
                                table.Columns.Add("Name", typeof(string));
                                table.Columns.Add("Jurisdiction", typeof(string));
                                table.Columns.Add("ClaimAmt", typeof(string));
                                table.Columns.Add("ResponseDate", typeof(string));
                                table.Columns.Add("Responsedescription", typeof(string));
                                table.Columns.Add("ResponseRemark", typeof(string));
                                table.Columns.Add("NoticeDueDate", typeof(string));
                                table.Columns.Add("ExpenseTillDate", typeof(string));
                                table.Columns.Add("Owner", typeof(string));
                                table.Columns.Add("Lawfirm", typeof(string));
                                table.Columns.Add("State", typeof(string));
                                table.Columns.Add("Monetory", typeof(string));
                                table.Columns.Add("FYName", typeof(string));
                                table.Columns.Add("NoticeResult", typeof(string));
                                table.Columns.Add("Provisionalamt", typeof(string));
                                table.Columns.Add("BankGurantee", typeof(string));
                                table.Columns.Add("ProtestMoney", typeof(string));
                                table.Columns.Add("RiskType", typeof(string));

                                int Count = 0;
                                foreach (var item in MisReportClosedNotice)
                                {
                                    Count++;
                                    table.Rows.Add(Count, item.NoticeType, item.NoticeDate, item.CloseDate, item.RefNo, item.NoticeTitle, item.NoticeTerm, item.CaseType1, item.Party, item.NoticeDetailDesc, item.Name, item.Jurisdiction, item.ClaimAmt, item.ResponseDate, item.Responsedescription, item.ResponseRemark, item.NoticeDueDate, item.ExpenseTillDate, item.Owner, item.Lawfirm, item.State, item.Monetory, item.FYName, item.NoticeResult, item.Provisionalamt, item.BankGurantee, item.ProtestMoney,item.RiskType);
                                }
                                ExcelWorksheet exWorkSheetC1 = exportPackge.Workbook.Worksheets.Add("Closed Notice");
                                DataView view = new System.Data.DataView(table);
                                DataTable ExcelData = null;
                                ExcelData = view.ToTable("Selected", false, "SLNO", "NoticeType", "NoticeDate", "CloseDate", "RefNo", "NoticeTitle", "NoticeTerm", "CaseType1", "Party", "NoticeDetailDesc", "Name", "Jurisdiction", "ClaimAmt", "ResponseDate", "Responsedescription", "ResponseRemark", "NoticeDueDate", "ExpenseTillDate", "Owner", "Lawfirm", "State", "Monetory", "FYName", "NoticeResult", "Provisionalamt", "BankGurantee", "ProtestMoney","RiskType");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["NoticeDate"])))
                                    {
                                        item["NoticeDate"] = Convert.ToDateTime(item["NoticeDate"]).ToString("dd-MMM-yyyy");
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["CloseDate"])))
                                    {
                                        item["CloseDate"] = Convert.ToDateTime(item["CloseDate"]).ToString("dd-MMM-yyyy");
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
                                    {
                                        item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["NoticeDueDate"])))
                                    {
                                        item["NoticeDueDate"] = Convert.ToDateTime(item["NoticeDueDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                exWorkSheetC1.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                exWorkSheetC1.Cells["A1"].Value = "Sr NO";
                                exWorkSheetC1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["A1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["A1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["B1"].Value = "Type";
                                exWorkSheetC1.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["B1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["B1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["C1"].Value = "Notice Date";
                                exWorkSheetC1.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["C1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["C1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["D1"].Value = "Notice Close Date";
                                exWorkSheetC1.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["D1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["D1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["E1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["E1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["E1"].Value = "Notice Ref No";
                                exWorkSheetC1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["F1"].Value = "Notice Title";
                                exWorkSheetC1.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["F1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["F1"].AutoFitColumns(30);
                                exWorkSheetC1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["G1"].Value = "Notice Term";
                                exWorkSheetC1.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["G1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["G1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["H1"].Value = "Type of Notice";
                                exWorkSheetC1.Cells["H1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["H1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["H1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["I1"].Value = "Opponent";
                                exWorkSheetC1.Cells["I1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["I1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["I1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheetC1.Cells["J1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["J1"].Value = "Notice Detail Description";
                                exWorkSheetC1.Cells["J1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["J1"].AutoFitColumns(40);
                                exWorkSheetC1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheetC1.Cells["K1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["K1"].Value = "Entity/Location/Branch";
                                exWorkSheetC1.Cells["K1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["K1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["L1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["L1"].Value = "Jurisdiction";
                                exWorkSheetC1.Cells["L1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["L1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["M1"].Value = "Claim Amt";
                                exWorkSheetC1.Cells["M1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["M1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["M1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["N1"].Value = "Response Date";
                                exWorkSheetC1.Cells["N1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["N1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["N1"].AutoFitColumns(35);
                                exWorkSheetC1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["O1"].Value = "Response Description";
                                exWorkSheetC1.Cells["O1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["O1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["O1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["P1"].Value = "Response Remark";
                                exWorkSheetC1.Cells["P1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["P1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["P1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["Q1"].Value = "Notice Due Date";
                                exWorkSheetC1.Cells["Q1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["Q1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["Q1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["Q1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["R1"].Value = "Expense Till Date";
                                exWorkSheetC1.Cells["R1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["R1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["R1"].AutoFitColumns(20);
                                exWorkSheetC1.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["R1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["R1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["S1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["S1"].Value = "Owner";
                                exWorkSheetC1.Cells["S1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["S1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["S1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["S1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["T1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["T1"].Value = "Law Firm";
                                exWorkSheetC1.Cells["T1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["T1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["T1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["T1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["T1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheetC1.Cells["U1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["U1"].Value = "State";
                                exWorkSheetC1.Cells["U1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["U1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["U1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["U1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["U1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["U1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["V1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["V1"].Value = "Monetory";
                                exWorkSheetC1.Cells["V1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["V1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["V1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["V1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                if (FYCustID == true)
                                {
                                    exWorkSheetC1.Cells["W1"].Value = "Disputed Financial Year";
                                }
                                else
                                {
                                    exWorkSheetC1.Cells["W1"].Value = "FinancialYear";
                                }
                                exWorkSheetC1.Cells["W1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["W1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["W1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["W1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["W1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["W1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["W1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663



                                exWorkSheetC1.Cells["X1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["X1"].Value = "Notice Result";
                                exWorkSheetC1.Cells["X1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["X1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["X1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["X1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["X1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["X1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheetC1.Cells["Y1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["Y1"].Value = "Provisional amount";
                                exWorkSheetC1.Cells["Y1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["Y1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["Y1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["Y1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["Y1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["Y1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["Z1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["Z1"].Value = "Bank Guarantee";
                                exWorkSheetC1.Cells["Z1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["Z1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["Z1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["Z1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["Z1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["Z1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["AA1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["AA1"].Value = "Protest Money";
                                exWorkSheetC1.Cells["AA1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["AA1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["AA1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["AA1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["AA1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["AA1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                if(RiskType == true)
                                {
                                    exWorkSheetC1.Cells["AB1"].Style.Font.Bold = true;
                                    exWorkSheetC1.Cells["AB1"].Value = "Risk";
                                    exWorkSheetC1.Cells["AB1"].Style.Font.Size = 12;
                                    exWorkSheetC1.Cells["AB1"].AutoFitColumns(25);
                                    exWorkSheetC1.Cells["AB1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheetC1.Cells["AB1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheetC1.Cells["AB1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheetC1.Cells["AB1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                 }   
                                int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                                using (ExcelRange col = exWorkSheetC1.Cells[1, 1, count, 28])
                                {
                                    col.Style.WrapText = true;

                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                }
                            }

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "Closed Notice-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                            //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnMISNew_Click(object sender, EventArgs e)
        {
            try
            {
                //sp_GetMisReports
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MisReport = (entities.Sp_MISResportNew_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                    if (Branchlist.Count > 0)
                    {
                        MisReport = MisReport.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }
                  
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if ( IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        MisReport = MisReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                         
                            MisReport = MisReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticecaseInstanceId))).ToList();
                    
                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            MisReport = MisReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            MisReport = MisReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (MisReport.Count > 0)
                    {
                        MisReport = (from g in MisReport
                                     group g by new
                                     {
                                         g.NoticeType1,
                                         g.Party,
                                         g.Name,
                                         g.Jurisdiction,
                                         g.ClaimAmt,
                                         g.ExpenseTillDate,
                                         g.Owner,
                                         g.Lawfirm,
                                         g.Monetory,
                                         g.FYName,
                                         g.RiskType
                                         //g.Provisionalamt,
                                         //g.BankGurantee,
                                         //g.ProtestMoney,
                                     } into GCS
                                     select new Sp_MISResportNew_Notice_Result()
                                     {
                                         NoticeType1 = GCS.Key.NoticeType1,
                                         Party = GCS.Key.Party,
                                         Name = GCS.Key.Name,
                                         Jurisdiction = GCS.Key.Jurisdiction,
                                         ClaimAmt = GCS.Key.ClaimAmt,
                                         ExpenseTillDate = GCS.Key.ExpenseTillDate,
                                         Owner = GCS.Key.Owner,
                                         Lawfirm = GCS.Key.Lawfirm,
                                         Monetory = GCS.Key.Monetory,
                                         FYName = GCS.Key.FYName,
                                         RiskType = GCS.Key.RiskType
                                         //Provisionalamt=GCS.Key.Provisionalamt,
                                         //BankGurantee=GCS.Key.BankGurantee,
                                         //ProtestMoney=GCS.Key.ProtestMoney,
                                     }).ToList();
                    }
                    //if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    //{
                    //    MisReport = MisReport.Where(entry => entry.Name == tvFilterLocation.SelectedNode.Text).ToList();
                    //}
                    #region MIS
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            DataTable table = new DataTable();
                            table.Columns.Add("NoticeType1", typeof(string));
                            table.Columns.Add("Party", typeof(string));
                            table.Columns.Add("Name", typeof(string));
                            table.Columns.Add("Jurisdiction", typeof(string));
                            table.Columns.Add("ClaimAmt", typeof(string));
                            table.Columns.Add("ExpenseTillDate", typeof(string));
                            table.Columns.Add("Owner", typeof(string));
                            table.Columns.Add("Lawfirm", typeof(string));
                            table.Columns.Add("Monetory", typeof(string));
                            table.Columns.Add("FYName", typeof(string));
                            table.Columns.Add("RiskType", typeof(string));

                            //table.Columns.Add("Provisionalamt", typeof(string));
                            //table.Columns.Add("BankGurantee", typeof(string));
                            //table.Columns.Add("ProtestMoney", typeof(string));
                            foreach (var item in MisReport)
                            {
                                table.Rows.Add(item.NoticeType1, item.Party, item.Name, item.Jurisdiction, item.ClaimAmt, item.ExpenseTillDate, item.Owner, item.Lawfirm, item.Monetory, item.FYName,item.RiskType);
                            }

                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MIS");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            if(RiskType == true)
                            {
                                ExcelData = view.ToTable("Selected", false, "NoticeType1", "Party", "Name", "Jurisdiction", "ClaimAmt", "ExpenseTillDate", "Owner", "Lawfirm", "Monetory", "FYName", "RiskType");
                            }
                            else
                            {
                                ExcelData = view.ToTable("Selected", false, "NoticeType1", "Party", "Name", "Jurisdiction", "ClaimAmt", "ExpenseTillDate", "Owner", "Lawfirm", "Monetory", "FYName");
                            }

                            exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet1.Cells["A1"].Value = "Type of Notice";
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["B1"].Value = "Opponent";
                            exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["B1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["C1"].Value = "Entity/Location/Branch";
                            exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["D1"].Value = "Jurisdiction";
                            exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D1"].AutoFitColumns(15);
                            exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["E1"].Value = "Claim Amount";
                            exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E1"].AutoFitColumns(15);
                            exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F1"].Value = "Expense Till Date";
                            exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F1"].AutoFitColumns(35);
                            exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G1"].Value = "Owner";
                            exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H1"].Value = "Law Firm";
                            exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I1"].Value = "Monetory";
                            exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            if (FYCustID == true)
                            {
                                exWorkSheet1.Cells["J1"].Value = "Disputed Financial Year";
                            }
                            else
                            {
                                exWorkSheet1.Cells["J1"].Value = "Financial Year";
                            }
                            exWorkSheet1.Cells["J1"].Style.Font.Bold = true; 
                            exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            if (RiskType == true)
                            {
                                exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["K1"].Value = "Risk";
                                exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["K1"].AutoFitColumns(25);
                                exWorkSheet1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                             
                            }               
                            //exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["L1"].Value = "Bank Gurantee";
                            //exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
                            //exWorkSheet1.Cells["L1"].AutoFitColumns(25);
                            //exWorkSheet1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            //exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M1"].Value = "Protest Money";
                            //exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
                            //exWorkSheet1.Cells["M1"].AutoFitColumns(25);
                            //exWorkSheet1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 11])
                            {
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            }

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "Notice MIS-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                            //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                            
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnLawyerPerformance_Click(object sender, EventArgs e)
        {
            try
            {
                //sp_GetLawyerPerformanceReport_Notice0
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var LawyerPerformanceReport = (entities.sp_GetLawyerPerformanceReport_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                    
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if ( IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        LawyerPerformanceReport = LawyerPerformanceReport.Where(entry => entry.RoleID == 3).ToList();
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                           
                            LawyerPerformanceReport = LawyerPerformanceReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticecaseInstanceId))).ToList();
                         
                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            LawyerPerformanceReport = LawyerPerformanceReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            LawyerPerformanceReport = LawyerPerformanceReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (LawyerPerformanceReport.Count > 0)
                    {
                        LawyerPerformanceReport = (from g in LawyerPerformanceReport
                                                   group g by new
                                                   {
                                                       g.LawyerName,
                                                       g.Firm_Name,
                                                       g.NoticeHandled,
                                                       g.WIN,
                                                       g.LOSS,
                                                       g.In_Progress,
                                                       g.Settled,
                                                       g.Withdrawn,
                                                       g.Rating
                                                   } into GCS
                                                   select new sp_GetLawyerPerformanceReport_Notice_Result()
                                                   {
                                                       LawyerName = GCS.Key.LawyerName,
                                                       Firm_Name = GCS.Key.Firm_Name,
                                                       NoticeHandled = GCS.Key.NoticeHandled,
                                                       WIN = GCS.Key.WIN,
                                                       LOSS = GCS.Key.LOSS,
                                                       In_Progress = GCS.Key.In_Progress,
                                                       Settled = GCS.Key.Settled,
                                                       Withdrawn = GCS.Key.Withdrawn,
                                                       Rating = GCS.Key.Rating
                                                   }).ToList();
                    }

                    var CriteriyawiseReport = (entities.Sp_LitigationCriteriyaReport_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                    if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                        CriteriyawiseReport = CriteriyawiseReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        CriteriyawiseReport = CriteriyawiseReport.Where(entry => entry.RoleID == 3).ToList();
                    }
                    if (CriteriyawiseReport.Count > 0)
                    {
                        CriteriyawiseReport = (from g in CriteriyawiseReport
                                               group g by new
                                               {
                                                   g.RefNo,
                                                   g.NoticeTitle,
                                                   g.UserName,
                                                   g.LawyerName,
                                                   g.CriteriaName,
                                                   g.Rating
                                               } into GCS
                                               select new Sp_LitigationCriteriyaReport_Notice_Result()
                                               {
                                                   RefNo = GCS.Key.RefNo,
                                                   NoticeTitle = GCS.Key.NoticeTitle,
                                                   UserName = GCS.Key.UserName,
                                                   LawyerName = GCS.Key.LawyerName,
                                                   CriteriaName = GCS.Key.CriteriaName,
                                                   Rating = GCS.Key.Rating
                                               }).ToList();
                    }
                    #region excel report
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            #region Lawyer Performance Report
                            ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("Lawyers Performance Report ");
                            DataView view = new System.Data.DataView(LawyerPerformanceReport.ToDataTable());
                            DataTable ExcelData = null;
                            ExcelData = view.ToTable("Selected", false, "LawyerName", "Firm_Name", "NoticeHandled", "WIN", "LOSS", "In_Progress", "Settled", "Withdrawn", "Rating");

                            exWorkSheet2.Cells["A1"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet2.Cells["A1"].Value = "Name of Lawyer";
                            exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["A1"].AutoFitColumns(25);
                            exWorkSheet2.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet2.Cells["B1"].Value = "Firm Name";
                            exWorkSheet2.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["B1"].AutoFitColumns(25);
                            exWorkSheet2.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet2.Cells["C1"].Value = "No of Notice Handled";
                            exWorkSheet2.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["C1"].AutoFitColumns(10);
                            exWorkSheet2.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet2.Cells["D1"].Value = "Won";
                            exWorkSheet2.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["D1"].AutoFitColumns(10);
                            exWorkSheet2.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet2.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["E1"].AutoFitColumns(10);
                            exWorkSheet2.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["E1"].Value = "Lost";
                            exWorkSheet2.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet2.Cells["F1"].Value = "In Progress";
                            exWorkSheet2.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["F1"].AutoFitColumns(10);
                            exWorkSheet2.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet2.Cells["G1"].Value = "Settled";
                            exWorkSheet2.Cells["G1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["G1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["G1"].AutoFitColumns(10);
                            exWorkSheet2.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet2.Cells["H1"].Value = "Withdrawn";
                            exWorkSheet2.Cells["H1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["H1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["H1"].AutoFitColumns(10);
                            exWorkSheet2.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            exWorkSheet2.Cells["I1"].Value = "Rating";
                            exWorkSheet2.Cells["I1"].Style.Font.Bold = true;
                            exWorkSheet2.Cells["I1"].Style.Font.Size = 12;
                            exWorkSheet2.Cells["I1"].AutoFitColumns(10);
                            exWorkSheet2.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet2.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            using (ExcelRange col = exWorkSheet2.Cells[1, 1, count, 9])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            }
                            #endregion

                            #region Lawyer Performance Report
                            ExcelWorksheet exWorkSheett3 = exportPackge.Workbook.Worksheets.Add("Lawyers Performance Detail");
                            DataView view1 = new System.Data.DataView(CriteriyawiseReport.ToDataTable());
                            DataTable ExcelData1 = null;
                            ExcelData1 = view1.ToTable("Selected", false, "RefNo", "NoticeTitle", "UserName", "LawyerName", "CriteriaName", "Rating");

                            exWorkSheett3.Cells["A1"].LoadFromDataTable(ExcelData1, true);
                            exWorkSheett3.Cells["A1"].Value = "Notice No";
                            exWorkSheett3.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["A1"].AutoFitColumns(25);
                            exWorkSheett3.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["B1"].Value = "Notice Title";
                            exWorkSheett3.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["B1"].AutoFitColumns(25);
                            exWorkSheett3.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["C1"].Value = "User";
                            exWorkSheett3.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["C1"].AutoFitColumns(10);
                            exWorkSheett3.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["D1"].Value = "Lawyer";
                            exWorkSheett3.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["D1"].AutoFitColumns(10);
                            exWorkSheett3.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["E1"].AutoFitColumns(10);
                            exWorkSheett3.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["E1"].Value = "Criteria";
                            exWorkSheett3.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["F1"].Value = "Rating";
                            exWorkSheett3.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["F1"].AutoFitColumns(10);
                            exWorkSheett3.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            int count1 = Convert.ToInt32(ExcelData1.Rows.Count) + 1;
                            using (ExcelRange col = exWorkSheett3.Cells[1, 1, count1, 6])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            }
                            #endregion

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "LawyerPerformance-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                                                                                                                             //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }

                    #endregion

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnBudgetVsExpenseTracking_Click(object sender, EventArgs e)
        {
            try
            {
                ////Name	Schema	Create Date	Policy Health State	Owner
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var ExpensevsbudgetReport = (entities.sp_GetNoticeBudgetVsExpense(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                    if (Branchlist.Count > 0)
                    {
                        ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => Branchlist.Contains(entry.customerbranchId)).ToList();
                    }

                    
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if ( IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                         
                            ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                       
                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (ExpensevsbudgetReport.Count > 0)
                    {
                        ExpensevsbudgetReport = (from g in ExpensevsbudgetReport
                                                 group g by new
                                                 {
                                                     g.RefNo,
                                                     g.PaymentType,
                                                     g.BudgetedAmount,
                                                     g.Amount,
                                                     g.Remark,
                                                     g.Alert,
                                                     //g.FYName,
                                                     //g.Provisionalamt,
                                                     //g.BankGurantee,
                                                     //g.ProtestMoney,
                                                 } into GCS
                                                 select new sp_GetNoticeBudgetVsExpense_Result()
                                                 {
                                                     RefNo = GCS.Key.RefNo,
                                                     PaymentType = GCS.Key.PaymentType,
                                                     BudgetedAmount = GCS.Key.BudgetedAmount,
                                                     Amount = GCS.Key.Amount,
                                                     Remark = GCS.Key.Remark,
                                                     Alert = GCS.Key.Alert,
                                                     //FYName=GCS.Key.FYName,
                                                     //Provisionalamt=GCS.Key.Provisionalamt,
                                                     //BankGurantee=GCS.Key.BankGurantee,
                                                     //ProtestMoney=GCS.Key.ProtestMoney,
                                                 }).ToList();
                    }

                    #region excel report
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("Budget Vs Expense Tracking");
                            DataView view = new System.Data.DataView(ExpensevsbudgetReport.ToDataTable());
                            DataTable ExcelData = null;
                            ExcelData = view.ToTable("Selected", false, "RefNo", "PaymentType", "BudgetedAmount", "Amount", "Remark", "Alert");

                            #region Lawyer Performance Report
                            exWorkSheet3.Cells["A1"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet3.Cells["A1"].Value = "Notice No.";
                            exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["A1"].AutoFitColumns(25);
                            exWorkSheet3.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet3.Cells["B1"].Value = "Payment Type (Court Fee/ Lawyer Fee/ Miscellaneous/ Other Expenses)";
                            exWorkSheet3.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["B1"].AutoFitColumns(25);
                            exWorkSheet3.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet3.Cells["C1"].Value = "Estimated Budgeted Exp";
                            exWorkSheet3.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["C1"].AutoFitColumns(25);
                            exWorkSheet3.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet3.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["D1"].AutoFitColumns(20);
                            exWorkSheet3.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["D1"].Value = "Amount";
                            exWorkSheet3.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet3.Cells["E1"].Value = "Remark";
                            exWorkSheet3.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["E1"].AutoFitColumns(25);
                            exWorkSheet3.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet3.Cells["F1"].Value = "Alert for Exceeding from Budget";
                            exWorkSheet3.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["F1"].AutoFitColumns(10);
                            exWorkSheet3.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            //exWorkSheet3.Cells["G1"].Value = "Financial Year";
                            //exWorkSheet3.Cells["G1"].Style.Font.Bold = true;
                            //exWorkSheet3.Cells["G1"].Style.Font.Size = 12;
                            //exWorkSheet3.Cells["G1"].AutoFitColumns(10);
                            //exWorkSheet3.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet3.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet3.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet3.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            //exWorkSheet3.Cells["H1"].Value = "Provisional amount";
                            //exWorkSheet3.Cells["H1"].Style.Font.Bold = true;
                            //exWorkSheet3.Cells["H1"].Style.Font.Size = 12;
                            //exWorkSheet3.Cells["H1"].AutoFitColumns(10);
                            //exWorkSheet3.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet3.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet3.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet3.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            //exWorkSheet3.Cells["I1"].Value = "Bank Gurantee";
                            //exWorkSheet3.Cells["I1"].Style.Font.Bold = true;
                            //exWorkSheet3.Cells["I1"].Style.Font.Size = 12;
                            //exWorkSheet3.Cells["I1"].AutoFitColumns(10);
                            //exWorkSheet3.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet3.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet3.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet3.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            //exWorkSheet3.Cells["J1"].Value = "Protest Amount";
                            //exWorkSheet3.Cells["J1"].Style.Font.Bold = true;
                            //exWorkSheet3.Cells["J1"].Style.Font.Size = 12;
                            //exWorkSheet3.Cells["J1"].AutoFitColumns(10);
                            //exWorkSheet3.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet3.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet3.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet3.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            #endregion

                            int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            using (ExcelRange col = exWorkSheet3.Cells[1, 1, count, 6])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            }


                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "Notice BudgetVsExpense-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                                                                                                                             //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnLawyerDetails_Click(object sender, EventArgs e)
        {
            try
            {
                //sp_GetLawyerDetails_Notice
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var LawyerDetailsReport = (entities.sp_GetLawyerDetails_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                   
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if ( IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        LawyerDetailsReport = LawyerDetailsReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);

                            LawyerDetailsReport = LawyerDetailsReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();

                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            LawyerDetailsReport = LawyerDetailsReport.Where(entry => (entry.AssignedUser == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            LawyerDetailsReport = LawyerDetailsReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (LawyerDetailsReport.Count > 0)
                    {
                        LawyerDetailsReport = (from g in LawyerDetailsReport
                                               group g by new
                                               {
                                                   g.LawyerName,
                                                   g.Address,
                                                   g.ContactNumber,
                                                   g.Email,
                                                   g.Specialisation,
                                                   g.Rating
                                               } into GCS
                                               select new sp_GetLawyerDetails_Notice_Result()
                                               {
                                                   LawyerName = GCS.Key.LawyerName,
                                                   Address = GCS.Key.Address,
                                                   ContactNumber = GCS.Key.ContactNumber,
                                                   Email = GCS.Key.Email,
                                                   Specialisation = GCS.Key.Specialisation,
                                                   Rating = GCS.Key.Rating
                                               }).ToList();
                    }

                    #region excel report
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            ExcelWorksheet exWorkSheet4 = exportPackge.Workbook.Worksheets.Add("Lawyers Details");
                            DataView view = new System.Data.DataView(LawyerDetailsReport.ToDataTable());
                            DataTable ExcelData = null;
                            ExcelData = view.ToTable("Selected", false, "SLNO", "LawyerName", "Address", "ContactNumber", "Email", "Specialisation", "Rating");

                            int Count = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                // Count++;
                                Count++;
                                item["SLNO"] = Count;

                                #region Lawyer Performance Report
                                exWorkSheet4.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                exWorkSheet4.Cells["A1"].Value = "Sr No.";
                                exWorkSheet4.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet4.Cells["A1"].Style.Font.Size = 12;
                                exWorkSheet4.Cells["A1"].AutoFitColumns(10);
                                exWorkSheet4.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet4.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet4.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet4.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet4.Cells["B1"].Value = "Name of Lawyer";
                                exWorkSheet4.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheet4.Cells["B1"].Style.Font.Size = 12;
                                exWorkSheet4.Cells["B1"].AutoFitColumns(30);
                                exWorkSheet4.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet4.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet4.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet4.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet4.Cells["C1"].Value = "Address";
                                exWorkSheet4.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheet4.Cells["C1"].Style.Font.Size = 12;
                                exWorkSheet4.Cells["C1"].AutoFitColumns(30);
                                exWorkSheet4.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet4.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet4.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet4.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet4.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheet4.Cells["D1"].Style.Font.Size = 12;
                                exWorkSheet4.Cells["D1"].AutoFitColumns(10);
                                exWorkSheet4.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet4.Cells["D1"].Value = "Contact No";
                                exWorkSheet4.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet4.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet4.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet4.Cells["E1"].Value = "EMail";
                                exWorkSheet4.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheet4.Cells["E1"].Style.Font.Size = 12;
                                exWorkSheet4.Cells["E1"].AutoFitColumns(30);
                                exWorkSheet4.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet4.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet4.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet4.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet4.Cells["F1"].Value = "Specialization";
                                exWorkSheet4.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheet4.Cells["F1"].Style.Font.Size = 12;
                                exWorkSheet4.Cells["F1"].AutoFitColumns(10);
                                exWorkSheet4.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet4.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet4.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet4.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                                                                                                                                 //Rating
                                exWorkSheet4.Cells["G1"].Value = "Rating";
                                exWorkSheet4.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheet4.Cells["G1"].Style.Font.Size = 12;
                                exWorkSheet4.Cells["G1"].AutoFitColumns(10);
                                exWorkSheet4.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet4.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet4.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet4.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                #endregion
                            }
                            int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            using (ExcelRange col = exWorkSheet4.Cells[1, 1, count, 7])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            }

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "LawyerDetails-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                                                                                                                             //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnNoticepayments_Click(object sender, EventArgs e)
        {
            try
            {
                //sp_GetCasePaymentLog_Notice
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var NoticePaymentsReport = (entities.sp_GetNoticePaymentLog(Convert.ToInt32(AuthenticationHelper.CustomerID), "N")).ToList();

                    if (Branchlist.Count > 0)
                    {
                        NoticePaymentsReport = NoticePaymentsReport.Where(entry => Branchlist.Contains(entry.customerBranchId)).ToList();
                    }

                    
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if ( IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        NoticePaymentsReport = NoticePaymentsReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);

                            NoticePaymentsReport = NoticePaymentsReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                            
                        }
                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            NoticePaymentsReport = NoticePaymentsReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            NoticePaymentsReport = NoticePaymentsReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (NoticePaymentsReport.Count > 0)
                    {
                        NoticePaymentsReport = (from g in NoticePaymentsReport
                                                group g by new
                                                {
                                                    g.RefNo,
                                                    g.InvoiceNo,
                                                    g.PaymentType,
                                                    g.PaymentDate,
                                                    g.Amount,
                                                    g.Remark,
                                                    g.Lawyer,
                                                    g.AmountTax ,
                                                    g.CBU,
                                                    g.Zone,
                                                    g.Region,
                                                    g.Territory,
                                                    g.CaseCreator,
                                                    g.EmailId,
                                                    g.ContactNumber

                                                    //g.FYName,
                                                    //g.Provisionalamt,
                                                    //g.BankGurantee,
                                                    //g.ProtestMoney,

                                                } into GCS
                                                select new sp_GetNoticePaymentLog_Result()
                                                {
                                                    RefNo = GCS.Key.RefNo,
                                                    InvoiceNo = GCS.Key.InvoiceNo,
                                                    PaymentType = GCS.Key.PaymentType,
                                                    PaymentDate = GCS.Key.PaymentDate,
                                                    Amount = GCS.Key.Amount,
                                                    Remark = GCS.Key.Remark,
                                                    Lawyer = GCS.Key.Lawyer,
                                                    AmountTax = GCS.Key.AmountTax,
                                                    CBU = GCS.Key.CBU,
                                                    Zone = GCS.Key.Zone,
                                                    Region = GCS.Key.Region,
                                                    Territory = GCS.Key.Territory,
                                                    CaseCreator = GCS.Key.CaseCreator,
                                                    EmailId = GCS.Key.EmailId,
                                                    ContactNumber = GCS.Key.ContactNumber
                                                    //FYName=GCS.Key.FYName,
                                                    //Provisionalamt=GCS.Key.Provisionalamt,
                                                    //BankGurantee=GCS.Key.BankGurantee,
                                                    //ProtestMoney=GCS.Key.ProtestMoney,

                                                }).ToList();
                    }

                    #region excel report
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            DataTable table = new DataTable();
                            table.Columns.Add("RefNo", typeof(string));
                            table.Columns.Add("PaymentInvoiceNo", typeof(string));
                            table.Columns.Add("PaymentType", typeof(string));
                            table.Columns.Add("PaymentDate", typeof(string));
                            table.Columns.Add("Amount", typeof(string));
                            table.Columns.Add("AmountTax", typeof(string));
                            table.Columns.Add("Remark", typeof(string));
                            table.Columns.Add("Lawyer", typeof(string));
                            if (NewColumnsLitigation == true)
                            {
                                table.Columns.Add("CBU", typeof(string));
                                table.Columns.Add("Zone", typeof(string));
                                table.Columns.Add("Region", typeof(string));
                                table.Columns.Add("Territory", typeof(string));
                                table.Columns.Add("CaseCreator", typeof(string));
                                table.Columns.Add("EmailId", typeof(string));
                                table.Columns.Add("ContactNumber", typeof(string));
                            }

                            //table.Columns.Add("FYName", typeof(string));
                            //table.Columns.Add("Provisionalamt", typeof(string));
                            //table.Columns.Add("BankGurantee", typeof(string));
                            //table.Columns.Add("ProtestMoney", typeof(string));
                            if (NewColumnsLitigation == true)
                            {
                                foreach (var item in NoticePaymentsReport)
                                {
                                    table.Rows.Add(item.RefNo, item.InvoiceNo, item.PaymentType, item.PaymentDate, item.Amount, item.AmountTax, item.Remark, item.Lawyer, item.CBU, item.Zone, item.Region, item.Territory, item.CaseCreator, item.EmailId, item.ContactNumber);

                                }
                            }
                            else
                            {
                                foreach (var item in NoticePaymentsReport)
                                {
                                    table.Rows.Add(item.RefNo, item.InvoiceNo, item.PaymentType, item.PaymentDate, item.Amount, item.AmountTax, item.Remark, item.Lawyer);

                                }
                            }

                              

                            ExcelWorksheet exWorkSheet5 = exportPackge.Workbook.Worksheets.Add("Notice Payments  Data");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            if (NewColumnsLitigation == true)
                            {
                                ExcelData = view.ToTable("Selected", false, "RefNo", "PaymentInvoiceNo", "PaymentType", "PaymentDate", "Amount", "AmountTax", "Remark", "Lawyer", "CBU", "Zone", "Region", "Territory", "CaseCreator", "EmailId", "ContactNumber");

                            }
                            else
                            {
                                ExcelData = view.ToTable("Selected", false, "RefNo", "PaymentInvoiceNo", "PaymentType", "PaymentDate", "Amount", "AmountTax", "Remark", "Lawyer");

                            }

                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["PaymentDate"])))
                                {
                                    item["PaymentDate"] = Convert.ToDateTime(item["PaymentDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            #region Lawyer Performance Report
                            exWorkSheet5.Cells["A1"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet5.Cells["A1"].Value = "Notice No.";
                            exWorkSheet5.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet5.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheet5.Cells["A1"].AutoFitColumns(25);
                            exWorkSheet5.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet5.Cells["B1"].Value = "Invoice No";
                            exWorkSheet5.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet5.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheet5.Cells["B1"].AutoFitColumns(25);
                            exWorkSheet5.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet5.Cells["C1"].Value = "Payment Type (Court Fee/ Lawyer Fee/ Miscellaneous/ Other Expenses)";
                            exWorkSheet5.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet5.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheet5.Cells["C1"].AutoFitColumns(10);
                            exWorkSheet5.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet5.Cells["D1"].Value = "Payment Date";
                            exWorkSheet5.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet5.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheet5.Cells["D1"].AutoFitColumns(10);
                            exWorkSheet5.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet5.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet5.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheet5.Cells["E1"].AutoFitColumns(20);
                            exWorkSheet5.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells["E1"].Value = "Amount excluding tax";
                            exWorkSheet5.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet5.Cells["F1"].Value = "Tax Amount";
                            exWorkSheet5.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet5.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheet5.Cells["F1"].AutoFitColumns(25);
                            exWorkSheet5.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            
                            exWorkSheet5.Cells["G1"].Value = "Remark";
                            exWorkSheet5.Cells["G1"].Style.Font.Bold = true;
                            exWorkSheet5.Cells["G1"].Style.Font.Size = 12;
                            exWorkSheet5.Cells["G1"].AutoFitColumns(25);
                            exWorkSheet5.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet5.Cells["H1"].Value = "Lawyer";
                            exWorkSheet5.Cells["H1"].Style.Font.Bold = true;
                            exWorkSheet5.Cells["H1"].Style.Font.Size = 12;
                            exWorkSheet5.Cells["H1"].AutoFitColumns(25);
                            exWorkSheet5.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            if (NewColumnsLitigation == true)
                            {

                                exWorkSheet5.Cells["I1"].Value = "CBU";
                                exWorkSheet5.Cells["I1"].Style.Font.Bold = true;
                                exWorkSheet5.Cells["I1"].Style.Font.Size = 12;
                                exWorkSheet5.Cells["I1"].AutoFitColumns(25);
                                exWorkSheet5.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheet5.Cells["J1"].Value = "Zone";
                                exWorkSheet5.Cells["J1"].Style.Font.Bold = true;
                                exWorkSheet5.Cells["J1"].Style.Font.Size = 12;
                                exWorkSheet5.Cells["J1"].AutoFitColumns(25);
                                exWorkSheet5.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet5.Cells["K1"].Value = "Region";
                                exWorkSheet5.Cells["K1"].Style.Font.Bold = true;
                                exWorkSheet5.Cells["K1"].Style.Font.Size = 12;
                                exWorkSheet5.Cells["K1"].AutoFitColumns(25);
                                exWorkSheet5.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet5.Cells["L1"].Value = "Territory";
                                exWorkSheet5.Cells["L1"].Style.Font.Bold = true;
                                exWorkSheet5.Cells["L1"].Style.Font.Size = 12;
                                exWorkSheet5.Cells["L1"].AutoFitColumns(25);
                                exWorkSheet5.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet5.Cells["M1"].Value = "Notice Creator";
                                exWorkSheet5.Cells["M1"].Style.Font.Bold = true;
                                exWorkSheet5.Cells["M1"].Style.Font.Size = 12;
                                exWorkSheet5.Cells["M1"].AutoFitColumns(25);
                                exWorkSheet5.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheet5.Cells["N1"].Value = "Email ID";
                                exWorkSheet5.Cells["N1"].Style.Font.Bold = true;
                                exWorkSheet5.Cells["N1"].Style.Font.Size = 12;
                                exWorkSheet5.Cells["N1"].AutoFitColumns(25);
                                exWorkSheet5.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheet5.Cells["O1"].Value = "Contact Number";
                                exWorkSheet5.Cells["O1"].Style.Font.Bold = true;
                                exWorkSheet5.Cells["O1"].Style.Font.Size = 12;
                                exWorkSheet5.Cells["O1"].AutoFitColumns(25);
                                exWorkSheet5.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663






                            }

                            //exWorkSheet5.Cells["H1"].Value = "Financial Year";
                            //exWorkSheet5.Cells["H1"].Style.Font.Bold = true;
                            //exWorkSheet5.Cells["H1"].Style.Font.Size = 12;
                            //exWorkSheet5.Cells["H1"].AutoFitColumns(25);
                            //exWorkSheet5.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet5.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet5.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet5.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            //exWorkSheet5.Cells["I1"].Value = "Provisional amount";
                            //exWorkSheet5.Cells["I1"].Style.Font.Bold = true;
                            //exWorkSheet5.Cells["I1"].Style.Font.Size = 12;
                            //exWorkSheet5.Cells["I1"].AutoFitColumns(25);
                            //exWorkSheet5.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet5.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet5.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet5.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            //exWorkSheet5.Cells["J1"].Value = "Bank Gurantee";
                            //exWorkSheet5.Cells["J1"].Style.Font.Bold = true;
                            //exWorkSheet5.Cells["J1"].Style.Font.Size = 12;
                            //exWorkSheet5.Cells["J1"].AutoFitColumns(25);
                            //exWorkSheet5.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet5.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet5.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet5.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            //exWorkSheet5.Cells["K1"].Value = "Protest Money";
                            //exWorkSheet5.Cells["K1"].Style.Font.Bold = true;
                            //exWorkSheet5.Cells["K1"].Style.Font.Size = 12;
                            //exWorkSheet5.Cells["K1"].AutoFitColumns(25);
                            //exWorkSheet5.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //exWorkSheet5.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet5.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet5.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            #endregion

                            int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            using (ExcelRange col = exWorkSheet5.Cells[1, 1, count, 15])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            }

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "NoticePayments-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //Hearing Report Hearing Date
        protected void btnNoticeResponse_Click(object sender, EventArgs e)
        {
            try
            {
                //sp_GetNoticeResponse
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var NoticeResponseReport = (entities.sp_GetNoticeResponse(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                    if (Branchlist.Count > 0)
                    {
                        NoticeResponseReport = NoticeResponseReport.Where(entry => Branchlist.Contains(entry.customerBranchId)).ToList();
                    }
                    ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        NoticeResponseReport = NoticeResponseReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);

                            NoticeResponseReport = NoticeResponseReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();

                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            NoticeResponseReport = NoticeResponseReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            NoticeResponseReport = NoticeResponseReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (NoticeResponseReport.Count > 0)
                    {
                        NoticeResponseReport = (from g in NoticeResponseReport
                                                group g by new
                                                {
                                                    g.RefNo,
                                                    g.NoticedueDate,
                                                    g.ResponseRefNo,
                                                    g.Description,
                                                    g.Remark,
                                                    g.ResponseDocument
                                                } into GCS
                                                select new sp_GetNoticeResponse_Result()
                                                {
                                                    RefNo = GCS.Key.RefNo,
                                                    NoticedueDate = GCS.Key.NoticedueDate,
                                                    ResponseRefNo = GCS.Key.ResponseRefNo,
                                                    Description = GCS.Key.Description,
                                                    Remark = GCS.Key.Remark,
                                                    ResponseDocument = GCS.Key.ResponseDocument,
                                                }).ToList();
                    }

                    if (!string.IsNullOrEmpty(txtFromDate.Text))
                    {
                        NoticeResponseReport = NoticeResponseReport.Where(entry => entry.NoticedueDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
                    }
                    if (!string.IsNullOrEmpty(txtToDate.Text))
                    {
                        NoticeResponseReport = NoticeResponseReport.Where(entry => entry.NoticedueDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
                    }

                    NoticeResponseReport = NoticeResponseReport.OrderBy(entry => entry.RefNo).ThenByDescending(row => row.NoticedueDate).ToList();

                    #region excel report
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            DataTable table = new DataTable();
                            table.Columns.Add("RefNo", typeof(string));
                            table.Columns.Add("NoticedueDate", typeof(string));
                            table.Columns.Add("ResponseRefNo", typeof(string));
                            table.Columns.Add("Description", typeof(string));
                            table.Columns.Add("Remark", typeof(string));
                            table.Columns.Add("ResponseDocument", typeof(string));

                            foreach (var item in NoticeResponseReport)
                            {
                                table.Rows.Add(item.RefNo, item.NoticedueDate, item.ResponseRefNo, item.Description, item.Remark, item.ResponseDocument);
                            }

                            ExcelWorksheet exWorkSheet6 = exportPackge.Workbook.Worksheets.Add("Notice Response");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;
                            ExcelData = view.ToTable("Selected", false, "RefNo", "NoticedueDate", "ResponseRefNo", "Description", "Remark", "ResponseDocument");

                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["NoticedueDate"])))
                                {
                                    item["NoticedueDate"] = Convert.ToDateTime(item["NoticedueDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            #region Lawyer Performance Report
                            exWorkSheet6.Cells["A1"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet6.Cells["A1"].Value = "Notice No.";
                            exWorkSheet6.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet6.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheet6.Cells["A1"].AutoFitColumns(20);
                            exWorkSheet6.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet6.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet6.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet6.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet6.Cells["B1"].Value = "Response Due Date";
                            exWorkSheet6.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet6.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheet6.Cells["B1"].AutoFitColumns(10);
                            exWorkSheet6.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet6.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet6.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet6.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet6.Cells["C1"].Value = "Response Ref No";
                            exWorkSheet6.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet6.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheet6.Cells["C1"].AutoFitColumns(25);
                            exWorkSheet6.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet6.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet6.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet6.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet6.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet6.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheet6.Cells["D1"].AutoFitColumns(50);
                            exWorkSheet6.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet6.Cells["D1"].Value = "Response Deascription";
                            exWorkSheet6.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet6.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet6.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet6.Cells["E1"].Value = "Remark";
                            exWorkSheet6.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet6.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheet6.Cells["E1"].AutoFitColumns(50);
                            exWorkSheet6.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet6.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet6.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet6.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet6.Cells["F1"].Value = "Response Documents";
                            exWorkSheet6.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet6.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheet6.Cells["F1"].AutoFitColumns(50);
                            exWorkSheet6.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet6.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet6.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet6.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            #endregion

                            int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            using (ExcelRange col = exWorkSheet6.Cells[1, 1, count, 6])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            }


                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "Notice Response-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                                                                                                                             //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }


                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //Mis Report filter brach and Open date

        protected void btnAll_Click(object sender, EventArgs e)
        {
            try
            {
                // filter Mis ,NoticeResponseReport,CourtCasesReport,CaseOrderReport
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MisReport = (entities.sp_GetMisReports_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                    if (Branchlist.Count > 0)
                    {
                        MisReport = MisReport.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        MisReport = MisReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);

                            MisReport = MisReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticecaseInstanceId))).ToList();

                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            MisReport = MisReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            MisReport = MisReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (MisReport.Count > 0)
                    {
                        MisReport = (from g in MisReport
                                     group g by new
                                     {
                                         g.NoticeType,
                                         g.NoticeDate,
                                         g.RefNo,
                                         g.NoticeTitle,
                                         g.NoticeTerm,
                                         g.CaseType1,
                                         g.Party,
                                         g.NoticeDetailDesc,
                                         g.Name,
                                         g.Jurisdiction,
                                         g.ClaimAmt,
                                         g.ResponseDate,
                                         g.Responsedescription,
                                         g.ResponseRemark,
                                         g.NoticeDueDate,
                                         g.ExpenseTillDate,
                                         g.Owner,
                                         g.Lawfirm,
                                         g.State,
                                         g.FYName,
                                         g.Monetory,
                                         g.Provisionalamt,
                                         g.BankGurantee,
                                         g.ProtestMoney,
                                         g.ProbableAmt,
                                         g.AssignedUser,
                                         g.RecoveryAmount,
                                         g.CBU,
                                         g.Zone,
                                         g.Region,
                                         g.Territory,
                                         g.CaseCreator,
                                         g.EmailId,
                                         g.ContactNumber,
                                         g.RiskType
                                     } into GCS
                                     select new sp_GetMisReports_Notice_Result()
                                     {
                                         NoticeType = GCS.Key.NoticeType,
                                         NoticeDate = GCS.Key.NoticeDate,
                                         RefNo = GCS.Key.RefNo,
                                         NoticeTitle = GCS.Key.NoticeTitle,
                                         NoticeTerm = GCS.Key.NoticeTerm,
                                         CaseType1 = GCS.Key.CaseType1,
                                         Party = GCS.Key.Party,
                                         NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                         Name = GCS.Key.Name,
                                         Jurisdiction = GCS.Key.Jurisdiction,
                                         ClaimAmt = GCS.Key.ClaimAmt,
                                         ResponseDate = GCS.Key.ResponseDate,
                                         Responsedescription = GCS.Key.Responsedescription,
                                         ResponseRemark = GCS.Key.ResponseRemark,
                                         NoticeDueDate = GCS.Key.NoticeDueDate,
                                         ExpenseTillDate = GCS.Key.ExpenseTillDate,
                                         Owner = GCS.Key.Owner,
                                         Lawfirm = GCS.Key.Lawfirm,
                                         State = GCS.Key.State,
                                         FYName = GCS.Key.FYName,
                                         Monetory = GCS.Key.Monetory,
                                         Provisionalamt = GCS.Key.Provisionalamt,
                                         BankGurantee = GCS.Key.BankGurantee,
                                         ProtestMoney = GCS.Key.ProtestMoney,
                                         ProbableAmt = GCS.Key.ProbableAmt,
                                         AssignedUser = GCS.Key.AssignedUser,
                                         RecoveryAmount = GCS.Key.RecoveryAmount,
                                         CBU = GCS.Key.CBU,
                                         Zone = GCS.Key.Zone,
                                         Region = GCS.Key.Region,
                                         Territory = GCS.Key.Territory,
                                         CaseCreator = GCS.Key.CaseCreator,
                                         EmailId = GCS.Key.EmailId,
                                         ContactNumber = GCS.Key.ContactNumber,
                                         RiskType = GCS.Key.RiskType

                                     }).ToList();
                    }

                    var MisReportClosedNotice = (entities.sp_GetMisReportsClosed_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                    if (Branchlist.Count > 0)
                    {
                        MisReportClosedNotice = MisReportClosedNotice.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();
                    }
                    if (isexist != null)
                    {
                        MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);

                            MisReportClosedNotice = MisReportClosedNotice.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticecaseInstanceId))).ToList();

                        }

                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            MisReportClosedNotice = MisReportClosedNotice.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (MisReportClosedNotice.Count > 0)
                    {
                        MisReportClosedNotice = (from g in MisReportClosedNotice
                                                 group g by new
                                                 {
                                                     g.NoticeType,
                                                     g.NoticeDate,
                                                     g.CloseDate,
                                                     g.RefNo,
                                                     g.NoticeTitle,
                                                     g.NoticeTerm,
                                                     g.CaseType1,
                                                     g.Party,
                                                     g.NoticeDetailDesc,
                                                     g.Name,
                                                     g.Jurisdiction,
                                                     g.ClaimAmt,
                                                     g.ResponseDate,
                                                     g.Responsedescription,
                                                     g.ResponseRemark,
                                                     g.NoticeDueDate,
                                                     g.ExpenseTillDate,
                                                     g.Owner,
                                                     g.Lawfirm,
                                                     g.State,
                                                     g.Monetory,
                                                     g.FYName,
                                                     g.Provisionalamt,
                                                     g.BankGurantee,
                                                     g.ProtestMoney,
                                                     g.RiskType
                                                 } into GCS
                                                 select new sp_GetMisReportsClosed_Notice_Result()
                                                 {
                                                     NoticeType = GCS.Key.NoticeType,
                                                     NoticeDate = GCS.Key.NoticeDate,
                                                     CloseDate = GCS.Key.CloseDate,
                                                     RefNo = GCS.Key.RefNo,
                                                     NoticeTitle = GCS.Key.NoticeTitle,
                                                     NoticeTerm = GCS.Key.NoticeTerm,
                                                     CaseType1 = GCS.Key.CaseType1,
                                                     Party = GCS.Key.Party,
                                                     NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                                     Name = GCS.Key.Name,
                                                     Jurisdiction = GCS.Key.Jurisdiction,
                                                     ClaimAmt = GCS.Key.ClaimAmt,
                                                     ResponseDate = GCS.Key.ResponseDate,
                                                     Responsedescription = GCS.Key.Responsedescription,
                                                     ResponseRemark = GCS.Key.ResponseRemark,
                                                     NoticeDueDate = GCS.Key.NoticeDueDate,
                                                     ExpenseTillDate = GCS.Key.ExpenseTillDate,
                                                     Owner = GCS.Key.Owner,
                                                     Lawfirm = GCS.Key.Lawfirm,
                                                     State = GCS.Key.State,
                                                     Monetory = GCS.Key.Monetory,
                                                     FYName = GCS.Key.FYName,
                                                     Provisionalamt = GCS.Key.Provisionalamt,
                                                     BankGurantee = GCS.Key.BankGurantee,
                                                     ProtestMoney = GCS.Key.ProtestMoney,
                                                      RiskType=GCS.Key.RiskType
                                                 }).ToList();
                    }
                    var LawyerPerformanceReport = (entities.sp_GetLawyerPerformanceReport_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                    if (isexist != null)
                    {
                        LawyerPerformanceReport = LawyerPerformanceReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                            LawyerPerformanceReport = LawyerPerformanceReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticecaseInstanceId))).ToList();
                        }
                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            LawyerPerformanceReport = LawyerPerformanceReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            LawyerPerformanceReport = LawyerPerformanceReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (LawyerPerformanceReport.Count > 0)
                    {
                        LawyerPerformanceReport = (from g in LawyerPerformanceReport
                                                   group g by new
                                                   {
                                                       g.LawyerName,
                                                       g.Firm_Name,
                                                       g.NoticeHandled,
                                                       g.WIN,
                                                       g.LOSS,
                                                       g.In_Progress,
                                                       g.Settled,
                                                       g.Rating,

                                                   } into GCS
                                                   select new sp_GetLawyerPerformanceReport_Notice_Result()
                                                   {
                                                       LawyerName = GCS.Key.LawyerName,
                                                       Firm_Name = GCS.Key.Firm_Name,
                                                       NoticeHandled = GCS.Key.NoticeHandled,
                                                       WIN = GCS.Key.WIN,
                                                       LOSS = GCS.Key.LOSS,
                                                       In_Progress = GCS.Key.In_Progress,
                                                       Settled = GCS.Key.Settled,
                                                       Rating = GCS.Key.Rating,

                                                   }).ToList();
                    }

                    var ExpensevsbudgetReport = (entities.sp_GetNoticeBudgetVsExpense(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                    if (Branchlist.Count > 0)
                    {
                        ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => Branchlist.Contains(entry.customerbranchId)).ToList();
                    }
                    if (isexist != null)
                    {
                        ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                            ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        }
                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            ExpensevsbudgetReport = ExpensevsbudgetReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (ExpensevsbudgetReport.Count > 0)
                    {
                        ExpensevsbudgetReport = (from g in ExpensevsbudgetReport
                                                 group g by new
                                                 {
                                                     g.RefNo,
                                                     g.PaymentType,
                                                     g.BudgetedAmount,
                                                     g.Amount,
                                                     g.Remark,
                                                     g.Alert,
                                                     //g.FYName,
                                                     //g.Provisionalamt,
                                                     //g.BankGurantee,
                                                     //g.ProtestMoney,

                                                 } into GCS
                                                 select new sp_GetNoticeBudgetVsExpense_Result()
                                                 {
                                                     RefNo = GCS.Key.RefNo,
                                                     PaymentType = GCS.Key.PaymentType,
                                                     BudgetedAmount = GCS.Key.BudgetedAmount,
                                                     Amount = GCS.Key.Amount,
                                                     Remark = GCS.Key.Remark,
                                                     Alert = GCS.Key.Alert,
                                                     //FYName = GCS.Key.FYName,
                                                     //Provisionalamt = GCS.Key.Provisionalamt,
                                                     //BankGurantee = GCS.Key.BankGurantee,
                                                     //ProtestMoney = GCS.Key.ProtestMoney,
                                                 }).ToList();
                    }

                    var LawyerDetailsReport = (entities.sp_GetLawyerDetails_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                    if (isexist != null)
                    {
                        LawyerDetailsReport = LawyerDetailsReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                            LawyerDetailsReport = LawyerDetailsReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        }
                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            LawyerDetailsReport = LawyerDetailsReport.Where(entry => (entry.AssignedUser == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            LawyerDetailsReport = LawyerDetailsReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (LawyerDetailsReport.Count > 0)
                    {
                        LawyerDetailsReport = (from g in LawyerDetailsReport
                                               group g by new
                                               {
                                                   g.LawyerName,
                                                   g.Address,
                                                   g.ContactNumber,
                                                   g.Email,
                                                   g.Specialisation,
                                                   g.Rating
                                               } into GCS
                                               select new sp_GetLawyerDetails_Notice_Result()
                                               {
                                                   LawyerName = GCS.Key.LawyerName,
                                                   Address = GCS.Key.Address,
                                                   ContactNumber = GCS.Key.ContactNumber,
                                                   Email = GCS.Key.Email,
                                                   Specialisation = GCS.Key.Specialisation,
                                                   Rating = GCS.Key.Rating
                                               }).ToList();
                    }

                    var NoticePaymentsReport = (entities.sp_GetNoticePaymentLog(Convert.ToInt32(AuthenticationHelper.CustomerID), "N")).ToList();
                    if (Branchlist.Count > 0)
                    {
                        NoticePaymentsReport = NoticePaymentsReport.Where(entry => Branchlist.Contains(entry.customerBranchId)).ToList();
                    }
                    if (isexist != null)
                    {
                        NoticePaymentsReport = NoticePaymentsReport.Where(entry => entry.RoleID == 3).ToList();
                        if (AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                            NoticePaymentsReport = NoticePaymentsReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        }
                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            NoticePaymentsReport = NoticePaymentsReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            NoticePaymentsReport = NoticePaymentsReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (NoticePaymentsReport.Count > 0)
                    {
                        NoticePaymentsReport = (from g in NoticePaymentsReport
                                                group g by new
                                                {
                                                    g.RefNo,
                                                    g.InvoiceNo,
                                                    g.PaymentType,
                                                    g.PaymentDate,
                                                    g.Amount,
                                                    g.Remark,
                                                    g.Lawyer,
                                                    g.AmountTax,
                                                    g.CBU,
                                                    g.Zone,
                                                    g.Region,
                                                    g.Territory,
                                                    g.CaseCreator,
                                                    g.EmailId,
                                                    g.ContactNumber
                                                    //g.FYName,
                                                    //g.Provisionalamt,
                                                    //g.BankGurantee,
                                                    //g.ProtestMoney,
                                                } into GCS
                                                select new sp_GetNoticePaymentLog_Result()
                                                {
                                                    RefNo = GCS.Key.RefNo,
                                                    InvoiceNo = GCS.Key.InvoiceNo,
                                                    PaymentType = GCS.Key.PaymentType,
                                                    PaymentDate = GCS.Key.PaymentDate,
                                                    Amount = GCS.Key.Amount,
                                                    Remark = GCS.Key.Remark,
                                                    Lawyer = GCS.Key.Lawyer,
                                                    AmountTax = GCS.Key.AmountTax,
                                                    CBU = GCS.Key.CBU,
                                                    Zone = GCS.Key.Zone,
                                                    Region = GCS.Key.Region,
                                                    Territory = GCS.Key.Territory,
                                                    CaseCreator = GCS.Key.CaseCreator,
                                                    EmailId = GCS.Key.EmailId,
                                                    ContactNumber = GCS.Key.ContactNumber
                                                    //FYName=GCS.Key.FYName,
                                                    //Provisionalamt=GCS.Key.Provisionalamt,
                                                    //BankGurantee=GCS.Key.BankGurantee,
                                                    //ProtestMoney=GCS.Key.ProtestMoney,
                                                }).ToList();
                    }

                    var NoticeResponseReport = (entities.sp_GetNoticeResponse(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                    if (Branchlist.Count > 0)
                    {
                        NoticeResponseReport = NoticeResponseReport.Where(entry => Branchlist.Contains(entry.customerBranchId)).ToList();
                    }
                    if (isexist != null)
                    {
                        NoticeResponseReport = NoticeResponseReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                            NoticeResponseReport = NoticeResponseReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        }
                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            NoticeResponseReport = NoticeResponseReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            NoticeResponseReport = NoticeResponseReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (NoticeResponseReport.Count > 0)
                    {
                        NoticeResponseReport = (from g in NoticeResponseReport
                                                group g by new
                                                {
                                                    g.RefNo,
                                                    g.NoticedueDate,
                                                    g.ResponseRefNo,
                                                    g.Description,
                                                    g.Remark,
                                                    g.ResponseDocument
                                                } into GCS
                                                select new sp_GetNoticeResponse_Result()
                                                {
                                                    RefNo = GCS.Key.RefNo,
                                                    NoticedueDate = GCS.Key.NoticedueDate,
                                                    ResponseRefNo = GCS.Key.ResponseRefNo,
                                                    Description = GCS.Key.Description,
                                                    Remark = GCS.Key.Remark,
                                                    ResponseDocument = GCS.Key.ResponseDocument,
                                                }).ToList();
                    }
                    var CriteriawiseReport = (entities.Sp_LitigationCriteriyaReport_Notice(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                    if (isexist != null)
                    {
                        CriteriawiseReport = CriteriawiseReport.Where(entry => entry.RoleID == 3).ToList();
                        if ( AuthenticationHelper.Role != "CADMN")
                        {
                            var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.Role, 1);
                            CriteriawiseReport = CriteriawiseReport.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        }
                    }
                    else
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            CriteriawiseReport = CriteriawiseReport.Where(entry => (entry.AssignedUserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            CriteriawiseReport = CriteriawiseReport.Where(entry => entry.RoleID == 3).ToList();
                        }
                    }
                    if (CriteriawiseReport.Count > 0)
                    {
                        CriteriawiseReport = (from g in CriteriawiseReport
                                              group g by new
                                              {
                                                  g.RefNo,
                                                  g.NoticeTitle,
                                                  g.UserName,
                                                  g.LawyerName,
                                                  g.CriteriaName,
                                                  g.Rating
                                              } into GCS
                                              select new Sp_LitigationCriteriyaReport_Notice_Result()
                                              {
                                                  RefNo = GCS.Key.RefNo,
                                                  NoticeTitle = GCS.Key.NoticeTitle,
                                                  UserName = GCS.Key.UserName,
                                                  LawyerName = GCS.Key.LawyerName,
                                                  CriteriaName = GCS.Key.CriteriaName,
                                                  Rating = GCS.Key.Rating
                                              }).ToList();
                    }

                    NoticeResponseReport = NoticeResponseReport.OrderBy(entry => entry.RefNo).ThenByDescending(row => row.NoticedueDate).ToList();

                    //if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    //{
                    //    MisReport = MisReport.Where(entry => entry.Name == tvFilterLocation.SelectedNode.Text).ToList();
                    //}
                    if (!string.IsNullOrEmpty(txtFromDate.Text))
                    {
                        MisReport = MisReport.Where(entry => entry.NoticeDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
                    }
                    if (!string.IsNullOrEmpty(txtToDate.Text))
                    {
                        MisReport = MisReport.Where(entry => entry.NoticeDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
                    }

                    //if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    //{
                    //    MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.Name == tvFilterLocation.SelectedNode.Text).ToList();
                    //}
                    if (!string.IsNullOrEmpty(txtFromDate.Text))
                    {
                        MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.NoticeDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
                    }
                    if (!string.IsNullOrEmpty(txtToDate.Text))
                    {
                        MisReportClosedNotice = MisReportClosedNotice.Where(entry => entry.NoticeDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
                    }

                    if (!string.IsNullOrEmpty(txtFromDate.Text))
                    {
                        NoticeResponseReport = NoticeResponseReport.Where(entry => entry.NoticedueDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
                    }
                    if (!string.IsNullOrEmpty(txtToDate.Text))
                    {
                        NoticeResponseReport = NoticeResponseReport.Where(entry => entry.NoticedueDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
                    }

                    #region excel report
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            #region MIS Old

                            if (MisReport.Count > 0)
                            {
                                {
                                    DataTable table = new DataTable();
                                    table.Columns.Add("SLNO", typeof(string));
                                    table.Columns.Add("NoticeType", typeof(string));
                                    table.Columns.Add("NoticeDate", typeof(string));
                                    table.Columns.Add("RefNo", typeof(string));
                                    table.Columns.Add("NoticeTitle", typeof(string));
                                    table.Columns.Add("NoticeTerm", typeof(string));
                                    table.Columns.Add("CaseType1", typeof(string));
                                    table.Columns.Add("Party", typeof(string));
                                    table.Columns.Add("NoticeDetailDesc", typeof(string));
                                    table.Columns.Add("Name", typeof(string));
                                    table.Columns.Add("Jurisdiction", typeof(string));
                                    table.Columns.Add("ResponseDate", typeof(string));
                                    table.Columns.Add("Responsedescription", typeof(string));
                                    table.Columns.Add("ResponseRemark", typeof(string));
                                    table.Columns.Add("NoticeDueDate", typeof(string));
                                    table.Columns.Add("Owner", typeof(string));
                                    table.Columns.Add("Lawfirm", typeof(string));
                                    table.Columns.Add("AssignedUser", typeof(string));
                                    table.Columns.Add("State", typeof(string));
                                    table.Columns.Add("FYName", typeof(string));
                                    table.Columns.Add("Monetory", typeof(string));
                                    table.Columns.Add("BankGurantee", typeof(string));
                                    table.Columns.Add("RecoveryAmount", typeof(decimal));
                                    table.Columns.Add("ClaimAmt", typeof(decimal));
                                    table.Columns.Add("Provisionalamt", typeof(decimal));
                                    table.Columns.Add("ProtestMoney", typeof(decimal));
                                    table.Columns.Add("ProbableAmt", typeof(decimal));
                                    table.Columns.Add("ExpenseTillDate", typeof(int));
                                    if (NewColumnsLitigation == true)
                                    {
                                        table.Columns.Add("CBU", typeof(string));
                                        table.Columns.Add("Zone", typeof(string));
                                        table.Columns.Add("Region", typeof(string));
                                        table.Columns.Add("Territory", typeof(string));
                                        table.Columns.Add("CaseCreator", typeof(string));
                                        table.Columns.Add("EmailId", typeof(string));
                                        table.Columns.Add("ContactNumber", typeof(string));
                                    }
                                    table.Columns.Add("RiskType", typeof(string));


                                    int Count = 0;

                                    if (NewColumnsLitigation == true)
                                    {
                                        foreach (var item in MisReport)
                                        {
                                            Count++;
                                            table.Rows.Add(Count, item.NoticeType, item.NoticeDate, item.RefNo, item.NoticeTitle, item.NoticeTerm, item.CaseType1,
                                                item.Party.TrimEnd(','), item.NoticeDetailDesc, item.Name, item.Jurisdiction, item.ResponseDate,
                                                item.Responsedescription, item.ResponseRemark, item.NoticeDueDate,
                                                item.Owner, item.Lawfirm.TrimEnd(','), item.AssignedUser.TrimEnd(','), item.State, item.FYName.TrimEnd(','),
                                                item.Monetory, item.BankGurantee, item.RecoveryAmount, item.ClaimAmt, item.Provisionalamt, item.ProtestMoney, item.ProbableAmt, item.ExpenseTillDate, item.CBU, item.Zone, item.Region, item.Territory, item.CaseCreator, item.EmailId, item.ContactNumber);
                                        }
                                    }

                                    else
                                    {
                                        foreach (var item in MisReport)
                                        {
                                            Count++;
                                            table.Rows.Add(Count, item.NoticeType, item.NoticeDate, item.RefNo, item.NoticeTitle, item.NoticeTerm, item.CaseType1,
                                                item.Party.TrimEnd(','), item.NoticeDetailDesc, item.Name, item.Jurisdiction, item.ResponseDate,
                                                item.Responsedescription, item.ResponseRemark, item.NoticeDueDate,
                                                item.Owner, item.Lawfirm.TrimEnd(','), item.AssignedUser.TrimEnd(','), item.State, item.FYName.TrimEnd(','),
                                                item.Monetory, item.BankGurantee, item.RecoveryAmount, item.ClaimAmt, item.Provisionalamt, item.ProtestMoney, item.ProbableAmt, item.ExpenseTillDate,item.RiskType);
                                        }
                                    }
                                       
                        
                                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MIS");
                                    DataView view = new System.Data.DataView(table);
                                    DataTable ExcelData = null;
                                    if (NewColumnsLitigation == true)
                                    {
                                        ExcelData = view.ToTable("Selected", false, "SLNO", "NoticeType", "NoticeDate", "RefNo", "NoticeTitle", "NoticeTerm", "CaseType1", "Party", "NoticeDetailDesc", "Name", "Jurisdiction", "ResponseDate", "Responsedescription", "ResponseRemark", "NoticeDueDate",
                                      "Owner", "Lawfirm", "AssignedUser", "State", "FYName", "Monetory", "BankGurantee", "RecoveryAmount", "ClaimAmt", "Provisionalamt", "ProtestMoney", "ProbableAmt", "ExpenseTillDate", "CBU", "Zone", "Region", "Territory", "CaseCreator", "EmailId", "ContactNumber");

                                    }
                                    else
                                    {
                                        ExcelData = view.ToTable("Selected", false, "SLNO", "NoticeType", "NoticeDate", "RefNo", "NoticeTitle", "NoticeTerm", "CaseType1", "Party", "NoticeDetailDesc", "Name", "Jurisdiction", "ResponseDate", "Responsedescription", "ResponseRemark", "NoticeDueDate",
                                         "Owner", "Lawfirm", "AssignedUser", "State", "FYName", "Monetory", "BankGurantee", "RecoveryAmount", "ClaimAmt", "Provisionalamt", "ProtestMoney", "ProbableAmt", "ExpenseTillDate","RiskType");

                                    }

                                    foreach (DataRow item in ExcelData.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(item["NoticeDate"])))
                                        {
                                            var Opendate = Convert.ToDateTime(item["NoticeDate"]).ToString("dd-MMM-yyyy");
                                            item["NoticeDate"] = Opendate;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
                                        {
                                            item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(item["NoticeDueDate"])))
                                        {
                                            item["NoticeDueDate"] = Convert.ToDateTime(item["NoticeDueDate"]).ToString("dd-MMM-yyyy");
                                        }
                                    }

                                    int count3 = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                                    int count2 = Convert.ToInt32(ExcelData.Rows.Count) + 2;

                                    exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet1.Cells["A1"].Value = "Sr NO";
                                    exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["A1"].AutoFitColumns(10);
                                    exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["B1"].Value = "Type";
                                    exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["B1"].AutoFitColumns(10);
                                    exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["C1"].Value = "Notice Open Date";
                                    exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["C1"].AutoFitColumns(10);
                                    //exWorkSheet1.Cells[1A3"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                    exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["D1"].AutoFitColumns(25);
                                    exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["D1"].Value = "Notice No";
                                    exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["E1"].Value = "Notice Title";
                                    exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["E1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["F1"].Value = "Notice Term";
                                    exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["F1"].AutoFitColumns(20);
                                    exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["G1"].Value = "Type of Notice";
                                    exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["G1"].AutoFitColumns(20);
                                    exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    exWorkSheet1.Cells["H1"].Value = "Opponent";
                                    exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["H1"].AutoFitColumns(20);
                                    //exWorkSheet1.Cells1C5"].Style.Numberformat.Format = "dd-MM-yyyy";
                                    exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["I1"].Value = "Notice Detail Description";
                                    exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["I1"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["J1"].Value = "Entity/Location/Branch";
                                    exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["J1"].AutoFitColumns(25);
                                    exWorkSheet1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["K1"].Value = "Jurisdiction";
                                    exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["K1"].AutoFitColumns(10);
                                    exWorkSheet1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["L1"].Value = "Response Date";
                                    exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["L1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["M1"].Value = "Response Description";
                                    exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["M1"].AutoFitColumns(20);
                                    exWorkSheet1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["N1"].Value = "Response Remark";
                                    exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["N1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["N1"].AutoFitColumns(10);
                                    exWorkSheet1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["O1"].Value = "Notice Due Date";
                                    exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["O1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["O1"].AutoFitColumns(10);
                                    //exWorkSheet1.CellsO"M5"].Style.Numberformat.Format = "dd-MM-yyyy";
                                    exWorkSheet1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["P1"].Value = "Owner";
                                    exWorkSheet1.Cells["P1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["P1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["Q1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["Q1"].Value = "Law Firm";
                                    exWorkSheet1.Cells["Q1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["Q1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["Q1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["R1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["R1"].Value = "Name of Lawyer's";
                                    exWorkSheet1.Cells["R1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["R1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["R1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["R1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["S1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["S1"].Value = "State";
                                    exWorkSheet1.Cells["S1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["S1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["S1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["S1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    if (FYCustID == true)
                                    {
                                        exWorkSheet1.Cells["T1"].Value = "Disputed Financial Year";
                                    }
                                    else
                                    {
                                        exWorkSheet1.Cells["T1"].Value = "Financial Year";
                                    }
                                    exWorkSheet1.Cells["T1"].Style.Font.Bold = true;
                                    //exWorkSheet1.Cells["T1"].Value = "Financial Year";
                                    exWorkSheet1.Cells["T1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["T1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["T1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["T1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["T1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["U1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["U1"].Value = "Monetory";
                                    exWorkSheet1.Cells["U1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["U1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["U1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["U1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["U1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["U1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    exWorkSheet1.Cells["V1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["V1"].Value = "Bank Guarantee";
                                    exWorkSheet1.Cells["V1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["V1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["V1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["V1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    exWorkSheet1.Cells["W1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["W1"].Value = "Recovery Amount";
                                    exWorkSheet1.Cells["W1:W" + count3].Style.Numberformat.Format = "#,##0.00";
                                    exWorkSheet1.Cells["W1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["W1"].AutoFitColumns(25);
                                    exWorkSheet1.Cells["W1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["W1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["W1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["W1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["X1"].Value = "Claim Amount";
                                    exWorkSheet1.Cells["X1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["X1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["X1:X" + count3].Style.Numberformat.Format = "#,##0.00";
                                    exWorkSheet1.Cells["X1"].AutoFitColumns(10);
                                    exWorkSheet1.Cells["X1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["X1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["X1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["X1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["Y1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["Y1"].Value = "Provisional Amount";
                                    exWorkSheet1.Cells["Y1:Y" + count3].Style.Numberformat.Format = "#,##0.00";
                                    exWorkSheet1.Cells["Y1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["Y1"].AutoFitColumns(25);
                                    exWorkSheet1.Cells["Y1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["Y1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["Y1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["Y1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["Z1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["Z1"].Value = "Protest Money";
                                    exWorkSheet1.Cells["Z1:Z" + count3].Style.Numberformat.Format = "#,##0.00";
                                    exWorkSheet1.Cells["Z1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["Z1"].AutoFitColumns(25);
                                    exWorkSheet1.Cells["Z1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["Z1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["Z1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["Z1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["AA1"].Value = "Probable Amount";
                                    exWorkSheet1.Cells["AA1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["AA1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["AA1:AA" + count3].Style.Numberformat.Format = "#,##0.00";
                                    exWorkSheet1.Cells["AA1"].AutoFitColumns(10);
                                    exWorkSheet1.Cells["AA1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["AA1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["AA1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["AA1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet1.Cells["AB1"].Value = "Expense Till Date";
                                    exWorkSheet1.Cells["AB1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["AB1"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["AB1:AB" + count3].Style.Numberformat.Format = "#,##0.00";
                                    exWorkSheet1.Cells["AB1"].AutoFitColumns(35);
                                    exWorkSheet1.Cells["AB1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["AB1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["AB1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["AB1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                    if (RiskType == true)
                                    {
                                        exWorkSheet1.Cells["AC1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["AC1"].Value = "Risk";
                                        exWorkSheet1.Cells["AC1:AC" + count3].Style.Numberformat.Format = "#,##0.00";
                                        exWorkSheet1.Cells["AC1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["AC1"].AutoFitColumns(25);
                                        exWorkSheet1.Cells["AC1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["AC1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["AC1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["AC1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                    }
                                        if (NewColumnsLitigation == true)
                                    {
                                        exWorkSheet1.Cells["AC1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["AC1"].Value = "CBU";
                                        exWorkSheet1.Cells["AC1:AC" + count3].Style.Numberformat.Format = "#,##0.00";
                                        exWorkSheet1.Cells["AC1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["AC1"].AutoFitColumns(25);
                                        exWorkSheet1.Cells["AC1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["AC1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["AC1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["AC1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["AD1"].Value = "Zone";
                                        exWorkSheet1.Cells["AD1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["AD1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["AD1:AD" + count3].Style.Numberformat.Format = "#,##0.00";
                                        exWorkSheet1.Cells["AD1"].AutoFitColumns(20);
                                        exWorkSheet1.Cells["AD1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["AD1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["AD1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["AD1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["AE1"].Value = "Region";
                                        exWorkSheet1.Cells["AE1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["AE1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["AE1:AE" + count3].Style.Numberformat.Format = "#,##0.00";
                                        exWorkSheet1.Cells["AE1"].AutoFitColumns(35);
                                        exWorkSheet1.Cells["AE1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["AE1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["AE1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["AE1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["AF1"].Value = "Territory";
                                        exWorkSheet1.Cells["AF1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["AF1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["AF1:AF" + count3].Style.Numberformat.Format = "#,##0.00";
                                        exWorkSheet1.Cells["AF1"].AutoFitColumns(20);
                                        exWorkSheet1.Cells["AF1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["AF1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["AF1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["AF1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                        exWorkSheet1.Cells["AG1"].Value = "Notice Creator";
                                        exWorkSheet1.Cells["AG1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["AG1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["AG1"].AutoFitColumns(15);
                                        exWorkSheet1.Cells["AG1:AG" + count3].Style.Numberformat.Format = "#,##0.00";
                                        exWorkSheet1.Cells["AG1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["AG1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["AG1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["AG1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["AH1"].Value = "Email ID";
                                        exWorkSheet1.Cells["AH1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["AH1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["AH1"].AutoFitColumns(15);
                                        exWorkSheet1.Cells["AH1:AH" + count3].Style.Numberformat.Format = "#,##0.00";
                                        exWorkSheet1.Cells["AH1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["AH1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["AH1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["AH1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["AI1"].Value = "Contact Number";
                                        exWorkSheet1.Cells["AI1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["AI1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["AI1"].AutoFitColumns(15);
                                        exWorkSheet1.Cells["AI1:AI" + count3].Style.Numberformat.Format = "#,##0.00";
                                        exWorkSheet1.Cells["AI1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["AI1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["AI1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["AI1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    }

                                    exWorkSheet1.Cells["A" + count2].Value = "Total";
                                    exWorkSheet1.Cells["A" + count2].Style.Font.Bold = true;

                                    exWorkSheet1.Cells["W" + count2].Formula = "=SUM(W2:W" + count3 + ")";
                                    exWorkSheet1.Cells["W" + count2].Style.Numberformat.Format = "#,##0.00";

                                    exWorkSheet1.Cells["X" + count2].Formula = "=SUM(X2:X" + count3 + ")";
                                    exWorkSheet1.Cells["X" + count2].Style.Numberformat.Format = "#,##0.00";

                                    exWorkSheet1.Cells["Y" + count2].Formula = "=SUM(Y2:Y" + count3 + ")";
                                    exWorkSheet1.Cells["Y" + count2].Style.Numberformat.Format = "#,##0.00";

                                    exWorkSheet1.Cells["Z" + count2].Formula = "=SUM(Z2:Z" + count3 + ")";
                                    exWorkSheet1.Cells["Z" + count2].Style.Numberformat.Format = "#,##0.00";

                                    exWorkSheet1.Cells["AA" + count2].Formula = "=SUM(AA2:AA" + count3 + ")";
                                    exWorkSheet1.Cells["AA" + count2].Style.Numberformat.Format = "#,##0.00";

                                    exWorkSheet1.Cells["AB" + count2].Formula = "=SUM(AB2:AB" + count3 + ")";
                                    exWorkSheet1.Cells["AB" + count2].Style.Numberformat.Format = "#,##0.00";


                                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 2;
                                    using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 35])
                                    {
                                        col.Style.WrapText = true;
                                        //col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    }
                                }
                            }
                            #endregion

                            #region MIS Closed Cases

                            {
                                DataTable table = new DataTable();
                                table.Columns.Add("SLNO", typeof(string));
                                table.Columns.Add("NoticeType", typeof(string));
                                table.Columns.Add("NoticeDate", typeof(string));
                                table.Columns.Add("CloseDate", typeof(string));
                                table.Columns.Add("RefNo", typeof(string));
                                table.Columns.Add("NoticeTitle", typeof(string));
                                table.Columns.Add("NoticeTerm", typeof(string));
                                table.Columns.Add("CaseType1", typeof(string));
                                table.Columns.Add("Party", typeof(string));
                                table.Columns.Add("NoticeDetailDesc", typeof(string));
                                table.Columns.Add("Name", typeof(string));
                                table.Columns.Add("Jurisdiction", typeof(string));
                                table.Columns.Add("ClaimAmt", typeof(string));
                                table.Columns.Add("ResponseDate", typeof(string));
                                table.Columns.Add("Responsedescription", typeof(string));
                                table.Columns.Add("ResponseRemark", typeof(string));
                                table.Columns.Add("NoticeDueDate", typeof(string));
                                table.Columns.Add("ExpenseTillDate", typeof(string));
                                table.Columns.Add("Owner", typeof(string));
                                table.Columns.Add("Lawfirm", typeof(string));
                                table.Columns.Add("State", typeof(string));
                                table.Columns.Add("FYName", typeof(string));
                                table.Columns.Add("Monetory", typeof(string));

                                table.Columns.Add("Provisionalamt", typeof(string));
                                table.Columns.Add("BankGurantee", typeof(string));
                                table.Columns.Add("ProtestMoney", typeof(string));
                                table.Columns.Add("RiskType", typeof(string));

                                int Count = 0;
                                foreach (var item in MisReportClosedNotice)
                                {
                                    Count++;
                                    table.Rows.Add(Count, item.NoticeType, item.NoticeDate, item.CloseDate, item.RefNo, item.NoticeTitle, item.NoticeTerm, item.CaseType1, item.Party, item.NoticeDetailDesc, item.Name, item.Jurisdiction, item.ClaimAmt, item.ResponseDate, item.Responsedescription, item.ResponseRemark, item.NoticeDueDate, item.ExpenseTillDate, item.Owner, item.Lawfirm, item.State, item.FYName, item.Monetory, item.Provisionalamt, item.BankGurantee, item.ProtestMoney,item.RiskType);
                                }
                                ExcelWorksheet exWorkSheetC1 = exportPackge.Workbook.Worksheets.Add("Closed Notices");
                                DataView view = new System.Data.DataView(table);
                                DataTable ExcelData = null;
                                ExcelData = view.ToTable("Selected", false, "SLNO", "NoticeType", "NoticeDate", "CloseDate", "RefNo", "NoticeTitle", "NoticeTerm", "CaseType1", "Party", "NoticeDetailDesc", "Name", "Jurisdiction", "ClaimAmt", "ResponseDate", "Responsedescription", "ResponseRemark", "NoticeDueDate", "ExpenseTillDate", "Owner", "Lawfirm", "State", "FYName", "Monetory", "Provisionalamt", "BankGurantee", "ProtestMoney","RiskType");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["NoticeDate"])))
                                    {
                                        item["NoticeDate"] = Convert.ToDateTime(item["NoticeDate"]).ToString("dd-MMM-yyyy");
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["CloseDate"])))
                                    {
                                        item["CloseDate"] = Convert.ToDateTime(item["CloseDate"]).ToString("dd-MMM-yyyy");
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
                                    {
                                        item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["NoticeDueDate"])))
                                    {
                                        item["NoticeDueDate"] = Convert.ToDateTime(item["NoticeDueDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                exWorkSheetC1.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                exWorkSheetC1.Cells["A1"].Value = "Sr NO";
                                exWorkSheetC1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["A1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["A1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["B1"].Value = "Type";
                                exWorkSheetC1.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["B1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["B1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["C1"].Value = "Notice Date";
                                exWorkSheetC1.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["C1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["C1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["D1"].Value = "Notice Close Date";
                                exWorkSheetC1.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["D1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["D1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["E1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["E1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["E1"].Value = "Notice Ref No";
                                exWorkSheetC1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["F1"].Value = "Notice Title";
                                exWorkSheetC1.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["F1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["F1"].AutoFitColumns(30);
                                exWorkSheetC1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["G1"].Value = "Notice Term";
                                exWorkSheetC1.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["G1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["G1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["H1"].Value = "Type of Notice";
                                exWorkSheetC1.Cells["H1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["H1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["H1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["I1"].Value = "Opponent";
                                exWorkSheetC1.Cells["I1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["I1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["I1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheetC1.Cells["J1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["J1"].Value = "Notice Detail Description";
                                exWorkSheetC1.Cells["J1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["J1"].AutoFitColumns(40);
                                exWorkSheetC1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                exWorkSheetC1.Cells["K1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["K1"].Value = "Entity/Location/Branch";
                                exWorkSheetC1.Cells["K1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["K1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["L1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["L1"].Value = "Jurisdiction";
                                exWorkSheetC1.Cells["L1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["L1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["M1"].Value = "Claim Amt";
                                exWorkSheetC1.Cells["M1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["M1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["M1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["N1"].Value = "Response Date";
                                exWorkSheetC1.Cells["N1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["N1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["N1"].AutoFitColumns(35);
                                exWorkSheetC1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["O1"].Value = "Response Description";
                                exWorkSheetC1.Cells["O1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["O1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["O1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["P1"].Value = "Response Remark";
                                exWorkSheetC1.Cells["P1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["P1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["P1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["Q1"].Value = "Notice Due Date";
                                exWorkSheetC1.Cells["Q1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["Q1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["Q1"].AutoFitColumns(10);
                                exWorkSheetC1.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["Q1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["R1"].Value = "Expense Till Date";
                                exWorkSheetC1.Cells["R1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["R1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["R1"].AutoFitColumns(20);
                                exWorkSheetC1.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["R1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["R1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["S1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["S1"].Value = "Owner";
                                exWorkSheetC1.Cells["S1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["S1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["S1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["S1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["T1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["T1"].Value = "Law Firm";
                                exWorkSheetC1.Cells["T1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["T1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["T1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["T1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["T1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["U1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["U1"].Value = "State";
                                exWorkSheetC1.Cells["U1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["U1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["U1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["U1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["U1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["U1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                if (FYCustID == true)
                                {
                                    exWorkSheetC1.Cells["V1"].Value = "Disputed Financial Year";
                                }
                                else
                                {
                                    exWorkSheetC1.Cells["V1"].Value = "FinancialYear";
                                }
                                exWorkSheetC1.Cells["V1"].Style.Font.Bold = true;                              
                                exWorkSheetC1.Cells["V1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["V1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["V1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["V1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["W1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["W1"].Value = "Monetory";
                                exWorkSheetC1.Cells["W1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["W1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["W1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["W1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["W1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["W1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["X1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["X1"].Value = "Provisional amount";
                                exWorkSheetC1.Cells["X1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["X1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["X1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["X1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["X1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["X1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["Y1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["Y1"].Value = "Bank Guarantee";
                                exWorkSheetC1.Cells["Y1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["Y1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["Y1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["Y1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["Y1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["Y1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                exWorkSheetC1.Cells["Z1"].Style.Font.Bold = true;
                                exWorkSheetC1.Cells["Z1"].Value = "Protest Money";
                                exWorkSheetC1.Cells["Z1"].Style.Font.Size = 12;
                                exWorkSheetC1.Cells["Z1"].AutoFitColumns(25);
                                exWorkSheetC1.Cells["Z1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheetC1.Cells["Z1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheetC1.Cells["Z1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheetC1.Cells["Z1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                if(RiskType == true)
                                {
                                    exWorkSheetC1.Cells["AA1"].Style.Font.Bold = true;
                                    exWorkSheetC1.Cells["AA1"].Value = "Risk";
                                    exWorkSheetC1.Cells["AA1"].Style.Font.Size = 12;
                                    exWorkSheetC1.Cells["AA1"].AutoFitColumns(25);
                                    exWorkSheetC1.Cells["AA1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheetC1.Cells["AA1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheetC1.Cells["AA1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheetC1.Cells["AA1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                }

                                int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                                using (ExcelRange col = exWorkSheetC1.Cells[1, 1, count, 27])
                                {
                                    col.Style.WrapText = true;

                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                }
                            }

                            #endregion

                            #region Lawyer Performance Report
                            {
                                {
                                    ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("Lawyers Performance Report ");
                                    DataView view = new System.Data.DataView(LawyerPerformanceReport.ToDataTable());
                                    DataTable ExcelData = null;
                                    ExcelData = view.ToTable("Selected", false, "LawyerName", "Firm_Name", "NoticeHandled", "WIN", "LOSS", "In_Progress", "Settled", "Withdrawn", "Rating");

                                    exWorkSheet2.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet2.Cells["A1"].Value = "Name of Lawyer";
                                    exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["A1"].AutoFitColumns(25);
                                    exWorkSheet2.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet2.Cells["B1"].Value = "Firm Name";
                                    exWorkSheet2.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["B1"].AutoFitColumns(25);
                                    exWorkSheet2.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet2.Cells["C1"].Value = "No of Notice Handled";
                                    exWorkSheet2.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["C1"].AutoFitColumns(10);
                                    exWorkSheet2.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet2.Cells["D1"].Value = "Won";
                                    exWorkSheet2.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["D1"].AutoFitColumns(10);
                                    exWorkSheet2.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet2.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["E1"].AutoFitColumns(10);
                                    exWorkSheet2.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["E1"].Value = "Lost";
                                    exWorkSheet2.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet2.Cells["F1"].Value = "In Progress";
                                    exWorkSheet2.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["F1"].AutoFitColumns(10);
                                    exWorkSheet2.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet2.Cells["G1"].Value = "Settled";
                                    exWorkSheet2.Cells["G1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["G1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["G1"].AutoFitColumns(10);
                                    exWorkSheet2.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet2.Cells["H1"].Value = "Withdrawn";
                                    exWorkSheet2.Cells["H1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["H1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["H1"].AutoFitColumns(10);
                                    exWorkSheet2.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663




                                    exWorkSheet2.Cells["I1"].Value = "Rating";
                                    exWorkSheet2.Cells["I1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["I1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["I1"].AutoFitColumns(10);
                                    exWorkSheet2.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet2.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet2.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet2.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                                    using (ExcelRange col = exWorkSheet2.Cells[1, 1, count, 9])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    }
                                }
                            }
                            #endregion

                            #region Lawyer Performance Report In Details
                            ExcelWorksheet exWorkSheett3 = exportPackge.Workbook.Worksheets.Add("Lawyers Performance Detail");
                            DataView view1 = new System.Data.DataView(CriteriawiseReport.ToDataTable());
                            DataTable ExcelData1 = null;
                            ExcelData1 = view1.ToTable("Selected", false, "RefNo", "NoticeTitle", "UserName", "LawyerName", "CriteriaName", "Rating");

                            exWorkSheett3.Cells["A1"].LoadFromDataTable(ExcelData1, true);
                            exWorkSheett3.Cells["A1"].Value = "Notice No";
                            exWorkSheett3.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["A1"].AutoFitColumns(25);
                            exWorkSheett3.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["B1"].Value = "Notice Title";
                            exWorkSheett3.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["B1"].AutoFitColumns(25);
                            exWorkSheett3.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["C1"].Value = "User";
                            exWorkSheett3.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["C1"].AutoFitColumns(10);
                            exWorkSheett3.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["D1"].Value = "Lawyer";
                            exWorkSheett3.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["D1"].AutoFitColumns(10);
                            exWorkSheett3.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["E1"].AutoFitColumns(10);
                            exWorkSheett3.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["E1"].Value = "Criteria";
                            exWorkSheett3.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheett3.Cells["F1"].Value = "Rating";
                            exWorkSheett3.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheett3.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheett3.Cells["F1"].AutoFitColumns(10);
                            exWorkSheett3.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheett3.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheett3.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheett3.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            int count1 = Convert.ToInt32(ExcelData1.Rows.Count) + 1;
                            using (ExcelRange col = exWorkSheett3.Cells[1, 1, count1, 6])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            }
                            #endregion

                            #region Budget Vs Expense Tracking
                            {
                                {
                                    ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("Budget Vs Expense Tracking");
                                    DataView view = new System.Data.DataView(ExpensevsbudgetReport.ToDataTable());
                                    DataTable ExcelData = null;
                                    ExcelData = view.ToTable("Selected", false, "RefNo", "PaymentType", "BudgetedAmount", "Amount", "Remark", "Alert");

                                    #region Lawyer Performance Report
                                    exWorkSheet3.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet3.Cells["A1"].Value = "Notice No.";
                                    exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["A1"].AutoFitColumns(25);
                                    exWorkSheet3.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet3.Cells["B1"].Value = "Payment Type (Court Fee/ Lawyer Fee/ Miscellaneous/ Other Expenses)";
                                    exWorkSheet3.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["B1"].AutoFitColumns(25);
                                    exWorkSheet3.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet3.Cells["C1"].Value = "Estimated Budgeted Exp";
                                    exWorkSheet3.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["C1"].AutoFitColumns(25);
                                    exWorkSheet3.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet3.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["D1"].AutoFitColumns(20);
                                    exWorkSheet3.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells["D1"].Value = "Amount";
                                    exWorkSheet3.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet3.Cells["E1"].Value = "Remark";
                                    exWorkSheet3.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["E1"].AutoFitColumns(25);
                                    exWorkSheet3.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet3.Cells["F1"].Value = "Alert for Exceeding from Budget";
                                    exWorkSheet3.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["F1"].AutoFitColumns(10);
                                    exWorkSheet3.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    //exWorkSheet3.Cells["G1"].Value = "Financial Year";
                                    //exWorkSheet3.Cells["G1"].Style.Font.Bold = true;
                                    //exWorkSheet3.Cells["G1"].Style.Font.Size = 12;
                                    //exWorkSheet3.Cells["G1"].AutoFitColumns(10);
                                    //exWorkSheet3.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    //exWorkSheet3.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    //exWorkSheet3.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    //exWorkSheet3.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    //exWorkSheet3.Cells["H1"].Value = "Provisional Amount";
                                    //exWorkSheet3.Cells["H1"].Style.Font.Bold = true;
                                    //exWorkSheet3.Cells["H1"].Style.Font.Size = 12;
                                    //exWorkSheet3.Cells["H1"].AutoFitColumns(10);
                                    //exWorkSheet3.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    //exWorkSheet3.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    //exWorkSheet3.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    //exWorkSheet3.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    //exWorkSheet3.Cells["I1"].Value = "Bank Gurantee";
                                    //exWorkSheet3.Cells["I1"].Style.Font.Bold = true;
                                    //exWorkSheet3.Cells["I1"].Style.Font.Size = 12;
                                    //exWorkSheet3.Cells["I1"].AutoFitColumns(10);
                                    //exWorkSheet3.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    //exWorkSheet3.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    //exWorkSheet3.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    //exWorkSheet3.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    //exWorkSheet3.Cells["J1"].Value = "Protest Money";
                                    //exWorkSheet3.Cells["J1"].Style.Font.Bold = true;
                                    //exWorkSheet3.Cells["J1"].Style.Font.Size = 12;
                                    //exWorkSheet3.Cells["J1"].AutoFitColumns(10);
                                    //exWorkSheet3.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    //exWorkSheet3.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    //exWorkSheet3.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    //exWorkSheet3.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    #endregion

                                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                                    using (ExcelRange col = exWorkSheet3.Cells[1, 1, count, 7])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    }
                                }
                            }
                            #endregion

                            #region Lawyer Data
                            {
                                {
                                    ExcelWorksheet exWorkSheet4 = exportPackge.Workbook.Worksheets.Add("Lawyers Details");
                                    DataView view = new System.Data.DataView(LawyerDetailsReport.ToDataTable());
                                    DataTable ExcelData = null;
                                    ExcelData = view.ToTable("Selected", false, "SLNO", "LawyerName", "Address", "ContactNumber", "Email", "Specialisation", "Rating");
                                    int Count = 0;
                                    foreach (DataRow item in ExcelData.Rows)
                                    {
                                        Count++;
                                        item["SLNO"] = Count;
                                    }
                                    #region Lawyer Performance Report
                                    exWorkSheet4.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet4.Cells["A1"].Value = "Sr No.";
                                    exWorkSheet4.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet4.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet4.Cells["A1"].AutoFitColumns(10);
                                    exWorkSheet4.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet4.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet4.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet4.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet4.Cells["B1"].Value = "Name of Lawyer";
                                    exWorkSheet4.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet4.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet4.Cells["B1"].AutoFitColumns(30);
                                    exWorkSheet4.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet4.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet4.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet4.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet4.Cells["C1"].Value = "Address";
                                    exWorkSheet4.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet4.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet4.Cells["C1"].AutoFitColumns(30);
                                    exWorkSheet4.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet4.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet4.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet4.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet4.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet4.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet4.Cells["D1"].AutoFitColumns(10);
                                    exWorkSheet4.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet4.Cells["D1"].Value = "Contact No";
                                    exWorkSheet4.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet4.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet4.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet4.Cells["E1"].Value = "EMail";
                                    exWorkSheet4.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet4.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet4.Cells["E1"].AutoFitColumns(30);
                                    exWorkSheet4.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet4.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet4.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet4.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet4.Cells["F1"].Value = "Specialization";
                                    exWorkSheet4.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet4.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet4.Cells["F1"].AutoFitColumns(10);
                                    exWorkSheet4.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet4.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet4.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet4.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                                                                                                                                     //Rating
                                    exWorkSheet4.Cells["G1"].Value = "Rating";
                                    exWorkSheet4.Cells["G1"].Style.Font.Bold = true;
                                    exWorkSheet4.Cells["G1"].Style.Font.Size = 12;
                                    exWorkSheet4.Cells["G1"].AutoFitColumns(10);
                                    exWorkSheet4.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet4.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet4.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet4.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    #endregion

                                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                                    using (ExcelRange col = exWorkSheet4.Cells[1, 1, count, 7])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    }
                                }
                            }
                            #endregion

                            #region Notice Payment
                            {
                                {
                                    DataTable table = new DataTable();
                                    table.Columns.Add("RefNo", typeof(string));
                                    table.Columns.Add("PaymentInvoiceNo", typeof(string));
                                    table.Columns.Add("PaymentType", typeof(string));
                                    table.Columns.Add("PaymentDate", typeof(string));
                                    table.Columns.Add("Amount", typeof(string));
                                    table.Columns.Add("AmountTax", typeof(string));
                                    table.Columns.Add("Remark", typeof(string));
                                    table.Columns.Add("Lawyer", typeof(string));
                                    if (NewColumnsLitigation == true)
                                    {
                                        table.Columns.Add("CBU", typeof(string));
                                        table.Columns.Add("Zone", typeof(string));
                                        table.Columns.Add("Region", typeof(string));
                                        table.Columns.Add("Territory", typeof(string));
                                        table.Columns.Add("CaseCreator", typeof(string));
                                        table.Columns.Add("EmailId", typeof(string));
                                        table.Columns.Add("ContactNumber", typeof(string));
                                    }

                                    //table.Columns.Add("FYName", typeof(string));
                                    //table.Columns.Add("Provisionalamt", typeof(string));
                                    //table.Columns.Add("BankGurantee", typeof(string));
                                    //table.Columns.Add("ProtestMoney", typeof(string));
                                    if (NewColumnsLitigation == true)
                                    {
                                        foreach (var item in NoticePaymentsReport)
                                        {
                                            table.Rows.Add(item.RefNo, item.InvoiceNo, item.PaymentType, item.PaymentDate, item.Amount, item.AmountTax, item.Remark, item.Lawyer, item.CBU, item.Zone, item.Region, item.Territory, item.CaseCreator, item.EmailId, item.ContactNumber);
                                        }
                                    }
                                    else
                                    {
                                        foreach (var item in NoticePaymentsReport)
                                        {
                                            table.Rows.Add(item.RefNo, item.InvoiceNo, item.PaymentType, item.PaymentDate, item.Amount, item.AmountTax, item.Remark, item.Lawyer);
                                        }
                                    }
                                       

                                    ExcelWorksheet exWorkSheet5 = exportPackge.Workbook.Worksheets.Add("Notice Payments  Data");
                                    DataView view = new System.Data.DataView(table);
                                    DataTable ExcelData = null;
                                    if (NewColumnsLitigation == true)
                                    {
                                        ExcelData = view.ToTable("Selected", false, "RefNo", "PaymentInvoiceNo", "PaymentType", "PaymentDate", "Amount", "AmountTax", "Remark", "Lawyer", "CBU", "Zone", "Region", "Territory", "CaseCreator", "EmailId", "ContactNumber");

                                    }
                                    else
                                    {
                                        ExcelData = view.ToTable("Selected", false, "RefNo", "PaymentInvoiceNo", "PaymentType", "PaymentDate", "Amount", "AmountTax", "Remark", "Lawyer");

                                    }

                                    foreach (DataRow item in ExcelData.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(item["PaymentDate"])))
                                        {
                                            item["PaymentDate"] = Convert.ToDateTime(item["PaymentDate"]).ToString("dd-MMM-yyyy");
                                        }
                                    }

                                    #region Lawyer Performance Report
                                    exWorkSheet5.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet5.Cells["A1"].Value = "Notice No.";
                                    exWorkSheet5.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet5.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet5.Cells["A1"].AutoFitColumns(25);
                                    exWorkSheet5.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet5.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet5.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet5.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet5.Cells["B1"].Value = "Invoice No";
                                    exWorkSheet5.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet5.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet5.Cells["B1"].AutoFitColumns(25);
                                    exWorkSheet5.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet5.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet5.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet5.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet5.Cells["C1"].Value = "Payment Type (Court Fee/ Lawyer Fee/ Miscellaneous/ Other Expenses)";
                                    exWorkSheet5.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet5.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet5.Cells["C1"].AutoFitColumns(10);
                                    exWorkSheet5.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet5.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet5.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet5.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet5.Cells["D1"].Value = "Payment Date";
                                    exWorkSheet5.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet5.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet5.Cells["D1"].AutoFitColumns(10);
                                    exWorkSheet5.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet5.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet5.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet5.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet5.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet5.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet5.Cells["E1"].AutoFitColumns(20);
                                    exWorkSheet5.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet5.Cells["E1"].Value = "Amount excluding tax";
                                    exWorkSheet5.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet5.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet5.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet5.Cells["F1"].Value = "Tax Amount";
                                    exWorkSheet5.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet5.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet5.Cells["F1"].AutoFitColumns(25);
                                    exWorkSheet5.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet5.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet5.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet5.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    exWorkSheet5.Cells["G1"].Value = "Remark";
                                    exWorkSheet5.Cells["G1"].Style.Font.Bold = true;
                                    exWorkSheet5.Cells["G1"].Style.Font.Size = 12;
                                    exWorkSheet5.Cells["G1"].AutoFitColumns(25);
                                    exWorkSheet5.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet5.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet5.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet5.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                    exWorkSheet5.Cells["H1"].Value = "Lawyer";
                                    exWorkSheet5.Cells["H1"].Style.Font.Bold = true;
                                    exWorkSheet5.Cells["H1"].Style.Font.Size = 12;
                                    exWorkSheet5.Cells["H1"].AutoFitColumns(25);
                                    exWorkSheet5.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet5.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet5.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet5.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    if (NewColumnsLitigation == true)
                                    {

                                        exWorkSheet5.Cells["I1"].Value = "CBU";
                                        exWorkSheet5.Cells["I1"].Style.Font.Bold = true;
                                        exWorkSheet5.Cells["I1"].Style.Font.Size = 12;
                                        exWorkSheet5.Cells["I1"].AutoFitColumns(25);
                                        exWorkSheet5.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet5.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet5.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet5.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                        exWorkSheet5.Cells["J1"].Value = "Zone";
                                        exWorkSheet5.Cells["J1"].Style.Font.Bold = true;
                                        exWorkSheet5.Cells["J1"].Style.Font.Size = 12;
                                        exWorkSheet5.Cells["J1"].AutoFitColumns(25);
                                        exWorkSheet5.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet5.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet5.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet5.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet5.Cells["K1"].Value = "Region";
                                        exWorkSheet5.Cells["K1"].Style.Font.Bold = true;
                                        exWorkSheet5.Cells["K1"].Style.Font.Size = 12;
                                        exWorkSheet5.Cells["K1"].AutoFitColumns(25);
                                        exWorkSheet5.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet5.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet5.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet5.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet5.Cells["L1"].Value = "Territory";
                                        exWorkSheet5.Cells["L1"].Style.Font.Bold = true;
                                        exWorkSheet5.Cells["L1"].Style.Font.Size = 12;
                                        exWorkSheet5.Cells["L1"].AutoFitColumns(25);
                                        exWorkSheet5.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet5.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet5.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet5.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet5.Cells["M1"].Value = "Notice Creator";
                                        exWorkSheet5.Cells["M1"].Style.Font.Bold = true;
                                        exWorkSheet5.Cells["M1"].Style.Font.Size = 12;
                                        exWorkSheet5.Cells["M1"].AutoFitColumns(25);
                                        exWorkSheet5.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet5.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet5.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet5.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet5.Cells["N1"].Value = "Email ID";
                                        exWorkSheet5.Cells["N1"].Style.Font.Bold = true;
                                        exWorkSheet5.Cells["N1"].Style.Font.Size = 12;
                                        exWorkSheet5.Cells["N1"].AutoFitColumns(25);
                                        exWorkSheet5.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet5.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet5.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet5.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                                        exWorkSheet5.Cells["O1"].Value = "Contact Number";
                                        exWorkSheet5.Cells["O1"].Style.Font.Bold = true;
                                        exWorkSheet5.Cells["O1"].Style.Font.Size = 12;
                                        exWorkSheet5.Cells["O1"].AutoFitColumns(25);
                                        exWorkSheet5.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet5.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet5.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet5.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    }
                                   


                                    #endregion

                                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                                    using (ExcelRange col = exWorkSheet5.Cells[1, 1, count, 8])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    }
                                }
                            }
                            #endregion

                            #region Notice Response Report
                            {
                                {
                                    DataTable table = new DataTable();
                                    table.Columns.Add("RefNo", typeof(string));
                                    table.Columns.Add("NoticedueDate", typeof(string));
                                    table.Columns.Add("ResponseRefNo", typeof(string));
                                    table.Columns.Add("Description", typeof(string));
                                    table.Columns.Add("Remark", typeof(string));
                                    table.Columns.Add("ResponseDocument", typeof(string));

                                    foreach (var item in NoticeResponseReport)
                                    {
                                        table.Rows.Add(item.RefNo, item.NoticedueDate, item.ResponseRefNo, item.Description, item.Remark, item.ResponseDocument);
                                    }

                                    ExcelWorksheet exWorkSheet6 = exportPackge.Workbook.Worksheets.Add("Notice Response");
                                    DataView view = new System.Data.DataView(table);
                                    DataTable ExcelData = null;
                                    ExcelData = view.ToTable("Selected", false, "RefNo", "NoticedueDate", "ResponseRefNo", "Description", "Remark", "ResponseDocument");

                                    foreach (DataRow item in ExcelData.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(item["NoticedueDate"])))
                                        {
                                            item["NoticedueDate"] = Convert.ToDateTime(item["NoticedueDate"]).ToString("dd-MMM-yyyy");
                                        }
                                    }

                                    #region Lawyer Performance Report
                                    exWorkSheet6.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet6.Cells["A1"].Value = "Notice No.";
                                    exWorkSheet6.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet6.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet6.Cells["A1"].AutoFitColumns(20);
                                    exWorkSheet6.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet6.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet6.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet6.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet6.Cells["B1"].Value = "Response Due Date";
                                    exWorkSheet6.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet6.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet6.Cells["B1"].AutoFitColumns(10);
                                    exWorkSheet6.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet6.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet6.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet6.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet6.Cells["C1"].Value = "Response Ref No";
                                    exWorkSheet6.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet6.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet6.Cells["C1"].AutoFitColumns(25);
                                    exWorkSheet6.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet6.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet6.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet6.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet6.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet6.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet6.Cells["D1"].AutoFitColumns(50);
                                    exWorkSheet6.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet6.Cells["D1"].Value = "Response Deascription";
                                    exWorkSheet6.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet6.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet6.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet6.Cells["E1"].Value = "Remark";
                                    exWorkSheet6.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet6.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet6.Cells["E1"].AutoFitColumns(50);
                                    exWorkSheet6.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet6.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet6.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet6.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    exWorkSheet6.Cells["F1"].Value = "Response Documents";
                                    exWorkSheet6.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet6.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet6.Cells["F1"].AutoFitColumns(50);
                                    exWorkSheet6.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet6.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet6.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet6.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                    #endregion

                                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                                    using (ExcelRange col = exWorkSheet6.Cells[1, 1, count, 6])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    }
                                }
                            }
                            #endregion

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "All Notice -" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                                                                                                                             //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {

        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                Branchlist.Clear();
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                GetAllHierarchy((int)AuthenticationHelper.CustomerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int IsForBranch = UserManagement.ExistBranchAssignment(customerID);
                if (IsForBranch == 1)
                {
                    tvFilterLocation.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchyManagementSatutoryNew(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }

                    tbxFilterLocation.Text = "Select Entity/Location";

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    List<TreeNode> nodes = new List<TreeNode>();

                    int userId = -1;
                    userId = AuthenticationHelper.UserID;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var BranchList = (from row in entities.LitigationEntitiesAssignments
                                          where row.UserID == userId
                                          select (int)row.BranchID).ToList();

                        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                         join row1 in entities.tbl_LegalCaseInstance
                                                         on row.CaseInstanceID equals row1.ID
                                                         where row1.CustomerID == (int)customerID
                                                         && (row.UserID == userId || row1.OwnerID == userId || row1.CreatedBy == userId || BranchList.Contains(row1.CustomerBranchID))
                                                         select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                           join row1 in entities.tbl_LegalNoticeInstance
                                                           on row.NoticeInstanceID equals row1.ID
                                                           where row1.CustomerID == (int)customerID
                                                           && (row.UserID == userId || row1.OwnerID == userId || row1.CreatedBy == userId || BranchList.Contains(row1.CustomerBranchID))
                                                           select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                        var LocationList = LegalCasbranchlist.Select(a => a).ToList();

                        BindBranchesHierarchyNew(null, branch, nodes, LocationList);
                    }
                    
                   
                    foreach (TreeNode item in nodes)
                    {
                        tvFilterLocation.Nodes.Add(item);
                    }

                    tvFilterLocation.CollapseAll();
                }
                else
                {
                    tvFilterLocation.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }

                    tbxFilterLocation.Text = "Select Entity/Location";

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    List<TreeNode> nodes = new List<TreeNode>();

                    int userId = -1;
                    userId = AuthenticationHelper.UserID;

                    BindBranchesHierarchy(null, branch, nodes);

                    foreach (TreeNode item in nodes)
                    {
                        tvFilterLocation.Nodes.Add(item);
                    }

                    tvFilterLocation.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchyNew(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes, List<int> branchlst)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchyNew(node, item, nodes, branchlst);
                        if (branchlst.Contains(item.ID) == true)
                        {
                            nodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvCasePopUp.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvCasePopUp.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkCase_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Litigation/Reports/litigationreports.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void btnCaseNotice_Click(object sender, EventArgs e)
        {
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    #region CaseNotice
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        try
                        {
                            string LatestHearingOnly = "Case/Notice Report";
                            List<HearingDetailReport> HearingDataobj = new List<HearingDetailReport>();
                            List<ResponseDetailReport> ResponseDetailsObj = new List<ResponseDetailReport>();
                            List<int> CaseInstanceIDDynamic = new List<int>();
                            DataTable tableCaseCustomField = new DataTable();


                            #region All customer

                            entities.Database.CommandTimeout = 180;
                            var CaseNoticeReport = (entities.SP_CaseNoticeCombineReport(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                            if (Branchlist.Count > 0)
                            {
                                CaseNoticeReport = CaseNoticeReport.Where(entry => Branchlist.Contains(Convert.ToInt32(entry.CustomerBranchID))).ToList();
                            }
                           
                            if (!string.IsNullOrEmpty(txtFromDate.Text))
                            {
                                CaseNoticeReport = CaseNoticeReport.Where(entry => entry.OpenDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
                            }
                            if (!string.IsNullOrEmpty(txtToDate.Text))
                            {
                                CaseNoticeReport = CaseNoticeReport.Where(entry => entry.OpenDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
                            }
                            if (CaseNoticeReport.Count > 0)
                            {
                                CaseNoticeReport = (from g in CaseNoticeReport
                                                    group g by new
                                                    {

                                                        g.CaseRefNo,
                                                        g.CaseTitle,
                                                        g.Name,
                                                        g.TypeName,
                                                        g.Category,
                                                        g.Department,
                                                        g.NoticeCaseDesc,
                                                        g.Party,
                                                        g.OpenDate,
                                                        g.CloseDate,
                                                        g.Status,
                                                        g.CaseNoticeRemark,
                                                        g.Jurisdiction,
                                                        g.StateName,
                                                        g.ClaimAmount,
                                                        g.ProbableAmount,
                                                        g.RecoveryAmount,
                                                        g.Type

                                                    } into GCS
                                                    select new SP_CaseNoticeCombineReport_Result()
                                                    {
                                                        CaseRefNo = GCS.Key.CaseRefNo,
                                                        CaseTitle = GCS.Key.CaseTitle,
                                                        Name = GCS.Key.Name,
                                                        TypeName = GCS.Key.TypeName,
                                                        Category = GCS.Key.Category,
                                                        Department = GCS.Key.Department,
                                                        NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
                                                        Party = GCS.Key.Party,
                                                        OpenDate = GCS.Key.OpenDate,
                                                        CloseDate = GCS.Key.CloseDate,
                                                        Status = GCS.Key.Status,
                                                        CaseNoticeRemark = GCS.Key.CaseNoticeRemark,
                                                        Jurisdiction = GCS.Key.Jurisdiction,
                                                        StateName = GCS.Key.StateName,
                                                        ClaimAmount = GCS.Key.ClaimAmount,
                                                        ProbableAmount = GCS.Key.ProbableAmount,
                                                        RecoveryAmount = GCS.Key.RecoveryAmount,
                                                        Type = GCS.Key.Type,

                                                    }).ToList();
                            }
                            if (!string.IsNullOrEmpty(txtFromDate.Text))
                            {
                                CaseNoticeReport = CaseNoticeReport.Where(entry => entry.OpenDate >= DateTimeExtensions.GetDate(txtFromDate.Text)).ToList();
                            }
                            if (!string.IsNullOrEmpty(txtToDate.Text))
                            {
                                CaseNoticeReport = CaseNoticeReport.Where(entry => entry.OpenDate <= DateTimeExtensions.GetDate(txtToDate.Text)).ToList();
                            }


                            DataTable table = new DataTable();
                            table.Columns.Add("SLNO", typeof(string));
                            table.Columns.Add("CaseRefNo", typeof(string));
                            table.Columns.Add("CaseTitle", typeof(string));
                            table.Columns.Add("Name", typeof(string));
                            table.Columns.Add("TypeName", typeof(string));
                            table.Columns.Add("Category", typeof(string));
                            table.Columns.Add("Department", typeof(string));
                            table.Columns.Add("NoticeCaseDesc", typeof(string));
                            table.Columns.Add("Party", typeof(string));
                            table.Columns.Add("OpenDate", typeof(string));
                            table.Columns.Add("CloseDate", typeof(string));
                            table.Columns.Add("Status", typeof(string));

                            table.Columns.Add("CaseNoticeRemark", typeof(string));
                            table.Columns.Add("Jurisdiction", typeof(string));  //Change
                            table.Columns.Add("StateName", typeof(string));

                            table.Columns.Add("ClaimAmount", typeof(string));
                            table.Columns.Add("ProbableAmount", typeof(string)); //change
                            table.Columns.Add("RecoveryAmount", typeof(string));
                            table.Columns.Add("Type", typeof(string));


                            int Count = 0;
                            CaseNoticeReport = CaseNoticeReport.OrderBy(entry => entry.NoticeCaseInstanceID).ToList();
                            foreach (var item in CaseNoticeReport)
                            {
                                string updatedHearingDate = string.Empty;
                                string updatedNextHearingDate = string.Empty;



                                Count++;
                                CaseInstanceIDDynamic.Add(Convert.ToInt32(item.NoticeCaseInstanceID));
                                table.Rows.Add(Count, item.CaseRefNo, item.CaseTitle, item.Name, item.TypeName, item.Category,
                                    item.Department, item.NoticeCaseDesc, item.Party, item.OpenDate,
                                    item.CloseDate, item.Status, item.CaseNoticeRemark,
                                    item.Jurisdiction, item.StateName, item.ClaimAmount, item.ProbableAmount, item.RecoveryAmount,
                                    item.Type);
                            }

                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Case/Notice");
                            DataView view = new System.Data.DataView(table);
                            DataTable ExcelData = null;

                            ExcelData = view.ToTable("Selected", false, "SLNO", "CaseRefNo", "CaseTitle", "Name", "TypeName", "Category", "Department", "NoticeCaseDesc", "Party", "OpenDate", "CloseDate", "Status",
                                 "CaseNoticeRemark", "Jurisdiction", "StateName", "ClaimAmount", "ProbableAmount", "RecoveryAmount", "Type");


                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["OpenDate"])))
                                {
                                    var Opendate = Convert.ToDateTime(item["OpenDate"]).ToString("dd-MMM-yyyy");
                                    item["OpenDate"] = Opendate;
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(item["CloseDate"])))
                                {
                                    var CloseDate = Convert.ToDateTime(item["CloseDate"]).ToString("dd-MMM-yyyy");
                                    item["CloseDate"] = CloseDate;
                                }
                            }
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            int count2 = Convert.ToInt32(ExcelData.Rows.Count) + 2;

                            exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet1.Cells["A1"].Value = "Sr.NO.";
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A1"].AutoFitColumns(10);
                            exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["B1"].Value = "Ref No";
                            exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["B1"].AutoFitColumns(10);
                            exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["C1"].Value = "Title";
                            exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C1"].AutoFitColumns(40);
                            //exWorkSheet1.Cells[1A3"].Style.Numberformat.Format = "dd-MMM-yyyy";
                            exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["D1"].Value = "Branch Name";
                            exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["E1"].Value = "Type";
                            exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E1"].AutoFitColumns(30);
                            exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["F1"].Value = "Category";
                            exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["G1"].Value = "Department";
                            exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G1"].AutoFitColumns(35);
                            //exWorkSheet1.Cells[1C5"].Style.Numberformat.Format = "dd-MM-yyyy";
                            exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H1"].Value = "Description";
                            exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H1"].AutoFitColumns(40);
                            exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I1"].Value = "Party Name";
                            exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J1"].Value = "Open Date";
                            exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J1"].AutoFitColumns(40);
                            exWorkSheet1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K1"].Value = "Close Date";
                            exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["K1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["L1"].Style.Font.Bold = true;

                            exWorkSheet1.Cells["L1"].Value = "Status";
                            exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["L1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            exWorkSheet1.Cells["M1"].Value = "Remark";
                            exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["M1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N1"].Value = "Jurisdiction";
                            exWorkSheet1.Cells["N1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["N1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O1"].Value = "State";
                            exWorkSheet1.Cells["O1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["O1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P1"].Value = "Claim Amount";
                            exWorkSheet1.Cells["P1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["P1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["Q1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q1"].Value = "Probable Amount";
                            exWorkSheet1.Cells["Q1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["Q1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Q1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                            exWorkSheet1.Cells["R1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R1"].Value = "Recovery Amount";
                            exWorkSheet1.Cells["R1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["R1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["R1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["R1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                            exWorkSheet1.Cells["S1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S1"].Value = "Type";
                            exWorkSheet1.Cells["S1"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["S1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["S1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["S1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663






                            #region Start Dynamic Column of Notice

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet1.Cells["A2"].LoadFromDataTable(ExcelData, false);
                                exWorkSheet1.DefaultRowHeight = 42;
                            }

                            #endregion

                            #region Merging cells

                            exWorkSheet1.Column(1).Width = 9;
                            exWorkSheet1.Column(2).Width = 14;
                            exWorkSheet1.Column(3).Width = 14;
                            exWorkSheet1.Column(4).Width = 41;
                            exWorkSheet1.Column(5).Width = 32;
                            exWorkSheet1.Column(6).Width = 23;
                            exWorkSheet1.Column(7).Width = 22;
                            exWorkSheet1.Column(8).Width = 24;
                            exWorkSheet1.Column(9).Width = 30;
                            exWorkSheet1.Column(10).Width = 60;
                            exWorkSheet1.Column(11).Width = 24;
                            exWorkSheet1.Column(12).Width = 18;
                            exWorkSheet1.Column(13).Width = 19;
                            exWorkSheet1.Column(14).Width = 19;
                            exWorkSheet1.Column(15).Width = 19;
                            exWorkSheet1.Column(16).Width = 16;
                            exWorkSheet1.Column(17).Width = 20;
                            exWorkSheet1.Column(18).Width = 25;
                            exWorkSheet1.Column(19).Width = 25;


                            int count = Convert.ToInt32(ExcelData.Rows.Count) + 2;
                            using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 19])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            #endregion

                            #endregion
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            string locatioName = "Case/Notice-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                            Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                                                                                                                             //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.



                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}