﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="LitigationNoticeReportAll.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Reports.LitigationNoticeReportAll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .btnreports {
            width: 274px;
            height: 70px;
            margin: 10px 30px 10px 10px;
            font-size: 20px;
            font-weight: 500;
        }

        .panel-heading {
            background: #ffffff;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: #1fd9e1;
                background-color: #fff;
            }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('Notice Reports');
            BindControls();
        });

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtToDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        //maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                    });
            });

            $(function () {
                $('input[id*=txtFromDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        //maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                    });
            });
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
    <div class="col-md-12 colpadding0" style="margin-top:10px; margin-bottom:50px;">
        <div class="col-md-10 colpadding0">
            <div class="tab-bg-primary panel-heading ">
                <ul class="nav nav-tabs">
                    <li class="active" id="liNotice" runat="server">
                        <asp:LinkButton ID="lnkNotice" CausesValidation="false" runat="server">Notice</asp:LinkButton>
                    </li>
                    <li class="" id="liCase" runat="server">
                        <asp:LinkButton ID="lnkCase" CausesValidation="false" runat="server" OnClick="lnkCase_Click">Case</asp:LinkButton>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-2 colpadding0 text-right">
        </div>
    </div>
        </div>
    <div class="row">
        <div class="col-md-12 colpadding0">

            <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                <ContentTemplate>
                    <div class="col-md-3 colpadding0 entrycount">
                        <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Entity/Branch/Location</label>
                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 32px; width: 95%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                            CssClass="s" />
                        <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px;" id="divFilterLocation">
                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="100%" NodeStyle-ForeColor="#8e8e93"
                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="col-md-3 colpadding0 entrycount">
                <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333; width: 50%; margin-left: 15px;">From Date</label>
                <%--<asp:TextBox ID="txtFromDate" autocomplete="off" runat="server" CssClass="form-control" MaxLength="100" Style="width: 60%; padding-left: 10px; display: initial; background-color: #fff; cursor: pointer;"></asp:TextBox>--%>
                <div class="col-md-6 input-group date" style="width: 65%">
                    <span class="input-group-addon">
                        <span class="fa fa-calendar color-black" style="padding: 3px !important;"></span>
                    </span>
                    <asp:TextBox runat="server" placeholder="From Date" class="form-control" ID="txtFromDate" />
                </div>
            </div>

            <div class="col-md-3 colpadding0 entrycount">
                <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333; width: 50%; margin-left: 15px;">To Date</label>
                <%--<asp:TextBox ID="txtToDate" autocomplete="off" runat="server" CssClass="form-control" MaxLength="100" Style="width: 60%; display: initial; background-color: #fff; cursor: pointer;"></asp:TextBox>--%>
                <div class="col-md-6 input-group date" style="width: 65%">
                    <span class="input-group-addon">
                        <span class="fa fa-calendar color-black" style="padding: 3px !important;"></span>
                    </span>
                    <asp:TextBox runat="server" placeholder="To Date" class="form-control" ID="txtToDate" />
                </div>
            </div>
            <div class="col-md-3 colpadding0 entrycount">
            </div>
        </div>
    </div>

    <asp:Button ID="btnMis" CssClass="btn btn-search btnreports" runat="server" Text="MIS" OnClick="btnMis_Click" />
    <asp:Button ID="btnMisCloseReport" CssClass="btn btn-search btnreports" runat="server" Text="Closed Notices" OnClick="btnMisCloseReport_Click" />
    <asp:Button ID="btnMISNew" CssClass="btn btn-search btnreports" runat="server" Text="MIS New" OnClick="btnMISNew_Click" />
    <asp:Button ID="btnLawyerPerformance" CssClass="btn btn-search btnreports" runat="server" Text="Lawyer Performance" OnClick="btnLawyerPerformance_Click" />
    <asp:Button ID="btnBudgetVsExpenseTracking" runat="server" CssClass="btn btn-search btnreports" Text="Budget Vs Expense Tracking" OnClick="btnBudgetVsExpenseTracking_Click" />
    <asp:Button ID="btnLawyerDetails" runat="server" CssClass="btn btn-search btnreports" Text="Lawyer Details" OnClick="btnLawyerDetails_Click" />
    <asp:Button ID="btnNoticepayments" runat="server" CssClass="btn btn-search btnreports" Text="Notice Payments" OnClick="btnNoticepayments_Click" />
    <asp:Button ID="btnNoticeResponse" runat="server" CssClass="btn btn-search btnreports" Text="Notice Response" OnClick="btnNoticeResponse_Click" />
    <asp:Button ID="btnAll" runat="server" Text="All" CssClass="btn btn-search btnreports" OnClick="btnAll_Click" />
    <%if (IsCaseNoticeReport == CustID) %>
    <%{ %>
     <asp:Button ID="btnCaseNotice" runat="server" Text="Case/Notice" CssClass="btn btn-search btnreports" onclick="btnCaseNotice_Click" />
    <%} %>
</asp:Content>
