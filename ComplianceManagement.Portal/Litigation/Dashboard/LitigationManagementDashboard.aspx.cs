﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Dashboard
{
    public partial class LitigationManagementDashboard : System.Web.UI.Page
    {
        protected static string NoticeCaseWiseExpensesData;
        protected static string NoticeCaseExpenseDepartmentWise;
        protected static string NoticeCaseTypeWiseSeriesData;
        protected static string strNoticeCaseDepartments;
        protected static string NoticeCaseDepartmentWiseData;
        protected static string NoticeCaseWinningProspectData;
        protected static string NoticeCaseBranchWiseData;
        protected static string CaseStageWiseData;
        protected static string NoticeCaseTypeWise;
        protected static string noticeCaseAgeingLessThanYear;
        protected static string noticeCaseAgeing1to2Years;
        protected static string noticeCaseAgeing2to3Years;
        protected static string noticeCaseAgeingMoreThan3Years;
        public static bool RPACustomerEnable;
        protected static string NoticeCaseRiskType;
        protected static bool RiskGraph;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                RPACustomerEnable = CaseManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "RPA");
                RiskGraph = CaseManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "RiskType");
                HiddenField home = (HiddenField) Master.FindControl("Ishome");
                home.Value = "true";

                BindCustomerBranches();
                BindParty(customerID);
                BindDepartment();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var viewNoticeAssignedInstanceRecords = (from c in entities.View_NoticeAssignedInstance
                                                             where c.CustomerID == customerID
                                                             select c).ToList();

                    var ViewCaseAssignedInstanceRecords = (from c in entities.View_CaseAssignedInstance
                                                           where c.CustomerID == customerID
                                                           select c).ToList();

                    var ViewTaskRecords = (from row in entities.View_NoticeCaseTaskDetail
                                           where row.CustomerID == customerID
                                           && row.IsActive == true
                                           select row).ToList();
                    var viewRPACaseRecords = (from c in entities.tbl_RPACaseStatus
                                              where c.CustomerID == customerID
                                              && c.IsDeleted == false
                                              select c).Count();

                    //Notice
                    divOpenNoticeCount.InnerText = NoticeManagement.GetAssignedNoticeCount(viewNoticeAssignedInstanceRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 1, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToString();
                    divClosedNoticeCount.InnerText = NoticeManagement.GetAssignedNoticeCount(viewNoticeAssignedInstanceRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 3, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToString();

                    viewNoticeAssignedInstanceRecords.Clear();
                    viewNoticeAssignedInstanceRecords = null;

                    //Case
                    divOpenCaseCount.InnerText = CaseManagement.GetAssignedCaseCount(ViewCaseAssignedInstanceRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 1, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToString();
                    divClosedCaseCount.InnerText = CaseManagement.GetAssignedCaseCount(ViewCaseAssignedInstanceRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 3, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToString();

                    if (RPACustomerEnable)
                    {
                        divRPADraft.InnerText = Convert.ToString(viewRPACaseRecords);
                    }
                    ViewCaseAssignedInstanceRecords.Clear();
                    ViewCaseAssignedInstanceRecords = null;

                    //Task
                    divOpenTaskCount.InnerText = LitigationTaskManagement.GetAssignedTaskCount(ViewTaskRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 1).ToString();
                    divClosedTaskCount.InnerText = LitigationTaskManagement.GetAssignedTaskCount(ViewTaskRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 3).ToString();

                    var totalsum = (from row in entities.Sp_TotalLiability(customerID)
                                           select row).FirstOrDefault();

                    if (totalsum !=null)
                    {
                        try
                        {
                            var a = decimal.Round((decimal)totalsum / 1000000, 2, MidpointRounding.AwayFromZero);
                            divTotalLiability.InnerText = $"{a:n2}" + " mn";
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            divTotalLiability.InnerText = "0";
                            
                        }
                       
                    }

                    //String.Format();
                    //Task List - Open then Recent Due Date
                    var lstTasks = LitigationTaskManagement.GetDashboardTaskList(ViewTaskRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3);

                    grdTaskActivity.DataSource = lstTasks;
                    grdTaskActivity.DataBind();

                    ViewTaskRecords.Clear();
                    ViewTaskRecords = null;

                    //Upcoming Hearing
                    //BindUpcompingHearing();

                    //Bind Graphs
                    BindGraphData();
                }                
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userId = -1;
                userId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                int IsForBranch = UserManagement.ExistBranchAssignment(customerID);
                if (IsForBranch == 1)
                {
                    tvFilterLocation.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchyManagementSatutoryNew(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }

                    tbxFilterLocation.Text = "Select Entity/Location";

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    List<TreeNode> nodes = new List<TreeNode>();
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var BranchList = (from row in entities.LitigationEntitiesAssignments
                                          where row.UserID == userId
                                          select (int)row.BranchID).ToList();

                        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                         join row1 in entities.tbl_LegalCaseInstance
                                                         on row.CaseInstanceID equals row1.ID
                                                         where row1.CustomerID == (int)customerID
                                                         && (row.UserID == userId || row1.OwnerID == userId || row1.CreatedBy == userId || BranchList.Contains(row1.CustomerBranchID))
                                                         select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                           join row1 in entities.tbl_LegalNoticeInstance
                                                           on row.NoticeInstanceID equals row1.ID
                                                           where row1.CustomerID == (int)customerID
                                                           && (row.UserID == userId || row1.OwnerID == userId || row1.CreatedBy == userId || BranchList.Contains(row1.CustomerBranchID))
                                                           select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                        var LocationList = LegalCasbranchlist.Select(a => a).ToList();

                        BindBranchesHierarchyNew(null, branch, nodes, LocationList);
                    }

                    foreach (TreeNode item in nodes)
                    {
                        tvFilterLocation.Nodes.Add(item);
                    }

                    tvFilterLocation.CollapseAll();
                }
                else
                {

                    tvFilterLocation.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchy(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }

                    tbxFilterLocation.Text = "Select Entity/Location";

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    List<TreeNode> nodes = new List<TreeNode>();
                    BindBranchesHierarchy(null, branch, nodes);

                    foreach (TreeNode item in nodes)
                    {
                        tvFilterLocation.Nodes.Add(item);
                    }

                    tvFilterLocation.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchyNew(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes, List<int> branchlst)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchyNew(node, item, nodes, branchlst);
                        if (branchlst.Contains(item.ID) == true)
                        {
                            nodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvCasePopUp.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindParty(long customerID)
        {
            var obj = LitigationLaw.GetLCPartyDetails(customerID);

            //Drop-Down at Page
            ddlPartyPage.DataTextField = "Name";
            ddlPartyPage.DataValueField = "ID";

            ddlPartyPage.DataSource = obj;
            ddlPartyPage.DataBind();

            ddlPartyPage.Items.Insert(0, new ListItem("Select Party", "-1"));
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("Select Department", "-1"));
        }

        public void BindGraphData()
        {
            try
            {
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int catID = -1;
                int riskID = -1;
                int stageID = -1;
                int noticeCaseStatus = -1;

                string noticeCaseType = string.Empty;
                string filterNoticeOrCase = string.Empty;

                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var noticeCaseDetailViewRecords = (from record in entities.View_LitigationDashboard
                                                       where record.CustomerID == customerID
                                                       select record).ToList();

                    filterNoticeOrCase = ddlTypePage.SelectedValue;

                    if (!String.IsNullOrEmpty(ddlTypePage.SelectedValue))
                    {
                        if (ddlTypePage.SelectedValue != "-1" && ddlTypePage.SelectedValue != "B")//B--Both --Notice(1) and Case(2)
                        {
                            noticeCaseDetailViewRecords = noticeCaseDetailViewRecords.Where(entry => entry.Type == filterNoticeOrCase).ToList();
                        }
                    }

                    //Filters
                    if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                    {
                        noticeCaseType = ddlNoticeTypePage.SelectedValue;
                    }

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                        if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                    {
                        partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                    {
                        noticeCaseStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlWinningImpact.SelectedValue))
                    {
                        riskID = Convert.ToInt32(ddlWinningImpact.SelectedValue);
                    }
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if ( IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,AuthenticationHelper.Role,-1);
                        if (caseList.Count > 0)
                        {
                            noticeCaseDetailViewRecords = noticeCaseDetailViewRecords.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeInstanceID))).ToList();
                        }
                        else
                        {
                            noticeCaseDetailViewRecords = noticeCaseDetailViewRecords.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeInstanceID))).ToList();
                        }
                    }
                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                    if (noticeCaseDetailViewRecords != null)
                    {
                        // In case of MGMT or CADMN 
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            noticeCaseDetailViewRecords = noticeCaseDetailViewRecords.Where(entry => entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3).ToList();
                        else
                        {
                            // In case of MGMT or CADMN
                            noticeCaseDetailViewRecords = noticeCaseDetailViewRecords.Where(entry => entry.RoleID == 3).ToList();

                            if (noticeCaseDetailViewRecords.Count > 0)
                            {
                                noticeCaseDetailViewRecords = (from g in noticeCaseDetailViewRecords
                                                               group g by new
                                                               {
                                                                   g.Type,
                                                                   g.NoticeInstanceID,
                                                                   g.NoticeTitle,
                                                                   g.NoticeRiskID,
                                                                   g.NoticeType,
                                                                   g.NoticeTypeName,
                                                                   g.TxnStatusID,
                                                                   g.TotalPayment,
                                                                   g.CustomerID,
                                                                   g.CustomerBranchID,
                                                                   g.BranchName,
                                                                   g.DepartmentID,
                                                                   g.DeptName,
                                                                   g.PartyID,
                                                                   g.PartyName,
                                                                   g.NoticeCategoryID,
                                                                   g.NoticeCategory,
                                                                   g.CaseStageID,
                                                                   g.CaseStage,
                                                                   g.AgeingDays,
                                                                   g.RiskType,
                                                                   g.RiskTypeID
                                                               } into GCS
                                                               select new View_LitigationDashboard()
                                                               {
                                                                   Type = GCS.Key.Type,
                                                                   NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                                                   NoticeType = GCS.Key.NoticeType,
                                                                   NoticeTypeName = GCS.Key.NoticeTypeName,
                                                                   NoticeTitle = GCS.Key.NoticeTitle,
                                                                   NoticeRiskID = GCS.Key.NoticeRiskID,
                                                                   TxnStatusID = GCS.Key.TxnStatusID,
                                                                   TotalPayment = GCS.Key.TotalPayment,
                                                                   CustomerBranchID = GCS.Key.CustomerBranchID,
                                                                   BranchName = GCS.Key.BranchName,
                                                                   DepartmentID = GCS.Key.DepartmentID,
                                                                   DeptName = GCS.Key.DeptName,
                                                                   PartyID = GCS.Key.PartyID,
                                                                   PartyName = GCS.Key.PartyName,
                                                                   NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                                                   NoticeCategory = GCS.Key.NoticeCategory,
                                                                   CaseStageID = GCS.Key.CaseStageID,
                                                                   CaseStage = GCS.Key.CaseStage,
                                                                   AgeingDays = GCS.Key.AgeingDays,
                                                                   CustomerID = GCS.Key.CustomerID,
                                                                   RiskType = GCS.Key.RiskType,
                                                                   RiskTypeID = GCS.Key.RiskTypeID
                                                               }).ToList();
                            }
                        } //End MGMT Filter

                        GetNoticeCaseCountByType(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);

                        GetCaseByCaseStage(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);

                        //GetNoticeCaseByWinningProspect(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);

                        GetNoticeCaseByDepartment(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);

                        GetNoticeCaseAgeing(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);

                        GetNoticeCaseCountByCategory(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);
                        if(RiskGraph == true)
                        {
                            GetNoticeCaseCountByRisk(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);
                        }

                        //GetNoticeCaseWiseExpenses(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID,stageID, noticeCaseStatus);

                        //GetNoticeCaseExpensesByDepartment(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);

                        //GetNoticeCaseByBranch(noticeCaseDetailViewRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchID, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetNoticeCaseWiseExpenses(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strNoticeCaseWiseExpenseSeriesData = String.Empty;

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    viewRecords.ForEach(eachRecord =>
                    {
                        if (eachRecord.TotalPayment != null && eachRecord.TotalPayment > 0)
                            strNoticeCaseWiseExpenseSeriesData += "{name:'" + eachRecord.NoticeTitle + "', y:" + eachRecord.TotalPayment + "," +
                            "events: { click: function(e) { ShowGraphDetail('" + eachRecord.Type + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + deptID + "," + catID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";
                        
                        //strNoticeCaseWiseExpenseSeriesData += "['" + eachRecord.NoticeTitle + "'," + eachRecord.TotalPayment + "],";
                        //else
                        //    strNoticeCaseWiseExpenseSeriesData += "['" + eachRecord.NoticeTitle + "',0],";
                    });

                    strNoticeCaseWiseExpenseSeriesData = strNoticeCaseWiseExpenseSeriesData.Trim(',');

                    NoticeCaseWiseExpensesData = strNoticeCaseWiseExpenseSeriesData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetNoticeCaseExpensesByDepartment(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strNoticeCaseExpDepartmentWiseSeriesData = String.Empty;

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    var lstDistinctDept = (from g in viewRecords
                                           group g by new
                                           {
                                               g.DepartmentID,
                                               g.DeptName
                                           } into groupedData
                                           select new dummyClass()
                                           {
                                               ID = (int) groupedData.Key.DepartmentID,
                                               Name = groupedData.Key.DeptName,
                                           }).ToList();

                    lstDistinctDept.ForEach(eachDept =>
                    {
                        var totalExpDeptWise = (from row in viewRecords
                                                where row.DepartmentID == eachDept.ID
                                                select row.TotalPayment).Sum();
                        if (totalExpDeptWise > 0)
                            strNoticeCaseExpDepartmentWiseSeriesData += "{name:'" + eachDept.Name + "', y:" + totalExpDeptWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + eachDept.ID + "," + catID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";
                        //strNoticeCaseExpDepartmentWiseSeriesData += "['" + eachDept.Name + "'," + totalExpDeptWise + "],";
                    });

                    strNoticeCaseExpDepartmentWiseSeriesData = strNoticeCaseExpDepartmentWiseSeriesData.Trim(',');

                    NoticeCaseExpenseDepartmentWise = strNoticeCaseExpDepartmentWiseSeriesData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void GetNoticeCaseCountByRisk(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strNoticeCaseCategorySeriesData = String.Empty;

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                //if (branchList.Count > 0)
                //    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                //if (deptID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                //if (riskID != -1 && riskID != 0)
                //    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }
                 
                if (viewRecords.Count > 0)
                {
                    var lstDistinctCategory = (from g in viewRecords
                                               where g.RiskType != null
                                               && g.RiskTypeID !=null
                                               && g.RiskTypeID !=-1
                                               && g.CustomerID == AuthenticationHelper.CustomerID
                                               group g by new
                                               {
                                                   g.RiskTypeID,
                                                   g.RiskType

                                               } into groupedData
                                               select new dummyClass()
                                               {
                                                   ID = (int)groupedData.Key.RiskTypeID,
                                                   Name = groupedData.Key.RiskType

                                               }).ToList();

                    lstDistinctCategory.ForEach(eachRisk =>
                    {
                        var totalCaseCategoryWise = (from row in viewRecords
                                                     where row.RiskTypeID == eachRisk.ID
                                                     && row.RiskType != null && row.RiskTypeID != null
                                                     && row.RiskTypeID != -1
                                                     && row.CustomerID == AuthenticationHelper.CustomerID
                                                     select row).Count();
                        if (totalCaseCategoryWise > 0)
                            //   strNoticeCaseCategorySeriesData += "{name:'" + eachRisk.Name + "', y:" + totalCaseCategoryWise + "}"; 
                            strNoticeCaseCategorySeriesData += "{name:'" + eachRisk.Name + "', y:" + totalCaseCategoryWise + "," +
                         "events: { click: function(e) { ShowRiskGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + deptID + "," + eachRisk.ID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";

                        //+
                        // "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + deptID + "," + eachRisk.ID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";

                        //   strNoticeCaseCategorySeriesData += "['" + eachRisk.Name + "'," + count + "],";
                    });

                    strNoticeCaseCategorySeriesData = strNoticeCaseCategorySeriesData.Trim(',');

                    NoticeCaseRiskType = strNoticeCaseCategorySeriesData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetNoticeCaseCountByCategory(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strNoticeCaseCategorySeriesData = String.Empty;

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    var lstDistinctCategory = (from g in viewRecords
                                               group g by new
                                               {
                                                   g.NoticeCategoryID,
                                                   g.NoticeCategory

                                               } into groupedData
                                               select new dummyClass()
                                               {
                                                   ID = (int) groupedData.Key.NoticeCategoryID,
                                                   Name = groupedData.Key.NoticeCategory,
                                               }).ToList();

                    lstDistinctCategory.ForEach(eachCategory =>
                    {
                        var totalCaseCategoryWise = (from row in viewRecords
                                                     where row.NoticeCategoryID == eachCategory.ID
                                                     select row).Count();

                        if (totalCaseCategoryWise > 0)
                            strNoticeCaseCategorySeriesData += "{name:'" + eachCategory.Name + "', y:" + totalCaseCategoryWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + deptID + "," + eachCategory.ID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";

                        //strNoticeCaseCategorySeriesData += "['" + eachCategory.Name + "'," + count + "],";
                    });

                    strNoticeCaseCategorySeriesData = strNoticeCaseCategorySeriesData.Trim(',');

                    NoticeCaseTypeWiseSeriesData = strNoticeCaseCategorySeriesData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetNoticeCaseByDepartment(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strNoticeCaseDepartmentWiseSeriesData = String.Empty;

                string highCountList = string.Empty;
                string lowCountList = string.Empty;
                string mediumCountList = string.Empty;

                strNoticeCaseDepartments = String.Empty;
                strNoticeCaseDepartments = "xAxis: { categories: [";

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    var lstDistinctDept = (from g in viewRecords
                                           group g by new
                                           {
                                               g.DepartmentID,
                                               g.DeptName
                                           } into groupedData
                                           select new dummyClass()
                                           {
                                               ID = (int) groupedData.Key.DepartmentID,
                                               Name = groupedData.Key.DeptName,
                                           }).ToList();

                    lstDistinctDept.ForEach(eachDept =>
                    {
                        var totalCaseDeptWise = (from row in viewRecords
                                                 where row.DepartmentID == eachDept.ID
                                                 select row).ToList();

                        if (totalCaseDeptWise.Count > 0)
                        {
                            strNoticeCaseDepartments += "'" + eachDept.Name + "',";
                            // Stacked Column - {name: 'High', color: '#FF7473', [{y: 10, events: { click: function(e) { alert(e.point)} } }, {y: 5, events: { click: function(e) { alert(e.point)} } } ], ,
                            // Normal Column -  {name: 'Microsoft Internet Explorer', y: 56.33, }, 

                            var highCount = totalCaseDeptWise.Where(entry => entry.NoticeRiskID == 1).Count();
                            var mediumCount = totalCaseDeptWise.Where(entry => entry.NoticeRiskID == 2).Count();
                            var lowCount = totalCaseDeptWise.Where(entry => entry.NoticeRiskID == 3).Count();

                            //if (highCount > 0)                                                                                                NOC, Type, BID, PID, DID, CID, RID, SID, Status, AID
                            highCountList = highCountList + "{ y:" + highCount + ",events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + eachDept.ID + "," + catID + "," + 1 + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";
                            //if (mediumCount > 0)
                            mediumCountList = mediumCountList + "{ y:" + mediumCount + ",events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + eachDept.ID + "," + catID + "," + 2 + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";
                            //if (lowCount > 0)
                            lowCountList = lowCountList + "{ y:" + lowCount + ",events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + eachDept.ID + "," + catID + "," + 3 + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";
                        }
                    });

                    highCountList = "{ name: 'High', color: '#FF7473', data: [" + highCountList.Trim(',') + "] },";                    
                    mediumCountList = "{ name: 'Medium', color: '#FFC952', data: [" + mediumCountList.Trim(',') + "] },";
                    lowCountList = "{ name: 'Low', color: '#1FD9E1', data: [" + lowCountList.Trim(',') + "] },";                    

                    strNoticeCaseDepartmentWiseSeriesData = highCountList + mediumCountList + lowCountList.Trim(',');

                    NoticeCaseDepartmentWiseData = strNoticeCaseDepartmentWiseSeriesData;

                    strNoticeCaseDepartments = strNoticeCaseDepartments.Trim(',') + "]},";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetNoticeCaseByWinningProspect(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strNoticeCaseWinningProspectSeriesData = String.Empty;

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    var lstDistinctProspect = (from g in viewRecords
                                               group g by new
                                               {
                                                   g.NoticeRiskID,
                                               } into groupedData
                                               select new dummyClass()
                                               {
                                                   ID = (int) groupedData.Key.NoticeRiskID,
                                               }).OrderBy(x =>x.ID).ToList();

                    lstDistinctProspect.ForEach(eachProspect =>
                    {
                        var totalCaseProspectWise = (from row in viewRecords
                                                     where row.NoticeRiskID == eachProspect.ID
                                                     select row).Count();

                        if (totalCaseProspectWise > 0)
                        {
                            string prospectName = string.Empty;

                            if (eachProspect.ID == 1)
                                prospectName = "High";
                            else if (eachProspect.ID == 2)
                                prospectName = "Medium";
                            else if (eachProspect.ID == 3)
                                prospectName = "Low";
                            else
                                prospectName = "High";

                            strNoticeCaseWinningProspectSeriesData += "{name:'" + prospectName + "', y:" + totalCaseProspectWise + "," +
                              "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + deptID + "," + catID + "," + eachProspect.ID + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";
                        }
                    });

                    strNoticeCaseWinningProspectSeriesData = strNoticeCaseWinningProspectSeriesData.Trim(',');

                    NoticeCaseWinningProspectData = strNoticeCaseWinningProspectSeriesData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetNoticeCaseByBranch(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strGraphBranchWiseSeriesData = String.Empty;
                
                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    var lstDistinctBranch = (from g in viewRecords
                                           group g by new
                                           {
                                               g.CustomerBranchID,
                                               g.BranchName
                                           } into groupedData
                                           select new dummyClass()
                                           {
                                               ID = (int) groupedData.Key.CustomerBranchID,
                                               Name = groupedData.Key.BranchName,
                                           }).ToList();

                    lstDistinctBranch.ForEach(eachBranch =>
                    {
                        var totalCaseBranchWise = (from row in viewRecords
                                                 where row.CustomerBranchID == eachBranch.ID
                                                 select row).Count();

                        if (totalCaseBranchWise > 0)
                        {
                            strGraphBranchWiseSeriesData += "{name:'" + eachBranch.Name + "', y:" + totalCaseBranchWise + "," +
                              "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + eachBranch.ID + "," + partyID + "," + deptID + "," + catID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";
                        }
                    });
                                        
                    strGraphBranchWiseSeriesData = strGraphBranchWiseSeriesData.Trim(',');

                    NoticeCaseBranchWiseData = strGraphBranchWiseSeriesData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetCaseByCaseStage(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strGraphCaseStageWiseSeriesData = String.Empty;

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    var lstDistinctCaseStage = (from g in viewRecords
                                                where g.CaseStageID != null && g.CaseStageID != 0
                                                group g by new
                                                {
                                                    g.CaseStageID,
                                                    g.CaseStage
                                                } into groupedData
                                                select new dummyClass()
                                                {
                                                    ID = (int) groupedData.Key.CaseStageID,
                                                    Name = groupedData.Key.CaseStage,
                                                }).ToList();

                    lstDistinctCaseStage.ForEach(eachCaseStage =>
                    {
                        var totalCaseSatgeWise = (from row in viewRecords
                                                   where row.CaseStageID == eachCaseStage.ID
                                                   select row).Count();

                        if (totalCaseSatgeWise > 0)
                        {
                            strGraphCaseStageWiseSeriesData += "{name:'" + eachCaseStage.Name + "', y:" + totalCaseSatgeWise + "," +
                              "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + noticeCaseType + "'," + branchID + "," + partyID + "," + deptID + "," + catID + "," + riskID + "," + eachCaseStage.ID + "," + noticeCaseStatus + "," + 0 + ") }} },";
                        }
                    });

                    strGraphCaseStageWiseSeriesData = strGraphCaseStageWiseSeriesData.Trim(',');

                    CaseStageWiseData = strGraphCaseStageWiseSeriesData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetNoticeCaseCountByType(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strNoticeCaseTypeSeriesData = String.Empty;

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    var lstDistinctType = (from g in viewRecords
                                               group g by new
                                               {
                                                   g.NoticeType,
                                                   g.NoticeTypeName

                                               } into groupedData
                                               select new dummyClass()
                                               {
                                                   Code = groupedData.Key.NoticeType,
                                                   Name = groupedData.Key.NoticeTypeName,
                                               }).ToList();

                    lstDistinctType.ForEach(eachType =>
                    {
                        var totalCaseCategoryWise = (from row in viewRecords
                                                     where row.NoticeType == eachType.Code
                                                     select row).Count();

                        if (totalCaseCategoryWise > 0)
                            strNoticeCaseTypeSeriesData += "{name:'" + eachType.Name + "', y:" + totalCaseCategoryWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + eachType.Code + "'," + branchID + "," + partyID + "," + deptID + "," + catID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 0 + ") }} },";

                        //strNoticeCaseCategorySeriesData += "['" + eachCategory.Name + "'," + count + "],";
                    });

                    strNoticeCaseTypeSeriesData = strNoticeCaseTypeSeriesData.Trim(',');

                    NoticeCaseTypeWise = strNoticeCaseTypeSeriesData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public void GetNoticeCaseAgeing(List<View_LitigationDashboard> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, int branchID, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                string strGraphAgeing1 = String.Empty;
                string strGraphAgeing2 = String.Empty;
                string strGraphAgeing3 = String.Empty;
                string strGraphAgeing4 = String.Empty;

                //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                //else
                //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                //Graph 1
                #region Graph 1 -- Less than a year
                var ageingRecords = viewRecords.Where(entry => entry.AgeingDays <= 365).ToList();

                if (ageingRecords != null)
                {
                    var lstDistinctType = (from g in ageingRecords
                                           group g by new
                                           {
                                               g.NoticeType,
                                               g.NoticeTypeName

                                           } into groupedData
                                           select new dummyClass()
                                           {
                                               Code = groupedData.Key.NoticeType,
                                               Name = groupedData.Key.NoticeTypeName,
                                           }).OrderBy(row => row.Code).ToList();

                    lstDistinctType.ForEach(eachType =>
                    {
                        var totalCaseCategoryWise = (from row in ageingRecords
                                                     where row.NoticeType == eachType.Code
                                                     select row).Count();

                        if (totalCaseCategoryWise > 0)
                        {
                            string colorCode = string.Empty;

                            if (eachType.Name.Contains("Inward"))
                                colorCode = "color: '#FFC952'";
                            else
                                colorCode = "color: '#FF7473'";

                            strGraphAgeing1 += "{name:'" + eachType.Name + "',"+ colorCode + ", y:" + totalCaseCategoryWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + eachType.Code + "'," + branchID + "," + partyID + "," + deptID + "," + catID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 1 + ") }} },";
                        }                     
                    });

                    strGraphAgeing1 = strGraphAgeing1.Trim(',');

                    noticeCaseAgeingLessThanYear = strGraphAgeing1;
                }
                #endregion 

                //Graph 2
                #region Graph 2 -- 1 to 2 years
                ageingRecords = viewRecords.Where(entry => entry.AgeingDays > 365 && entry.AgeingDays <= 730).ToList();

                if (ageingRecords != null)
                {
                    var lstDistinctType = (from g in ageingRecords
                                           group g by new
                                           {
                                               g.NoticeType,
                                               g.NoticeTypeName

                                           } into groupedData
                                           select new dummyClass()
                                           {
                                               Code = groupedData.Key.NoticeType,
                                               Name = groupedData.Key.NoticeTypeName,
                                           }).OrderBy(row => row.Code).ToList();

                    lstDistinctType.ForEach(eachType =>
                    {
                        var totalCaseCategoryWise = (from row in ageingRecords
                                                     where row.NoticeType == eachType.Code
                                                     select row).Count();

                        if (totalCaseCategoryWise > 0)
                        {
                            string colorCode = string.Empty;

                            if (eachType.Name.Contains("Inward"))
                                colorCode = "color: '#FFC952'";
                            else
                                colorCode = "color: '#FF7473'";

                            strGraphAgeing2 += "{name:'" + eachType.Name + "'," + colorCode + ", y:" + totalCaseCategoryWise + "," +                          
                           "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + eachType.Code + "'," + branchID + "," + partyID + "," + deptID + "," + catID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 2 + ") }} },";
                        }
                    });

                    strGraphAgeing2 = strGraphAgeing2.Trim(',');

                    noticeCaseAgeing1to2Years = strGraphAgeing2;
                }
                #endregion

                //Graph 3
                #region Graph 3 -- 2 to 3 years

                ageingRecords = viewRecords.Where(entry => entry.AgeingDays > 730 && entry.AgeingDays <= 1095).ToList();

                if (ageingRecords != null)
                {
                    var lstDistinctType = (from g in ageingRecords
                                           group g by new
                                           {
                                               g.NoticeType,
                                               g.NoticeTypeName

                                           } into groupedData
                                           select new dummyClass()
                                           {
                                               Code = groupedData.Key.NoticeType,
                                               Name = groupedData.Key.NoticeTypeName,
                                           }).OrderBy(row => row.Code).ToList();

                    lstDistinctType.ForEach(eachType =>
                    {
                        var totalCaseCategoryWise = (from row in ageingRecords
                                                     where row.NoticeType == eachType.Code
                                                     select row).Count();

                        if (totalCaseCategoryWise > 0)
                        {
                            string colorCode = string.Empty;

                            if (eachType.Name.Contains("Inward"))
                                colorCode = "color: '#FFC952'";
                            else
                                colorCode = "color: '#FF7473'";

                            strGraphAgeing3 += "{name:'" + eachType.Name + "',"+ colorCode + ", y:" + totalCaseCategoryWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + eachType.Code + "'," + branchID + "," + partyID + "," + deptID + "," + catID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 3 + ") }} },";
                        }
                    });

                    strGraphAgeing3 = strGraphAgeing3.Trim(',');

                    noticeCaseAgeing2to3Years = strGraphAgeing3;
                }

                #endregion

                //Graph 4
                #region Graph 4 -- More than 3 years

                ageingRecords = viewRecords.Where(entry => entry.AgeingDays > 1095).ToList();

                if (ageingRecords != null)
                {
                    var lstDistinctType = (from g in ageingRecords
                                           group g by new
                                           {
                                               g.NoticeType,
                                               g.NoticeTypeName

                                           } into groupedData
                                           select new dummyClass()
                                           {
                                               Code = groupedData.Key.NoticeType,
                                               Name = groupedData.Key.NoticeTypeName,
                                           }).OrderBy(row => row.Code).ToList();

                    lstDistinctType.ForEach(eachType =>
                    {
                        var totalCaseCategoryWise = (from row in ageingRecords
                                                     where row.NoticeType == eachType.Code
                                                     select row).Count();

                        if (totalCaseCategoryWise > 0)
                        {
                            string colorCode = string.Empty;

                            if (eachType.Name.Contains("Inward"))
                                colorCode = "color: '#FFC952'";
                            else
                                colorCode = "color: '#FF7473'";

                            strGraphAgeing4 += "{name:'" + eachType.Name + "',"+ colorCode + ", y:" + totalCaseCategoryWise + "," +
                           "events: { click: function(e) { ShowGraphDetail('" + noticeCase + "','" + eachType.Code + "'," + branchID + "," + partyID + "," + deptID + "," + catID + "," + riskID + "," + stageID + "," + noticeCaseStatus + "," + 4 + ") }} },";
                        }
                    });

                    strGraphAgeing4 = strGraphAgeing4.Trim(',');

                    noticeCaseAgeingMoreThan3Years = strGraphAgeing4;
                }
                #endregion 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void lnkShowHearingDetail_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btn = (ImageButton) (sender);
                if (btn != null)
                {
                    string[] commandArg = btn.CommandArgument.ToString().Split(',');

                    int noticeCaseInstanceID = Convert.ToInt32(commandArg[0]);
                    //string type = commandArg[1];
                    string type = "C";

                    if (noticeCaseInstanceID != 0 && !string.IsNullOrEmpty(type))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowNoticeCaseDialog(" + noticeCaseInstanceID + ",'" + type + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGraphData();

                collapseDivFilters.Attributes.Remove("class");
                collapseDivFilters.Attributes.Add("class", "panel-collapse in");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlTypePage.ClearSelection();
                ddlNoticeTypePage.ClearSelection();
                ddlPartyPage.ClearSelection();
                ddlDeptPage.ClearSelection();
                ddlStatus.ClearSelection();
                ddlWinningImpact.ClearSelection();

                ClearTreeViewSelection(tvFilterLocation);

                tbxFilterLocation.Text = "Select Entity/Location";

                btnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        #region Task Detail

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnTaskReminder = (LinkButton) e.Row.FindControl("lnkBtnTaskReminder");

                if (lnkBtnTaskReminder != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskReminder);
                }

                LinkButton lnkBtnTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnTaskResponse");

                if (lnkBtnTaskResponse != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskResponse);
                }

                LinkButton lnkBtnDeleteTask = (LinkButton) e.Row.FindControl("lnkBtnDeleteTask");

                if (lnkBtnDeleteTask != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteTask);
                }

                Label lblTaskStatus = e.Row.FindControl("lblTaskStatus") as Label;

                if (lblTaskStatus != null)
                {
                    HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;
                    
                    if (lblTaskStatus.Text != "" && lblTaskStatus.Text != "Open")
                    {
                        GridView gvTaskResponses = e.Row.FindControl("gvTaskResponses") as GridView;
                       
                        if (gvTaskResponses != null)
                        {
                            if (grdTaskActivity.DataKeys[e.Row.RowIndex].Value != null)
                            {
                                int taskID = 0;
                                taskID = Convert.ToInt32(grdTaskActivity.DataKeys[e.Row.RowIndex].Value);

                                var dataBindSuccess = BindTaskResponses(taskID, gvTaskResponses);

                                if (imgCollapseExpand != null)
                                {
                                    if (dataBindSuccess)
                                        imgCollapseExpand.Visible = true;
                                    else
                                        imgCollapseExpand.Visible = false;
                                }
                            }
                        }

                    }
                   else
                    {
                        if (imgCollapseExpand != null)
                            imgCollapseExpand.Visible = false;
                    }
                }
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        public bool BindTaskResponses(int taskID, GridView grd)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grd.DataSource = lstTaskResponses;
                    grd.DataBind();
                    return true;
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region Aws
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                        }
                                        int i = 0;
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                //string[] filename = file.Name.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];

                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                                }
                                            }
                                            i++;
                                        }
                                        

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        int i = 0;
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                //string[] filename = file.Name.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];

                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                cvTaskPanel.IsValid = false;
                                cvTaskPanel.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]), grdTaskActivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvTaskPanel.IsValid = false;
                        cvTaskPanel.ErrorMessage = "Response Deleted Successfully.";
                    }
                    else
                    {
                        cvTaskPanel.IsValid = false;
                        cvTaskPanel.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPanel.IsValid = false;
                cvTaskPanel.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #endregion
        
        #region Methods to Show Dashboard Counts

        public decimal GetOpenNoticeGuagePercentage()
        {
            decimal returnvalue = 0;

            int openNotice = Convert.ToInt32(divOpenNoticeCount.InnerText);

            int ClosedNotice = Convert.ToInt32(divClosedNoticeCount.InnerText);

            int total = openNotice + ClosedNotice;

            if (openNotice > 0)
            {
                if (ClosedNotice > 0)
                    returnvalue = ((Convert.ToDecimal(openNotice) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetClosedNoticeGuagePercentage()
        {
            decimal returnvalue = 0;

            int openNotice = Convert.ToInt32(divOpenNoticeCount.InnerText);
            int closedNotice = Convert.ToInt32(divClosedNoticeCount.InnerText);

            int total = openNotice + closedNotice;

            if (closedNotice > 0)
            {
                if (openNotice > 0)
                    returnvalue = ((Convert.ToDecimal(closedNotice) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetOpenCaseGuagePercentage()
        {
            decimal returnvalue = 0;

            int openCase = Convert.ToInt32(divOpenCaseCount.InnerText);

            int ClosedCase = Convert.ToInt32(divClosedCaseCount.InnerText);

            int total = openCase + ClosedCase;

            if (openCase > 0)
            {
                if (ClosedCase > 0)
                    returnvalue = ((Convert.ToDecimal(openCase) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetClosedCaseGuagePercentage()
        {
            decimal returnvalue = 0;

            int openCase = Convert.ToInt32(divOpenCaseCount.InnerText);

            int ClosedCase = Convert.ToInt32(divClosedCaseCount.InnerText);

            int total = openCase + ClosedCase;

            if (ClosedCase > 0)
            {
                if (openCase > 0)
                    returnvalue = ((Convert.ToDecimal(ClosedCase) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetOpenTaskGuagePercentage()
        {
            decimal returnvalue = 0;

            int openTask = Convert.ToInt32(divOpenTaskCount.InnerText);

            int ClosedTask = Convert.ToInt32(divClosedTaskCount.InnerText);

            int total = openTask + ClosedTask;

            if (total > 5)
                lnkShowMoreTask.Visible = true;
            else
                lnkShowMoreTask.Visible = false;

            if (openTask > 0)
            {
                if (ClosedTask > 0)
                    returnvalue = ((Convert.ToDecimal(openTask) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetClosedTaskGuagePercentage()
        {
            decimal returnvalue = 0;

            int openTask = Convert.ToInt32(divOpenTaskCount.InnerText);

            int ClosedTask = Convert.ToInt32(divClosedTaskCount.InnerText);

            int total = openTask + ClosedTask;

            if (ClosedTask > 0)
            {
                if (openTask > 0)
                    returnvalue = ((Convert.ToDecimal(ClosedTask) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        #endregion

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    int taskID = Convert.ToInt32(commandArgs[0]);
                    int noticeCaseInstanceID = Convert.ToInt32(commandArgs[1]);

                    if (taskID != 0 && noticeCaseInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + taskID + "," + noticeCaseInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvTaskPage.IsValid = false;
                //cvTaskPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string GetAssignedTo(long NoticeCaseInstanceID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var getuserdetails = (from row in entities.tbl_LegalCaseAssignment
                                              join  row1 in entities.Users
                                              on row.UserID equals row1.ID
                                              where row1.CustomerID == AuthenticationHelper.CustomerID
                                              && row.CaseInstanceID== NoticeCaseInstanceID
                                              && row.IsActive==true
                                              && row1.IsDeleted==false
                                              && row1.IsActive==true
                                              select row1.FirstName + " " + row1.LastName).ToList();
                    string result = string.Empty;
                    if (getuserdetails.Count>0)
                    {                      
                        foreach (var item in getuserdetails)
                        {
                            result += item + ',';
                        }
                    }
                    result = result.Trim(',');
                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }
       
        protected void btnCreateICS_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string finalitem = string.Empty;
            string CalendarItem = string.Empty;
            string FileName = "CalendarItem";

            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userid = AuthenticationHelper.UserID;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                          where row.CustomerID == customerid
                                          && row.ReminderDate != null
                                          && row.ReminderDate >= DateTime.Now
                                          && row.TxnStatusID != 3
                                          select row).ToList();

                if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                    viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID)).ToList(); //viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                else // In case of MGMT or CADMN 
                {
                    viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                }

                dt = viewHearingDetails.ToDataTable();
            }

            var listEmployee = dt;
            if (listEmployee.Rows.Count > 0)
            {
                //create a new stringbuilder instance
                StringBuilder sb = new StringBuilder();
                //start the calendar item
                sb.AppendLine("BEGIN:VCALENDAR");
                sb.AppendLine("VERSION:2.0");
                sb.AppendLine("PRODID:avantis.co.in");
                sb.AppendLine("CALSCALE:GREGORIAN");
                sb.AppendLine("METHOD:PUBLISH");
                foreach (DataRow item in listEmployee.Rows)
                {
                    DateTime DateStart = Convert.ToDateTime(item["ReminderDate"]);
                    DateTime DateEnd = DateStart.AddDays(1);
                    string CaseRefNo = item["CaseRefNo"].ToString();// item.Item1;
                    string CaseTitle = item["Title"].ToString();  //item.Item3;
                    string BranchName = item["BranchName"].ToString();// item.Item1;

                    //create a new stringbuilder instance
                    StringBuilder sbevent = new StringBuilder();
                    //add the event
                    sbevent.AppendLine("BEGIN:VEVENT");
                    sbevent.AppendLine("DTSTART:" + DateStart.ToString("yyyyMMddTHHmm00"));
                    sbevent.AppendLine("DTEND:" + DateEnd.ToString("yyyyMMddTHHmm00"));
                    sbevent.AppendLine("SUMMARY:" + CaseRefNo + "");
                    sbevent.AppendLine("LOCATION:" + BranchName + "");
                    sbevent.AppendLine("DESCRIPTION:" + CaseTitle + "");
                    sbevent.AppendLine("END:VEVENT");

                    //create a string from the stringbuilder
                    CalendarItem += sbevent.ToString();
                }

                var end = "END:VCALENDAR";
                finalitem = sb.ToString() + CalendarItem + end;

                Response.Buffer = true;
                Response.ClearHeaders();
                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "text/calendar";
                Response.AddHeader("content-length", finalitem.Length.ToString());
                Response.AddHeader("content-disposition", "attachment; filename=\"" + FileName + ".ics\"");
                Response.Write(finalitem);
                Response.Flush();
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }
        }
    }
}