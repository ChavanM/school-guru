﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Dashboard
{
    public partial class LitigationDashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                HiddenField home = (HiddenField) Master.FindControl("Ishome");
                home.Value = "true";

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var viewNoticeAssignedInstanceRecords = (from c in entities.View_NoticeAssignedInstance
                                                             where c.CustomerID == customerID
                                                             select c).ToList();

                    var ViewCaseAssignedInstanceRecords = (from c in entities.View_CaseAssignedInstance
                                                           where c.CustomerID == customerID
                                                           select c).ToList();

                    var ViewTaskRecords = (from row in entities.View_NoticeCaseTaskDetail
                                           where row.CustomerID == customerID
                                           && row.IsActive == true
                                           select row).ToList();

                    //Notice
                    divOpenNoticeCount.InnerText = NoticeManagement.GetAssignedNoticeCount(viewNoticeAssignedInstanceRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 1,Convert.ToInt32(AuthenticationHelper.CustomerID)).ToString();
                    divClosedNoticeCount.InnerText = NoticeManagement.GetAssignedNoticeCount(viewNoticeAssignedInstanceRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 3, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToString();

                    viewNoticeAssignedInstanceRecords.Clear();
                    viewNoticeAssignedInstanceRecords = null;

                    //Case
                    divOpenCaseCount.InnerText = CaseManagement.GetAssignedCaseCount(ViewCaseAssignedInstanceRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 1,Convert.ToInt32(AuthenticationHelper.CustomerID)).ToString();
                    divClosedCaseCount.InnerText = CaseManagement.GetAssignedCaseCount(ViewCaseAssignedInstanceRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 3, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToString();

                    ViewCaseAssignedInstanceRecords.Clear();
                    ViewCaseAssignedInstanceRecords = null;

                    //Task
                    divOpenTaskCount.InnerText = LitigationTaskManagement.GetAssignedTaskCount(ViewTaskRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 1).ToString();
                    divClosedTaskCount.InnerText = LitigationTaskManagement.GetAssignedTaskCount(ViewTaskRecords, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, 3).ToString();
                    var totalsum = (from row in entities.Sp_TotalLiability_user(customerID, AuthenticationHelper.UserID)
                                    select row).FirstOrDefault();

                    if (totalsum != null)
                    {
                        try
                        {
                            var a = decimal.Round((decimal)totalsum / 1000000, 2, MidpointRounding.AwayFromZero);
                            divTotalLiability.InnerText = $"{a:n2}" + " mn";
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            divTotalLiability.InnerText = "0";

                        }

                    }
                    //Task List - Open then Recent Due Date
                    BindTask(ViewTaskRecords);

                    ViewTaskRecords.Clear();
                    ViewTaskRecords = null;
                    
                }
            }
        }

        public int GetAssignedNoticeCount(List<View_NoticeAssignedInstance> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID, int noticeStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in masterRecords
                             select row).ToList();

                if (noticeStatus == 3) //3--Closed otherwise Open
                    query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                else
                    query = query.Where(entry => entry.TxnStatusID < 3).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == roleID).ToList();
                else
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                    if (query.Count > 0)
                    {
                        query = (from g in query
                                 group g by new
                                 {
                                     g.NoticeInstanceID,
                                     g.RefNo,
                                     g.NoticeTitle,
                                     g.NoticeDetailDesc,
                                     g.NoticeType,
                                     g.NoticeTypeName,
                                     g.CustomerID,
                                     g.CustomerBranchID,
                                     g.BranchName,
                                     g.DepartmentID,
                                     g.DeptName,
                                     g.PartyID,
                                     g.PartyName
                                 } into GCS
                                 select new View_NoticeAssignedInstance()
                                 {
                                     NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                     NoticeTitle = GCS.Key.NoticeTitle,
                                     NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                     NoticeTypeName = GCS.Key.NoticeTypeName,
                                     CustomerID = GCS.Key.CustomerID,
                                     CustomerBranchID = GCS.Key.CustomerBranchID,
                                     BranchName = GCS.Key.BranchName,
                                     DepartmentID = GCS.Key.DepartmentID,
                                     DeptName = GCS.Key.DeptName,
                                     PartyID = GCS.Key.PartyID,
                                     PartyName = GCS.Key.PartyName,
                                 }).ToList();
                    }
                }

                return query.Count();
            }
        }

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnTaskReminder = (LinkButton) e.Row.FindControl("lnkBtnTaskReminder");

                if (lnkBtnTaskReminder != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskReminder);
                }

                LinkButton lnkBtnTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnTaskResponse");

                if (lnkBtnTaskResponse != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskResponse);
                }

                LinkButton lnkBtnDeleteTask = (LinkButton) e.Row.FindControl("lnkBtnDeleteTask");

                if (lnkBtnDeleteTask != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteTask);
                }

                Label lblTaskStatus = e.Row.FindControl("lblTaskStatus") as Label;
                Label lblCTitle = e.Row.FindControl("lblCTitle") as Label;
                Label lblC = e.Row.FindControl("lblC") as Label;
                Label NoticeCaseInstanceID = e.Row.FindControl("NoticeCaseInstanceID") as Label;

                if (lblTaskStatus != null)
                {
                    lblCTitle.Text = LitigationTaskManagement.NoticeCaseTaskTitle(lblC.Text.ToLower(), Convert.ToInt32(NoticeCaseInstanceID.Text));
                    if (lblTaskStatus.Text != "" && lblTaskStatus.Text != "Open")
                    {
                        GridView gvTaskResponses = e.Row.FindControl("gvTaskResponses") as GridView;

                        if (gvTaskResponses != null)
                        {
                            if (grdTaskActivity.DataKeys[e.Row.RowIndex].Value != null)
                            {
                                int taskID = 0;
                                taskID = Convert.ToInt32(grdTaskActivity.DataKeys[e.Row.RowIndex].Value);



                                BindTaskResponses(taskID, gvTaskResponses);
                            }
                        }

                    }
                    else
                    {
                        HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;
                        //Image imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as Image;
                        if (imgCollapseExpand != null)
                            imgCollapseExpand.Visible = false;
                    }
                }
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        public void BindTask(List<View_NoticeCaseTaskDetail> masterRecord)
        {
            try
            {
                var lstTasks = LitigationTaskManagement.GetDashboardTaskList(masterRecord, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3);

                grdTaskActivity.DataSource = lstTasks;
                grdTaskActivity.DataBind();

                if (lstTasks.Count > 5)
                    lnkShowMoreTask.Visible = true;
                else
                    lnkShowMoreTask.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTaskResponses(int taskID, GridView grd)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grd.DataSource = lstTaskResponses;
                    grd.DataBind();
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                        }

                                        int i = 0;
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                //string[] filename = file.Name.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];

                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));                                                    
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        int i = 0;
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                //string[] filename = file.Name.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];

                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                cvTaskPanel.IsValid = false;
                                cvTaskPanel.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]), grdTaskActivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvTaskPanel.IsValid = false;
                        cvTaskPanel.ErrorMessage = "Response Deleted Successfully.";
                    }
                    else
                    {
                        cvTaskPanel.IsValid = false;
                        cvTaskPanel.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPanel.IsValid = false;
                cvTaskPanel.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        #region Methods to Show Dashboard Counts
        public decimal GetOpenNoticeGuagePercentage()
        {
            decimal returnvalue = 0;

            int openNotice = Convert.ToInt32(divOpenNoticeCount.InnerText);

            int ClosedNotice = Convert.ToInt32(divClosedNoticeCount.InnerText);

            int total = openNotice + ClosedNotice;

            if (openNotice > 0)
            {
                if (ClosedNotice > 0)
                    returnvalue = ((Convert.ToDecimal(openNotice) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetClosedNoticeGuagePercentage()
        {
            decimal returnvalue = 0;

            int openNotice = Convert.ToInt32(divOpenNoticeCount.InnerText);
            int closedNotice = Convert.ToInt32(divClosedNoticeCount.InnerText);

            int total = openNotice + closedNotice;

            if (closedNotice > 0)
            {
                if (openNotice > 0)
                    returnvalue = ((Convert.ToDecimal(closedNotice) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetOpenCaseGuagePercentage()
        {
            decimal returnvalue = 0;

            int openCase = Convert.ToInt32(divOpenCaseCount.InnerText);

            int ClosedCase = Convert.ToInt32(divClosedCaseCount.InnerText);

            int total = openCase + ClosedCase;

            if (openCase > 0)
            {
                if (ClosedCase > 0)
                    returnvalue = ((Convert.ToDecimal(openCase) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetClosedCaseGuagePercentage()
        {
            decimal returnvalue = 0;

            int openCase = Convert.ToInt32(divOpenCaseCount.InnerText);

            int ClosedCase = Convert.ToInt32(divClosedCaseCount.InnerText);

            int total = openCase + ClosedCase;

            if (ClosedCase > 0)
            {
                if (openCase > 0)
                    returnvalue = ((Convert.ToDecimal(ClosedCase) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetOpenTaskGuagePercentage()
        {
            decimal returnvalue = 0;

            int openTask = Convert.ToInt32(divOpenTaskCount.InnerText);

            int ClosedTask = Convert.ToInt32(divClosedTaskCount.InnerText);

            int total = openTask + ClosedTask;

            if (openTask > 0)
            {
                if (ClosedTask > 0)
                    returnvalue = ((Convert.ToDecimal(openTask) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        public decimal GetClosedTaskGuagePercentage()
        {
            decimal returnvalue = 0;

            int openTask = Convert.ToInt32(divOpenTaskCount.InnerText);

            int ClosedTask = Convert.ToInt32(divClosedTaskCount.InnerText);

            int total = openTask + ClosedTask;

            if (ClosedTask > 0)
            {
                if (openTask > 0)
                    returnvalue = ((Convert.ToDecimal(ClosedTask) / Convert.ToDecimal(total)) * 100);
                else
                    returnvalue = 100;
            }

            return returnvalue;
        }

        #endregion
        
        protected void btnCreateICS_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string finalitem = string.Empty;
            string CalendarItem = string.Empty;
            string FileName = "CalendarItem";

            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userid = AuthenticationHelper.UserID;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                          where row.CustomerID == customerid
                                          && row.ReminderDate != null
                                          && row.ReminderDate >= DateTime.Now
                                          && row.TxnStatusID != 3
                                          select row).ToList();

                if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                    viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID)).ToList(); //viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                else // In case of MGMT or CADMN 
                {
                    viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                }

                dt = viewHearingDetails.ToDataTable();
            }

            var listEmployee = dt;
            if (listEmployee.Rows.Count > 0)
            {
                //create a new stringbuilder instance
                StringBuilder sb = new StringBuilder();
                //start the calendar item
                sb.AppendLine("BEGIN:VCALENDAR");
                sb.AppendLine("VERSION:2.0");
                sb.AppendLine("PRODID:avantis.co.in");
                sb.AppendLine("CALSCALE:GREGORIAN");
                sb.AppendLine("METHOD:PUBLISH");
                foreach (DataRow item in listEmployee.Rows)
                {
                    DateTime DateStart = Convert.ToDateTime(item["ReminderDate"]);
                    DateTime DateEnd = DateStart.AddDays(1);
                    string CaseRefNo = item["CaseRefNo"].ToString();// item.Item1;
                    string CaseTitle = item["Title"].ToString();  //item.Item3;
                    string BranchName = item["BranchName"].ToString();// item.Item1;

                    //create a new stringbuilder instance
                    StringBuilder sbevent = new StringBuilder();
                    //add the event
                    sbevent.AppendLine("BEGIN:VEVENT");
                    sbevent.AppendLine("DTSTART:" + DateStart.ToString("yyyyMMddTHHmm00"));
                    sbevent.AppendLine("DTEND:" + DateEnd.ToString("yyyyMMddTHHmm00"));
                    sbevent.AppendLine("SUMMARY:" + CaseRefNo + "");
                    sbevent.AppendLine("LOCATION:" + BranchName + "");
                    sbevent.AppendLine("DESCRIPTION:" + CaseTitle + "");
                    sbevent.AppendLine("END:VEVENT");

                    //create a string from the stringbuilder
                    CalendarItem += sbevent.ToString();
                }

                var end = "END:VCALENDAR";
                finalitem = sb.ToString() + CalendarItem + end;

                Response.Buffer = true;
                Response.ClearHeaders();
                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "text/calendar";
                Response.AddHeader("content-length", finalitem.Length.ToString());
                Response.AddHeader("content-disposition", "attachment; filename=\"" + FileName + ".ics\"");
                Response.Write(finalitem);
                Response.Flush();
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }
        }

        protected void lnkShowHearingDetail_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btn = (ImageButton) (sender);
                if (btn != null)
                {
                    string[] commandArg = btn.CommandArgument.ToString().Split(',');

                    int noticeCaseInstanceID = Convert.ToInt32(commandArg[0]);
                    //string type = commandArg[1];
                    string type = "C";

                    if (noticeCaseInstanceID != 0 && !string.IsNullOrEmpty(type))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowNoticeCaseDialog(" + noticeCaseInstanceID + ",'" + type + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
