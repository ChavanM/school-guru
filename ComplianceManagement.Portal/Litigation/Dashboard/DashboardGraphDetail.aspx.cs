﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Dashboard
{
    public partial class DashboardGraphDetail : System.Web.UI.Page
    {
        protected bool flag;
        public bool CaseResult;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                CaseResult = CaseManagement.CheckForClient(Convert.ToInt32(customerID), "CaseNoticeLabel");  
                BindCustomerBranches();
                BindParty(customerID);
                BindDepartment();
                flag = false;
                if (!string.IsNullOrEmpty(Request.QueryString["NOC"]))
                {
                    ddlTypePage.SelectedValue = Request.QueryString["NOC"];  // B-Both, N-Notice, C-Case
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    ddlNoticeTypePage.SelectedValue = Request.QueryString["Type"];  // B-Both, I-Inward, O-Outward
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    ddlStatus.SelectedValue = Request.QueryString["Status"]; //Open Closed
                }

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    tvFilterLocation.SelectedNode.Value = Request.QueryString["BID"];
                    tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ddlPartyPage.SelectedValue = Request.QueryString["PID"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["DID"]))
                {
                    ddlDeptPage.SelectedValue = Request.QueryString["DID"];
                }
                
                BindData();
                bindPageNumber();
                ShowGridDetail();
                upDivLocation_Load(sender, e);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvFilterLocation.Nodes.Clear();
                NameValueHierarchy branch = null;

                // var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                List<NameValueHierarchy> branches;
                string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                if (CacheHelper.Exists(key))
                {
                    CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                }
                else
                {
                    branches = CustomerBranchManagement.GetAllHierarchy(customerID);
                    CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                }
                if (branches.Count > 0)
                {
                    branch = branches[0];
                }

                tbxFilterLocation.Text = "Select Entity/Location";

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    tvFilterLocation.Nodes.Add(item);
                }

                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvCasePopUp.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindParty(long customerID)
        {
            var obj = LitigationLaw.GetLCPartyDetails(customerID);

            //Drop-Down at Page
            ddlPartyPage.DataTextField = "Name";
            ddlPartyPage.DataValueField = "ID";

            ddlPartyPage.DataSource = obj;
            ddlPartyPage.DataBind();

            ddlPartyPage.Items.Insert(0, new ListItem("Select Opponent", "-1"));
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("Select Department", "-1"));
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdDetailData"] != null)
                    {
                        String FileName = String.Empty;

                        FileName = "LitigationReport";

                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;

                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                        ExcelData = view.ToTable("Selected", false, "TypeName", "NoticeTypeName", "NoticeTitle", "NoticeDetailDesc", "RefNo", "InternalCaseNo", "NoticeCategory", "PartyName", "BranchName", "DeptName", "NoticeRiskID", "TotalPayment", "CaseStage", "Status", "NoticeResult");

                        if (ExcelData.Rows.Count > 0)
                        {

                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);
                            ExcelData.Columns.Add("WinningProspect");

                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (item["NoticeRiskID"] != null)
                                {
                                    if (item["NoticeRiskID"].ToString() == "1")
                                        item["WinningProspect"] = "High";
                                    else if (item["NoticeRiskID"].ToString() == "2")
                                        item["WinningProspect"] = "Medium";
                                    else if (item["NoticeRiskID"].ToString() == "3")
                                        item["WinningProspect"] = "Low";
                                }
                            }

                            ExcelData.Columns.Remove("NoticeRiskID");

                            var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = customer.Name;
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;
                            }

                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "Litigation Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Notice / Case";
                            exWorkSheet.Cells["B5"].AutoFitColumns(15);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "Notice/Case Type";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(25);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "Description";
                            exWorkSheet.Cells["E5"].AutoFitColumns(50);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Notice/Case No";
                            exWorkSheet.Cells["F5"].AutoFitColumns(25);

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Internal Notice/Case No";
                            exWorkSheet.Cells["G5"].AutoFitColumns(25);

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "Category";
                            exWorkSheet.Cells["H5"].AutoFitColumns(25);

                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["I5"].Value = "Opposition/Party";
                            exWorkSheet.Cells["I5"].AutoFitColumns(25);

                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["J5"].Value = "Branch";
                            exWorkSheet.Cells["J5"].AutoFitColumns(25);

                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["K5"].Value = "Department";
                            exWorkSheet.Cells["K5"].AutoFitColumns(15);

                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["L5"].Value = "Expense(Total)";
                            exWorkSheet.Cells["L5"].AutoFitColumns(15);

                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["M5"].Value = "Stage";
                            exWorkSheet.Cells["M5"].AutoFitColumns(15);

                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["N5"].Value = "Status";
                            exWorkSheet.Cells["N5"].AutoFitColumns(15);

                            if(CaseResult == true)
                            {
                                exWorkSheet.Cells["O5"].Value = "Result(Win/Loss/In-Progress/Partial)";
                            }
                            else
                            {
                                exWorkSheet.Cells["O5"].Value = "Result(Win/Loss/In-Progress)";
                            }
                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["O5"].AutoFitColumns(15);

                            exWorkSheet.Cells["P5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["P5"].Value = "Winning Prospect";
                            exWorkSheet.Cells["P5"].AutoFitColumns(15);                            

                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 16])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        else
                        {
                            cvGraphDetail.IsValid = false;
                            cvGraphDetail.ErrorMessage = "No data available to export for current selected values";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindData()
        {
            try
            {
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int catID = -1;
                int riskID = -1;
                int stageID = -1;
                int ageingID = -1;
                int noticeCaseStatus = -1;
                string noticeCase = string.Empty;
                string noticeCaseType = string.Empty;

                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                //Filters
                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    if (ddlTypePage.SelectedValue != "-1")
                    {
                        noticeCase = ddlTypePage.SelectedValue;  // B-Both, N-Notice, C-Case
                    }
                }

                if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                {
                    if (ddlNoticeTypePage.SelectedValue != "-1")
                    {
                        noticeCaseType = ddlNoticeTypePage.SelectedValue;  // B-Both, I-Inward, O-Outward
                    }
                }

                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (ddlStatus.SelectedValue != "-1")
                    {
                        noticeCaseStatus = Convert.ToInt32(Request.QueryString["Status"]); //Open Closed
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                {
                    if (ddlPartyPage.SelectedValue != "-1")
                    {
                        partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    if (ddlDeptPage.SelectedValue != "-1")
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    catID = Convert.ToInt32(Request.QueryString["CID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["RID"]))
                {
                    riskID = Convert.ToInt32(Request.QueryString["RID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    stageID = Convert.ToInt32(Request.QueryString["SID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["AID"]))
                {
                    ageingID = Convert.ToInt32(Request.QueryString["AID"]);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                GetNoticeCaseGraphData(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, noticeCase, noticeCaseType, noticeCaseStatus, branchList, partyID, deptID, catID, riskID, stageID, ageingID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetNoticeCaseGraphData(long custID, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeOrCase, string noticeCaseType, int noticeCaseStatus, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int ageingID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var viewRecords = (from record in entities.View_LitigationDashboard
                                       where record.CustomerID == custID
                                       select record).ToList();

                    //if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    //    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                    //else
                    //    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                    if (!String.IsNullOrEmpty(noticeOrCase))
                    {
                        if (noticeOrCase != "-1" && noticeOrCase != "B")//Both(B), Notice(1) and Case(2)
                        {
                            viewRecords = viewRecords.Where(entry => entry.Type == noticeOrCase).ToList();
                        }
                    }

                    if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                        viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                    if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                    {
                        if (noticeCaseStatus == 3) //3--Closed otherwise Open
                            viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                        else
                            viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                    }

                    //if (branchID != -1)
                    //    viewRecords = viewRecords.Where(entry => entry.CustomerBranchID == branchID).ToList();

                    if (branchList.Count > 0)
                        viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    //if (partyID != -1)
                    //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                    if (deptID != -1)
                        viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (catID != -1)
                        viewRecords = viewRecords.Where(entry => entry.NoticeCategoryID == catID).ToList();

                    if (riskID != -1 && riskID != 0)
                        viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                    if (stageID != -1)
                        viewRecords = viewRecords.Where(entry => entry.CaseStageID == stageID).ToList();

                    if (ageingID != -1 && ageingID != 0)
                    {
                        if (ageingID == 1)
                            viewRecords = viewRecords.Where(entry => entry.AgeingDays <= 365).ToList();
                        else if (ageingID == 2)
                            viewRecords = viewRecords.Where(entry => entry.AgeingDays > 365 && entry.AgeingDays <= 730).ToList();
                        else if (ageingID == 3)
                            viewRecords = viewRecords.Where(entry => entry.AgeingDays > 730 && entry.AgeingDays <= 1095).ToList();
                        else if (ageingID == 4)
                            viewRecords = viewRecords.Where(entry => entry.AgeingDays > 1095).ToList();
                    }
                    //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
                    //if (AuthenticationHelper.Role == "MGMT" && IsEntityassignmentCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (isexist != null)
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,-1);
                        if (caseList.Count > 0)
                        {
                            viewRecords = viewRecords.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeInstanceID))).ToList();
                        }
                        else
                        {
                            viewRecords = viewRecords.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeInstanceID))).ToList();
                        }
                    }
                    if (viewRecords.Count > 0)
                    {
                        // In case of MGMT or CADMN 

                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            viewRecords = viewRecords.Where(entry => entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3).ToList();
                        else
                        {
                            // In case of MGMT or CADMN

                            viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList();

                            if (viewRecords.Count > 0)
                            {
                                viewRecords = (from g in viewRecords
                                               group g by new
                                               {
                                                   g.RefNo,
                                                   g.InternalCaseNo,
                                                   g.NoticeInstanceID,
                                                   g.NoticeRiskID,
                                                   g.NoticeType,
                                                   g.NoticeTypeName,
                                                   g.TxnStatusID,
                                                   g.Status,
                                                   g.TotalPayment,
                                                   g.NoticeTitle,
                                                   g.NoticeDetailDesc,
                                                   g.Type,
                                                   g.TypeName,
                                                   g.CustomerID,
                                                   g.CustomerBranchID,
                                                   g.BranchName,
                                                   g.DepartmentID,
                                                   g.DeptName,
                                                   g.PartyID,
                                                   g.PartyName,
                                                   g.NoticeCategoryID,
                                                   g.NoticeCategory,
                                                   g.CaseStageID,
                                                   g.CaseStage,
                                                   g.NoticeResult
                                               } into GCS
                                               select new View_LitigationDashboard()
                                               {
                                                   RefNo = GCS.Key.RefNo,
                                                   InternalCaseNo = GCS.Key.InternalCaseNo,
                                                   NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                                   NoticeRiskID = GCS.Key.NoticeRiskID,
                                                   NoticeType = GCS.Key.NoticeType,
                                                   NoticeTypeName = GCS.Key.NoticeTypeName,
                                                   TxnStatusID = GCS.Key.TxnStatusID,
                                                   Status = GCS.Key.Status,
                                                   TotalPayment = GCS.Key.TotalPayment,
                                                   NoticeTitle = GCS.Key.NoticeTitle,
                                                   NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                                   Type = GCS.Key.Type,
                                                   TypeName = GCS.Key.TypeName,
                                                   CustomerID = GCS.Key.CustomerID,
                                                   CustomerBranchID = GCS.Key.CustomerBranchID,
                                                   BranchName = GCS.Key.BranchName,
                                                   DepartmentID = GCS.Key.DepartmentID,
                                                   DeptName = GCS.Key.DeptName,
                                                   PartyID = GCS.Key.PartyID,
                                                   PartyName = GCS.Key.PartyName,
                                                   NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                                   NoticeCategory = GCS.Key.NoticeCategory,
                                                   CaseStageID = GCS.Key.CaseStageID,
                                                   CaseStage = GCS.Key.CaseStage,
                                                   NoticeResult = GCS.Key.NoticeResult,
                                               }).ToList();
                            }
                        }
                    }
                    Session["TotalRows"] = null;
                    grdNoticeCaseDetails.DataSource = viewRecords;
                    grdNoticeCaseDetails.DataBind();

                    Session["TotalRows"] = viewRecords.Count;
                    Session["grdDetailData"] = (grdNoticeCaseDetails.DataSource as List<View_LitigationDashboard>).ToDataTable();

                    //bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdNoticeCaseDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdNoticeCaseDetails.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Page Number-Bottom

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());

            grdNoticeCaseDetails.PageIndex = chkSelectedPage - 1;

            grdNoticeCaseDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindData();
            ShowGridDetail();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion

        protected void lnkShowDetail_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btn = (ImageButton) (sender);
                if (btn != null)
                {
                    string[] commandArg = btn.CommandArgument.ToString().Split(',');

                    int noticeCaseInstanceID = Convert.ToInt32(commandArg[0]);
                    string type = commandArg[1];

                    if (noticeCaseInstanceID != 0 && !string.IsNullOrEmpty(type))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + noticeCaseInstanceID + ",'" + type + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        

        protected void btnApplyTo_Click(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
            ShowGridDetail();
            upDivLocation_Load(sender, e);
        }

        protected void grdNoticeCaseDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int catID = -1;
                int riskID = -1;
                int stageID = -1;
                int ageingID = -1;
                int noticeCaseStatus = -1;
                string noticeCase = string.Empty;
                string noticeCaseType = string.Empty;

                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                //Filters
                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    if (ddlTypePage.SelectedValue != "-1")
                    {
                        noticeCase = ddlTypePage.SelectedValue;  // B-Both, N-Notice, C-Case
                    }
                }

                if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                {
                    if (ddlNoticeTypePage.SelectedValue != "-1")
                    {
                        noticeCaseType = ddlNoticeTypePage.SelectedValue;  // B-Both, I-Inward, O-Outward
                    }
                }

                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (ddlStatus.SelectedValue != "-1")
                    {
                        noticeCaseStatus = Convert.ToInt32(Request.QueryString["Status"]); //Open Closed
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                {
                    if (ddlPartyPage.SelectedValue != "-1")
                    {
                        partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    if (ddlDeptPage.SelectedValue != "-1")
                    {
                        deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    catID = Convert.ToInt32(Request.QueryString["CID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["RID"]))
                {
                    riskID = Convert.ToInt32(Request.QueryString["RID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    stageID = Convert.ToInt32(Request.QueryString["SID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["AID"]))
                {
                    ageingID = Convert.ToInt32(Request.QueryString["AID"]);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                //GetNoticeCaseGraphData(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, noticeCase, noticeCaseType, noticeCaseStatus, branchList, partyID, deptID, catID, riskID, stageID, ageingID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var viewRecords = (from record in entities.View_LitigationDashboard
                                       where record.CustomerID == customerID
                                       select record).ToList();
                    
                    if (!String.IsNullOrEmpty(noticeCase))
                    {
                        if (noticeCase != "-1" && noticeCase != "B")//Both(B), Notice(1) and Case(2)
                        {
                            viewRecords = viewRecords.Where(entry => entry.Type == noticeCase).ToList();
                        }
                    }

                    if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                        viewRecords = viewRecords.Where(entry => entry.NoticeType == noticeCaseType).ToList();

                    if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                    {
                        if (noticeCaseStatus == 3) //3--Closed otherwise Open
                            viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                        else
                            viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                    }
                    
                    if (branchList.Count > 0)
                        viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                    
                    if (deptID != -1)
                        viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (catID != -1)
                        viewRecords = viewRecords.Where(entry => entry.NoticeCategoryID == catID).ToList();

                    if (riskID != -1 && riskID != 0)
                        viewRecords = viewRecords.Where(entry => entry.NoticeRiskID == riskID).ToList();

                    if (stageID != -1)
                        viewRecords = viewRecords.Where(entry => entry.CaseStageID == stageID).ToList();

                    if (ageingID != -1 && ageingID != 0)
                    {
                        if (ageingID == 1)
                            viewRecords = viewRecords.Where(entry => entry.AgeingDays <= 365).ToList();
                        else if (ageingID == 2)
                            viewRecords = viewRecords.Where(entry => entry.AgeingDays > 365 && entry.AgeingDays <= 730).ToList();
                        else if (ageingID == 3)
                            viewRecords = viewRecords.Where(entry => entry.AgeingDays > 730 && entry.AgeingDays <= 1095).ToList();
                        else if (ageingID == 4)
                            viewRecords = viewRecords.Where(entry => entry.AgeingDays > 1095).ToList();
                    }

                    if (viewRecords.Count > 0)
                    {
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            viewRecords = viewRecords.Where(entry => entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3).ToList();
                        else
                        {
                            viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList();

                            if (viewRecords.Count > 0)
                            {
                                viewRecords = (from g in viewRecords
                                               group g by new
                                               {
                                                   g.RefNo,
                                                   g.InternalCaseNo,
                                                   g.NoticeInstanceID,
                                                   g.NoticeRiskID,
                                                   g.NoticeType,
                                                   g.NoticeTypeName,
                                                   g.TxnStatusID,
                                                   g.Status,
                                                   g.TotalPayment,
                                                   g.NoticeTitle,
                                                   g.NoticeDetailDesc,
                                                   g.Type,
                                                   g.TypeName,
                                                   g.CustomerID,
                                                   g.CustomerBranchID,
                                                   g.BranchName,
                                                   g.DepartmentID,
                                                   g.DeptName,
                                                   g.PartyID,
                                                   g.PartyName,
                                                   g.NoticeCategoryID,
                                                   g.NoticeCategory,
                                                   g.CaseStageID,
                                                   g.CaseStage
                                               } into GCS
                                               select new View_LitigationDashboard()
                                               {
                                                   RefNo = GCS.Key.RefNo,
                                                   InternalCaseNo = GCS.Key.InternalCaseNo,
                                                   NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                                   NoticeRiskID = GCS.Key.NoticeRiskID,
                                                   NoticeType = GCS.Key.NoticeType,
                                                   NoticeTypeName = GCS.Key.NoticeTypeName,
                                                   TxnStatusID = GCS.Key.TxnStatusID,
                                                   Status = GCS.Key.Status,
                                                   TotalPayment = GCS.Key.TotalPayment,
                                                   NoticeTitle = GCS.Key.NoticeTitle,
                                                   NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                                   Type = GCS.Key.Type,
                                                   TypeName = GCS.Key.TypeName,
                                                   CustomerID = GCS.Key.CustomerID,
                                                   CustomerBranchID = GCS.Key.CustomerBranchID,
                                                   BranchName = GCS.Key.BranchName,
                                                   DepartmentID = GCS.Key.DepartmentID,
                                                   DeptName = GCS.Key.DeptName,
                                                   PartyID = GCS.Key.PartyID,
                                                   PartyName = GCS.Key.PartyName,
                                                   NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                                   NoticeCategory = GCS.Key.NoticeCategory,
                                                   CaseStageID = GCS.Key.CaseStageID,
                                                   CaseStage = GCS.Key.CaseStage,
                                               }).ToList();
                            }
                        }
                    }
                    if (direction == SortDirection.Ascending)
                    {
                        viewRecords = viewRecords.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        viewRecords = viewRecords.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                    }

                    foreach (DataControlField field in grdNoticeCaseDetails.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["SortIndex"] = grdNoticeCaseDetails.Columns.IndexOf(field);
                        }
                    }
                    Session["TotalRows"] = null;
                    flag = true;
                    grdNoticeCaseDetails.DataSource = viewRecords;
                    grdNoticeCaseDetails.DataBind();

                    Session["TotalRows"] = viewRecords.Count;
                    Session["grdDetailData"] = (grdNoticeCaseDetails.DataSource as List<View_LitigationDashboard>).ToDataTable();

                    ShowGridDetail();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdNoticeCaseDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (flag == true)
            {
                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "/Images/up_arrow1.png";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "/Images/down_arrow1.png";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
        }

        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
        }
    }
}