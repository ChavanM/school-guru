﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Documents
{
    public partial class MyDocumentLitigationNew : System.Web.UI.Page
    {
        protected static string Path;
        protected static string FlagIsApp;
        protected static int CustId;
        protected static int UId;
        protected static string Authorization;
        protected string IsAdvocateBillShow;
        protected static string customerId;
        public bool NewColumnsLitigation;
        protected bool FYCustID;
        protected void Page_Load(object sender, EventArgs e)
        {
            NewColumnsLitigation = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "LitigationNewColumn");
            FYCustID = CaseManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "CaseNoticeLabel");
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            FlagIsApp = AuthenticationHelper.Role;
            IsAdvocateBillShow = Convert.ToString(ConfigurationManager.AppSettings["IsAdvocateBill"]);
            customerId = Convert.ToString(AuthenticationHelper.CustomerID);

        }
    }
}