﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentOverview.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common.DocumentOverview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>



    <script type="text/javascript">

        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };
        function fopendocfileReview(file) {
            $('#divViewDocument').modal('show');
            $('#docViewerStatutory').attr('src', "../docviewer.aspx?docurl=" + file);
        }

         function fopendocfileReviewPopUp() {
            $('#divViewDocument').modal('show');
        }
        function fopendocfileLitigation(file) {

            $('#divViewDocument').modal('show');
            $('#docViewerLitigation').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div id="divViewDocument" class="modal fade" style="width: 100%; background: #fff;">
                <div style="float: left; width: 10%">
                     <table width="100%" style="text-align: left; margin-left: 10%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="upLitigationDetails" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptLitigationVersionView" runat="server" OnItemCommand="rptLitigationVersionView_ItemCommand"
                                                            OnItemDataBound="rptLitigationVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th><b>File Name</b></th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <%#Container.ItemIndex+1 %>.
                                                                        <asp:UpdatePanel ID="UpLinkbuttonbold" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("NoticeCaseInstanceID") + ","+ Eval("FileName")+ ","+ Eval("DocTypeInstanceID") %>'
                                                                                    ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName")%>'
                                                                                    Text='<%# Eval("FileName").ToString().Trim().Length > 18 ? Eval("FileName").ToString().Substring(0,18) + "..." : Eval("FileName").ToString().Trim() %>'></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                </div>
                <div style="float: left; width: 90%">
                       <asp:UpdatePanel ID="updateButtonaa" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="docViewerLitigation" runat="server" width="100%" height="530px"></iframe>
                                        </fieldset>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
