﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class DocumentOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["InstanceID"])
                && !string.IsNullOrEmpty(Request.QueryString["NoticeCaseInstanceID"])
                && !string.IsNullOrEmpty(Request.QueryString["DocType"])
                && !string.IsNullOrEmpty(Request.QueryString["DocTypeDownload"]))
                {
                    BindTransactionDetails();
                }
            }
        }

        protected void rptLitigationVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string LitigatinoDocPath = string.Empty;
                string NoticeCaseInstanceID = Request.QueryString["NoticeCaseInstanceID"];
                string DocType = Request.QueryString["DocType"];
                string DocTypeDownload = Request.QueryString["DocTypeDownload"];

                if (DocType == "N" || DocType == "C")
                {
                    string TypeName = string.Empty;
                    List<string> Doctypes = new List<string>();

                    if (DocTypeDownload != "-1")
                    {
                        if (DocTypeDownload == "A")
                        {
                            if (DocType == "N")
                            {
                                Doctypes.Add("A");
                                Doctypes.Add("N");
                                Doctypes.Add("NR");
                                Doctypes.Add("NT");
                                TypeName = "Notice Document";
                            }
                            else
                            {
                                Doctypes.Add("A");
                                Doctypes.Add("C");
                                Doctypes.Add("CH");
                                Doctypes.Add("CT");
                                Doctypes.Add("CA");
                                TypeName = "Case Document";
                            }
                        }
                        else
                        {
                            Doctypes.Add(DocTypeDownload);
                        }
                    }

                    if (DocType == "N")
                    {
                        TypeName = "Notice Document";
                    }
                    else
                    {
                        TypeName = "Case Document";
                    }
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<tbl_LitigationFileData> LitigationFileData = new List<tbl_LitigationFileData>();
                    List<tbl_LitigationFileData> LitigationDocument = new List<tbl_LitigationFileData>();
                    //if (ddlTypePage.SelectedValue == "T")
                    //{
                    //    LitigationDocument = LitigationDocumentManagement.GetTaskRelatedDoc(Convert.ToInt32(commandArgs[2]));
                    //}
                    //else
                    //{
                    LitigationDocument = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(NoticeCaseInstanceID), Doctypes);
                    //}

                    if (commandArgs[1].Equals("1.0"))
                    {
                        LitigationFileData = LitigationDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                        if (LitigationFileData.Count <= 0)
                        {
                            LitigationFileData = LitigationDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        LitigationFileData = LitigationDocument.Where(entry => entry.FileName == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("View"))
                    {
                        if (LitigationFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in LitigationFileData)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS
                                    if (file.FilePath != null)
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                        string directoryPath = "~/TempDocuments/AWS/" + User;

                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }
                                        string filePath1 = directoryPath + "/" + file.FileName;
                                        LitigatinoDocPath = filePath1;
                                        LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                        lblMessage.Text = "";                                        
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        i++;
                                        LitigatinoDocPath = FileName;

                                        LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
                else {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<tbl_LitigationFileData> LitigationFileData = new List<tbl_LitigationFileData>();
                    List<tbl_LitigationFileData> LitigationDocument = new List<tbl_LitigationFileData>();
                    LitigationDocument = LitigationDocumentManagement.GetTaskRelatedDoc(Convert.ToInt32(commandArgs[2]));
                    if (commandArgs[1].Equals("1.0"))
                    {
                        LitigationFileData = LitigationDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                        if (LitigationFileData.Count <= 0)
                        {
                            LitigationFileData = LitigationDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        LitigationFileData = LitigationDocument.Where(entry => entry.FileName == commandArgs[1]).ToList();
                    }
                    if (e.CommandName.Equals("View"))
                    {
                        if (LitigationFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in LitigationFileData)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS
                                    if (file.FilePath != null)
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                        string directoryPath = "~/TempDocuments/AWS/" + User;

                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }
                                        string filePath1 = directoryPath + "/" + file.FileName;
                                        LitigatinoDocPath = filePath1;
                                        lblMessage.Text = "";                                        
                                        LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        i++;
                                        LitigatinoDocPath = FileName;

                                        LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptLitigationVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblIDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);

            }
        }

        public void BindTransactionDetails()
        {
            string LitigatinoDocPath = string.Empty;
            string TaskId = Request.QueryString["InstanceID"];
            string NoticeCaseInstanceID = Request.QueryString["NoticeCaseInstanceID"];
            string DocType = Request.QueryString["DocType"];
            string DocTypeDownload = Request.QueryString["DocTypeDownload"];

            if (DocType == "N" || DocType == "C")
            {
                string TypeName = string.Empty;
                List<string> Doctypes = new List<string>();

                if (DocTypeDownload != "-1")
                {
                    if (DocTypeDownload == "A")
                    {
                        if (DocType == "N")
                        {
                            Doctypes.Add("A");
                            Doctypes.Add("N");
                            Doctypes.Add("NR");
                            Doctypes.Add("NT");
                            TypeName = "Notice Document";
                        }
                        else
                        {
                            Doctypes.Add("A");
                            Doctypes.Add("C");
                            Doctypes.Add("CH");
                            Doctypes.Add("CT");
                            Doctypes.Add("CA");
                            TypeName = "Case Document";
                        }
                    }
                    else
                    {
                        Doctypes.Add(DocTypeDownload);
                    }
                }

                var CheckType = string.Empty;
                var NameOfDocu = string.Empty;

                //Doctypes.Add("C");
                //Doctypes.Add("CH");
                //Doctypes.Add("CT");
                if (DocType == "C")
                {
                    TypeName = "Case Document";
                }
                else if (DocType == "N")
                {
                    TypeName = "Notice Document";
                }
               

                List<tbl_LitigationFileData> CMPDocuments = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(NoticeCaseInstanceID), Doctypes);

                if (CMPDocuments.Count > 0)
                {
                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS
                        List<tbl_LitigationFileData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                            entityData.Version = "1.0";
                            entityData.NoticeCaseInstanceID = Convert.ToInt64(NoticeCaseInstanceID);
                            entityData.DocTypeInstanceID = 0;
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptLitigationVersionView.DataBind();

                            foreach (var file in CMPDocuments)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }
                                string filePath1 = directoryPath + "/" + file.FileName;
                                LitigatinoDocPath = filePath1;
                                LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                lblMessage.Text = "";
                                break;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal
                        List<tbl_LitigationFileData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                            entityData.Version = "1.0";
                            entityData.NoticeCaseInstanceID = Convert.ToInt64(NoticeCaseInstanceID);
                            entityData.DocTypeInstanceID = 0;
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptLitigationVersionView.DataBind();

                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles/Litigation";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();

                                    LitigatinoDocPath = FileName;
                                    LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                                }
                                break;
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                }
            }
            else
            {
                var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(Convert.ToInt64(TaskId), Convert.ToInt64(NoticeCaseInstanceID));
                if (lstTaskDocument != null)
                {
                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS
                        List<tbl_LitigationFileData> CMPDocuments = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Count > 0)
                        {
                            List<tbl_LitigationFileData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                entityData.Version = "1.0";
                                entityData.NoticeCaseInstanceID = Convert.ToInt64(NoticeCaseInstanceID);
                                entityData.DocTypeInstanceID = 0;
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptLitigationVersionView.DataBind();

                                foreach (var file in CMPDocuments)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    string filePath1 = directoryPath + "/" + file.FileName;
                                    LitigatinoDocPath = filePath1;
                                    LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                    lblMessage.Text = "";
                                    
                                    break;
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal
                        List<tbl_LitigationFileData> CMPDocuments = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Count > 0)
                        {
                            List<tbl_LitigationFileData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                entityData.Version = "1.0";
                                entityData.NoticeCaseInstanceID = Convert.ToInt64(NoticeCaseInstanceID);
                                entityData.DocTypeInstanceID = 0;
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptLitigationVersionView.DataBind();

                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles/Litigation";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        LitigatinoDocPath = FileName;
                                        LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                        lblMessage.Text = "";
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                        }
                        #endregion
                    }
                }
            }
        }
    }
}