﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class ShowNoticeDetail : System.Web.UI.Page
    {
        public static string DocumentPath = "";
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindAct();
                    BindParty();
                    BindCustomerBranches();
                    BindDepartment();
                    BindUsers();
                    BindLawyer();
                    BindNoticeCategoryType();

                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var noticeInstanceID = Request.QueryString["AccessID"];
                        if (noticeInstanceID != "")
                        {
                            ViewState["noticeInstanceID"] = noticeInstanceID;

                            if (Convert.ToInt32(noticeInstanceID) == 0)
                            {
                                liNoticeResponse.Visible = false;
                                liNoticeTask.Visible = false;
                                liNoticeStatusPayment.Visible = false;

                                btnAddNotice_Click(sender, e);  //Add Detail 
                            }
                            else
                            {
                                liNoticeResponse.Visible = true;
                                liNoticeTask.Visible = true;
                                liNoticeStatusPayment.Visible = true;

                                btnEditNotice_Click(sender, e); //Edit Detail
                            }

                            lnkNoticeDetail_Click(sender, e);
                        }
                    }

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                //Show Hide Grid Control - Enable/Disable Form Controls
                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        enableDisableNoticePopUpControls(false);
                    else
                        enableDisableNoticePopUpControls(true);
                }
                else
                    enableDisableNoticePopUpControls(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindAct()
        {
            var obj = LitigationLaw.GetAllAct();

            ddlAct.DataTextField = "Name";
            ddlAct.DataValueField = "ID";

            ddlAct.DataSource = obj;
            ddlAct.DataBind();

            ddlAct.Items.Add(new ListItem("Add New", "0"));
        }

        private void BindNoticeCategoryType()
        {
            try
            {
                var lstNoticeCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);

                ddlNoticeCategory.DataTextField = "CaseType";
                ddlNoticeCategory.DataValueField = "ID";

                ddlNoticeCategory.DataSource = lstNoticeCaseType;
                ddlNoticeCategory.DataBind();

                ddlNoticeCategory.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;

                //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                List<NameValueHierarchy> branches;
                string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                if (CacheHelper.Exists(key))
                {
                    CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                }
                else
                {
                    branches = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                }
                if (branches.Count > 0)
                {
                    branch = branches[0];
                }
                tbxBranch.Text = "Select Entity/Location";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";

            ddlDepartment.DataSource = obj;
            ddlDepartment.DataBind();

            ddlDepartment.Items.Add(new ListItem("Add New", "0"));
        }

        public void BindLawyer()
        {
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            var obj = LawyerManagement.GetLawyerListForMapping(customerID);

            ddlLawFirm.DataTextField = "Name";
            ddlLawFirm.DataValueField = "ID";

            ddlLawFirm.DataSource = obj;
            ddlLawFirm.DataBind();
        }

        public void BindParty()
        {
            var obj = LitigationLaw.GetLCPartyDetails(CustomerID);

            //Drop-Down at Modal Pop-up
            ddlParty.DataTextField = "Name";
            ddlParty.DataValueField = "ID";

            ddlParty.DataSource = obj;
            ddlParty.DataBind();

            ddlParty.Items.Add(new ListItem("Add New", "0"));
        }

        private void BindPaymentType(DropDownList ddl)
        {
            try
            {
                var PaymentMasterList = LitigationPaymentType.GetAllPaymentMasterList();

                ddl.DataValueField = "ID";
                ddl.DataTextField = "TypeName";

                ddl.DataSource = PaymentMasterList;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //var lstAllUsers = LitigationUserManagement.GetLitigationUsers(customerID,);

                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);

                var bothUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 0);

                ddlOwner.DataValueField = "ID";
                ddlOwner.DataTextField = "Name";
                ddlOwner.DataSource = bothUsers;
                ddlOwner.DataBind();

                var internalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 1);

                lstBoxPerformer.DataTextField = "Name";
                lstBoxPerformer.DataValueField = "ID";

                lstBoxPerformer.DataSource = internalUsers;
                lstBoxPerformer.DataBind();

                //ddlPerformer.DataValueField = "ID";
                //ddlPerformer.DataTextField = "Name";
                //ddlPerformer.DataSource = internalUsers;
                //ddlPerformer.DataBind();

                ddlReviewer.DataValueField = "ID";
                ddlReviewer.DataTextField = "Name";
                ddlReviewer.DataSource = internalUsers;
                ddlReviewer.DataBind();

                // var externalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);

                //ddlLawyerUser.DataValueField = "ID";
                //ddlLawyerUser.DataTextField = "Name";
                //ddlLawyerUser.DataSource = externalUsers;
                //ddlLawyerUser.DataBind();

                lstAllUsers.Clear();
                lstAllUsers = null;

                internalUsers.Clear();
                internalUsers = null;

                //externalUsers.Clear();
                // externalUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindNoticeRelatedDocuments(int noticeInstanceID)
        {
            try
            {
                List<Sp_Litigation_CaseDocument_Result> lstNoticeDocs = new List<Sp_Litigation_CaseDocument_Result>();

                lstNoticeDocs = NoticeManagement.GetNoticeDocumentMapping(noticeInstanceID,"", AuthenticationHelper.CustomerID);
                if (lstNoticeDocs.Count > 0)
                {
                    lstNoticeDocs = (from g in lstNoticeDocs
                                     group g by new
                                     {
                                         g.ID,
                                         g.DocType,
                                         g.FileName,
                                         g.Version,
                                         g.CreatedByText,
                                         g.CreatedOn
                                     } into GCS
                                     select new Sp_Litigation_CaseDocument_Result()
                                     {
                                         ID = GCS.Key.ID,
                                         DocType = GCS.Key.DocType,
                                         FileName = GCS.Key.FileName,
                                         Version = GCS.Key.Version,
                                         CreatedByText = GCS.Key.CreatedByText,
                                         CreatedOn = GCS.Key.CreatedOn,
                                     }).ToList();
                }
                if (lstNoticeDocs != null && lstNoticeDocs.Count > 0)
                {
                    grdNoticeDocuments.DataSource = lstNoticeDocs;
                    grdNoticeDocuments.DataBind();
                }
                else
                {
                    grdNoticeDocuments.DataSource = null;
                    grdNoticeDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindNoticeResponses(int noticeInstanceID)
        {
            try
            {
                List<tbl_LegalNoticeResponse> lstNoticeResponses = new List<tbl_LegalNoticeResponse>();

                lstNoticeResponses = NoticeManagement.GetNoticeResponseDetails(noticeInstanceID);


                grdResponseLog.DataSource = lstNoticeResponses;
                grdResponseLog.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpResponse.IsValid = false;
                cvNoticePopUpResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTaskResponses(int taskID, GridView grd)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grd.DataSource = lstTaskResponses;
                    grd.DataBind();
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTasks(int noticeInstanceID)
        {
            try
            {
                var lstNoticeTasks = LitigationTaskManagement.GetTaskDetails(noticeInstanceID, "N");

                grdTaskActivity.DataSource = lstNoticeTasks;
                grdTaskActivity.DataBind();

                lstNoticeTasks.Clear();
                lstNoticeTasks = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvNoticePopUpTask.IsValid = false;
                                cvNoticePopUpTask.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]), grdTaskActivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton) e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
            }
        }

        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Response Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindNoticePayments(int noticeInstanceID)
        {
            try
            {
                List<tbl_NoticeCasePayment> lstNoticePayments = new List<tbl_NoticeCasePayment>();

                lstNoticePayments = NoticeManagement.GetNoticeCasePaymentDetails(noticeInstanceID, "N");

                if (lstNoticePayments != null && lstNoticePayments.Count > 0)
                {
                    grdNoticePayment.DataSource = lstNoticePayments;
                    grdNoticePayment.DataBind();
                }
                else
                {
                    tbl_NoticeCasePayment obj = new tbl_NoticeCasePayment(); //initialize empty class that may contain properties
                    lstNoticePayments.Add(obj); //Add empty object to list

                    grdNoticePayment.DataSource = lstNoticePayments; /*Assign datasource to create one row with default values for the class you have*/
                    grdNoticePayment.DataBind(); //Bind that empty source                    

                    //To Hide row
                    grdNoticePayment.Rows[0].Visible = false;
                    grdNoticePayment.Rows[0].Controls.Clear();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddNotice_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                //BindAct();
                //BindParty();
                //BindCustomerBranches();
                //BindDepartment();
                //BindUsers();
                //BindLawyer();

                enableDisableNoticeControls(true);

                enableDisableNoticePopUpControls(true);

                clearNoticeControls();

                grdNoticeDocuments.DataSource = null;
                grdNoticeDocuments.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditNotice_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                    if (noticeInstanceID != 0)
                    {
                        ViewState["Mode"] = 1;

                        var noticeRecord = NoticeManagement.GetNoticeByID(noticeInstanceID);

                        if (noticeRecord != null)
                        {
                            enableDisableNoticeControls(false);

                            if (noticeRecord.NoticeType != null)
                            {
                                if (noticeRecord.NoticeType.ToString() == "I")
                                    rbNoticeInOutType.SelectedValue = "I";
                                else if (noticeRecord.NoticeType.ToString() == "O")
                                    rbNoticeInOutType.SelectedValue = "O";
                            }

                            tbxRefNo.Text = noticeRecord.RefNo;

                            if (noticeRecord.NoticeDate != null)
                                txtNoticeDate.Text = Convert.ToDateTime(noticeRecord.NoticeDate).ToString("dd-MM-yyyy");

                            var ListofParty = CaseManagement.GetListOfParty(noticeInstanceID, 2);
                            ddlParty.ClearSelection();
                            if (ddlParty.Items.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(ListofParty.ToString()))
                                {
                                    foreach (var item in ListofParty)
                                    {
                                        ddlParty.Items.FindByValue(item.PartyID.ToString()).Selected = true;
                                    }
                                }
                            }

                            var ListofAct = CaseManagement.GetListOfAct(noticeInstanceID, 2);
                            ddlAct.ClearSelection();
                            if (ddlAct.Items.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(ListofAct.ToString()))
                                {
                                    foreach (var item in ListofAct)
                                    {
                                        ddlAct.Items.FindByValue(item.ActID.ToString()).Selected = true;
                                    }
                                }
                            }

                            tbxSection.Text = noticeRecord.Section;

                            if (noticeRecord.NoticeCategoryID != null)
                            {
                                ddlNoticeCategory.ClearSelection();

                                if (ddlNoticeCategory.Items.FindByValue(noticeRecord.NoticeCategoryID.ToString()) != null)
                                    ddlNoticeCategory.SelectedValue = noticeRecord.NoticeCategoryID.ToString();
                            }

                            tbxTitle.Text = noticeRecord.NoticeTitle;
                            tbxDescription.Text = noticeRecord.NoticeDetailDesc;

                            if (noticeRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == noticeRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == noticeRecord.CustomerBranchID.ToString())
                                            item1.Selected = true;
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(noticeRecord.DepartmentID.ToString()) != null)
                                ddlDepartment.SelectedValue = noticeRecord.DepartmentID.ToString();

                            if (noticeRecord.OwnerID != null)
                            {
                                ddlOwner.ClearSelection();

                                if (ddlOwner.Items.FindByValue(noticeRecord.OwnerID.ToString()) != null)
                                    ddlOwner.SelectedValue = noticeRecord.OwnerID.ToString();
                            }

                            if (noticeRecord.NoticeRiskID != null)
                            {
                                ddlNoticeRisk.ClearSelection();

                                if (ddlNoticeRisk.Items.FindByValue(noticeRecord.NoticeRiskID.ToString()) != null)
                                    ddlNoticeRisk.SelectedValue = noticeRecord.NoticeRiskID.ToString();
                            }

                            if (noticeRecord.ClaimAmt != null)
                                tbxClaimedAmt.Text = noticeRecord.ClaimAmt.ToString();
                            else
                                tbxClaimedAmt.Text = "";

                            if (noticeRecord.ProbableAmt != null)
                                tbxProbableAmt.Text = noticeRecord.ProbableAmt.ToString();
                            else
                                tbxProbableAmt.Text = "";

                            //Potential Impact
                            if (noticeRecord.ImpactType != null)
                            {
                                if (noticeRecord.ImpactType.ToString() == "M")
                                    rblPotentialImpact.SelectedValue = "M";
                                else if (noticeRecord.ImpactType.ToString() == "N")
                                    rblPotentialImpact.SelectedValue = "N";
                                else if (noticeRecord.ImpactType.ToString() == "B")
                                    rblPotentialImpact.SelectedValue = "B";
                            }

                            if (noticeRecord.Monetory != null)
                                tbxMonetory.Text = noticeRecord.Monetory.ToString();
                            else
                                tbxMonetory.Text = "";

                            if (noticeRecord.NonMonetory != null)
                                tbxNonMonetory.Text = noticeRecord.NonMonetory.ToString();
                            else
                                tbxNonMonetory.Text = "";

                            if (noticeRecord.Years != null)
                                tbxNonMonetoryYears.Text = noticeRecord.Years.ToString();
                            else
                                tbxNonMonetoryYears.Text = "";

                            //Get Lawyer Mapping
                            var lstNoticeLawyer = NoticeManagement.GetNoticeLawyerMapping(noticeInstanceID);

                            if (lstNoticeLawyer != null)
                            {
                                ddlLawFirm.ClearSelection();
                                ddlLawFirm.SelectedValue = Convert.ToString(lstNoticeLawyer.LawyerID);
                            }

                            //Get Notice Assignment
                            var lstNoticeAssignment = NoticeManagement.GetNoticeAssignment(noticeInstanceID);

                            if (lstNoticeAssignment.Count > 0)
                            {
                                //ddlPerformer.ClearSelection();
                                //ddlReviewer.ClearSelection();
                                //ddlLawyerUser.ClearSelection();

                                lstBoxPerformer.ClearSelection();
                                ddlReviewer.ClearSelection();
                                lstBoxLawyerUser.ClearSelection();

                                foreach (var eachAssignmentRecord in lstNoticeAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                        if (lstBoxLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                        //if (ddlPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                        //    ddlPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                        //if (ddlLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                        //    ddlLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 4)
                                    {
                                        if (ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }

                            //Notice Status 
                            var StatusDetails = NoticeManagement.GetNoticeStatusDetail(noticeInstanceID);

                            if (StatusDetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.TxnStatusID)))
                                {
                                    ddlNoticeStatus.ClearSelection();

                                    if (ddlNoticeStatus.Items.FindByValue(StatusDetails.TxnStatusID.ToString()) != null)
                                        ddlNoticeStatus.Items.FindByValue(StatusDetails.TxnStatusID.ToString()).Selected = true;

                                    ViewState["noticeStatus"] = Convert.ToInt32(StatusDetails.TxnStatusID);

                                    //if (Convert.ToInt32(StatusDetails.TxnStatusID) == 3)
                                    //    enableDisableNoticePopUpControls(false);
                                    //else
                                    //    enableDisableNoticePopUpControls(true);
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.CloseDate)))
                                {
                                    tbxNoticeCloseDate.Text = Convert.ToDateTime(StatusDetails.CloseDate).ToString("dd-MM-yyyy");
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.ClosureRemark)))
                                {
                                    tbxCloseRemark.Text = StatusDetails.ClosureRemark;
                                }
                            }
                            //Notice Status Log--End

                            //Bind Notice Related Documents
                            BindNoticeRelatedDocuments(noticeInstanceID);

                            //Bind Notice Response Details
                            BindNoticeResponses(noticeInstanceID);

                            //Bind Notice Action Details
                            BindTasks(noticeInstanceID);

                            //Bind Notice Payment Details
                            BindNoticePayments(noticeInstanceID);
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public bool SendTaskAssignmentMail(tbl_TaskScheduleOn taskRecord, string accessURL, string assignedBy)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (taskRecord != null)
                {
                    User User = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));

                    if (User != null)
                    {
                        if (User.Email != null && User.Email != "")
                        {
                            string taskPriority = string.Empty;

                            if (taskRecord.PriorityID != null)
                            {
                                if (taskRecord.PriorityID == 1)
                                    taskPriority = "High";
                                else if (taskRecord.PriorityID == 2)
                                    taskPriority = "Medium";
                                else if (taskRecord.PriorityID == 3)
                                    taskPriority = "Low";
                            }

                            string assignedToUserName = string.Format("{0} {1}", User.FirstName, User.LastName);
                            string portalURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalURL = Urloutput.URL;
                            }
                            else
                            {
                                portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_TaskAssignment
                                                  .Replace("@User", assignedToUserName)
                                                  .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                  .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                  .Replace("@Priority", taskPriority)
                                                  .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                                                  .Replace("@AssignedBy", assignedBy)
                                                  .Replace("@Remark", taskRecord.Remark)
                                                  .Replace("@AccessURL", accessURL)
                                                  .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                                                  .Replace("@PortalURL", Convert.ToString(portalURL));


                            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_TaskAssignment
                            //                        .Replace("@User", assignedToUserName)
                            //                        .Replace("@TaskTitle", taskRecord.TaskTitle)
                            //                        .Replace("@TaskDesc", taskRecord.TaskDesc)
                            //                        .Replace("@Priority", taskPriority)
                            //                        .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                            //                        .Replace("@AssignedBy", assignedBy)
                            //                        .Replace("@Remark", taskRecord.Remark)
                            //                        .Replace("@AccessURL", accessURL)
                            //                        .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                            //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { User.Email }), null, null, "Litigation Notification-Task Assigned", message);

                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public void clearNoticeControls()
        {
            try
            {
                txtNoticeDate.Text = "";
                tbxRefNo.Text = "";
                ddlParty.ClearSelection();
                ddlAct.ClearSelection();
                tbxSection.Text = "";
                ddlNoticeCategory.ClearSelection();
                tbxTitle.Text = "";
                tbxDescription.Text = "";
                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                //tvBranches.SelectedNode.Selected = false;
                ddlDepartment.ClearSelection();
                ddlOwner.ClearSelection();
                ddlNoticeRisk.ClearSelection();
                tbxClaimedAmt.Text = "";
                tbxProbableAmt.Text = "";

                rblPotentialImpact.ClearSelection();
                tbxMonetory.Text = "";
                tbxNonMonetory.Text = "";
                tbxNonMonetoryYears.Text = "";

                ddlLawFirm.ClearSelection();
                lstBoxPerformer.ClearSelection();
                lstBoxLawyerUser.ClearSelection();

                ddlReviewer.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableNoticeControls(bool flag)
        {
            try
            {
                rbNoticeInOutType.Enabled = flag;
                txtNoticeDate.Enabled = flag;
                tbxRefNo.Enabled = flag;
                ddlParty.Enabled = flag;
                ddlAct.Enabled = flag;
                tbxSection.Enabled = flag;
                ddlNoticeCategory.Enabled = flag;
                tbxTitle.Enabled = flag;
                tbxDescription.Enabled = flag;
                tbxBranch.Enabled = flag;
                //tvBranches.SelectedNode.Selected = false;
                ddlDepartment.Enabled = flag;
                ddlOwner.Enabled = flag;
                ddlNoticeRisk.Enabled = flag;
                tbxClaimedAmt.Enabled = flag;
                tbxProbableAmt.Enabled = flag;

                rblPotentialImpact.Enabled = flag;
                tbxMonetory.Enabled = flag;
                tbxNonMonetory.Enabled = flag;
                tbxNonMonetoryYears.Enabled = flag;

                ddlLawFirm.Enabled = flag;

                lstBoxPerformer.Enabled = flag;
                ddlReviewer.Enabled = flag;
                lstBoxLawyerUser.Enabled = flag;
                ddlParty.Enabled = flag;
                ddlAct.Enabled = flag;

                if (flag)
                {
                    ddlLawFirm.Attributes.Remove("disabled");

                    lstBoxPerformer.Attributes.Remove("disabled");
                    lstBoxLawyerUser.Attributes.Remove("disabled");
                    ddlReviewer.Attributes.Remove("disabled");
                    ddlParty.Attributes.Remove("disabled");
                    ddlAct.Attributes.Remove("disabled");
                }
                else
                {
                    ddlLawFirm.Attributes.Add("disabled", "disabled");

                    lstBoxPerformer.Attributes.Add("disabled", "disabled");
                    lstBoxLawyerUser.Attributes.Add("disabled", "disabled");
                    ddlReviewer.Attributes.Add("disabled", "disabled");
                    ddlAct.Attributes.Add("disabled", "disabled");
                    ddlParty.Attributes.Add("disabled", "disabled");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        public void enableDisableNoticePopUpControls(bool flag)
        {
            try
            {
                pnlNotice.Enabled = flag;
                pnlNoticeAssignment.Enabled = flag;
                pnlTask.Enabled = flag;
                pnlResponse.Enabled = flag;

                grdTaskActivity.Columns[7].Visible = flag;

                grdNoticePayment.ShowFooter = flag;
                grdNoticePayment.Columns[5].Visible = flag;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DownloadNoticeDocument(int noticeFileID)
        {
            try
            {
                var file = NoticeManagement.GetNoticeDocumentByID(noticeFileID);
                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteNoticeFile(int noticeFileID)
        {
            try
            {
                if (noticeFileID != 0)
                {
                    if (NoticeManagement.DeleteNoticeDocument(noticeFileID, AuthenticationHelper.UserID))
                    {
                        cvNoticePopUp.IsValid = false;
                        cvNoticePopUp.ErrorMessage = "Document Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePopUp.IsValid = false;
                        cvNoticePopUp.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteNoticeResponse(int noticeResponseID)
        {
            try
            {
                if (noticeResponseID != 0)
                {
                    //Delete Response with Documents
                    if (NoticeManagement.DeleteNoticeResponseLog(noticeResponseID, AuthenticationHelper.UserID))
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "Document Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpResponse.IsValid = false;
                cvNoticePopUpResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteTask(int taskID,long CustomerID)
        {
            try
            {
                if (taskID != 0)
                {
                    if (LitigationTaskManagement.DeleteTask(taskID, AuthenticationHelper.UserID, CustomerID))
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Task Detail Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeletePaymentLog(int noticePaymentID)
        {
            try
            {
                if (noticePaymentID != 0)
                {
                    if (NoticeManagement.DeleteNoticePaymentLog(noticePaymentID, AuthenticationHelper.UserID))
                    {
                        cvNoticePayment.IsValid = false;
                        cvNoticePayment.ErrorMessage = "Action Detail Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePayment.IsValid = false;
                        cvNoticePayment.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdNoticeDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadNoticeDoc = (LinkButton) e.Row.FindControl("lnkBtnDownLoadNoticeDoc");

            if (lnkBtnDownLoadNoticeDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadNoticeDoc);
            }

            LinkButton lnkBtnDeleteNoticeDoc = (LinkButton) e.Row.FindControl("lnkBtnDeleteNoticeDoc");
            if (lnkBtnDeleteNoticeDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteNoticeDoc);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteNoticeDoc.Visible = false;
                    else
                        lnkBtnDeleteNoticeDoc.Visible = true;
                }
            }
        }

        protected void grdNoticeDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadNoticeDoc"))
                    {
                        DownloadNoticeDocument(Convert.ToInt32(e.CommandArgument));
                    }
                    else if (e.CommandName.Equals("DeleteNoticeDoc"))
                    {
                        DeleteNoticeFile(Convert.ToInt32(e.CommandArgument));

                        //Bind Notice Related Documents
                        if (ViewState["noticeInstanceID"] != null)
                            BindNoticeRelatedDocuments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                    }
                    else if (e.CommandName.Equals("ViewNoticeDocView"))
                    {
                        var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdNoticeDocuments_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdNoticeDocuments.PageIndex = e.NewPageIndex;
                    BindNoticeRelatedDocuments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnTaskReminder = (LinkButton) e.Row.FindControl("lnkBtnTaskReminder");

                if (lnkBtnTaskReminder != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskReminder);
                }

                LinkButton lnkBtnTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnTaskResponse");

                if (lnkBtnTaskResponse != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskResponse);
                }

                LinkButton lnkBtnDeleteTask = (LinkButton) e.Row.FindControl("lnkBtnDeleteTask");

                if (lnkBtnDeleteTask != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteTask);
                }

                Label lblTaskStatus = e.Row.FindControl("lblTaskStatus") as Label;

                if (lblTaskStatus != null)
                {
                    if (lblTaskStatus.Text != "" && lblTaskStatus.Text != "Open")
                    {
                        GridView gvTaskResponses = e.Row.FindControl("gvTaskResponses") as GridView;

                        if (gvTaskResponses != null)
                        {
                            if (grdTaskActivity.DataKeys[e.Row.RowIndex].Value != null)
                            {
                                int taskID = 0;
                                taskID = Convert.ToInt32(grdTaskActivity.DataKeys[e.Row.RowIndex].Value);

                                BindTaskResponses(taskID, gvTaskResponses);
                            }
                        }
                    }
                    else
                    {
                        HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;
                        //Image imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as Image;
                        if (imgCollapseExpand != null)
                            imgCollapseExpand.Visible = false;
                    }


                    //Hide Close Task Button
                    LinkButton lnkBtnCloseTask = e.Row.FindControl("lnkBtnCloseTask") as LinkButton;
                    if (lnkBtnCloseTask != null)
                    {
                        if (lblTaskStatus.Text != "" && lblTaskStatus.Text == "Closed")
                            lnkBtnCloseTask.Visible = false;
                        else
                            lnkBtnCloseTask.Visible = true;
                    }
                }
            }
        }

        protected void grdTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdTaskActivity.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Related Documents
                    BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);

                    //ScriptManager.RegisterClientScriptBlock(Me.GridView1, Me.GetType(), "myScript", "alert('Done with paging');", True)
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (ViewState["noticeInstanceID"] != null)
                    {
                        int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                        int taskID = Convert.ToInt32(e.CommandArgument);

                        if (e.CommandName.Equals("DeleteTask"))
                        {
                            DeleteTask(taskID,AuthenticationHelper.CustomerID);

                            //Re-Bind Notice Related Documents
                            BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                        else if (e.CommandName.Equals("CloseTask"))
                        {
                            //Update Task Status to Closed and Expire URL
                            LitigationTaskManagement.UpdateTaskStatus(taskID, 3, AuthenticationHelper.UserID, AuthenticationHelper.CustomerID); //Status 3 - Closed

                            //Re-Bind Notice Related Documents
                            BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                        else if (e.CommandName.Equals("TaskReminder")) //Send Reminder or Re-generate URL
                        {
                            if (taskID != 0)
                            {
                                string accessURL = string.Empty;
                                bool sendSuccess = false;
                                string portalURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    portalURL = Urloutput.URL;
                                }
                                else
                                {
                                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }
                                //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                                accessURL = Convert.ToString(portalURL) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                   CryptographyManagement.Encrypt(taskID.ToString()) +
                                   "&NID=" + CryptographyManagement.Encrypt(noticeInstanceID.ToString());

                                //accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                //    CryptographyManagement.Encrypt(taskID.ToString()) +
                                //    "&NID=" + CryptographyManagement.Encrypt(noticeInstanceID.ToString());

                                //Get Task Record
                                var taskRecord = LitigationTaskManagement.GetTaskDetailByTaskID(noticeInstanceID, taskID, "N", AuthenticationHelper.CustomerID);

                                if (taskRecord != null)
                                {
                                    sendSuccess = SendTaskAssignmentMail(taskRecord, accessURL, AuthenticationHelper.User);

                                    if (sendSuccess)
                                    {
                                        cvNoticePopUpTask.ErrorMessage = "An Email containing task detail and access URL to provide response sent to assignee.";

                                        taskRecord.AccessURL = accessURL;
                                        taskRecord.UpdatedBy = AuthenticationHelper.UserID;
                                        sendSuccess = LitigationTaskManagement.UpdateTaskAccessURL(taskRecord.ID, taskRecord);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        protected void grdResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdResponseLog.PageIndex = e.NewPageIndex;

                    BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);
                        int NoticeInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && NoticeInstanceID != 0)
                        {
                            var lstResponseDocument = NoticeManagement.GetNoticeResponseDocuments(NoticeInstanceID, responseID, "NR");

                            if (lstResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=NoticeResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvNoticePopUpResponse.IsValid = false;
                                cvNoticePopUpResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }

                            DownloadNoticeDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                        }
                    }
                    else if (e.CommandName.Equals("DeleteResponse"))
                    {
                        DeleteNoticeResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID 

                        //Bind Notice Responses
                        if (ViewState["noticeInstanceID"] != null)
                        {
                            BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                    }
                    else if (e.CommandName.Equals("ViewNoticeResposeDocView"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);//HearingID
                        int CaseInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && CaseInstanceID != 0)
                        {
                            var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, responseID, "NR");

                            if (lstResponseDocument != null)
                            {
                                List<tbl_LitigationFileData> entitiesData = lstResponseDocument.Where(entry => entry.Version != null).ToList();
                                if (lstResponseDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                    entityData.Version = "1.0";
                                    entityData.NoticeCaseInstanceID = CaseInstanceID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in lstResponseDocument)
                                    {
                                        rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptDocmentVersionView.DataBind();
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            DocumentPath = FileName;

                                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                            lblMessage.Text = "";

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadResponseDoc = (LinkButton) e.Row.FindControl("lnkBtnDownLoadResponseDoc");

            if (lnkBtnDownLoadResponseDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadResponseDoc);
            }

            LinkButton lnkBtnDeleteResponse = (LinkButton) e.Row.FindControl("lnkBtnDeleteResponse");
            if (lnkBtnDeleteResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteResponse);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteResponse.Visible = false;
                    else
                        lnkBtnDeleteResponse.Visible = true;
                }
            }
        }

        protected void grdNoticePayment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnDeletePayment = (LinkButton) e.Row.FindControl("lnkBtnDeletePayment");
                if (lnkBtnDeletePayment != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeletePayment);
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlPaymentType = (DropDownList) e.Row.FindControl("ddlPaymentType");

                if (ddlPaymentType != null)
                    BindPaymentType(ddlPaymentType);
            }
        }

        protected void grdNoticePayment_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdNoticePayment.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Payments
                    BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdNoticePayment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeletePayment"))
                    {
                        DeletePaymentLog(Convert.ToInt32(e.CommandArgument));

                        //Re-Bind Notice Payments
                        if (ViewState["noticeInstanceID"] != null)
                        {
                            BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                //ScriptManager.RegisterStartupScript(this.upNoticePopup, this.upNoticePopup.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkNoticeDetail_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "active");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;

            if (ViewState["Mode"] != null)
            {
                if (Convert.ToInt32(ViewState["Mode"]) == 0)
                {
                    enableDisableNoticeControls(true);
                }
                else if (Convert.ToInt32(ViewState["Mode"]) == 1)
                {
                    enableDisableNoticeControls(false);
                }
            }
        }

        protected void lnkNoticeTask_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "active");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 1;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
        }

        protected void lnkNoticeResponse_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "active");
            liNoticeStatusPayment.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 2;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
        }

        protected void lnkNoticeStatusPayment_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 3;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
        }

        public string ShowNoticeResponseDocCount(long noticeInstanceID, long noticeResponseID)
        {
            try
            {
                var docCount = NoticeManagement.GetNoticeResponseDocuments(noticeInstanceID, noticeResponseID, "NR").Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        // New code
        protected void lnkBtnDept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lnkBtnParty_Click(object sender, EventArgs e)
        {
            BindParty();
        }

        protected void lnkBtnAct_Click(object sender, EventArgs e)
        {
            BindAct();
        }

        protected void lnkBtnCategory_Click(object sender, EventArgs e)
        {
            BindNoticeCategoryType();
        }

        protected void lnkAddNewUser_Click(object sender, EventArgs e)
        {
            BindUsers();
        }

        protected void ddlLawFirm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLawFirm.SelectedValue))
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);
                var externalUsers = LitigationUserManagement.GetRequiredUsersByLawFirm(lstAllUsers, 2, Convert.ToInt32(ddlLawFirm.SelectedValue));

                lstBoxLawyerUser.DataTextField = "Name";
                lstBoxLawyerUser.DataValueField = "ID";
                lstBoxLawyerUser.DataSource = externalUsers;
                lstBoxLawyerUser.DataBind();
            }
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(commandArgs[2]));

                    if (AllinOneDocumentList != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                        if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);

                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (AllinOneDocumentList.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            DocumentPath = FileName;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            lblMessage.Text = "";
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton) e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
    }
}