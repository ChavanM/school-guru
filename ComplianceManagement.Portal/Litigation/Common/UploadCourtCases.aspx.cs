﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using System.Data;
using Ionic.Zip;
using System.Net;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using BM_ManegmentServices.Data;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class UploadCourtCases : System.Web.UI.Page
    {
        bool suucessmsg = false;
        bool suucess = false;
        public string IsNoticeDate;

        public static List<long> Branchlist = new List<long>();
        private static int CustomerID = 0;
        public bool IsWinningProspect;
        public bool RiskType;
        protected void Page_Load(object sender, EventArgs e)
        {
            IsNoticeDate = Convert.ToString(ConfigurationManager.AppSettings["IsNoticeDateCustID"]);
            if (!IsPostBack)
            {
                CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                //lblMessage.Text = string.Empty;
                //LblErormessage.Text = string.Empty;
                ViewState["Flagvalue"] = null;
                lnkCC_Click(sender, e);
            }
            IsWinningProspect = CaseManagement.CheckForClient(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), "CaseNoticeLabel");
            RiskType = CaseManagement.CheckForClient(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), "RiskType");

        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {

        }

        #region  Import Court Cases     
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Litigation/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Litigation/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoCCUpload.Checked)
                            {
                                bool flag = TransactionSheetsExitsts(xlWorkbook, "Court Cases");
                                if (flag == true)
                                {
                                    ProcessCourtCaseData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Court Cases'.";
                                }
                            }
                            else if (rdoCHUpload.Checked)
                            {
                                bool flag = TransactionSheetsExitsts(xlWorkbook, "Case Hearing");
                                if (flag == true)
                                {
                                    ProcessCaseHearingData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Case Hearing'.";
                                }
                            }
                            else if (rdoCOUpload.Checked)
                            {
                                bool flag = TransactionSheetsExitsts(xlWorkbook, "Case Orders");
                                if (flag == true)
                                {
                                    ProcessCaseOrderData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Case Orders'.";
                                }
                            }
                            else if (rdoCPUpload.Checked)
                            {
                                bool flag = TransactionSheetsExitsts(xlWorkbook, "Case Payments");
                                if (flag == true)
                                {
                                    ProcessCasePaymentsData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Case Payments'.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                            //if (suucessmsg == true)
                            //{
                            //    cvDuplicateEntry.IsValid = false;
                            //    cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";
                            //}
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
                }
            }
        }
        private bool TransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("Court Cases"))
                    {
                        if (sheet.Name.Trim().Equals("Case Hearing") || sheet.Name.Trim().Equals("Case Orders") || sheet.Name.Trim().Equals("Court Cases") || sheet.Name.Trim().Equals("Case Payments"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    else if (data.Equals("Case Hearing"))
                    {
                        if (sheet.Name.Trim().Equals("Case Hearing") || sheet.Name.Trim().Equals("Case Orders") || sheet.Name.Trim().Equals("Court Cases") || sheet.Name.Trim().Equals("Case Payments"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    else if (data.Equals("Case Orders"))
                    {
                        if (sheet.Name.Trim().Equals("Case Hearing") || sheet.Name.Trim().Equals("Case Orders") || sheet.Name.Trim().Equals("Court Cases") || sheet.Name.Trim().Equals("Case Payments"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    else if (data.Equals("Case Payments"))
                    {
                        if (sheet.Name.Trim().Equals("Case Hearing") || sheet.Name.Trim().Equals("Case Orders") || sheet.Name.Trim().Equals("Court Cases") || sheet.Name.Trim().Equals("Case Payments"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }        
        private void ProcessCourtCaseData(ExcelPackage xlWorkbook)
        {
            try
            {
                CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Court Cases"];
                if (xlWorksheet != null)
                {
                    int uploadedCaseCount = 0;

                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Type_of_Case = string.Empty;
                    string stringCaseOpenDate = string.Empty;
                    string Court_Case_No = string.Empty;
                    string Internal_Case_No = string.Empty;
                    string Case_Title = string.Empty;
                    List<string> ActIds = new List<string>();
                    string Section = string.Empty;
                    int casetypeID = -1;
                    List<string> Opp_Counter_Partys = new List<string>();
                    List<string> Opposition_Lawyers = new List<string>();
                    int CourtID = -1;
                    string Judge = string.Empty;
                    string CaseDescription = string.Empty;
                    int Entity_Location_BranchID = -1;
                    int DepartmentID = -1;
                    int OwnerID = -1;
                    string WinningProspect = string.Empty;
                    int WinningProspectID = -1;
                    string ClaimedAmount = string.Empty;
                    string ProbableAmount = string.Empty;
                    string Potential_Impact_Monetary_Non_Monetary_both = string.Empty;
                    string Monetary = string.Empty;
                    string Non_Monetary = string.Empty;
                    string Years_if_Non_Monetary = string.Empty;
                    int Law_FirmID = -1;
                    List<string> Internal_Users = new List<string>();
                    List<string> External_Users = new List<string>();
                    List<string> Finanacial_Years = new List<string>();
                    int casestageID = -1;
                    string caseStatus = String.Empty;
                    string caseResult = String.Empty;
                    int caseStatusID = -1;
                    int caseResultID = -1;                    
                    string caseCloseDate = String.Empty;
                    string branchName = string.Empty;
                    string deptName = string.Empty;

                    string caseBudget = string.Empty;
                    
                    string recoveryAmount = string.Empty;
                    int jurisdiction = -1;
                    int CPDepartment = -1;
                    string ProvisonalAmount = string.Empty;
                    string ProtestAmount = string.Empty;
                    string BankGuarantee = string.Empty;
                    int StateID = -1;
                    string stringNoticeDate = string.Empty;
                    string RiskTypeName = string.Empty;
                    int RiskTypeID = 1;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        Type_of_Case = string.Empty;
                        stringCaseOpenDate = string.Empty;
                        Court_Case_No = string.Empty;
                        Internal_Case_No = string.Empty;
                        Case_Title = string.Empty;
                        Section = string.Empty;
                        casetypeID = -1;
                        CourtID = -1;
                        Judge = string.Empty;
                        CaseDescription = string.Empty;
                        Entity_Location_BranchID = -1;
                        DepartmentID = -1;
                        OwnerID = -1;
                        WinningProspect = string.Empty;
                        WinningProspectID = -1;
                        ClaimedAmount = string.Empty;
                        ProbableAmount = string.Empty;
                        Potential_Impact_Monetary_Non_Monetary_both = string.Empty;
                        Monetary = string.Empty;
                        Non_Monetary = string.Empty;
                        Years_if_Non_Monetary = string.Empty;
                        Law_FirmID = -1;
                        casestageID = -1;
                        caseStatus = String.Empty;
                        caseStatusID = -1;
                        caseCloseDate = String.Empty;
                        caseResult = String.Empty;
                        caseBudget = string.Empty;
                        recoveryAmount = string.Empty;
                        jurisdiction = -1;
                        CPDepartment = -1;
                        ProvisonalAmount = string.Empty;
                        ProtestAmount = string.Empty;
                        BankGuarantee = string.Empty;
                        StateID = -1;
                        RiskTypeName = string.Empty;
                        RiskTypeID = -1;

                        #region 1 Type of Case (Defendant / Plaintiff)
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Type_of_Case = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            if (Type_of_Case.ToUpper().Trim() == "DEFENDANT")
                            {
                                Type_of_Case = "I";
                            }
                            else if (Type_of_Case.ToUpper().Trim() == "PLAINTIFF")
                            {
                                Type_of_Case = "O";
                            }
                            else
                            {
                                errorMessage.Add("Correct Type of Case (Defendant / Plaintiff) at row number - " + (count + 1) + "");
                            }
                        }
                        if (String.IsNullOrEmpty(Type_of_Case))
                        {
                            errorMessage.Add("Required Type of Case (Defendant / Plaintiff) at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 2 Case Open Date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                            stringCaseOpenDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

                        if (stringCaseOpenDate == "")
                        {
                            errorMessage.Add("Please Check the Case Open Date or Case Open Date can not be empty at row - " + i + "");
                        }
                        else
                        {
                            try
                            {
                                bool check = CheckDate(stringCaseOpenDate);
                                if (!check)
                                {
                                    errorMessage.Add("Please Check the Case Open Date or Case Open Date can not be empty at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Case Open Date or Case Open Date can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 3 Court Case No.
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();

                            bool matchSuccess = false;
                            matchSuccess = checkDuplicateDataExistExcelSheet(xlWorksheet, i, 3, Court_Case_No);

                            if (matchSuccess)
                            {
                                errorMessage.Add("Case with Court Case No at Row - " + (count + 1) + " Exists Multiple Times in the Uploaded Excel Document");
                            }
                        }

                        if (String.IsNullOrEmpty(Court_Case_No))
                        {
                            errorMessage.Add("Required Court Case No at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            bool existCaseNo = CaseManagement.ExistsCourtCaseNo(Court_Case_No, 0);
                            if (existCaseNo)
                            {
                                errorMessage.Add("Case with Same Court Case No already exists, Check at at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 4 Internal Case No.
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            Internal_Case_No = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                        }

                        if (!String.IsNullOrEmpty(Internal_Case_No))
                        {
                            if (Internal_Case_No.Length > 50)
                            {
                                errorMessage.Add("Internal Case No can not exceeds 50 characters at row number - " + (count + 1) + "");
                            }
                        }

                        #endregion

                        #region 5 Case Title
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {
                            Case_Title = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();

                            bool matchSuccess = false;
                            matchSuccess = checkDuplicateDataExistExcelSheet(xlWorksheet, i, 5, Case_Title);

                            if (matchSuccess)
                            {
                                errorMessage.Add("Case with Case Title at Row - " + (count + 1) + " Exists Multiple Times in the Uploaded Excel Document");
                            }
                        }
                        if (String.IsNullOrEmpty(Case_Title))
                        {
                            errorMessage.Add("Required Case Title at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            if (Case_Title.Length <= 1000)
                            {
                                bool chkcasetitle = CaseManagement.ExistsCase(Case_Title, 0, AuthenticationHelper.CustomerID);
                                if (chkcasetitle)
                                {
                                    errorMessage.Add("Case with Same Title already exists, Check at row number - " + (count + 1) + "");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Case Title can not exceeds 1000 Characters at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region  6 Act Mapping add to List (Act under which the case has been filed)
                        string acts = xlWorksheet.Cells[i, 6].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(acts))
                        {
                            errorMessage.Add("Required Act at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            string[] split = acts.Split('#');
                            if (split.Length > 0)
                            {
                                string actid = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    actid = Convert.ToString(GetActIDByName(split[rs].ToString().Trim()));
                                    if (actid == "0" || actid == "-1")
                                    {
                                        errorMessage.Add("Please Correct the Act Name or Act(" + split[rs].ToString().Trim() + ") not Defined in the System at row number - " + (count + 1) + "");
                                    }
                                }
                            }
                        }

                        #endregion

                        #region 7 Section
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                        {
                            Section = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            if (Section.Length > 1000)
                            {
                                errorMessage.Add("Section can not exceeds 1000 Characters at row number - " + (count + 1) + "");
                            }
                        }

                        #endregion

                        #region 8 Case Type                        
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        {
                            casetypeID = GetCaseTypeIDByName(CustomerID, xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                        }
                        if (casetypeID == 0 || casetypeID == -1)
                        {

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                tbl_CaseType objCaseType = new tbl_CaseType();
                                objCaseType.CaseType = xlWorksheet.Cells[i, 8].Text.ToString().Trim();
                                objCaseType.IsDeleted = false;
                                objCaseType.CreatedOn = DateTime.UtcNow.Date;
                                objCaseType.CreatedBy = AuthenticationHelper.UserID;
                                objCaseType.CustomerID = (int)AuthenticationHelper.CustomerID;
                                LitigationCourtAndCaseType.CreateLegalCaseTypeDetails(objCaseType);
                            }
                            else
                            {
                                errorMessage.Add("Please Correct the Case Type or Case Type not Defined in the System at row number - " + (count + 1) + "");
                            }
                            // 
                        }
                        #endregion

                        #region 9 Opponent
                        string opposition_counterpartys = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(opposition_counterpartys))
                        {
                            errorMessage.Add("Required Opponent Name at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            string[] split = opposition_counterpartys.Split(',');
                            if (split.Length > 0)
                            {
                                string oppcouid = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    oppcouid = Convert.ToString(GetPartyIDByName(CustomerID, split[rs].ToString().Trim()));
                                    if (oppcouid == "0" || oppcouid == "-1")
                                    {
                                        //  errorMessage.Add("Please Correct the Opponent Name or Opponent(" + split[rs].ToString().Trim() + ") not Defined in the System at row number - " + (count + 1) + "");
                                        tbl_PartyDetail parT = new tbl_PartyDetail();

                                        parT.CustomerID = CustomerID;
                                        parT.PartyType = "Individual";
                                        parT.ContactNumber = "0";
                                        parT.CityID = 0;
                                        parT.StateID = 0;
                                        //if (split[rs].ToString().Trim().Contains("@"))
                                        //{
                                        //    parT.Email = split[rs].ToString().Trim();
                                        //    parT.Name = split[rs].ToString().Trim().Split('@')[0];
                                        //}
                                        //else
                                        //{
                                        //    parT.Email = "";
                                        //    parT.Name = split[rs].ToString().Trim();
                                        //}
                                        parT.Email = "";
                                        parT.Name = split[rs].ToString().Trim();
                                        LitigationLaw.CreateLegalCasePartyData(parT);
                                    }

                                }
                            }
                        }
                        #endregion

                        #region 10 Opposition Lawyer
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                        {
                            string opposition_lawyer = xlWorksheet.Cells[i, 10].Text.ToString().Trim();
                            string[] split = opposition_lawyer.Split(',');
                            if (split.Length > 0)
                            {
                                string opplawid = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    opplawid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                    if (opplawid == "0" || opplawid == "-1")
                                    {
                                        errorMessage.Add("Please Correct the Opposition Lawyer or Lawyer(" + split[rs].ToString().Trim() + ") not defined in the system at row number - " + (count + 1) + "");
                                    }
                                }
                            }
                        }
                        #endregion

                        #region 11 Court
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                        {
                            CourtID = GetCourtIDByName(CustomerID, xlWorksheet.Cells[i, 11].Text.Trim());
                        }
                        if (CourtID == 0 || CourtID == -1)
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                            {
                                tbl_CourtMaster objcourt = new tbl_CourtMaster();
                                objcourt.CourtName = xlWorksheet.Cells[i, 11].Text.Trim();
                                objcourt.CustomerID = (int)AuthenticationHelper.CustomerID;
                                objcourt.CreatedBy = AuthenticationHelper.UserID;
                                objcourt.IsDeleted = false;
                                LitigationCourtAndCaseType.CreateCourtMasterDetails(objcourt);
                            }
                            else
                            {
                                errorMessage.Add("Please Correct the Court or Court not defined in the system at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 12 Judge
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                        {
                            Judge = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim();
                        }
                        #endregion

                        #region 13 Case Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                        {
                            CaseDescription = Convert.ToString(xlWorksheet.Cells[i, 13].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(CaseDescription))
                        {
                            errorMessage.Add("Required Case Description at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 14 Entity/ Location/ Branch
                        branchName = string.Empty;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                        {
                            branchName = xlWorksheet.Cells[i, 14].Text.ToString().Trim();
                            Entity_Location_BranchID = GetEntityLocationBranchIDByName(CustomerID, branchName);
                        }
                        if (Entity_Location_BranchID == 0 || Entity_Location_BranchID == -1)
                        {
                            errorMessage.Add("Please Correct the (Entity/ Location/ Branch) or (Entity/ Location/ Branch) not Defined in the System at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 15 Jurisdiction
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                        {
                            jurisdiction = GetJusrisdictionIDByName(xlWorksheet.Cells[i, 15].Text.Trim());
                            if (jurisdiction == 0 || jurisdiction == -1)
                            {
                                errorMessage.Add("Please Correct the Jurisdiction or Jurisdiction not defined in the system at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 16 Department
                        deptName = string.Empty;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                        {
                            deptName = xlWorksheet.Cells[i, 16].Text.ToString().Trim();
                            DepartmentID = CompDeptManagement.GetDepartmentIDByName(deptName, (int)AuthenticationHelper.CustomerID);
                        }
                        if (DepartmentID == 0 || DepartmentID == -1)
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                            {
                                Department objDept = new Department();
                                objDept.Name = xlWorksheet.Cells[i, 16].Text.ToString().Trim();
                                objDept.IsDeleted = false;
                                objDept.CustomerID = (int)AuthenticationHelper.CustomerID;
                                CompDeptManagement.CreateDepartmentMaster(objDept);
                            }
                            else
                            {
                                errorMessage.Add("Please Correct the Department or Department (" + deptName + ") not Defined in the System at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 17 Contact Person of Department
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                        {
                            CPDepartment = GetCPersonIDByName(CustomerID, xlWorksheet.Cells[i, 17].Text.Trim());

                            if (CPDepartment == 0 || CPDepartment == -1)
                            {
                                errorMessage.Add("Please Correct the Contact Person of Department or not defined in the system at row number - " + (count + 1) + "");
                            }
                        }
                       
                        #endregion

                        #region 18 Owner
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                        {
                            OwnerID = GetOwnerUserIDByEmail(CustomerID, xlWorksheet.Cells[i, 18].Text.ToString().Trim());

                        }
                        if (OwnerID == 0 || OwnerID == -1)
                        {
                            errorMessage.Add("Please Correct the Owner or Owner not Defined in the System at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 19 Winning Prospect
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text))
                        {
                            WinningProspect = xlWorksheet.Cells[i, 19].Text.Trim();
                        }
                        if (WinningProspect.ToUpper() == "HIGH")
                        {
                            WinningProspectID = 1;
                        }
                        else if (WinningProspect.ToUpper() == "MEDIUM")
                        {
                            WinningProspectID = 2;
                        }
                        else if (WinningProspect.ToUpper() == "LOW")
                        {
                            WinningProspectID = 3;
                        }
                        else
                        {
                            WinningProspectID = 0;
                        }
                       if (WinningProspectID == 0)
                        {
                            errorMessage.Add("Please Correct the Winning Prospect or Winning Prospect can not left blank(possible values High/Medium/Low) at row - " + i + "");
                        }
                        #endregion

                        #region 20 Claimed Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text))
                            ClaimedAmount = Convert.ToString(xlWorksheet.Cells[i, 20].Text).Trim();

                        if (!String.IsNullOrEmpty(ClaimedAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(ClaimedAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Claimed Amount or Claimed Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Claimed Amount or Claimed Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 21 Probable Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text))
                            ProbableAmount = Convert.ToString(xlWorksheet.Cells[i, 21].Text).Trim();

                        if (!String.IsNullOrEmpty(ProbableAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(ProbableAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Probable Amount or Probable Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Probable Amount or Probable Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 22 Provisional Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text))
                            ProvisonalAmount = Convert.ToString(xlWorksheet.Cells[i, 22].Text).Trim();

                        if (!String.IsNullOrEmpty(ProvisonalAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(ProvisonalAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Provisonal Amount or Provisonal Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Provisonal Amount or Provisonal Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 23 Protest Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text))
                            ProtestAmount = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();

                        if (!String.IsNullOrEmpty(ProtestAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(ProtestAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Protest Amount or Protest Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Protest Amount or Protest Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 24 Recovery Amount

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text))
                            recoveryAmount = Convert.ToString(xlWorksheet.Cells[i, 24].Text).Trim();

                        if (!String.IsNullOrEmpty(recoveryAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(recoveryAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Recovery Amount or Recovery Amount  contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Recovery Amount  or Recovery Amount  contains only numbers at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 25 Bank Guarantee
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                        {
                            BankGuarantee = Convert.ToString(xlWorksheet.Cells[i, 25].Text).Trim();
                        }
                        #endregion

                        #region 26 Potential Impact - Monetary/ Non-Monetary/ both
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()))
                        {
                            Potential_Impact_Monetary_Non_Monetary_both = Convert.ToString(xlWorksheet.Cells[i, 26].Text).Trim();
                            if (!string.IsNullOrEmpty(Potential_Impact_Monetary_Non_Monetary_both))
                            {
                                if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "BOTH")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "BOTH";
                                }
                                else if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "MONETARY")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "MONETARY";
                                }
                                else if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "NON MONETARY")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "NON MONETARY";
                                }
                                else
                                {
                                    errorMessage.Add("Please Check the Potential Impact Or Possible cases(Monetary/ Non-Monetary/ both) at row - " + i + "");
                                }
                            }
                            else
                            {
                                Potential_Impact_Monetary_Non_Monetary_both = "BOTH";
                            }
                        }

                        #endregion

                        #region 27 Monetary
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                        {
                            Monetary = Convert.ToString(xlWorksheet.Cells[i, 27].Text).Trim();
                        }
                        #endregion

                        #region 28 Non-Monetary
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                        {
                            Non_Monetary = Convert.ToString(xlWorksheet.Cells[i, 28].Text).Trim();
                        }
                        #endregion

                        #region 29 Years (if Non-Monetary)
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text))
                            Years_if_Non_Monetary = Convert.ToString(xlWorksheet.Cells[i, 29].Text).Trim();

                        if (!String.IsNullOrEmpty(Years_if_Non_Monetary))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(Years_if_Non_Monetary, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Years (if Non-Monetary) or Years (if Non-Monetary) contains only numbers at row  - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Years (if Non-Monetary) or Years (if Non-Monetary) contains only numbers at row  - " + i + "");
                            }
                        }
                        #endregion

                        #region 30 State
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                        {
                            StateID = GetStateIDByName(xlWorksheet.Cells[i, 30].Text.Trim());

                            if (StateID == 0 || StateID == -1)
                            {
                                errorMessage.Add("Please Correct the State or State not defined in the system at row number - " + (count + 1) + "");
                            }
                        }
                       
                        #endregion

                        #region 31 Law Firm
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                        {
                            Law_FirmID = GetLawFirmIDByName(CustomerID, xlWorksheet.Cells[i, 31].Text.Trim());
                        }
                        if (Law_FirmID == 0 || Law_FirmID == -1)
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                            {
                                tbl_Lawyer objlawyer = new tbl_Lawyer()
                                {
                                    FirstName = xlWorksheet.Cells[i, 31].Text.ToString().Trim(),
                                    LastName = "",
                                    Email = "",
                                    Address = "",
                                    ContactNumber = "0",
                                    IsActive = true,
                                    CustomerID = Convert.ToInt32(CustomerID),
                                    Specilisation = null
                                };
                                LawyerManagement.CreateLawyerData(objlawyer);
                            }
                            else
                            {
                                errorMessage.Add("Please Check the Law Firm or check the values from Masters->Law Firm at row - " + i + "");
                            }
                        }
                        #endregion

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()) || !String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()))
                        {
                            #region 32 Performer (Internal User) 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                            {
                                string InternalUsers = xlWorksheet.Cells[i, 32].Text.ToString().Trim();
                                string[] split = InternalUsers.Split(',');
                                if (split.Length > 0)
                                {
                                    string Usersid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        Usersid = Convert.ToString(GetInternalUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (Usersid == "0" || Usersid == "-1")
                                        {
                                            errorMessage.Add("Please Correct the Performer (Internal User) or Performer (" + split[rs].ToString().Trim() + ") not Defined in the System, check at row number - " + (count + 1) + "");
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region 33 Performer (Lawyer User)
                            if (Law_FirmID != 0 || Law_FirmID != -1)
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()))
                                {
                                    string ExternalUsers = xlWorksheet.Cells[i, 33].Text.ToString().Trim();
                                    string[] split = ExternalUsers.Split(',');
                                    if (split.Length > 0)
                                    {
                                        string Usersid = "";
                                        for (int rs = 0; rs < split.Length; rs++)
                                        {
                                            //Usersid = Convert.ToString(GetLawyerUserIDByName(split[rs].ToString().Trim()));
                                            Usersid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim(), Law_FirmID));
                                            if (Usersid == "0" || Usersid == "-1")
                                            {
                                                errorMessage.Add("Please Correct the Performer(Lawyer User) or Lawyer(" + split[rs].ToString().Trim() + ") not defined in the system at row number - " + (count + 1) + "");
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            errorMessage.Add("Please Assign At Least One Performer either (Internal User) Or (Lawyer User) for case at row number - " + (count + 1) + "");
                        }

                        #region 34 Case Stage
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()))
                        {
                            casestageID = GetCaseStageIDByName(xlWorksheet.Cells[i, 34].Text.ToString().Trim());
                            if (casestageID == 0 || casestageID == -1)
                            {
                                // errorMessage.Add("Please Correct the Case Stage or Case Stage not Defined in the System at row number - " + (count + 1) + "");
                                tbl_TypeMaster _objCaseStage = new tbl_TypeMaster()
                                {
                                    TypeName = xlWorksheet.Cells[i, 34].Text.ToString().Trim(),
                                    customerID = (int)AuthenticationHelper.CustomerID,
                                    TypeCode = "CS",
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.UtcNow.Date,

                                };
                                CaseManagement.CreateCaseStageDetails(_objCaseStage);
                            }


                        } else
                        {
                            errorMessage.Add("Please add case stage - " + (count + 1) + "");
                        }
                        #endregion

                        #region 35 Case Status

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text))
                        {
                            caseStatus = xlWorksheet.Cells[i, 35].Text.Trim();
                        }
                        if (caseStatus.ToUpper() == "OPEN")
                        {
                            caseStatusID = 1;
                        }
                        else if (caseStatus.ToUpper() == "CLOSED")
                        {
                            caseStatusID = 3;
                        }
                        else if (caseStatus.ToUpper() == "SETTLED")
                        {
                            caseStatusID = 4;
                        }
                        else
                        {
                            caseStatusID = 0;
                        }
                        if (caseStatusID == 0 || caseStatusID == -1)
                        {
                            errorMessage.Add("Please Correct the Case Status or Case Status can not be Empty (Possible values are Open/Closed) at row - " + i + "");
                        }
                        #endregion

                        #region 36 Case Close Date

                        if (caseStatusID == 3)
                        {
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 36].Text))
                            {
                                caseCloseDate = xlWorksheet.Cells[i, 36].Text.Trim();
                            }

                            if (string.IsNullOrEmpty(caseCloseDate))
                            {
                                errorMessage.Add("Case Close Date can not be empty at row - " + i + "");
                            }
                            else
                            {
                                try
                                {
                                    bool check = CheckDate(caseCloseDate);
                                    if (!check)
                                    {
                                        errorMessage.Add("Please provide valid Case Close Date at row - " + i + "");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add("Please provide valid Case Close Date or Case Close Date can not be empty at row - " + i + "");
                                }
                            }
                        }

                        #endregion

                        #region 37 Case Result

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text))
                        {
                            caseResult = xlWorksheet.Cells[i, 37].Text.Trim();
                            if (!String.IsNullOrEmpty(caseResult))
                            {
                                caseResultID = GetResultTypeIDByName(caseResult);
                            }
                            if (caseResultID == 0 || caseResultID == -1)
                            {
                                errorMessage.Add("Please Correct the Case Result or Case Result can not be Empty (Possible values are Win/Loss/In Progress) at row - " + i + "");
                            }
                        }
                        else if (caseStatus.ToUpper() == "CLOSED")
                        {
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text))
                                errorMessage.Add("Please Correct the Case Result or Case Result can not be Empty (Possible values are Win/Loss/In Progress) at row - " + i + "");
                        }

                        #endregion

                        #region 38 Case Budget

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 38].Text))
                            caseBudget = Convert.ToString(xlWorksheet.Cells[i, 38].Text).Trim();

                        if (!String.IsNullOrEmpty(caseBudget))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(caseBudget, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Case Budget or Case Budget contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Case Budget or Case Budget contains only numbers at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 39  Case FY
                        string FNYear = xlWorksheet.Cells[i, 39].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(FNYear))
                        {
                            errorMessage.Add("Required Financial Year at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            string[] split = FNYear.Split(',');
                            if (split.Length > 0)
                            {
                                string FYId = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    FYId = Convert.ToString(CaseManagement.ValidataFY(split[rs].ToString().Trim()));
                                    if (FYId == "0" || FYId == "-1")
                                    {
                                        errorMessage.Add("Please Correct the Financial Year (" + split[rs].ToString().Trim() + ") not Defined in the System at row number - " + (count + 1) + "");
                                    }
                                }
                            }
                        }
                        #endregion
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 40].Text))
                        {
                            RiskTypeName = xlWorksheet.Cells[i, 40].Text.Trim();
                        }
                        if (RiskTypeName.ToUpper() == "HIGH")
                        {
                            RiskTypeID = 1;
                        }
                        else if (RiskTypeName.ToUpper() == "MEDIUM")
                        {
                            RiskTypeID = 2;
                        }
                        else if (RiskTypeName.ToUpper() == "LOW")
                        {
                            RiskTypeID = 3;
                        }
                        else
                        {
                            errorMessage.Add("Please Correct the Risk or Risk can not left blank(possible values High/Medium/Low) at row - " + i + "");
                        }
                      
                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        long NewCaseID = 0;

                        List<tbl_LegalCaseAssignment> mastertbl_LegalCaseAssignment = new List<tbl_LegalCaseAssignment>();
                        List<tbl_ActMapping> mastertbl_ActMapping = new List<tbl_ActMapping>();
                        List<tbl_OppositionLawyerList> mastertbl_OppositionLawyerList = new List<tbl_OppositionLawyerList>();
                        List<tbl_PartyMapping> mastertbl_PartyMapping = new List<tbl_PartyMapping>();
                        List<FinancialYearMapping> lstObjFYMapping = new List<FinancialYearMapping>();
                        List<tbl_LegalCaseLawyerMapping> mastertbl_LegalCaseLawyerMapping = new List<tbl_LegalCaseLawyerMapping>();

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            if (mastertbl_LegalCaseAssignment.Count > 0)
                                mastertbl_LegalCaseAssignment.Clear();

                            if (mastertbl_ActMapping.Count > 0)
                                mastertbl_ActMapping.Clear();

                            if (mastertbl_OppositionLawyerList.Count > 0)
                                mastertbl_OppositionLawyerList.Clear();

                            if (mastertbl_PartyMapping.Count > 0)
                                mastertbl_PartyMapping.Clear();

                            if (mastertbl_LegalCaseLawyerMapping.Count > 0)
                                mastertbl_LegalCaseLawyerMapping.Clear();

                            if (ActIds.Count > 0)
                                ActIds.Clear();

                            if (Opp_Counter_Partys.Count > 0)
                                Opp_Counter_Partys.Clear();

                            if (Opposition_Lawyers.Count > 0)
                                Opposition_Lawyers.Clear();

                            if (Internal_Users.Count > 0)
                                Internal_Users.Clear();

                            if (External_Users.Count > 0)
                                External_Users.Clear();

                            if (Finanacial_Years.Count > 0)
                                Finanacial_Years.Clear();

                            List<string> Both_Users = new List<string>();
                            count = count + 1;
                            Type_of_Case = string.Empty;
                            stringCaseOpenDate = string.Empty;
                            Court_Case_No = string.Empty;
                            Internal_Case_No = string.Empty;
                            Case_Title = string.Empty;
                            Section = string.Empty;
                            casetypeID = -1;
                            CourtID = -1;
                            Judge = string.Empty;
                            CaseDescription = string.Empty;
                            Entity_Location_BranchID = -1;
                            DepartmentID = -1;
                            OwnerID = -1;
                            WinningProspect = string.Empty;
                            WinningProspectID = -1;
                            ClaimedAmount = string.Empty;
                            ProbableAmount = string.Empty;
                            Potential_Impact_Monetary_Non_Monetary_both = string.Empty;
                            Monetary = string.Empty;
                            Non_Monetary = string.Empty;
                            Years_if_Non_Monetary = string.Empty;
                            Law_FirmID = -1;
                            casestageID = -1;
                            caseStatus = string.Empty;
                            caseResult = string.Empty;
                            caseStatusID = -1;
                            caseResultID = -1;
                            caseCloseDate = String.Empty;
                            caseBudget = string.Empty;
                            recoveryAmount = string.Empty;
                            jurisdiction = -1;
                            CPDepartment = -1;
                            ProvisonalAmount = string.Empty;
                            ProtestAmount = string.Empty;
                            BankGuarantee = string.Empty;
                            StateID = -1;
                            stringNoticeDate = string.Empty;
                            RiskTypeName = string.Empty;
                            RiskTypeID = -1;

                            NewCaseID = 0;

                            #region 1 Type of Case (Defendant / Plaintiff)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                Type_of_Case = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();

                                if (Type_of_Case.ToUpper().Trim() == "Defendant".ToUpper().Trim())
                                {
                                    Type_of_Case = "I";
                                }
                                else if (Type_of_Case.ToUpper().Trim() == "Plaintiff".ToUpper().Trim())
                                {
                                    Type_of_Case = "O";
                                }
                            }
                            #endregion

                            #region 2 Case Open Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                                stringCaseOpenDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            #endregion

                            #region 3 Court Case No.
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            }

                            #endregion

                            #region 4 Internal Case No.
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                            {
                                Internal_Case_No = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                            }
                            #endregion

                            #region 5 Case Title
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                Case_Title = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            }
                            #endregion

                            #region  6 Act Mapping add to List (Act under which the case has been filed)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                string acts = xlWorksheet.Cells[i, 6].Text.ToString().Trim();
                                string[] split = acts.Split('#');
                                if (split.Length > 0)
                                {
                                    //string finalactid = "";
                                    string actid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        actid = Convert.ToString(GetActIDByName(split[rs].ToString().Trim()));
                                        if (actid == "0" || actid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            ActIds.Add(actid);
                                            suucess = true;
                                        }
                                       
                                    }
                                   
                                }
                            }
                            #endregion

                            #region 7 Section
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                            {
                                Section = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            }
                            #endregion

                            #region 8 Case Type                        
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                casetypeID = GetCaseTypeIDByName(CustomerID, xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                            }
                            #endregion

                            #region 9 Opponent
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                            {
                                string opposition_counterpartys = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                                string[] split = opposition_counterpartys.Split(',');
                                if (split.Length > 0)
                                {
                                  
                                    string oppcouid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        oppcouid = Convert.ToString(GetPartyIDByName(CustomerID, split[rs].ToString().Trim()));
                                        if (oppcouid == "0" || oppcouid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            Opp_Counter_Partys.Add(oppcouid);
                                            suucess = true;
                                        }
                                      
                                    }
                                  
                                }
                            }

                            #endregion

                            #region 10 Opposition Lawyer                        
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                            {
                                string opposition_lawyer = xlWorksheet.Cells[i, 10].Text.ToString().Trim();
                                string[] split = opposition_lawyer.Split(',');
                                if (split.Length > 0)
                                {
                                    //string finalopplawid = "";
                                    string opplawid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        opplawid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (opplawid == "0" || opplawid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            Opposition_Lawyers.Add(opplawid);
                                            suucess = true;
                                        }
                                      
                                    }
                                }
                            }

                            #endregion

                            #region 11 Court
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                            {
                                CourtID = GetCourtIDByName(CustomerID, xlWorksheet.Cells[i, 11].Text.Trim());
                            }
                            #endregion

                            #region 12 Judge
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                            {
                                Judge = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim();
                            }
                            #endregion

                            #region 13 Case Description
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                            {
                                CaseDescription = Convert.ToString(xlWorksheet.Cells[i, 13].Text).Trim();
                            }
                            #endregion

                            #region 14 Entity/ Location/ Branch
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                            {
                                Entity_Location_BranchID = GetEntityLocationBranchIDByName(CustomerID, xlWorksheet.Cells[i, 14].Text.ToString().Trim());
                            }
                            #endregion

                            #region 15 Jurisdiction
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                            {
                                jurisdiction = GetJusrisdictionIDByName(xlWorksheet.Cells[i, 15].Text.Trim());
                            }
                            #endregion

                            #region 16 Department
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                            {
                                DepartmentID = CompDeptManagement.GetDepartmentIDByName(xlWorksheet.Cells[i, 16].Text.ToString().Trim(), (int)AuthenticationHelper.CustomerID);
                            }
                            #endregion

                            #region 17 Contact Person of Department
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                            {
                                CPDepartment = GetCPersonIDByName(CustomerID, xlWorksheet.Cells[i, 17].Text.Trim());
                            }
                            #endregion

                            #region 18 Owner
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                            {
                                OwnerID = GetOwnerUserIDByEmail(CustomerID, xlWorksheet.Cells[i, 18].Text.ToString().Trim());
                            }

                            #endregion

                            #region 19 Winning Prospect
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text))
                            {
                                WinningProspect = xlWorksheet.Cells[i, 19].Text.Trim();
                            }
                            if (WinningProspect.ToUpper() == "HIGH")
                            {
                                WinningProspectID = 1;
                            }
                            else if (WinningProspect.ToUpper() == "MEDIUM")
                            {
                                WinningProspectID = 2;
                            }
                            else if (WinningProspect.ToUpper() == "LOW")
                            {
                                WinningProspectID = 3;
                            }


                            #endregion

                            #region 20 Claimed Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text))
                                ClaimedAmount = Convert.ToString(xlWorksheet.Cells[i, 20].Text).Trim();
                            #endregion

                            #region 21 Probable Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text))
                                ProbableAmount = Convert.ToString(xlWorksheet.Cells[i, 21].Text).Trim();
                            #endregion

                            #region 22 Provisional Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text))
                                ProvisonalAmount = Convert.ToString(xlWorksheet.Cells[i, 22].Text).Trim();
                            #endregion

                            #region 23 Protest Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text))
                                ProtestAmount = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();
                            #endregion

                            #region 24 Recovery Amount

                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text))
                                recoveryAmount = Convert.ToString(xlWorksheet.Cells[i, 24].Text).Trim();

                            #endregion

                            #region 25 Bank Guarantee
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                            {
                                BankGuarantee = Convert.ToString(xlWorksheet.Cells[i, 25].Text).Trim();
                            }
                            #endregion

                            #region 26 Potential Impact - Monetary/ Non-Monetary/ both
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()))
                            {
                                Potential_Impact_Monetary_Non_Monetary_both = Convert.ToString(xlWorksheet.Cells[i, 26].Text).Trim();
                            }
                            if (!string.IsNullOrEmpty(Potential_Impact_Monetary_Non_Monetary_both))
                            {
                                if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "BOTH")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "BOTH";
                                }
                                else if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "MONETARY")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "MONETARY";
                                }
                                else if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "NON MONETARY")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "NON MONETARY";
                                }
                            }
                            else
                            {
                                Potential_Impact_Monetary_Non_Monetary_both = "BOTH";
                            }
                            #endregion

                            #region 27 Monetary
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                            {
                                Monetary = Convert.ToString(xlWorksheet.Cells[i, 27].Text).Trim();
                            }
                            #endregion

                            #region 28 Non-Monetary
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                            {
                                Non_Monetary = Convert.ToString(xlWorksheet.Cells[i, 28].Text).Trim();
                            }
                            #endregion

                            #region 29 Years (if Non-Monetary)
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text))
                                Years_if_Non_Monetary = Convert.ToString(xlWorksheet.Cells[i, 29].Text).Trim();
                            #endregion

                            #region 30 State
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                            {
                                StateID = GetStateIDByName(xlWorksheet.Cells[i, 30].Text.Trim());
                            }
                            #endregion

                            #region 31 Law Firm
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                            {
                                Law_FirmID = GetLawFirmIDByName(CustomerID, xlWorksheet.Cells[i, 31].Text.Trim());
                            }

                            #endregion

                            #region 32 Performer (Internal User) 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                            {
                                string InternalUsers = xlWorksheet.Cells[i, 32].Text.ToString().Trim();
                                string[] split = InternalUsers.Split(',');
                                if (split.Length > 0)
                                {
                                    //string finalUsersid = "";
                                    string Usersid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        Usersid = Convert.ToString(GetInternalUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (Usersid == "0" || Usersid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            Internal_Users.Add(Usersid);
                                            suucess = true;
                                        }
                                 
                                    }
                                 
                                }
                            }

                            #endregion

                            #region 33 Performer (Lawyer User)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()))
                            {
                                string ExternalUsers = xlWorksheet.Cells[i, 33].Text.ToString().Trim();
                                string[] split = ExternalUsers.Split(',');
                                if (split.Length > 0)
                                {
                                    //string finalUsersid = "";
                                    string Usersid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        Usersid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (Usersid == "0" || Usersid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            External_Users.Add(Usersid);
                                            suucess = true;
                                        }
                                 
                                    }
                                  
                                }
                            }
                            #endregion

                            #region 34 Case Stage
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()))
                            {
                                casestageID = GetCaseStageIDByName(xlWorksheet.Cells[i, 34].Text.ToString().Trim());
                            }
                            #endregion

                            #region 35 Case Status

                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text))
                            {
                                caseStatus = xlWorksheet.Cells[i, 35].Text.Trim();
                            }
                            if (caseStatus.ToUpper() == "OPEN")
                            {
                                caseStatusID = 1;
                            }
                            else if (caseStatus.ToUpper() == "CLOSED")
                            {
                                caseStatusID = 3;
                            }
                            else if (caseStatus.ToUpper() == "SETTLED")
                            {
                                caseStatusID = 4;
                            }

                            #endregion

                            #region 36 Case Close Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 36].Text))
                                caseCloseDate = Convert.ToString(xlWorksheet.Cells[i, 36].Text).Trim();
                            #endregion

                            #region 37 Case Result

                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text))
                            {
                                caseResult = xlWorksheet.Cells[i, 37].Text.Trim();
                                caseResultID = GetResultTypeIDByName(caseResult);
                            }
                            #endregion

                            #region 38 Case Budget
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 38].Text))
                                caseBudget = Convert.ToString(xlWorksheet.Cells[i, 38].Text).Trim();
                            #endregion

                            #region 39 Case FY
                            string FNYear = xlWorksheet.Cells[i, 39].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(FNYear))
                            { 
                                string[] split = FNYear.Split(',');
                                if (split.Length > 0)
                                {
                                    string FYId = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        FYId = Convert.ToString(CaseManagement.ValidataFY(split[rs].ToString().Trim()));
                                        if (FYId == "0" || FYId == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            Finanacial_Years.Add(FYId);
                                            suucess = true;
                                        }
                                    }
                                }
                            }

                            #endregion
                            #region 40 Case Notice Date
                            if(IsNoticeDate == Convert.ToString(AuthenticationHelper.CustomerID))
                            {
                                if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 40].Text))
                                stringNoticeDate = Convert.ToString(xlWorksheet.Cells[i, 40].Text).Trim();
                            }   
                            #endregion
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 40].Text))
                            {
                                RiskTypeName = xlWorksheet.Cells[i, 40].Text.Trim();
                            }
                            if (RiskTypeName.ToUpper() == "HIGH")
                            {
                                RiskTypeID = 1;
                            }
                            else if (RiskTypeName.ToUpper() == "MEDIUM")
                            {
                                RiskTypeID = 2;
                            }
                            else if (RiskTypeName.ToUpper() == "LOW")
                            {
                                RiskTypeID = 3;
                            }    
                            tbl_LegalCaseInstance TBL_LCI = new tbl_LegalCaseInstance();
                            TBL_LCI.IsDeleted = false;
                            TBL_LCI.AssignmentType = 1;
                            TBL_LCI.CaseStageID = casestageID;
                            TBL_LCI.CaseType = Type_of_Case;
                            TBL_LCI.CaseRefNo = Court_Case_No;
                            TBL_LCI.OpenDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringCaseOpenDate).ToString("dd/MM/yyyy"));
                            TBL_LCI.Section = Section;
                            TBL_LCI.CourtID = CourtID;
                            TBL_LCI.Judge = Judge;
                            TBL_LCI.CaseCategoryID = casetypeID;
                            TBL_LCI.CaseTitle = Case_Title;
                            TBL_LCI.CaseDetailDesc = CaseDescription;
                            TBL_LCI.CustomerBranchID = Entity_Location_BranchID;
                            TBL_LCI.DepartmentID = DepartmentID;
                            TBL_LCI.OwnerID = OwnerID;
                            TBL_LCI.CaseRiskID = WinningProspectID;
                            TBL_LCI.BankGurantee = BankGuarantee;
                            TBL_LCI.RiskTypeID = RiskTypeID;

                            if (!String.IsNullOrEmpty(stringNoticeDate))
                            {
                                TBL_LCI.NoticeDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringNoticeDate).ToString("dd/MM/yyyy"));

                            }

                            if (CPDepartment != -1)
                            {
                                TBL_LCI.ContactPersonOfDepartment = CPDepartment;
                            }
                            if (jurisdiction != -1)
                            {
                                TBL_LCI.Jurisdiction = jurisdiction;
                            }
                            if (!string.IsNullOrEmpty(ProvisonalAmount))
                            {
                                TBL_LCI.Provisionalamt = Convert.ToDecimal(ProvisonalAmount);
                            }
                            if (!string.IsNullOrEmpty(ProtestAmount))
                            {
                                TBL_LCI.ProtestMoney = Convert.ToDecimal(ProtestAmount);
                            }
                            if (StateID != -1)
                            {
                                TBL_LCI.state = StateID;
                            }

                            if (!string.IsNullOrEmpty(recoveryAmount))
                            {
                                TBL_LCI.RecoveryAmount = Convert.ToDecimal(recoveryAmount);
                            }

                            if (!string.IsNullOrEmpty(ClaimedAmount))
                            {
                                TBL_LCI.ClaimAmt = Convert.ToDecimal(ClaimedAmount);
                            }
                            if (!string.IsNullOrEmpty(ProbableAmount))
                            {
                                TBL_LCI.ProbableAmt = Convert.ToDecimal(ProbableAmount);
                            }
                            if (Potential_Impact_Monetary_Non_Monetary_both == "BOTH")
                            {
                                TBL_LCI.ImpactType = "B";
                            }
                            else if (Potential_Impact_Monetary_Non_Monetary_both == "MONETARY")
                            {
                                TBL_LCI.ImpactType = "M";
                            }
                            else if (Potential_Impact_Monetary_Non_Monetary_both == "NON MONETARY")
                            {
                                TBL_LCI.ImpactType = "N";
                            }

                            TBL_LCI.Monetory = Monetary;
                            TBL_LCI.NonMonetory = Non_Monetary;
                            if (!string.IsNullOrEmpty(Years_if_Non_Monetary))
                            {
                                TBL_LCI.Years = Years_if_Non_Monetary;
                            }

                            TBL_LCI.CreatedOn = DateTime.Now;
                            TBL_LCI.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                            TBL_LCI.InternalCaseNo = Internal_Case_No;
                            TBL_LCI.CustomerID = CustomerID;

                            if (caseResultID != 0 || caseResultID != -1)
                                TBL_LCI.CaseResult = caseResultID;

                            if (!string.IsNullOrEmpty(caseBudget))
                            {
                                TBL_LCI.CaseBudget = Convert.ToDecimal(caseBudget);
                            }
                            
                            NewCaseID = CaseManagement.CreateCase(TBL_LCI);

                            if (NewCaseID > 0)
                            {
                                uploadedCaseCount++;
                                LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Case Created By Excel Upload", true);
                                bool saveSuccess = false;

                                //Case Status Transaction
                                #region Status Transaction
                                tbl_LegalCaseStatusTransaction newStatusTxnRecord = new tbl_LegalCaseStatusTransaction()
                                {
                                    CaseInstanceID = NewCaseID,
                                    StatusID = caseStatusID,
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,
                                    UserID = Portal.Common.AuthenticationHelper.UserID,
                                    RoleID = 3,
                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                    UpdatedBy = Portal.Common.AuthenticationHelper.UserID,
                                };

                                if (!CaseManagement.ExistCaseStatusTransaction(newStatusTxnRecord))
                                    saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusTxnRecord);

                                if (caseStatusID == 3)
                                {
                                    tbl_LegalCaseStatus newStatusRecord = new tbl_LegalCaseStatus()
                                    {
                                        CaseInstanceID = NewCaseID,
                                        StatusID = caseStatusID,
                                        CloseDate = Convert.ToDateTime(caseCloseDate),
                                        IsActive = true,
                                        IsDeleted = false,

                                        CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                        UpdatedBy = Portal.Common.AuthenticationHelper.UserID,
                                    };

                                    //Create or Update Status Record
                                    if (!CaseManagement.ExistCaseStatus(newStatusRecord))
                                        saveSuccess = CaseManagement.CreateCaseStatus(newStatusRecord);
                                    else
                                        saveSuccess = CaseManagement.UpdateCaseStatus(newStatusRecord);
                                }
                                #endregion

                                if (!string.IsNullOrEmpty(Convert.ToString(Law_FirmID)))
                                {
                                    #region tbl_LegalCaseLawyerMapping

                                    tbl_LegalCaseLawyerMapping TBL_LCLM = new tbl_LegalCaseLawyerMapping()
                                    {
                                        CaseInstanceID = NewCaseID,
                                        LawyerID = Law_FirmID,
                                        IsActive = true,
                                        CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };
                                    //mastertbl_LegalCaseLawyerMapping.Add(TBL_LCLM);                                    
                                    saveSuccess = CaseManagement.CreateCaseLawyerMapping(TBL_LCLM);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseLawyerMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Law Firm Mapped by Excel Upload", true);
                                    }
                                    #endregion
                                }

                                #region tbl_LegalCaseAssignment
                                if (Internal_Users.Count > 0)
                                {
                                    foreach (var item in Internal_Users)
                                    {
                                        tbl_LegalCaseAssignment TBL_LCA = new tbl_LegalCaseAssignment();
                                        TBL_LCA.AssignmentType = 1;
                                        TBL_LCA.CaseInstanceID = NewCaseID;
                                        TBL_LCA.UserID = Convert.ToInt32(item);
                                        TBL_LCA.RoleID = 3;
                                        TBL_LCA.IsActive = true;
                                        TBL_LCA.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                        TBL_LCA.CreatedOn = DateTime.Now;
                                        TBL_LCA.IsLawyer = false;

                                        mastertbl_LegalCaseAssignment.Add(TBL_LCA);
                                    }
                                }

                                if (External_Users.Count > 0)
                                {
                                    foreach (var item in External_Users)
                                    {
                                        tbl_LegalCaseAssignment TBL_LCA = new tbl_LegalCaseAssignment();
                                        TBL_LCA.AssignmentType = 1;
                                        TBL_LCA.CaseInstanceID = NewCaseID;
                                        TBL_LCA.UserID = Convert.ToInt32(item);
                                        TBL_LCA.RoleID = 3;
                                        TBL_LCA.IsActive = true;
                                        TBL_LCA.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                        TBL_LCA.CreatedOn = DateTime.Now;
                                        TBL_LCA.IsLawyer = true;

                                        mastertbl_LegalCaseAssignment.Add(TBL_LCA);
                                    }
                                }

                                if (mastertbl_LegalCaseAssignment.Count > 0)
                                {
                                    foreach (var newAssignment in mastertbl_LegalCaseAssignment)
                                    {
                                        saveSuccess = CaseManagement.CreateUpdateCaseAssignment(newAssignment);
                                    }

                                    if (saveSuccess)
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Uploaded", true);
                                }
                                #endregion

                                if (ActIds.Count > 0)
                                {
                                    #region tbl_ActMapping
                                    foreach (var item in ActIds)
                                    {
                                        tbl_ActMapping TBL_AM = new tbl_ActMapping()
                                        {
                                            ActID = Convert.ToInt32(item.ToString().Trim()),
                                            Type = 1,
                                            CaseNoticeInstanceID = NewCaseID,
                                            IsActive = true,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        mastertbl_ActMapping.Add(TBL_AM);

                                        //string[] split = item.Split(',');
                                        //if (split.Length > 0)
                                        //{
                                        //    for (int c = 0; c < split.Length; c++)
                                        //    {

                                        //    }
                                        //}
                                    }

                                    if (mastertbl_ActMapping.Count > 0)
                                        saveSuccess = CaseManagement.CreateUpdateActMapping(mastertbl_ActMapping);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_ActMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Act Mapped by Excel Upload", true);
                                    }

                                    #endregion
                                }

                                if (Opposition_Lawyers.Count > 0)
                                {
                                    #region tbl_OppositionLawyerList
                                    foreach (var item in Opposition_Lawyers)
                                    {
                                        tbl_OppositionLawyerList TBL_LLMO = new tbl_OppositionLawyerList()
                                        {
                                            LawyerID = Convert.ToInt32(item.ToString().Trim()),
                                            Type = 1,
                                            CaseNoticeInstanceID = NewCaseID,
                                            IsActive = true,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        mastertbl_OppositionLawyerList.Add(TBL_LLMO);

                                        //string[] split = item.Split(',');
                                        //if (split.Length > 0)
                                        //{
                                        //    for (int d = 0; d < split.Length; d++)
                                        //    {

                                        //    }
                                        //}
                                    }

                                    if (mastertbl_OppositionLawyerList.Count > 0)
                                        saveSuccess = CaseManagement.CreateUpdateOppositionLawyerMapping(mastertbl_OppositionLawyerList);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_OppositionLawyerList", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Opposition Lawyer Assigned by Excel Upload", true);
                                    }
                                    #endregion
                                }

                                if (Opp_Counter_Partys.Count > 0)
                                {
                                    #region tbl_PartyMapping
                                    foreach (var item in Opp_Counter_Partys)
                                    {
                                        tbl_PartyMapping TBL_PM = new tbl_PartyMapping()
                                        {
                                            PartyID = Convert.ToInt32(item.ToString().Trim()),
                                            Type = 1,
                                            CaseNoticeInstanceID = NewCaseID,
                                            IsActive = true,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        mastertbl_PartyMapping.Add(TBL_PM);

                                        //string[] split = item.Split(',');
                                        //if (split.Length > 0)
                                        //{
                                        //    for (int e = 0; e < split.Length; e++)
                                        //    {

                                        //    }
                                        //}
                                    }

                                    if (mastertbl_PartyMapping.Count > 0)
                                        saveSuccess = CaseManagement.CreateUpdatePartyMapping(mastertbl_PartyMapping);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_PartyMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Opponent Detail Created by Excel Upload", true);
                                    }
                                    #endregion
                                }
                                if (Finanacial_Years.Count > 0)
                                {
                                    #region Save Financial Year Mappping
                                    foreach (var item in Finanacial_Years)
                                    {

                                        FinancialYearMapping objFYMapping = new FinancialYearMapping()
                                        {
                                            FYID = item.ToString().Trim(),
                                            Type = 1,//1 as Case and 2 as Notice
                                            CaseNoticeInstanceID = NewCaseID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            UpdatedOn = DateTime.Now,
                                        };
                                          lstObjFYMapping.Add(objFYMapping);
                                    }
                                        saveSuccess = CaseManagement.CreateUpdateFYMapping(lstObjFYMapping);

                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("C", NewCaseID, "FinancialYearMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Financial Year Mapped.", false);
                                        }
                                    //Refresh List
                                    //lstObjFYMapping.Clear();
                                    //lstObjFYMapping = null;
                                    lstObjFYMapping = new List<FinancialYearMapping>();
                                    #endregion
                                }

                                suucessmsg = true;
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again...!!!";
                                suucessmsg = false;
                                break;
                            }
                        }//END FOREACH --END SAVE
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (suucessmsg)
                    {
                        if (uploadedCaseCount > 0)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = uploadedCaseCount + " Case Detail(s) Uploaded Successfully";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again later.";
            }
        }
        private void ProcessCaseHearingData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;
                int uploadedHearingCount = 0;
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Case Hearing"];
                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Court_Case_No = string.Empty;
                    string stringHearingDate = string.Empty;
                    string stringNextHearingDate = string.Empty;
                    string HearingDescription = string.Empty;
                    string Remarks = string.Empty;
                    long legalcaseinstanceid = -1;
                    int Law_FirmID = -1;
                    string caseBudget = string.Empty;
                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        Court_Case_No = string.Empty;
                        stringHearingDate = string.Empty;
                        HearingDescription = string.Empty;
                        Remarks = string.Empty;
                        stringNextHearingDate = string.Empty;
                        string NoticeDate = string.Empty;
                        Law_FirmID = -1;
                        caseBudget = string.Empty;

                        #region 1 Court Case No.
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Court_Case_No))
                        {
                            errorMessage.Add("Required Court Case No. at row number - " + (count + 1) + ".");
                        }
                        else
                        {
                            bool chkInternalCaseNo = CaseManagement.ExistsCaseRefNo(Court_Case_No, 0);
                            if (!chkInternalCaseNo)
                            {
                                errorMessage.Add("Case with " + Court_Case_No + " not exists at row number - " + (count + 1) + ".");
                            }
                        }
                        #endregion

                        #region 2 Hearing Date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                            stringHearingDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

                        if (stringHearingDate == "")
                        {
                            errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
                        }
                        else
                        {
                            try
                            {

                                bool check = CheckDate(stringHearingDate);
                                bool chkNoticeClosedDate = CaseManagement.ClosedDateCheck(Court_Case_No, stringHearingDate);
                                if (!check)
                                {
                                    errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
                                }
                                if (!chkNoticeClosedDate)
                                {
                                    errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be greater than Closed Date at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 3 Law Firm
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            Law_FirmID = GetLawFirmIDByName(CustomerID, xlWorksheet.Cells[i, 3].Text.Trim());
                        }
                        if (Law_FirmID == 0 || Law_FirmID == -1)
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                tbl_Lawyer objlawyer = new tbl_Lawyer()
                                {
                                    FirstName = xlWorksheet.Cells[i, 3].Text.ToString().Trim(),
                                    LastName = "",
                                    Email = "",
                                    Address = "",
                                    ContactNumber = "0",
                                    IsActive = true,
                                    CustomerID = Convert.ToInt32(CustomerID),
                                    Specilisation = null
                                };
                                LawyerManagement.CreateLawyerData(objlawyer);
                            }
                            else
                            {
                                errorMessage.Add("Please Check the Law Firm or check the values from Masters->Law Firm at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 4 Performer (Lawyer User)
                        if (Law_FirmID != 0 || Law_FirmID != -1)
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                            {
                                string ExternalUsers = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
                                string[] split = ExternalUsers.Split(',');
                                if (split.Length > 0)
                                {
                                    string Usersid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        Usersid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim(), Law_FirmID));
                                        if (Usersid == "0" || Usersid == "-1")
                                        {
                                            errorMessage.Add("Please Correct the Performer(Lawyer User) or Lawyer(" + split[rs].ToString().Trim() + ") not defined in the system at row number - " + (count + 1) + "");
                                        }
                                    }
                                }
                            }
                        }

                        #endregion

                        #region 5 Case Budget

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text))
                            caseBudget = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();

                        if (!String.IsNullOrEmpty(caseBudget))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(caseBudget, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Budget or Budget contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Budget or Budget contains only numbers at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 6 Hearing Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                        {
                            HearingDescription = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(HearingDescription))
                        {
                            errorMessage.Add("Required Hearing Description at row number - " + (count + 1) + ".");
                        }

                        #endregion

                        #region 7 Next Hearing Date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text))
                            stringNextHearingDate = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();

                        if (!string.IsNullOrEmpty(stringNextHearingDate))
                        {
                            try
                            {
                                bool check = CheckDate(stringNextHearingDate);
                                bool chkNoticeClosedDate = CaseManagement.ClosedDateCheck(Court_Case_No, stringNextHearingDate);
                                if (!check)
                                {
                                    errorMessage.Add("Please Check the Next Hearing Date or Next Hearing Date can not be empty at row - " + i + "");
                                }
                                else
                                {
                                    var HearingDate = DateTime.Parse(stringHearingDate).Date;
                                    var NextHearingDate = DateTime.Parse(stringNextHearingDate).Date;
                                    if (NextHearingDate < HearingDate)
                                    {
                                        errorMessage.Add("Please Check the Next Hearing Date or Next Hearing Date can not be less than Hearing Date at row - " + i + "");
                                    }
                                }
    
                                if (!chkNoticeClosedDate)
                                {
                                    errorMessage.Add("Please Check the Next Hearing Date or Next Hearing Date can not be greater than Closed Date at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Next Hearing Date or Next Hearing Date can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 8 Remarks
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        {
                            Remarks = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                        }
                        #endregion                        
                    }
                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            List<string> Both_Users = new List<string>();
                            count = count + 1;
                            Court_Case_No = string.Empty;
                            stringHearingDate = string.Empty;
                            HearingDescription = string.Empty;
                            Remarks = string.Empty;
                            stringNextHearingDate = string.Empty;
                            Law_FirmID = -1;
                            caseBudget = string.Empty;
                            string AssignedUserList = string.Empty;

                            #region 1 Court Case No.
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Hearing Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                                stringHearingDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            #endregion

                            #region 3 Law Firm
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                Law_FirmID = GetLawFirmIDByName(CustomerID, xlWorksheet.Cells[i, 3].Text.Trim());
                            }

                            #endregion

                            #region 4 Performer (Lawyer User)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                            {
                                string ExternalUsers = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
                                string[] split = ExternalUsers.Split(',');
                                if (split.Length > 0)
                                {
                                    //string finalUsersid = "";
                                    string Usersid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        Usersid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (Usersid == "0" || Usersid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            AssignedUserList += Usersid + ",";
                                            suucess = true;
                                        }

                                    }
                                    AssignedUserList = AssignedUserList.TrimEnd(',');
                                }
                            }
                            #endregion

                            #region 5 Hearing Description
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                caseBudget = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            }
                            #endregion

                            #region 6 Hearing Description
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                HearingDescription = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                            }
                            #endregion

                            #region 7 Next Hearing Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text))
                                stringNextHearingDate = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            #endregion

                            #region 8 Remarks
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                Remarks = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                            }
                            #endregion
                           

                            legalcaseinstanceid = CaseManagement.GetCaseInstanceIDByCourtCaseNo(Court_Case_No);

                            if (legalcaseinstanceid != 0 || legalcaseinstanceid != -1)
                            {
                                tbl_CaseHearingRef TBL_CHR = new tbl_CaseHearingRef();
                                TBL_CHR.CustomerID = CustomerID;
                                TBL_CHR.IsDeleted = false;
                                TBL_CHR.CaseNoticeInstanceID = legalcaseinstanceid;
                                if (!string.IsNullOrEmpty(stringHearingDate))
                                {
                                    TBL_CHR.HearingRefNo = "Hearing-" + Convert.ToDateTime(stringHearingDate).ToString("dd-MM-yyyy");
                                }
                                TBL_CHR.HearingDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringHearingDate).ToString("dd/MM/yyyy"));
                                TBL_CHR.CreatedOn = DateTime.Now;
                                TBL_CHR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;

                                var result = CaseManagement.GetExistsRefNo(TBL_CHR);

                                if (result != -1)
                                {
                                    var newHearingRefID = CaseManagement.CreateNewRefNo(TBL_CHR);

                                    if (newHearingRefID > 0)
                                    //saveSuccess = true;
                                    {
                                        tbl_LegalCaseResponse TBL_LCR = new tbl_LegalCaseResponse();
                                        TBL_LCR.IsActive = true;
                                        TBL_LCR.CaseInstanceID = legalcaseinstanceid;
                                        TBL_LCR.RefID = newHearingRefID;
                                        TBL_LCR.ResponseDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringHearingDate).ToString("dd/MM/yyyy"));
                                        TBL_LCR.Description = HearingDescription;
                                        TBL_LCR.UserID = Portal.Common.AuthenticationHelper.UserID;
                                        TBL_LCR.RoleID = 3;
                                        TBL_LCR.Remark = Remarks; 

                                        if (!String.IsNullOrEmpty(stringNextHearingDate))
                                            TBL_LCR.ReminderDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringNextHearingDate).ToString("dd/MM/yyyy"));

                                        TBL_LCR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                        TBL_LCR.CreatedOn = DateTime.Now;
                                        TBL_LCR.CreatedByText = Portal.Common.AuthenticationHelper.User;

                                        TBL_LCR.HearingBudget = caseBudget;
                                        if (Law_FirmID != -1)
                                        {
                                            TBL_LCR.HearingLawFirm =Convert.ToString(Law_FirmID);
                                        }
                                        TBL_LCR.HearingLawyer = AssignedUserList;

                                        var newResponseID = CaseManagement.CreateCaseResponseLog(TBL_LCR);
                                        if (newResponseID > 0)
                                        {
                                            uploadedHearingCount++;
                                            saveSuccess = true;
                                            suucessmsg = true;

                                            LitigationManagement.CreateAuditLog("C", legalcaseinstanceid, "tbl_LegalCaseResponse", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Created By Excel Upload", true);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (saveSuccess)
                    {
                        if (uploadedHearingCount > 0)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = uploadedHearingCount + " Case Hearing(s) Details Uploaded Successfully";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void ProcessCaseOrderData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedOrderCount = 0;

                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Case Orders"];
                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Court_Case_No = string.Empty;
                    int Order_TypeID = -1;
                    string Order_Date = string.Empty;
                    string Order_Title = string.Empty;
                    string Order_Description = string.Empty;
                    string Remark = string.Empty;
                    long legalcaseinstanceid = -1;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        Court_Case_No = string.Empty;
                        Order_TypeID = -1;
                        Order_Date = string.Empty;
                        Order_Title = string.Empty;
                        Order_Description = string.Empty;
                        Remark = string.Empty;

                        #region 1 Court Case No.
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Court_Case_No))
                        {
                            errorMessage.Add("Required Court Case No. at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            bool chkInternalCaseNo = CaseManagement.ExistsCaseRefNo(Court_Case_No, 0);
                            if (!chkInternalCaseNo)
                            {
                                errorMessage.Add("Case with " + Court_Case_No + " not exists at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 2 Order Type (Interim/ Judgment/Other)
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            Order_TypeID = GetOrderTypeIDByName(xlWorksheet.Cells[i, 2].Text.ToString().Trim());
                        }
                        if (Order_TypeID == 0 || Order_TypeID == -1)
                        {
                            errorMessage.Add("Please Correct the Order Type or Order Type not Defined in the System at row number - " + (count + 1) + "");
                        }

                        #endregion

                        #region 3 Order Date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text))
                            Order_Date = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();

                        if (Order_Date == "")
                        {
                            errorMessage.Add("Please Check the Order Date or Order Date can not be empty  at row - " + i + "");
                        }
                        else
                        {
                            try
                            {
                                bool check = CheckDate(Order_Date);
                                bool chkNoticeClosedDate = CaseManagement.ClosedDateCheck(Court_Case_No, Order_Date);
                                if (!check)
                                {
                                    errorMessage.Add("Please Check the Order Date or Order Date can not be empty at row - " + i + "");
                                }
                                if (!chkNoticeClosedDate)
                                {
                                    errorMessage.Add("Please Check the Order Date or Order Date can not be greater than Closed Date at row - " + i + "");
                                }

                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Order Date or Order Date can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 4 Order Title
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            Order_Title = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Order_Title))
                        {
                            errorMessage.Add("Required Order Title at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            if (Order_Title.Length <= 100)
                            {
                                bool chkcasetitle = CaseManagement.ExistsCaseOrder(Order_Title, 0);
                                if (chkcasetitle)
                                {
                                    errorMessage.Add("Order with Same Title already exists at row number - " + (count + 1) + "");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Order Title can not exceeds 100 Characters at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 5 Order Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {
                            Order_Description = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Order_Description))
                        {
                            errorMessage.Add("Required Order Description at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 6 Remarks
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                        {
                            Remark = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                        }
                        #endregion
                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            List<string> Both_Users = new List<string>();
                            count = count + 1;
                            Court_Case_No = string.Empty;
                            Order_TypeID = -1;
                            Order_Date = string.Empty;
                            Order_Title = string.Empty;
                            Order_Description = string.Empty;
                            Remark = string.Empty;

                            #region 1 Court Case No.
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Order Type (Interim/ Judgement/Other)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                            {
                                Order_TypeID = GetOrderTypeIDByName(xlWorksheet.Cells[i, 2].Text.ToString().Trim());
                            }
                            #endregion

                            #region 3 Order Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text))
                                Order_Date = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            #endregion

                            #region 4 Order Title
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                            {
                                Order_Title = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                            }
                            #endregion

                            #region 5 Order Description
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                Order_Description = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            }
                            #endregion

                            #region 6 Remarks
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                Remark = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                            }
                            #endregion

                            //TBD
                            legalcaseinstanceid = CaseManagement.GetCaseInstanceIDByCourtCaseNo(Court_Case_No);

                            if (legalcaseinstanceid != 0 || legalcaseinstanceid != -1)
                            {
                                long newOrderID = 0;

                                tbl_LegalCaseOrder TBL_CO = new tbl_LegalCaseOrder();
                                TBL_CO.IsActive = true;
                                TBL_CO.OrderTypeID = Convert.ToInt32(Order_TypeID);
                                TBL_CO.OrderTitle = Order_Title.Trim();
                                TBL_CO.OrderDesc = Order_Description.Trim();
                                TBL_CO.CaseInstanceID = Convert.ToInt32(legalcaseinstanceid);
                                TBL_CO.OrderDate = DateTimeExtensions.GetDate(Convert.ToDateTime(Order_Date).ToString("dd/MM/yyyy"));
                                TBL_CO.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                TBL_CO.CreatedByText = Portal.Common.AuthenticationHelper.User;
                                TBL_CO.UserID = Portal.Common.AuthenticationHelper.UserID;
                                TBL_CO.RoleID = 3;
                                TBL_CO.OrderRemark = Remark.Trim();
                                newOrderID = CaseManagement.CreateCaseOrderLog(TBL_CO);

                                if (newOrderID > 0)
                                {
                                    uploadedOrderCount++;
                                    saveSuccess = true;
                                    suucessmsg = true;

                                    LitigationManagement.CreateAuditLog("C", legalcaseinstanceid, "tbl_LegalCaseOrder", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Order Added by Excel Upload", true);
                                }
                                else
                                    suucessmsg = false;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (suucessmsg)
                    {
                        if (uploadedOrderCount > 0)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = uploadedOrderCount + " Case Order(s) Details Uploaded Successfully";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void ProcessCasePaymentsData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;
                int uploadedPaymentCount = 0;

                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Case Payments"];
                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Court_Case_No = string.Empty;
                    int Payment_TypeID = -1;
                    string Payment_Type = string.Empty;
                    string Payment_Date = string.Empty;
                    string Amount = string.Empty;
                    string AmountTax = string.Empty;                    
                    string Remark = string.Empty;
                    string InvoiceNo = string.Empty;
                    string AmountPaid = string.Empty;
                    string LawyerName = string.Empty;
                
                    long legalcaseinstanceid = -1;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        Court_Case_No = string.Empty;
                        Payment_TypeID = -1;
                        Payment_Date = string.Empty;
                        Amount = string.Empty;
                        AmountTax = string.Empty;
                        Remark = string.Empty;
                        Payment_Type = string.Empty;
                        InvoiceNo = string.Empty;
                        AmountPaid = string.Empty;
                        LawyerName = string.Empty;

                        #region 1 InvoiceNo 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            InvoiceNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                        }
                        if (string.IsNullOrEmpty(InvoiceNo.Trim()))
                        {
                            errorMessage.Add("Invoice No should not be emty at row number - " + (count + 1) + "");
                        }
                        #endregion


                        #region 2 Lawyer 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            LawyerName = LitigationPaymentType.getLawyerList(Convert.ToInt32(AuthenticationHelper.CustomerID), xlWorksheet.Cells[i, 2].Text.ToString().Trim());
                        }
                        if (LawyerName == null)
                        {
                            errorMessage.Add("Please Correct the Lawyer or Please check Lawyer at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 3 Court Case No.
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Court_Case_No))
                        {
                            errorMessage.Add("Required Court Case No. at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            bool chkInternalCaseNo = CaseManagement.ExistsCaseRefNo(Court_Case_No, 0);
                            if (!chkInternalCaseNo)
                            {
                                errorMessage.Add("Case with " + Court_Case_No + " not exists at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 4 Payment Type (Court Fee/ Lawyer Fee/ Miscellaneous/ Other Expenses)
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            Payment_TypeID = GetPaymentTypeIDByName(xlWorksheet.Cells[i, 4].Text.ToString().Trim());
                        }
                        if (Payment_TypeID == 0 || Payment_TypeID == -1)
                        {
                            errorMessage.Add("Please Correct the Payment Type or Please check possible values from Masters->Payment Type at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 5 Payment Date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text))
                            Payment_Date = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();

                        if (Payment_Date == "")
                        {
                            errorMessage.Add("Please Check the Payment Date or Payment Date can not be empty at row - " + i + "");
                        }
                        else
                        {
                            try
                            {
                                bool check = CheckDate(Payment_Date);
                                bool chkNoticeClosedDate = CaseManagement.ClosedDateCheck(Court_Case_No, Payment_Date);
                                if (!check)
                                {
                                    errorMessage.Add("Please Check the Payment Date or Payment Date can not be empty at row - " + i + "");
                                }
                                if (!chkNoticeClosedDate)
                                {
                                    errorMessage.Add("Please Check the Payment Date or Payment Date can not be greater than Closed Date empty at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Payment Date or Payment Date can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 6 Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text))
                            Amount = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                        if (String.IsNullOrEmpty(Amount))
                        {
                            errorMessage.Add("Required Amount at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(Amount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Amount or Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Amount or Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 7 Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text))
                            AmountPaid = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                        if (String.IsNullOrEmpty(AmountPaid))
                        {
                            errorMessage.Add("Required Amount Paid at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(AmountPaid, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Amount Paid or Amount Paid contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Amount Paid or Amount Paid contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 8 Amount Tax
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text))
                            AmountTax = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                        if (!String.IsNullOrEmpty(AmountTax))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(AmountTax, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Taxes Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Taxes Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 9 Remarks
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            Remark = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                        }
                        if (!String.IsNullOrEmpty(Remark))
                        {
                            if (Remark.Length > 250)
                            {
                                errorMessage.Add("Payment Remark can not exceeds 250 Characters at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            List<string> Both_Users = new List<string>();
                            count = count + 1;
                            Court_Case_No = string.Empty;
                            Payment_TypeID = -1;
                            Payment_Date = string.Empty;
                            Amount = string.Empty;
                            AmountTax = string.Empty;
                            Remark = string.Empty;
                            InvoiceNo = string.Empty;
                            AmountPaid = string.Empty;
                           
                            #region 1 InvoiceNo 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                InvoiceNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Lawyer 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                            {
                                LawyerName = LitigationPaymentType.getLawyerList(Convert.ToInt32(AuthenticationHelper.CustomerID), xlWorksheet.Cells[i, 2].Text.ToString().Trim());
                            }
                            #endregion

                            #region 3 Court Case No.
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            }
                            #endregion

                            #region 4 Payment Type (Court Fee/ Lawyer Fee/ Miscellaneous/ Other Expenses)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                            {
                                Payment_Type = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
                                Payment_TypeID = GetPaymentTypeIDByName(xlWorksheet.Cells[i, 4].Text.ToString().Trim());
                            }
                            #endregion

                            #region 5 Payment Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text))
                                Payment_Date = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            #endregion

                            #region 6 Amount
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                Amount = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                            }
                            #endregion

                            #region 7 Amount paid
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                            {
                                AmountPaid = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            }
                            #endregion

                            #region 8 Tax Amount
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                AmountTax = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                            }
                            #endregion

                            #region 9 Remarks
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                            {
                                Remark = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                            }

                            #endregion


                            //TBD
                            legalcaseinstanceid = CaseManagement.GetCaseInstanceIDByCourtCaseNo(Court_Case_No);
                            if (legalcaseinstanceid != -1 || legalcaseinstanceid != 0)
                            {
                                tbl_NoticeCasePayment TBL_NCP = new tbl_NoticeCasePayment()
                                {
                                    NoticeOrCase = "C",
                                    IsActive = true,
                                    NoticeOrCaseInstanceID = Convert.ToInt32(legalcaseinstanceid),
                                    PaymentDate = DateTimeExtensions.GetDate(Convert.ToDateTime(Payment_Date).ToString("dd/MM/yyyy")),
                                    PaymentID = Convert.ToInt32(Payment_TypeID),
                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = Portal.Common.AuthenticationHelper.User,
                                    Remark = Remark.Trim(),
                                    AmountPaid= Convert.ToDecimal(AmountPaid),
                                    Lawyer=LawyerName,
                                    InvoiceNo = InvoiceNo
                                };
                                if (!string.IsNullOrEmpty(Amount))
                                {
                                    TBL_NCP.Amount = Convert.ToDecimal(Amount);
                                }
                                if (!string.IsNullOrEmpty(AmountTax))
                                {
                                    TBL_NCP.AmountTax = Convert.ToDecimal(AmountTax);
                                }
                                
                                saveSuccess = CaseManagement.CreateCasePaymentLog(TBL_NCP);
                                if (saveSuccess)
                                {
                                    uploadedPaymentCount++;
                                    suucessmsg = true;

                                    LitigationManagement.CreateAuditLog("C", legalcaseinstanceid, "tbl_NoticeCasePayment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Payment Detail Added by Excel Upload", true);
                                }
                                else
                                    suucessmsg = false;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (suucessmsg)
                    {
                        if (uploadedPaymentCount > 0)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = uploadedPaymentCount + " Case Payment(s) Details Uploaded Successfully";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void rdoCCUpload_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoCCUpload.Checked == true)
                MasterFileUpload.Visible = true;

        }
        protected void rdoCHUpload_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoCHUpload.Checked == true)
                MasterFileUpload.Visible = true;
        }
        protected void rdoCOUpload_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoCOUpload.Checked == true)
                MasterFileUpload.Visible = true;
        }
        protected void rdoCPUpload_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoCPUpload.Checked == true)
                MasterFileUpload.Visible = true;
        }
        protected void sampleForm_Click(object sender, EventArgs e)
        {
            WebClient req = new WebClient();
            HttpResponse response = HttpContext.Current.Response;
            string filePath = "";
            if (RiskType == true)
            {
                filePath = Server.MapPath("~/LitigationDocuments/Litigation_RiskCase_Upload_Format.xlsx");
            }
            else if (IsNoticeDate == Convert.ToString(AuthenticationHelper.CustomerID))
            {
                filePath = Server.MapPath("~/LitigationDocuments/Litigation_Case_UploadFormat.xlsx");
            }
            else if (IsWinningProspect == true)
            {
                filePath = Server.MapPath("~/LitigationDocuments/Litigation_Case_Upload_Format_New.xlsx");   
            }
       
            else
            {
                filePath = Server.MapPath("~/LitigationDocuments/Litigation_Case_Upload_Format.xlsx"); 
            }
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment;filename=Litigation_Case_Upload_Format.xlsx");
            byte[] data = req.DownloadData(filePath);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            response.BinaryWrite(data);
            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Flush();
            Response.End();

        }
        #endregion

        #region Common Functions
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public void ErrorMessages(List<string> emsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }

        public bool checkDuplicateDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, int colNum, string providedText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            TextToCompare = xlWorksheet.Cells[i, colNum].Text.ToString().Trim();
                            if (String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                            {
                                matchSuccess = true;
                                i = lastRow; //exit from for Loop
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
        protected void lnkCC_Click(object sender, EventArgs e)
        {
            try
            {
                liCC.Attributes.Add("class", "active");
                divCase.Visible = true;
                divCase.Attributes.Remove("class");
                divCase.Attributes.Add("class", "tab-pane active");

                liLN.Attributes.Add("class", "");
                divNotice.Visible = false;
                divNotice.Attributes.Remove("class");
                divNotice.Attributes.Add("class", "tab-pane");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkLN_Click(object sender, EventArgs e)
        {
            try
            {
                liCC.Attributes.Add("class", "");
                divCase.Visible = false;
                divCase.Attributes.Remove("class");
                divCase.Attributes.Add("class", "tab-pane");

                liLN.Attributes.Add("class", "active");
                divNotice.Visible = true;
                divNotice.Attributes.Remove("class");
                divNotice.Attributes.Add("class", "tab-pane active");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static int GetActIDByName(string Actname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actid = (from row in entities.Act_Litigation
                             where row.Name.ToUpper().Trim() == Actname.ToUpper().Trim()
                             && row.IsDeleted == false
                             select row.ID).FirstOrDefault();

                return Convert.ToInt32(actid);
            }
        }
        public static int GetPartyIDByName(int customerID, string partyName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var PartyID = (from row in entities.tbl_PartyDetail
                               where row.Name.ToUpper().Trim() == partyName.ToUpper().Trim()
                               && row.CustomerID == customerID
                               select row.ID).FirstOrDefault();

                return Convert.ToInt32(PartyID);
            }
        }

        public static int GetPartyIDByEmail(int customerID, string email)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var PartyID = (from row in entities.tbl_PartyDetail
                               where row.Email.ToUpper().Trim() == email.ToUpper().Trim()
                                && row.CustomerID == customerID
                               select row.ID).FirstOrDefault();

                return Convert.ToInt32(PartyID);
            }
        }
        public static int GetInternalUserIDByEmail(int customerID, string email)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserID = (from row in entities.Users
                              where row.Email.ToUpper().Trim() == email.ToUpper().Trim()
                              && row.LawyerFirmID == null
                              && row.IsDeleted == false
                              && row.IsActive == true
                              && row.CustomerID == customerID
                              select row.ID).FirstOrDefault();

                return Convert.ToInt32(UserID);
            }
        }
        public static int GetOwnerUserIDByEmail(int customerID, string email)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserID = (from row in entities.Users
                              where row.Email.ToUpper().Trim() == email.ToUpper().Trim()
                              && row.CustomerID == customerID
                              && row.IsDeleted == false
                              && row.IsActive == true
                              select row.ID).FirstOrDefault();

                return Convert.ToInt32(UserID);
            }
        }
        public static int GetLawyerUserIDByEmail(int customerID, string email, int lawFirmID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserID = (from row in entities.Users
                              where row.Email.ToUpper().Trim() == email.ToUpper().Trim()
                              && row.LawyerFirmID == lawFirmID
                              && row.IsDeleted == false
                              && row.IsActive == true
                              && row.CustomerID == customerID
                              select row.ID).FirstOrDefault();

                return Convert.ToInt32(UserID);
            }
        }
        public static int GetLawyerUserIDByEmail(int customerID, string email)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserID = (from row in entities.Users
                              where row.Email.ToUpper().Trim() == email.ToUpper().Trim()
                              && row.LawyerFirmID != null
                              && row.IsDeleted == false
                              && row.IsActive == true
                              && row.CustomerID == customerID
                              select row.ID).FirstOrDefault();

                return Convert.ToInt32(UserID);
            }
        }
        public static int GetEntityLocationBranchIDByName(int customerID, string name)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EntityLocationBranchID = (from row in entities.CustomerBranches
                                              where row.Name.ToUpper().Trim() == name.ToUpper().Trim()
                                              && row.IsDeleted == false && row.Status == 1
                                              && row.CustomerID == customerID
                                              select row.ID).FirstOrDefault();

                return Convert.ToInt32(EntityLocationBranchID);
            }
        }

        public static int GetCaseTypeIDByName(int customerID, string Casetypename)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CaseID = (from row in entities.tbl_CaseType
                              where row.CaseType.ToUpper().Trim() == Casetypename.ToUpper().Trim()
                              && row.CustomerID == customerID
                              && row.IsDeleted == false
                              select row.ID).FirstOrDefault();

                return Convert.ToInt32(CaseID);
            }
        }

        public static int GetResultTypeIDByName(string resultName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var PaymentTypeID = (from row in entities.tbl_TypeMaster
                                     where row.TypeName.ToUpper().Trim() == resultName.ToUpper().Trim()
                                     && row.IsActive == true && row.TypeCode == "Re"
                                     select row.ID).FirstOrDefault();

                return Convert.ToInt32(PaymentTypeID);

            }
        }
        public static int GetLawFirmIDByName(int customerID, string LawFirmname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var LawFirmID = (from row in entities.tbl_Lawyer
                                 where row.FirstName.ToUpper().Trim() + row.LastName.ToUpper().Trim() == LawFirmname.ToUpper().Trim()
                                 && row.CustomerID == customerID
                                 && row.IsActive == true
                                 select row.ID).FirstOrDefault();

                return Convert.ToInt32(LawFirmID);
            }
        }
        public static int GetCourtIDByName(int customerID, string Courtname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CourtID = (from row in entities.tbl_CourtMaster
                               where row.CourtName.ToUpper().Trim() == Courtname.ToUpper().Trim()
                               && row.CustomerID == customerID
                               && row.IsDeleted == false
                               select row.ID).FirstOrDefault();

                return Convert.ToInt32(CourtID);
            }
        }

        public static int GetJusrisdictionIDByName(string Jurisdiction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CityID = (from row in entities.Cities
                               where row.Name.ToUpper().Trim() == Jurisdiction.ToUpper().Trim()
                               && row.IsDeleted == false
                               select row.ID).FirstOrDefault();

                return Convert.ToInt32(CityID);
            }
        }
        public static int GetStateIDByName(string State)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var StateID = (from row in entities.States
                              where row.Name.ToUpper().Trim() == State.ToUpper().Trim()
                              && row.IsDeleted == false
                              select row.ID).FirstOrDefault();

                return Convert.ToInt32(StateID);
            }
        }
        public static int GetCPersonIDByName(int customerID, string CPDepartment)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             //&& row.IsActive == true
                             && row.CustomerID == customerID
                             && row.FirstName.ToUpper().Trim() + " " + row.LastName.ToUpper().Trim() == CPDepartment.ToUpper().Trim()
                             && row.LitigationRoleID != null
                             select row.ID).FirstOrDefault();

                return Convert.ToInt32(query);

            }
        }
        public static int GetCaseStageIDByName(string CaseStagename)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CaseStageID = (from row in entities.tbl_TypeMaster
                                   where row.TypeName.ToUpper().Trim() == CaseStagename.ToUpper().Trim()
                                   && row.IsActive == true && row.TypeCode == "CS"
                                   && row.customerID == AuthenticationHelper.CustomerID
                                   select row.ID).FirstOrDefault();

                return Convert.ToInt32(CaseStageID);
            }
        }
        public static int GetOrderTypeIDByName(string OrderType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var OrderTypeID = (from row in entities.tbl_TypeMaster
                                   where row.TypeName.ToUpper().Trim() == OrderType.ToUpper().Trim()
                                   && row.IsActive == true && row.TypeCode == "O"
                                   select row.ID).FirstOrDefault();

                return Convert.ToInt32(OrderTypeID);
            }
        }
        public static int GetPaymentTypeIDByName(string Typename)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var PaymentTypeID = (from row in entities.tbl_PaymentType
                                     where row.TypeName.ToUpper().Trim() == Typename.ToUpper().Trim()
                                     && row.IsDeleted == false
                                     select row.ID).FirstOrDefault();

                return Convert.ToInt32(PaymentTypeID);
            }
        }
        #endregion

        #region Import Legal Notice        
        protected void rdoLNUpload_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoLNUpload.Checked == true)
                MasterFileUploadLN.Visible = true;

        }
        protected void rdoNRUpload_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoNRUpload.Checked == true)
                MasterFileUploadLN.Visible = true;
        }
        protected void rdoPIUpload_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoPIUpload.Checked == true)
                MasterFileUploadLN.Visible = true;
        }
        protected void sampleFormLN_Click(object sender, EventArgs e)
        {
            WebClient req = new WebClient();
            HttpResponse response = HttpContext.Current.Response;
            string filePath = "";
            if (RiskType == true)
            {
                filePath = Server.MapPath("~/LitigationDocuments/Litigation_RiskNotice_Upload_Format.xlsx");

            }
            else if (IsWinningProspect == true)
            {
                filePath = Server.MapPath("~/LitigationDocuments/Litigation_Notice_Upload_Format_New.xlsx");

            }
          
            else
            {
                filePath = Server.MapPath("~/LitigationDocuments/Litigation_Notice_Upload_Format.xlsx");
            }
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;          
            response.AddHeader("Content-Disposition", "attachment;filename=Litigation_Notice_Upload_Format.xlsx");
            byte[] data = req.DownloadData(filePath);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            response.BinaryWrite(data);
            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Flush();
            Response.End();

        }
        protected void btnUploadFileLN_Click(object sender, EventArgs e)
        {
            if (MasterFileUploadLN.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUploadLN.FileName);
                    MasterFileUploadLN.SaveAs(Server.MapPath("~/Litigation/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Litigation/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoLNUpload.Checked)
                            {
                                bool flag = LNTransactionSheetsExitsts(xlWorkbook, "Legal Notice");
                                if (flag == true)
                                {
                                    ProcessLegalNoticeData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Court Cases'.";
                                }
                            }
                            else if (rdoNRUpload.Checked)
                            {
                                bool flag = LNTransactionSheetsExitsts(xlWorkbook, "Notice Responses");
                                if (flag == true)
                                {
                                    ProcessNoticeResponsesData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Case Hearing'.";
                                }
                            }
                            else if (rdoPIUpload.Checked)
                            {
                                bool flag = LNTransactionSheetsExitsts(xlWorkbook, "Payment Info");
                                if (flag == true)
                                {
                                    ProcessPaymentInfoData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Case Orders'.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                            //if (suucessmsg == true)
                            //{
                            //    cvDuplicateEntry.IsValid = false;
                            //    cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";
                            //}
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
                }
            }
        }
        private bool LNTransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("Legal Notice"))
                    {
                        if (sheet.Name.Trim().Equals("Legal Notice") || sheet.Name.Trim().Equals("Notice Responses") || sheet.Name.Trim().Equals("Payment Info"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    else if (data.Equals("Notice Responses"))
                    {
                        if (sheet.Name.Trim().Equals("Legal Notice") || sheet.Name.Trim().Equals("Notice Responses") || sheet.Name.Trim().Equals("Payment Info"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    else if (data.Equals("Payment Info"))
                    {
                        if (sheet.Name.Trim().Equals("Legal Notice") || sheet.Name.Trim().Equals("Notice Responses") || sheet.Name.Trim().Equals("Payment Info"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }
        private void ProcessLegalNoticeData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;
                suucessmsg = false;

                int uploadedNoticeCount = 0;

                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Legal Notice"];
                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string Type_of_Notice = string.Empty;
                    string Legal_Notice_Open_Date = string.Empty;
                    string Ref_No = string.Empty;
                    string Notice_Title = string.Empty;
                    string Notice_Description = string.Empty;
                    List<string> ActIds = new List<string>();
                    string Section = string.Empty;
                    int Notice_CategoryID = -1;
                    List<string> Opp_Counter_Partys = new List<string>();
                    List<string> Opposition_Lawyers = new List<string>();
                    int Entity_Location_BranchID = -1;
                    int DepartmentID = -1;
                    int OwnerID = -1;
                    string WinningProspect = string.Empty;
                    int WinningProspectID = -1;
                    string ClaimedAmount = string.Empty;
                    string ProbableAmount = string.Empty;
                    string Potential_Impact_Monetary_Non_Monetary_both = string.Empty;
                    string Monetary = string.Empty;
                    string Non_Monetary = string.Empty;
                    string Years_if_Non_Monetary = string.Empty;
                    int Law_FirmID = -1;
                    List<string> Internal_Users = new List<string>();
                    List<string> External_Users = new List<string>();
                    List<string> Finanacial_Years = new List<string>();
                    string noticeStatus = string.Empty;
                    string noticeResult = string.Empty;
                    int noticeResultID = -1;
                    int Notice_statusID = -1;
                    string Date_Of_Reciept = string.Empty;
                    string noticeCloseDate = String.Empty;
                    string noticeBudget = string.Empty;
                    string recoveryAmount = string.Empty;
                    int jurisdiction = -1;
                    int CPDepartment = -1;
                    string ProvisonalAmount = string.Empty;
                    string ProtestAmount = string.Empty;
                    string BankGuarantee = string.Empty;
                    int StateID = -1;
                    string NoticeTerm = string.Empty;
                    string RiskName = string.Empty;
                    int RiskTypeID = -1;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        Type_of_Notice = string.Empty;
                        Legal_Notice_Open_Date = string.Empty;
                        Ref_No = string.Empty;
                        Notice_Title = string.Empty;
                        Notice_Description = string.Empty;
                        Section = string.Empty;
                        Notice_CategoryID = -1;
                        Entity_Location_BranchID = -1;
                        DepartmentID = -1;
                        OwnerID = -1;
                        WinningProspect = string.Empty;
                        WinningProspectID = -1;
                        ClaimedAmount = string.Empty;
                        ProbableAmount = string.Empty;
                        Potential_Impact_Monetary_Non_Monetary_both = string.Empty;
                        Monetary = string.Empty;
                        Non_Monetary = string.Empty;
                        Years_if_Non_Monetary = string.Empty;
                        Law_FirmID = -1;
                        Notice_statusID = -1;
                        Date_Of_Reciept = string.Empty;
                        noticeCloseDate = String.Empty;

                        noticeStatus = string.Empty;
                        noticeResult = string.Empty;
                        noticeResultID = -1;
                        noticeBudget = string.Empty;
                        recoveryAmount = string.Empty;
                        jurisdiction = -1;
                        CPDepartment = -1;
                        ProvisonalAmount = string.Empty;
                        ProtestAmount = string.Empty;
                        BankGuarantee = string.Empty;
                        StateID = -1;
                        NoticeTerm = string.Empty;
                        RiskName = string.Empty;
                        RiskTypeID = -1;

                        #region 1 Type of Notice (Inward/ Outward)
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Type_of_Notice = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            if (Type_of_Notice.ToUpper().Trim() == "INWARD")
                            {
                                Type_of_Notice = "I";
                            }
                            else if (Type_of_Notice.ToUpper().Trim() == "OUTWARD")
                            {
                                Type_of_Notice = "O";
                            }
                            else
                            {
                                errorMessage.Add("Correct Type of Notice (Inward or Outward) at row number - " + (count + 1) + "");
                            }
                        }
                        if (String.IsNullOrEmpty(Type_of_Notice))
                        {
                            errorMessage.Add("Required Type of Notice (Inward or Outward) at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 2 Legal Notice Open Date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                            Legal_Notice_Open_Date = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

                        if (Legal_Notice_Open_Date == "")
                        {
                            errorMessage.Add("Please Check the Notice Open Date or Notice Open Date can not be empty at row - " + i + "");
                        }
                        else
                        {
                            try
                            {
                                bool check = CheckDate(Legal_Notice_Open_Date);
                                if (!check)
                                {
                                    errorMessage.Add("Please provide valid Notice Open Date or Notice Open Date can not be empty at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please provide valid Notice Open Date or Notice Open Date can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 3 Ref No.
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            Ref_No = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Ref_No))
                        {
                            errorMessage.Add("Required Reference No. at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Ref_No))
                            {
                                bool existNoticeRefNo = NoticeManagement.ExistsNoticeRefNo(Ref_No, 0);
                                if (existNoticeRefNo)
                                {
                                    errorMessage.Add("Notice with Same Reference No already exists, Check at row number - " + (count + 1) + "");
                                }
                            }
                        }
                        #endregion

                        #region 4 Notice Title
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            Notice_Title = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Notice_Title))
                        {
                            errorMessage.Add("Required Notice Title at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            if (Notice_Title.Length <= 1000)
                            {
                                bool chkcasetitle = NoticeManagement.ExistsNotice(Notice_Title, 0,AuthenticationHelper.CustomerID);
                                if (chkcasetitle)
                                {
                                    errorMessage.Add("Notice with Same Title already exists at row number - " + (count + 1) + "");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Notice Title can not exceeds 100 Characters at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 5 Notice Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {
                            Notice_Description = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Notice_Description))
                        {
                            errorMessage.Add("Required Notice Description at row number - " + (count + 1) + "");
                        }

                        #endregion

                        #region  6 Act Mapping add to List (Act under which the case has been filed)
                        string acts = xlWorksheet.Cells[i, 6].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(acts))
                        {
                            // errorMessage.Add("Required Act at row number - " + (count + 1) + "");
                            string[] split = acts.Split('#');
                            if (split.Length > 0)
                            {
                                string actid = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    actid = Convert.ToString(GetActIDByName(split[rs].ToString().Trim()));
                                    if (actid == "0" || actid == "-1")
                                    {
                                        errorMessage.Add("Please Correct the Act Name or Act(" + split[rs].ToString().Trim() + ") not defined in the system at row number - " + (count + 1) + "");
                                    }
                                }
                            }
                        }
                        else
                        {
                            
                        }
                        #endregion

                        #region 7 Section
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                        {
                            Section = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            if (Section.Length > 1000)
                            {
                                errorMessage.Add("Section can not exceeds 100 Characters at row number - " + (count + 1) + "");
                            }
                        }
                       
                        #endregion

                        #region 8 Notice Category                        
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        {
                            Notice_CategoryID = GetCaseTypeIDByName(CustomerID, xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                        }
                        if (Notice_CategoryID == 0 || Notice_CategoryID == -1)
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                tbl_CaseType objCaseType = new tbl_CaseType();
                                objCaseType.CaseType = xlWorksheet.Cells[i, 8].Text.ToString().Trim();
                                objCaseType.IsDeleted = false;
                                objCaseType.CreatedOn = DateTime.UtcNow.Date;
                                objCaseType.CreatedBy = AuthenticationHelper.UserID;
                                objCaseType.CustomerID = (int)AuthenticationHelper.CustomerID;
                                LitigationCourtAndCaseType.CreateLegalCaseTypeDetails(objCaseType);
                            }else
                            { errorMessage.Add("Please Correct the Notice Type or Notice Type not defined in the system at row number - " + (count + 1) + "");
                            }
                            // 
                        }
                        #endregion

                        #region 9 Opponent
                        string opposition_counterpartys = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(opposition_counterpartys))
                        {
                            errorMessage.Add("Required Opponent at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            string[] split = opposition_counterpartys.Split(',');
                            if (split.Length > 0)
                            {
                                string oppcouid = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    oppcouid = Convert.ToString(GetPartyIDByName(CustomerID, split[rs].ToString().Trim()));
                                    if (oppcouid == "0" || oppcouid == "-1")
                                    {
                                        //  errorMessage.Add("Please Correct the Opponent or Opponent(" + split[rs].ToString().Trim() + ") not defined in the system at row number - " + (count + 1) + "");
                                        tbl_PartyDetail parT = new tbl_PartyDetail();

                                        parT.CustomerID = CustomerID;
                                        parT.PartyType = "Individual";
                                        parT.ContactNumber = "0";
                                        parT.CityID = 0;
                                        parT.StateID = 0;
                                        //if (split[rs].ToString().Trim().Contains("@"))
                                        //{
                                        //    parT.Email = split[rs].ToString().Trim();
                                        //    parT.Name = split[rs].ToString().Trim().Split('@')[0];
                                        //}
                                        //else
                                        //{
                                        //    parT.Email = "";
                                        //    parT.Name = split[rs].ToString().Trim();
                                        //}
                                        parT.Email = "";
                                        parT.Name = split[rs].ToString().Trim();
                                        LitigationLaw.CreateLegalCasePartyData(parT);
                                    }
                                }
                            }
                        }
                        #endregion

                        #region 10 Opposition Lawyer   
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                        {
                            string opposition_lawyer = xlWorksheet.Cells[i, 10].Text.ToString().Trim();
                            string[] split = opposition_lawyer.Split(',');
                            if (split.Length > 0)
                            {
                                string opplawid = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    opplawid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                    if (opplawid == "0" || opplawid == "-1")
                                    {
                                       errorMessage.Add("Please Correct the Opposition Lawyer or Lawyer(" + split[rs].ToString().Trim() + ") not defined in the system at row number - " + (count + 1) + "");
                                    }
                                }
                            }
                        }
                        #endregion

                        #region 11 Entity/ Location/ Branch
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                        {
                            Entity_Location_BranchID = GetEntityLocationBranchIDByName(CustomerID, xlWorksheet.Cells[i, 11].Text.ToString().Trim());
                        }
                        if (Entity_Location_BranchID == 0 || Entity_Location_BranchID == -1)
                        {
                            errorMessage.Add("Please Correct the (Entity/ Location/ Branch) or (Entity/ Location/ Branch) not Defined in the System at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 12 Jurisdiction
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                        {
                            jurisdiction = GetJusrisdictionIDByName(xlWorksheet.Cells[i, 12].Text.Trim());
                            if (jurisdiction == 0 || jurisdiction == -1)
                            {
                                errorMessage.Add("Please Correct the Jurisdiction or Jurisdiction not defined in the system at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion

                        #region 13 Department
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                        {
                            DepartmentID = CompDeptManagement.GetDepartmentIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(),(int)AuthenticationHelper.CustomerID);
                        }
                        if (DepartmentID == 0 || DepartmentID == -1)
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                            {
                                Department objDept = new Department();
                                objDept.Name = xlWorksheet.Cells[i, 13].Text.ToString().Trim();
                                objDept.IsDeleted = false;
                                objDept.CustomerID = (int)AuthenticationHelper.CustomerID;
                                CompDeptManagement.CreateDepartmentMaster(objDept);
                            }
                            else
                            {
                                errorMessage.Add("Please Correct the Department or Department not Defined in the System at row number - " + (count + 1) + "");
                            }
                           
                        }
                        #endregion

                        #region 14 Contact Person of Department
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                        {
                            CPDepartment = GetCPersonIDByName(CustomerID, xlWorksheet.Cells[i, 14].Text.Trim());
                            if (CPDepartment == 0 || CPDepartment == -1)
                            {
                                errorMessage.Add("Please Correct the Contact Person of Department or not defined in the system at row number - " + (count + 1) + "");
                            }
                        }
                     
                        #endregion

                        #region 15 Notice Term
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text))
                            NoticeTerm = Convert.ToString(xlWorksheet.Cells[i, 15].Text).Trim();

                        if (!String.IsNullOrEmpty(NoticeTerm))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(NoticeTerm, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Notice Term or Notice Term contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Notice Term or Notice Term contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 16 Owner
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                        {
                            OwnerID = GetOwnerUserIDByEmail(CustomerID, xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                         
                        }
                        if (OwnerID == 0 || OwnerID == -1)
                        {
                            errorMessage.Add("Please Correct the Owner or Owner not Defined in the System at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 17 Winning Prospect
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text))
                        {
                            WinningProspect = xlWorksheet.Cells[i, 17].Text.Trim();
                        }
                        if (WinningProspect.ToUpper() == "HIGH")
                        {
                            WinningProspectID = 1;
                        }
                        else if (WinningProspect.ToUpper() == "MEDIUM")
                        {
                            WinningProspectID = 2;
                        }
                        else if (WinningProspect.ToUpper() == "LOW")
                        {
                            WinningProspectID = 3;
                        }
                        else
                        {
                            WinningProspectID = 0;
                        }
                        if (WinningProspectID == 0)
                        {
                            if(IsWinningProspect == true)
                            {
                                WinningProspectID = 1;
                            }
                            else
                            {
                                errorMessage.Add("Please Correct the Winning Prospect or Winning Prospect can not left blank(Possible values are High/Medium/Low) at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 18 Notice Budget

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text))
                            noticeBudget = Convert.ToString(xlWorksheet.Cells[i, 18].Text).Trim();

                        if (!String.IsNullOrEmpty(noticeBudget))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(noticeBudget, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Notice Budget or Notice Budget contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Notice Budget or Notice Budget contains only numbers at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 19 Claimed Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text))
                            ClaimedAmount = Convert.ToString(xlWorksheet.Cells[i, 19].Text).Trim();

                        if (!String.IsNullOrEmpty(ClaimedAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(ClaimedAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Claimed Amount or Claimed Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Claimed Amount or Claimed Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 20 Probable Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text))
                            ProbableAmount = Convert.ToString(xlWorksheet.Cells[i, 20].Text).Trim();

                        if (!String.IsNullOrEmpty(ProbableAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(ProbableAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Probable Amount or Probable Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Probable Amount or Probable Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 21 Provisional Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text))
                            ProvisonalAmount = Convert.ToString(xlWorksheet.Cells[i, 21].Text).Trim();

                        if (!String.IsNullOrEmpty(ProvisonalAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(ProvisonalAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Provisonal Amount or Provisonal Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Provisonal Amount or Provisonal Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 22 Protest Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text))
                            ProtestAmount = Convert.ToString(xlWorksheet.Cells[i, 22].Text).Trim();

                        if (!String.IsNullOrEmpty(ProtestAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(ProtestAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Protest Amount or Protest Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Protest Amount or Protest Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 23 Recovery Amount

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text))
                            recoveryAmount = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();

                        if (!String.IsNullOrEmpty(recoveryAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(recoveryAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Notice Budget or Notice Budget contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Notice Budget or Notice Budget contains only numbers at row - " + i + "");
                            }
                        }

                        #endregion

                        #region 24 State
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                        {
                            StateID = GetStateIDByName(xlWorksheet.Cells[i, 24].Text.Trim());
                            if (StateID == 0 || StateID == -1)
                            {
                                errorMessage.Add("Please Correct the State or State not defined in the system at row number - " + (count + 1) + "");
                            }
                        }
                       
                        #endregion

                        #region 25 Bank Guarantee
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                        {
                            BankGuarantee = Convert.ToString(xlWorksheet.Cells[i, 25].Text).Trim();
                        }
                        #endregion

                        #region 26 Potential Impact - Monetary/ Non-Monetary/ both
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()))
                        {
                            Potential_Impact_Monetary_Non_Monetary_both = Convert.ToString(xlWorksheet.Cells[i, 26].Text).Trim();
                        }
                        if (!string.IsNullOrEmpty(Potential_Impact_Monetary_Non_Monetary_both))
                        {
                            if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "BOTH")
                            {
                                Potential_Impact_Monetary_Non_Monetary_both = "BOTH";
                            }
                            else if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "MONETARY")
                            {
                                Potential_Impact_Monetary_Non_Monetary_both = "MONETARY";
                            }
                            else if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "NON MONETARY")
                            {
                                Potential_Impact_Monetary_Non_Monetary_both = "NON MONETARY";
                            }
                            else
                            {
                                errorMessage.Add("Please Check the Potential Impact (Possible Values are Monetary/ Non-Monetary/ Both) at row - " + i + "");
                            }
                        }
                        else
                        {
                            Potential_Impact_Monetary_Non_Monetary_both = "BOTH";
                        }
                        #endregion

                        #region 27 Monetary
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                        {
                            Monetary = Convert.ToString(xlWorksheet.Cells[i, 27].Text).Trim();
                        }
                        #endregion

                        #region 28 Non-Monetary
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                        {
                            Non_Monetary = Convert.ToString(xlWorksheet.Cells[i, 28].Text).Trim();
                        }
                        #endregion

                        #region 29 Years (if Non-Monetary)
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text))
                            Years_if_Non_Monetary = Convert.ToString(xlWorksheet.Cells[i, 29].Text).Trim();

                        if (!String.IsNullOrEmpty(Years_if_Non_Monetary))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(Years_if_Non_Monetary, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Years (if Non-Monetary) or Years (if Non-Monetary) contains only numbers at row  - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Years (if Non-Monetary) or Years (if Non-Monetary) contains only numbers at row  - " + i + "");
                            }
                        }
                        #endregion

                        #region 30 Law Firm
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                        {
                            Law_FirmID = GetLawFirmIDByName(CustomerID, xlWorksheet.Cells[i, 30].Text.Trim());
                            
                        }
                        if (Law_FirmID == 0 || Law_FirmID == -1)
                        {

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                            {
                                tbl_Lawyer objlawyer = new tbl_Lawyer()
                                {
                                    FirstName = xlWorksheet.Cells[i, 30].Text.ToString().Trim(),
                                    LastName = "",
                                    Email = "",
                                    Address = "",
                                    ContactNumber = "0",
                                    IsActive = true,
                                    CustomerID = Convert.ToInt32(CustomerID),
                                    Specilisation = null
                                };
                                LawyerManagement.CreateLawyerData(objlawyer);
                            }
                            else
                            {
                                errorMessage.Add("Please Check the Law Firm or check the values from Masters->Law Firm at row - " + i + "");
                            }
                        }
                        #endregion

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()) || !String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                        {
                            #region 31 Performer (Internal User) 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                            {
                                string InternalUsers = xlWorksheet.Cells[i, 31].Text.ToString().Trim();
                                string[] split = InternalUsers.Split(',');
                                if (split.Length > 0)
                                {
                                    string Usersid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        Usersid = Convert.ToString(GetInternalUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (Usersid == "0" || Usersid == "-1")
                                        {
                                            errorMessage.Add("Please Correct the Performer(Internal User) or User(" + split[rs].ToString().Trim() + ") not defined in the system at row number - " + (count + 1) + "");
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region 32 Performer (Lawyer User)

                            if (Law_FirmID != 0 || Law_FirmID != -1)
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                                {
                                    string ExternalUsers = xlWorksheet.Cells[i, 32].Text.ToString().Trim();
                                    string[] split = ExternalUsers.Split(',');
                                    if (split.Length > 0)
                                    {
                                        string Usersid = "";
                                        for (int rs = 0; rs < split.Length; rs++)
                                        {
                                            //Usersid = Convert.ToString(GetLawyerUserIDByName(split[rs].ToString().Trim()));
                                            Usersid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim(), Law_FirmID));
                                            if (Usersid == "0" || Usersid == "-1")
                                            {
                                                errorMessage.Add("Please Correct the Performer(Lawyer User) or Lawyer(" + split[rs].ToString().Trim() + ") not defined in the system at row number - " + (count + 1) + "");
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            errorMessage.Add("Please Assign At Least One Performer either Internal Or Lawyer for case at row number - " + (count + 1) + "");
                        }

                        #region 33 Notice Status (Open/Closed)

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text))
                        {
                            noticeStatus = xlWorksheet.Cells[i, 33].Text.Trim();
                        }
                        if (noticeStatus.ToUpper() == "OPEN")
                        {
                            Notice_statusID = 1;
                        }
                        else if (noticeStatus.ToUpper() == "CLOSED")
                        {
                            Notice_statusID = 3;
                        }
                        else if (noticeStatus.ToUpper() == "SETTLED")
                        {
                            Notice_statusID = 4;
                        }
                        else
                        {
                            Notice_statusID = 0;
                        }

                        if (Notice_statusID == 0 || Notice_statusID == -1)
                        {
                            errorMessage.Add("Please Correct the Notice Status or Notice Status can not be Empty (Possible values are Open/Closed) at row - " + i + "");
                        }

                        #endregion

                        //#region 25 Date Of Receipt
                        //if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text))
                        //{
                        //    Date_Of_Reciept = Convert.ToString(xlWorksheet.Cells[i, 25].Text).Trim();
                        //    try
                        //    {
                        //        bool check = CheckDate(Date_Of_Reciept);
                        //        if (!check)
                        //        {
                        //            errorMessage.Add("Please Check the Notice Receipt Date or Notice Receipt Date can not be empty at row - " + i + "");
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        errorMessage.Add("Please Check the Notice Receipt Date or Notice Receipt Date can not be empty at row - " + i + "");
                        //    }
                        //}
                        //#endregion

                        #region 34 Notice Close Date

                        if (Notice_statusID == 3)
                        {
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text))
                            {
                                noticeCloseDate = xlWorksheet.Cells[i, 34].Text.Trim();
                            }

                            if (string.IsNullOrEmpty(noticeCloseDate))
                            {
                                errorMessage.Add("Notice Close Date can not be empty at row - " + i + "");
                            }
                            else
                            {
                                try
                                {
                                    bool check = CheckDate(noticeCloseDate);
                                    if (!check)
                                    {
                                        errorMessage.Add("Please provide valid Notice Close Date at row - " + i + "");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add("Please provide valid Notice Close Date or Notice Close Date can not be empty at row - " + i + "");
                                }
                            }
                        }

                        #endregion

                        #region 35 Notice Result

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text))
                        {
                            noticeResult = xlWorksheet.Cells[i, 35].Text.Trim();
                            if (!String.IsNullOrEmpty(noticeResult))
                            {
                                noticeResultID = GetResultTypeIDByName(noticeResult);
                            }
                            if (noticeResultID == 0 || noticeResultID == -1)
                            {
                                errorMessage.Add("Please Correct the Notice Result or Notice Result can not be Empty (Possible values are Win/Loss/In Progress) at row - " + i + "");
                            }
                        }
                        else if (noticeStatus.ToUpper() == "CLOSED")
                        {
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text))
                                errorMessage.Add("Please Correct the Notice Result or Notice Result can not be Empty (Possible values are Win/Loss/In Progress) at row - " + i + "");

                        }
                     
                        #endregion

                        #region 36 Case FY
                        string FNYear = xlWorksheet.Cells[i, 36].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(FNYear))
                        {
                            errorMessage.Add("Required Financial Year at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            string[] split = FNYear.Split(',');
                            if (split.Length > 0)
                            {
                                string FYId = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    FYId = Convert.ToString(CaseManagement.ValidataFY(split[rs].ToString().Trim()));
                                    if (FYId == "0" || FYId == "-1")
                                    {
                                        errorMessage.Add("Please Correct the Financial Year (" + split[rs].ToString().Trim() + ") not Defined in the System at row number - " + (count + 1) + "");
                                    }
                                }
                            }
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text))
                        {
                            RiskName = xlWorksheet.Cells[i, 37].Text.Trim();
                        }
                        if (RiskName.ToUpper() == "HIGH")
                        {
                            RiskTypeID = 1;
                        }
                        else if (RiskName.ToUpper() == "MEDIUM")
                        {
                            RiskTypeID = 2;
                        }
                        else if (RiskName.ToUpper() == "LOW")
                        {
                            RiskTypeID = 3;
                        }
                        else
                        {
                            errorMessage.Add("Please Correct the Risk or Risk can not left blank(Possible values are High/Medium/Low) at row - " + i + "");

                        }
                      
                        #endregion   
                    }    

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        List<tbl_LegalNoticeAssignment> mastertbl_LegalNoticeAssignment = new List<tbl_LegalNoticeAssignment>();
                        List<tbl_ActMapping> mastertbl_ActMapping = new List<tbl_ActMapping>();
                        List<tbl_OppositionLawyerList> mastertbl_OppositionLawyerList = new List<tbl_OppositionLawyerList>();
                        List<tbl_PartyMapping> mastertbl_PartyMapping = new List<tbl_PartyMapping>();
                        List<tbl_LegalNoticeLawyerMapping> mastertbl_LegalNoticeLawyerMapping = new List<tbl_LegalNoticeLawyerMapping>();
                        List<FinancialYearMapping> lstObjFYMapping = new List<FinancialYearMapping>();

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            if (mastertbl_LegalNoticeAssignment.Count > 0)
                                mastertbl_LegalNoticeAssignment.Clear();

                            if (mastertbl_ActMapping.Count > 0)
                                mastertbl_ActMapping.Clear();

                            if (mastertbl_OppositionLawyerList.Count > 0)
                                mastertbl_OppositionLawyerList.Clear();

                            if (mastertbl_PartyMapping.Count > 0)
                                mastertbl_PartyMapping.Clear();

                            if (mastertbl_LegalNoticeLawyerMapping.Count > 0)
                                mastertbl_LegalNoticeLawyerMapping.Clear();

                            if (ActIds.Count > 0)
                                ActIds.Clear();

                            if (Opp_Counter_Partys.Count > 0)
                                Opp_Counter_Partys.Clear();

                            if (Opposition_Lawyers.Count > 0)
                                Opposition_Lawyers.Clear();

                            if (Internal_Users.Count > 0)
                                Internal_Users.Clear();

                            if (External_Users.Count > 0)
                                External_Users.Clear();

                            if (Finanacial_Years.Count > 0)
                                Finanacial_Years.Clear();

                            List<string> Both_Users = new List<string>();
                            count = count + 1;
                            Type_of_Notice = string.Empty;
                            Legal_Notice_Open_Date = string.Empty;
                            Ref_No = string.Empty;
                            Notice_Title = string.Empty;
                            Notice_Description = string.Empty;
                            Section = string.Empty;
                            Notice_CategoryID = -1;
                            Entity_Location_BranchID = -1;
                            DepartmentID = -1;
                            OwnerID = -1;
                            WinningProspect = string.Empty;
                            WinningProspectID = -1;
                            ClaimedAmount = string.Empty;
                            ProbableAmount = string.Empty;
                            Potential_Impact_Monetary_Non_Monetary_both = string.Empty;
                            Monetary = string.Empty;
                            Non_Monetary = string.Empty;
                            Years_if_Non_Monetary = string.Empty;
                            Law_FirmID = -1;
                            noticeStatus = string.Empty;
                            Notice_statusID = -1;
                            Date_Of_Reciept = string.Empty;
                            noticeCloseDate = String.Empty;

                            noticeBudget = string.Empty;
                            recoveryAmount = string.Empty;
                            jurisdiction = -1;
                            CPDepartment = -1;
                            ProvisonalAmount = string.Empty;
                            ProtestAmount = string.Empty;
                            BankGuarantee = string.Empty;
                            StateID = -1;
                            NoticeTerm = string.Empty;
                            RiskTypeID = -1;

                            long NewNoticeID = 0;

                            #region 1 Type of Notice (Inward/ Outward)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                Type_of_Notice = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();

                                if (Type_of_Notice.ToUpper().Trim() == "Inward".ToUpper().Trim())
                                {
                                    Type_of_Notice = "I";
                                }
                                else if (Type_of_Notice.ToUpper().Trim() == "Outward".ToUpper().Trim())
                                {
                                    Type_of_Notice = "O";
                                }
                            }
                            #endregion

                            #region 2 Legal Notice Open Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                                Legal_Notice_Open_Date = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            #endregion

                            #region 3 Ref No.
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                Ref_No = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            }
                            #endregion

                            #region 4 Notice Title
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                            {
                                Notice_Title = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                            }
                            #endregion

                            #region 5 Notice Description
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                Notice_Description = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            }
                            #endregion

                            #region  6 Act Mapping add to List (Act under which the case has been filed)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                string acts = xlWorksheet.Cells[i, 6].Text.ToString().Trim();
                                string[] split = acts.Split('#');
                                if (split.Length > 0)
                                {
                                    //string finalactid = "";
                                    string actid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        actid = Convert.ToString(GetActIDByName(split[rs].ToString().Trim()));
                                        if (actid == "0" || actid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            ActIds.Add(actid);
                                            suucess = true;
                                        }
                                        //if (split.Length > 1)
                                        //{
                                        //    finalactid += actid + ",";
                                        //}
                                        //else
                                        //{
                                        //    finalactid += actid;
                                        //}
                                    }
                                    //if (suucess)
                                    //{
                                    //    ActIds.Add(finalactid.Trim(','));
                                    //}
                                }
                            }
                            #endregion

                            #region 7 Section
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                            {
                                Section = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            }
                            #endregion

                            #region 8 Notice Category                         
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                Notice_CategoryID = GetCaseTypeIDByName(CustomerID, xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                            }
                            #endregion

                            #region 9 Opponent
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                            {
                                string opposition_counterpartys = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                                string[] split = opposition_counterpartys.Split(',');
                                if (split.Length > 0)
                                {
                                    //string finaloppcouid = "";
                                    string oppcouid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        oppcouid = Convert.ToString(GetPartyIDByName(CustomerID, split[rs].ToString().Trim()));
                                        if (oppcouid == "0" || oppcouid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            Opp_Counter_Partys.Add(oppcouid);
                                            suucess = true;
                                        }
                                        //if (split.Length > 1)
                                        //{
                                        //    finaloppcouid += oppcouid + ",";
                                        //}
                                        //else
                                        //{
                                        //    finaloppcouid += oppcouid;
                                        //}
                                    }
                                    //if (suucess)
                                    //{
                                    //    Opp_Counter_Partys.Add(finaloppcouid.Trim(','));
                                    //}
                                }
                            }

                            #endregion

                            #region 10 Opposition Lawyer                        
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                            {
                                string opposition_lawyer = xlWorksheet.Cells[i, 10].Text.ToString().Trim();
                                string[] split = opposition_lawyer.Split(',');
                                if (split.Length > 0)
                                {
                                    //string finalopplawid = "";
                                    string opplawid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        opplawid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (opplawid == "0" || opplawid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            Opposition_Lawyers.Add(opplawid);
                                            suucess = true;
                                        }
                                        //if (split.Length > 1)
                                        //{
                                        //    finalopplawid += opplawid + ",";
                                        //}
                                        //else
                                        //{
                                        //    finalopplawid += opplawid;
                                        //}
                                    }
                                }
                            }

                            #endregion

                            #region 11 Entity/ Location/ Branch
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                            {
                                Entity_Location_BranchID = GetEntityLocationBranchIDByName(CustomerID, xlWorksheet.Cells[i, 11].Text.ToString().Trim());
                            }
                            #endregion

                            #region 12 Jurisdiction
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                            {
                                jurisdiction = GetJusrisdictionIDByName(xlWorksheet.Cells[i, 12].Text.Trim());
                            }
                            #endregion

                            #region 13 Department
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                            {
                                DepartmentID = CompDeptManagement.GetDepartmentIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), (int)AuthenticationHelper.CustomerID);
                            }
                            #endregion

                            #region 14 Contact Person of Department
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                            {
                                CPDepartment = GetCPersonIDByName(CustomerID, xlWorksheet.Cells[i, 14].Text.Trim());
                            }
                            #endregion

                            #region 15 Notice Term
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text))
                                NoticeTerm = Convert.ToString(xlWorksheet.Cells[i, 15].Text).Trim();

                            if (!String.IsNullOrEmpty(NoticeTerm))
                            {
                                try
                                {
                                    double n;
                                    var isNumeric = double.TryParse(NoticeTerm, out n);
                                    if (!isNumeric)
                                    {
                                        errorMessage.Add("Please Check the Notice Term or Notice Term contains only numbers at row - " + i + "");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add("Please Check the Notice Term or Notice Term contains only numbers at row - " + i + "");
                                }
                            }
                            #endregion

                            #region 16 Owner
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                            {
                                OwnerID = GetOwnerUserIDByEmail(CustomerID, xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                            }

                            #endregion

                            #region 17 Winning Prospect
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text))
                            {
                                WinningProspect = xlWorksheet.Cells[i, 17].Text.Trim();
                            }
                            if (WinningProspect.ToUpper() == "HIGH")
                            {
                                WinningProspectID = 1;
                            }
                            else if (WinningProspect.ToUpper() == "MEDIUM")
                            {
                                WinningProspectID = 2;
                            }
                            else if (WinningProspect.ToUpper() == "LOW")
                            {
                                WinningProspectID = 3;
                            }

                            #endregion

                            #region 18 Notice Budget
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text))
                                noticeBudget = Convert.ToString(xlWorksheet.Cells[i, 18].Text).Trim();
                            #endregion

                            #region 19 Claimed Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text))
                                ClaimedAmount = Convert.ToString(xlWorksheet.Cells[i, 19].Text).Trim();
                            #endregion

                            #region 20 Probable Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text))
                                ProbableAmount = Convert.ToString(xlWorksheet.Cells[i, 20].Text).Trim();
                            #endregion

                            #region 21 Provisional Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text))
                                ProvisonalAmount = Convert.ToString(xlWorksheet.Cells[i, 21].Text).Trim();
                            #endregion

                            #region 22 Protest Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text))
                                ProtestAmount = Convert.ToString(xlWorksheet.Cells[i, 22].Text).Trim();
                            #endregion

                            #region 23 Recovery Amount
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text))
                                recoveryAmount = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();
                            #endregion

                            #region 24 State
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                            {
                                StateID = GetStateIDByName(xlWorksheet.Cells[i, 24].Text.Trim());
                            }
                            #endregion

                            #region 25 Bank Guarantee
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                            {
                                BankGuarantee = Convert.ToString(xlWorksheet.Cells[i, 25].Text).Trim();
                            }
                            #endregion

                            #region 26 Potential Impact - Monetary/ Non-Monetary/ both
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()))
                            {
                                Potential_Impact_Monetary_Non_Monetary_both = Convert.ToString(xlWorksheet.Cells[i, 26].Text).Trim();
                            }
                            if (!string.IsNullOrEmpty(Potential_Impact_Monetary_Non_Monetary_both))
                            {
                                if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "BOTH")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "BOTH";
                                }
                                else if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "MONETARY")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "MONETARY";
                                }
                                else if (Potential_Impact_Monetary_Non_Monetary_both.ToUpper().Trim() == "NON MONETARY")
                                {
                                    Potential_Impact_Monetary_Non_Monetary_both = "NON MONETARY";
                                }
                            }
                            else
                            {
                                Potential_Impact_Monetary_Non_Monetary_both = "BOTH";
                            }
                            #endregion

                            #region 27 Monetary
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                            {
                                Monetary = Convert.ToString(xlWorksheet.Cells[i, 27].Text).Trim();
                            }
                            #endregion

                            #region 28 Non-Monetary
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                            {
                                Non_Monetary = Convert.ToString(xlWorksheet.Cells[i, 28].Text).Trim();
                            }
                            #endregion

                            #region 29 Years (if Non-Monetary)
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text))
                                Years_if_Non_Monetary = Convert.ToString(xlWorksheet.Cells[i, 29].Text).Trim();
                            #endregion

                            #region 30 Law Firm
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                            {
                                Law_FirmID = GetLawFirmIDByName(CustomerID, xlWorksheet.Cells[i, 30].Text.Trim());
                            }

                            #endregion

                            #region 31 Performer (Internal User) 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                            {
                                string InternalUsers = xlWorksheet.Cells[i, 31].Text.ToString().Trim();
                                string[] split = InternalUsers.Split(',');
                                if (split.Length > 0)
                                {
                                    //string finalUsersid = "";
                                    string Usersid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        Usersid = Convert.ToString(GetInternalUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (Usersid == "0" || Usersid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            Internal_Users.Add(Usersid);
                                            suucess = true;
                                        }
                                        //if (split.Length > 1)
                                        //{
                                        //    finalUsersid += Usersid + ",";
                                        //}
                                        //else
                                        //{
                                        //    finalUsersid += Usersid;
                                        //}
                                    }
                                    //if (suucess)
                                    //{
                                    //    Internal_Users.Add(finalUsersid.Trim(','));
                                    //}
                                }
                            }

                            #endregion

                            #region 32 Performer (Lawyer User)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                            {
                                string ExternalUsers = xlWorksheet.Cells[i, 32].Text.ToString().Trim();
                                string[] split = ExternalUsers.Split(',');
                                if (split.Length > 0)
                                {
                                    //string finalUsersid = "";
                                    string Usersid = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        Usersid = Convert.ToString(GetLawyerUserIDByEmail(CustomerID, split[rs].ToString().Trim()));
                                        if (Usersid == "0" || Usersid == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            External_Users.Add(Usersid);
                                            suucess = true;
                                        }
                                        //if (split.Length > 1)
                                        //{
                                        //    finalUsersid += Usersid + ",";
                                        //}
                                        //else
                                        //{
                                        //    finalUsersid += Usersid;
                                        //}
                                    }
                                    //if (suucess)
                                    //{
                                    //    External_Users.Add(finalUsersid.Trim(','));
                                    //}
                                }
                            }
                            #endregion

                            #region 33 Notice Status

                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text))
                            {
                                noticeStatus = xlWorksheet.Cells[i, 33].Text.Trim();
                            }
                            if (noticeStatus.ToUpper() == "OPEN")
                            {
                                Notice_statusID = 1;
                            }
                            else if (noticeStatus.ToUpper() == "CLOSED")
                            {
                                Notice_statusID = 3;
                            }
                            else if (noticeStatus.ToUpper() == "SETTLED")
                            {
                                Notice_statusID = 4;
                            }
                            #endregion

                            //#region 25 Date Of Receipt
                            //if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text))
                            //    Date_Of_Reciept = Convert.ToString(xlWorksheet.Cells[i, 25].Text).Trim();
                            //#endregion

                            #region 34 Notice Close Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text))
                                noticeCloseDate = Convert.ToString(xlWorksheet.Cells[i, 34].Text).Trim();
                            #endregion

                            #region 35 Notice Result
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i,35].Text))
                            {
                                noticeResult = xlWorksheet.Cells[i, 35].Text.Trim();
                                noticeResultID = GetResultTypeIDByName(noticeResult);
                            }
                            #endregion

                            #region  36 Case FY
                            string FNYear = xlWorksheet.Cells[i, 36].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(FNYear))
                            {
                                string[] split = FNYear.Split(',');
                                if (split.Length > 0)
                                {
                                    string FYId = "";
                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        FYId = Convert.ToString(CaseManagement.ValidataFY(split[rs].ToString().Trim()));
                                        if (FYId == "0" || FYId == "-1")
                                        {
                                            suucess = false;
                                        }
                                        else
                                        {
                                            Finanacial_Years.Add(FYId);
                                            suucess = true;
                                        }
                                    }
                                }
                            }
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text))
                            {
                                RiskName = xlWorksheet.Cells[i, 37].Text.Trim();
                            }
                            if (RiskName.ToUpper() == "HIGH")
                            {
                                RiskTypeID = 1;
                            }
                            else if (RiskName.ToUpper() == "MEDIUM")
                            {
                                RiskTypeID = 2;
                            }
                            else if (RiskName.ToUpper() == "LOW")
                            {
                                RiskTypeID = 3;
                            }

                            #endregion


                            tbl_LegalNoticeInstance TBL_LNI = new tbl_LegalNoticeInstance();
                            TBL_LNI.IsDeleted = false;
                            TBL_LNI.AssignmentType = 1;
                            TBL_LNI.NoticeType = Type_of_Notice;
                            TBL_LNI.RefNo = Ref_No;
                            TBL_LNI.Section = Section;
                            TBL_LNI.NoticeCategoryID = Notice_CategoryID;
                            TBL_LNI.NoticeTitle = Notice_Title;
                            TBL_LNI.NoticeDetailDesc = Notice_Description;
                            TBL_LNI.CustomerBranchID = Entity_Location_BranchID;
                            TBL_LNI.DepartmentID = DepartmentID;
                            TBL_LNI.OwnerID = OwnerID;
                            TBL_LNI.NoticeRiskID = WinningProspectID;  
                            TBL_LNI.BankGurantee = BankGuarantee;
                            TBL_LNI.RiskTypeID = RiskTypeID;

                            if (!String.IsNullOrEmpty(Legal_Notice_Open_Date))
                                TBL_LNI.NoticeDate = DateTimeExtensions.GetDate(Convert.ToDateTime(Legal_Notice_Open_Date).ToString("dd/MM/yyyy"));

                            if (CPDepartment != -1)
                            {
                                TBL_LNI.ContactPersonOfDepartment = CPDepartment;
                            }
                            if (jurisdiction != -1)
                            {
                                TBL_LNI.Jurisdiction = jurisdiction;
                            }
                            if (!string.IsNullOrEmpty(ProvisonalAmount))
                            {
                                TBL_LNI.Provisionalamt = Convert.ToDecimal(ProvisonalAmount);
                            }
                            if (!string.IsNullOrEmpty(ProtestAmount))
                            {
                                TBL_LNI.ProtestMoney = Convert.ToDecimal(ProtestAmount);
                            }
                            if (StateID != -1)
                            {
                                TBL_LNI.state = StateID;
                            }
                            if (!string.IsNullOrEmpty(NoticeTerm))
                            {
                                TBL_LNI.NoticeTerm = Convert.ToInt32(NoticeTerm);
                            }


                            if (!string.IsNullOrEmpty(ClaimedAmount))
                            {
                                TBL_LNI.ClaimAmt = Convert.ToDecimal(ClaimedAmount);
                            }
                            if (!string.IsNullOrEmpty(recoveryAmount))
                            {
                                TBL_LNI.RecoveryAmount = Convert.ToDecimal(recoveryAmount);
                            }
                            if (!string.IsNullOrEmpty(ProbableAmount))
                            {
                                TBL_LNI.ProbableAmt = Convert.ToDecimal(ProbableAmount);
                            }
                            if (Potential_Impact_Monetary_Non_Monetary_both == "BOTH")
                            {
                                TBL_LNI.ImpactType = "B";
                            }
                            else if (Potential_Impact_Monetary_Non_Monetary_both == "MONETARY")
                            {
                                TBL_LNI.ImpactType = "M";
                            }
                            else if (Potential_Impact_Monetary_Non_Monetary_both == "NON MONETARY")
                            {
                                TBL_LNI.ImpactType = "N";
                            }
                            TBL_LNI.Monetory = Monetary;
                            TBL_LNI.NonMonetory = Non_Monetary;
                            if (!string.IsNullOrEmpty(Years_if_Non_Monetary))
                            {
                                TBL_LNI.Years = Years_if_Non_Monetary;
                            }
                            TBL_LNI.CreatedOn = DateTime.Now;
                            TBL_LNI.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                            TBL_LNI.CustomerID = CustomerID;

                            if (noticeResultID != 0 || noticeResultID != -1)
                                TBL_LNI.NoticeResult = noticeResultID;

                            if (!string.IsNullOrEmpty(Date_Of_Reciept))
                            {
                                TBL_LNI.DateOfReciept = DateTimeExtensions.GetDate(Convert.ToDateTime(Date_Of_Reciept).ToString("dd/MM/yyyy")); ;
                            }

                            if (!string.IsNullOrEmpty(noticeBudget))
                            {
                                TBL_LNI.NoticeBudget = Convert.ToDecimal(noticeBudget);
                            }
                            NewNoticeID = NoticeManagement.CreateNotice(TBL_LNI);

                            if (NewNoticeID > 0)
                            {
                                uploadedNoticeCount++;
                                LitigationManagement.CreateAuditLog("N", NewNoticeID, "tbl_LegalNoticeInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Notice Uploaded", true);

                                #region tbl_LegalNoticeAssignment
                                if (Internal_Users.Count > 0)
                                {
                                    foreach (var item in Internal_Users)
                                    {
                                        tbl_LegalNoticeAssignment TBL_LNA = new tbl_LegalNoticeAssignment();
                                        TBL_LNA.AssignmentType = 1;
                                        TBL_LNA.NoticeInstanceID = NewNoticeID;
                                        TBL_LNA.UserID = Convert.ToInt32(item);
                                        TBL_LNA.RoleID = 3;
                                        TBL_LNA.IsActive = true;
                                        TBL_LNA.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                        TBL_LNA.CreatedOn = DateTime.Now;
                                        TBL_LNA.IsLawyer = false;
                                        mastertbl_LegalNoticeAssignment.Add(TBL_LNA);
                                    }
                                }

                                if (External_Users.Count > 0)
                                {
                                    foreach (var item in External_Users)
                                    {
                                        tbl_LegalNoticeAssignment TBL_LNA = new tbl_LegalNoticeAssignment();
                                        TBL_LNA.AssignmentType = 1;
                                        TBL_LNA.NoticeInstanceID = NewNoticeID;
                                        TBL_LNA.UserID = Convert.ToInt32(item);
                                        TBL_LNA.RoleID = 3;
                                        TBL_LNA.IsActive = true;
                                        TBL_LNA.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                        TBL_LNA.CreatedOn = DateTime.Now;
                                        TBL_LNA.IsLawyer = true;
                                        mastertbl_LegalNoticeAssignment.Add(TBL_LNA);
                                    }
                                }

                                if (mastertbl_LegalNoticeAssignment.Count > 0)
                                {
                                    foreach (var newAssignment in mastertbl_LegalNoticeAssignment)
                                    {
                                        saveSuccess = NoticeManagement.CreateNoticeAssignment(newAssignment);
                                    }

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", NewNoticeID, "tbl_LegalNoticeAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Created by Excel Upload", true);
                                    }
                                }
                                #endregion

                                //Notice Status Transaction
                                #region Status Transaction
                                tbl_LegalNoticeStatusTransaction newStatusTxnRecord = new tbl_LegalNoticeStatusTransaction()
                                {
                                    NoticeInstanceID = NewNoticeID,
                                    StatusID = Notice_statusID,
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,
                                    UserID = Portal.Common.AuthenticationHelper.UserID,
                                    RoleID = 3,
                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                    UpdatedBy = Portal.Common.AuthenticationHelper.UserID,
                                };

                                if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusTxnRecord))
                                    saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusTxnRecord);

                                if (Notice_statusID == 3)
                                {
                                    //Status Record - i.e. Notice Closure Record; Only Active on Notice Close otherwise DeActive 
                                    tbl_LegalNoticeStatus newStatusRecord = new tbl_LegalNoticeStatus()
                                    {
                                        NoticeInstanceID = NewNoticeID,
                                        StatusID = Notice_statusID,
                                        CloseDate = Convert.ToDateTime(noticeCloseDate),
                                        IsActive = true,
                                        IsDeleted = false,

                                        CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                        UpdatedBy = Portal.Common.AuthenticationHelper.UserID,
                                    };

                                    //Create or Update Status Record
                                    if (!NoticeManagement.ExistNoticeStatus(newStatusRecord))
                                        saveSuccess = NoticeManagement.CreateNoticeStatus(newStatusRecord);
                                    else
                                        saveSuccess = NoticeManagement.UpdateNoticeStatus(newStatusRecord);
                                }

                                #endregion

                                if (ActIds.Count > 0)
                                {
                                    #region tbl_ActMapping
                                    foreach (var item in ActIds)
                                    {
                                        tbl_ActMapping TBL_AM = new tbl_ActMapping()
                                        {
                                            ActID = Convert.ToInt32(item),
                                            Type = 2,
                                            CaseNoticeInstanceID = NewNoticeID,
                                            IsActive = true,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        mastertbl_ActMapping.Add(TBL_AM);
                                    }

                                    if (mastertbl_ActMapping.Count > 0)
                                        saveSuccess = CaseManagement.CreateUpdateActMapping(mastertbl_ActMapping);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", NewNoticeID, "tbl_ActMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Act Mapped by Excel Upload", true);
                                    }
                                    #endregion
                                }

                                if (Opposition_Lawyers.Count > 0)
                                {
                                    #region tbl_OppositionLawyerList
                                    foreach (var item in Opposition_Lawyers)
                                    {
                                        tbl_OppositionLawyerList TBL_LLMO = new tbl_OppositionLawyerList()
                                        {
                                            LawyerID = Convert.ToInt32(item),
                                            Type = 2,
                                            CaseNoticeInstanceID = NewNoticeID,
                                            IsActive = true,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        mastertbl_OppositionLawyerList.Add(TBL_LLMO);
                                    }

                                    if (mastertbl_OppositionLawyerList.Count > 0)
                                        saveSuccess = CaseManagement.CreateUpdateOppositionLawyerMapping(mastertbl_OppositionLawyerList);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", NewNoticeID, "tbl_OppositionLawyerList", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Opposition Lawyer Mapped by Excel Upload", true);
                                    }
                                    #endregion
                                }

                                if (Opp_Counter_Partys.Count > 0)
                                {
                                    #region tbl_PartyMapping
                                    foreach (var item in Opp_Counter_Partys)
                                    {
                                        tbl_PartyMapping TBL_PM = new tbl_PartyMapping()
                                        {
                                            PartyID = Convert.ToInt32(item),
                                            Type = 2,
                                            CaseNoticeInstanceID = NewNoticeID,
                                            IsActive = true,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        mastertbl_PartyMapping.Add(TBL_PM);
                                    }

                                    if (mastertbl_PartyMapping.Count > 0)
                                        saveSuccess = CaseManagement.CreateUpdatePartyMapping(mastertbl_PartyMapping);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", NewNoticeID, "tbl_PartyMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Party Mapped by Excel Upload", true);
                                    }
                                    #endregion
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(Law_FirmID)))
                                {
                                    #region tbl_LegalNoticeLawyerMapping
                                    tbl_LegalNoticeLawyerMapping TBL_LNLM = new tbl_LegalNoticeLawyerMapping()
                                    {
                                        NoticeInstanceID = NewNoticeID,
                                        LawyerID = Law_FirmID,
                                        IsActive = true,
                                        CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };
                                    //mastertbl_LegalNoticeLawyerMapping.Add(TBL_LNLM);
                                    saveSuccess = NoticeManagement.CreateNoticeLawyerMapping(TBL_LNLM);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", NewNoticeID, "tbl_LegalNoticeLawyerMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Lawyer Mapped by Excel Upload", true);
                                    }
                                    #endregion
                                }
                                if (Finanacial_Years.Count > 0)
                                {
                                    #region Save Financial Year Mappping
                                    foreach (var item in Finanacial_Years)
                                    {

                                        FinancialYearMapping objFYMapping = new FinancialYearMapping()
                                        { 
                                            FYID = item.ToString().Trim(),
                                            Type = 2,//1 as Case and 2 as Notice
                                            CaseNoticeInstanceID = NewNoticeID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            UpdatedOn = DateTime.Now,
                                        };
                                        lstObjFYMapping.Add(objFYMapping);
                                    }
                                    saveSuccess = CaseManagement.CreateUpdateFYMapping(lstObjFYMapping);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewNoticeID, "FinancialYearMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Financial Year Mapped.", false);
                                    }
                                    //Refresh List
                                    //lstObjFYMapping.Clear();
                                    //lstObjFYMapping = null;
                                    lstObjFYMapping = new List<FinancialYearMapping>();
                                    #endregion
                                }

                                suucessmsg = true;
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again...!!!";
                                suucessmsg = false;
                                break;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (suucessmsg)
                    {
                        if (uploadedNoticeCount > 0)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = uploadedNoticeCount + " Notice Detail(s) Uploaded Successfully";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again later";
            }
        }
        private void ProcessNoticeResponsesData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;
                suucessmsg = false;

                int uploadedResponseCount = 0;

                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Notice Responses"];
                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Ref_No = string.Empty;
                    int ResponeTypeID = -1;
                    string Response_Receipt_Date = string.Empty;
                    int Deliverymode = -1;
                    string Courier_Company_Post_Detail = string.Empty;
                    string Ref_Tracking_No = string.Empty;
                    string Description = string.Empty;
                    string Reply_Due_Date = string.Empty;
                    string Remarks = string.Empty;

                    long legalNoticeinstanceid = -1;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        Ref_No = string.Empty;
                        ResponeTypeID = -1;
                        Response_Receipt_Date = string.Empty;
                        Deliverymode = -1;
                        Courier_Company_Post_Detail = string.Empty;
                        Ref_Tracking_No = string.Empty;
                        Description = string.Empty;
                        Reply_Due_Date = string.Empty;
                        Remarks = string.Empty;

                        #region 1 Ref No.( refer sheet Legal Notice)
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Ref_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Required Ref No. at row number - " + (count + 1) + ".");
                        }
                        else
                        {
                            bool chkNoticeRefNo = NoticeManagement.ExistsRefNo(Ref_No);
                            if (!chkNoticeRefNo)
                            {
                                errorMessage.Add("Notice with " + Ref_No + " not exists, Please check at row number - " + (count + 1) + ".");
                            }
                        }
                        #endregion

                        #region 2 Response Type  (Sent /Received)                                       
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            if (xlWorksheet.Cells[i, 2].Text.ToString().Trim().ToUpper().Trim() == "Sent".ToUpper().Trim())
                            {
                                ResponeTypeID = 0;
                            }
                            else if (xlWorksheet.Cells[i, 2].Text.ToString().Trim().ToUpper().Trim() == "Received".ToUpper().Trim())
                            {
                                ResponeTypeID = 1;
                            }
                            else
                            {
                                errorMessage.Add("Please Correct the Response Type or Possible Values are Sent or Received, Check at row number - " + (count + 1) + "");
                            }
                        }
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Required Response Type at row number - " + (count + 1) + ".");
                        }
                        #endregion

                        #region 3 Response /Receipt date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text))
                            Response_Receipt_Date = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text))
                        {
                            errorMessage.Add("Please Check the Response /Receipt Date  or Response /Receipt Date can not be empty at row - " + i + "");
                        }
                        else
                        {
                            try
                            {
                                bool check = CheckDate(Response_Receipt_Date);
                                bool chkNoticeClosedDate = NoticeManagement.ClosedDateCheck(Ref_No, Response_Receipt_Date);
                                if (!check)
                                {
                                    errorMessage.Add("Please Check the Response /Receipt Date  or Response /Receipt Date can not be empty at row - " + i + "");
                                }
                                if (!chkNoticeClosedDate)
                                {
                                    errorMessage.Add("Please Check the Response /Receipt Date  or Response /Receipt Date can not be empty at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Response /Receipt Date  or Response /Receipt Date can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 4 Delivery mode(courier/by hand/indian post)
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper().Trim() == "Courier".ToUpper().Trim())
                            {
                                Deliverymode = 1;
                            }
                            else if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper().Trim() == "Post".ToUpper().Trim())
                            {
                                Deliverymode = 2;
                            }
                            else if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper().Trim() == "Other".ToUpper().Trim())
                            {
                                Deliverymode = 0;
                            }
                            else
                            {
                                errorMessage.Add("Please Correct the Delivery mode or Delivery mode not Defined in the System at row number - " + (count + 1) + "");
                            }
                        }
                        else
                        {
                            errorMessage.Add("Please Check the Delivery mode or Delivery mode can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 5 Courier Company/Post Detail
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {
                            Courier_Company_Post_Detail = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                        }
                        else
                        {
                            errorMessage.Add("Required Courier Company/Post Detail at row number - " + (count + 1) + ".");
                        }
                        #endregion

                        #region 6 Ref / Tracking No.
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                        {
                            Ref_Tracking_No = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                        }
                        else
                        {
                            errorMessage.Add("Required Ref / Tracking No. at row number - " + (count + 1) + ".");
                        }
                        #endregion

                        #region 7 Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                        {
                            Description = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                        }
                        else
                        {
                            errorMessage.Add("Required Description at row number - " + (count + 1) + ".");
                        }
                        #endregion

                        #region 8 Reply Due Date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text))
                        {
                            Reply_Due_Date = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                            try
                            {
                                bool check = CheckDate(Reply_Due_Date);
                                if (!check)
                                {
                                    errorMessage.Add("Please Check the Reply Due Date or Reply Due Date can not be empty at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Reply Due Date or Reply Due Date can not be empty at row - " + i + "");
                            }
                        }
                        else
                        {
                            errorMessage.Add("Required Reply due date at row number - " + (count + 1) + ".");
                        }
                        #endregion

                        #region 9 Remarks
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            Remarks = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                        }
                        #endregion                        
                    }
                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            Ref_No = string.Empty;
                            ResponeTypeID = -1;
                            Response_Receipt_Date = string.Empty;
                            Deliverymode = -1;
                            Courier_Company_Post_Detail = string.Empty;
                            Ref_Tracking_No = string.Empty;
                            Description = string.Empty;
                            Reply_Due_Date = string.Empty;
                            Remarks = string.Empty;

                            #region 1 Ref No.( refer sheet Legal Notice)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                Ref_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Respone Type  (Sent /Received)                                       
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                            {
                                if (xlWorksheet.Cells[i, 2].Text.ToString().Trim().ToUpper().Trim() == "Sent".ToUpper().Trim())
                                {
                                    ResponeTypeID = 0;
                                }
                                else if (xlWorksheet.Cells[i, 2].Text.ToString().Trim().ToUpper().Trim() == "Received".ToUpper().Trim())
                                {
                                    ResponeTypeID = 1;
                                }
                            }
                            #endregion

                            #region 3 Response /Receipt date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text))
                                Response_Receipt_Date = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            #endregion

                            #region 4 Delivery mode(courier/by hand/indian post)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                            {
                                if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper().Trim() == "Courier".ToUpper().Trim())
                                {
                                    Deliverymode = 1;
                                }
                                else if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper().Trim() == "Post".ToUpper().Trim())
                                {
                                    Deliverymode = 2;
                                }
                                else if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper().Trim() == "Other".ToUpper().Trim())
                                {
                                    Deliverymode = 0;
                                }
                            }
                            #endregion

                            #region 5 Courier Company/Post Detail
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                Courier_Company_Post_Detail = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            }
                            #endregion

                            #region 6 Ref / Tracking No.
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                Ref_Tracking_No = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                            }
                            #endregion

                            #region 7 Description
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                            {
                                Description = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            }
                            #endregion

                            #region 8 Reply Due Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text))
                            {
                                Reply_Due_Date = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                            }
                     
                            #endregion

                            #region 9 Remarks
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                            {
                                Remarks = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                            }
                            #endregion

                            legalNoticeinstanceid = NoticeManagement.GetNoticeInstanceIDByRefNo(Ref_No);
                            if (legalNoticeinstanceid != 0 || legalNoticeinstanceid != -1)
                            {
                                tbl_LegalNoticeResponse TBL_LNR = new tbl_LegalNoticeResponse();
                                TBL_LNR.IsActive = true;
                                TBL_LNR.NoticeInstanceID = legalNoticeinstanceid;
                                TBL_LNR.ResponseDate = DateTimeExtensions.GetDate(Convert.ToDateTime(Response_Receipt_Date).ToString("dd/MM/yyyy"));
                                TBL_LNR.RespondedBy = Deliverymode;
                                TBL_LNR.Description = Description;
                                TBL_LNR.Remark = Remarks;
                                TBL_LNR.UserID = Portal.Common.AuthenticationHelper.UserID;
                                TBL_LNR.RoleID = 3;
                                TBL_LNR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                TBL_LNR.CreatedOn = DateTime.Now;
                                TBL_LNR.CreatedByText = Portal.Common.AuthenticationHelper.User;
                                TBL_LNR.ResponseType = ResponeTypeID;
                                TBL_LNR.ResponseThrough = Courier_Company_Post_Detail;
                                TBL_LNR.ResponseRefNo = Ref_Tracking_No;
                                if (!string.IsNullOrEmpty(Reply_Due_Date))
                                {
                                    TBL_LNR.NoticeDueDate = DateTimeExtensions.GetDate(Convert.ToDateTime(Reply_Due_Date).ToString("dd/MM/yyyy"));
                                }
                                var newResponseID = NoticeManagement.CreateNoticeResponseLog(TBL_LNR);
                                if (newResponseID > 0)
                                {
                                    uploadedResponseCount++;
                                    saveSuccess = true;
                                    suucessmsg = true;

                                    LitigationManagement.CreateAuditLog("N", legalNoticeinstanceid, "tbl_LegalNoticeResponse", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Response Created by Excel Upload", true);
                                }
                                else
                                    suucessmsg = false;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (suucessmsg)
                    {
                        if (uploadedResponseCount > 0)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = uploadedResponseCount + " Notice Response Details Uploaded Successfully";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void ProcessPaymentInfoData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;
                suucessmsg = false;
                int uploadedPaymentCount = 0;

                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Payment Info"];
                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string Ref_No = string.Empty;
                    int Payment_TypeID = -1;
                    string Payment_Type = string.Empty;
                    string Payment_Date = string.Empty;
                    string Amount = string.Empty;
                    string TaxAmount = string.Empty;
                    string AmountPaid = string.Empty;
                    string Remark = string.Empty;
                    string InvoiceNo = string.Empty;
                    string LawyerName = string.Empty;
                  
                    long legalNoticeinstanceid = -1;

                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        Ref_No = string.Empty;
                        Payment_TypeID = -1;
                        Payment_Date = string.Empty;
                        Amount = string.Empty;
                        TaxAmount = string.Empty;
                        AmountPaid = string.Empty;
                        Remark = string.Empty;
                        Payment_Type = string.Empty;
                        InvoiceNo = string.Empty;
                        LawyerName = string.Empty;

                        #region 1 Ref No.( refer sheet Legal Notice)
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Ref_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            errorMessage.Add("Required Ref No. at row number - " + (count + 1) + ".");
                        }
                        else
                        {
                            bool chkInternalCaseNo = NoticeManagement.ExistsRefNo(Ref_No);
                            if (!chkInternalCaseNo)
                            {
                                errorMessage.Add("Notice with " + Ref_No + " not exists at row number - " + (count + 1) + ".");
                            }
                        }
                        #endregion

                        #region 2 Invoice Number
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            InvoiceNo = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                        }
                        else
                        {
                            errorMessage.Add("Required Invoice No. at row number - " + (count + 1) + ".");
                        }
                        #endregion
                      
                        #region 3 Lawyer 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            LawyerName =LitigationPaymentType.getLawyerList(Convert.ToInt32(AuthenticationHelper.CustomerID), xlWorksheet.Cells[i, 3].Text.ToString().Trim());
                        }
                        if (LawyerName == null)
                        {
                            errorMessage.Add("Please Correct the Lawyer or Please check Lawyer at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 4 Payment Date
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text))
                            Payment_Date = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();

                        if (Payment_Date == "")
                        {
                            errorMessage.Add("Please Check the Payment Date or Payment Date can not be empty at row - " + i + "");
                        }
                        else
                        {
                            try
                            {
                                bool check = CheckDate(Payment_Date);
                                bool chkNoticeClosedDate = NoticeManagement.ClosedDateCheck(Ref_No, Payment_Date);
                                if (!check)
                                {
                                    errorMessage.Add("Please Check the Payment Date or Payment Date can not be empty at row - " + i + "");
                                }
                                if (!chkNoticeClosedDate)
                                {
                                    errorMessage.Add("Please Check the Payment Date or Payment Date can not be greater than Closed date at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Payment Date or Payment Date can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 5 Payment Type (Court Fee/ Lawyer Fee/ Miscellaneous/ Other Expenses)
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {
                            Payment_TypeID = GetPaymentTypeIDByName(xlWorksheet.Cells[i, 5].Text.ToString().Trim());
                        }
                        if (Payment_TypeID == 0 || Payment_TypeID == -1)
                        {
                            errorMessage.Add("Please Correct the Payment Type or Please check possible values from Masters->Payment Type at row number - " + (count + 1) + "");
                        }
                        #endregion
                    
                        #region 6 Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text))
                            Amount = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                        if (String.IsNullOrEmpty(Amount))
                        {
                            errorMessage.Add("Required Amount. at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(Amount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Amount or Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Amount or Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 7 Amount Paid
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text))
                            AmountPaid = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                        if (String.IsNullOrEmpty(Amount))
                        {
                            errorMessage.Add("Required Amount. at row number - " + (count + 1) + "");
                        }
                        else
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(Amount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Amount Paid or Amount Paid contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Amount Paid or Amount Paid contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 8 Tax Amount
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text))
                            TaxAmount = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                        if (!String.IsNullOrEmpty(TaxAmount))
                        {
                            try
                            {
                                double n;
                                var isNumeric = double.TryParse(TaxAmount, out n);
                                if (!isNumeric)
                                {
                                    errorMessage.Add("Please Check the Tax Amount contains only numbers at row - " + i + "");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage.Add("Please Check the Tax Amount contains only numbers at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 9 Remarks
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            Remark = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                        }
                        if (!String.IsNullOrEmpty(Remark))
                        {
                            if (Remark.Length > 250)
                            {
                                errorMessage.Add("Payment Remark can not exceeds 250 Characters at row number - " + (count + 1) + "");
                            }
                        }
                        #endregion
                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            List<string> Both_Users = new List<string>();
                            count = count + 1;
                            Ref_No = string.Empty;
                            Payment_TypeID = -1;
                            Payment_Date = string.Empty;
                            Amount = string.Empty;
                            TaxAmount = string.Empty;
                            AmountPaid = string.Empty;
                            Remark = string.Empty;
                            InvoiceNo = string.Empty;
                            LawyerName = string.Empty;

                            #region 1 Court Case No.
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                Ref_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Invoice Number
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                            {
                                InvoiceNo = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            }
                            #endregion

                            #region 3 Lawyer 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                LawyerName = LitigationPaymentType.getLawyerList(Convert.ToInt32(AuthenticationHelper.CustomerID), xlWorksheet.Cells[i, 3].Text.ToString().Trim());
                            }
                            #endregion

                            #region 4 Payment Date
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text))
                                Payment_Date = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                            #endregion

                            #region 5 Payment Type (Court Fee/ Lawyer Fee/ Miscellaneous/ Other Expenses)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                Payment_Type = xlWorksheet.Cells[i, 5].Text.ToString().Trim();
                                Payment_TypeID = GetPaymentTypeIDByName(xlWorksheet.Cells[i, 5].Text.ToString().Trim());
                            }
                            #endregion
                       
                            #region 6 Amount
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                Amount = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                            }
                            #endregion

                            #region 7 Amount Paid
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                            {
                                AmountPaid = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            }
                            #endregion

                            #region 8 Amount Paid
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                TaxAmount = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                            }
                            #endregion                            

                            #region 9 Remarks
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                            {
                                Remark = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                            }
                            #endregion

                            //TBD
                            legalNoticeinstanceid = NoticeManagement.GetNoticeInstanceIDByRefNo(Ref_No);
                            if (legalNoticeinstanceid != -1 || legalNoticeinstanceid != 0)
                            {
                                tbl_NoticeCasePayment TBL_NCP = new tbl_NoticeCasePayment()
                                {
                                    NoticeOrCase = "N",
                                    IsActive = true,
                                    NoticeOrCaseInstanceID = Convert.ToInt32(legalNoticeinstanceid),
                                    PaymentDate = DateTimeExtensions.GetDate(Convert.ToDateTime(Payment_Date).ToString("dd/MM/yyyy")),
                                    PaymentID = Convert.ToInt32(Payment_TypeID),
                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = Portal.Common.AuthenticationHelper.User,
                                    Remark = Remark.Trim(),
                                    InvoiceNo = InvoiceNo,
                                    AmountPaid = Convert.ToDecimal(AmountPaid),
                                    Lawyer = LawyerName

                                };
                                if (!string.IsNullOrEmpty(TaxAmount))
                                {
                                    TBL_NCP.AmountTax = Convert.ToDecimal(TaxAmount);
                                }
                                if (!string.IsNullOrEmpty(Amount))
                                {
                                    TBL_NCP.Amount = Convert.ToDecimal(Amount);
                                }
                                saveSuccess = CaseManagement.CreateCasePaymentLog(TBL_NCP);
                                if (saveSuccess)
                                {
                                    uploadedPaymentCount++;
                                    suucessmsg = true;
                                    LitigationManagement.CreateAuditLog("N", legalNoticeinstanceid, "tbl_NoticeCasePayment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Payment Added by Excel Upload", true);
                                }
                                else
                                    suucessmsg = false;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (suucessmsg)
                    {
                        if (uploadedPaymentCount > 0)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = uploadedPaymentCount + " Notice Payment(s) Details Uploaded Successfully";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        #endregion
    }
}