﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="ShowCaseDetail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common.ShowCaseDetail" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Case Detail</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            //$('[id*=ddlLawFirm]').multiselect({
            //    includeSelectAllOption: true,
            //    numberDisplayed: 1,
            //    buttonWidth: '60%'
            //});

            $('[id*=lstBoxPerformer]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '60%'
            });

            $('[id*=lstBoxLawyerUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '60%'
            });

            $('[id*=ddlParty]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%'
            });

            $('[id*=ddlAct]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%'
            });
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
            ddlStatusChange();
        });

        function OpenDocviewer(file) {
            
            $('#DocumentReviewPopUp1').modal('show');
            $('#CaseDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtCaseDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });

           

            

            $(function () {
                $('input[id*=tbxCaseCloseDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });
        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);

        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 500, function () { });
        }

        $(document).ready(function () {
            $("#<%=tbxBranch.ClientID %>").unbind('click');

            $("#<%=tbxBranch.ClientID %>").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function ddlActChange() {
            
            var selectedActID = $("#ddlAct").find("option:selected").text();
            if (selectedActID != null) {
                if (selectedActID == "Add New") {
                    $("#lnkShowAddNewActModal").show();
                }
                else {
                    $("#lnkShowAddNewActModal").hide();
                }
            }
        }

        function ddlPartyChange() {
           
            var selectedPartyID = $("#ddlParty").find("option:selected").text();
            if (selectedPartyID != null) {
                if (selectedPartyID == "Add New") {
                    $("#lnkShowAddNewPartyModal").show();
                }
                else {
                    $("#lnkShowAddNewPartyModal>").hide();
                }
            }
        }

        function ddlCaseCategoryChange() {
            var selectedCaseCatID = $("#<%=ddlCaseCategory.ClientID %>").val();
            if (selectedCaseCatID != null) {
                if (selectedCaseCatID == 0) {
                    $("#lnkAddNewCaseCategoryModal").show();
                }
                else {
                    $("#lnkAddNewCaseCategoryModal").hide();
                }
            }
        }

        function ddlCourtChange() {
            var selectedCaseCatID = $("#<%=ddlCourt.ClientID %>").val();
            if (selectedCaseCatID != null) {
                if (selectedCaseCatID == 0) {
                    $("#lnkAddNewCourtModal").show();
                }
                else {
                    $("#lnkAddNewCourtModal").hide();
                }
            }
        }

        function ddlDepartmentChange() {
            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == 0) {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }    

        $(document).ready(function () {
            ddlStatusChange();
            rblImpactChange();

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });

        function ddlStatusChange() {
            var selectedStatusID = $("#<%=ddlCaseStatus.ClientID %>").val();
            if (selectedStatusID != null) {
                if (selectedStatusID == 3) {
                    $("#divClosureDetail").show();
                    $("#divClosureRemark").show();
                    $("#divConvertToCase").show();
                }
                else {
                    $("#divClosureDetail").hide();
                    $("#divClosureRemark").hide();
                    $("#divConvertToCase").hide();
                }
            }
        }

        function rblImpactChange() {
            
            var radioButtonlist = document.getElementsByName("<%=rblPotentialImpact.ClientID%>");
             for (var x = 0; x < radioButtonlist.length; x++) {
                 if (radioButtonlist[x].checked) {
                     var selectedImpactID = radioButtonlist[x].value;
                     if (selectedImpactID != null && selectedImpactID != '') {
                         if (selectedImpactID == 'M') {
                             $("#divMonetory").show();
                             $("#divNonMonetory").hide();
                         }
                         else if (selectedImpactID == 'N') {
                             $("#divMonetory").hide();
                             $("#divNonMonetory").show();
                         }
                         else if (selectedImpactID == 'B') {
                             $("#divMonetory").show();
                             $("#divNonMonetory").show();
                         }
                     }
                     x = radioButtonlist.length;
                 }
             }
         }
    </script>

    <style type="text/css">
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .panel-heading .nav > li > a {
            border-bottom: 0px;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
             height: 100px;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }

        label {
            font-weight: 500;
            color: #666;
        }

        .fixed {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {            
            imgExpandCollapse();
        });

        function gridPageIndexChanged() {
            imgExpandCollapse();
        }

        function imgExpandCollapse() {
            
            $("[src*=collapse]").on('click', function () {
                
                $(this).attr("src", "/Images/add.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=add]").on('click', function () {
                
                if ($(this).attr('src').indexOf('add.png') > -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "/Images/collapse.png");
                    imgExpandhide();
                } else if ($(this).attr('src').indexOf('collapse.png') > -1) {
                    $(this).attr("src", "/Images/add.png");
                    $(this).closest("tr").next().remove();
                    imgExpandhide();
                }
            });
        }

        function imgExpandhide() {
            
            $("[src*=hide]").on('click', function () {
                
                $(this).attr("src", "/Images/expand.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=expand]").on('click', function () {
                
                if ($(this).attr('src').indexOf('expand.png') > -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "/Images/hide.png");
                } else if ($(this).attr('src').indexOf('hide.png') > -1) {
                    $(this).attr("src", "/Images/expand.png");
                    $(this).closest("tr").next().remove();
                }
            });
        }

        function ChangeRowColor(rowID) {
            var color = document.getElementById(rowID).style.backgroundColor;
            var oldColor = document.getElementById(rowID).style.backgroundColor;

            if (color != 'rgb(247, 247, 247)')
                document.getElementById("hiddenColor").style.backgroundColor = color;

            if (color == 'rgb(247, 247, 247)')
                document.getElementById(rowID).style.backgroundColor = document.getElementById("hiddenColor").style.backgroundColor;
            else
                document.getElementById(rowID).style.backgroundColor = 'rgb(247, 247, 247)';
        }
    </script>
</head>
<body style="background: none !important; overflow-y: hidden;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <input type="hidden" id="hiddenColor" style="display: none;" />

        <div style="background-color: #f7f7f7;">

            <header class="panel-heading tab-bg-primary" style="background: none !important; margin-bottom: 10px;">
                <ul class="nav nav-tabs">
                    <li class="active" id="liCaseDetail" runat="server">
                        <asp:LinkButton ID="lnkCaseDetail" OnClick="TabCase_Click" runat="server" Style="background-color: #f7f7f7;">Case Summary</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseTask" runat="server">
                        <asp:LinkButton ID="lnkCaseTask" OnClick="TabTask_Click" runat="server" Style="background-color: #f7f7f7;">Task/Activity</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseHearing" runat="server">
                        <asp:LinkButton ID="lnkCaseHearing" OnClick="TabHearing_Click" runat="server" Style="background-color: #f7f7f7;">Hearing</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseOrder" runat="server">
                        <asp:LinkButton ID="lnkCaseOrder" OnClick="TabOrder_Click" runat="server" Style="background-color: #f7f7f7;">Order</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseStatus" runat="server">
                        <asp:LinkButton ID="lnkCaseStatus" OnClick="TabStatus_Click" runat="server" Style="background-color: #f7f7f7;">Status/Payment</asp:LinkButton>
                    </li>
                </ul>
            </header>

            <div class="clearfix" style="height: 20px;"></div>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="divMainView" class="boxscroll do-nicescroll4" style="height: 500px; overflow-y: auto;">
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="CaseSummaryView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">

                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="VSCasePopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="CasePopUpValidationGroup" />
                                    <asp:CustomValidator ID="cvCasePopUp" runat="server" EnableClientScript="False"
                                        ValidationGroup="CasePopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>

                                <div id="divCaseDetails" class="row Dashboard-white-widget">
                                    <!--CaseDetail Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Case Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDetails">
                                                    <a>
                                                        <h2></h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivCaseDetails" class="panel-collapse collapse in">
                                                <div class="panel-body">

                                                    <asp:Panel ID="pnlCase" runat="server">
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Type</label>
                                                                <asp:RadioButtonList ID="rbCaseInOutType" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Inward" Value="I" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Outward" Value="O"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Open Date</label>
                                                                <asp:TextBox runat="server" ID="txtCaseDate" autocomplete="off" Style="width: 60%; background-color: #fff; cursor: pointer;"
                                                                    CssClass="form-control" MaxLength="100" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Case Date can not be empty."
                                                                    ControlToValidate="txtCaseDate" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case No.</label>
                                                                <asp:TextBox runat="server" ID="tbxRefNo" Style="width: 60%" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvRefNo" ErrorMessage="Case No. can not be empty."
                                                                    ControlToValidate="tbxRefNo" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Title</label>
                                                                <asp:TextBox runat="server" ID="tbxTitle" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Title can not be empty."
                                                                    ControlToValidate="tbxTitle" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Act</label>
                                                                <div style="float: left; width: 60%">
                                                                    <%--<asp:DropDownListChosen runat="server" ID="ddlAct" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        class="form-control" DataPlaceHolder="Select Act" Width="100%" onchange="ddlActChange()" />--%>
                                                                    <asp:ListBox ID="ddlAct" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="ddlActChange()"></asp:ListBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Act or Select 'Not Applicable'"
                                                                        ControlToValidate="ddlAct" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                        Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewActModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddActPopup()" alt="Add New Party" title="Add New Party" />
                                                                    <asp:LinkButton ID="lnkBtnAct" OnClick="lnkBtnAct_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Under Section</label>
                                                                <asp:TextBox runat="server" ID="tbxSection" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Category</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlCaseCategory" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        class="form-control" Width="100%" DataPlaceHolder="Select Case Category" onchange="ddlCaseCategoryChange()">
                                                                    </asp:DropDownListChosen>
                                                                    <asp:RequiredFieldValidator ID="rfvCaseCategory" ErrorMessage="Please Select Case Category"
                                                                        ControlToValidate="ddlCaseCategory" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>

                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkAddNewCaseCategoryModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenCategoryTypePopup()" alt="Add New Party" title="Add New Party" />
                                                                    <asp:LinkButton ID="lnkBtnCategory" OnClick="lnkBtnCategory_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Opposition/Party</label>
                                                                <div style="float: left; width: 60%">
                                                                    <%--<asp:DropDownListChosen runat="server" ID="ddlParty" AllowSingleDeselect="false" DisableSearchThreshold="5" DataPlaceHolder="Select Opposition/Party"
                                                                        CssClass="form-control" Width="100%" onchange="ddlPartyChange()" />--%>
                                                                    <asp:ListBox ID="ddlParty" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="ddlPartyChange()"></asp:ListBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Party"
                                                                        ControlToValidate="ddlParty" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewPartyModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenPartyDetailsPopup()" alt="Add New Party" title="Add New Party" />
                                                                    <asp:LinkButton ID="lnkBtnParty" OnClick="lnkBtnParty_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Court</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlCourt" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        class="form-control" Width="100%" DataPlaceHolder="Select Court" onchange="ddlCourtChange()">
                                                                    </asp:DropDownListChosen>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Select Court"
                                                                        ControlToValidate="ddlCourt" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>

                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkAddNewCourtModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenCourtPopup()" alt="Add New Court" title="Add New Court" />
                                                                    <asp:LinkButton ID="lblCourt" OnClick="lblCourt_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Judge</label>
                                                                <asp:TextBox runat="server" ID="tbxJudge" Style="width: 60%" CssClass="form-control" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvJudge" ErrorMessage="Provide Judge Detail."
                                                                    ControlToValidate="tbxJudge" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Description</label>
                                                                <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description can not be empty."
                                                                    ControlToValidate="tbxDescription" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Entity/Location</label>

                                                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 70%; background-color: #fff; cursor: pointer;"
                                                                    autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" ReadOnly="true" />
                                                                <%--onclick="txtclick()"--%>
                                                                <div style="margin-left: 28%; position: absolute; z-index: 10; width: 70%;" id="divBranches">
                                                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged"
                                                                        Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                                    </asp:TreeView>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity/Location"
                                                                    ControlToValidate="tbxBranch" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Department</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Department" class="form-control" Width="100%" onchange="ddlDepartmentChange()" />
                                                                    <asp:RequiredFieldValidator ID="rfvDept" ErrorMessage="Please Select Department"
                                                                        ControlToValidate="ddlDepartment" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkAddNewDepartmentModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenDepartmentPopup('')" alt="Add New Department" title="Add New Department" />
                                                                    <asp:LinkButton ID="lnkBtnDept" OnClick="lnkBtnDept_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Owner</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlOwner" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />
                                                                <asp:RequiredFieldValidator ID="rfvOwner" ErrorMessage="Please Select Owner"
                                                                    ControlToValidate="ddlOwner" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Winning Prospect</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCaseRisk" DataPlaceHolder="Select Risk" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%">
                                                                    <%-- <asp:ListItem Text="Select Risk" Value="-1" Selected="True"></asp:ListItem>--%>
                                                                    <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Claimed Amount</label>
                                                                <asp:TextBox runat="server" ID="tbxClaimedAmt" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:CompareValidator ID="cvClaimedAmt" runat="server" ControlToValidate="tbxClaimedAmt" ErrorMessage="Only Numbers in Claimed Amount."
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Probable Amount</label>
                                                                <asp:TextBox runat="server" ID="tbxProbableAmt" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:CompareValidator ID="cvProbableAmt" runat="server" ControlToValidate="tbxProbableAmt" ErrorMessage="Only Numbers in Probable Amount."
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Potential Impact</label>
                                                                <asp:RadioButtonList ID="rblPotentialImpact" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Monetary" Value="M" Selected="True" onclick="rblImpactChange()"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Non-Monetary" Value="N" onclick="rblImpactChange()"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Both" Value="B" onclick="rblImpactChange()"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>

                                                            <div class="form-group col-md-6" id="divMonetory">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Monetary</label>
                                                                <asp:TextBox runat="server" ID="tbxMonetory" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row" id="divNonMonetory">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Non-Monetory</label>
                                                                <asp:TextBox runat="server" ID="tbxNonMonetory" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Years</label>
                                                                <asp:TextBox runat="server" ID="tbxNonMonetoryYears" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Upload Documents</label>
                                                                <div style="width: 100%;">
                                                                    <div style="width: 50%; float: left;">
                                                                        <asp:FileUpload ID="CaseFileUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93;" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <asp:UpdatePanel ID="upCaseDocUploadPopup" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:GridView runat="server" ID="grdCaseDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCaseDocuments_RowCommand"
                                                                        OnRowDataBound="grdCaseDocuments_RowDataBound" OnPageIndexChanging="grdCaseDocuments_PageIndexChanging">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Document" ItemStyle-Width="40%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                        <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                        <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton
                                                                                                CommandArgument='<%# Eval("ID")%>' CommandName="DownloadCaseDoc"
                                                                                                ID="lnkBtnDownLoadCaseDoc" runat="server">
                                                                                         <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download" /> <%--width="15" height="15"--%>
                                                                                            </asp:LinkButton>
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                    AutoPostBack="true" CommandName="ViewCaseOrder"
                                                                                                    ID="lnkBtnViewDocCase" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                </asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDownLoadCaseDoc" />    
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnViewDocCase" />                                                                                         
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />                                                                        
                                                                        <EmptyDataTemplate>
                                                                            No Records Found.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--CaseDetail Panel End-->
                                </div>

                                <div id="divCaseAssignmentDetails" class="row Dashboard-white-widget">
                                    <!--CaseAssignment Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Case Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseAssignmentDetails">
                                                    <a>
                                                        <h2>Assignment Details</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                            href="#collapseDivCaseAssignmentDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivCaseAssignmentDetails" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:Panel ID="pnlCaseAssignment" runat="server">

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Lawyer Firm</label>
                                                               <asp:DropDownListChosen ID="ddlLawFirm" CssClass="form-control" AutoPostBack="true" runat="server" DataPlaceHolder="Select User" Width="70%"
                                                                     OnSelectedIndexChanged="ddlLawFirm_SelectedIndexChanged" AllowSingleDeselect="false"></asp:DropDownListChosen>                                                                
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Assigned To (Internal)</label>
                                                                 <asp:ListBox ID="lstBoxPerformer" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%"></asp:ListBox>                                                                
                                                            </div>                                                          

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Assigned To (External)</label>
                                                                 <asp:ListBox ID="lstBoxLawyerUser" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%"></asp:ListBox>                                                              
                                                            </div>  
                                                        </div> 

                                                        <div class="row" style="display: none;">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Reviewer</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlReviewer" DataPlaceHolder="Select Reviewer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--Case Assignment Panel End-->
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="TaskView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="ThirdTabAccordion">

                                    <div class="row Dashboard-white-widget">
                                        <!--Task/Activity Panel Start-->
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">

                                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Case Detail">
                                                    <div class="panel-heading">
                                                        <%--data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskLogs"--%>
                                                        <a>
                                                            <h2></h2>
                                                        </a>
                                                        <div class="panel-actions">
                                                            <%--<a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskLogs">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>--%>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="collapseDivTaskLogs" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="container">
                                                             <div class="row">
                                                                    <asp:ValidationSummary ID="ValidationSummary5" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                        ValidationGroup="CasePopUpTaskValidationGroup" />
                                                                    <asp:CustomValidator ID="cvCasePopUpTask" runat="server" EnableClientScript="False"
                                                                        ValidationGroup="CasePopUpTaskValidationGroup" Display="None" />
                                                                </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <asp:UpdatePanel ID="upCaseTaskActivity" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                                OnRowCommand="grdTaskActivity_RowCommand" OnRowDataBound="grdTaskActivity_RowDataBound" DataKeyNames="TaskID"
                                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCreated="grdTaskActivity_RowCreated"
                                                                                OnPageIndexChanging="grdTaskActivity_OnPageIndexChanging">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <img id="imgCollapseExpand" class="sample" src="/Images/add.png" runat="server" alt="Show Responses" style="cursor: pointer" />
                                                                                            <asp:Panel ID="pnlTaskResponse" runat="server" Style="display: none">
                                                                                                <asp:GridView ID="gvTaskResponses" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                                                    Width="100%" ShowHeaderWhenEmpty="false" GridLines="None" OnRowCommand="grdTaskResponseLog_RowCommand"
                                                                                                    OnRowDataBound="grdTaskResponseLog_RowDataBound">
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                                            <ItemTemplate>
                                                                                                                <%#Container.DataItemIndex+1 %>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>

                                                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Response" ItemStyle-Width="20%">
                                                                                                            <ItemTemplate>
                                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                    <asp:Label ID="lblTask" runat="server" Text='<%# Eval("Description") %>'
                                                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                                                </div>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>

                                                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="20%">
                                                                                                            <ItemTemplate>
                                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                                                    <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("Remark") %>'
                                                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                                                </div>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>

                                                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Responded On" ItemStyle-Width="20%">
                                                                                                            <ItemTemplate>
                                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                    <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                                        data-toggle="tooltip" data-placement="bottom"
                                                                                                                        ToolTip='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                                                </div>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>

                                                                                                        <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                                            <ItemTemplate>
                                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                    <asp:Label ID="lblTaskResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                                                    </asp:Label>
                                                                                                                </div>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>

                                                                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                                            ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                                                        </asp:LinkButton>
                                                                                                                    </ContentTemplate>
                                                                                                                    <Triggers>
                                                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                                                        
                                                                                                                    </Triggers>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                    </Columns>
                                                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                                                    <EmptyDataTemplate>
                                                                                                        No Response Submitted yet.
                                                                                                    </EmptyDataTemplate>
                                                                                                </asp:GridView>
                                                                                            </asp:Panel>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Hearing" ItemStyle-Wrap="true" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                <asp:Label ID="lblTaskHearingRef" runat="server" Text='<%# Eval("HearingRefNo") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("HearingRefNo") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>                                                                                 

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Wrap="true" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                <HeaderStyle CssClass="clsheadergrid" />

                                                                                <EmptyDataTemplate>
                                                                                    No Records Found.
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Task/Activity Panel End-->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="HearingView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">

                                <div id="secondTabAccordion">
                                    <div class="row Dashboard-white-widget">
                                        <!--Hearing Detail Panel Start-->
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">

                                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View case Detail">
                                                    <div class="panel-heading">
                                                        <%--data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivHearingLogs"--%>
                                                        <a>
                                                            <h2></h2>
                                                        </a>
                                                        <div class="panel-actions">
                                                            <%--<a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivHearingLogs">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>--%>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="collapseDivHearingLogs" class="panel-collapse collapse  in">
                                                    <div class="panel-body">
                                                        <div class="container"> 
                                                            <div class="row">
                                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                        ValidationGroup="CasePopUpHearingValidationGroup" />
                                                                    <asp:CustomValidator ID="cvCasePopUpResponse" runat="server" EnableClientScript="False"
                                                                        ValidationGroup="CasePopUpHearingValidationGroup" Display="None" />
                                                                </div>
                                                             
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <asp:UpdatePanel ID="upResponseDocUpload" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView runat="server" ID="grdResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" DataKeyNames="ResponseID"
                                                                                OnPageIndexChanging="grdResponseLog_OnPageIndexChanging" OnRowCommand="grdResponseLog_RowCommand"
                                                                                OnRowDataBound="grdResponseLog_RowDataBound" OnRowCreated="grdResponseLog_RowCreated">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <img id="imgCollapseExpand" class="sample" src="/Images/add.png" runat="server" alt="Show Responses" style="cursor: pointer" />
                                                                                            <asp:Panel ID="pnlTask" runat="server" Style="display: none">
                                                                                                <asp:UpdatePanel ID="upResponseTask" runat="server">
                                                                                                    <ContentTemplate>
                                                                                                        <asp:GridView runat="server" ID="grdResTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                                            GridLines="None" PageSize="5" AllowPaging="false" AutoPostBack="true" CssClass="table" Width="100%"
                                                                                                            OnRowCommand="grdTaskActivity_RowCommand" OnRowDataBound="grdTaskActivity_RowDataBound" DataKeyNames="TaskID"
                                                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdResTaskActivity_OnPageIndexChanging">
                                                                                                            <%--OnRowCreated="grdTaskActivity_RowCreated"--%>
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <img id="imgCollapseExpand" class="sample" src="/Images/expand.png" runat="server" alt="Show Responses" style="cursor: pointer" />
                                                                                                                        <asp:Panel ID="pnlTaskResponse" runat="server" Style="display: none">
                                                                                                                            <asp:GridView ID="gvTaskResponses" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                                                                                Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="grdTaskResponseLog_RowCommand"
                                                                                                                                OnRowDataBound="grdTaskResponseLog_RowDataBound" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                                                                                <Columns>
                                                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>

                                                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Response" ItemStyle-Width="20%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                                                <asp:Label ID="lblTask" runat="server" Text='<%# Eval("Description") %>'
                                                                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                                                                            </div>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>

                                                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="20%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                                                                                <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("Remark") %>'
                                                                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                                                                            </div>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>

                                                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Responded On" ItemStyle-Width="20%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                                                <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                                                                    data-toggle="tooltip" data-placement="bottom"
                                                                                                                                                    ToolTip='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                                                                            </div>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>

                                                                                                                                    <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                                                <asp:Label ID="lblTaskResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                                                                                </asp:Label>
                                                                                                                                            </div>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>

                                                                                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                                                                                <ContentTemplate>
                                                                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                                                                        ID="lnkBtnDownloadResTaskResDoc" runat="server">
                                                                                                                                                    <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                                                                                    </asp:LinkButton>
                                                                                                                                                </ContentTemplate>
                                                                                                                                                <Triggers>
                                                                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownloadResTaskResDoc" />                                                                                                                                                   
                                                                                                                                                </Triggers>
                                                                                                                                            </asp:UpdatePanel>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>
                                                                                                                                </Columns>
                                                                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                                                                <EmptyDataTemplate>
                                                                                                                                    No Response Submitted yet.
                                                                                                                                </EmptyDataTemplate>
                                                                                                                            </asp:GridView>
                                                                                                                        </asp:Panel>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>

                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>

                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Hearing" ItemStyle-Wrap="true" ItemStyle-Width="20%" Visible="false">
                                                                                                                    <ItemTemplate>
                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                            <asp:Label ID="lblTaskHearingRef" runat="server" Text='<%# Eval("HearingRefNo") %>'
                                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("HearingRefNo") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>

                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Wrap="true" ItemStyle-Width="20%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>

                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                            <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>

                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="20%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                            <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>

                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="20%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                                            <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>

                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                                                            <HeaderStyle CssClass="clsheadergrid" />

                                                                                                            <EmptyDataTemplate>
                                                                                                                No Records Found.
                                                                                                            </EmptyDataTemplate>
                                                                                                        </asp:GridView>
                                                                                                    </ContentTemplate>
                                                                                                </asp:UpdatePanel>
                                                                                            </asp:Panel>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Hearing" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" HeaderStyle-Width="10%" Visible="false">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblRefID" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                                    ToolTip='<%# Eval("RefID") %>' Text='<%# Eval("RefID") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Hearing" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblHearingRefNo" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                                    ToolTip='<%# Eval("HearingRefNo") %>' Text='<%# Eval("HearingRefNo") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="20%" HeaderStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                <asp:Label ID="lblResDesc" runat="server" Text='<%# Eval("Description") %>'
                                                                                                    ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                                <%--data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'--%>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Next Hearing" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                <asp:Label ID="lblReminderDate" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                                    ToolTip='<%# Eval("ReminderDate") != null ? Convert.ToDateTime(Eval("ReminderDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                    Text='<%# Eval("ReminderDate") != null ? Convert.ToDateTime(Eval("ReminderDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                                                <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowCaseResponseDocCount((long)Eval("CaseInstanceID"),(long)Eval("ResponseID")) %>'>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                                        <ItemTemplate>
                                                                                            <asp:UpdatePanel runat="server" ID="upResDocDelete" UpdateMode="Always">
                                                                                                <ContentTemplate>
                                                                                                    <asp:LinkButton
                                                                                                        CommandArgument='<%# Eval("ResponseID")+","+ Eval("CaseInstanceID")%>' CommandName="DownloadResponseDoc"
                                                                                                        ID="lnkBtnDownLoadResponseDoc" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                    </asp:LinkButton>
                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownLoadResponseDoc" />                                                                                                   
                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                </Columns>
                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                <HeaderStyle CssClass="clsheadergrid" />

                                                                                <EmptyDataTemplate>
                                                                                    No Records Found.
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--Hearing Detail Panel End-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="OrderView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="OrderTabAccordion">

                                    <div class="row Dashboard-white-widget">
                                        <!--Order Panel Start-->
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">

                                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Case Order Detail">
                                                    <div class="panel-heading">
                                                        <%--data-toggle="collapse" data-parent="#OrderTabAccordion" href="#collapseDivOrderLogs"--%>
                                                        <a>
                                                            <h2></h2>
                                                        </a>
                                                        <div class="panel-actions">
                                                            <%-- <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#OrderTabAccordion" href="#collapseDivOrderLogs">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>--%>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="collapseDivOrderLogs" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="container">                                                           
                                                            <div class="row">
                                                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                        ValidationGroup="CasePopUpOrderValidationGroup" />
                                                                    <asp:CustomValidator ID="cvCaseOrderPopup" runat="server" EnableClientScript="False"
                                                                        ValidationGroup="CasePopUpOrderValidationGroup" Display="None" />
                                                                </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <asp:UpdatePanel ID="upCaseOrder" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView runat="server" ID="grdCaseOrder" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                                OnRowCommand="grdCaseOrder_RowCommand" OnRowDataBound="grdCaseOrder_RowDataBound" DataKeyNames="ID"
                                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdCaseOrder_OnPageIndexChanging">
                                                                                <Columns>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Order Type" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                                <asp:Label ID="lblOrderType" runat="server" Text='<%# ShowOrderType((int)Eval("OrderTypeID")) %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Order" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                <asp:Label ID="lblOrderTitle" runat="server" Text='<%# Eval("OrderTitle") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("OrderTitle") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Description" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                <asp:Label ID="lblOrderDesc" runat="server" Text='<%# Eval("OrderDesc") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("OrderDesc") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Order Date" ItemStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("OrderDate") != null ? Convert.ToDateTime(Eval("OrderDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("OrderDate") != null ? Convert.ToDateTime(Eval("OrderDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowOrderDocCount((long)Eval("CaseInstanceID"),(long)Eval("ID")) %>'>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                                <ContentTemplate>
                                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                            AutoPostBack="true" CommandName="DownloadCaseOrder"
                                                                                                            ID="lnkBtnDownloadOrderDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Document" />
                                                                                                        </asp:LinkButton>

                                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                        AutoPostBack="true" CommandName="ViewCaseOrder"
                                                                                                        ID="lnkBtnViewDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                    </asp:LinkButton>
                                                                                                    </div>

                                                                                                     
                                                                                                </ContentTemplate>                                                                                                
                                                                                            </asp:UpdatePanel>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                </Columns>
                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                <EmptyDataTemplate>
                                                                                    No Records Found.
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Order Panel End-->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="StatusPaymentView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="FourthTabAccordion">

                                    <div class="row Dashboard-white-widget">
                                        <!--Status Log Panel Start-->
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">
                                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View/Edit Case Status">
                                                    <div class="panel-heading" data-toggle="collapse" data-parent="#FourthTabAccordion" href="#collapseDivStatusLogs">
                                                        <a>
                                                            <h2>Status</h2>
                                                        </a>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#FourthTabAccordion" href="#collapseDivStatusLogs">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="collapseDivStatusLogs" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div style="margin-bottom: 7px">
                                                            <asp:ValidationSummary ID="ValidationSummary3" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                ValidationGroup="CasePopUpStatusValidationGroup" />
                                                            <asp:CustomValidator ID="cvCaseStatus" runat="server" EnableClientScript="False"
                                                                ValidationGroup="CasePopUpStatusValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Case Stage</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCaseStage" DataPlaceHolder="Select Status" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="40%">
                                                                </asp:DropDownListChosen>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Case Status</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCaseStatus" DataPlaceHolder="Select Status" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="40%" onchange="ddlStatusChange()">
                                                                    <asp:ListItem Text="Select Status" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="In Progress" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Close" Value="3"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6" id="divClosureDetail">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Close Date</label>
                                                                <asp:TextBox runat="server" ID="tbxCaseCloseDate" autocomplete="off" Style="width: 40%; background-color: #fff; cursor: pointer;"
                                                                    CssClass="form-control" />
                                                            </div>

                                                            <div class="form-group col-md-6"></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12" id="divClosureRemark">
                                                                <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                <asp:TextBox ID="tbxCloseRemark" runat="server" CssClass="form-control" Width="85%" TextMode="MultiLine"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Status Log Panel End-->
                                    </div>

                                    <div class="row Dashboard-white-widget">
                                        <!--Payment Log Panel Start-->
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">

                                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Payment Detail">
                                                    <div class="panel-heading" data-toggle="collapse" data-parent="#FourthTabAccordion" href="#collapseDivPaymentLog">
                                                        <a>
                                                            <h2>Payment Log</h2>
                                                        </a>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#FourthTabAccordion" href="#collapseDivPaymentLog">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="collapseDivPaymentLog" class="panel-collapse collapse in">
                                                    <div class="panel-body">

                                                        <div style="margin-bottom: 7px">
                                                            <asp:ValidationSummary ID="ValidationSummary4" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                ValidationGroup="CasePopUpPaymentLogValidationGroup" />
                                                            <asp:CustomValidator ID="cvCasePayment" runat="server" EnableClientScript="true"
                                                                ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="form-group col-md-12">

                                                            <asp:UpdatePanel ID="upCasePayment" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:GridView runat="server" ID="grdCasePayment" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                        OnRowCommand="grdCasePayment_RowCommand" OnRowDataBound="grdCasePayment_RowDataBound"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdCasePayment_PageIndexChanging">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Payment Date" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("PaymentDate") != null ? Convert.ToDateTime(Eval("PaymentDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    <asp:TextBox ID="tbxPaymentDate" runat="server" class="form-control"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="rfvPaymentDate" ErrorMessage="Provide Payment Date." runat="server"
                                                                                        ControlToValidate="tbxPaymentDate" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Payment Type" ItemStyle-Width="20%" FooterStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblPaymentType" runat="server" Text='<%# Eval("PaymentType") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    <asp:DropDownListChosen runat="server" ID="ddlPaymentType" DataPlaceHolder="Payment Type" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                        CssClass="form-control" Width="100%">
                                                                                    </asp:DropDownListChosen>
                                                                                    <asp:RequiredFieldValidator ID="rfvPaymentType" ErrorMessage="Select Payment Type."
                                                                                        ControlToValidate="ddlPaymentType" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Amount" ItemStyle-Width="20%" FooterStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    <asp:TextBox ID="tbxAmount" runat="server" class="form-control"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="rfvAmount" ErrorMessage="Provide Payment Amount."
                                                                                        ControlToValidate="tbxAmount" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="30%" FooterStyle-Width="30%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblPaymentRemark" runat="server" Text='<%# Eval("Remark") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    <asp:TextBox ID="tbxPaymentRemark" runat="server" class="form-control"></asp:TextBox>
                                                                                     <asp:RequiredFieldValidator ID="rfvPaymentRemark" ErrorMessage="Provide Payment Remark."
                                                                                    ControlToValidate="tbxPaymentRemark" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />                                                                      
                                                                        <EmptyDataTemplate>
                                                                            No Records Found.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Payment Log Panel End-->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>

        </div>

        <div>
                <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                    OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("DocTypeInstanceID") + ","+ Eval("Version") + ","+ Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString().Substring(0,10) %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="CaseDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    </form>
</body>
</html>
