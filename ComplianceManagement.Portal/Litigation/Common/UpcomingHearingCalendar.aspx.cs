﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class UpcomingHearingCalendar : System.Web.UI.Page
    {
        public static string CalendarDate;
        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["type"] != "undefined")
                {
                    try
                    {
                        string type = Convert.ToString(Request.QueryString["type"]);
                        GetCalender(type);
                        // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "fcal", "fcal('" + CalendarTodayOrNextDate.ToString("yyyy-MM-dd") + "');", true);
                    }
                    catch (Exception ex) { }
                }
            }
        }

        public void GetCalender(string type)
        {
            DataTable dt = new DataTable();
            DateTime dt1 = DateTime.Now.Date;
            string month = dt1.Month.ToString();
            var TodayDatetime = DateTime.Now;
            var TodayDate = TodayDatetime.Date;
            if (Convert.ToInt32(month) < 10)
            {
                month = "0" + month;
            }
            string year = dt1.Year.ToString();
            CalendarDate = year + "-" + month;
            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userid = AuthenticationHelper.UserID;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                          where row.CustomerID == customerid
                                          && row.ReminderDate != null
                                          && row.TxnStatusID != 3
                                          select row).ToList();
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerid));
                if (isexist != null)
                {
                    viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                    if (AuthenticationHelper.Role != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, -1);
                        if (caseList.Count > 0)
                        {
                            viewHearingDetails = viewHearingDetails.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        }
                    }                   
                }
                else
                {
                    if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                        viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                    }
                }
                if (viewHearingDetails.Count > 0)
                {
                    viewHearingDetails = (from g in viewHearingDetails
                                          group g by new
                                          {
                                              g.NoticeCaseInstanceID,
                                              g.Title,
                                              g.CaseRefNo,
                                              // g.CourtID,
                                              // g.CourtName,
                                              // g.CaseType,
                                              g.CustomerID,
                                              // g.CustomerBranchID,
                                              // g.BranchName,
                                              // g.DepartmentID,
                                              // g.CaseRiskID,
                                              //  g.PartyID,
                                              //  g.PartyName,
                                              g.ResponseDate,
                                              g.TxnStatusID,
                                              g.ReminderDate,
                                              //g.lstAssignedTo,
                                             // g.UserID,
                                              g.RoleID,
                                              g.OwnerID,
                                              g.ResponseID
                                          } into GCS
                                          select new View_LitigationCaseResponse()
                                          {
                                              NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                              Title = GCS.Key.Title,
                                              CaseRefNo = GCS.Key.CaseRefNo,
                                              //CourtID = GCS.Key.CourtID,
                                              //CourtName = GCS.Key.CourtName,
                                              //CaseType = GCS.Key.CaseType,
                                              CustomerID = GCS.Key.CustomerID,
                                              //CustomerBranchID = GCS.Key.CustomerBranchID,
                                              //BranchName = GCS.Key.BranchName,
                                              //DepartmentID = GCS.Key.DepartmentID,
                                              //CaseRiskID = GCS.Key.CaseRiskID,
                                              //PartyID = GCS.Key.PartyID,
                                              //PartyName = GCS.Key.PartyName,
                                              ResponseDate = GCS.Key.ResponseDate,
                                              TxnStatusID = GCS.Key.TxnStatusID,
                                              ReminderDate = GCS.Key.ReminderDate,
                                              //lstAssignedTo = GCS.Key.lstAssignedTo,
                                              //UserID = GCS.Key.UserID,
                                              RoleID = GCS.Key.RoleID,
                                              OwnerID = GCS.Key.OwnerID,
                                              ResponseID = GCS.Key.ResponseID
                                          }).ToList();
                }

                dt = viewHearingDetails.ToDataTable();
            }
            var list = (from r in dt.AsEnumerable()
                        select r["ReminderDate"]).Distinct().ToList();

            CalenderDateString = "";
            for (int i = 0; i < list.Count; i++)
            {
                Compliancecalendar _Event = new Compliancecalendar();
                string clor = "";
                int UpcomingCount = 0;
                DateTime date = Convert.ToDateTime(list[i]);

                //if (date >= TodayDate)
                {
                    var Assigned = (from dts in dt.AsEnumerable()
                                    where dts.Field<DateTime>("ReminderDate") == date
                                    select dts.Field<DateTime>("ReminderDate")).ToList();

                    if (Assigned.Count() > 0)
                    {
                        UpcomingCount = Assigned.Count;
                        clor = "Upcoming";
                        DateTime UpcomingDate = Convert.ToDateTime(list[i]);
                        var Upcomingstring = UpcomingDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                        CalenderDateString += "\"" + Upcomingstring.ToString() + "\": { \"number\" :" + UpcomingCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Upcomingstring.ToString() + "');\"},";
                    }
                }
                //else
                //{
                //    var Assigned = (from dts in dt.AsEnumerable()
                //                    where dts.Field<DateTime>("ResponseDate") != date
                //                    && dts.Field<DateTime>("ReminderDate") == date
                //                    select dts.Field<DateTime>("ReminderDate")).ToList();
                //    if (Assigned.Count() > 0)
                //    {
                //        UpcomingCount = Assigned.Count;
                //        clor = "Pending";
                //        DateTime UpcomingDate = Convert.ToDateTime(list[i]);
                //        var Upcomingstring = UpcomingDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                //        CalenderDateString += "\"" + Upcomingstring.ToString() + "\": { \"number\" :" + UpcomingCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Upcomingstring.ToString() + "');\"},";
                //    }
                //}
            }
            CalenderDateString = CalenderDateString.Trim(',');
        }

    }
}