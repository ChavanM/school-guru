﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LitigationComplianceDocument.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.LitigationComplianceDocument" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Litigation Compliance Document Detail</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="../../NewCSS/litigation_custom_style.css" rel="stylesheet" />

    <script type="text/javascript">

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls(); CheckBoxChecks();

        });

        function CheckBoxChecks() {
            
            
            $("[id*=chkAllDocument]").click(function () {
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function () {
                    if (chkHeader.is(":checked")) {
                        $(this).attr("checked", "checked");
                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            });

            $("[id*=chkDocument]").click(function () {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkAllDocument]", grid);
                if (!$(this).is(":checked")) {
                    chkHeader.removeAttr("checked");
                } else {
                    if ($("[id*=chkDocument]", grid).length == $("[id*=chkDocument]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
            });
        }

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtFromDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                    });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                    });
            });

        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);

        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

    </script>

    <style type="text/css">
        body {
            background: #fff;
        }
    </style>     
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="upLitigationDocument" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <%-- <div class="row Dashboard-white-widget">--%>
                <div class="dashboard">
                    <div class="col-md-12 plr0">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="ACTPopupValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ACTPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>

                    <%--Filters--%>
                    <div class="col-md-12 plr0">
                        <div class="col-md-3">
                            <div class="col-md-12 plr0">
                                <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Act</label>
                            </div>
                            <div class="col-md-12 plr0">
                                <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlAct" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                    class="form-control" Width="100%" DataPlaceHolder="Select Act">
                                </asp:DropDownListChosen>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="col-md-12 plr0">
                                <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Compliance</label>
                            </div>
                            <div class="col-md-12 plr0">
                                <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlCompliance" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                    class="form-control" Width="100%" DataPlaceHolder="Select Compliance">
                                </asp:DropDownListChosen>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="col-md-12 plr0">
                                <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">From Date</label>
                            </div>
                            <div class="col-md-12 plr0 input-group date">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                </span>
                                <asp:TextBox runat="server" placeholder="DD-MM-YYYY" class="form-control" ID="txtFromDate" autocomplete="off" />
                                <%--<asp:TextBox ID="" autocomplete="off" runat="server" CssClass="form-control"
                                MaxLength="100" Style="width: 50%; display: initial; background-color: #fff; cursor: pointer;"></asp:TextBox>
                                </div>--%>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="col-md-12 plr0">
                                <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">To Date</label>
                            </div>
                            <div class="col-md-12 plr0 input-group date">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                </span>
                                <asp:TextBox ID="txtEndDate" placeholder="DD-MM-YYYY" autocomplete="off" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-2 text-right">
                            <div class="col-md-12 plr0">
                                <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">&nbsp;</label>
                            </div>
                            <div class="col-md-12 plr0 input-group date">
                                <asp:LinkButton Text="Apply" CssClass="btn btn-primary"
                                    runat="server" ID="lnkBtnApplyFilter" OnClick="lnkBtnApplyFilter_Click" />
                            </div>
                        </div>
                    </div>
                    <%--Filters END--%>

                    <div class="row col-md-12" style="height: 350px; overflow-y: auto;">

                        <asp:GridView runat="server" ID="grdComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                            OnRowCommand="grdComplianceDocument_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdComplianceDocument_RowDataBound"
                            AutoPostBack="true" ShowHeaderWhenEmpty="true">
                            <%--OnPageIndexChanging="grdComplianceDocument_PageIndexChanging"--%>
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="2%">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkAllDocument" runat="server"  onclick="javascript:checkAll(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkDocument" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                        <asp:Label ID="lblScheduledOnID" runat="server" Text='<%# Eval("ScheduledOnID")%>' Visible="false"></asp:Label>
                                        <%--<asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("FilePath") %>' Visible="false"></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Act" ItemStyle-Width="40%">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                            <asp:Label ID="lblAct" runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'
                                                data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Compliance" ItemStyle-Width="40%">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                            <asp:Label ID="lblShortDesc" runat="server"
                                                data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                            <%# Eval("ForMonth")%>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="lblDownLoadfile" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download"
                                                    CommandArgument='<%# Eval("ScheduledOnID")%>' data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Download Compliance Related Document(s) for this Period"></asp:ImageButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                <%--<asp:PostBackTrigger ControlID="lblViewfile" />--%>
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <PagerStyle HorizontalAlign="Right" />
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Record Found
                            </EmptyDataTemplate>
                        </asp:GridView>

                    </div>

                    <div id="divLinkCaseSaveCount" class="row col-md-12 plr0" style="display: none;">
                        <div class="col-md-2 text-left">
                            <asp:Label runat="server" ID="lblTotalDocumentSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                        </div>
                        <div class="col-md-2 text-left">
                            <asp:DropDownList runat="server" ID="ddlDownloadLink" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                CssClass="form-control" Width="100%">
                                <asp:ListItem Text="Add to Case" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Download" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-8 text-left">
                            <asp:LinkButton Text="Submit" CssClass="btn btn-primary" runat="server" ID="lnkSaveDocument" OnClick="lnkSaveDocument_Click" />
                            <asp:LinkButton Text="Close" CssClass="btn btn-primary" runat="server" ID="lnkDownload" data-dismiss="modal"/>
                        </div>
                    </div>
                </div>
                <
                <%--  </div>--%>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkSaveDocument" />
            </Triggers>
        </asp:UpdatePanel>
    </form>

     <script type="text/javascript">

        function checkAll(chkHeader) {
            
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdComplianceDocument.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSaveLinkCase = document.getElementById("<%=lnkSaveDocument.ClientID %>");
                var lblTotalCaseSelected = document.getElementById("<%=lblTotalDocumentSelected.ClientID %>");
                if ((btnSaveLinkCase != null || btnSaveLinkCase != undefined) && (lblTotalCaseSelected != null || lblTotalCaseSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalCaseSelected.innerHTML = selectedRowCount + " Document(s) Selected";
                        divLinkCaseSaveCount.style.display = "block";
                    }
                    else {
                        lblTotalCaseSelected.innerHTML = "";;
                        divLinkCaseSaveCount.style.display = "none";
                    }
                }
            }
        }


        function checkUncheckRow(clickedCheckBoxObj) {
          
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdComplianceDocument.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;                        
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSaveLinkCase = document.getElementById("<%=lnkSaveDocument.ClientID %>");
             var lblTotalCaseSelected = document.getElementById("<%=lblTotalDocumentSelected.ClientID %>");
            if ((btnSaveLinkCase != null || btnSaveLinkCase != undefined) && (lblTotalCaseSelected != null || lblTotalCaseSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalCaseSelected.innerHTML = selectedRowCount + " Document(s) Selected";
                    divLinkCaseSaveCount.style.display = "block";
                }
                else {
                    lblTotalCaseSelected.innerHTML = "";;
                    divLinkCaseSaveCount.style.display = "none";
                }
            }

            //var ck = header;
            //var selectedRowCount = 0;

            //var headerchk = document.getElementById(header);
            //var rowcount = grid.rows.length;
            ////By using this for loop we will count how many checkboxes has checked
            //for (i = 1; i < grid.rows.length; i++) {
            //    var inputs = grid.rows[i].getElementsByTagName('input');
            //    if (grid.rows[i].cells[1] != undefined) {
            //        if (inputs[0].checked) {
            //            selectedRowCount++;
            //        }
            //    }
            //}
            ////Condition to check all the checkboxes selected or not
            //if (selectedRowCount == rowcount - 1) {
            //    headerchk.checked = true;
            //}
            //else {
            //    headerchk.checked = false;
            //}            
        }

    </script>
</body>
</html>
