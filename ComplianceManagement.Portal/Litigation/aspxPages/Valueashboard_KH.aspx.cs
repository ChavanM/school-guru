﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class Valueashboard_KH : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //DIVTLINTAXCor
            //DivPBINTAXCor
            //DivPDINTAXCor
            //DivNOACINTAXCor
            if (!IsPostBack)
            {
                Count();
            }
        }
        protected void ExportToExcel(object sender, EventArgs e)
        {
            //Response.Clear();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition", "attachment;filename=HTML.xls");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.Output.Write(Request.Form[hfGridHtml.UniqueID]);
            //Response.Flush();
            //Response.End();
            Response.ContentType = "application/x-msexcel";
            Response.AddHeader("Content-Disposition", "attachment;filename = ExcelFile.xls");
            Response.ContentEncoding = Encoding.UTF8;
            StringWriter tw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            tbl.RenderControl(hw);
            Response.Write(tw.ToString());
            Response.End();
        }

        protected void ExportToExcel1(object sender, EventArgs e)
        {
            #region excel report
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("ValueDashboard");

                    exWorkSheet3.Cells["A1"].Value = "Division";
                    exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet3.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A1"].Style.Font.Name = "Calibri";

                    exWorkSheet3.Cells["A1"].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));
                    exWorkSheet3.Cells["A1:A2"].Merge = true;
                    exWorkSheet3.Cells["A1"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#168598"));//#1F5663

                    exWorkSheet3.Cells["B1"].Value = "Figures in Mn INR";
                    exWorkSheet3.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet3.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B1"].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));
                    exWorkSheet3.Cells["B1:E1"].Merge = true;
                    exWorkSheet3.Cells["B1"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet3.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#19BFDB"));//#1F5663


                    exWorkSheet3.Cells["B2"].Value = "Recovery Amount";
                    exWorkSheet3.Cells["B2"].Style.Font.Bold = true;
                    exWorkSheet3.Cells["B2"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B2"].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));
                    exWorkSheet3.Cells["B2"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet3.Cells["B2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#168598"));//#1F5663

                    exWorkSheet3.Cells["C2"].Value = "	Claim Amount";
                    exWorkSheet3.Cells["C2"].Style.Font.Bold = true;
                    exWorkSheet3.Cells["C2"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C2"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C2"].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));
                    exWorkSheet3.Cells["C2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet3.Cells["C2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#168598"));//#1F5663

                    exWorkSheet3.Cells["D2"].Value = "Provisional Amount";
                    exWorkSheet3.Cells["D2"].Style.Font.Bold = true;
                    exWorkSheet3.Cells["D2"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D2"].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));
                    exWorkSheet3.Cells["D2"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet3.Cells["D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#168598"));//#1F5663

                    exWorkSheet3.Cells["E2"].Value = "Protest Money";
                    exWorkSheet3.Cells["E2"].Style.Font.Bold = true;
                    exWorkSheet3.Cells["E2"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E2"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E2"].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));
                    exWorkSheet3.Cells["E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet3.Cells["E2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#168598"));//#1F5663

                   

                    exWorkSheet3.Cells["F1"].Value = "No of active cases";
                    exWorkSheet3.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet3.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F1"].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));
                    exWorkSheet3.Cells["F1:F2"].Merge = true;
                    exWorkSheet3.Cells["F1"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet3.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#168598"));//#1F5663




                    exWorkSheet3.Cells["A3"].Value = "Direct Tax(DT)";
                    exWorkSheet3.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet3.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A3"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A3:F3"].Merge = true;
                    exWorkSheet3.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#EBF7F9"));//#1F5663

                    exWorkSheet3.Cells["A4"].Value = "Income Tax - Corporate Office, Mumbai";

                    exWorkSheet3.Cells["A4"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A4"].AutoFitColumns(30);
                    exWorkSheet3.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["B4"].Value =Convert.ToDecimal(DivREINTAXCor.InnerText);

                    exWorkSheet3.Cells["B4"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B4"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["C4"].Value = Convert.ToDecimal(DIVTLINTAXCor.InnerText);
                    exWorkSheet3.Cells["C4"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C4"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["C4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["D4"].Value = Convert.ToDecimal(DivPBINTAXCor.InnerText);

                    exWorkSheet3.Cells["D4"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D4"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["D4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["E4"].Value = Convert.ToDecimal(DivPDINTAXCor.InnerText);
                    exWorkSheet3.Cells["E4"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E4"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    


                    exWorkSheet3.Cells["F4"].Value = Convert.ToDecimal(DivNOACINTAXCor.InnerText);
                    exWorkSheet3.Cells["F4"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F4"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    //
                    exWorkSheet3.Cells["A5"].Value = "Total DT";
                    exWorkSheet3.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A5"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BDD7EE"));//#1F5663

                    exWorkSheet3.Cells["B5"].Value = Convert.ToDecimal(DIVTDT_RE.InnerText);

                    exWorkSheet3.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B5"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BDD7EE"));//#1F5663

                    exWorkSheet3.Cells["C5"].Value = Convert.ToDecimal(DIVTDT_TL.InnerText);
                    exWorkSheet3.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C5"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BDD7EE"));//#1F5663


                    exWorkSheet3.Cells["D5"].Value = Convert.ToDecimal(DIVTDT_PB.InnerText);

                    exWorkSheet3.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D5"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BDD7EE"));//#1F5663

                    exWorkSheet3.Cells["E5"].Value = Convert.ToDecimal(DIVTDT_PD.InnerText);
                    exWorkSheet3.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E5"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BDD7EE"));//#1F5663


                    

                    exWorkSheet3.Cells["F5"].Value = Convert.ToDecimal(DIVTDT_NOAC.InnerText);
                    exWorkSheet3.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F5"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BDD7EE"));//#1F5663
                    //
                    
                    ////
                    exWorkSheet3.Cells["A6"].Value = "HR Cases";
                    exWorkSheet3.Cells["A6"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["A6"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A6"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["B6"].Value = Convert.ToDecimal(DIVHR_RE.InnerText);
                    exWorkSheet3.Cells["B6"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["B6"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B6"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["B6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["C6"].Value = Convert.ToDecimal(DIVHR_TL.InnerText);
                    exWorkSheet3.Cells["C6"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["C6"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C6"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["C6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["D6"].Value = Convert.ToDecimal(DIVHR_PB.InnerText);
                    exWorkSheet3.Cells["D6"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["D6"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D6"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["D6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["E6"].Value = Convert.ToDecimal(DIVHR_PD.InnerText);
                    exWorkSheet3.Cells["E6"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["E6"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E6"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["E6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663





                    exWorkSheet3.Cells["F6"].Value = Convert.ToDecimal(DIVHR_NOAC.InnerText);
                    exWorkSheet3.Cells["F6"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["F6"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F6"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["F6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    /////


                    ////
                    exWorkSheet3.Cells["A7"].Value = "Civil Cases";
                    exWorkSheet3.Cells["A7"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["A7"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A7"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A7"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["B7"].Value = Convert.ToDecimal(DIV_Civil_RE.InnerText);
                    exWorkSheet3.Cells["B7"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["B7"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B7"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["B7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B7"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["C7"].Value = Convert.ToDecimal(DIV_Civil_TL.InnerText);
                    exWorkSheet3.Cells["C7"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["C7"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C7"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["C7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C7"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["D7"].Value = Convert.ToDecimal(DIV_Civil_PB.InnerText);
                    exWorkSheet3.Cells["D7"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["D7"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D7"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["D7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D7"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["E7"].Value = Convert.ToDecimal(DIV_Civil_PD.InnerText);
                    exWorkSheet3.Cells["E7"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["E7"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E7"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["E7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E7"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    

                    exWorkSheet3.Cells["F7"].Value = Convert.ToDecimal(DIV_Civil_NOAC.InnerText);
                    exWorkSheet3.Cells["F7"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["F7"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F7"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["F7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F7"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    /////


                    ////
                    exWorkSheet3.Cells["A8"].Value = "Criminal Cases";
                    exWorkSheet3.Cells["A8"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["A8"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A8"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A8"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["B8"].Value = Convert.ToDecimal(DIV_Criminal_RE.InnerText);
                    exWorkSheet3.Cells["B8"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["B8"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B8"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["B8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B8"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["C8"].Value = Convert.ToDecimal(DIV_Criminal_TL.InnerText);
                    exWorkSheet3.Cells["C8"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["C8"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C8"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["C8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C8"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["D8"].Value = Convert.ToDecimal(DIV_Criminal_PB.InnerText);
                    exWorkSheet3.Cells["D8"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["D8"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D8"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["D8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D8"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["E8"].Value = Convert.ToDecimal(DIV_Criminal_PD.InnerText);
                    exWorkSheet3.Cells["E8"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["E8"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E8"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E8"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                  


                    exWorkSheet3.Cells["F8"].Value = Convert.ToDecimal(DIV_Criminal_NOAC.InnerText);
                    exWorkSheet3.Cells["F8"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["F8"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F8"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["F8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F8"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    /////


                    ////
                    exWorkSheet3.Cells["A9"].Value = "Other Legal Cases";
                    exWorkSheet3.Cells["A9"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["A9"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A9"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A9"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["B9"].Value = Convert.ToDecimal(DIV_Other_RE.InnerText);
                    exWorkSheet3.Cells["B9"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["B9"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B9"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["B9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B9"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["C9"].Value = Convert.ToDecimal(DIV_Other_TL.InnerText);
                    exWorkSheet3.Cells["C9"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["C9"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C9"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["C9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C9"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    exWorkSheet3.Cells["D9"].Value = Convert.ToDecimal(DIV_Other_PB.InnerText);
                    exWorkSheet3.Cells["D9"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["D9"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D9"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D9"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663


                    exWorkSheet3.Cells["E9"].Value = Convert.ToDecimal(DIV_Other_PD.InnerText);
                    exWorkSheet3.Cells["E9"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["E9"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E9"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["E9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E9"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    

                    exWorkSheet3.Cells["F9"].Value = Convert.ToDecimal(DIV_Other_NOAC.InnerText);
                    exWorkSheet3.Cells["F9"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["F9"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F9"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["F9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F9"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#fff"));//#1F5663

                    /////

                    ////
                    exWorkSheet3.Cells["A10"].Value = "Total (HR + Civil + Criminal + Other Legal cases)";
                    exWorkSheet3.Cells["A10"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["A10"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A10"].Style.ShrinkToFit = true;
                    exWorkSheet3.Cells["A10"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A10"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8E4BC"));//#1F5663


                    exWorkSheet3.Cells["B10"].Value = Convert.ToDecimal(DIVTotal_RE.InnerText);
                    exWorkSheet3.Cells["B10"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["B10"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B10"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["B10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B10"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8E4BC"));//#1F5663

                    exWorkSheet3.Cells["C10"].Value = Convert.ToDecimal(DIVTotal_TL.InnerText);
                    exWorkSheet3.Cells["C10"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["C10"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C10"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["C10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C10"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8E4BC"));//#1F5663

                    exWorkSheet3.Cells["D10"].Value = Convert.ToDecimal(DIVTotal_PB.InnerText);
                    exWorkSheet3.Cells["D10"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["D10"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D10"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["D10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D10"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8E4BC"));//#1F5663


                    exWorkSheet3.Cells["E10"].Value = Convert.ToDecimal(DIVTotal_PD.InnerText);
                    exWorkSheet3.Cells["E10"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["E10"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E10"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["E10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E10"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8E4BC"));//#1F5663
                    


                    exWorkSheet3.Cells["F10"].Value = Convert.ToDecimal(DIVTotal_NOAC.InnerText);
                    exWorkSheet3.Cells["F10"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["F10"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F10"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["F10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F10"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8E4BC"));//#1F5663

                    /////


                    ////
                    exWorkSheet3.Cells["A11"].Value = "	Claim Amount";
                    exWorkSheet3.Cells["A11"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["A11"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["A11"].AutoFitColumns(45);
                    exWorkSheet3.Cells["A11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet3.Cells["A11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["A11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["A11"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFD700"));//#1F5663


                    exWorkSheet3.Cells["B11"].Value = Convert.ToDecimal(DIVAllTotal_RE.InnerText);
                    exWorkSheet3.Cells["B11"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["B11"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["B11"].AutoFitColumns(30);
                    exWorkSheet3.Cells["B11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["B11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["B11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["B11"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFD700"));//#1F5663

                    exWorkSheet3.Cells["C11"].Value = Convert.ToDecimal(DIVAllTotal_TL.InnerText);
                    exWorkSheet3.Cells["C11"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["C11"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["C11"].AutoFitColumns(30);
                    exWorkSheet3.Cells["C11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["C11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["C11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["C11"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFD700"));//#1F5663

                    exWorkSheet3.Cells["D11"].Value = Convert.ToDecimal(DIVAllTotal_PB.InnerText);
                    exWorkSheet3.Cells["D11"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["D11"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["D11"].AutoFitColumns(30);
                    exWorkSheet3.Cells["D11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["D11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["D11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["D11"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFD700"));//#1F5663


                    exWorkSheet3.Cells["E11"].Value = Convert.ToDecimal(DIVAllTotal_PD.InnerText);
                    exWorkSheet3.Cells["E11"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["E11"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["E11"].AutoFitColumns(30);
                    exWorkSheet3.Cells["E11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["E11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["E11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["E11"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFD700"));//#1F5663

                    


                    exWorkSheet3.Cells["F11"].Value = Convert.ToDecimal(DIVAllTotal_NOAC.InnerText);
                    exWorkSheet3.Cells["F11"].Style.Font.Bold = false;
                    exWorkSheet3.Cells["F11"].Style.Font.Size = 12;
                    exWorkSheet3.Cells["F11"].AutoFitColumns(30);
                    exWorkSheet3.Cells["F11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    exWorkSheet3.Cells["F11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet3.Cells["F11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet3.Cells["F11"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFD700"));//#1F5663

                    /////

                    #endregion

                    exWorkSheet3.Row(1).Height = 25;
                    exWorkSheet3.Row(2).Height = 25;
                    exWorkSheet3.Row(3).Height = 25;
                    exWorkSheet3.Row(4).Height = 25;
                    exWorkSheet3.Row(5).Height = 25;
                    exWorkSheet3.Row(6).Height = 25;
                    exWorkSheet3.Row(7).Height = 25;
                    exWorkSheet3.Row(8).Height = 25;
                    exWorkSheet3.Row(9).Height = 25;
                    exWorkSheet3.Row(10).Height = 25;
                    exWorkSheet3.Row(11).Height = 25;
                    exWorkSheet3.Row(12).Height = 25;
                    exWorkSheet3.Row(13).Height = 25;
                    exWorkSheet3.Row(14).Height = 25;
                    exWorkSheet3.Row(15).Height = 25;
                    exWorkSheet3.Row(16).Height = 25;
                    exWorkSheet3.Row(17).Height = 35;
                    exWorkSheet3.Row(18).Height = 25;

                    //  int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                    using (ExcelRange col = exWorkSheet3.Cells[1, 1, 19, 6])
                    {
                        col.Style.WrapText = true;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.ColorTranslator.FromHtml("#ceced2"));
                        col.Style.Border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#ceced2"));
                        col.Style.Border.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#ceced2"));
                        col.Style.Border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#ceced2"));
                        col.Style.Border.Bottom.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#ceced2"));
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                    }


                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    string locatioName = "ValueDashboard-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename=" + locatioName);//locatioName
                                                                                                                     //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }


        }

        public void Count()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    decimal? All_TotalLiability = 0;
                    decimal? All_Provisioninbooks = 0;
                    decimal? All_PreDeposits = 0;
                    decimal? All_Recovery = 0;
                    decimal? All_Noofactivecases = 0;

                    var legrandlist = (from c in entities.mst_Legrand
                                       where c.Customerid == AuthenticationHelper.CustomerID
                                       select c).ToList();

                    #region Direct TAX
                    decimal? TotalLiability_DT_INDT = 0;
                    decimal? Provisioninbooks_DT_INDT = 0;
                    decimal? PreDeposits_DT_INDT = 0;
                    decimal? Noofactivecases_DT_INDT = 0;
                    decimal? Recovery_DT_INDT = 0;
                    var Dirdetails = legrandlist.Where(a => a.IsCorporateOffice == true).FirstOrDefault();
                    if (Dirdetails != null)
                    {
                        var detilsdata = (from c in entities.Sp_TotalLiabilityDirTax(AuthenticationHelper.CustomerID, Dirdetails.BranchId, AuthenticationHelper.Role, AuthenticationHelper.UserID)
                                          select c).FirstOrDefault();
                        if (detilsdata != null)
                        {
                            DIVTLINTAXCor.InnerHtml = $"{decimal.Round((decimal)detilsdata.TotalLiability / 1000000, 2, MidpointRounding.AwayFromZero):n0}";
                            DivPBINTAXCor.InnerHtml = $"{decimal.Round((decimal)detilsdata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DivPDINTAXCor.InnerHtml = $"{decimal.Round((decimal)detilsdata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DivREINTAXCor.InnerHtml = $"{decimal.Round((decimal)detilsdata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DivNOACINTAXCor.InnerHtml = Convert.ToString(detilsdata.No_Of_Active_Cases);

                            DIVTDT_TL.InnerHtml = $"{decimal.Round((decimal)detilsdata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIVTDT_PB.InnerHtml = $"{decimal.Round((decimal)detilsdata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIVTDT_PD.InnerHtml = $"{decimal.Round((decimal)detilsdata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIVTDT_RE.InnerHtml = $"{decimal.Round((decimal)detilsdata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIVTDT_NOAC.InnerHtml = Convert.ToString(detilsdata.No_Of_Active_Cases);


                            TotalLiability_DT_INDT += detilsdata.TotalLiability;
                            Provisioninbooks_DT_INDT += (decimal)detilsdata.ProvisioninBooks;
                            PreDeposits_DT_INDT += detilsdata.Provisionalamt;
                            Recovery_DT_INDT += detilsdata.RecoveryAmount;
                            Noofactivecases_DT_INDT += detilsdata.No_Of_Active_Cases;
                        }
                    }
                    else
                    {
                        DIVTLINTAXCor.InnerHtml = "0";
                        DivPBINTAXCor.InnerHtml = "0";
                        DivPDINTAXCor.InnerHtml = "0";
                        DivREINTAXCor.InnerHtml = "0";
                        DivNOACINTAXCor.InnerHtml = "0";

                        DIVTDT_TL.InnerHtml = "0";
                        DIVTDT_PB.InnerHtml = "0";
                        DIVTDT_PD.InnerHtml = "0";
                        DIVTDT_RE.InnerHtml = "0";
                        DIVTDT_NOAC.InnerHtml = "0";
                    }
                    #endregion

                    //#region In Direct Tax                
                    //var INDIRdetails = legrandlist.Where(a => a.IsCorporateOffice == false).ToList();
                    //decimal? TotalLiability_INDT = 0;
                    //decimal? Provisioninbooks_INDT = 0;
                    //decimal? PreDeposits_INDT = 0;
                    //decimal? Recovery_INDT = 0;
                    //decimal? Noofactivecases_INDT = 0;

                    //if (INDIRdetails != null && INDIRdetails.Count > 0)
                    //{
                    //    foreach (var item in legrandlist)
                    //    {
                    //        if (item.BranchName == "Legrand")
                    //        {
                    //            var detilsLegranddata = (from c in entities.Sp_TotalLiabilityInDirTax(AuthenticationHelper.CustomerID, item.BranchId, AuthenticationHelper.Role, AuthenticationHelper.UserID)
                    //                                     select c).FirstOrDefault();
                    //            if (detilsLegranddata != null)
                    //            {
                    //                DIVLegrand_TL.InnerHtml = $"{decimal.Round((decimal)detilsLegranddata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVLegrand_PB.InnerHtml = $"{decimal.Round((decimal)detilsLegranddata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVLegrand_PD.InnerHtml = $"{decimal.Round((decimal)detilsLegranddata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVLegrand_RE.InnerHtml = $"{decimal.Round((decimal)detilsLegranddata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";

                    //                DIVLegrand_NOAC.InnerHtml = Convert.ToString(detilsLegranddata.No_Of_Active_Cases);

                    //                TotalLiability_INDT += detilsLegranddata.TotalLiability;
                    //                Provisioninbooks_INDT += (decimal)detilsLegranddata.ProvisioninBooks;
                    //                PreDeposits_INDT += detilsLegranddata.Provisionalamt;
                    //                Noofactivecases_INDT += detilsLegranddata.No_Of_Active_Cases;
                    //                Recovery_INDT += detilsLegranddata.RecoveryAmount;
                    //            }
                    //            else
                    //            {
                    //                DIVLegrand_TL.InnerHtml = "0";
                    //                DIVLegrand_PB.InnerHtml = "0";
                    //                DIVLegrand_PD.InnerHtml = "0";
                    //                DIVLegrand_RE.InnerHtml = "0";
                    //                DIVLegrand_NOAC.InnerHtml = "0";
                    //            }
                    //        }
                    //        else if (item.BranchName == "Indoasian")
                    //        {
                    //            var detilsIndoasiandata = (from c in entities.Sp_TotalLiabilityInDirTax(AuthenticationHelper.CustomerID, item.BranchId, AuthenticationHelper.Role, AuthenticationHelper.UserID)
                    //                                       select c).FirstOrDefault();

                    //            if (detilsIndoasiandata != null)
                    //            {
                    //                DIVIndoasian_TL.InnerHtml = $"{decimal.Round((decimal)detilsIndoasiandata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVIndoasian_PB.InnerHtml = $"{decimal.Round((decimal)detilsIndoasiandata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVIndoasian_PD.InnerHtml = $"{decimal.Round((decimal)detilsIndoasiandata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVIndoasian_RE.InnerHtml = $"{decimal.Round((decimal)detilsIndoasiandata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVIndoasian_NOAC.InnerHtml = Convert.ToString(detilsIndoasiandata.No_Of_Active_Cases);

                    //                TotalLiability_INDT += detilsIndoasiandata.TotalLiability;
                    //                Provisioninbooks_INDT += (decimal)detilsIndoasiandata.ProvisioninBooks;
                    //                PreDeposits_INDT += detilsIndoasiandata.Provisionalamt;
                    //                Recovery_INDT += detilsIndoasiandata.RecoveryAmount;
                    //                Noofactivecases_INDT += detilsIndoasiandata.No_Of_Active_Cases;
                    //            }
                    //            else
                    //            {
                    //                DIVIndoasian_TL.InnerHtml = "0";
                    //                DIVIndoasian_PB.InnerHtml = "0";
                    //                DIVIndoasian_PD.InnerHtml = "0";
                    //                DIVIndoasian_RE.InnerHtml = "0";
                    //                DIVIndoasian_NOAC.InnerHtml = "0";
                    //            }
                    //        }
                    //        else if (item.BranchName == "Numeric")
                    //        {
                    //            var detilsNumericdata = (from c in entities.Sp_TotalLiabilityInDirTax(AuthenticationHelper.CustomerID, item.BranchId, AuthenticationHelper.Role, AuthenticationHelper.UserID)
                    //                                     select c).FirstOrDefault();

                    //            if (detilsNumericdata != null)
                    //            {
                    //                DIVNumeric_TL.InnerHtml = $"{decimal.Round((decimal)detilsNumericdata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVNumeric_PB.InnerHtml = $"{decimal.Round((decimal)detilsNumericdata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVNumeric_PD.InnerHtml = $"{decimal.Round((decimal)detilsNumericdata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVNumeric_RE.InnerHtml = $"{decimal.Round((decimal)detilsNumericdata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVNumeric_NOAC.InnerHtml = Convert.ToString(detilsNumericdata.No_Of_Active_Cases);

                    //                TotalLiability_INDT += detilsNumericdata.TotalLiability;
                    //                Provisioninbooks_INDT += (decimal)detilsNumericdata.ProvisioninBooks;
                    //                PreDeposits_INDT += detilsNumericdata.Provisionalamt;
                    //                Recovery_INDT += detilsNumericdata.RecoveryAmount;
                    //                Noofactivecases_INDT += detilsNumericdata.No_Of_Active_Cases;
                    //            }
                    //            else
                    //            {
                    //                DIVNumeric_TL.InnerHtml = "0";
                    //                DIVNumeric_PB.InnerHtml = "0";
                    //                DIVNumeric_PD.InnerHtml = "0";
                    //                DIVNumeric_RE.InnerHtml = "0";
                    //                DIVNumeric_NOAC.InnerHtml = "0";
                    //            }
                    //        }
                    //        else if (item.BranchName == "Adlec")
                    //        {
                    //            var detilsAdlecdata = (from c in entities.Sp_TotalLiabilityInDirTax(AuthenticationHelper.CustomerID, item.BranchId, AuthenticationHelper.Role, AuthenticationHelper.UserID)
                    //                                   select c).FirstOrDefault();

                    //            if (detilsAdlecdata != null)
                    //            {
                    //                DIVAdlec_TL.InnerHtml = $"{decimal.Round((decimal)detilsAdlecdata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVAdlec_PB.InnerHtml = $"{decimal.Round((decimal)detilsAdlecdata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVAdlec_PD.InnerHtml = $"{decimal.Round((decimal)detilsAdlecdata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVAdlec_RE.InnerHtml = $"{decimal.Round((decimal)detilsAdlecdata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //                DIVAdlec_NOAC.InnerHtml = Convert.ToString(detilsAdlecdata.No_Of_Active_Cases);

                    //                TotalLiability_INDT += detilsAdlecdata.TotalLiability;
                    //                Provisioninbooks_INDT += (decimal)detilsAdlecdata.ProvisioninBooks;
                    //                PreDeposits_INDT += detilsAdlecdata.Provisionalamt;
                    //                Recovery_INDT += detilsAdlecdata.RecoveryAmount;
                    //                Noofactivecases_INDT += detilsAdlecdata.No_Of_Active_Cases;
                    //            }
                    //            else
                    //            {
                    //                DIVAdlec_TL.InnerHtml = "0";
                    //                DIVAdlec_PB.InnerHtml = "0";
                    //                DIVAdlec_PD.InnerHtml = "0";
                    //                DIVAdlec_RE.InnerHtml = "0";
                    //                DIVAdlec_NOAC.InnerHtml = "0";
                    //            }
                    //        }
                    //    }

                    //    DIVTotalIDT_TL.InnerHtml = $"{decimal.Round((decimal)TotalLiability_INDT/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //    DIVTotalIDT_PB.InnerHtml = $"{decimal.Round((decimal)Provisioninbooks_INDT/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //    DIVTotalIDT_PD.InnerHtml = $"{decimal.Round((decimal)PreDeposits_INDT/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //    DIVTotalIDT_RE.InnerHtml = $"{decimal.Round((decimal)Recovery_INDT/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //    DIVTotalIDT_NOAC.InnerHtml = Convert.ToString(Noofactivecases_INDT);
                    //}
                    //else
                    //{
                    //    DIVLegrand_TL.InnerHtml = "0";
                    //    DIVLegrand_PB.InnerHtml = "0";
                    //    DIVLegrand_PD.InnerHtml = "0";
                    //    DIVLegrand_RE.InnerHtml = "0";
                    //    DIVLegrand_NOAC.InnerHtml = "0";

                    //    DIVIndoasian_TL.InnerHtml = "0";
                    //    DIVIndoasian_PB.InnerHtml = "0";
                    //    DIVIndoasian_PD.InnerHtml = "0";
                    //    DIVIndoasian_RE.InnerHtml = "0";
                    //    DIVIndoasian_NOAC.InnerHtml = "0";

                    //    DIVNumeric_TL.InnerHtml = "0";
                    //    DIVNumeric_PB.InnerHtml = "0";
                    //    DIVNumeric_PD.InnerHtml = "0";
                    //    DIVNumeric_RE.InnerHtml = "0";
                    //    DIVNumeric_NOAC.InnerHtml = "0";

                    //    DIVAdlec_TL.InnerHtml = "0";
                    //    DIVAdlec_PB.InnerHtml = "0";
                    //    DIVAdlec_PD.InnerHtml = "0";
                    //    DIVAdlec_RE.InnerHtml = "0";
                    //    DIVAdlec_NOAC.InnerHtml = "0";

                    //    DIVTotalIDT_TL.InnerHtml = "0";
                    //    DIVTotalIDT_PB.InnerHtml = "0";
                    //    DIVTotalIDT_PD.InnerHtml = "0";
                    //    DIVTotalIDT_RE.InnerHtml = "0";
                    //    DIVTotalIDT_NOAC.InnerHtml = "0";
                    //}
                    //#endregion

                    //TotalLiability_DT_INDT += TotalLiability_INDT;
                    //Provisioninbooks_DT_INDT += Provisioninbooks_INDT;
                    //PreDeposits_DT_INDT += PreDeposits_INDT;
                    //Recovery_DT_INDT += Recovery_INDT;
                    //Noofactivecases_DT_INDT += Noofactivecases_INDT;

                    //#region Total DT + INDT
                    //DIVIDT_DT_TL.InnerHtml = $"{decimal.Round((decimal)TotalLiability_DT_INDT/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //DIVIDT_DT_PB.InnerHtml = $"{decimal.Round((decimal)Provisioninbooks_DT_INDT/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //DIVIDT_DT_PD.InnerHtml = $"{decimal.Round((decimal)PreDeposits_DT_INDT/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //DIVIDT_DT_RE.InnerHtml = $"{decimal.Round((decimal)Recovery_DT_INDT/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    //DIVIDT_DT_NOAC.InnerHtml = Convert.ToString(Noofactivecases_DT_INDT);


                    //All_TotalLiability += TotalLiability_DT_INDT;
                    //All_Provisioninbooks += Provisioninbooks_DT_INDT;
                    //All_PreDeposits += PreDeposits_DT_INDT;
                    //All_Recovery += Recovery_DT_INDT;
                    //All_Noofactivecases += Noofactivecases_DT_INDT;

                    //#endregion




                    decimal? TotalLiability_Civil_Criminal_Other = 0;
                    decimal? Provisioninbooks_Civil_Criminal_Other = 0;
                    decimal? PreDeposits_Civil_Criminal_Other = 0;
                    decimal? Recovery_Civil_Criminal_Other = 0;
                    decimal? Noofactivecases_Civil_Criminal_Other = 0;

                    #region HR Cases

                    var hrDetails = legrandlist.Where(A => A.IsDepartment == true).FirstOrDefault();
                    if (hrDetails != null)
                    {
                        var detilsHRdata = (from c in entities.Sp_TotalLiabilityDepartmentWise(AuthenticationHelper.CustomerID, hrDetails.BranchId, AuthenticationHelper.Role, AuthenticationHelper.UserID)
                                            select c).FirstOrDefault();
                        if (detilsHRdata != null)
                        {
                            DIVHR_TL.InnerHtml = $"{decimal.Round((decimal)detilsHRdata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIVHR_PB.InnerHtml = $"{decimal.Round((decimal)detilsHRdata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIVHR_PD.InnerHtml = $"{decimal.Round((decimal)detilsHRdata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIVHR_RE.InnerHtml = $"{decimal.Round((decimal)detilsHRdata.RecoveryAmount / 1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIVHR_NOAC.InnerHtml = Convert.ToString(detilsHRdata.No_Of_Active_Cases);

                            TotalLiability_Civil_Criminal_Other += detilsHRdata.TotalLiability;
                            Provisioninbooks_Civil_Criminal_Other += (decimal)detilsHRdata.ProvisioninBooks;
                            PreDeposits_Civil_Criminal_Other += detilsHRdata.Provisionalamt;
                            Recovery_Civil_Criminal_Other += detilsHRdata.RecoveryAmount;
                            Noofactivecases_Civil_Criminal_Other += detilsHRdata.No_Of_Active_Cases;
                        }
                        else
                        {
                            DIVHR_TL.InnerHtml = "0";
                            DIVHR_PB.InnerHtml = "0";
                            DIVHR_PD.InnerHtml = "0";
                            DIVHR_RE.InnerHtml = "0";
                            DIVHR_NOAC.InnerHtml = "0";
                        }
                    }
                    else
                    {
                        DIVHR_TL.InnerHtml = "0";
                        DIVHR_PB.InnerHtml = "0";
                        DIVHR_PD.InnerHtml = "0";
                        DIVHR_RE.InnerHtml = "0";
                        DIVHR_NOAC.InnerHtml = "0";
                    }
                    #endregion

                    #region Civil Cases
                    var CivilDetails = legrandlist.Where(A => A.Civil != null).FirstOrDefault();
                    if (CivilDetails != null)
                    {
                        var detilsCVdata = (from c in entities.Sp_TotalLiabilityCaseTypeWise(AuthenticationHelper.CustomerID, CivilDetails.Civil, "C", AuthenticationHelper.Role, AuthenticationHelper.UserID)
                                            select c).FirstOrDefault();
                        if (detilsCVdata != null)
                        {
                            DIV_Civil_TL.InnerHtml = $"{decimal.Round((decimal)detilsCVdata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIV_Civil_PB.InnerHtml = $"{decimal.Round((decimal)detilsCVdata.ProvisioninBooks / 1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIV_Civil_PD.InnerHtml = $"{decimal.Round((decimal)detilsCVdata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIV_Civil_RE.InnerHtml = $"{decimal.Round((decimal)detilsCVdata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIV_Civil_NOAC.InnerHtml = Convert.ToString(detilsCVdata.No_Of_Active_Cases);

                            TotalLiability_Civil_Criminal_Other += detilsCVdata.TotalLiability;
                            Provisioninbooks_Civil_Criminal_Other += (decimal)detilsCVdata.ProvisioninBooks;
                            PreDeposits_Civil_Criminal_Other += detilsCVdata.Provisionalamt;
                            Recovery_Civil_Criminal_Other += detilsCVdata.RecoveryAmount;
                            Noofactivecases_Civil_Criminal_Other += detilsCVdata.No_Of_Active_Cases;
                        }
                        else
                        {
                            DIV_Civil_TL.InnerHtml = "0";
                            DIV_Civil_PB.InnerHtml = "0";
                            DIV_Civil_PD.InnerHtml = "0";
                            DIV_Civil_RE.InnerHtml = "0";
                            DIV_Civil_NOAC.InnerHtml = "0";
                        }
                    }
                    else
                    {
                        DIV_Civil_TL.InnerHtml = "0";
                        DIV_Civil_PB.InnerHtml = "0";
                        DIV_Civil_PD.InnerHtml = "0";
                        DIV_Civil_RE.InnerHtml = "0";
                        DIV_Civil_NOAC.InnerHtml = "0";
                    }
                    #endregion

                    #region Criminal Cases
                    var CriminalDetails = legrandlist.Where(A => A.Criminal != null).FirstOrDefault();
                    if (CriminalDetails != null)
                    {
                        var detilsCRdata = (from c in entities.Sp_TotalLiabilityCaseTypeWise(AuthenticationHelper.CustomerID, CriminalDetails.Criminal, "C",AuthenticationHelper.Role, AuthenticationHelper.UserID)
                                            select c).FirstOrDefault();
                        if (detilsCRdata != null)
                        {
                            DIV_Criminal_TL.InnerHtml = $"{decimal.Round((decimal)detilsCRdata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIV_Criminal_PB.InnerHtml = $"{decimal.Round((decimal)detilsCRdata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIV_Criminal_PD.InnerHtml = $"{decimal.Round((decimal)detilsCRdata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIV_Criminal_RE.InnerHtml = $"{decimal.Round((decimal)detilsCRdata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                            DIV_Criminal_NOAC.InnerHtml = Convert.ToString(detilsCRdata.No_Of_Active_Cases);

                            TotalLiability_Civil_Criminal_Other += detilsCRdata.TotalLiability;
                            Provisioninbooks_Civil_Criminal_Other += (decimal)detilsCRdata.ProvisioninBooks;
                            PreDeposits_Civil_Criminal_Other += detilsCRdata.Provisionalamt;
                            Recovery_Civil_Criminal_Other += detilsCRdata.RecoveryAmount;
                            Noofactivecases_Civil_Criminal_Other += detilsCRdata.No_Of_Active_Cases;
                        }
                        else
                        {
                            DIV_Criminal_TL.InnerHtml = "0";
                            DIV_Criminal_PB.InnerHtml = "0";
                            DIV_Criminal_PD.InnerHtml = "0";
                            DIV_Criminal_RE.InnerHtml = "0";
                            DIV_Criminal_NOAC.InnerHtml = "0";
                        }
                    }
                    else
                    {
                        DIV_Criminal_TL.InnerHtml = "0";
                        DIV_Criminal_PB.InnerHtml = "0";
                        DIV_Criminal_PD.InnerHtml = "0";
                        DIV_Criminal_RE.InnerHtml = "0";
                        DIV_Criminal_NOAC.InnerHtml = "0";
                    }
                    #endregion

                    #region Other Legal Cases              
                    var detilsOTdata = (from c in entities.Sp_TotalLiabilityCaseTypeWise(AuthenticationHelper.CustomerID, -1, "O", AuthenticationHelper.Role, AuthenticationHelper.UserID)
                                        select c).FirstOrDefault();
                    if (detilsOTdata != null)
                    {
                        DIV_Other_TL.InnerHtml = $"{decimal.Round((decimal)detilsOTdata.TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                        DIV_Other_PB.InnerHtml = $"{decimal.Round((decimal)detilsOTdata.ProvisioninBooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                        DIV_Other_PD.InnerHtml = $"{decimal.Round((decimal)detilsOTdata.Provisionalamt/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                        DIV_Other_RE.InnerHtml = $"{decimal.Round((decimal)detilsOTdata.RecoveryAmount/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                        DIV_Other_NOAC.InnerHtml = Convert.ToString(detilsOTdata.No_Of_Active_Cases);

                        TotalLiability_Civil_Criminal_Other += detilsOTdata.TotalLiability;
                        Provisioninbooks_Civil_Criminal_Other += (decimal)detilsOTdata.ProvisioninBooks;
                        PreDeposits_Civil_Criminal_Other += detilsOTdata.Provisionalamt;
                        Recovery_Civil_Criminal_Other += detilsOTdata.RecoveryAmount;
                        Noofactivecases_Civil_Criminal_Other += detilsOTdata.No_Of_Active_Cases;
                    }
                    else
                    {
                        DIV_Other_TL.InnerHtml = "0";
                        DIV_Other_PB.InnerHtml = "0";
                        DIV_Other_PD.InnerHtml = "0";
                        DIV_Other_RE.InnerHtml = "0";
                        DIV_Other_NOAC.InnerHtml = "0";
                    }
                    #endregion

                    DIVTotal_TL.InnerHtml = $"{decimal.Round((decimal)TotalLiability_Civil_Criminal_Other/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    DIVTotal_PB.InnerHtml = $"{decimal.Round((decimal)Provisioninbooks_Civil_Criminal_Other/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    DIVTotal_PD.InnerHtml = $"{decimal.Round((decimal)PreDeposits_Civil_Criminal_Other/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    DIVTotal_RE.InnerHtml = $"{decimal.Round((decimal)Recovery_Civil_Criminal_Other/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    DIVTotal_NOAC.InnerHtml = Convert.ToString(Noofactivecases_Civil_Criminal_Other);




                    All_TotalLiability += TotalLiability_Civil_Criminal_Other;
                    All_Provisioninbooks += Provisioninbooks_Civil_Criminal_Other;
                    All_PreDeposits += PreDeposits_Civil_Criminal_Other;
                    All_Recovery += Recovery_Civil_Criminal_Other;
                    All_Noofactivecases += Noofactivecases_Civil_Criminal_Other;


                    DIVAllTotal_TL.InnerHtml = $"{decimal.Round((decimal)All_TotalLiability/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    DIVAllTotal_PB.InnerHtml = $"{decimal.Round((decimal)All_Provisioninbooks/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    DIVAllTotal_PD.InnerHtml = $"{decimal.Round((decimal)All_PreDeposits/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    DIVAllTotal_RE.InnerHtml = $"{decimal.Round((decimal)All_Recovery/1000000, 2, MidpointRounding.AwayFromZero):n2}";
                    DIVAllTotal_NOAC.InnerHtml = Convert.ToString(All_Noofactivecases);

                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}