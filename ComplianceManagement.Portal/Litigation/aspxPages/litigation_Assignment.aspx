﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="litigation_Assignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.litigation_Assignment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        div#ContentPlaceHolder1_ddlDepartment_chosen {
            width: 370px !important;
        }

        div#ContentPlaceHolder1_ddlAct_chosen {
            width: 370px !important;
        }

        div#ContentPlaceHolder1_ddlEntity_chosen {
            width: 370px !important;
        }

        div#ContentPlaceHolder1_ddlCity_chosen {
            width: 370px !important;
        }

        div#ContentPlaceHolder1_ddlMappingLawyer_sl {
            WIDTH: 370px !important;
            height: 35px !important;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        function fopenpopup() {
            $('#DivAddLCPartyDetails').modal('show');
        }
    </script>

    <script type="text/javascript">

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('Legal Case Party Detail');
            fhead('Legal Case Party Detail');
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upLCPanel" runat="server" UpdateMode="Conditional" OnLoad="upLCPanel_Load">
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <%--<section class="panel">--%>
                        <div style="margin-bottom: 4px" />
                        <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-3 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Text="5" Selected="True" />
                                <asp:ListItem Text="10" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                        </div>

                         <div class="col-md-10 colpadding0" style="text-align: right">
                             <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddPromotor" OnClick="btnAddPromotor_Click" />
                        </div>
                    </div>
                   
                    <div style="margin-bottom: 4px">
                        <asp:GridView runat="server" ID="grdLitigationDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                            PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID" OnRowCommand="grdLitigationDetails_RowCommand">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Act">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActID") %>' ToolTip='<%# Eval("ActID") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Entity">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LegalEntity") %>' ToolTip='<%# Eval("LegalEntity") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City" Visible="false">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CityID") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Department">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DepartmentID") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount of liability">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LiabilityAmount") %>' ToolTip='<%# Eval("LiabilityAmount") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_LawyerLC" OnClientClick="fopenpopup()" ToolTip="Change details" data-toggle="tooltip"
                                            CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Details" title="Edit  Details" /></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_LawyerLC" ToolTip="Delete details" data-toggle="tooltip" Visible="true"
                                            CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Party Details?');"><img src="../../Images/delete_icon_new.png" alt="Delete Details" title="Delete Details" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerSettings Visible="false" />
                            <PagerTemplate>
                            </PagerTemplate>
                            <EmptyDataTemplate>No Record Found</EmptyDataTemplate>
                        </asp:GridView>

                    </div>

                    <div class="col-md-12 colpadding0">
                        <div class="col-md-10 colpadding0"></div>
                        <div class="col-md-2 colpadding0" style="float: right;">
                            <div style="float: left; width: 50%">
                                <p class="clsPageNo">Page</p>
                            </div>
                            <div style="float: left; width: 50%">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No"
                                    OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    </div>
                </div>
                <%--</section>--%>
            </div>
                
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="DivAddLCPartyDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 550px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="AddCountry">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional" OnLoad="upPromotor_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="NewAddLaywerValidate" />
                                        <asp:CustomValidator ID="cvCustomeRemark" runat="server" EnableClientScript="False"
                                            ValidationGroup="NewAddLaywerValidate" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="NewAddLaywerValidate" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                                Entity</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlEntity" class="form-control m-bot15" Style="width: 250px;"
                                                AutoPostBack="true" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Select Entity"
                                                ControlToValidate="ddlEntity" runat="server" ValidationGroup="NewAddLaywerValidate"
                                                Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                                City</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlCity" class="form-control m-bot15" Style="width: 250px;"
                                                AutoPostBack="true" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Select City"
                                                ControlToValidate="ddlCity" runat="server" ValidationGroup="NewAddLaywerValidate"
                                                Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                                Department</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlDepartment" class="form-control m-bot15" Style="width: 250px;"
                                                AutoPostBack="true" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Please Select Department"
                                                ControlToValidate="ddlDepartment" runat="server" ValidationGroup="NewAddLaywerValidate"
                                                Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                                Mapping Lawyer</label>
                                            <asp:DropDownCheckBoxes ID="ddlMappingLawyer" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15" AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 390px; height: 37px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Type" />
                                            </asp:DropDownCheckBoxes>
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                                Act</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlAct" class="form-control m-bot15" Style="width: 250px;"
                                                AutoPostBack="true" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Act"
                                                ControlToValidate="ddlAct" runat="server" ValidationGroup="NewAddLaywerValidate"
                                                Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                                Title</label>
                                            <asp:TextBox runat="server" ID="tbxTitle" Style="width: 370px;" CssClass="form-control" MaxLength="100" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Title can not be empty."
                                                ControlToValidate="tbxTitle" runat="server" ValidationGroup="NewAddLaywerValidate"
                                                Display="None" />
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                                ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid first name."
                                                ControlToValidate="tbxTitle" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>--%>
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                                Description</label>
                                            <asp:TextBox runat="server" ID="tbxDescription" Style="width: 370px;" CssClass="form-control" MaxLength="100" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description can not be empty."
                                                ControlToValidate="tbxDescription" runat="server" ValidationGroup="NewAddLaywerValidate"
                                                Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                                Amount of liability</label>
                                            <asp:TextBox runat="server" ID="tbxAmtLiability" Style="width: 370px;" CssClass="form-control" MaxLength="100" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Amount of Liability can not be empty."
                                                ControlToValidate="tbxAmtLiability" runat="server" ValidationGroup="NewAddLaywerValidate"
                                                Display="None" />
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid first name."
                                                ControlToValidate="tbxAmtLiability" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>--%>
                                        </div>
                                        <div style="margin-bottom: 7px; float: right; margin-right: 150px; margin-top: 10px;">
                                            <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                                ValidationGroup="NewAddLaywerValidate" />
                                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 15px;">
                                            <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                        </div>
                                    </div>
                                    <div class="clearfix" style="height: 100px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
