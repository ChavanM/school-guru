﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon;
using Amazon.S3.Model;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class AddIndependentTask : System.Web.UI.Page
    {
        public static string DocumentPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUsers();
                if (!string.IsNullOrEmpty(Request.QueryString["NoticeCaseInstanceID"]))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["TaskID"]))
                    {
                        if (Convert.ToString(Request.QueryString["TaskID"]) != "0")
                        {
                            TaskEdit(Convert.ToInt32(Request.QueryString["NoticeCaseInstanceID"]), Convert.ToInt32(Request.QueryString["TaskID"]));
                            ViewState["TaskMode"] = "Edit";
                        }
                        else
                        {
                            ViewState["TaskMode"] = "ADD";
                        }
                    }
                    else
                    {
                        ViewState["TaskMode"] = "ADD";
                    }
                }
            }
        }

        protected void TaskEdit(int NoticeCaseInstanceID, int taskID)
        {
            if (taskID != 0)
            {
                string TaskTypeVal = string.Empty;
                string DoctypeVal = string.Empty;

                TaskTypeVal = NoticeManagement.GetTaskType(taskID, AuthenticationHelper.CustomerID);
                var Caseresponse = NoticeManagement.GetNoticeTaskDetailByTaskID(NoticeCaseInstanceID, taskID, TaskTypeVal, AuthenticationHelper.CustomerID);
                if (Caseresponse != null)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Caseresponse.ScheduleOnDate)))
                    {
                        tbxTaskDueDate.Text = Convert.ToDateTime(Caseresponse.ScheduleOnDate).ToString("dd-MM-yyyy");
                    }

                    tbxTaskTitle.Text = Caseresponse.TaskTitle;
                    tbxTaskDesc.Text = Caseresponse.TaskDesc;
                    tbxExpOutcome.Text = Caseresponse.ExpOutcome;
                    ddlTaskPriorityADD.SelectedValue = Convert.ToString(Caseresponse.PriorityID);
                    tbxTaskRemark.Text = Caseresponse.Remark;
                    tbxTaskID.Text = Convert.ToString(Caseresponse.ID);
                    //ddlHearingRefNo.SelectedValue = Convert.ToString(Caseresponse.RefID);
                    var FindIntText = ddlTaskLawyerListInternal.Items.FindByValue(Convert.ToString(Caseresponse.AssignTo));
                    var FindExtText = ddlTaskUserExternal.Items.FindByValue(Convert.ToString(Caseresponse.AssignTo));
                    if (FindIntText != null)
                    {
                        ddlTaskLawyerListInternal.SelectedValue = Convert.ToString(Caseresponse.AssignTo);
                    }
                    if (FindExtText != null)
                    {
                        ddlTaskUserExternal.SelectedValue = Convert.ToString(Caseresponse.AssignTo);
                    }

                    if (Caseresponse.StatusID == 3)
                    {
                        ViewState["TaskStatus"] = "Closed";
                        btnTaskSave.Enabled = false;
                        // grdTaskEditDoc.Enabled = false;
                    }
                    else
                    {
                        btnTaskSave.Enabled = true;
                        //  grdTaskEditDoc.Enabled = true;
                    }

                    if (TaskTypeVal == "C")
                    {
                        DoctypeVal = "CT";
                    }
                    else if (TaskTypeVal == "N")
                    {
                        DoctypeVal = "NT";
                    }
                    if (TaskTypeVal == "T")
                    {
                        DoctypeVal = "T";
                    }

                    var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(NoticeCaseInstanceID, taskID, DoctypeVal);
                    if (lstResponseDocument.Count > 0)
                    {
                        grdTaskEditDoc.DataSource = lstResponseDocument;
                        grdTaskEditDoc.DataBind();
                        DivTaskEdit.Visible = true;
                        ViewState["TaskMode"] = "Edit";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                    }
                }
            }
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();

                var internalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 1);

                //Task Assignment - For User Filter
                ddlTaskLawyerListInternal.DataTextField = "Name";
                ddlTaskLawyerListInternal.DataValueField = "ID";

                ddlTaskLawyerListInternal.DataSource = internalUsers;
                ddlTaskLawyerListInternal.DataBind();
                ddlTaskLawyerListInternal.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", "0"));

                var lawyerAndExternalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);

                ddlTaskUserExternal.DataValueField = "ID";
                ddlTaskUserExternal.DataTextField = "Name";
                ddlTaskUserExternal.DataSource = lawyerAndExternalUsers;
                ddlTaskUserExternal.DataBind();

                ddlTaskUserExternal.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                lstAllUsers.Clear();
                lstAllUsers = null;
                internalUsers.Clear();
                internalUsers = null;
                lawyerAndExternalUsers.Clear();
                lawyerAndExternalUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnTaskSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool validateData = false;
                bool saveSuccess = false;
                int AssignedToUser = -1;
                int DocTypeID = -1;
                int customerID = -1;
                string TaskTypeVal = string.Empty;
                string DoctypeVal = string.Empty;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (tbxTaskTitle.Text != "")
                {
                    if (tbxTaskDueDate.Text != "")
                    {
                        if ((!String.IsNullOrEmpty(ddlTaskLawyerListInternal.SelectedValue) && ddlTaskLawyerListInternal.SelectedValue != "0") ||
                            (!String.IsNullOrEmpty(ddlTaskUserExternal.SelectedValue) && ddlTaskUserExternal.SelectedValue != "0"))
                        {
                            if (!String.IsNullOrEmpty(ddlTaskPriorityADD.SelectedValue))
                            {
                                if (tbxTaskDesc.Text != "")
                                {
                                    validateData = true;

                                    if (!String.IsNullOrEmpty(ddlTaskLawyerListInternal.SelectedValue) && ddlTaskLawyerListInternal.SelectedValue != "0")
                                        AssignedToUser = Convert.ToInt32(ddlTaskLawyerListInternal.SelectedValue);
                                    else if (!String.IsNullOrEmpty(ddlTaskUserExternal.SelectedValue) && ddlTaskUserExternal.SelectedValue != "0")
                                        AssignedToUser = Convert.ToInt32(ddlTaskUserExternal.SelectedValue);
                                }
                                else
                                {
                                    CvAddTasValidate.IsValid = false;
                                    CvAddTasValidate.ErrorMessage = "Provide Task Description.";
                                    return;
                                }
                            }
                            else
                            {
                                CvAddTasValidate.IsValid = false;
                                CvAddTasValidate.ErrorMessage = "Select Task Priority.";
                                return;
                            }
                        }
                        else
                        {
                            CvAddTasValidate.IsValid = false;
                            CvAddTasValidate.ErrorMessage = "Please Select at least one User either Internal or Lawyer/External for task assignment.";
                            return;
                        }
                    }
                    else
                    {
                        CvAddTasValidate.IsValid = false;
                        CvAddTasValidate.ErrorMessage = "Provide Task Due Date.";
                        return;
                    }
                }
                else
                {
                    CvAddTasValidate.IsValid = false;
                    CvAddTasValidate.ErrorMessage = "Provide Task Title.";
                    return;
                }

                tbl_TaskScheduleOn newRecord = new tbl_TaskScheduleOn();

                if (validateData)
                {
                    newRecord.IsActive = true;
                    newRecord.NoticeCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                    newRecord.ScheduleOnDate = DateTimeExtensions.GetDate(tbxTaskDueDate.Text);
                    newRecord.TaskTitle = tbxTaskTitle.Text.Trim();
                    newRecord.TaskDesc = tbxTaskDesc.Text.Trim();
                    newRecord.ExpOutcome = tbxExpOutcome.Text;
                    newRecord.StatusID = 1;
                    newRecord.CustomerID = customerID;
                    newRecord.CreatedBy = AuthenticationHelper.UserID;
                    newRecord.CreatedByText = AuthenticationHelper.User;
                    newRecord.LinkCreatedOn = DateTime.Now;
                    newRecord.URLExpired = false;
                    
                    if (AssignedToUser != -1)
                        newRecord.AssignTo = AssignedToUser;

                    if (!String.IsNullOrEmpty(ddlTaskPriorityADD.SelectedValue))
                        newRecord.PriorityID = Convert.ToInt32(ddlTaskPriorityADD.SelectedValue);

                    if (tbxTaskRemark.Text != "")
                        newRecord.Remark = tbxTaskRemark.Text.Trim();


                    if (Convert.ToString(ViewState["TaskMode"]) == "Edit")
                    {
                        newRecord.ID = Convert.ToInt32(tbxTaskID.Text);
                        
                        TaskTypeVal = NoticeManagement.GetTaskType(Convert.ToInt32(tbxTaskID.Text), AuthenticationHelper.CustomerID);

                        if (TaskTypeVal == "C")
                        {
                            DoctypeVal = "CT";
                        }
                        else if (TaskTypeVal == "N")
                        {
                            DoctypeVal = "NT";
                        }
                        if (TaskTypeVal == "T")
                        {
                            DoctypeVal = "T";
                        }
                        newRecord.TaskType = TaskTypeVal;
                        saveSuccess = LitigationTaskManagement.UpdateTask(newRecord);
                        grdTaskEditDoc.DataSource = null;
                        grdTaskEditDoc.DataBind();
                        DivTaskEdit.Visible = false;
                    }
                    else
                    {
                        newRecord.TaskType = "T";
                        if (!LitigationTaskManagement.ExistNoticeCaseTaskTitle(tbxTaskTitle.Text.Trim(), "C",(long)newRecord.NoticeCaseInstanceID, customerID))
                        {
                            saveSuccess = LitigationTaskManagement.CreateTask(newRecord);
                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("T", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Task Created", true);
                            }
                        }
                        else
                        {
                            saveSuccess = false;
                            CvAddTasValidate.IsValid = false;
                            CvAddTasValidate.ErrorMessage = "Task with same title already exists.";
                            tbxTaskTitle.Focus();
                            return;
                        }
                    }

                    if (saveSuccess)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            //Save Task Related Uploaded Documents
                            #region AWS Upload Document

                            if (fuTaskDocUpload.HasFiles)
                            {
                                tbl_LitigationFileData objTaskDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = newRecord.NoticeCaseInstanceID,
                                    DocTypeInstanceID = newRecord.ID, //TaskID
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    //DocType = "T"
                                };

                                if (Convert.ToString(ViewState["TaskMode"]) == "Edit")
                                {
                                    objTaskDoc.DocType = DoctypeVal;
                                }
                                else
                                {
                                    objTaskDoc.DocType = "T";
                                }

                                HttpFileCollection fileCollection1 = Request.Files;

                                if (fileCollection1.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                    string directoryPath = "";
                                    string fileName = "";

                                    if (newRecord.ID > 0)
                                    {
                                        for (int i = 0; i < fileCollection1.Count; i++)
                                        {
                                            HttpPostedFile uploadedFile = fileCollection1[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection1.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                //Get Document Version
                                                var caseTaskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objTaskDoc);

                                                caseTaskDocVersion++;
                                                objTaskDoc.Version = caseTaskDocVersion + ".0";

                                                
                                                directoryPath = "LitigationDocuments\\" + customerID + "\\Cases\\" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "\\" + "Task\\" + objTaskDoc.Version;

                                                IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                if (!di.Exists)
                                                {
                                                    di.Create();
                                                }

                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                                string p_strPath = string.Empty;
                                                string dirpath = string.Empty;
                                                //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                                                //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;
                                                //if (File.Exists(p_strPath))
                                                //    File.Delete(p_strPath);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                                dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;



                                                if (!Directory.Exists(dirpath))
                                                {
                                                    Directory.CreateDirectory(dirpath);
                                                }
                                                FileStream objFileStrm = File.Create(p_strPath);
                                                objFileStrm.Close();
                                                File.WriteAllBytes(p_strPath, bytes);

                                                Guid fileKey1 = Guid.NewGuid();

                                                string AWSpath = directoryPath + "\\" + uploadedFile.FileName;

                                                objTaskDoc.FileName = fileName;
                                                objTaskDoc.FilePath = directoryPath.Replace(@"\", "/");
                                                objTaskDoc.FileKey = fileKey1.ToString();
                                                objTaskDoc.VersionDate = DateTime.Now;
                                                objTaskDoc.CreatedOn = DateTime.Now;
                                                objTaskDoc.FileSize = uploadedFile.ContentLength;
                                                FileInfo localFile = new FileInfo(p_strPath);
                                                S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                if (!s3File.Exists)
                                                {
                                                    using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                    {
                                                        localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                    }
                                                }
                                                //DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                                saveSuccess = CaseManagement.CreateCaseDocumentMapping(objTaskDoc);

                                                Filelist1.Clear();
                                            }
                                        }//End For Each      
                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("T", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Document(s) Uploaded.", true);
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            #region Upload Document

                            if (fuTaskDocUpload.HasFiles)
                            {
                                tbl_LitigationFileData objTaskDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = newRecord.NoticeCaseInstanceID,
                                    DocTypeInstanceID = newRecord.ID, //TaskID
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    //DocType = "T"
                                };

                                if (Convert.ToString(ViewState["TaskMode"]) == "Edit")
                                {
                                    objTaskDoc.DocType = DoctypeVal;
                                }
                                else
                                {
                                    objTaskDoc.DocType = "T";
                                }

                                HttpFileCollection fileCollection1 = Request.Files;

                                if (fileCollection1.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                    string directoryPath = "";
                                    string fileName = "";

                                    if (newRecord.ID > 0)
                                    {
                                        for (int i = 0; i < fileCollection1.Count; i++)
                                        {
                                            HttpPostedFile uploadedFile = fileCollection1[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection1.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                //Get Document Version
                                                var caseTaskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objTaskDoc);

                                                caseTaskDocVersion++;
                                                objTaskDoc.Version = caseTaskDocVersion + ".0";

                                                directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "/Task/" + objTaskDoc.Version);

                                                if (!Directory.Exists(directoryPath))
                                                    Directory.CreateDirectory(directoryPath);

                                                Guid fileKey1 = Guid.NewGuid();
                                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                objTaskDoc.FileName = fileName;
                                                objTaskDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                objTaskDoc.FileKey = fileKey1.ToString();
                                                objTaskDoc.VersionDate = DateTime.Now;
                                                objTaskDoc.CreatedOn = DateTime.Now;
                                                objTaskDoc.FileSize = uploadedFile.ContentLength;
                                                DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                                saveSuccess = CaseManagement.CreateCaseDocumentMapping(objTaskDoc);

                                                Filelist1.Clear();
                                            }
                                        }//End For Each      
                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("T", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Document(s) Uploaded.", true);
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                    }
                }

                if (saveSuccess)
                {
                    string accessURL = string.Empty;

                    CustomValidatorSuccess.IsValid = false;
                    CustomValidatorSuccess.ErrorMessage = "Task Saved Successfully.";
                    string portalURL = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (Urloutput != null)
                    {
                        portalURL = Urloutput.URL;
                    }
                    else
                    {
                        portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                    if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(newRecord.AssignTo), customerID))
                        accessURL = Convert.ToString(portalURL) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                      CryptographyManagement.Encrypt(newRecord.ID.ToString()) +
                      "&NID=" + CryptographyManagement.Encrypt(newRecord.NoticeCaseInstanceID.ToString());
                    else
                        accessURL = Convert.ToString(portalURL);

                    //if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(newRecord.AssignTo), customerID))
                    //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                    //  CryptographyManagement.Encrypt(newRecord.ID.ToString()) +
                    //  "&NID=" + CryptographyManagement.Encrypt(newRecord.NoticeCaseInstanceID.ToString());
                    //else
                    //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);



                    if (saveSuccess)
                    {
                        if (Convert.ToString(ViewState["TaskMode"]) == "Edit")
                        {
                            CustomValidatorSuccess.IsValid = false;
                            CustomValidatorSuccess.ErrorMessage = "Task Updated Successfully.";
                            newRecord.AccessURL = accessURL;
                            saveSuccess = LitigationTaskManagement.UpdateTaskAccessURL(newRecord.ID, newRecord);
                        }
                        else
                        {
                            saveSuccess = SendTaskAssignmentMail(newRecord, accessURL, AuthenticationHelper.User);
                            CustomValidatorSuccess.IsValid = false;
                            CustomValidatorSuccess.ErrorMessage = "Task Saved Successfully.An Email containing task details and access URL to provide response, sent to assignee.";
                            newRecord.AccessURL = accessURL;
                            saveSuccess = LitigationTaskManagement.UpdateTaskAccessURL(newRecord.ID, newRecord);
                        }
                    }
                    clearTaskControls();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearTaskControls()
        {
            try
            {
                ddlTaskLawyerListInternal.ClearSelection();
                tbxTaskTitle.Text = "";
                tbxTaskDueDate.Text = "";
                tbxTaskDesc.Text = "";
                tbxTaskRemark.Text = "";
                ddlTaskPriorityADD.ClearSelection();
                ddlTaskUserExternal.ClearSelection();
                tbxExpOutcome.Text = "";
                grdTaskEditDoc.DataSource = null;
                grdTaskEditDoc.DataBind();
                DivTaskEdit.Visible = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "BindControls();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void btnTaskClear_Click(object sender, EventArgs e)
        {
            clearTaskControls();
        }

        protected void lnkBtnAssignUser_Click(object sender, EventArgs e)
        {
            BindUsers();
        }

        public bool SendTaskAssignmentMail(tbl_TaskScheduleOn taskRecord, string accessURL, string assignedBy)
        {
            try
            {
                User TaskAssignedUserDetail = null;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                #region Mail Data
                //User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                //string username = string.Format("{0} {1}", User.FirstName, User.LastName);

                if (!string.IsNullOrEmpty(Convert.ToString(taskRecord.AssignTo)))
                {
                    TaskAssignedUserDetail = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));
                }
                var Locations = string.Empty;

                string TaskTitleMerge = taskRecord.TaskTitle;
                string FinalTaskTitle = string.Empty;
                if (TaskTitleMerge.Length > 50)
                {
                    FinalTaskTitle = TaskTitleMerge.Substring(0, 50);
                    FinalTaskTitle = FinalTaskTitle + "...";
                }
                else
                {
                    FinalTaskTitle = TaskTitleMerge;
                }

                #endregion end mail

                if (taskRecord != null)
                {
                    User UserAssigeed = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));
                    var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                    if (UserAssigeed != null)
                    {
                        if (UserAssigeed.Email != null && UserAssigeed.Email != "")
                        {
                            string username = string.Format("{0} {1}", UserAssigeed.FirstName, UserAssigeed.LastName);

                            string portalURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalURL = Urloutput.URL;
                            }
                            else
                            {
                                portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }

                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_IndependentTaskAssignment
                                                                    .Replace("@User", username)
                                                                    .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                                    .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                                    .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                                                                    .Replace("@From", cname.Trim())
                                                                    .Replace("@PortalURL", Convert.ToString(portalURL));

                            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_IndependentTaskAssignment
                            //                                            .Replace("@User", username)
                            //                                            .Replace("@TaskTitle", taskRecord.TaskTitle)
                            //                                            .Replace("@TaskDesc", taskRecord.TaskDesc)
                            //                                            .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                            //                                            .Replace("@From", cname.Trim())
                            //                                            .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { TaskAssignedUserDetail.Email }), null, null, "Litigation Notification Task Assigned - " + FinalTaskTitle, message);//FinalNoticeTitle

                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        protected void grdTaskEditDoc_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteTaskEditDocument"))
            {
                long CaseInstanceID = 0;
                long taskID = 0;
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    bool NoticeRespose = NoticeManagement.DeleteResponseEditDocument(Convert.ToInt32(commandArgs));
                    if (NoticeRespose)
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["TaskID"]))
                        {
                            taskID = Convert.ToInt32(Request.QueryString["TaskID"]);
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["NoticeCaseInstanceID"]))
                        {
                            CaseInstanceID = Convert.ToInt32(Request.QueryString["NoticeCaseInstanceID"]);
                        }
                        string TaskTypeVal = string.Empty;
                        TaskTypeVal = NoticeManagement.GetTaskType(Convert.ToInt32(taskID), AuthenticationHelper.CustomerID);
                        var lstResponseDocument = NoticeManagement.GetNoticeResponseDocuments(CaseInstanceID, taskID, TaskTypeVal);
                        if (lstResponseDocument.Count > 0)
                        {
                            grdTaskEditDoc.DataSource = lstResponseDocument;
                            grdTaskEditDoc.DataBind();
                            DivTaskEdit.Visible = true;
                            updategrid.Update();
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                    }
                }
            }
            else if (e.CommandName.Equals("DownloadTaskEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = NoticeManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }
                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                if (file.FilePath != null)
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));                                        
                                    }
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        CvAddTasValidate.IsValid = false;
                        CvAddTasValidate.ErrorMessage = "No Document Available for Download.";
                        return;
                    }
                }
            }
            else if (e.CommandName.Equals("ViewTaskEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = NoticeManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            if (file.FilePath != null)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }
                                string filePath1 = directoryPath + "/" + file.FileName;
                                DocumentPath = filePath1;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                lblMessage.Text = "";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;

                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                lblMessage.Text = "";

                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                            #endregion
                        }
                    }
                }
            }
        }

        protected void grdTaskEditDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkbtnTaskDelete = (e.Row.FindControl("lnkbtnTaskDelete") as LinkButton);
                if (Convert.ToString(ViewState["TaskStatus"]) == "Closed")
                {
                    lnkbtnTaskDelete.Visible = false;
                }
                else
                {
                    lnkbtnTaskDelete.Visible = true;
                }
            }
        }
    }
}