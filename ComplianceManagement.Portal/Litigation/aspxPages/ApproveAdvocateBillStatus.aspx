﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApproveAdvocateBillStatus.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.ApproveAdvocateBillStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background-color:white">
<head runat="server">
  
    <title></title>
    <style>
        .center {
  margin-left: auto;
  margin-right: auto;
}
        td {
  padding: 5px;
}

    </style>
    <script>
        function fopenAdvBilldocfileReview(file) {

            $('#DocumentAdvBillReviewPopUp1').modal('show');
            $('#CaseAdvBillDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        function OpenAdvBillDocviewer(file) {
            $('#DocumentAdvBillReviewPopUp1').modal('show');
            $('#CaseAdvBillDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
    </script>
</head>
<body >
    <form id="form1" runat="server" style="background-color:white">
       
        <asp:ScriptManager ID="Isdf" runat="server" LoadScriptsBeforeUI="true" EnablePageMethods="true"></asp:ScriptManager>
        <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
  
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
   
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

           <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
        <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
        <link href="https://avacdn.azureedge.net/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />

  
    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />

 <div>
     <div style="margin-bottom: 4px">
         <asp:ValidationSummary ID="ValidationSummary1" Style="padding-left: 5%" runat="server" Display="none"
             class="alert alert-block alert-success fade in" ValidationGroup="ComplianceValidationGroup1" />

         <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in"
             EnableClientScript="true" ValidationGroup="ComplianceValidationGroup1" Style="display: none; padding-left: 40px;" />

         <asp:Label ID="Label1" class="alert alert-block alert-success fade in" Visible="false" runat="server"></asp:Label>
         <asp:Label ID="Labelmsg" class="alert alert-block alert-success fade in" Style="display: none;" runat="server"></asp:Label>
         <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
         <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
     </div>

     <table runat="server" align="center">
      
          <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Case Title</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lblcasetitle" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
          <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Case Description</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lblcasedesc" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
         <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Location</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lbllocation" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
          <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Department</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lbldepartment" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
          <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Law Firm</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lbllawfirm" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
          <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Hearing</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lblhearing" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
            <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Invoice No</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lblinvoiceno" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
          <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Invoice Amount</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lblinvoiceamount" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
          <tr>
             <td style="width: 15%;">
                
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Currency</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:Label runat="server" ID="lblcurrency" Style=" font-size: 13px; color: #333;"
                     maximunsize="500px" autosize="true" ></asp:Label>
                 
             </td>
         </tr>
         <tr>
             <td style="width: 15%;">
                 <% if (IsRemarkCompulsary)
                     {%>
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                 <%}
                     else
                     {%>
                 <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                 <%}%>
                 <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
             </td>
             <td style="width: 2%; font-weight: bold;">: </td>
             <td style="width: 73%;">
                 <asp:TextBox runat="server" ID="tbxRemarks" CssClass="form-control" Width="83.5%" />
             </td>
         </tr>

         <tr style="margin-bottom:2px;">
             <td style="width: 15%">
                 
             </td>
             <td>
                 </td>
         </tr>
         <tr style="margin-bottom:2px;">
             <td style="width: 15%">
                
             </td>
             <td>
                 </td>
         </tr>
          <tr style="margin-bottom:2px;">
             <td style="width: 15%">
                 
             </td>
             <td>
                 </td>
         </tr>
         <tr style="margin-bottom:2px;">
             <td style="width: 15%">
                
             </td>
             <td>
                 </td>
         </tr>
       
       
     </table>
     <%if (IViewState == "0")
         {%>
       <div style="margin-left: 500px;">
               <asp:Button  Text="Approve" runat="server" ID="btnApprove" style="margin-right:20px" CssClass="btn btn-primary"  OnClick="btnApprove_Click" ValidationGroup="ComplianceValidationGroup1" ></asp:Button>
               <asp:Button Text="Reject" runat="server" ID="btnReject" CssClass="btn btn-primary " OnClick="btnReject_Click" ValidationGroup="ComplianceValidationGroup1"></asp:Button>
         
         </div>
     <%} %>
     <div class="row" runat="server" id="divAdveditBilldocument" visible="true" style="margin-top:50px;margin-left: 168px;">
         <asp:GridView ID="grdAdvocateBillDocuments" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
             Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="grdAdvocateBillDocuments_RowCommand"
             PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
             <Columns>
                 <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                     <ItemTemplate>
                         <%#Container.DataItemIndex+1 %>
                     </ItemTemplate>
                 </asp:TemplateField>

                 <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="File Name" ItemStyle-Width="8%">
                     <ItemTemplate>
                         <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                             <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                 data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                         </div>
                     </ItemTemplate>
                 </asp:TemplateField>

                 <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                     <ItemTemplate>
                         <asp:UpdatePanel runat="server" ID="AdvBillDocDelete" UpdateMode="Always">
                             <ContentTemplate>
                                 <asp:LinkButton
                                     CommandArgument='<%# Eval("ID")%>' CommandName="DownloadAdvBillEditDocument"
                                     ID="lnkDownloadAdvBillEditDocument" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                 </asp:LinkButton>
                                 <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                     AutoPostBack="true" CommandName="ViewAdvBillEditDocument"
                                     ID="lnkViewAdvBillEditDocument" runat="server">
                                       <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                 </asp:LinkButton>
                                
                             </ContentTemplate>
                             <Triggers>
                               
                                 <asp:PostBackTrigger ControlID="lnkDownloadAdvBillEditDocument" />
                             </Triggers>
                         </asp:UpdatePanel>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
             <RowStyle CssClass="clsROWgrid" />
             <HeaderStyle CssClass="clsheadergrid" />
             <EmptyDataTemplate>
                 No Response Submitted yet.
             </EmptyDataTemplate>
         </asp:GridView>
     </div>

     <%-- Adv Bill Document pop up--%>
     <div>
         <div class="modal fade" id="DocumentAdvBillReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
             <div class="modal-dialog" style="width: 100%">
                 <div class="modal-content">
                     <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     </div>
                     <div class="modal-body" style="height: 570px;">
                         <div style="width: 100%;">
                             <div style="float: left; width: 10%">
                                 <table width="100%" style="text-align: left; margin-left: 5%;">
                                     <thead>
                                         <tr>
                                             <td valign="top">
                                                 <asp:UpdatePanel ID="UpdatePanel15" runat="server" UpdatleMode="Conditional">
                                                     <ContentTemplate>
                                                         <asp:Repeater ID="rptAdvBilldocumentview" runat="server" OnItemCommand="rptAdvBilldocumentview_ItemCommand"
                                                             OnItemDataBound="rptAdvBilldocumentview_ItemDataBound">
                                                             <HeaderTemplate>
                                                                 <table id="tblComplianceDocumnets">
                                                                     <thead>
                                                                         <th>File Name</th>
                                                                     </thead>
                                                             </HeaderTemplate>
                                                             <ItemTemplate>
                                                                 <tr>
                                                                     <td>
                                                                         <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                             <ContentTemplate>
                                                                                 <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("DocTypeInstanceID") + ","+ Eval("Version") + ","+ Eval("ID") %>' ID="lblAdvDocumentVersionView"
                                                                                     runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString().Substring(0,10) %>'></asp:LinkButton>
                                                                             </ContentTemplate>
                                                                             <Triggers>
                                                                                 <asp:AsyncPostBackTrigger ControlID="lblAdvDocumentVersionView" />
                                                                             </Triggers>
                                                                         </asp:UpdatePanel>
                                                                     </td>
                                                                 </tr>
                                                             </ItemTemplate>
                                                             <FooterTemplate>
                                                                 </table>
                                                             </FooterTemplate>
                                                         </asp:Repeater>
                                                     </ContentTemplate>
                                                     <Triggers>
                                                         <asp:AsyncPostBackTrigger ControlID="rptAdvBilldocumentview" />
                                                     </Triggers>
                                                 </asp:UpdatePanel>
                                             </td>
                                         </tr>
                                     </thead>
                                 </table>
                             </div>
                             <div style="float: right; width: 90%">
                                 <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                 <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                     <iframe src="about:blank" id="CaseAdvBillDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                 </fieldset>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <%--End Adv Bill Document pop up--%>
 </div>
    </form>
</body>
</html>
