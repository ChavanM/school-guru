﻿using AjaxControlToolkit;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;
using System.Globalization;
using System.Diagnostics;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon;
using Amazon.S3.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class NoticeDetailPage : System.Web.UI.Page
    {
        public static string DocumentPath = "";
        public static List<int> LawyerTypeList = new List<int>();
        public long CustomerID = AuthenticationHelper.CustomerID;
        public Boolean NewColumnsLitigation;
        public Boolean CompulsoaryPaymentFields;
        public static bool NoticeLabel;
        public static bool AuditScrutinyCustomerEnable;
        public bool RiskType;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            	NoticeLabel = CaseManagement.CheckForClient(Convert.ToInt32(CustomerID),"CaseNoticeLabel");
                NewColumnsLitigation = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "LitigationNewColumn");
                CompulsoaryPaymentFields= CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "LitigationPaymentCompulsoaryfield");
                AuditScrutinyCustomerEnable = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "AuditScrutinyCustomerEnable");
                RiskType = CaseManagement.CheckForClient(Convert.ToInt32(CustomerID), "RiskType");
                if (!IsPostBack)
                {
                    BindAct();
                    BindNoticeResult();
                    BindParty();
                    BindCustomerBranches();
                    BindDepartment();
                    BindUsers();
                    BindLawyer();
                    BindJurisdiction();
                    BindNoticeCategoryType();
                    BindNoticeStages();
                    BindState();
                    BindFY();
                    BindFinancialYear();
                    // BindLitigationDocType();
                    if (RiskType == true)
                    {
                        divRisk.Visible = true;
                    }
                    else
                    {
                        divRisk.Visible = false;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var noticeInstanceID = Request.QueryString["AccessID"];
                        if (noticeInstanceID != "")
                        {
                            ViewState["noticeInstanceID"] = noticeInstanceID;

                            if (Convert.ToInt32(noticeInstanceID) == 0)
                            {
                                lnkActDetails.Visible = false;

                                liDocument.Visible = false;
                                liNoticeTask.Visible = false;
                                liNoticeResponse.Visible = false;
                                liNoticeStatusPayment.Visible = false;
                                liLawyerRating.Visible = false;
                                liAuditLog.Visible = false;

                                btnAddNotice_Click(sender, e);  //Add Detail          
                               // bindAllLawfirmUser();
                                showHideButtons(false);
                            }
                            else
                            {
                                liDocument.Visible = true;
                                liNoticeTask.Visible = true;
                                liNoticeResponse.Visible = true;
                                liNoticeStatusPayment.Visible = true;
                                liLawyerRating.Visible = true;
                                liAuditLog.Visible = true;

                                btnEditNotice_Click(sender, e); //Edit Detail

                                showHideButtons(true);
                            }

                            lnkNoticeDetail_Click(sender, e);
                        }
                        List<int> ListOfUser = CaseManagement.UserCanCheckRating(Convert.ToInt32(noticeInstanceID), 2);
                        if (ListOfUser.Contains(AuthenticationHelper.UserID))
                        {
                            liLawyerRating.Visible = false;
                        }
                      
                    }
                    ViewState["ResponseMode"] = "Add";
                    ViewState["TaskMode"] = "Add";
                    ViewState["CustomefieldCount"] = null;
                    applyCSStoFileTag_ListItems();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                }

                ViewState["ListofFile"] = null;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                //Show Hide Grid Control - Enable/Disable Form Controls
                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                    {
                        showHideButtons(false);
                        enableDisableNoticePopUpControls(false);
                    }
                    else
                    {
                        showHideButtons(true);
                        enableDisableNoticePopUpControls(true);
                    }
                }
                else
                    enableDisableNoticePopUpControls(true);

                if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                {
                    enableDisableNoticePopUpControls(false);
                    showHideButtons(false);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Flagtab"]))
                {
                    string FlagActiveTab = Convert.ToString(Request.QueryString["Flagtab"]);

                    if (FlagActiveTab.Equals("H"))
                    {
                        lnkNoticeResponse_Click(sender, e);
                    }
                    else
                    {
                        lnkNoticeDetail_Click(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static object FillFnancialYear()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.FinancialYearDetails
                             where row.IsDeleted==false
                             select row).OrderByDescending(e => e.FinancialYear).ToList();


                return query;
            }
        }

        public void BindFinancialYear()
        {

            DropDownListChosen1.DataValueField = "Id";
            DropDownListChosen1.DataTextField = "FinancialYear";
            DropDownListChosen1.DataSource = FillFnancialYear();
            DropDownListChosen1.DataBind();
            //DropDownListChosen1.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        private void bindAllLawfirmUser()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);
            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in lstAllUsers
                             where row.IsDeleted == false
                             && row.IsActive == true
                             select row).ToList();

                if (query.Count > 0)
                {
                    query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID != null).ToList();
                }

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                lstBoxLawyerUser.DataTextField = "Name";
                lstBoxLawyerUser.DataValueField = "ID";
                lstBoxLawyerUser.DataSource = lstUsers;
                lstBoxLawyerUser.DataBind();
                lstBoxLawyerUser.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
            }
            // var lawyerUsers = LitigationUserManagement.GetRequiredUsersByLawFirm(lstAllUsers, 2, Convert.ToInt32(ddlLawFirm.SelectedValue));

            ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScript", "rebindLawyerUser();", true);
            UpdatePanel6.Update();
            if (ddlLawFirm.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "ShowAddButtonLawfirm", "ShowLawFirmAddbutton();", true);
            }
        }

        private void BindFY()
        {
            try
            {
                ddlFY.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select FY", "0"));
            }
            catch (Exception ex)
            {

            }
        }

        private void BindState()
        {
            try
            {
                var getallState = LitigationManagement.getAllSatatevalue();

                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = getallState;
                ddlState.DataBind();
                ddlState.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select State", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //private void BindLitigationDocType()
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //        var lstdocTypes = CaseManagement.GetLitigationDocTypes_All(customerID);

        //        ddlDocType.DataTextField = "Name";
        //        ddlDocType.DataValueField = "ID";

        //        ddlDocType.DataSource = lstdocTypes;
        //        ddlDocType.DataBind();
        //        ddlDocType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        #region Common
        protected void lnkActDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                {
                    var noticeInstanceID = Request.QueryString["AccessID"];
                    if (noticeInstanceID != "")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowNoticeDocumentDialog(" + noticeInstanceID + ",'N');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindAct()
        {
            var obj = LitigationLaw.GetAllAct();

            ddlAct.DataTextField = "Name";
            ddlAct.DataValueField = "ID";

            ddlAct.DataSource = obj;
            ddlAct.DataBind();

            ddlAct.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
        }

        private void BindNoticeResult()
        {
            try
            {
                var lstOrderType = LitigationManagement.GetAllType("Re");
                lstOrderType = lstOrderType.Where(entry => entry.customerID == null).ToList();
                ddlNoticeResult.DataTextField = "TypeName";
                ddlNoticeResult.DataValueField = "ID";

                ddlNoticeResult.DataSource = lstOrderType;
                ddlNoticeResult.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindNoticeCategoryType()
        {
            try
            {
                var lstNoticeCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);

                ddlNoticeCategory.DataTextField = "CaseType";
                ddlNoticeCategory.DataValueField = "ID";

                ddlNoticeCategory.DataSource = lstNoticeCaseType;
                ddlNoticeCategory.DataBind();

                ddlNoticeCategory.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindNoticeStages()
        {
            try
            {
                var lstNoticeCaseType = LitigationCourtAndCaseType.GetAllNoticeStagesData(CustomerID);

                ddlNoticeStage.DataTextField = "NoticeStage";
                ddlNoticeStage.DataValueField = "ID";

                ddlNoticeStage.DataSource = lstNoticeCaseType;
                ddlNoticeStage.DataBind();

                ddlNoticeStage.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Stage", "0"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int userid = -1;
                userid = AuthenticationHelper.UserID;
                int IsForBranch = UserManagement.ExistBranchAssignment(customerID);
                if (IsForBranch == 1)
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    // var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchyManagementSatutoryNew(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }
                    tbxBranch.Text = "Select Entity/Location";
                    List<TreeNode> nodes = new List<TreeNode>();

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var BranchList = (from row in entities.LitigationEntitiesAssignments
                                          where row.UserID == userid
                                          select (int)row.BranchID).ToList();

                        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                         join row1 in entities.tbl_LegalCaseInstance
                                                         on row.CaseInstanceID equals row1.ID
                                                         where row1.CustomerID == (int)customerID
                                                         && (row.UserID == userid || row1.OwnerID == userid || row1.CreatedBy == userid || BranchList.Contains(row1.CustomerBranchID))
                                                         select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                           join row1 in entities.tbl_LegalNoticeInstance
                                                           on row.NoticeInstanceID equals row1.ID
                                                           where row1.CustomerID == (int)customerID
                                                           && (row.UserID == userid || row1.OwnerID == userid || row1.CreatedBy == userid || BranchList.Contains(row1.CustomerBranchID))
                                                           select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                        var LocationList = LegalCasbranchlist.Select(a => a).ToList();

                        BindBranchesHierarchyNew(null, branch, nodes, LocationList);
                    }
                    
                       
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }

                    tvBranches.CollapseAll();
                }
                else
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    // var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }
                    tbxBranch.Text = "Select Entity/Location";
                    List<TreeNode> nodes = new List<TreeNode>();

                    BindBranchesHierarchy(null, branch, nodes);
                   
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }

                    tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }
        private void BindBranchesHierarchyNew(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes, List<int> branchlst)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, Convert.ToString(item.ID));
                        BindBranchesHierarchyNew(node, item, nodes, branchlst);
                        if (branchlst.Contains(item.ID) == true)
                        {
                            nodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, Convert.ToString(item.ID));
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";

            ddlDepartment.DataSource = obj;
            ddlDepartment.DataBind();


            ddlDepartment.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
        }


        private void BindJurisdiction()
        {
            try
            {
                var getallJurisdiction = LitigationManagement.getAllJurisdictionvalue();

                ddlJurisdiction.DataTextField = "Name";
                ddlJurisdiction.DataValueField = "ID";

                ddlJurisdiction.DataSource = getallJurisdiction;
                ddlJurisdiction.DataBind();

                ddlJurisdiction.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Jurisdiction", "0"));
            }
            catch (Exception ex)
            {

            }
        }
        public void BindLawyer()
        {
            UpdatePanel6.Update();
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            var obj = LawyerManagement.GetLawyerListForMapping(customerID);

            ddlLawFirm.DataTextField = "Name";
            ddlLawFirm.DataValueField = "ID";

            ddlLawFirm.DataSource = obj;
            ddlLawFirm.DataBind();
            ddlLawFirm.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
        }

        public void BindParty()
        {
            var obj = LitigationLaw.GetLCPartyDetails(CustomerID);

            //Drop-Down at Modal Pop-up
            ddlParty.DataTextField = "Name";
            ddlParty.DataValueField = "ID";

            ddlParty.DataSource = obj;
            ddlParty.DataBind();

            ddlParty.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
        }

        private void BindPaymentType(DropDownList ddl)
        {
            try
            {
                var PaymentMasterList = LitigationPaymentType.GetAllPaymentMasterList();

                ddl.DataValueField = "ID";
                ddl.DataTextField = "TypeName";

                ddl.DataSource = PaymentMasterList;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();

                var bothUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 0);

                ddlOwner.DataValueField = "ID";
                ddlOwner.DataTextField = "Name";
                ddlOwner.DataSource = bothUsers;
                ddlOwner.DataBind();



                ddlCPDepartment.DataValueField = "ID";
                ddlCPDepartment.DataTextField = "Name";
                ddlCPDepartment.DataSource = bothUsers;
                ddlCPDepartment.DataBind();

                ddlCPDepartment.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Contact Person", "0"));

                var internalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 1);

                lstBoxPerformer.DataTextField = "Name";
                lstBoxPerformer.DataValueField = "ID";

                lstBoxPerformer.DataSource = internalUsers;
                lstBoxPerformer.DataBind();

                ddlCPDepartment.DataValueField = "ID";
                ddlCPDepartment.DataTextField = "Name";

                ddlCPDepartment.DataSource = internalUsers;
                ddlCPDepartment.DataBind();

                ddlCPDepartment.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Contact Person", "0"));

                ddlReviewer.DataValueField = "ID";
                ddlReviewer.DataTextField = "Name";
                ddlReviewer.DataSource = internalUsers;
                ddlReviewer.DataBind();

                ddlTaskUserInternal.DataValueField = "ID";
                ddlTaskUserInternal.DataTextField = "Name";
                ddlTaskUserInternal.DataSource = internalUsers;
                ddlTaskUserInternal.DataBind();

                ddlTaskUserInternal.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", "0"));

                var lawyerUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);

                //Opposition Lawyer list 
                lstBoxOppositionLawyer.DataTextField = "Name";
                lstBoxOppositionLawyer.DataValueField = "ID";

                lstBoxOppositionLawyer.DataSource = lawyerUsers;
                lstBoxOppositionLawyer.DataBind();
                lstBoxOppositionLawyer.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                var lawyerAndExternalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 3);

                ddlTaskUserLawyerAndExternal.DataValueField = "ID";
                ddlTaskUserLawyerAndExternal.DataTextField = "Name";
                ddlTaskUserLawyerAndExternal.DataSource = lawyerAndExternalUsers;
                ddlTaskUserLawyerAndExternal.DataBind();

                ddlTaskUserLawyerAndExternal.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
                //ddlTaskUserLawyerAndExternal.Items.Add(new ListItem("Add New", "0"));
                //UpdatePanel1.Update();
                //lstAllUsers.Clear();
                //lstAllUsers = null;

                lstAllUsers.Clear();
                lstAllUsers = null;

                internalUsers.Clear();
                internalUsers = null;

                lawyerUsers.Clear();
                lawyerUsers = null;

                lawyerAndExternalUsers.Clear();
                lawyerAndExternalUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindNoticeAuditLogs()
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                    var lstAuditLogs = LitigationManagement.GetCaseNoticeAuditLog(customerID, noticeInstanceID, "N");

                    gvCaseAuditLog.DataSource = lstAuditLogs;
                    gvCaseAuditLog.DataBind();
                }
                else
                {
                    gvCaseAuditLog.DataSource = null;
                    gvCaseAuditLog.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //public void BindNoticeRelatedDocuments(int noticeInstanceID)
        //{
        //    try
        //    {
        //        int DocTypeID = -1;
        //        string TypeToSearch = string.Empty;
        //        string DisplayError = string.Empty;
        //        List<Sp_Litigation_CaseDocument_Result> lstNoticeDocs = new List<Sp_Litigation_CaseDocument_Result>();
        //        List<Sp_Litigation_CaseDocument_Result> CaseDataFiltered = new List<Sp_Litigation_CaseDocument_Result>();
        //        List<Sp_Litigation_CaseDocument_Result> CaseDataFinal = new List<Sp_Litigation_CaseDocument_Result>();
        //        lstNoticeDocs = NoticeManagement.GetNoticeDocumentMapping(noticeInstanceID, "");
        //        if (!string.IsNullOrEmpty(ddlDocType.SelectedValue))
        //        {
        //            if (ddlDocType.SelectedValue != "-1")
        //            {
        //                DocTypeID = Convert.ToInt32(ddlDocType.SelectedValue);
        //                lstNoticeDocs = lstNoticeDocs.Where(row => row.DocTypeID == DocTypeID).ToList();
        //            }
        //        }

        //        if (!string.IsNullOrEmpty(tbxFilter.Text.Trim()))
        //        {
        //            TypeToSearch = tbxFilter.Text;

        //            string[] FileTagsName = TypeToSearch.Trim().Split(',');

        //            if (FileTagsName.Length > 0)
        //            {
        //                for (int j = 0; j < FileTagsName.Length; j++)
        //                {
        //                    CaseDataFiltered = lstNoticeDocs.Where(entry => entry.FileTag.ToLower().Contains(((FileTagsName[j].Trim()).ToLower())) || entry.FileName.ToLower().Contains(((FileTagsName[j].Trim()).ToLower())) || entry.TypeName.ToLower().Contains(((FileTagsName[j].Trim()).ToLower()))).ToList();
        //                    foreach (var item in CaseDataFiltered)
        //                    {
        //                        CaseDataFinal.Add(item);
        //                    }
        //                    if (CaseDataFiltered.Count <= 0)
        //                    {
        //                        DisplayError += "," + FileTagsName[j].Trim();
        //                    }
        //                    CaseDataFiltered.Clear();
        //                }
        //            }
        //            if (CaseDataFinal.Count > 0)
        //            {
        //                CaseDataFinal = (from g in CaseDataFinal
        //                                 group g by new
        //                                 {
        //                                     g.ID,
        //                                     g.DocType,
        //                                     g.FileName,
        //                                     g.Version,
        //                                     g.CreatedByText,
        //                                     g.CreatedOn
        //                                 } into GCS
        //                                 select new Sp_Litigation_CaseDocument_Result()
        //                                 {
        //                                     ID = GCS.Key.ID,
        //                                     DocType = GCS.Key.DocType,
        //                                     FileName = GCS.Key.FileName,
        //                                     Version = GCS.Key.Version,
        //                                     CreatedByText = GCS.Key.CreatedByText,
        //                                     CreatedOn = GCS.Key.CreatedOn,
        //                                 }).ToList();

        //                if (!string.IsNullOrEmpty(DisplayError))
        //                {
        //                    DisplayError = DisplayError.TrimStart(',');
        //                    cvNoticeDocument.IsValid = false;
        //                    cvNoticeDocument.ErrorMessage = "Document does not exist with tag name, filename, document type" + " - " + DisplayError + ".";
        //                    vsNoticeDocument.CssClass = "alert alert-danger";
        //                }
        //                grdNoticeDocuments.DataSource = CaseDataFinal;
        //                grdNoticeDocuments.DataBind();
        //            }
        //            else
        //            {
        //                if (!string.IsNullOrEmpty(DisplayError))
        //                {
        //                    DisplayError = DisplayError.TrimStart(',');
        //                    cvNoticeDocument.IsValid = false;
        //                    cvNoticeDocument.ErrorMessage = "Document does not exist with tag name, filename, document type" + " - " + DisplayError + ".";
        //                    vsNoticeDocument.CssClass = "alert alert-danger";
        //                }
        //                grdNoticeDocuments.DataSource = null;
        //                grdNoticeDocuments.DataBind();
        //            }
        //        }
        //        else
        //        {
        //            if (lstNoticeDocs.Count > 0)
        //            {
        //                lstNoticeDocs = (from g in lstNoticeDocs
        //                                 group g by new
        //                                 {
        //                                     g.ID,
        //                                     g.DocType,
        //                                     g.FileName,
        //                                     g.Version,
        //                                     g.CreatedByText,
        //                                     g.CreatedOn
        //                                 } into GCS
        //                                 select new Sp_Litigation_CaseDocument_Result()
        //                                 {
        //                                     ID = GCS.Key.ID,
        //                                     DocType = GCS.Key.DocType,
        //                                     FileName = GCS.Key.FileName,
        //                                     Version = GCS.Key.Version,
        //                                     CreatedByText = GCS.Key.CreatedByText,
        //                                     CreatedOn = GCS.Key.CreatedOn,
        //                                 }).ToList();

        //                grdNoticeDocuments.DataSource = lstNoticeDocs;
        //                grdNoticeDocuments.DataBind();
        //            }
        //            else
        //            {
        //                grdNoticeDocuments.DataSource = null;
        //                grdNoticeDocuments.DataBind();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvNoticePopUp.IsValid = false;
        //        cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
        //        VSNoticePopup.CssClass = "alert alert-danger";
        //    }
        //}

        public void BindNoticeResponses(int noticeInstanceID)
        {
            try
            {
                List<tbl_LegalNoticeResponse> lstNoticeResponses = new List<tbl_LegalNoticeResponse>();

                lstNoticeResponses = NoticeManagement.GetNoticeResponseDetails(noticeInstanceID);


                grdResponseLog.DataSource = lstNoticeResponses;
                grdResponseLog.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpResponse.IsValid = false;
                cvNoticePopUpResponse.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary1.CssClass = "alert alert-danger";
            }
        }

        public void BindTaskResponses(int taskID, GridView grd)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grd.DataSource = lstTaskResponses;
                    grd.DataBind();
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        public void BindTasks(int noticeInstanceID)
        {
            try
            {
                var lstNoticeTasks = LitigationTaskManagement.GetTaskDetails(noticeInstanceID, "N");

                grdTaskActivity.DataSource = lstNoticeTasks;
                grdTaskActivity.DataBind();

                lstNoticeTasks.Clear();
                lstNoticeTasks = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                //ScriptManager.RegisterStartupScript(this.upNoticePopup, this.upNoticePopup.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        protected void lnkNoticeDetail_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "active");
            liDocument.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");
            liLawyerRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;

            if (ViewState["Mode"] != null)
            {
                if (Convert.ToInt32(ViewState["Mode"]) == 0)
                {
                    enableDisableNoticeSummaryTabControls(true);

                    btnSave.Visible = true;
                    btnSave.Text = "Save";
                    btnClearNoticeDetail.Visible = true;
                    btnEditNoticeDetail.Visible = false;
                    lnkActDetails.Visible = false;
                    btnSendMailPopup.Visible = false;
                }
                else if (Convert.ToInt32(ViewState["Mode"]) == 1)
                {
                    enableDisableNoticeSummaryTabControls(false);

                    btnSave.Visible = true;
                    btnSave.Text = "Update";
                    btnClearNoticeDetail.Visible = false;
                    btnEditNoticeDetail.Visible = true;
                    lnkActDetails.Visible = true;
                    btnSendMailPopup.Visible = true;
                }
            }
        }

        protected void TabDocument_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "active");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");
            liLawyerRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 1;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindNoticeRelatedDocuments_All(Convert.ToInt32(ViewState["noticeInstanceID"]));
                BindFileTags();
            }
        }

        protected void lnkNoticeTask_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "active");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");
            liLawyerRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 2;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
        }

        protected void lnkNoticeResponse_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "active");
            liNoticeStatusPayment.Attributes.Add("class", "");
            liLawyerRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 3;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
        }

        protected void lnkNoticeStatusPayment_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "active");
            liLawyerRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 4;
            if (ViewState["noticeInstanceID"] != null)
            {
                BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
            ViewState["PaymentMode"] = "Add";
        }

        protected void lnkLawyerRating_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");
            liLawyerRating.Attributes.Add("class", "active");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 5;

            if (!string.IsNullOrEmpty(Convert.ToString((ViewState["noticeInstanceID"]))))
            {
                int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                bool CheckUserForRating = CaseManagement.CheckUserIsInternal(AuthenticationHelper.UserID);

                if (CheckUserForRating)
                {
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var documentData = (from row in entities.sp_LiDisplayLawyerRatingForNotice(noticeInstanceID)
                                                select row).ToList();

                            var UserList = (from row in documentData
                                            select new { ID = row.LawyerID, Name = row.LawyerName }).OrderBy(entry => entry.Name).ToList<object>();

                            if (UserList.Count > 0)
                            {
                                grdLawyerRating.DataSource = null;
                                grdLawyerRating.DataBind();
                                ddlLayerType.DataValueField = "ID";
                                ddlLayerType.DataTextField = "Name";
                                ddlLayerType.DataSource = UserList;
                                ddlLayerType.DataBind();
                            }
                        }
                    }
                }
            }
        }

        public int ShowLawyerRating(decimal savedRating)
        {
            int returnRating = 0;
            try
            {
                returnRating = Convert.ToInt32(savedRating);
                return returnRating;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return returnRating;
            }
        }

        protected void lnkAuditLog_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");
            liLawyerRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 6;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindNoticeAuditLogs();
            }
        }

        #endregion

        //protected void ddlTaskLawyerList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //        List<object> lstUsers = new List<object>();

        //        if (!string.IsNullOrEmpty(ddlTaskLawyerList.SelectedValue) && ddlTaskLawyerList.SelectedValue != "0")
        //            lstUsers = LitigationUserManagement.GetLitigationUsersByLawyerID(customerID, Convert.ToInt32(ddlTaskLawyerList.SelectedValue));
        //        else
        //            lstUsers = LitigationUserManagement.GetLitigationUsers(customerID, 0);

        //        ddlTaskUser.Items.Clear();
        //        ddlTaskUser.DataValueField = "ID";
        //        ddlTaskUser.DataTextField = "Name";
        //        ddlTaskUser.DataSource = lstUsers;
        //        ddlTaskUser.DataBind();

        //        ddlTaskUser.Items.Add(new ListItem("Add New", "0"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvNoticePopUpTask.IsValid = false;
        //        cvNoticePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
        //    }
        //}

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    LitigationManagement.CreateAuditLog("N", taskID, "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Response Document(s) Downloaded", false);
                                }
                            }
                            else
                            {
                                cvNoticePopUpTask.IsValid = false;
                                cvNoticePopUpTask.ErrorMessage = "No Document Available for Download.";
                                ValidationSummary5.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]), grdTaskActivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        protected void grdTaskResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton)e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton)e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
            }
        }

        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Response Deleted Successfully.";
                        ValidationSummary5.CssClass = "alert alert-success";

                        LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_TaskResponse", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Response Deleted", true);
                    }
                    else
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        public void BindNoticePayments(int noticeInstanceID)
        {
            try
            {
                //List<tbl_NoticeCasePayment> lstNoticePayments = new List<tbl_NoticeCasePayment>();
                //lstNoticePayments = NoticeManagement.GetNoticeCasePaymentDetails(noticeInstanceID, "N");

                List<SP_Litigation_NoticeCasePayment_Result> lstNoticePayments = new List<SP_Litigation_NoticeCasePayment_Result>();
                lstNoticePayments = LitigationManagement.GetCasePaymentDetails(noticeInstanceID, "N");

                if (lstNoticePayments != null && lstNoticePayments.Count > 0)
                {
                    grdNoticePayment.DataSource = lstNoticePayments;
                    grdNoticePayment.DataBind();
                }
                else
                {
                    SP_Litigation_NoticeCasePayment_Result obj = new SP_Litigation_NoticeCasePayment_Result(); //initialize empty class that may contain properties
                    lstNoticePayments.Add(obj); //Add empty object to list

                    grdNoticePayment.DataSource = lstNoticePayments; /*Assign datasource to create one row with default values for the class you have*/
                    grdNoticePayment.DataBind(); //Bind that empty source                    

                    //To Hide row
                    grdNoticePayment.Rows[0].Visible = false;
                    grdNoticePayment.Rows[0].Controls.Clear();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        protected void btnClearNoticeControls_Click(object sender, EventArgs e)
        {
            try
            {
                clearNoticeControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearResponse_Click(object sender, EventArgs e)
        {
            try
            {
                clearResponseControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearTask_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditNoticeControls_Click(object sender, EventArgs e)
        {
            try
            {
                enableDisableNoticeSummaryTabControls(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnNoticeUpload_Click(object sender, EventArgs e)
        {

        }

        protected void btnAddNotice_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                enableDisableNoticeSummaryTabControls(true);

                enableDisableNoticePopUpControls(true);

                clearNoticeControls();

                btnSave.Enabled = true;

                btnSave.Visible = true;
                btnSave.Text = "Save";
                btnClearNoticeDetail.Visible = true;
                btnEditNoticeDetail.Visible = false;
                lnkActDetails.Visible = false;
                btnSendMailPopup.Visible = false;
                divLinkedCases.Visible = false;

                grdNoticeDocuments.DataSource = null;
                grdNoticeDocuments.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void btnEditNotice_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                    if (noticeInstanceID != 0)
                    {
                        ViewState["Mode"] = 1;

                        //if (CaseManagement.CheckUserIsInternalForNotice(AuthenticationHelper.UserID))
                        //{
                        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        //    {
                        //        var documentData = (from row in entities.sp_LiDisplayLawyerRatingForNotice(noticeInstanceID, AuthenticationHelper.UserID)
                        //                            select row).ToList();

                        //        if (documentData != null)
                        //        {
                        //            grdLawyerRating.DataSource = documentData;
                        //            grdLawyerRating.DataBind();
                        //        }
                        //    }
                        //}

                        var noticeRecord = NoticeManagement.GetNoticeByID(noticeInstanceID);

                        if (noticeRecord != null)
                        {
                            enableDisableNoticeSummaryTabControls(false);

                            btnSave.Visible = true;
                            btnSave.Text = "Update";
                            btnClearNoticeDetail.Visible = false;
                            btnEditNoticeDetail.Visible = true;
                            lnkActDetails.Visible = true;
                            btnSendMailPopup.Visible = true;

                            if (noticeRecord.NoticeType != null)
                            {
                                if (Convert.ToString(noticeRecord.NoticeType) == "I")
                                    rbNoticeInOutType.SelectedValue = "I";
                                else if (Convert.ToString(noticeRecord.NoticeType) == "O")
                                    rbNoticeInOutType.SelectedValue = "O";
                            }

                            tbxRefNo.Text = noticeRecord.RefNo;
                            if (noticeRecord.AuditScrutiny != null)
                            {
                                DropDownListAuditScrutiny.ClearSelection();

                                if (DropDownListAuditScrutiny.Items.FindByValue (noticeRecord.AuditScrutiny.ToString()) != null)
                                    DropDownListAuditScrutiny.SelectedValue =Convert.ToString(noticeRecord.AuditScrutiny);
                            }
                            tbxNoticeBudget.Text = Convert.ToString(noticeRecord.NoticeBudget);
                            //ddlNoticeResult.SelectedValue = Convert.ToString(noticeRecord.NoticeResult);
                            ddlNoticeStage.SelectedValue = noticeRecord.NoticeStage;
                            txtIntrest.Text = Convert.ToString(noticeRecord.Intrest);
                            txtPenalty.Text = Convert.ToString(noticeRecord.Penalty);
                            txtProvisonbook.Text = Convert.ToString(noticeRecord.ProvisioninBook);
                            txttaxDemand.Text = Convert.ToString(noticeRecord.Taxdemand);
                            ddlState.SelectedValue = Convert.ToString(noticeRecord.state);
                            ddlFY.SelectedValue = Convert.ToString(noticeRecord.Period);
                            tbxNoticeTerm.Text = Convert.ToString(noticeRecord.NoticeTerm);
                            txtRemark.Text = noticeRecord.Remark;
                            // DropDownListChosen1.SelectedValue = Convert.ToString(noticeRecord.FinancialYear);

                            if (noticeRecord.NoticeDate != null)
                                txtNoticeDate.Text = Convert.ToDateTime(noticeRecord.NoticeDate).ToString("dd-MM-yyyy");

                            tbxSection.Text = noticeRecord.Section;

                            if (noticeRecord.NoticeResult != null)
                            {
                                ddlNoticeResult.ClearSelection();

                                if (ddlNoticeResult.Items.FindByValue(Convert.ToString(noticeRecord.NoticeResult)) != null)
                                    ddlNoticeResult.SelectedValue = Convert.ToString(noticeRecord.NoticeResult);
                            }

                            if (noticeRecord.NoticeCategoryID != null)
                            {
                                ddlNoticeCategory.ClearSelection();

                                if (ddlNoticeCategory.Items.FindByValue(Convert.ToString(noticeRecord.NoticeCategoryID)) != null)
                                    ddlNoticeCategory.SelectedValue = Convert.ToString(noticeRecord.NoticeCategoryID);
                            }

                            tbxTitle.Text = noticeRecord.NoticeTitle;
                            tbxDescription.Text = noticeRecord.NoticeDetailDesc;
                            // DropDownListChosen1.SelectedValue = noticeRecord.FinancialYear;

                            if (noticeRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == Convert.ToString(noticeRecord.CustomerBranchID))
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == Convert.ToString(noticeRecord.CustomerBranchID))
                                            item1.Selected = true;
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);
                            if (noticeRecord.ContactPersonOfDepartment != null)
                            {
                                ddlJurisdiction.ClearSelection();

                                if (ddlJurisdiction.Items.FindByValue(Convert.ToString(noticeRecord.Jurisdiction)) != null)
                                    ddlJurisdiction.SelectedValue = Convert.ToString(noticeRecord.Jurisdiction);
                            }
                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(Convert.ToString(noticeRecord.DepartmentID)) != null)
                                ddlDepartment.SelectedValue = Convert.ToString(noticeRecord.DepartmentID);

                            if (noticeRecord.ContactPersonOfDepartment != null)
                            {
                                ddlCPDepartment.ClearSelection();

                                if (ddlCPDepartment.Items.FindByValue(Convert.ToString(noticeRecord.ContactPersonOfDepartment)) != null)
                                    ddlCPDepartment.SelectedValue = Convert.ToString(noticeRecord.ContactPersonOfDepartment);
                            }



                            if (noticeRecord.OwnerID != null)
                            {
                                ddlOwner.ClearSelection();

                                if (ddlOwner.Items.FindByValue(Convert.ToString(noticeRecord.OwnerID)) != null)
                                    ddlOwner.SelectedValue = Convert.ToString(noticeRecord.OwnerID);
                            }

                            if (noticeRecord.NoticeRiskID != null)
                            {
                                ddlNoticeRisk.ClearSelection();

                                if (ddlNoticeRisk.Items.FindByValue(noticeRecord.NoticeRiskID.ToString()) != null)
                                    ddlNoticeRisk.SelectedValue = noticeRecord.NoticeRiskID.ToString();
                            }
                            if (noticeRecord.RiskTypeID != null)
                            {
                                ddlRisk.ClearSelection();
                                if (ddlRisk.Items.FindByValue(noticeRecord.RiskTypeID.ToString()) != null)
                                    ddlRisk.SelectedValue = noticeRecord.RiskTypeID.ToString();
                            }

                            if (noticeRecord.ClaimAmt != null)
                                tbxClaimedAmt.Text = Convert.ToString(noticeRecord.ClaimAmt);
                            else
                                tbxClaimedAmt.Text = "";

                            if (noticeRecord.ProbableAmt != null)
                                tbxProbableAmt.Text = Convert.ToString(noticeRecord.ProbableAmt);
                            else
                                tbxProbableAmt.Text = "";

                            if (noticeRecord.ProtestMoney != null)
                                txtprotestmoney.Text = noticeRecord.ProtestMoney.ToString();
                            else
                                txtprotestmoney.Text = "";


                            if (noticeRecord.RecoveryAmount != null)
                                txtRecovery.Text = noticeRecord.RecoveryAmount.ToString();
                            else
                                txtRecovery.Text = "";

                            if (noticeRecord.Provisionalamt != null)
                                txtprovisionalamt.Text = noticeRecord.Provisionalamt.ToString();
                            else
                                txtprovisionalamt.Text = "";

                            if (noticeRecord.BankGurantee != null)
                                txtbankgurantee.Text = noticeRecord.BankGurantee;
                            else
                                txtbankgurantee.Text = "";

                            //Potential Impact
                            if (noticeRecord.ImpactType != null)
                            {
                                if (noticeRecord.ImpactType.ToString() == "M")
                                    rblPotentialImpact.SelectedValue = "M";
                                else if (noticeRecord.ImpactType.ToString() == "N")
                                    rblPotentialImpact.SelectedValue = "N";
                                else if (noticeRecord.ImpactType.ToString() == "B")
                                    rblPotentialImpact.SelectedValue = "B";
                            }

                            if (noticeRecord.Monetory != null)
                                tbxMonetory.Text = noticeRecord.Monetory.ToString();
                            else
                                tbxMonetory.Text = "";

                            if (noticeRecord.NonMonetory != null)
                                tbxNonMonetory.Text = noticeRecord.NonMonetory.ToString();
                            else
                                tbxNonMonetory.Text = "";

                            //if (noticeRecord.FinancialYear != null)
                            //    DropDownListChosen1.SelectedValue = noticeRecord.FinancialYear.ToString();
                            //else
                            //    DropDownListChosen1.SelectedValue = "";

                            if (noticeRecord.Years != null)
                                tbxNonMonetoryYears.Text = noticeRecord.Years.ToString();
                            else
                                tbxNonMonetoryYears.Text = "";

                            //Get Lawyer Mapping
                            var lstNoticeLawyer = NoticeManagement.GetNoticeLawyerMapping(noticeInstanceID);

                            if (lstNoticeLawyer != null)
                            {
                                ddlLawFirm.ClearSelection();
                                ddlLawFirm.SelectedValue = Convert.ToString(lstNoticeLawyer.LawyerID);
                            }
                            #region Financial Year
                            var ListofFY = CaseManagement.GetListOfFY(noticeInstanceID, 2);
                            DropDownListChosen1.ClearSelection();
                            if (DropDownListChosen1.Items.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(ListofFY.ToString()))
                                {
                                    foreach (var item in ListofFY)
                                    {
                                        if (DropDownListChosen1.Items.FindByValue(item.FYID.ToString()) != null)
                                            DropDownListChosen1.Items.FindByValue(item.FYID.ToString()).Selected = true;
                                    }
                                }
                            }
                            #endregion

                            #region Act
                            var ListofAct = CaseManagement.GetListOfAct(noticeInstanceID, 2);
                            ddlAct.ClearSelection();
                            if (ddlAct.Items.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ListofAct)))
                                {
                                    foreach (var item in ListofAct)
                                    {
                                        if (ddlAct.Items.FindByValue(item.ActID.ToString()) != null)
                                            ddlAct.Items.FindByValue(item.ActID.ToString()).Selected = true;
                                    }
                                }
                            }
                            #endregion

                            #region Opponent
                            var ListofParty = CaseManagement.GetListOfParty(noticeInstanceID, 2);
                            ddlParty.ClearSelection();
                            if (ddlParty.Items.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ListofParty)))
                                {
                                    foreach (var item in ListofParty)
                                    {
                                        if (ddlParty.Items.FindByValue(item.PartyID.ToString()) != null)
                                            ddlParty.Items.FindByValue(item.PartyID.ToString()).Selected = true;
                                    }
                                }
                            }
                            #endregion

                            #region Opposition Lawyer
                            var ListofOppoLawyer = CaseManagement.GetListOfOppoLaywer(noticeInstanceID, 2);
                            lstBoxOppositionLawyer.ClearSelection();
                            if (lstBoxOppositionLawyer.Items.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ListofOppoLawyer)))
                                {
                                    foreach (var item in ListofOppoLawyer)
                                    {
                                        if (lstBoxOppositionLawyer.Items.FindByValue(item.LawyerID.ToString()) != null)
                                            lstBoxOppositionLawyer.Items.FindByValue(item.LawyerID.ToString()).Selected = true;
                                    }
                                }
                            }
                            #endregion

                            //Get Notice Assignment
                            BindNoticeUserAssignments(noticeInstanceID);

                            ddlLawFirm_SelectedIndexChanged(sender, e);
                            var lstNoticeAssignment = NoticeManagement.GetNoticeAssignment(noticeInstanceID);

                            if (lstNoticeAssignment.Count > 0)
                            {

                                lstBoxPerformer.ClearSelection();
                                ddlReviewer.ClearSelection();
                                lstBoxLawyerUser.ClearSelection();

                                foreach (var eachAssignmentRecord in lstNoticeAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                        if (lstBoxLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                    }
                                    else if (eachAssignmentRecord.RoleID == 4)
                                    {
                                        if (ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }

                            //Notice Status 
                            var StatusDetails = NoticeManagement.GetNoticeStatusDetail(noticeInstanceID);

                            if (StatusDetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.TxnStatusID)))
                                {
                                    ddlNoticeStatus.ClearSelection();

                                    if (ddlNoticeStatus.Items.FindByValue(StatusDetails.TxnStatusID.ToString()) != null)
                                        ddlNoticeStatus.Items.FindByValue(StatusDetails.TxnStatusID.ToString()).Selected = true;

                                    ViewState["noticeStatus"] = Convert.ToInt32(StatusDetails.TxnStatusID);
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.CloseDate)))
                                {
                                    tbxNoticeCloseDate.Text = Convert.ToDateTime(StatusDetails.CloseDate).ToString("MM-dd-yyyy");
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.ClosureRemark)))
                                {
                                    tbxCloseRemark.Text = StatusDetails.ClosureRemark;
                                }
                            }
                            //Notice Status Log--End

                            //Bind Notice Related Documents
                            //BindNoticeRelatedDocuments(noticeInstanceID);

                            //Bind Custom Field
                            if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && CustomerID != 76)
                            {
                                BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                            }
                            else
                            {
                                BindCustomFields(grdCustomField, grdCustomField_History);
                            }

                            //Bind Linked Notice(s) if(Any)
                            BindLinkedNotices(Convert.ToInt64(noticeInstanceID));

                            //Bind Case To Link
                            BindNoticeListToLink(Convert.ToInt64(noticeInstanceID));

                            //Bind Notice Response Details
                            BindNoticeResponses(noticeInstanceID);

                            //Bind Notice Action Details
                            BindTasks(noticeInstanceID);

                            //Bind Notice Payment Details
                            BindNoticePayments(noticeInstanceID);

                            BindMailDocumentList(noticeInstanceID);
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void btnSaveNotice_Click(object sender, EventArgs e)
        {
            try
            {
                bool formValidateSuccess = false;
                bool saveSuccess = false;
                int SelectPartylist = 0;
                int SelectActList = 0;
                int selectedUserCount = 0;
                List<int> CheckAssingedUserOld = new List<int>();
                List<int> CheckAssingedUserNew = new List<int>();
                List<long> UpdatedAssingedUser = new List<long>();
                string AssignedUserList = string.Empty;

                #region Data Validation

                if (rbNoticeInOutType.SelectedValue != "")
                {
                    if (!String.IsNullOrEmpty(tbxRefNo.Text))
                    {
                        if (DropDownListChosen1.SelectedValue != "")
                        {
                            if (txtNoticeDate.Text != "")
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachParty in ddlParty.Items)
                                {
                                    if (eachParty.Selected)
                                        SelectPartylist++;
                                }
                                if (SelectPartylist > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachAct in ddlAct.Items)
                                    {
                                        if (eachAct.Selected)
                                            SelectActList++;
                                    }
                                    if (SelectActList > 0)
                                    {
                                        if (tbxTitle.Text != "")
                                        {
                                            if (tbxDescription.Text != "")
                                            {
                                                if (tvBranches.SelectedValue != "" && tvBranches.SelectedValue != "-1")
                                                {
                                                    if (ddlDepartment.SelectedValue != "" && ddlDepartment.SelectedValue != "-1")
                                                    {
                                                        if (ddlOwner.SelectedValue != "" && ddlOwner.SelectedValue != "-1")
                                                        {
                                                            foreach (System.Web.UI.WebControls.ListItem eachPerformer in lstBoxPerformer.Items)
                                                            {
                                                                if (eachPerformer.Selected)
                                                                    selectedUserCount++;
                                                            }
                                                            foreach (System.Web.UI.WebControls.ListItem eachLawyerUser in lstBoxLawyerUser.Items)
                                                            {
                                                                if (eachLawyerUser.Selected)
                                                                    selectedUserCount++;
                                                            }
                                                            if (selectedUserCount > 0)
                                                            {
                                                                //if (!string.IsNullOrEmpty(ddlLawFirm.SelectedValue))
                                                                //{
                                                                formValidateSuccess = true;
                                                                //}
                                                                //else
                                                                //{
                                                                //    cvNoticePopUp.IsValid = false;
                                                                //    cvNoticePopUp.ErrorMessage = "Select Law-Firm.";
                                                                //    VSNoticePopup.CssClass = "alert alert-danger";
                                                                //}
                                                            }
                                                            else
                                                            {
                                                                cvNoticePopUp.IsValid = false;
                                                                cvNoticePopUp.ErrorMessage = "Select User to Assign.";
                                                                VSNoticePopup.CssClass = "alert alert-danger";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvNoticePopUp.IsValid = false;
                                                            cvNoticePopUp.ErrorMessage = "Select Owner.";
                                                            VSNoticePopup.CssClass = "alert alert-danger";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvNoticePopUp.IsValid = false;
                                                        cvNoticePopUp.ErrorMessage = "Select Department.";
                                                        VSNoticePopup.CssClass = "alert alert-danger";
                                                    }
                                                }
                                                else
                                                {
                                                    cvNoticePopUp.IsValid = false;
                                                    cvNoticePopUp.ErrorMessage = "Select Entity/Location.";
                                                    VSNoticePopup.CssClass = "alert alert-danger";
                                                }
                                            }
                                            else
                                            {
                                                cvNoticePopUp.IsValid = false;
                                                cvNoticePopUp.ErrorMessage = "Please Provide Notice Description.";
                                                VSNoticePopup.CssClass = "alert alert-danger";
                                            }
                                        }
                                        else
                                        {
                                            cvNoticePopUp.IsValid = false;
                                            cvNoticePopUp.ErrorMessage = "Please Provide Notice Title.";
                                            VSNoticePopup.CssClass = "alert alert-danger";
                                        }
                                    }
                                    else
                                    {
                                        cvNoticePopUp.IsValid = false;
                                        cvNoticePopUp.ErrorMessage = "Select Act.";
                                        VSNoticePopup.CssClass = "alert alert-danger";
                                    }
                                }
                                else
                                {
                                    cvNoticePopUp.IsValid = false;
                                    cvNoticePopUp.ErrorMessage = "Select Party.";
                                    VSNoticePopup.CssClass = "alert alert-danger";
                                }
                            }
                            else
                            {
                                cvNoticePopUp.IsValid = false;
                                cvNoticePopUp.ErrorMessage = "Provide Notice Date.";
                                VSNoticePopup.CssClass = "alert alert-danger";
                            }
                        }
                        else
                        {
                            cvNoticePopUp.IsValid = false;
                            cvNoticePopUp.ErrorMessage = "Provide Financial Year.";
                            VSNoticePopup.CssClass = "alert alert-danger";
                        }
                    }
                    else
                    {
                        cvNoticePopUp.IsValid = false;
                        cvNoticePopUp.ErrorMessage = "Provide Refernce NO.";
                        VSNoticePopup.CssClass = "alert alert-danger";
                    }
                }
                else
                {
                    cvNoticePopUp.IsValid = false;
                    cvNoticePopUp.ErrorMessage = "Select Notice Type.";
                    VSNoticePopup.CssClass = "alert alert-danger";
                }
                if (NoticeLabel == true)
                {

                }
                else
                {
                    if (ddlNoticeRisk.SelectedValue != "" && ddlNoticeRisk.SelectedValue != "-1")
                    {

                    }
                    else
                    {
                        //  selectedUserCount++;
                        formValidateSuccess = false;
                        cvNoticePopUp.IsValid = false;
                        cvNoticePopUp.ErrorMessage = "Select Winning Prospect.";
                        VSNoticePopup.CssClass = "alert alert-danger";
                    }
                }
                if (RiskType == true)
                {
                    if (ddlRisk.SelectedValue != "" && ddlRisk.SelectedValue != "-1")
                    {

                    }
                    else
                    {
                        //  selectedUserCount++;
                        formValidateSuccess = false;
                        cvNoticePopUp.IsValid = false;
                        cvNoticePopUp.ErrorMessage = "Select Risk.";
                        VSNoticePopup.CssClass = "alert alert-danger";
                    }
                }
                else
                {
                  
                }


                #endregion

                #region Save/Edit Code

                if (formValidateSuccess)
                {
                    long newNoticeID = 0;

                    List<int> lstNoticeLawyer = new List<int>();
                    List<int> lstNoticePerformerUser = new List<int>();
                    List<int> lstPartyMapping = new List<int>();
                    List<int> lstActMapping = new List<int>();
                    List<int> lstOppoLawyerMapping = new List<int>();
                    List<string> lstFinancialYearMapping = new List<string>();
                    if (!(ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim())))
                    {
                        txtIntrest.Text = "";
                        txtPenalty.Text = "";
                        txtProvisonbook.Text = "";
                        txttaxDemand.Text = "";
                        ddlFY.SelectedValue = "0";
                    }
                    if (txttaxDemand.Text == "")
                    {
                        txttaxDemand.Text = "0";
                    }
                    if (txtIntrest.Text == "")
                    {
                        txtIntrest.Text = "0";
                    }

                    if (txtPenalty.Text == "")
                    {
                        txtPenalty.Text = "0";
                    }
                    if (ddlCPDepartment.SelectedValue == "")
                    {
                        ddlCPDepartment.SelectedValue = "0";
                    }
                    if (ddlState.SelectedValue == "")
                    {
                        ddlState.SelectedValue = "0";
                    }
                    if (ddlFY.SelectedValue == "")
                    {
                        ddlFY.SelectedValue = "0";
                    }
                    int? jrdiction = null;
                    if (!string.IsNullOrEmpty(ddlJurisdiction.SelectedValue))
                    {
                        jrdiction = Convert.ToInt32(ddlJurisdiction.SelectedValue);
                    }
                    tbl_LegalNoticeInstance newNotice = new tbl_LegalNoticeInstance()
                    {
                        IsDeleted = false,
                        NoticeType = rbNoticeInOutType.SelectedValue,
                        RefNo = tbxRefNo.Text.Trim(),
                        NoticeDate = DateTimeExtensions.GetDate(txtNoticeDate.Text),
                        Section = tbxSection.Text.Trim(),
                        NoticeCategoryID = Convert.ToInt32(ddlNoticeCategory.SelectedValue),
                        NoticeTitle = tbxTitle.Text.Trim(),
                        NoticeDetailDesc = tbxDescription.Text.Trim(),
                        CustomerBranchID = Convert.ToInt32(tvBranches.SelectedValue),
                        Jurisdiction = jrdiction,
                        DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                        ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue),
                        OwnerID = Convert.ToInt32(ddlOwner.SelectedValue),
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedBy = AuthenticationHelper.UserID,
                        CustomerID = AuthenticationHelper.CustomerID,
                        Period = ddlFY.SelectedValue,
                        Taxdemand = Convert.ToDecimal(txttaxDemand.Text),
                        Intrest = Convert.ToDecimal(txtIntrest.Text),
                        Penalty = Convert.ToDecimal(txtPenalty.Text),
                        ProvisioninBook = txtProvisonbook.Text,
                        state = Convert.ToInt32(ddlState.SelectedValue),
                        Remark=txtRemark.Text,
                        RiskTypeID=Convert.ToInt32(ddlRisk.SelectedValue)
                        
                        // FinancialYear = Convert.ToString(DropDownListChosen1.SelectedValue),

                    };
                    if (DropDownListAuditScrutiny.SelectedValue != "")
                    {
                        newNotice.AuditScrutiny = DropDownListAuditScrutiny.SelectedValue;
                    }

                    if (!string.IsNullOrEmpty(tbxNoticeBudget.Text))
                    {
                        newNotice.NoticeBudget = Convert.ToDecimal(tbxNoticeBudget.Text);
                    }
                    else
                    {
                        newNotice.NoticeBudget = 0;
                    }
                    if (!string.IsNullOrEmpty(tbxNoticeTerm.Text))
                    {
                        newNotice.NoticeTerm = Convert.ToInt32(tbxNoticeTerm.Text);
                    }
                    else
                    {
                        newNotice.NoticeTerm = 0;
                    }

                    if (ddlNoticeRisk.SelectedValue != "" && ddlNoticeRisk.SelectedValue != "-1")
                        newNotice.NoticeRiskID = Convert.ToInt32(ddlNoticeRisk.SelectedValue);

                    if (tbxClaimedAmt.Text != "")
                        newNotice.ClaimAmt = Convert.ToDecimal(tbxClaimedAmt.Text.Trim());

                    if (tbxProbableAmt.Text != "")
                        newNotice.ProbableAmt = Convert.ToDecimal(tbxProbableAmt.Text.Trim());

                    if (txtprovisionalamt.Text != "")
                        newNotice.Provisionalamt = Convert.ToDecimal(txtprovisionalamt.Text.Trim());

                    if (txtbankgurantee.Text != "")
                        newNotice.BankGurantee = txtbankgurantee.Text;

                    if (txtprotestmoney.Text != "")
                        newNotice.ProtestMoney = Convert.ToDecimal(txtprotestmoney.Text.Trim());

                    if (txtRecovery.Text != "")
                        newNotice.RecoveryAmount = Convert.ToDecimal(txtRecovery.Text.Trim());

                    if (rblPotentialImpact.SelectedValue != "" && rblPotentialImpact.SelectedValue != "-1")
                        newNotice.ImpactType = rblPotentialImpact.SelectedValue;

                    if (tbxMonetory.Text != "")
                        newNotice.Monetory = tbxMonetory.Text;

                    if (tbxNonMonetory.Text != "")
                        newNotice.NonMonetory = tbxNonMonetory.Text;

                    if (tbxNonMonetoryYears.Text != "")
                        newNotice.Years = tbxNonMonetoryYears.Text;

                    //if (DropDownListChosen1.SelectedValue != "-1")
                    //    newNotice.FinancialYear = DropDownListChosen1.SelectedValue;

                    //if (ddlPerformer.SelectedValue != "" && ddlPerformer.SelectedValue != "-1")
                    if (selectedUserCount > 0)
                        newNotice.AssignmentType = 1;

                    if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                        newNotice.AssignmentType = 2;

                    if ((int)ViewState["Mode"] == 0)
                    {
                        bool existNoticeRefNo = NoticeManagement.ExistsNoticeRefNo(tbxRefNo.Text, 0);

                        if (!existNoticeRefNo) //Same Notice Reference Number Previously not exists
                        {
                            if (!NoticeManagement.ExistsNotice(newNotice.NoticeTitle, 0))
                            {
                                newNoticeID = NoticeManagement.CreateNotice(newNotice);

                                if (newNoticeID > 0)
                                {
                                    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Notice Created", true);
                                    saveSuccess = true;
                                }
                            }
                            else
                            {
                                cvNoticePopUp.IsValid = false;
                                cvNoticePopUp.ErrorMessage = "Notice with Same Title already Exists.";
                                VSNoticePopup.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                        else
                        {
                            cvNoticePopUp.IsValid = false;
                            cvNoticePopUp.ErrorMessage = "Notice with Same Reference No. already exists";
                            VSNoticePopup.CssClass = "alert alert-danger";
                            tbxRefNo.Focus();
                        }

                        if (saveSuccess)
                        {
                            #region Save Party Mapping

                            if (ddlParty.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachParty in ddlParty.Items)
                                {
                                    if (eachParty.Selected)
                                        lstPartyMapping.Add(Convert.ToInt32(eachParty.Value));
                                }
                            }

                            if (lstPartyMapping.Count > 0)
                            {
                                List<tbl_PartyMapping> lstObjPartyMapping = new List<tbl_PartyMapping>();

                                lstPartyMapping.ForEach(EachParty =>
                                {
                                    tbl_PartyMapping objPartyMapping = new tbl_PartyMapping()
                                    {
                                        CaseNoticeInstanceID = newNoticeID,
                                        IsActive = true,
                                        Type = 2,//1 as Case and 2 as Notice
                                        PartyID = Convert.ToInt32(EachParty),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjPartyMapping.Add(objPartyMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdatePartyMapping(lstObjPartyMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_PartyMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Party Mapped", true);
                                }
                                //Refresh List
                                lstObjPartyMapping.Clear();
                                lstObjPartyMapping = null;

                                lstPartyMapping.Clear();
                                lstPartyMapping = null;
                            }
                            #endregion

                            #region Save Act Mapping

                            if (ddlAct.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachAct in ddlAct.Items)
                                {
                                    if (eachAct.Selected)
                                        lstActMapping.Add(Convert.ToInt32(eachAct.Value));
                                }
                            }

                            if (lstActMapping.Count > 0)
                            {
                                List<tbl_ActMapping> lstObjActMapping = new List<tbl_ActMapping>();

                                lstActMapping.ForEach(EachParty =>
                                {
                                    tbl_ActMapping objActMapping = new tbl_ActMapping()
                                    {
                                        CaseNoticeInstanceID = newNoticeID,
                                        IsActive = true,
                                        Type = 2,//1 as Case and 2 as Notice
                                        ActID = Convert.ToInt32(EachParty),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjActMapping.Add(objActMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateActMapping(lstObjActMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_ActMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Act Mapped", true);
                                }
                                //Refresh List
                                lstObjActMapping.Clear();
                                lstObjActMapping = null;

                                lstActMapping.Clear();
                                lstActMapping = null;
                            }
                            #endregion

                            #region Save Financial Year Mappping
                            if (DropDownListChosen1.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachFY in DropDownListChosen1.Items)
                                {
                                    if (eachFY.Selected)
                                        lstFinancialYearMapping.Add(Convert.ToString(eachFY.Value));
                                }
                            }

                            if (lstFinancialYearMapping.Count > 0)
                            {
                                List<FinancialYearMapping> lstObjFYMapping = new List<FinancialYearMapping>();

                                lstFinancialYearMapping.ForEach(EachFYID =>
                                {
                                    FinancialYearMapping objFYMapping = new FinancialYearMapping()
                                    {
                                        FYID = Convert.ToString(EachFYID),
                                        Type = 2,//1 as Case and 2 as Notice
                                        CaseNoticeInstanceID = newNoticeID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjFYMapping.Add(objFYMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateFYMapping(lstObjFYMapping);

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", newNoticeID, "FinancialYearMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Financial Year Mapped.", false);
                                }
                                //Refresh List
                                lstObjFYMapping.Clear();
                                lstObjFYMapping = null;

                                lstFinancialYearMapping.Clear();
                                lstFinancialYearMapping = null;
                            }
                            #endregion

                            #region Save Opposition Lawyer Mapping

                            if (lstBoxOppositionLawyer.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem LawyerList in lstBoxOppositionLawyer.Items)
                                {
                                    if (LawyerList.Selected)
                                        lstOppoLawyerMapping.Add(Convert.ToInt32(LawyerList.Value));
                                }
                            }

                            if (lstOppoLawyerMapping.Count > 0)
                            {
                                List<tbl_OppositionLawyerList> lstObjLawyerMapping = new List<tbl_OppositionLawyerList>();

                                lstOppoLawyerMapping.ForEach(EachParty =>
                                {
                                    tbl_OppositionLawyerList objLawyerMapping = new tbl_OppositionLawyerList()
                                    {
                                        CaseNoticeInstanceID = newNoticeID,
                                        IsActive = true,
                                        Type = 2,//1 as Case and 2 as Notice
                                        LawyerID = Convert.ToInt32(EachParty),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjLawyerMapping.Add(objLawyerMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateOppositionLawyerMapping(lstObjLawyerMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_OppositionLawyerList", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Opposition Lawyer Mapped", true);
                                }
                                //Refresh List
                                lstOppoLawyerMapping.Clear();
                                lstOppoLawyerMapping = null;

                                lstObjLawyerMapping.Clear();
                                lstObjLawyerMapping = null;
                            }
                            #endregion

                            //Notice Status Transaction
                            #region Status Transaction

                            tbl_LegalNoticeStatusTransaction newStatusRecord = new tbl_LegalNoticeStatusTransaction()
                            {
                                NoticeInstanceID = newNoticeID,
                                StatusID = 1,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false,
                                UserID = AuthenticationHelper.UserID,
                                RoleID = 3,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusRecord))
                                saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusRecord);

                            #endregion

                            //Lawyer Mapping
                            #region Lawyer Mapping
                            if (ddlLawFirm.SelectedValue == "")
                            {
                                ddlLawFirm.SelectedValue = "0";
                            }
                            tbl_LegalNoticeLawyerMapping objNoticeLawyerMapping = new tbl_LegalNoticeLawyerMapping()
                            {
                                NoticeInstanceID = newNoticeID,
                                IsActive = true,
                                LawyerID = Convert.ToInt32(ddlLawFirm.SelectedValue),
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            saveSuccess = NoticeManagement.CreateNoticeLawyerMapping(objNoticeLawyerMapping);
                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeLawyerMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Lawyer Mapped", true);
                            }

                            #endregion

                            #region Save Custom Field

                            //if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                            //{
                            //Select Which Grid to Loop based on Selected Category/Type
                            GridView gridViewToCollectData = null;

                            if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                            {
                                gridViewToCollectData = grdCustomField_TaxLitigation;
                            }
                            else
                            {
                                gridViewToCollectData = grdCustomField;
                            }

                            if (gridViewToCollectData != null)
                            {
                                for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                {
                                    Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                    TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                    if (lblID != null && tbxLabelValue != null)
                                    {
                                        if (!string.IsNullOrEmpty(lblID.Text))
                                        {
                                            if (lblID.Text != "0")
                                            {
                                                tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                {
                                                    NoticeCaseType = 2,
                                                    NoticeCaseInstanceID = newNoticeID,

                                                    LabelID = Convert.ToInt32(lblID.Text),
                                                    LabelValue = tbxLabelValue.Text,

                                                    IsDeleted = false,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                                {
                                                    TextBox tbxInterestValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxInterestValue");
                                                    TextBox tbxPenaltyValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxPenaltyValue");
                                                    TextBox tbxTotalValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxRowTotalValue");
                                                    TextBox tbxProvisionInbooks = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxProvisionInbooks");
                                                    TextBox tbxSettlementValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxSettlement");

                                                    if (tbxInterestValue != null && tbxPenaltyValue != null && tbxTotalValue != null
                                                        && tbxProvisionInbooks != null && tbxSettlementValue != null)
                                                    {
                                                        if (tbxInterestValue.Text != "")
                                                            ObjParameter.Interest = tbxInterestValue.Text;

                                                        if (tbxPenaltyValue.Text != "")
                                                            ObjParameter.Penalty = tbxPenaltyValue.Text;

                                                        if (tbxTotalValue.Text != "")
                                                            ObjParameter.Total = tbxTotalValue.Text;

                                                        if (tbxSettlementValue.Text != "")
                                                            ObjParameter.SettlementValue = tbxSettlementValue.Text;

                                                        if (tbxProvisionInbooks.Text != "")
                                                            ObjParameter.ProvisionInBook = tbxProvisionInbooks.Text;
                                                    }
                                                }

                                                ObjParameter.CreatedBy = AuthenticationHelper.UserID;
                                                ObjParameter.CreatedOn = DateTime.Now;

                                                ObjParameter.UpdatedBy = AuthenticationHelper.UserID;
                                                ObjParameter.UpdatedOn = DateTime.Now;

                                                saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);
                                            } //ID-0 Check
                                        }
                                    }
                                }//End For Each
                            }

                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_NoticeCaseCustomParameter", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Custom Parameter(s) Saved.", true);
                            }
                            //}

                            #endregion

                            //User Assignment
                            #region User Assignment

                            //De-Active All Previous Assignment
                            NoticeManagement.DeActivenoticeAssignments(newNoticeID, AuthenticationHelper.UserID);

                            if (lstBoxPerformer.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachPerformer in lstBoxPerformer.Items)
                                {
                                    if (eachPerformer.Selected)
                                    {
                                        lstNoticePerformerUser.Add(Convert.ToInt32(eachPerformer.Value));
                                        AssignedUserList += "," + eachPerformer.Text;
                                    }
                                }
                                if (lstNoticePerformerUser.Count > 0)
                                {
                                    lstNoticePerformerUser.ForEach(EachPerformer =>
                                    {
                                        tbl_LegalNoticeAssignment newAssignment = new tbl_LegalNoticeAssignment()
                                        {
                                            AssignmentType = newNotice.AssignmentType,
                                            NoticeInstanceID = newNoticeID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            IsLawyer = false,
                                            UserID = Convert.ToInt32(EachPerformer),
                                            RoleID = 3,
                                        };

                                        saveSuccess = NoticeManagement.CreateUpdateNoticeAssignment(newAssignment);

                                        //if (!NoticeManagement.ExistNoticeAssignment(newAssignment))
                                        //{
                                        //    saveSuccess = NoticeManagement.CreateNoticeAssignment(newAssignment);
                                        //    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Created", true);
                                        //}
                                        //else
                                        //{
                                        //    saveSuccess = NoticeManagement.UpdateNoticeAssignments(newAssignment);
                                        //    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Updated", true);
                                        //}
                                    });

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Created", true);
                                    }
                                }
                            }

                            lstNoticePerformerUser.Clear();

                            if (lstBoxLawyerUser.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachLawyerUser in lstBoxLawyerUser.Items)
                                {
                                    if (eachLawyerUser.Selected)
                                    {
                                        lstNoticePerformerUser.Add(Convert.ToInt32(eachLawyerUser.Value));
                                        AssignedUserList += "," + eachLawyerUser.Text;
                                    }
                                }
                                if (lstNoticePerformerUser.Count > 0)
                                {
                                    lstNoticePerformerUser.ForEach(EachPerformer =>
                                    {
                                        tbl_LegalNoticeAssignment newAssignment = new tbl_LegalNoticeAssignment()
                                        {
                                            AssignmentType = newNotice.AssignmentType,
                                            NoticeInstanceID = newNoticeID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            IsLawyer = true,
                                            UserID = Convert.ToInt32(EachPerformer),
                                            RoleID = 3,
                                        };

                                        saveSuccess = NoticeManagement.CreateUpdateNoticeAssignment(newAssignment);

                                        //if (!NoticeManagement.ExistNoticeAssignment(newAssignment))
                                        //{
                                        //    saveSuccess = NoticeManagement.CreateNoticeAssignment(newAssignment);
                                        //    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Created", true);
                                        //}
                                        //else
                                        //{
                                        //    saveSuccess = NoticeManagement.UpdateNoticeAssignments(newAssignment);
                                        //    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Updated", true);
                                        //}
                                    });
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Created", true);
                                    }
                                }
                            }

                            if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                            {
                                tbl_LegalNoticeAssignment newReviewerAssignment = new tbl_LegalNoticeAssignment()
                                {
                                    AssignmentType = newNotice.AssignmentType,
                                    NoticeInstanceID = newNoticeID,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,

                                    UserID = Convert.ToInt32(ddlReviewer.SelectedValue),
                                    RoleID = 4,
                                };

                                saveSuccess = NoticeManagement.CreateUpdateNoticeAssignment(newReviewerAssignment);

                                //if (!NoticeManagement.ExistNoticeAssignment(newReviewerAssignment))
                                //    saveSuccess = NoticeManagement.CreateNoticeAssignment(newReviewerAssignment);
                                //else
                                //    saveSuccess = NoticeManagement.UpdateNoticeAssignments(newReviewerAssignment);
                            }

                            #endregion

                        }

                        if (saveSuccess)
                        {
                            #region 
                            try
                            {
                                if (newNotice != null)
                                {
                                    lstNoticePerformerUser.ForEach(EachPerformer =>
                                    {
                                        tbl_LitigationReminderLog objRemind = new tbl_LitigationReminderLog()
                                        {
                                            UserID = EachPerformer,
                                            Role = 3,
                                            TriggerType = "LitigationNoticeAssignment",
                                            TriggerDate = DateTime.Now
                                        };

                                        CaseManagement.SaveLitigationReminderMail(objRemind);
                                    }); //End For Each - Performer User

                                    LitigationManagement.CreateAuditLog("N", newNotice.ID, "tbl_LitigationReminderLog", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Email Sent to Assigned Users", true);
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            #endregion
                            #region Sent Email to Assigned Users
                            List<long> OwnerMailID = new List<long>();
                            List<string> OwnerMailList = new List<string>();
                            List<string> AssignedUseerMailID = new List<string>();
                            User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                            CheckAssingedUserOld = NoticeManagement.GetAllAssingedUserList(newNoticeID, AuthenticationHelper.UserID);

                            if (CheckAssingedUserOld.Count > 0)
                            {
                                foreach (var item in CheckAssingedUserOld)
                                {
                                    if (CheckAssingedUserOld.Contains(item))
                                    {
                                        UpdatedAssingedUser.Add(Convert.ToInt64(item));
                                    }
                                }
                            }


                            bool islawyer = false;
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                //var Query = (from row in entities.CaseLawayerEmails
                                //             where row.Customerid == AuthenticationHelper.CustomerID
                                //             && row.IsActive == false
                                //             select row).FirstOrDefault();

                                //if (Query != null)
                                //{
                                //    islawyer = true;
                                //}
                            }
                            if (islawyer)
                            {
                                //AssignedUseerMailID = CaseManagement.GetAssignedUserAndOwnerMailexceptLawyer(UpdatedAssingedUser);
                                if (ddlOwner.SelectedValue != "" && ddlOwner.SelectedValue != "-1")
                                {
                                    OwnerMailID.Add(Convert.ToInt64(ddlOwner.SelectedValue));
                                }
                                //OwnerMailList = CaseManagement.GetAssignedUserAndOwnerMailexceptLawyer(OwnerMailID);
                            }
                            else
                            {
                                 AssignedUseerMailID = CaseManagement.GetAssignedUserAndOwnerMail(UpdatedAssingedUser);
                                if (ddlOwner.SelectedValue != "" && ddlOwner.SelectedValue != "-1")
                                {
                                    OwnerMailID.Add(Convert.ToInt64(ddlOwner.SelectedValue));
                                }
                                OwnerMailList = CaseManagement.GetAssignedUserAndOwnerMail(OwnerMailID);
                            }
                            var NoticeRecord = NoticeManagement.GetNoticeByID(Convert.ToInt32(newNoticeID));
                            var Locations = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(NoticeRecord.CustomerBranchID)))
                            {
                                Locations = CaseManagement.GetLocationByCaseInstanceID(NoticeRecord.CustomerBranchID);
                            }

                            List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                            List<string> UniqueMail = new List<string>();
                            if (OwnerMailList.Count > 0)
                            {
                                foreach (var item in OwnerMailList)
                                {
                                    if (!UniqueMail.Contains(item))
                                    {
                                        UniqueMail.Add(item);
                                    }
                                }
                            }
                            if (MgmUser.Count > 0)
                            {
                                foreach (var item in MgmUser)
                                {
                                    if (!UniqueMail.Contains(item))
                                    {
                                        UniqueMail.Add(item);
                                    }
                                }
                            }
                            // Remove assigned user if exist in mgm
                            if (AssignedUseerMailID.Count > 0)
                            {
                                foreach (var item in UniqueMail)
                                {
                                    if (AssignedUseerMailID.Contains(item))
                                    {
                                        AssignedUseerMailID.Remove(item);
                                        continue;
                                    }
                                }

                                string NoticeTitleMerge = NoticeRecord.NoticeTitle;
                                string FinalCaseTitle = string.Empty;
                                if (NoticeTitleMerge.Length > 50)
                                {
                                    FinalCaseTitle = NoticeTitleMerge.Substring(0, 50);
                                    FinalCaseTitle = FinalCaseTitle + "...";
                                }
                                else
                                {
                                    FinalCaseTitle = NoticeTitleMerge;
                                }

                                var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                AssignedUserList = AssignedUserList.TrimStart(',');
                                string username = string.Format("{0} {1}", User.FirstName, User.LastName);

                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_NoticeAssignment
                                                                        .Replace("@User", username)
                                                                        .Replace("@NoticeRefNo", newNotice.RefNo)
                                                                        .Replace("@NoticeTitle", newNotice.NoticeTitle)
                                                                        .Replace("@NoticeDetailDesc", newNotice.NoticeDetailDesc)
                                                                        .Replace("@Location", Locations)
                                                                        .Replace("@AssignedBy", AssignedUserList)
                                                                        .Replace("@Category", ddlNoticeCategory.SelectedItem.Text)
                                                                        .Replace("@From", cname.Trim())
                                                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(AssignedUseerMailID), UniqueMail, null, "Litigation Notification Notice Assigned - " + FinalCaseTitle, message);
                            }
                            #endregion
                            cvNoticePopUp.IsValid = false;
                            cvNoticePopUp.ErrorMessage = "Notice Created Successfully.";
                            VSNoticePopup.CssClass = "alert alert-success";
                            liNoticeResponse.Visible = true;
                            liNoticeTask.Visible = true;
                            liNoticeStatusPayment.Visible = true;
                            liLawyerRating.Visible = true;
                            liAuditLog.Visible = true;
                            liDocument.Visible = true;

                            ViewState["Mode"] = 1;
                            ViewState["noticeInstanceID"] = newNoticeID;

                            enableDisableNoticeSummaryTabControls(false);

                            btnSave.Visible = true;
                            btnSave.Text = "Update";
                            btnClearNoticeDetail.Visible = false;
                            btnEditNoticeDetail.Visible = true;
                            lnkActDetails.Visible = true;
                            btnSendMailPopup.Visible = true;
                            showHideButtons(true);
                            BindNoticeUserAssignments(newNoticeID);
                        }
                    }//Add Code End                                       

                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (ViewState["noticeInstanceID"] != null)
                        {
                            newNoticeID = Convert.ToInt32(ViewState["noticeInstanceID"]); //Selected Notice ID
                            newNotice.ID = newNoticeID;

                            bool existNoticeRefNo = NoticeManagement.ExistsNoticeRefNo(tbxRefNo.Text, newNoticeID);
                            if (!existNoticeRefNo) //Same Notice Reference Number Previously not exists
                            {
                                if (!NoticeManagement.ExistsNotice(newNotice.NoticeTitle, newNoticeID))
                                {
                                    saveSuccess = NoticeManagement.UpdateNotice(newNotice);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Details Updated", true);
                                        saveSuccess = true;
                                    }
                                }
                                else
                                {
                                    cvNoticePopUp.IsValid = false;
                                    cvNoticePopUp.ErrorMessage = "Notice with Same Title already Exists.";
                                    VSNoticePopup.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvNoticePopUp.IsValid = false;
                                cvNoticePopUp.ErrorMessage = "Notice with Same Reference No. already exists";
                                VSNoticePopup.CssClass = "alert alert-danger";
                                tbxRefNo.Focus();
                            }

                            if (saveSuccess)
                            {
                                //Notice Status Transaction
                                #region Status Transaction
                                if ((int)ViewState["Mode"] == 0)
                                {
                                    tbl_LegalNoticeStatusTransaction newStatusRecord = new tbl_LegalNoticeStatusTransaction()
                                    {
                                        NoticeInstanceID = newNoticeID,
                                        StatusID = 1,
                                        StatusChangeOn = DateTime.Now,
                                        IsActive = true,
                                        IsDeleted = false,
                                        UserID = AuthenticationHelper.UserID,
                                        RoleID = 3,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                    };

                                    if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusRecord))
                                    {
                                        saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusRecord);
                                        saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusRecord);
                                    }
                                }

                                #endregion

                                #region Save Party Mapping

                                if (ddlParty.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachParty in ddlParty.Items)
                                    {
                                        if (eachParty.Selected)
                                            lstPartyMapping.Add(Convert.ToInt32(eachParty.Value));
                                    }
                                }

                                if (lstPartyMapping.Count > 0)
                                {
                                    List<tbl_PartyMapping> lstObjPartyMapping = new List<tbl_PartyMapping>();
                                    saveSuccess = CaseManagement.DeActiveExistingPartyMapping(newNoticeID, 2);
                                    lstPartyMapping.ForEach(EachParty =>
                                    {
                                        tbl_PartyMapping objPartyMapping = new tbl_PartyMapping()
                                        {
                                            CaseNoticeInstanceID = newNoticeID,
                                            IsActive = true,
                                            Type = 2,//1 as Case and 2 as Notice
                                            PartyID = Convert.ToInt32(EachParty),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstObjPartyMapping.Add(objPartyMapping);
                                    });

                                    saveSuccess = CaseManagement.CreateUpdatePartyMapping(lstObjPartyMapping);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_PartyMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Party Mapping Updated", true);
                                    }
                                    //Refresh List
                                    lstObjPartyMapping.Clear();
                                    lstObjPartyMapping = null;

                                    lstPartyMapping.Clear();
                                    lstPartyMapping = null;
                                }
                                #endregion

                                #region Save Act Mapping

                                if (ddlAct.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachAct in ddlAct.Items)
                                    {
                                        if (eachAct.Selected)
                                            lstActMapping.Add(Convert.ToInt32(eachAct.Value));
                                    }
                                }

                                if (lstActMapping.Count > 0)
                                {
                                    List<tbl_ActMapping> lstObjActMapping = new List<tbl_ActMapping>();
                                    saveSuccess = CaseManagement.DeActiveExistingActMapping(newNoticeID, 2);

                                    lstActMapping.ForEach(EachParty =>
                                    {
                                        tbl_ActMapping objActMapping = new tbl_ActMapping()
                                        {
                                            CaseNoticeInstanceID = newNoticeID,
                                            IsActive = true,
                                            Type = 2,//1 as Case and 2 as Notice
                                            ActID = Convert.ToInt32(EachParty),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstObjActMapping.Add(objActMapping);
                                    });

                                    saveSuccess = CaseManagement.CreateUpdateActMapping(lstObjActMapping);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_ActMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Act Mapping Updated", true);
                                    }
                                    //Refresh List
                                    lstObjActMapping.Clear();
                                    lstObjActMapping = null;

                                    lstActMapping.Clear();
                                    lstActMapping = null;
                                }
                                #endregion

                                #region Save Financial Year Mapping
                                if (DropDownListChosen1.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachFY in DropDownListChosen1.Items)
                                    {
                                        if (eachFY.Selected)
                                            lstFinancialYearMapping.Add(Convert.ToString(eachFY.Value));
                                    }
                                }

                                if (lstFinancialYearMapping.Count > 0)
                                {
                                    List<FinancialYearMapping> lstObjFYMapping = new List<FinancialYearMapping>();
                                    saveSuccess = CaseManagement.DeActiveExistingFYMapping(newNoticeID, 2);

                                    lstFinancialYearMapping.ForEach(EachFYID =>
                                    {
                                        FinancialYearMapping objFYMapping = new FinancialYearMapping()
                                        {
                                            FYID = Convert.ToString(EachFYID),
                                            Type = 2,//1 as Case and 2 as Notice
                                            CaseNoticeInstanceID = newNoticeID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstObjFYMapping.Add(objFYMapping);
                                    });

                                    saveSuccess = CaseManagement.CreateUpdateFYMapping(lstObjFYMapping);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", newNoticeID, "FinancialYearMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Financial Year Mapped.", false);
                                    }
                                    //Refresh List
                                    lstObjFYMapping.Clear();
                                    lstObjFYMapping = null;

                                    lstFinancialYearMapping.Clear();
                                    lstFinancialYearMapping = null;
                                }
                                #endregion

                                #region Save Opposition Lawyer Mapping

                                if (lstBoxOppositionLawyer.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem LawyerList in lstBoxOppositionLawyer.Items)
                                    {
                                        if (LawyerList.Selected)
                                            lstOppoLawyerMapping.Add(Convert.ToInt32(LawyerList.Value));
                                    }
                                    saveSuccess = CaseManagement.DeActiveExistingOppoLawyerMapping(newNoticeID, 2);
                                }

                                if (lstOppoLawyerMapping.Count > 0)
                                {
                                    List<tbl_OppositionLawyerList> lstObjLawyerMapping = new List<tbl_OppositionLawyerList>();
                                 
                                    lstOppoLawyerMapping.ForEach(EachParty =>
                                    {
                                        tbl_OppositionLawyerList objLawyerMapping = new tbl_OppositionLawyerList()
                                        {
                                            CaseNoticeInstanceID = newNoticeID,
                                            IsActive = true,
                                            Type = 2,//1 as Case and 2 as Notice
                                            LawyerID = Convert.ToInt32(EachParty),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstObjLawyerMapping.Add(objLawyerMapping);
                                    });

                                    saveSuccess = CaseManagement.CreateUpdateOppositionLawyerMapping(lstObjLawyerMapping);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_OppositionLawyerList", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Opposition Lawyer Updated", true);
                                    }
                                    //Refresh List
                                    lstOppoLawyerMapping.Clear();
                                    lstOppoLawyerMapping = null;

                                    lstObjLawyerMapping.Clear();
                                    lstObjLawyerMapping = null;
                                }
                                #endregion

                                //Lawyer Mapping
                                #region Lawyer Mapping

                                if (ddlLawFirm.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachlawyer in ddlLawFirm.Items)
                                    {
                                        if (eachlawyer.Selected)
                                            lstNoticeLawyer.Add(Convert.ToInt32(eachlawyer.Value));
                                    }
                                }
                                saveSuccess = NoticeManagement.DeActiveExistingNoticeLawyerMapping(newNoticeID);

                                tbl_LegalNoticeLawyerMapping objNoticeLawyerMapping = new tbl_LegalNoticeLawyerMapping()
                                {
                                    NoticeInstanceID = newNoticeID,
                                    IsActive = true,
                                    LawyerID = Convert.ToInt32(ddlLawFirm.SelectedValue),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };

                                saveSuccess = NoticeManagement.UpdateNoticeLawyerMapping(objNoticeLawyerMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeLawyerMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Lawyer Mapping Updated", true);
                                }

                                #endregion

                                #region Update Custom Field
                                saveSuccess = LitigationManagement.DeActiveExistingeCustomsFieldByNoticeOrCaseID(2, newNoticeID);

                                //if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                //{
                                //Select Which Grid to Loop based on Selected Category/Type
                                GridView gridViewToCollectData = null;

                                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                {
                                    gridViewToCollectData = grdCustomField_TaxLitigation;
                                }
                                else
                                {
                                    gridViewToCollectData = grdCustomField;
                                }

                                for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                {
                                    Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                    TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                    if (lblID != null && tbxLabelValue != null)
                                    {
                                        if (!string.IsNullOrEmpty(lblID.Text) && !string.IsNullOrEmpty(tbxLabelValue.Text))
                                        {
                                            if (lblID.Text != "0")
                                            {
                                                tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                {
                                                    NoticeCaseType = 2,
                                                    NoticeCaseInstanceID = newNoticeID,

                                                    LabelID = Convert.ToInt32(lblID.Text),
                                                    LabelValue = tbxLabelValue.Text,

                                                    IsActive = true,
                                                    IsDeleted = false,
                                                };

                                                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                                {
                                                    TextBox tbxInterestValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxInterestValue");
                                                    TextBox tbxPenaltyValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxPenaltyValue");
                                                    TextBox tbxTotalValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxRowTotalValue");
                                                    TextBox tbxProvisionInbooks = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxProvisionInbooks");
                                                    TextBox tbxSettlementValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxSettlement");

                                                    if (tbxInterestValue != null && tbxPenaltyValue != null && tbxTotalValue != null
                                                        && tbxProvisionInbooks != null && tbxSettlementValue != null)
                                                    {
                                                        if (tbxInterestValue.Text != "")
                                                            ObjParameter.Interest = tbxInterestValue.Text;

                                                        if (tbxPenaltyValue.Text != "")
                                                            ObjParameter.Penalty = tbxPenaltyValue.Text;

                                                        if (tbxTotalValue.Text != "")
                                                            ObjParameter.Total = tbxTotalValue.Text;

                                                        if (tbxSettlementValue.Text != "")
                                                            ObjParameter.SettlementValue = tbxSettlementValue.Text;

                                                        if (tbxProvisionInbooks.Text != "")
                                                            ObjParameter.ProvisionInBook = tbxProvisionInbooks.Text;
                                                    }
                                                }

                                                ObjParameter.CreatedBy = AuthenticationHelper.UserID;
                                                ObjParameter.CreatedOn = DateTime.Now;

                                                ObjParameter.UpdatedBy = AuthenticationHelper.UserID;
                                                ObjParameter.UpdatedOn = DateTime.Now;

                                                saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);
                                            }//ID-0 Check
                                        }
                                    }
                                }//END FOR LOOP - grdCustomFields
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_NoticeCaseCustomParameter", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Updated.", true);
                                }
                                //}
                                #endregion

                                //User Assignment ---Able to Edit User Assignment Only After Re-Assign
                                #region User Assignment

                                //De-Active All Previous Assignment
                                NoticeManagement.DeActivenoticeAssignments(newNoticeID, AuthenticationHelper.UserID);

                                if (lstBoxPerformer.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachPerformer in lstBoxPerformer.Items)
                                    {
                                        if (eachPerformer.Selected)
                                            lstNoticePerformerUser.Add(Convert.ToInt32(eachPerformer.Value));
                                    }

                                    if (lstNoticePerformerUser.Count > 0)
                                    {
                                        lstNoticePerformerUser.ForEach(EachPerformer =>
                                        {
                                            tbl_LegalNoticeAssignment newAssignment = new tbl_LegalNoticeAssignment()
                                            {
                                                AssignmentType = newNotice.AssignmentType,
                                                NoticeInstanceID = newNoticeID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                IsLawyer = false,
                                                UserID = Convert.ToInt32(EachPerformer),
                                                RoleID = 3,
                                            };

                                            saveSuccess = NoticeManagement.CreateUpdateNoticeAssignment(newAssignment);

                                            //if (!NoticeManagement.ExistNoticeAssignment(newAssignment))
                                            //{
                                            //    saveSuccess = NoticeManagement.CreateNoticeAssignment(newAssignment);
                                            //    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Created", true);
                                            //}
                                            //else
                                            //{
                                            //    saveSuccess = NoticeManagement.UpdateNoticeAssignments(newAssignment);
                                            //    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Updated", true);
                                            //}
                                        });

                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Performer Assignment Created", true);
                                        }
                                    }
                                }

                                lstNoticePerformerUser.Clear();

                                if (lstBoxLawyerUser.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachLawyerUser in lstBoxLawyerUser.Items)
                                    {
                                        if (eachLawyerUser.Selected)
                                            lstNoticePerformerUser.Add(Convert.ToInt32(eachLawyerUser.Value));
                                    }

                                    if (lstNoticePerformerUser.Count > 0)
                                    {
                                        lstNoticePerformerUser.ForEach(EachPerformer =>
                                        {
                                            tbl_LegalNoticeAssignment newAssignment = new tbl_LegalNoticeAssignment()
                                            {
                                                AssignmentType = newNotice.AssignmentType,
                                                NoticeInstanceID = newNoticeID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                IsLawyer = true,
                                                UserID = Convert.ToInt32(EachPerformer),
                                                RoleID = 3,
                                            };

                                            saveSuccess = NoticeManagement.CreateUpdateNoticeAssignment(newAssignment);

                                            //if (!NoticeManagement.ExistNoticeAssignment(newAssignment))
                                            //{
                                            //    saveSuccess = NoticeManagement.CreateNoticeAssignment(newAssignment);
                                            //    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Created", true);
                                            //}
                                            //else
                                            //{
                                            //    saveSuccess = NoticeManagement.UpdateNoticeAssignments(newAssignment);
                                            //    LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Assignment Updated", true);
                                            //}
                                        });
                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LegalNoticeAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Performer Assignment Updated", true);
                                        }
                                    }
                                }

                                if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                                {
                                    tbl_LegalNoticeAssignment newReviewerAssignment = new tbl_LegalNoticeAssignment()
                                    {
                                        AssignmentType = newNotice.AssignmentType,
                                        NoticeInstanceID = newNoticeID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,

                                        UserID = Convert.ToInt32(ddlReviewer.SelectedValue),
                                        RoleID = 4,
                                    };

                                    saveSuccess = NoticeManagement.CreateUpdateNoticeAssignment(newReviewerAssignment);

                                    //if (!NoticeManagement.ExistNoticeAssignment(newReviewerAssignment))
                                    //    saveSuccess = NoticeManagement.CreateNoticeAssignment(newReviewerAssignment);
                                    //else
                                    //    saveSuccess = NoticeManagement.UpdateNoticeAssignments(newReviewerAssignment);
                                }

                                #endregion

                                //Upload Document
                                #region Upload Document

                                //if (NoticeFileUpload.HasFiles)
                                //{
                                //    tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                                //    {
                                //        NoticeCaseInstanceID = Convert.ToInt32(newNoticeID),
                                //        CreatedBy = AuthenticationHelper.UserID,
                                //        CreatedByText = AuthenticationHelper.User,
                                //        IsDeleted = false,
                                //        DocType = "N",
                                //    };

                                //    HttpFileCollection fileCollection = Request.Files;

                                //    if (fileCollection.Count > 0)
                                //    {
                                //        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                //        int customerID = -1;
                                //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                //        string directoryPath = "";
                                //        String fileName = "";

                                //        if (newNoticeID > 0)
                                //        {
                                //            for (int i = 0; i < fileCollection.Count; i++)
                                //            {
                                //                HttpPostedFile uploadedFile = fileCollection[i];

                                //                if (uploadedFile.ContentLength > 0)
                                //                {
                                //                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                //                    if (keys1[keys1.Count() - 1].Equals("NoticeFileUpload"))
                                //                    {
                                //                        fileName = uploadedFile.FileName;
                                //                    }

                                //                    objNoticeDoc.FileName = fileName;

                                //                    //Get Document Version
                                //                    var noticeDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeDoc);

                                //                    noticeDocVersion++;
                                //                    objNoticeDoc.Version = noticeDocVersion + ".0";

                                //                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(newNoticeID) + "/NoticeDocument/" + objNoticeDoc.Version);

                                //                    if (!Directory.Exists(directoryPath))
                                //                        Directory.CreateDirectory(directoryPath);

                                //                    Guid fileKey1 = Guid.NewGuid();
                                //                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                //                    Stream fs = uploadedFile.InputStream;
                                //                    BinaryReader br = new BinaryReader(fs);
                                //                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                //                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                //                    objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                //                    objNoticeDoc.FileKey = fileKey1.ToString();
                                //                    objNoticeDoc.VersionDate = DateTime.Now;
                                //                    objNoticeDoc.CreatedOn = DateTime.Now;

                                //                    DocumentManagement.SaveDocFiles(fileList);
                                //                    saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);

                                //                    fileList.Clear();
                                //                }

                                //            }//End For Each  
                                //            if (saveSuccess)
                                //            {
                                //                LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Document(s) Uploaded", true);
                                //            }
                                //        }
                                //    }

                                //    if (saveSuccess)
                                //    {
                                //        BindNoticeRelatedDocuments(Convert.ToInt32(newNoticeID));
                                //    }
                                //}

                                #endregion
                            }

                            if (saveSuccess)
                            {
                                cvNoticePopUp.IsValid = false;
                                cvNoticePopUp.ErrorMessage = "Notice Details Updated Successfully.";
                                VSNoticePopup.CssClass = "alert alert-success";
                                enableDisableNoticeSummaryTabControls(false);

                                btnSave.Visible = true;
                                btnSave.Text = "Update";
                                btnClearNoticeDetail.Visible = false;
                                btnEditNoticeDetail.Visible = true;
                                lnkActDetails.Visible = true;
                                btnSendMailPopup.Visible = true;
                                showHideButtons(true);
                                BindNoticeUserAssignments(newNoticeID);
                            }
                        }
                    }//Edit Code End
                }

                #endregion

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        protected void btnTaskSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    bool isBlankFile = false;
                    int AssignedToUser = -1;
                    int DocTypeID = -1;
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    HttpFileCollection fileCollection = Request.Files;

                    if (fileCollection.Count > 0)
                    {
                        string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < InvalidFileTypes.Length; j++)
                                        {
                                            if (ext == "." + InvalidFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }


                    if (tbxTaskTitle.Text != "")
                    {
                        if (tbxTaskDueDate.Text != "")
                        {
                            if ((!String.IsNullOrEmpty(ddlTaskUserInternal.SelectedValue) && ddlTaskUserInternal.SelectedValue != "0") ||
                                    (!String.IsNullOrEmpty(ddlTaskUserLawyerAndExternal.SelectedValue) && ddlTaskUserLawyerAndExternal.SelectedValue != "0"))
                            {
                                if (!String.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                                {
                                    if (tbxTaskDesc.Text != "")
                                    {
                                        if (isBlankFile == false)
                                        {
                                            validateData = true;

                                            if (!String.IsNullOrEmpty(ddlTaskUserInternal.SelectedValue) && ddlTaskUserInternal.SelectedValue != "0")
                                                AssignedToUser = Convert.ToInt32(ddlTaskUserInternal.SelectedValue);
                                            else if (!String.IsNullOrEmpty(ddlTaskUserLawyerAndExternal.SelectedValue) && ddlTaskUserLawyerAndExternal.SelectedValue != "0")
                                                AssignedToUser = Convert.ToInt32(ddlTaskUserLawyerAndExternal.SelectedValue);
                                        }

                                        else
                                        {
                                            cvNoticePopUpTask.IsValid = false;
                                            cvNoticePopUpTask.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                                            ValidationSummary5.CssClass = "alert alert-danger";
                                        }
                                    }
                                    else
                                    {
                                        cvNoticePopUpTask.IsValid = false;
                                        cvNoticePopUpTask.ErrorMessage = "Provide Task Description.";
                                        ValidationSummary5.CssClass = "alert alert-danger";
                                        return;
                                    }
                                }
                                else
                                {
                                    cvNoticePopUpTask.IsValid = false;
                                    cvNoticePopUpTask.ErrorMessage = "Select Task Priority.";
                                    ValidationSummary5.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvNoticePopUpTask.IsValid = false;
                                cvNoticePopUpTask.ErrorMessage = "Select User to Assign Task.";
                                ValidationSummary5.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                        else
                        {
                            cvNoticePopUpTask.IsValid = false;
                            cvNoticePopUpTask.ErrorMessage = "Provide Due Date.";
                            ValidationSummary5.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                    else
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Provide Task Title.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                        return;
                    }

                    tbl_TaskScheduleOn newRecord = new tbl_TaskScheduleOn();

                    if (validateData)
                    {
                        newRecord.IsActive = true;
                        newRecord.TaskType = "N";
                        newRecord.NoticeCaseInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                        newRecord.ScheduleOnDate = DateTimeExtensions.GetDate(tbxTaskDueDate.Text);
                        newRecord.TaskTitle = tbxTaskTitle.Text.Trim();
                        newRecord.TaskDesc = tbxTaskDesc.Text.Trim();

                        newRecord.StatusID = 1;
                        newRecord.CustomerID = customerID;
                        newRecord.CreatedBy = AuthenticationHelper.UserID;
                        newRecord.CreatedByText = AuthenticationHelper.User;
                        newRecord.LinkCreatedOn = DateTime.Now;
                        newRecord.URLExpired = false;

                        newRecord.ExpOutcome = tbxExpOutcome.Text;

                        if (AssignedToUser != -1)
                            newRecord.AssignTo = AssignedToUser;

                        if (!String.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                            newRecord.PriorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);

                        if (tbxTaskRemark.Text != "")
                            newRecord.Remark = tbxTaskRemark.Text.Trim();

                        if (Convert.ToString(ViewState["TaskMode"]) == "Edit")
                        {
                            newRecord.ID = Convert.ToInt32(tbxTaskID.Text);
                            saveSuccess = LitigationTaskManagement.UpdateTask(newRecord);
                            ViewState["TaskMode"] = "Add";
                            ViewState["NoticeInstanceID"] = newRecord.NoticeCaseInstanceID;
                            DivTaskEdit.Visible = false;
                            grdTaskEditDoc.DataSource = null;
                            grdTaskEditDoc.DataBind();
                        }
                        else
                        {
                            if (!LitigationTaskManagement.ExistNoticeCaseTaskTitle(tbxTaskTitle.Text.Trim(), "N", (long)newRecord.NoticeCaseInstanceID, customerID))
                            {
                                saveSuccess = LitigationTaskManagement.CreateTask(newRecord);

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Created", true);
                                }
                            }
                            else
                            {
                                saveSuccess = false;
                                CvTaskSaveMsg.IsValid = false;
                                CvTaskSaveMsg.ErrorMessage = "Task with same title already exists.";
                                ValidationSummary8.CssClass = "alert alert-danger";
                                tbxTaskTitle.Focus();
                                return;
                            }
                        }

                        if (saveSuccess)
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (AWSData != null)
                            {
                                //Save Task Related Uploaded Documents
                                #region AWS Upload Document

                                if (fuTaskDocUpload.HasFiles)
                                {
                                    tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                                    {
                                        NoticeCaseInstanceID = newRecord.NoticeCaseInstanceID,
                                        DocTypeInstanceID = newRecord.ID, //TaskID
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedByText = AuthenticationHelper.User,
                                        IsDeleted = false,
                                        DocTypeID = DocTypeID,
                                        DocType = "NT"
                                    };


                                    if (fileCollection.Count > 0)
                                    {
                                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                        string directoryPath = "";
                                        String fileName = "";

                                        if (newRecord.ID > 0)
                                        {
                                            for (int i = 0; i < fileCollection.Count; i++)
                                            {
                                                HttpPostedFile uploadedFile = fileCollection[i];

                                                if (uploadedFile.ContentLength > 0)
                                                {
                                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                                    if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                    {
                                                        fileName = uploadedFile.FileName;
                                                    }

                                                    objNoticeDoc.FileName = fileName;

                                                    //Get Document Version
                                                    var taskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeDoc);

                                                    taskDocVersion++;
                                                    objNoticeDoc.Version = taskDocVersion + ".0";

                                                    directoryPath = "LitigationDocuments\\" + customerID + "\\Notice\\" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "\\Task\\" + Convert.ToInt32(newRecord.ID) + "\\" + objNoticeDoc.Version;
                                                    //directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "/Task/" + Convert.ToInt32(newRecord.ID) + "/" + objNoticeDoc.Version);

                                                    //if (!Directory.Exists(directoryPath))
                                                    //    Directory.CreateDirectory(directoryPath);

                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                    string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                                    string p_strPath = string.Empty;
                                                    string dirpath = string.Empty;
                                                    //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                                                    //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;

                                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                    p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                                    dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;

                                                    //if (File.Exists(p_strPath))
                                                    //    File.Delete(p_strPath);
                                                    if (!Directory.Exists(dirpath))
                                                    {
                                                        Directory.CreateDirectory(dirpath);
                                                    }
                                                    FileStream objFileStrm = File.Create(p_strPath);
                                                    objFileStrm.Close();
                                                    File.WriteAllBytes(p_strPath, bytes);

                                                    Guid fileKey1 = Guid.NewGuid();
                                                    string AWSpath = directoryPath + "\\" + uploadedFile.FileName;

                                                    objNoticeDoc.FilePath = directoryPath.Replace(@"\", "/");
                                                    objNoticeDoc.FileKey = Convert.ToString(fileKey1);
                                                    objNoticeDoc.VersionDate = DateTime.Now;
                                                    objNoticeDoc.CreatedOn = DateTime.Now;
                                                    objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                                    FileInfo localFile = new FileInfo(p_strPath);
                                                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                    if (!di.Exists)
                                                    {
                                                        di.Create();
                                                    }
                                                    S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                    if (!s3File.Exists)
                                                    {
                                                        using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                        {
                                                            localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                        }
                                                    }
                                                    //DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                    saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);

                                                    fileList.Clear();
                                                }
                                            }//End For Each  
                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Document(s) Uploaded", true);
                                            }
                                        }
                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                #region Upload Document

                                if (fuTaskDocUpload.HasFiles)
                                {
                                    tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                                    {
                                        NoticeCaseInstanceID = newRecord.NoticeCaseInstanceID,
                                        DocTypeInstanceID = newRecord.ID, //TaskID
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedByText = AuthenticationHelper.User,
                                        IsDeleted = false,
                                        DocTypeID = DocTypeID,
                                        DocType = "NT"
                                    };


                                    if (fileCollection.Count > 0)
                                    {
                                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                        string directoryPath = "";
                                        String fileName = "";

                                        if (newRecord.ID > 0)
                                        {
                                            for (int i = 0; i < fileCollection.Count; i++)
                                            {
                                                HttpPostedFile uploadedFile = fileCollection[i];

                                                if (uploadedFile.ContentLength > 0)
                                                {
                                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                                    if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                    {
                                                        fileName = uploadedFile.FileName;
                                                    }

                                                    objNoticeDoc.FileName = fileName;

                                                    //Get Document Version
                                                    var taskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeDoc);

                                                    taskDocVersion++;
                                                    objNoticeDoc.Version = taskDocVersion + ".0";

                                                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "/Task/" + Convert.ToInt32(newRecord.ID) + "/" + objNoticeDoc.Version);

                                                    if (!Directory.Exists(directoryPath))
                                                        Directory.CreateDirectory(directoryPath);

                                                    Guid fileKey1 = Guid.NewGuid();
                                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                    objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                    objNoticeDoc.FileKey = Convert.ToString(fileKey1);
                                                    objNoticeDoc.VersionDate = DateTime.Now;
                                                    objNoticeDoc.CreatedOn = DateTime.Now;
                                                    objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                                    DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                    saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);

                                                    fileList.Clear();
                                                }
                                            }//End For Each  
                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Document(s) Uploaded", true);
                                            }
                                        }
                                    }
                                }

                                #endregion
                            }
                        }
                    }
                   

                    if (saveSuccess)
                    {
                        string accessURL = string.Empty;

                        CvTaskSaveMsg.IsValid = false;
                        CvTaskSaveMsg.ErrorMessage = "Task Saved Successfully.";
                        ValidationSummary8.CssClass = "alert alert-success";

                        //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                        if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(newRecord.AssignTo), customerID))
                            accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                CryptographyManagement.Encrypt(newRecord.ID.ToString()) +
                                "&NID=" + CryptographyManagement.Encrypt(newRecord.NoticeCaseInstanceID.ToString());
                        else
                            accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);

                        saveSuccess = SendTaskAssignmentMail(newRecord, accessURL, AuthenticationHelper.User);

                        if (saveSuccess)
                        {
                            CvTaskSaveMsg.IsValid = false;
                            CvTaskSaveMsg.ErrorMessage = "Task Saved Successfully. An Email containing task detail and access URL to provide response sent to assignee.";
                            ValidationSummary8.CssClass = "alert alert-success";
                            newRecord.AccessURL = accessURL;
                            saveSuccess = LitigationTaskManagement.UpdateTaskAccessURL(newRecord.ID, newRecord);
                        }

                        clearTaskControls();

                        //Re-Bind Notice Task Details
                        BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveResponse_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    bool isBlankFile = false;
                    long newResponseID = 0;
                    long noticeInstanceID = 0;
                    string strReplyDueDate = "NA";
                    DateTime ReplyDueDate = new DateTime();
                    int DocTypeID = -1;
                    noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                    if (tbxResponseDate.Text != "")
                    {
                        if (!String.IsNullOrEmpty(ddlRespBy.SelectedValue))
                        {
                            if (tbxRespThrough.Text != "")
                            {
                                if (tbxRespRefNo.Text != "")
                                {
                                    if (tbxResponseDesc.Text != "")
                                    {
                                        validateData = true;
                                    }
                                    else
                                    {
                                        cvNoticePopUpResponse.IsValid = false;
                                        cvNoticePopUpResponse.ErrorMessage = "Provide Notice Response Description.";
                                        ValidationSummary1.CssClass = "alert alert-danger";
                                        return;
                                    }
                                }
                                else
                                {
                                    cvNoticePopUpResponse.IsValid = false;
                                    cvNoticePopUpResponse.ErrorMessage = "Provide Reference/ Courier/ Post Traking Number, Put 'NA' if not available.";
                                    ValidationSummary1.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvNoticePopUpResponse.IsValid = false;
                                cvNoticePopUpResponse.ErrorMessage = "Provide Notice Response sent through (i.e. Courier Company/ Post Office Detail)";
                                ValidationSummary1.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                        else
                        {
                            cvNoticePopUpResponse.IsValid = false;
                            cvNoticePopUpResponse.ErrorMessage = "Select Response sent by Courier/Post/Other.";
                            ValidationSummary1.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                    else
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "Provide Notice Response Date.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                        return;
                    }
                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < InvalidFileTypes.Length; j++)
                                        {
                                            if (ext == "." + InvalidFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }


                    if (validateData && isBlankFile==false)
                    {
                        tbl_LegalNoticeResponse newRecord = new tbl_LegalNoticeResponse()
                        {
                            IsActive = true,
                            NoticeInstanceID = noticeInstanceID,
                            ResponseDate = DateTimeExtensions.GetDate(tbxResponseDate.Text),
                            RespondedBy = Convert.ToInt32(ddlRespBy.SelectedValue),
                            ResponseThrough = tbxRespThrough.Text,
                            ResponseRefNo = tbxRespRefNo.Text,
                            Description = tbxResponseDesc.Text,
                            ResponseType = Convert.ToInt32(ddlNoticeResponseDate.SelectedValue),
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                        };

                        //if (tbxNoticeDueDate.Text != "")
                        //{
                        //    newRecord.NoticeDueDate = DateTimeExtensions.GetDate(tbxNoticeDueDate.Text);
                        //    ReplyDueDate = DateTimeExtensions.GetDate(tbxNoticeDueDate.Text);
                        //}

                        //DateTime dt = new DateTime();
                        if (!string.IsNullOrEmpty(tbxNoticeDueDate.Text.Trim()))
                        {
                            newRecord.NoticeDueDate = DateTimeExtensions.GetDate(tbxNoticeDueDate.Text);
                            ReplyDueDate = DateTime.ParseExact(tbxNoticeDueDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            newRecord.NoticeDueDate = ReplyDueDate.Date;
                        }
                        else
                            newRecord.NoticeDueDate = null;

                        if (tbxResponseRemark.Text != "")
                            newRecord.Remark = tbxResponseRemark.Text;

                        #region Mail Data
                        User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                        string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                        List<string> CaseOwnerandIUser = NoticeManagement.getNoticeOwnerAndInternalUser(Convert.ToInt32(noticeInstanceID), AuthenticationHelper.CustomerID);
                        var NoticeRecord = NoticeManagement.GetNoticeByID(Convert.ToInt32(noticeInstanceID));
                        var Locations = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(NoticeRecord.CustomerBranchID)))
                        {
                            Locations = CaseManagement.GetLocationByCaseInstanceID(NoticeRecord.CustomerBranchID);
                        }

                        List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                        List<string> UniqueMail = new List<string>();
                        if (CaseOwnerandIUser.Count > 0)
                        {
                            foreach (var item in CaseOwnerandIUser)
                            {
                                if (MgmUser.Count > 0)
                                {
                                    if (!MgmUser.Contains(item))
                                    {
                                        UniqueMail.Add(item);
                                    }
                                }
                                else
                                    UniqueMail.Add(item);
                            }
                        }
                        //if (MgmUser.Count > 0)
                        //{
                        //    foreach (var item in MgmUser)
                        //    {
                        //        if (!UniqueMail.Contains(item))
                        //        {
                        //            UniqueMail.Add(item);
                        //        }
                        //    }
                        //}

                        string NoticeTitleMerge = NoticeRecord.NoticeTitle;
                        string FinalCaseTitle = string.Empty;
                        if (NoticeTitleMerge.Length > 50)
                        {
                            FinalCaseTitle = NoticeTitleMerge.Substring(0, 50);
                            FinalCaseTitle = FinalCaseTitle + "...";
                        }
                        else
                        {
                            FinalCaseTitle = NoticeTitleMerge;
                        }

                        #endregion end mail
                        string ic = "1/1/0001 12:00:00 AM";
                        if (ReplyDueDate == Convert.ToDateTime(ic))
                        {
                            strReplyDueDate = "NA";
                        }
                        else
                        {
                            strReplyDueDate = ReplyDueDate.Date.ToString("dd-MM-yyyy");
                        }
                        if (Convert.ToString(ViewState["ResponseMode"]) == "Edit")
                        {
                            newRecord.ID = Convert.ToInt32(tbxResponseID.Text);
                            newResponseID = NoticeManagement.UpdateNoticeResponse(newRecord);
                            ViewState["ResponseMode"] = "Add";
                            GrdResponseEditDocument.DataSource = null;
                            GrdResponseEditDocument.DataBind();
                            #region Send Mail to managment after Hearing update
                            var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (UniqueMail.Count > 0)
                            {

                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_ResponseUpdated
                                                        .Replace("@UpdatedUser", username)
                                                        .Replace("@NoticeRefNo", NoticeRecord.RefNo)
                                                        .Replace("@NoticeTitle", NoticeRecord.NoticeTitle)
                                                        .Replace("@Location", Locations)
                                                        .Replace("@ResponseDate", DateTimeExtensions.GetDate(tbxResponseDate.Text).ToString("dd-MM-yyyy"))
                                                        .Replace("@ReplyDueDate", strReplyDueDate)
                                                        .Replace("@ResponseDescription", tbxResponseDesc.Text)
                                                        .Replace("@AccessURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                        .Replace("@From", cname.Trim())
                                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), MgmUser, null, "Litigation Response Update-Notice Title-" + FinalCaseTitle, message);
                            }
                            #endregion
                            divResposeEditdoc.Visible = false;
                        }
                        else
                        {
                            newResponseID = NoticeManagement.CreateNoticeResponseLog(newRecord);
                            #region Mail send Create
                            var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (UniqueMail.Count > 0)
                            {
                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_ResponseAdd
                                                        .Replace("@UpdatedUser", username)
                                                        .Replace("@NoticeRefNo", NoticeRecord.RefNo)
                                                        .Replace("@NoticeTitle", NoticeRecord.NoticeTitle)
                                                        .Replace("@Location", Locations)
                                                        .Replace("@ResponseDate", DateTimeExtensions.GetDate(tbxResponseDate.Text).ToString("dd-MM-yyyy"))
                                                        .Replace("@ReplyDueDate", strReplyDueDate)
                                                        .Replace("@ResponseDescription", tbxResponseDesc.Text)
                                                        .Replace("@AccessURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                        .Replace("@From", cname.Trim())
                                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), MgmUser, null, "Litigation Response Added-Notice Title-" + FinalCaseTitle, message);
                            }
                            #endregion
                        }
                        if (newResponseID > 0)
                            saveSuccess = true;
                    }
                    else
                    {
                        CvResponseSaveMsg.IsValid = false;
                        CvResponseSaveMsg.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                        ValidationSummary10.CssClass = "alert alert-danger";
                    }

                    if (saveSuccess)
                    {
                        LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_LegalNoticeResponse", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Response Created", true);
                        //Save Notice Response Uploaded Documents
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS Upload Document

                            if (fuResponseDocUpload.HasFiles)
                            {
                                tbl_LitigationFileData objResponseDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = noticeInstanceID,
                                    DocTypeInstanceID = newResponseID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "NR"
                                };


                                if (fileCollection.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                    string directoryPath = "";
                                    String fileName = "";

                                    if (newResponseID > 0)
                                    {
                                        for (int i = 0; i < fileCollection.Count; i++)
                                        {
                                            int customerID = -1;
                                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            HttpPostedFile uploadedFile = fileCollection[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("fuResponseDocUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                objResponseDoc.FileName = fileName;

                                                //Get Document Version
                                                var responseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objResponseDoc);

                                                responseDocVersion++;
                                                objResponseDoc.Version = responseDocVersion + ".0";
                                                directoryPath = "LitigationDocuments\\" + customerID + "\\Notice\\" + noticeInstanceID + "\\" + "Response\\" + objResponseDoc.Version;
                                                IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                if (!di.Exists)
                                                {
                                                    di.Create();
                                                }
                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                                string p_strPath = string.Empty;
                                                string dirpath = string.Empty;
                                                //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                                                //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                                dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;
                                                
                                                //if (File.Exists(p_strPath))
                                                //    File.Delete(p_strPath);
                                                if (!Directory.Exists(dirpath))
                                                {
                                                    Directory.CreateDirectory(dirpath);
                                                }
                                                FileStream objFileStrm = File.Create(p_strPath);
                                                objFileStrm.Close();
                                                File.WriteAllBytes(p_strPath, bytes);

                                                Guid fileKey1 = Guid.NewGuid();
                                                
                                                objResponseDoc.FilePath = directoryPath.Replace(@"\", "/");
                                                objResponseDoc.FileKey = Convert.ToString(fileKey1);
                                                objResponseDoc.VersionDate = DateTime.Now;
                                                objResponseDoc.CreatedOn = DateTime.Now;
                                                objResponseDoc.FileSize = uploadedFile.ContentLength;
                                                //DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                FileInfo localFile = new FileInfo(p_strPath);
                                                string AWSpath = directoryPath + "\\" + uploadedFile.FileName;
                                                S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                if (!s3File.Exists)
                                                {
                                                    using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                    {
                                                        localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                    }
                                                }
                                                saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objResponseDoc);

                                                fileList.Clear();
                                            }

                                        }//End For Each 
                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Response Document(s) Uploaded", true);
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            #region Upload Document

                            if (fuResponseDocUpload.HasFiles)
                            {
                                tbl_LitigationFileData objResponseDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = noticeInstanceID,
                                    DocTypeInstanceID = newResponseID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "NR"
                                };


                                if (fileCollection.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                    string directoryPath = "";
                                    String fileName = "";

                                    if (newResponseID > 0)
                                    {
                                        for (int i = 0; i < fileCollection.Count; i++)
                                        {
                                            int customerID = -1;
                                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            HttpPostedFile uploadedFile = fileCollection[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("fuResponseDocUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                objResponseDoc.FileName = fileName;

                                                //Get Document Version
                                                var responseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objResponseDoc);

                                                responseDocVersion++;
                                                objResponseDoc.Version = responseDocVersion + ".0";

                                                directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + noticeInstanceID + "/Response/" + objResponseDoc.Version);

                                                if (!Directory.Exists(directoryPath))
                                                    Directory.CreateDirectory(directoryPath);

                                                Guid fileKey1 = Guid.NewGuid();
                                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                objResponseDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                objResponseDoc.FileKey = Convert.ToString(fileKey1);
                                                objResponseDoc.VersionDate = DateTime.Now;
                                                objResponseDoc.CreatedOn = DateTime.Now;
                                                objResponseDoc.FileSize = uploadedFile.ContentLength;
                                                DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objResponseDoc);

                                                fileList.Clear();
                                            }

                                        }//End For Each 
                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Response Document(s) Uploaded", true);
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        //Update Status
                        #region Status Transaction

                        tbl_LegalNoticeStatusTransaction newStatusRecord = new tbl_LegalNoticeStatusTransaction()
                        {
                            NoticeInstanceID = noticeInstanceID,
                            StatusID = 2,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            IsDeleted = false,
                            UserID = AuthenticationHelper.UserID,
                            RoleID = 3,
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };

                        if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusRecord))
                        {
                            saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusRecord);
                            saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusRecord);
                        }

                        #endregion

                        if (saveSuccess)
                        {
                            clearResponseControls();

                            CvResponseSaveMsg.IsValid = false;
                            CvResponseSaveMsg.ErrorMessage = "Response Details Saved Successfully.";
                            ValidationSummary10.CssClass = "alert alert-success";
                        }

                        //Re-Bind Notice Action Log Details
                        BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSaveStatus_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                int Noticeresult = 0;
                string NoticeStatus = string.Empty;
                if (ViewState["noticeInstanceID"] != null)
                {
                    if (!String.IsNullOrEmpty(ddlNoticeStatus.SelectedValue))
                    {
                        long noticeInstanceID = 0;
                        noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                        if (noticeInstanceID != 0)
                        {
                            int selectedStatusID = Convert.ToInt32(ddlNoticeStatus.SelectedValue);

                            //Status Transaction Record - Which will Create or Update on Each Status Move
                            tbl_LegalNoticeStatusTransaction newStatusTxnRecord = new tbl_LegalNoticeStatusTransaction()
                            {
                                NoticeInstanceID = noticeInstanceID,
                                StatusID = selectedStatusID,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false,
                                UserID = AuthenticationHelper.UserID,
                                RoleID = 3,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            if (!string.IsNullOrEmpty(ddlNoticeResult.SelectedValue))
                            {
                                if (ddlNoticeResult.SelectedValue != "0")
                                {
                                    //Noticeresult = Convert.ToInt32(ddlNoticeResult.SelectedValue);
                                    //NoticeStatus = ddlNoticeResult.SelectedItem.Text;

                                    Noticeresult = Convert.ToInt32(ddlNoticeResult.SelectedValue);
                                    NoticeStatus = ddlNoticeStatus.SelectedItem.Text;
                                }
                            }
                            else
                            {
                                Noticeresult = Convert.ToInt32(ddlNoticeResult.Items.FindByText("In Progress").Value);
                                NoticeStatus = "In Progress";
                            }
                            tbl_LegalNoticeInstance objNoticeInstance = new tbl_LegalNoticeInstance()
                            {
                                ID = noticeInstanceID,
                                NoticeResult = Noticeresult,
                                NoticeStage = Convert.ToString(ddlNoticeStage.SelectedValue)
                            };

                            saveSuccess = CaseManagement.UpdateNoticeResult(objNoticeInstance);
                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_LegalNoticeInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Result Updated", true);
                            }
                            //Status Record - i.e. Notice Closure Record; Only Active on Notice Close otherwise DeActive 
                            tbl_LegalNoticeStatus newStatusRecord = new tbl_LegalNoticeStatus()
                            {
                                NoticeInstanceID = noticeInstanceID,
                                StatusID = selectedStatusID,
                                //CloseDate = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false,

                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            if (tbxNoticeCloseDate.Text != "")
                            {
                                newStatusRecord.CloseDate = Convert.ToDateTime(tbxNoticeCloseDate.Text);
                            }

                            if (tbxCloseRemark.Text != "")
                                newStatusRecord.ClosureRemark = tbxCloseRemark.Text;

                            if (ddlNoticeStatus.SelectedValue != "3") //Open or In Progress
                            {
                                if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusTxnRecord))
                                {
                                    saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusTxnRecord);
                                    saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusTxnRecord);
                                    cvNoticeStatus.IsValid = false;
                                    cvNoticeStatus.ErrorMessage = "Notice Status Saved Successfully.";
                                    ValidationSummary3.CssClass = "alert alert-success";
                                }
                                else
                                {
                                    saveSuccess = NoticeManagement.UpdateNoticeStatusTransaction(newStatusTxnRecord);
                                    cvNoticeStatus.IsValid = false;
                                    cvNoticeStatus.ErrorMessage = "Notice Status Updated Successfully.";
                                    ValidationSummary3.CssClass = "alert alert-success";
                                }

                                //If Exists Notice Closure Record then DeActive it
                                if (NoticeManagement.ExistNoticeStatus(newStatusRecord))
                                {
                                    saveSuccess = NoticeManagement.UpdateNoticeStatus(newStatusRecord);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_LegalNoticeStatus", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Status Updated", true);
                                    }
                                }
                              
                            }
                            else if (ddlNoticeStatus.SelectedValue == "3") //Close
                            {
                                if (tbxNoticeCloseDate.Text != "")
                                {
                                   
                                    //Create or Update Status Transaction Records
                                    if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusTxnRecord))
                                    {
                                        saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusTxnRecord);
                                        saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusTxnRecord);
                                        cvNoticeStatus.IsValid = false;
                                        cvNoticeStatus.ErrorMessage = "Notice Status Saved Successfully.";
                                        ValidationSummary3.CssClass = "alert alert-success";
                                    }
                                    else
                                    {
                                        saveSuccess = NoticeManagement.UpdateNoticeStatusTransaction(newStatusTxnRecord);
                                        cvNoticeStatus.IsValid = false;
                                        cvNoticeStatus.ErrorMessage = "Notice Status Updated Successfully.";
                                        ValidationSummary3.CssClass = "alert alert-success";
                                    }
                                    //Create or Update Status Record
                                    if (!NoticeManagement.ExistNoticeStatus(newStatusRecord))
                                    {
                                        saveSuccess = NoticeManagement.CreateNoticeStatus(newStatusRecord);
                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_LegalNoticeStatus", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Status Created", true);
                                        }
                                    }
                                    else
                                    {
                                        saveSuccess = NoticeManagement.UpdateNoticeStatus(newStatusRecord);
                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_LegalNoticeStatus", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, " Notice Status Updated", true);
                                        }
                                    }
                                    var customizedid = GetCustomizedCustomerid();
                                    if (customizedid == AuthenticationHelper.CustomerID)
                                    {
                                        divNoticeDocumentControls.Visible = false;
                                        ddlNoticeStatus.Enabled = false;
                                        ddlNoticeResult.Enabled = false;
                                        tbxNoticeCloseDate.Enabled = false;
                                        //tbxAppealCaseNo.Enabled = false;
                                        tbxCloseRemark.Enabled = false;
                                        btnSaveStatus.Enabled = false;
                                    }
                                    if (saveSuccess)
                                    {
                                        #region Mail Data
                                        User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                                        string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                                        List<string> NoticeOwnerandIUser = NoticeManagement.getNoticeOwnerAndInternalUser(Convert.ToInt32(noticeInstanceID), AuthenticationHelper.CustomerID);
                                        var NoticeRecord = NoticeManagement.GetNoticeByID(Convert.ToInt32(noticeInstanceID));
                                        var Locations = string.Empty;
                                        if (!string.IsNullOrEmpty(Convert.ToString(NoticeRecord.CustomerBranchID)))
                                        {
                                            Locations = CaseManagement.GetLocationByCaseInstanceID(NoticeRecord.CustomerBranchID);
                                        }

                                        List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                                        List<string> UniqueMail = new List<string>();
                                        if (NoticeOwnerandIUser.Count > 0)
                                        {
                                            foreach (var item in NoticeOwnerandIUser)
                                            {
                                                if (MgmUser.Count > 0)
                                                {
                                                    if (!UniqueMail.Contains(item))
                                                    {
                                                        UniqueMail.Add(item);
                                                    }
                                                }
                                                else
                                                    UniqueMail.Add(item);
                                            }
                                        }
                                        //if (MgmUser.Count > 0)
                                        //{
                                        //    foreach (var item in MgmUser)
                                        //    {
                                        //        if (!UniqueMail.Contains(item))
                                        //        {
                                        //            UniqueMail.Add(item);
                                        //        }
                                        //    }
                                        //}

                                        string NoticeTitleMerge = NoticeRecord.NoticeTitle;
                                        string FinalNoticeTitle = string.Empty;
                                        if (NoticeTitleMerge.Length > 50)
                                        {
                                            FinalNoticeTitle = NoticeTitleMerge.Substring(0, 50);
                                            FinalNoticeTitle = FinalNoticeTitle + "...";
                                        }
                                        else
                                        {
                                            FinalNoticeTitle = NoticeTitleMerge;
                                        }

                                        #endregion end mail

                                        #region Send Mail to managment after Hearing update

                                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                        if (UniqueMail.Count > 0)
                                        {
                                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_NoticeClose
                                                                    .Replace("@UpdatedUser", username)
                                                                    .Replace("@NoticeRefNo", NoticeRecord.RefNo)
                                                                    .Replace("@NoticeTitle", NoticeRecord.NoticeTitle)
                                                                    .Replace("@Location", Locations)
                                                                    .Replace("@CloseDate", tbxNoticeCloseDate.Text)
                                                                    .Replace("@NoticeStatus", NoticeStatus)
                                                                    .Replace("@NotceRemark", tbxCloseRemark.Text)
                                                                    .Replace("@AccessURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                                    .Replace("@From", cname.Trim())
                                                                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), MgmUser, null, "Litigation Notice Closed - " + FinalNoticeTitle, message);
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    cvNoticeStatus.IsValid = false;
                                    cvNoticeStatus.ErrorMessage = "Please Provide Close Date.";
                                    ValidationSummary3.CssClass = "alert alert-danger";
                                    return;
                                }
                            }

                            if (saveSuccess)
                            {
                                ViewState["noticeStatus"] = selectedStatusID;

                                cvNoticeStatus.IsValid = false;
                                cvNoticeStatus.ErrorMessage = "Details Saved Successfully.";
                                ValidationSummary3.CssClass = "alert alert-success";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void btnSaveConvertToCase_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;

                if (ViewState["noticeInstanceID"] != null)
                {
                    if (!String.IsNullOrEmpty(ddlNoticeStatus.SelectedValue))
                    {
                        if (ddlNoticeStatus.SelectedValue == "3") //Status - Closed
                        {
                            if (tbxNoticeCloseDate.Text != "")
                            {
                                bool existCaseNo = false;
                                if (!string.IsNullOrEmpty(tbxAppealCaseNo.Text.Trim()))
                                    existCaseNo = CaseManagement.ExistsCourtCaseNo(tbxAppealCaseNo.Text, 0);

                                if (!existCaseNo)
                                {
                                    long oldNoticeInstanceID = 0;
                                    oldNoticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                                    if (oldNoticeInstanceID != 0)
                                    {
                                        if (ddlNoticeCategory.SelectedValue != "" && ddlNoticeCategory.SelectedValue != "0")
                                        {
                                            int noticeTypeID = 0;
                                            noticeTypeID = Convert.ToInt32(ddlNoticeCategory.SelectedValue);

                                            if (noticeTypeID != 0)
                                            {
                                                var lstCustomFieldsCaseWise = CaseManagement.GetCustomsFields(Convert.ToInt32(AuthenticationHelper.CustomerID), oldNoticeInstanceID, 2, noticeTypeID);
                                                tbl_CaseType ObjCaseTypeDetail = LitigationCourtAndCaseType.GetLegalCaseTypeDetailByID(noticeTypeID, CustomerID);

                                                if (ObjCaseTypeDetail != null)
                                                {
                                                    bool IsCarryForwardAllowed = false;

                                                    if (ObjCaseTypeDetail.IsCarryForwardAllowed)
                                                        IsCarryForwardAllowed = Convert.ToBoolean(ObjCaseTypeDetail.IsCarryForwardAllowed);

                                                    if (IsCarryForwardAllowed && lstCustomFieldsCaseWise.Count > 0)
                                                    {
                                                        //Open Custom Field Grid                                   
                                                        grdCustomField_CaseTransfer.DataSource = lstCustomFieldsCaseWise;
                                                        grdCustomField_CaseTransfer.DataBind();

                                                        if (lstCustomFieldsCaseWise.Count <= 0)
                                                            divCustomField.Visible = false;
                                                        else
                                                            divCustomField.Visible = true;
                                                    }
                                                    else //Not Allowed then Notice to Case transfer, Add All Custom Fields as it is 
                                                    {
                                                        long newCaseID = 0;
                                                        saveSuccess = saveNoticeToCaseTransfer(oldNoticeInstanceID, out newCaseID);

                                                        if (saveSuccess && newCaseID != 0)
                                                        {
                                                            #region Save Custom Field

                                                            if (lstCustomFieldsCaseWise.Count > 0)
                                                            {
                                                                foreach (var item in lstCustomFieldsCaseWise)
                                                                {
                                                                    tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                                    {
                                                                        NoticeCaseType = 1,
                                                                        NoticeCaseInstanceID = newCaseID,
                                                                        LabelID = item.LableID,
                                                                        LabelValue = item.labelValue,
                                                                        Interest = item.Interest,
                                                                        Penalty = item.Penalty,
                                                                        Total = item.Total,
                                                                        SettlementValue = item.SettlementValue,
                                                                        ProvisionInBook = item.ProvisionInBook,
                                                                        IsActive = true,
                                                                        IsDeleted = false,
                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                        CreatedOn = DateTime.Now,
                                                                    };

                                                                    saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                            saveSuccess = false;

                                                       
                                                        var customizedid = GetCustomizedCustomerid();
                                                        if (customizedid == AuthenticationHelper.CustomerID)
                                                        {
                                                            divNoticeDocumentControls.Visible = false;
                                                            ddlNoticeStatus.Enabled = false;
                                                            ddlNoticeResult.Enabled = false;
                                                            tbxNoticeCloseDate.Enabled = false;
                                                            tbxAppealCaseNo.Enabled = false;
                                                            tbxCloseRemark.Enabled = false;
                                                            btnSaveStatus.Enabled = false;
                                                            btnSaveConvertCase.Enabled = false;
                                                        }
                                                        if (saveSuccess)
                                                        {
                                                            tbxAppealCaseNo.Text = string.Empty;
                                                            cvNoticeStatus.IsValid = false;
                                                            cvNoticeStatus.ErrorMessage = "Notice Converted to Case Successfully. Please See and Update the details(if required) Under Case List";
                                                            ValidationSummary3.CssClass = "alert alert-success";
                                                        }
                                                    }
                                                }
                                            }//Notice Type Check
                                        }
                                    }//InstanceID 0 Check
                                }
                                else
                                {
                                    saveSuccess = false;
                                    cvNoticeStatus.IsValid = false;
                                    cvNoticeStatus.ErrorMessage = "Court Case No. already exists";
                                    ValidationSummary3.CssClass = "alert alert-danger";
                                }
                            }
                            else
                            {
                                saveSuccess = false;
                                cvNoticeStatus.IsValid = false;
                                cvNoticeStatus.ErrorMessage = "Please Provide Close Date";
                                ValidationSummary3.CssClass = "alert alert-danger";
                            }
                        }
                        else
                        {
                            cvNoticeStatus.IsValid = false;
                            cvNoticeStatus.ErrorMessage = "Selected Status must be Close";
                            ValidationSummary3.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnPaymentSave_Click(object sender, EventArgs e)
        {
            try
            {
                TextBox tbxPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentDate");
                DropDownList ddlPaymentType = (DropDownList)grdNoticePayment.FooterRow.FindControl("ddlPaymentType");
                TextBox tbxAmount = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxAmount");
                TextBox tbxPaymentRemark = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentRemark");
                TextBox tbxAmountPaid = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxAmountPaid");
                TextBox tbxAmountTaxPaid = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxAmountTaxPaid");
                TextBox tbxInvoiceNo = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxInvoiceNo");
                DropDownList ddlLawyer = (DropDownList)grdNoticePayment.FooterRow.FindControl("ddlLawyer");
                TextBox tbxPaymentID = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentID");
                FileUpload fu = (FileUpload)grdNoticePayment.FooterRow.FindControl("fuSampleFileNew");
                long newPaymentID = 0;
                if (ViewState["noticeInstanceID"] != null)
                {
                    if (tbxPaymentDate != null && ddlPaymentType != null && tbxAmount != null && tbxPaymentRemark != null)
                    {
                        bool validateData = false;
                        bool saveSuccess = false;

                        if (tbxPaymentDate.Text != "")
                        {
                            if (!String.IsNullOrEmpty(ddlPaymentType.SelectedValue))
                            {
                                if (tbxAmount.Text != "")
                                {
                                    if (tbxPaymentRemark.Text != "")
                                    {
                                        if (tbxAmountPaid.Text != "")
                                        {
                                            try
                                            {
                                                Convert.ToDecimal(tbxAmount.Text);
                                                validateData = true;
                                            }
                                            catch (Exception ex)
                                            {
                                                validateData = false;
                                            }
                                        }
                                        else
                                        {
                                            cvNoticePayment.IsValid = false;
                                            cvNoticePayment.ErrorMessage = "Please Provide Amount Paid.";
                                            ValidationSummary4.CssClass = "alert alert-danger";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        cvNoticePayment.IsValid = false;
                                        cvNoticePayment.ErrorMessage = "Please Provide Payment Remark.";
                                        ValidationSummary4.CssClass = "alert alert-danger";
                                        return;
                                    }
                                }
                                else
                                {
                                    cvNoticePayment.IsValid = false;
                                    cvNoticePayment.ErrorMessage = "Please Provide Payment Amount.";
                                    ValidationSummary4.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvNoticePayment.IsValid = false;
                                cvNoticePayment.ErrorMessage = "Please Select Payment Type.";
                                ValidationSummary4.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                        else
                        {
                            cvNoticePayment.IsValid = false;
                            cvNoticePayment.ErrorMessage = "Please provide payment Date.";
                            ValidationSummary4.CssClass = "alert alert-danger";
                            return;
                        }
                       
                            #region Upload Document
                            HttpFileCollection fileCollection = Request.Files;
                            if (fileCollection.Count > 0)
                            {
                                bool isBlankFile = false;
                                string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = null;
                                    uploadfile = fileCollection[i];
                                    int filelength = uploadfile.ContentLength;
                                    string fileName = Path.GetFileName(uploadfile.FileName);
                                    string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                                    if (!string.IsNullOrEmpty(fileName))
                                    {
                                        if (filelength == 0)
                                        {
                                            isBlankFile = true;
                                            break;
                                        }
                                        else if (ext == "")
                                        {
                                            isBlankFile = true;
                                            break;
                                        }
                                        else
                                        {
                                            if (ext != "")
                                            {
                                                for (int j = 0; j < InvalidFileTypes.Length; j++)
                                                {
                                                    if (ext == "." + InvalidFileTypes[j])
                                                    {
                                                        isBlankFile = true;
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }

                            }

                            #endregion
                       
                        if (validateData)
                        {
                            tbl_NoticeCasePayment newRecord = new tbl_NoticeCasePayment()
                            {
                                NoticeOrCase = "N",
                                IsActive = true,
                                NoticeOrCaseInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]),
                                PaymentDate = DateTimeExtensions.GetDate(tbxPaymentDate.Text),
                                PaymentID = Convert.ToInt32(ddlPaymentType.SelectedValue),
                                Amount = Convert.ToDecimal(tbxAmount.Text),
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                AmountPaid = Convert.ToInt32(tbxAmountPaid.Text),
                                Lawyer = Convert.ToString(ddlLawyer.SelectedItem),
                                Remark = Convert.ToString(tbxPaymentRemark.Text)
                            };
                            
                            if (tbxAmountTaxPaid.Text != null && tbxAmountTaxPaid.Text != "")
                                newRecord.AmountTax = Convert.ToDecimal(tbxAmountTaxPaid.Text);

                            if (tbxPaymentRemark.Text != "")
                                newRecord.Remark = tbxPaymentRemark.Text;

                            if (tbxInvoiceNo.Text != "")
                                newRecord.InvoiceNo = tbxInvoiceNo.Text;
                            //if (ddlHearingID.SelectedValue == "")
                            //{
                            //    newRecord.HearingID = null;
                            //}
                            //else
                            //{
                            //    newRecord.HearingID = Convert.ToInt32(ddlHearingID.SelectedValue);
                            //}

                            #region edit
                            if (Convert.ToString(ViewState["PaymentMode"]) != "Edit")
                            {
                                saveSuccess = NoticeManagement.CreateNoticePaymentLog(newRecord);
                            }

                            if (Convert.ToString(ViewState["PaymentMode"]) == "Edit")
                            {
                                newRecord.ID = Convert.ToInt32(tbxPaymentID.Text);
                                newPaymentID = NoticeManagement.UpdatePayment(newRecord);
                                HttpFileCollection fileCollectionnew = Request.Files;
                                var fname = GetOldFilename(newRecord.ID);
                                if(fu.FileName=="")
                                {

                                }
                               else if (fu.FileName != fname )
                                {
                                    RemoveOldDocument(newRecord.ID);
                                }
                                else
                                {

                                }

                            }


                            if (newPaymentID > 0)
                                saveSuccess = true;
                            #endregion
                            ViewState["Newpaymentid"] = newRecord.ID;
                            ViewState["invoiceno"] = newRecord.ID;
                            //ViewState["invoiceno"] = newRecord.InvoiceNo;

                            // saveSuccess = NoticeManagement.CreateNoticePaymentLog(newRecord);
                        }

                        if (saveSuccess)
                        {
                            uploadDocuments(Convert.ToInt32(ViewState["noticeInstanceID"]), Request.Files, "NoticeCaseFileUpload", Convert.ToInt32(ViewState["Newpaymentid"]), Convert.ToInt32(ViewState["invoiceno"]));
                            LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_NoticeCasePayment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Payment Added", true);
                            //Re-Bind Notice Payment Log Details
                            BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                            cvNoticePayment.IsValid = false;
                            cvNoticePayment.ErrorMessage = "Payment Details Saved Successfully.";
                            ValidationSummary4.CssClass = "alert alert-success";
                            ViewState["PaymentMode"] = "Add";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void RemoveOldDocument(long paymentid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var query = (from row in entities.tbl_LitigationPaymentFileData
                                 where row.NoticeCasePaymentId == paymentid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        entities.tbl_LitigationPaymentFileData.Remove(query);
                        entities.SaveChanges();
                       
                    }
                 
                      

                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
              
            }
        }
        protected bool uploadDocuments(long contractID, HttpFileCollection fileCollection, string fileUploadControlName, long paymentid, long invoiceno)
        {
            bool saveSuccess = false;
            try
            {
                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                if (AWSData != null)
                {
                    #region AWS Document
                    
                    if (ViewState["noticeInstanceID"] != null)
                    {

                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        string Flag = "PaymentNotice";
                        string directoryPath = string.Empty;
                        string fileName = string.Empty;
                        string docType = string.Empty;


                        tbl_LitigationPaymentFileData objNoticeCaseDoc = new tbl_LitigationPaymentFileData()
                        {
                            NoticeCasePaymentId = paymentid,
                            NoticeCaseInstanceID = Convert.ToInt32(contractID),
                            InvoiceNo = invoiceno,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                            IsDeleted = false,
                            DocType = "N",

                        };

                        if (fileCollection.Count > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();


                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadedFile = fileCollection[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                    if (keys1[keys1.Count() - 1].Equals("LitigationFileUpload"))
                                    {
                                        fileName = uploadedFile.FileName;
                                    }

                                    //Get Document Version
                                    var caseDocVersion = CaseManagement.ExistsCasePaymentDocumentReturnVersion(objNoticeCaseDoc);

                                    caseDocVersion++;
                                    objNoticeCaseDoc.Version = caseDocVersion + ".0";

                                    directoryPath = "LitigationDocuments\\" + customerID + "\\" + Flag + "\\" + Convert.ToInt32(contractID) + "\\" + Flag + "Document\\" + objNoticeCaseDoc.Version;
                                    //directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/" + Flag + "/" + Convert.ToInt32(contractID) + "/" + Flag + "Document/" + objNoticeCaseDoc.Version);

                                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                    if (!di.Exists)
                                    {
                                        di.Create();
                                    }
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                    string p_strPath = string.Empty;
                                    string dirpath = string.Empty;
                                    //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                                    //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                    dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;
                                    
                                    //if (File.Exists(p_strPath))
                                    //    File.Delete(p_strPath);
                                    if (!Directory.Exists(dirpath))
                                    {
                                        Directory.CreateDirectory(dirpath);
                                    }
                                    FileStream objFileStrm = File.Create(p_strPath);
                                    objFileStrm.Close();
                                    File.WriteAllBytes(p_strPath, bytes);

                                    Guid fileKey1 = Guid.NewGuid();
                                    string AWSpath = directoryPath + "\\" + uploadedFile.FileName;
                                    
                                    objNoticeCaseDoc.FilePath = directoryPath.Replace(@"\", "/");
                                    objNoticeCaseDoc.FileKey = fileKey1.ToString();
                                    objNoticeCaseDoc.VersionDate = DateTime.Now;
                                    objNoticeCaseDoc.CreatedOn = DateTime.Now;
                                    objNoticeCaseDoc.FileSize = uploadedFile.ContentLength;
                                    objNoticeCaseDoc.FileName = uploadedFile.FileName;

                                    FileInfo localFile = new FileInfo(p_strPath);
                                    S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                    if (!s3File.Exists)
                                    {
                                        using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                        {
                                            localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                        }
                                    }
                                    //DocumentManagement.Litigation_SaveDocFiles(fileList);

                                    int FileID = CaseManagement.CreateCaseDocumentPaymentMappingGetID(objNoticeCaseDoc);
                                    if (FileID != 0)
                                    {
                                        saveSuccess = true;
                                    }

                                    List<tbl_Litigation_FileDataTagsMapping> lstFileTagMapping = new List<tbl_Litigation_FileDataTagsMapping>();


                                    tbl_Litigation_FileDataTagsMapping objFileTagMapping = new tbl_Litigation_FileDataTagsMapping()
                                    {
                                        FileID = FileID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                        UpdatedOn = DateTime.Now,
                                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    };
                                    lstFileTagMapping.Add(objFileTagMapping);


                                    if (lstFileTagMapping.Count > 0)
                                    {
                                        saveSuccess = CaseManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                    }

                                    fileList.Clear();
                                }
                            }//End For Each
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Upload Document


                    if (ViewState["noticeInstanceID"] != null)
                    {

                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        string Flag = "PaymentNotice";
                        string directoryPath = string.Empty;
                        string fileName = string.Empty;
                        string docType = string.Empty;


                        tbl_LitigationPaymentFileData objNoticeCaseDoc = new tbl_LitigationPaymentFileData()
                        {
                            NoticeCasePaymentId = paymentid,
                            NoticeCaseInstanceID = Convert.ToInt32(contractID),
                            InvoiceNo = invoiceno,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                            IsDeleted = false,
                            DocType = "N",

                        };

                        if (fileCollection.Count > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();


                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadedFile = fileCollection[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                    if (keys1[keys1.Count() - 1].Equals("LitigationFileUpload"))
                                    {
                                        fileName = uploadedFile.FileName;
                                    }

                                    //Get Document Version
                                    var caseDocVersion = CaseManagement.ExistsCasePaymentDocumentReturnVersion(objNoticeCaseDoc);

                                    caseDocVersion++;
                                    objNoticeCaseDoc.Version = caseDocVersion + ".0";

                                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/" + Flag + "/" + Convert.ToInt32(contractID) + "/" + Flag + "Document/" + objNoticeCaseDoc.Version);

                                    if (!Directory.Exists(directoryPath))
                                        Directory.CreateDirectory(directoryPath);

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                    objNoticeCaseDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    objNoticeCaseDoc.FileKey = fileKey1.ToString();
                                    objNoticeCaseDoc.VersionDate = DateTime.Now;
                                    objNoticeCaseDoc.CreatedOn = DateTime.Now;
                                    objNoticeCaseDoc.FileSize = uploadedFile.ContentLength;
                                    objNoticeCaseDoc.FileName = uploadedFile.FileName;
                                    DocumentManagement.Litigation_SaveDocFiles(fileList);
                                    int FileID = CaseManagement.CreateCaseDocumentPaymentMappingGetID(objNoticeCaseDoc);
                                    if (FileID != 0)
                                    {
                                        saveSuccess = true;
                                    }

                                    List<tbl_Litigation_FileDataTagsMapping> lstFileTagMapping = new List<tbl_Litigation_FileDataTagsMapping>();


                                    tbl_Litigation_FileDataTagsMapping objFileTagMapping = new tbl_Litigation_FileDataTagsMapping()
                                    {
                                        FileID = FileID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                        UpdatedOn = DateTime.Now,
                                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    };
                                    lstFileTagMapping.Add(objFileTagMapping);


                                    if (lstFileTagMapping.Count > 0)
                                    {
                                        saveSuccess = CaseManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                    }

                                    fileList.Clear();
                                }
                            }//End For Each
                        }
                    }
                    #endregion
                }
                return saveSuccess;

                //return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        public bool SendTaskAssignmentMail(tbl_TaskScheduleOn taskRecord, string accessURL, string assignedBy)
        {
            try
            {
                List<string> OwnerMailList = new List<string>();
                User NoticeOwner = null;
                User TaskAssignedUserDetail = null;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                #region Mail Data
                User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                var NoticeRecord = NoticeManagement.GetNoticeByID(Convert.ToInt32(taskRecord.NoticeCaseInstanceID));

                if (!string.IsNullOrEmpty(Convert.ToString(NoticeRecord.OwnerID)))
                {
                    NoticeOwner = UserManagement.GetByID(Convert.ToInt32(NoticeRecord.OwnerID));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(taskRecord.AssignTo)))
                {
                    TaskAssignedUserDetail = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));
                }
                var Locations = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(NoticeRecord.CustomerBranchID)))
                {
                    Locations = CaseManagement.GetLocationByCaseInstanceID(NoticeRecord.CustomerBranchID);
                }
                OwnerMailList.Add(NoticeOwner.Email);
                List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                List<string> UniqueMail = new List<string>();
                if (OwnerMailList.Count > 0)
                {
                    foreach (var item in OwnerMailList)
                    {
                        if (!UniqueMail.Contains(item))
                        {
                            UniqueMail.Add(item);
                        }
                    }
                }
                if (MgmUser.Count > 0)
                {
                    foreach (var item in MgmUser)
                    {
                        if (!UniqueMail.Contains(item))
                        {
                            UniqueMail.Add(item);
                        }
                    }
                }

                string NoticeTitleMerge = NoticeRecord.NoticeTitle;
                string FinalNoticeTitle = string.Empty;
                if (NoticeTitleMerge.Length > 50)
                {
                    FinalNoticeTitle = NoticeTitleMerge.Substring(0, 50);
                    FinalNoticeTitle = FinalNoticeTitle + "...";
                }
                else
                {
                    FinalNoticeTitle = NoticeTitleMerge;
                }

                #endregion end mail

                var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (taskRecord != null)
                {
                    User UserAssigeed = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));

                    if (UserAssigeed != null)
                    {
                        if (UserAssigeed.Email != null && UserAssigeed.Email != "")
                        {
                            string assignedToUserName = string.Format("{0} {1}", UserAssigeed.FirstName, UserAssigeed.LastName);

                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_TaskAssignment
                                                                       .Replace("@User", username)
                                                                       .Replace("@NoticeRefNo", NoticeRecord.RefNo)
                                                                       .Replace("@NoticeTitle", NoticeRecord.NoticeTitle)
                                                                       .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                                       .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                                       .Replace("@Location", Locations)
                                                                       .Replace("@AssignedBy", assignedToUserName)
                                                                       .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                                                                       .Replace("@From", cname.Trim())
                                                                       .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { TaskAssignedUserDetail.Email }), UniqueMail, null, "Litigation Notification Task Assigned - " + FinalNoticeTitle, message);
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public void clearTaskControls()
        {
            try
            {
                tbxTaskTitle.Text = "";
                tbxTaskDueDate.Text = "";
                tbxTaskDesc.Text = "";
                tbxTaskRemark.Text = "";
                tbxExpOutcome.Text = "";
                grdTaskEditDoc.DataSource = null;
                grdTaskEditDoc.DataBind();
                tbxTaskID.Text = "";

                ddlTaskPriority.ClearSelection();

                ddlTaskUserInternal.ClearSelection();
                ddlTaskUserLawyerAndExternal.ClearSelection();

                fuTaskDocUpload.Attributes.Clear();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearResponseControls()
        {
            try
            {
                tbxResponseDate.Text = "";
                ddlRespBy.ClearSelection();
                tbxRespThrough.Text = "";
                tbxRespRefNo.Text = "";
                tbxResponseDesc.Text = "";
                tbxResponseRemark.Text = "";
                tbxNoticeDueDate.Text = "";

                fuResponseDocUpload.Attributes.Clear();
                GrdResponseEditDocument.DataSource = null;
                GrdResponseEditDocument.DataBind();
                divResposeEditdoc.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearNoticeControls()
        {
            try
            {
                txtNoticeDate.Text = "";
                tbxRefNo.Text = "";
                DropDownListAuditScrutiny.ClearSelection();
                ddlParty.ClearSelection();
                ddlAct.ClearSelection();
                DropDownListChosen1.ClearSelection();
                tbxSection.Text = "";
                tbxNoticeTerm.Text = "";
                ddlNoticeCategory.ClearSelection();
                ddlNoticeStage.ClearSelection();
                tbxTitle.Text = "";
                tbxDescription.Text = "";
                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                //tvBranches.SelectedNode.Selected = false;
                ddlJurisdiction.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlCPDepartment.ClearSelection();
                ddlOwner.ClearSelection();
                ddlNoticeRisk.ClearSelection();
                tbxClaimedAmt.Text = "";
                tbxProbableAmt.Text = "";
                txtprotestmoney.Text = "";
                txtbankgurantee.Text = "";
                txtprovisionalamt.Text = "";

                rblPotentialImpact.ClearSelection();
                tbxMonetory.Text = "";
                tbxNonMonetory.Text = "";
                tbxNonMonetoryYears.Text = "";


                // DropDownListChosen1.SelectedValue = "";
                //NoticeFileUpload.Attributes.Clear();

                ddlLawFirm.ClearSelection();
                lstBoxPerformer.ClearSelection();
                lstBoxLawyerUser.ClearSelection();
                lstBoxOppositionLawyer.ClearSelection();

                ddlReviewer.ClearSelection();
                if(RiskType == true)
                {
                    ddlRisk.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableNoticeSummaryTabControls(bool flag)
        {
            try
            {
                rbNoticeInOutType.Enabled = flag;
                txtNoticeDate.Enabled = flag;
                txtIntrest.Enabled = flag;
                txtPenalty.Enabled = flag;
                txttaxDemand.Enabled = flag;
                txtProvisonbook.Enabled = flag;
                tbxRefNo.Enabled = flag;
                DropDownListAuditScrutiny.Enabled = flag;
                ddlParty.Enabled = flag;
                ddlAct.Enabled = flag;
                tbxSection.Enabled = flag;
                tbxNoticeTerm.Enabled = flag;
                ddlNoticeCategory.Enabled = flag;
                tbxTitle.Enabled = flag;
                tbxDescription.Enabled = flag;
                tbxBranch.Enabled = flag;
                //tvBranches.SelectedNode.Selected = false;
                ddlJurisdiction.Enabled = flag;
                ddlDepartment.Enabled = flag;
                ddlCPDepartment.Enabled = flag;
                ddlOwner.Enabled = flag;
                ddlNoticeRisk.Enabled = flag;
                tbxClaimedAmt.Enabled = flag;
                tbxProbableAmt.Enabled = flag;
                txtprotestmoney.Enabled = flag;
                txtbankgurantee.Enabled = flag;
                txtprovisionalamt.Enabled = flag;
                rblPotentialImpact.Enabled = flag;
                tbxMonetory.Enabled = flag;
                tbxNonMonetory.Enabled = flag;
                tbxNonMonetoryYears.Enabled = flag;
                DropDownListChosen1.Enabled = flag;


                tbxNoticeBudget.Enabled = flag;
                txtPreDeposit.Enabled = flag;
                txtPostDeposit.Enabled = flag;

                //NoticeFileUpload.Enabled = flag;

                ddlLawFirm.Enabled = flag;
                lstBoxOppositionLawyer.Enabled = flag;
                lstBoxPerformer.Enabled = flag;
                ddlReviewer.Enabled = flag;
                lstBoxLawyerUser.Enabled = flag;

                btnSave.Enabled = flag;
                btnClearNoticeDetail.Enabled = flag;

                if (flag)
                {
                    ddlState.Attributes.Remove("disabled");
                    ddlLawFirm.Attributes.Remove("disabled");
                    ddlFY.Attributes.Remove("disabled");
                    lstBoxPerformer.Attributes.Remove("disabled");
                    lstBoxLawyerUser.Attributes.Remove("disabled");
                    ddlReviewer.Attributes.Remove("disabled");
                    lstBoxOppositionLawyer.Attributes.Remove("disabled");
                    ddlAct.Attributes.Remove("disabled");
                    DropDownListChosen1.Attributes.Remove("disabled");
                    ddlParty.Attributes.Remove("disabled");
                    btnSave.Attributes.Remove("disabled");
                    btnClearNoticeDetail.Attributes.Remove("disabled");
                }
                else
                {
                    ddlLawFirm.Attributes.Add("disabled", "disabled");
                    ddlFY.Attributes.Add("disabled", "disabled");
                    lstBoxPerformer.Attributes.Add("disabled", "disabled");
                    lstBoxLawyerUser.Attributes.Add("disabled", "disabled");
                    ddlReviewer.Attributes.Add("disabled", "disabled");
                    lstBoxOppositionLawyer.Attributes.Add("disabled", "disabled");
                    ddlAct.Attributes.Add("disabled", "disabled");
                    DropDownListChosen1.Attributes.Add("disabled", "disabled");
                    ddlParty.Attributes.Add("disabled", "disabled");
                    ddlState.Attributes.Add("disabled", "disabled");
                    btnSave.Attributes.Add("disabled", "disabled");
                    btnClearNoticeDetail.Attributes.Add("disabled", "disabled");
                }

                //Custom Parameter
                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                {
                    if (grdCustomField_TaxLitigation != null)
                    {
                        grdCustomField_TaxLitigation.Enabled = flag;
                        HideShowGridColumns(grdCustomField_TaxLitigation, "Action", flag);
                    }
                }
                else
                {
                    if (grdCustomField != null)
                    {
                        grdCustomField.Enabled = flag;
                        HideShowGridColumns(grdCustomField, "Action", flag);
                    }
                }

                if (flag)
                {
                    divGridUserAssignment.Visible = false;
                    pnlNoticeAssignment.Visible = flag;
                }
                else
                {
                    divGridUserAssignment.Visible = true;
                    pnlNoticeAssignment.Visible = flag;
                }
                if (RiskType == true)
                {
                    ddlRisk.Enabled = flag;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void enableDisableTaskControls(bool flag)
        {
            try
            {

                tbxTaskTitle.Enabled = flag;
                tbxTaskDueDate.Enabled = flag;

                ddlTaskUserInternal.Enabled = flag;
                ddlTaskUserLawyerAndExternal.Enabled = flag;

                ddlTaskPriority.Enabled = flag;
                tbxTaskDesc.Enabled = flag;
                tbxTaskRemark.Enabled = flag;
                fuTaskDocUpload.Enabled = flag;

                btnTaskSave.Enabled = flag;
                btnTaskClear.Enabled = flag;

                grdTaskActivity.Enabled = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void enableDisableNoticePopUpControls(bool flag)
        {
            try
            {
                pnlNotice.Enabled = flag;
                pnlNoticeAssignment.Enabled = flag;
                pnlTask.Enabled = flag;
                pnlResponse.Enabled = flag;
                AddTask.Visible = flag;
                AddNewResponse.Visible = flag;

                btnSave.Enabled = flag;
                btnEditNoticeDetail.Enabled = flag;
                lnkActDetails.Enabled = flag;
                btnSendMailPopup.Enabled = flag;
                if (ddlNoticeStatus.SelectedValue == "1")
                {
                    btnSaveStatus.Enabled = true;
                }
                else
                {
                    btnSaveStatus.Enabled = flag;
                }
                //btnSaveStatus.Enabled = flag;

                //Custom Parameter
                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                {
                    if (grdCustomField_TaxLitigation != null)
                    {
                        grdCustomField_TaxLitigation.Enabled = flag;
                        HideShowGridColumns(grdCustomField_TaxLitigation, "Action", flag);
                    }
                }
                else
                {
                    if (grdCustomField != null)
                    {
                        grdCustomField.Enabled = flag;
                        HideShowGridColumns(grdCustomField, "Action", flag);
                    }
                }

                //if (grdTaskActivity.Columns[7] != null)
                //    grdTaskActivity.Columns[7].Visible = flag;
                // HideShowGridColumns(grdTaskActivity, "Action", flag);

                grdNoticePayment.ShowFooter = flag;
                //if (grdNoticePayment.Columns[6] != null)
                //    grdNoticePayment.Columns[6].Visible = flag;
                HideShowGridColumns(grdNoticePayment, "Action", flag);

                //User Assignment Grid Action Column
                if (grdUserAssignment != null)
                {
                    //if (grdUserAssignment.Columns[5] != null)
                    //    grdUserAssignment.Columns[5].Visible = flag;

                    HideShowGridColumns(grdUserAssignment, "Action", flag);
                }

                var customizedid = GetCustomizedCustomerid();
                if (customizedid == AuthenticationHelper.CustomerID)
                {
                    divNoticeDocumentControls.Visible = flag;
                    ddlNoticeStatus.Enabled = flag;
                    ddlNoticeResult.Enabled = flag;
                    tbxNoticeCloseDate.Enabled = flag;
                    tbxAppealCaseNo.Enabled = flag;
                    tbxCloseRemark.Enabled = flag;
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int GetCustomizedCustomerid()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "HideEditButtonClosedCase"
                            select row.ClientID).FirstOrDefault();

                return data;
            }
        }
        public void DownloadNoticeDocument(int noticeFileID)
        {
            try
            {
                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                if (AWSData != null)
                {
                    #region AWS Storage
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        var ComplianceData = NoticeManagement.GetNoticeDocumentByID(noticeFileID);

                        //var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                        //ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
                        //ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                        
                        string directoryPath = "~/TempDocuments/AWS/" + User;
                        
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                        }

                        if (ComplianceData != null)
                        {
                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + ComplianceData.FilePath;
                                request.Key = ComplianceData.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + ComplianceData.FileName);
                            }

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(ComplianceData.FileName));
                           
                            Response.BinaryWrite(DocumentManagement.ReadDocFiles(Server.MapPath(directoryPath) + "\\" + ComplianceData.FileName)); // create the file
                           
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                            
                        }
                    }
                    #endregion
                }
                else
                {
                    var file = NoticeManagement.GetNoticeDocumentByID(noticeFileID);
                    if (file != null)
                    {
                        if (file.FilePath != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (filePath != null && File.Exists(filePath))
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("N", noticeFileID, "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Document(s) Downloaded", false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void DeleteNoticeFile(int noticeFileID)
        {
            try
            {
                if (noticeFileID != 0)
                {
                    if (NoticeManagement.DeleteNoticeDocument(noticeFileID, AuthenticationHelper.UserID))
                    {
                        LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_LitigationFileData", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Document Deleted", true);
                        cvNoticeDocument.IsValid = false;
                        cvNoticeDocument.ErrorMessage = "Document Deleted Successfully.";
                        vsNoticeDocument.CssClass = "alert alert-success";
                        BindFileTags();
                    }
                    else
                    {
                        cvNoticeDocument.IsValid = false;
                        cvNoticeDocument.ErrorMessage = "Something went wrong, Please try again.";
                        vsNoticeDocument.CssClass = "alert alert-danger";
                        BindFileTags();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        public void DeleteNoticeResponse(int noticeResponseID)
        {
            try
            {
                if (noticeResponseID != 0)
                {
                    //Delete Response with Documents
                    if (NoticeManagement.DeleteNoticeResponseLog(noticeResponseID, AuthenticationHelper.UserID))
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "Document Deleted Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";

                        LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_LegalNoticeResponse", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Response Deleted", true);

                    }
                    else
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpResponse.IsValid = false;
                cvNoticePopUpResponse.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary1.CssClass = "alert alert-danger";
            }
        }

        public void DeleteTask(int taskID, long CustomerID)
        {
            try
            {
                if (taskID != 0)
                {
                    if (LitigationTaskManagement.DeleteTask(taskID, AuthenticationHelper.UserID, CustomerID))
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Task Details Deleted Successfully.";
                        ValidationSummary5.CssClass = "alert alert-success";

                        LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_TaskScheduleOn", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Deleted", true);
                    }
                    else
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        public void DeletePaymentLog(int noticePaymentID)
        {
            try
            {
                if (noticePaymentID != 0)
                {
                    if (NoticeManagement.DeleteNoticePaymentLog(noticePaymentID, AuthenticationHelper.UserID))
                    {
                        LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_NoticeCasePayment", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Payment Deleted", true);
                        cvNoticePayment.IsValid = false;
                        cvNoticePayment.ErrorMessage = "Payment Details Deleted Successfully.";
                        ValidationSummary4.CssClass = "alert alert-success";
                    }
                    else
                    {
                        cvNoticePayment.IsValid = false;
                        cvNoticePayment.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary4.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        protected void grdNoticeDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadNoticeDoc = (LinkButton)e.Row.FindControl("lnkBtnDownLoadNoticeDoc");

            if (lnkBtnDownLoadNoticeDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadNoticeDoc);
            }

            LinkButton lnkBtnDeleteNoticeDoc = (LinkButton)e.Row.FindControl("lnkBtnDeleteNoticeDoc");
            if (lnkBtnDeleteNoticeDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteNoticeDoc);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteNoticeDoc.Visible = false;
                    else
                        lnkBtnDeleteNoticeDoc.Visible = true;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                {
                    lnkBtnDeleteNoticeDoc.Visible = false;
                }
            }
        }

        protected void grdNoticeDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadNoticeDoc"))
                    {
                        DownloadNoticeDocument(Convert.ToInt32(e.CommandArgument));
                    }
                    else if (e.CommandName.Equals("DeleteNoticeDoc"))
                    {
                        DeleteNoticeFile(Convert.ToInt32(e.CommandArgument));

                        //Bind Notice Related Documents
                        if (ViewState["noticeInstanceID"] != null)
                            BindNoticeRelatedDocuments_All(Convert.ToInt32(ViewState["noticeInstanceID"]));
                    }
                    else if (e.CommandName.Equals("ViewNoticeDocView"))
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(e.CommandArgument));
                            if (AllinOneDocumentList != null)
                            {
                                if (AllinOneDocumentList != null)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                        request.Key = AllinOneDocumentList.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                    }
                                    string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                    DocumentPath = filePath1;
                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);                                    
                                    lblMessage.Text = "";
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenDocviewer();", true);
                                }
                            }
                        }
                        else
                        {
                            var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(e.CommandArgument));
                            if (AllinOneDocumentList != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                                if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (AllinOneDocumentList.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    DocumentPath = FileName;
                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenDocviewer();", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        protected void grdNoticeDocuments_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdNoticeDocuments.PageIndex = e.NewPageIndex;
                    BindNoticeRelatedDocuments_All(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnTaskReminder = (LinkButton)e.Row.FindControl("lnkBtnTaskReminder");

                if (lnkBtnTaskReminder != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskReminder);
                }

                LinkButton lnkBtnTaskResponse = (LinkButton)e.Row.FindControl("lnkBtnTaskResponse");

                if (lnkBtnTaskResponse != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskResponse);
                }

                LinkButton lnkBtnDeleteTask = (LinkButton)e.Row.FindControl("lnkBtnDeleteTask");

                if (lnkBtnDeleteTask != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteTask);
                }

                Label lblTaskStatus = e.Row.FindControl("lblTaskStatus") as Label;

                if (lblTaskStatus != null)
                {
                    if (lblTaskStatus.Text != "" && lblTaskStatus.Text != "Open")
                    {
                        GridView gvTaskResponses = e.Row.FindControl("gvTaskResponses") as GridView;

                        if (gvTaskResponses != null)
                        {
                            if (grdTaskActivity.DataKeys[e.Row.RowIndex].Value != null)
                            {
                                int taskID = 0;
                                taskID = Convert.ToInt32(grdTaskActivity.DataKeys[e.Row.RowIndex].Value);

                                BindTaskResponses(taskID, gvTaskResponses);
                            }
                        }
                    }
                    else
                    {
                        HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;
                        //Image imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as Image;
                        if (imgCollapseExpand != null)
                            imgCollapseExpand.Visible = false;
                    }


                    //Hide Close Task Button
                    LinkButton lnkBtnCloseTask = e.Row.FindControl("lnkBtnCloseTask") as LinkButton;
                    if (lnkBtnCloseTask != null)
                    {
                        if (lblTaskStatus.Text != "" && lblTaskStatus.Text == "Closed")
                            lnkBtnCloseTask.Visible = false;
                        else
                            lnkBtnCloseTask.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                    {
                        lnkBtnTaskReminder.Visible = false;
                        // lnkBtnTaskResponse.Visible = false;
                        lnkBtnDeleteTask.Visible = false;
                        lnkBtnCloseTask.Visible = false;
                    }
                }
            }
        }

        protected void grdTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdTaskActivity.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Related Documents
                    BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);

                    //ScriptManager.RegisterClientScriptBlock(Me.GridView1, Me.GetType(), "myScript", "alert('Done with paging');", True)
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (ViewState["noticeInstanceID"] != null)
                    {
                        int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                        int taskID = Convert.ToInt32(e.CommandArgument);

                        if (e.CommandName.Equals("DeleteTask"))
                        {
                            DeleteTask(taskID, AuthenticationHelper.CustomerID);

                            //Re-Bind Notice Related Documents
                            BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                        else if (e.CommandName.Equals("EditTaskDoc"))
                        {
                            ViewState["taskID"] = taskID;

                            if (taskID != 0)
                            {
                                var Caseresponse = NoticeManagement.GetNoticeTaskDetailByTaskID(noticeInstanceID, taskID, "N", AuthenticationHelper.CustomerID);
                                if (Caseresponse != null)
                                {
                                    tbxTaskDueDate.Text = Convert.ToString(Caseresponse.ScheduleOnDate);
                                    tbxTaskTitle.Text = Caseresponse.TaskTitle;
                                    tbxTaskDesc.Text = Caseresponse.TaskDesc;
                                    tbxExpOutcome.Text = Caseresponse.ExpOutcome;
                                    ddlTaskPriority.SelectedValue = Convert.ToString(Caseresponse.PriorityID);
                                    tbxTaskRemark.Text = Caseresponse.Remark;
                                    tbxTaskID.Text = Convert.ToString(Caseresponse.ID);

                                    var FindIntText = ddlTaskUserInternal.Items.FindByValue(Convert.ToString(Caseresponse.AssignTo));
                                    var FindExtText = ddlTaskUserLawyerAndExternal.Items.FindByValue(Convert.ToString(Caseresponse.AssignTo));
                                    if (FindIntText != null)
                                    {
                                        ddlTaskUserInternal.SelectedValue = Convert.ToString(Caseresponse.AssignTo);
                                    }
                                    if (FindExtText != null)
                                    {
                                        ddlTaskUserLawyerAndExternal.SelectedValue = Convert.ToString(Caseresponse.AssignTo);
                                    }
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                                    var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(noticeInstanceID, taskID, "NT");
                                    if (lstResponseDocument.Count > 0)
                                    {
                                        grdTaskEditDoc.DataSource = lstResponseDocument;
                                        grdTaskEditDoc.DataBind();
                                        DivTaskEdit.Visible = true;
                                        ViewState["TaskMode"] = "Edit";
                                    }
                                }
                            }
                        }
                        else if (e.CommandName.Equals("CloseTask"))
                        {
                            //Update Task Status to Closed and Expire URL
                            LitigationTaskManagement.UpdateTaskStatus(taskID, 3, AuthenticationHelper.UserID, AuthenticationHelper.CustomerID); //Status 3 - Closed

                            //Re-Bind Notice Related Documents
                            BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                        else if (e.CommandName.Equals("TaskReminder")) //Send Reminder or Re-generate URL
                        {
                            if (taskID != 0)
                            {
                                string accessURL = string.Empty;
                                bool sendSuccess = false;

                                //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                                accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                    CryptographyManagement.Encrypt(Convert.ToString(taskID)) +
                                    "&NID=" + CryptographyManagement.Encrypt(Convert.ToString(noticeInstanceID));

                                //Get Task Record
                                var taskRecord = LitigationTaskManagement.GetTaskDetailByTaskID(noticeInstanceID, taskID, "N", AuthenticationHelper.CustomerID);

                                if (taskRecord != null)
                                {
                                    sendSuccess = SendTaskAssignmentMail(taskRecord, accessURL, AuthenticationHelper.User);

                                    if (sendSuccess)
                                    {
                                        cvNoticePopUpTask.ErrorMessage = "An Email containing task detail and access URL to provide response sent to assignee.";
                                        ValidationSummary5.CssClass = "alert alert-danger";
                                        taskRecord.AccessURL = accessURL;
                                        taskRecord.UpdatedBy = AuthenticationHelper.UserID;
                                        sendSuccess = LitigationTaskManagement.UpdateTaskAccessURL(taskRecord.ID, taskRecord);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        protected void grdResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdResponseLog.PageIndex = e.NewPageIndex;

                    BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("EditResponseDoc"))
                    {
                        string[] commandArgs = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);
                        int NoticeInstanceID = Convert.ToInt32(commandArgs[1]);
                        ViewState["responseID"] = responseID;
                        ViewState["NoticeInstanceID"] = NoticeInstanceID;
                        if (responseID != 0 && NoticeInstanceID != 0)
                        {
                            var ObjResponse = NoticeManagement.GetNoticeResponseDetailsByID(responseID, NoticeInstanceID);

                            if (ObjResponse != null)
                            {
                                if (ObjResponse.ResponseDate != null)
                                    tbxResponseDate.Text = Convert.ToDateTime(ObjResponse.ResponseDate).ToString("dd-MM-yyyy");
                                //tbxResponseDate.Text = Convert.ToString(ObjResponse.ResponseDate);
                                ddlRespBy.SelectedValue = Convert.ToString(ObjResponse.RespondedBy);
                                tbxRespThrough.Text = ObjResponse.ResponseThrough;
                                tbxRespRefNo.Text = ObjResponse.ResponseRefNo;
                                tbxResponseDesc.Text = ObjResponse.Description;
                                ddlNoticeResponseDate.SelectedValue = Convert.ToString(ObjResponse.ResponseType);
                                if (ObjResponse.NoticeDueDate != null)
                                    tbxNoticeDueDate.Text = Convert.ToDateTime(ObjResponse.NoticeDueDate).ToString("dd-MM-yyyy");

                                //tbxNoticeDueDate.Text = Convert.ToString(ObjResponse.NoticeDueDate);
                                tbxResponseID.Text = Convert.ToString(ObjResponse.ID);
                                tbxResponseRemark.Text = ObjResponse.Remark;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowResponseDivForEdit();", true);
                                var lstResponseDocument = NoticeManagement.GetNoticeResponseDocuments(NoticeInstanceID, responseID, "NR");
                                if (lstResponseDocument.Count > 0)
                                {
                                    GrdResponseEditDocument.DataSource = lstResponseDocument;
                                    GrdResponseEditDocument.DataBind();
                                    divResposeEditdoc.Visible = true;
                                    ViewState["ResponseMode"] = "Edit";

                                }
                            }
                            else
                            {
                                cvNoticePopUpResponse.IsValid = false;
                                cvNoticePopUpResponse.ErrorMessage = "No Document Available for Edit.";
                                ValidationSummary1.CssClass = "alert alert-danger";
                                return;
                            }

                            //DownloadNoticeDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                        }
                    }
                    else if (e.CommandName.Equals("DownloadResponseDoc"))
                    {
                        string[] commandArgs = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);
                        int NoticeInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && NoticeInstanceID != 0)
                        {
                            var lstResponseDocument = NoticeManagement.GetNoticeResponseDocuments(NoticeInstanceID, responseID, "NR");

                            if (lstResponseDocument.Count > 0)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS

                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }
                                        
                                        foreach (var file in lstResponseDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                        }
                                        int i = 0;
                                        foreach (var file in lstResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                            
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;

                                                responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                                
                                                i++;
                                            }
                                        }
                                        
                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=NoticeResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                        LitigationManagement.CreateAuditLog("N", NoticeInstanceID, "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Response Document(s) Downloaded", false);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal

                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        int i = 0;
                                        foreach (var file in lstResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=NoticeResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                        LitigationManagement.CreateAuditLog("N", NoticeInstanceID, "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Response Document(s) Downloaded", false);
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                cvNoticePopUpResponse.IsValid = false;
                                cvNoticePopUpResponse.ErrorMessage = "No Document Available for Download.";
                                ValidationSummary1.CssClass = "alert alert-danger";
                                return;
                            }

                            //DownloadNoticeDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                        }
                    }
                    else if (e.CommandName.Equals("DeleteResponse"))
                    {
                        DeleteNoticeResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID 

                        //Bind Notice Responses
                        if (ViewState["noticeInstanceID"] != null)
                        {
                            BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                    }
                    else if (e.CommandName.Equals("ViewNoticeResposeDocView"))
                    {
                        string[] commandArgs = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);//HearingID
                        int CaseInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && CaseInstanceID != 0)
                        {
                            var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, responseID, "NR");

                            if (lstResponseDocument != null)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS

                                    List<tbl_LitigationFileData> entitiesData = lstResponseDocument.Where(entry => entry.Version != null).ToList();
                                    if (lstResponseDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                        entityData.Version = "1.0";
                                        entityData.NoticeCaseInstanceID = CaseInstanceID;
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptDocmentVersionView.DataBind();

                                        foreach (var file in lstResponseDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string extension = System.IO.Path.GetExtension(file.FileName);
                                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                                {
                                                    lblMessage.Text = "";
                                                    lblMessage.Text = "Zip file can't view please download it";
                                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                                }
                                                else
                                                {
                                                    string filePath1 = directoryPath + "/" + file.FileName;
                                                    DocumentPath = filePath1;
                                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                                    lblMessage.Text = "";

                                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                                }
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                            }
                                            break;
                                        }
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal

                                    List<tbl_LitigationFileData> entitiesData = lstResponseDocument.Where(entry => entry.Version != null).ToList();
                                    if (lstResponseDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                        entityData.Version = "1.0";
                                        entityData.NoticeCaseInstanceID = CaseInstanceID;
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        foreach (var file in lstResponseDocument)
                                        {
                                            rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                            rptDocmentVersionView.DataBind();
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string Folder = "~/TempFiles";
                                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                                string DateFolder = Folder + "/" + File;

                                                string extension = System.IO.Path.GetExtension(filePath);

                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                DocumentPath = FileName;

                                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                                lblMessage.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                            }
                                            break;
                                        }
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        protected void grdResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadResponseDoc = (LinkButton)e.Row.FindControl("lnkBtnDownLoadResponseDoc");

            if (lnkBtnDownLoadResponseDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadResponseDoc);
            }

            LinkButton lnkBtnDeleteResponse = (LinkButton)e.Row.FindControl("lnkBtnDeleteResponse");
            if (lnkBtnDeleteResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteResponse);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteResponse.Visible = false;
                    else
                        lnkBtnDeleteResponse.Visible = true;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                {
                    lnkBtnDeleteResponse.Visible = false;
                }
            }
        }

        protected void grdNoticePayment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnDeletePayment = (LinkButton)e.Row.FindControl("lnkBtnDeletePayment");
                if (lnkBtnDeletePayment != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeletePayment);
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlPaymentType = (DropDownList)e.Row.FindControl("ddlPaymentType");

                if (ddlPaymentType != null)
                    BindPaymentType(ddlPaymentType);
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlLawyer = (DropDownList)e.Row.FindControl("ddlLawyer");

                if (ddlLawyer != null)
                    BindLawyers(ddlLawyer);
            }
        }

        private void BindLawyers(DropDownList ddlLawyer)
        {
            var LayerList = LitigationPaymentType.getLawyerList(Convert.ToInt32(AuthenticationHelper.CustomerID));
            ddlLawyer.DataValueField = "ID";
            ddlLawyer.DataTextField = "Name";

            ddlLawyer.DataSource = LayerList;
            ddlLawyer.DataBind();

            ddlLawyer.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

        }

        protected void grdNoticePayment_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdNoticePayment.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Payments
                    BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdNoticePayment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                TextBox tbxPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentDate");
                DropDownList ddlPaymentType = (DropDownList)grdNoticePayment.FooterRow.FindControl("ddlPaymentType");
                TextBox tbxAmount = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxAmount");
                TextBox tbxPaymentRemark = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentRemark");
                TextBox tbxAmountPaid = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxAmountPaid");
                TextBox tbxAmountTaxPaid = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxAmountTaxPaid");
                TextBox tbxInvoiceNo = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxInvoiceNo");
                DropDownList ddlLawyer = (DropDownList)grdNoticePayment.FooterRow.FindControl("ddlLawyer");
                TextBox tbxPaymentID = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentID");
              
                int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                int payId = Convert.ToInt32(e.CommandArgument);
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeletePayment"))
                    {
                        DeletePaymentLog(Convert.ToInt32(e.CommandArgument));

                        //Re-Bind Notice Payments
                        if (ViewState["noticeInstanceID"] != null)
                        {
                            BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                    }
                    #region viewdocument
                    else if (e.CommandName.Equals("ViewPayment"))
                    {

                        ViewState["PaymentMode"] = "Add";

                        var AllinOneDocumentList = CaseManagement.GetCasePaymentDocumentByID(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS                                
                                if (AllinOneDocumentList.FilePath != null)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                        request.Key = AllinOneDocumentList.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                    }
                                    lblMessage.Text = "";
                                    string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                    DocumentPath = filePath1;
                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);                                    
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal
                                string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                                if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }
                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (AllinOneDocumentList.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    DocumentPath = FileName;
                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewNew('" + DocumentPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewNew();", true);
                        }
                        #endregion

                    }
                    #region edit Payment
                    else if (e.CommandName.Equals("EditPayment"))
                    {
                        if (noticeInstanceID != 0 && payId != 0)
                        {
                            var PaymentData = NoticeManagement.GetPaymentDetailsByID(noticeInstanceID, payId);
                            tbxInvoiceNo.Text = Convert.ToString(PaymentData.InvoiceNo);
                            tbxPaymentID.Text = Convert.ToString(payId);
                            tbxPaymentDate.Text = Convert.ToDateTime(PaymentData.PaymentDate).ToString("dd-MM-yyyy");
                           // ddlPaymentType.SelectedValue = PaymentData.PaymentID.ToString();
                            tbxAmount.Text = Convert.ToString(PaymentData.Amount);
                            // tbxAmountPaid.Text = Convert.ToString(PaymentData.AmountPaid);

                            if (PaymentData.PaymentID != null)
                                ddlPaymentType.SelectedValue = PaymentData.PaymentID.ToString();

                            if (PaymentData.AmountPaid != null)
                                tbxAmountPaid.Text = Convert.ToString(PaymentData.AmountPaid);

                            if (PaymentData.AmountTax != null)
                                tbxAmountTaxPaid.Text = Convert.ToString(PaymentData.AmountTax);

                            if (PaymentData.Lawyer != null)
                                ddlLawyer.SelectedItem.Text = PaymentData.Lawyer.ToString();

                           // ddlLawyer.SelectedItem.Text = PaymentData.Lawyer.ToString();
                            tbxPaymentRemark.Text = PaymentData.Remark;
                       
                            ViewState["PaymentMode"] = "Edit";
                           
                      
                        }
                    }
                    else  if (e.CommandName.Equals("DownloadNoticePaymentDoc"))
                    {
                        DownloadNoticePaymentDocument(Convert.ToInt32(e.CommandArgument));
                    }


                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }
        public void DownloadNoticePaymentDocument(int caseFileID)
        {
            try
            {
                var file = CaseManagement.GetCasePaymentDocumentByID(caseFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }
                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                            }
                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            
                            Response.BinaryWrite(DocumentManagement.ReadDocFiles(filePath)); // create the file
                            
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document Downloaded", false);
                            applyCSStoFileTag_ListItems();
                            
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (filePath != null && File.Exists(filePath))
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document Downloaded", false);
                                applyCSStoFileTag_ListItems();
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvCasePopUp.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                //VSCasePopup.CssClass = "alert alert-danger";
            }
        }
        public static string GetOldFilename(long paymentid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_LitigationPaymentFileData
                            where row.NoticeCasePaymentId == paymentid
                            select row.FileName).SingleOrDefault();
                return data;
            }
        }
        public string ShowNoticeResponseDocCount(long noticeInstanceID, long noticeResponseID)
        {
            try
            {
                var docCount = NoticeManagement.GetNoticeResponseDocuments(noticeInstanceID, noticeResponseID, "NR").Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        // New code
        protected void lnkBtnDept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lnkBtnParty_Click(object sender, EventArgs e)
        {
            BindParty();
        }

        protected void lnkBtnAct_Click(object sender, EventArgs e)
        {
            BindAct();
        }

        protected void lnkBtnCategory_Click(object sender, EventArgs e)
        {
            BindNoticeCategoryType();
        }

        protected void lnkAddNewUser_Click(object sender, EventArgs e)
        {
            BindUsers();
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = Convert.ToString(e.CommandArgument).Split(',');
                    var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(commandArgs[2]));

                    if (AllinOneDocumentList != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                    request.Key = AllinOneDocumentList.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                }
                                string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                DocumentPath = filePath1;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";                                
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        }                       
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void btnSaveLawRating_Click(object sender, EventArgs e)
        {
            int LawyerID = -1;
            long NewNoticeID = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
            {
                List<tbl_LawyerListRating> objMainList = new List<tbl_LawyerListRating>();
                for (int i = 0; i < grdLawyerRating.Rows.Count; i++)
                {
                    // Label lblID = (Label) grdLawyerRating.Rows[i].Cells[3].FindControl("lblLawyerID");
                    // Label lblType = (Label) grdLawyerRating.Rows[i].Cells[3].FindControl("lblType");
                    // Label lblNoticeInstanceID = (Label) grdLawyerRating.Rows[i].Cells[3].FindControl("lblNoticeInstanceID");
                    //TextBox tbxRating = (TextBox) grdLawyerRating.Rows[i].Cells[2].FindControl("tbxLawyerRating");
                    Rating LawyerRating = (Rating)grdLawyerRating.Rows[i].Cells[2].FindControl("LawyerRating");
                    Label lblCriteriaID = (Label)grdLawyerRating.Rows[i].Cells[3].FindControl("lblCriteriaID");
                    if ((!string.IsNullOrEmpty(ddlLayerType.SelectedValue)))
                    {
                        LawyerID = Convert.ToInt32(ddlLayerType.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(LawyerRating.CurrentRating)))
                    {
                        if (Convert.ToDecimal(LawyerRating.CurrentRating) <= 5)
                        {
                            if (Convert.ToDecimal(LawyerRating.CurrentRating) != 0)
                            {
                                NewNoticeID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                                tbl_LawyerListRating objLawRating = new tbl_LawyerListRating()
                                {
                                    LawyerID = LawyerID,
                                    Rating = Convert.ToDecimal(LawyerRating.CurrentRating),
                                    Type = 2,
                                    CaseNoticeID = NewNoticeID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UserID = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    IsActive = true,
                                    CriteriaRatingID = Convert.ToInt32(lblCriteriaID.Text)
                                };
                                objMainList.Add(objLawRating);
                            }
                        }
                        else
                        {
                            CvValidLaywRating.IsValid = false;
                            CvValidLaywRating.ErrorMessage = "Rating value should be between 0 to 5(i.e. 0.5 or 4)";
                            ValidationSummary6.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                }

                bool check = CaseManagement.SaveLaywerRatingListData(objMainList);
                if (check)
                {
                    LitigationManagement.CreateAuditLog("N", NewNoticeID, "tbl_LawyerListRating", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Rating Assigned to Lawyer(s)", true);
                }
                objMainList.ForEach(eachRatingRecord =>
                        {
                            decimal LawyerRating = CaseManagement.GetLawyerRating(Convert.ToInt32(eachRatingRecord.LawyerID));
                            if (!string.IsNullOrEmpty(Convert.ToString(LawyerRating)))
                            {
                                tbl_LawyerFinalRating objNewRate = new tbl_LawyerFinalRating()
                                {
                                    LawyerID = Convert.ToInt32(eachRatingRecord.LawyerID),
                                    Rating = LawyerRating,
                                    IsActive = true
                                };

                                if (CaseManagement.CheckIsExistLawyerRating(objNewRate))
                                {
                                    objNewRate.CreatedBy = AuthenticationHelper.UserID;
                                    objNewRate.CreatedOn = DateTime.Now;
                                    CaseManagement.CreateLawyerFinalRating(objNewRate);
                                    CvValidLaywRating.IsValid = false;
                                    CvValidLaywRating.ErrorMessage = "Rating Saved Successfully.";
                                    ValidationSummary6.CssClass = "alert alert-success";
                                }
                                else
                                {
                                    objNewRate.UpdatedBy = AuthenticationHelper.UserID;
                                    objNewRate.UpdatedOn = DateTime.Now;
                                    CaseManagement.UpdateLawyerFinalRating(objNewRate);
                                    CvValidLaywRating.IsValid = false;
                                    CvValidLaywRating.ErrorMessage = "Rating Saved Successfully.";
                                    ValidationSummary6.CssClass = "alert alert-success";
                                }
                            }
                        });
            }
        }

        protected void grdLawyerRating_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdLawyerRating_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
                {
                    if (!string.IsNullOrEmpty(ddlLayerType.SelectedValue))
                    {
                        int NoticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                        grdLawyerRating.PageIndex = e.NewPageIndex;
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var documentData = (from row in entities.sp_LiDisplayCriteriaRatingNotice(NoticeInstanceID, Convert.ToInt32(ddlLayerType.SelectedValue), Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                select row).ToList();

                            if (documentData != null)
                            {
                                grdLawyerRating.DataSource = documentData;
                                grdLawyerRating.DataBind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdShowDocumentList_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void grdShowDocumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdShowDocumentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdShowDocumentList.PageIndex = e.NewPageIndex;

                    //Re-Bind Case Payments
                    BindMailDocumentList(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void chkAllDocument_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)grdShowDocumentList.HeaderRow.FindControl("chkAllDocument");

            foreach (GridViewRow row in grdShowDocumentList.Rows)
            {
                CheckBox checkDoc = (CheckBox)row.FindControl("chkDocument");

                if (ChkBoxHeader.Checked)
                    checkDoc.Checked = true;
                else
                    checkDoc.Checked = false;
            }

            ShowSelectedRecords(sender, e);
        }

        protected void chkDocument_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)grdShowDocumentList.HeaderRow.FindControl("chkAllDocument");
            int Count = 0;
            foreach (GridViewRow row in grdShowDocumentList.Rows)
            {
                CheckBox checkDoc = (CheckBox)row.FindControl("chkDocument");

                if (checkDoc.Checked)
                    Count++;
            }

            if (Count == grdShowDocumentList.Rows.Count)
                ChkBoxHeader.Checked = true;
            else
                ChkBoxHeader.Checked = false;

            ShowSelectedRecords(sender, e);
        }

        protected void btnSendDocumentMail_Click(object sender, EventArgs e)
        {
            string messageAll = string.Empty;

            int noticeInstanceID = 0;

            if (ViewState["noticeInstanceID"] != null)
            {
                noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                var noticeDetailinfo = NoticeManagement.GetNoticeByID(noticeInstanceID);

                messageAll = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_DocumentWithSummary_Notice
                                       .Replace("@message", tbxMailMsg.Text)
                                       .Replace("@CaseNo", Convert.ToString(noticeDetailinfo.RefNo))
                                       .Replace("@CaseTitle", noticeDetailinfo.NoticeTitle)
                                       .Replace("@CaseDescription", noticeDetailinfo.NoticeDetailDesc)
                                       .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                                       .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
            }
            try
            {
                if (tbxMailTo.Text != "")
                {
                    bool sendMailSuccess = false;
                    List<string> lstTO = new List<string>();
                    List<string> lstCc = new List<string>();
                    List<string> lstBcc = new List<string>();

                    string strReceiver = string.Empty;

                    strReceiver = tbxMailTo.Text;
                    string[] Multiple = strReceiver.Split(',');

                    foreach (string multiple_email in Multiple)
                    {
                        if (multiple_email != "")
                            lstTO.Add(multiple_email.Trim());
                    }

                    List<Tuple<string, string>> attachmentwithPath = new List<Tuple<string, string>>();

                    string folderPath = string.Empty;
                    if (ViewState["docsToAttach"] != null)
                    {
                        attachmentwithPath = (List<Tuple<string, string>>)ViewState["docsToAttach"];

                        try
                        {
                            SendGridEmailManager.SendGridNewsLetterMail1(ConfigurationManager.AppSettings["SenderEmailAddress"], Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]), lstTO, lstCc, lstBcc, "Litigation Case Summary with Documents", messageAll, attachmentwithPath);
                            sendMailSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            sendMailSuccess = false;
                            CvDocListWithMail.IsValid = false;
                            CvDocListWithMail.ErrorMessage = "Something went wrong. Please try again.";
                            ValidationSummary7.CssClass = "alert alert-danger";
                        }

                        if (sendMailSuccess)
                        {
                            CvDocListWithMail.IsValid = false;
                            CvDocListWithMail.ErrorMessage = "E-Mail Sent Successfully.";
                            ValidationSummary7.CssClass = "alert alert-success";
                            lblTotalSelected.Text = "";
                            grdShowDocumentList.DataSource = null;
                            grdShowDocumentList.DataBind();
                            tbxMailMsg.Text = "";
                            tbxMailTo.Text = "";
                            btnSendDocumentMail.Visible = false;
                            LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_LegalCaseInstance", "Email", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Summary with Document(s) Sent as Email", true);
                        }
                    }
                }
                else
                {
                    CvDocListWithMail.IsValid = false;
                    CvDocListWithMail.ErrorMessage = "Required Subject Line and From Name.";
                    ValidationSummary7.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvDocListWithMail.IsValid = false;
                CvDocListWithMail.ErrorMessage = "Something went wrong. Please try again.";
                ValidationSummary7.CssClass = "alert alert-danger";
            }
        }

        private void GetCheckBoxValue()
        {

            try
            {
                ArrayList DocumentList = new ArrayList();
                ArrayList DocumentFileNameList = new ArrayList();

                List<Tuple<string, string>> lstDocsToAttach = new List<Tuple<string, string>>();

                foreach (GridViewRow GridDoc in grdShowDocumentList.Rows)
                {
                    Label lblID = (Label)GridDoc.FindControl("lblID");
                    Label lblFileName = (Label)GridDoc.FindControl("lblFileName");
                    Label lblFilePath = (Label)GridDoc.FindControl("lblFilePath");

                    bool result = ((CheckBox)GridDoc.FindControl("chkDocument")).Checked;

                    if (result && !string.IsNullOrEmpty(lblFilePath.Text) && !string.IsNullOrEmpty(lblFileName.Text))
                    {


                        var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(lblID.Text));
                        if (AllinOneDocumentList != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string folderPath = "~/TempFiles/" + DateTime.Now.ToString("ddMMyyyy");
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (!Directory.Exists(folderPath))
                                    Directory.CreateDirectory(Server.MapPath(folderPath));

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AllinOneDocumentList.FileName + "" + FileDate;
                                string FileName = folderPath + "/" + User + "" + extension;
                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                DocumentList.Add(lblID.Text);
                                DocumentFileNameList.Add(lblFilePath.Text);

                                lstDocsToAttach.Add(new Tuple<string, string>(DocumentPath, FileName));
                                ViewState["ListofFile"] = DocumentList;
                                ViewState["docsToAttach"] = lstDocsToAttach;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ShowCheckBoxCheckedValue()
        {
            try
            {
                ArrayList DocumentList = (ArrayList)ViewState["ListofFile"];

                if (DocumentList != null && DocumentList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdShowDocumentList.Rows)
                    {
                        Label lblID = (Label)gvrow.FindControl("lblID");
                        Label lblFileName = (Label)gvrow.FindControl("lblFileName");

                        if (DocumentList.Contains(lblID.Text))
                        {
                            CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkDocument");
                            myCheckBox.Checked = true;
                        }
                    }
                    if (DocumentList.Count > 0)
                    {
                        lblTotalSelected.Text = DocumentList.Count + " Selected";
                        btnSendDocumentMail.Visible = true;
                    }
                    else if (DocumentList.Count == 0)
                    {
                        lblTotalSelected.Text = "";
                        btnSendDocumentMail.Visible = false;
                    }
                }
                else
                {
                    lblTotalSelected.Text = "";
                    btnSendDocumentMail.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            lblTotalSelected.Text = "";

            GetCheckBoxValue();

            ShowCheckBoxCheckedValue();
        }

        private void BindMailDocumentList(int NoticeInstanceID)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(NoticeInstanceID)))
            {
                //var DocList = CaseManagement.GettDocumentList(NoticeInstanceID, "N");
                var DocList = NoticeManagement.GetNoticeDocumentMapping(NoticeInstanceID, "", AuthenticationHelper.CustomerID);
                if (DocList.Count > 0)
                {
                    DocList = (from g in DocList
                               group g by new
                               {
                                   g.ID,
                                   g.DocType,
                                   g.FileName,
                                   g.Version,
                                   g.CreatedByText,
                                   g.CreatedOn,
                                   g.FilePath
                               } into GCS
                               select new Sp_Litigation_CaseDocument_Result()
                               {
                                   ID = GCS.Key.ID,
                                   DocType = GCS.Key.DocType,
                                   FileName = GCS.Key.FileName,
                                   Version = GCS.Key.Version,
                                   CreatedByText = GCS.Key.CreatedByText,
                                   CreatedOn = GCS.Key.CreatedOn,
                                   FilePath = GCS.Key.FilePath
                               }).ToList();
                }
                grdShowDocumentList.DataSource = DocList;
                grdShowDocumentList.DataBind();
            }
        }

        protected void ddlLawFirm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLawFirm.SelectedValue))
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();

                var lawyerUsers = LitigationUserManagement.GetRequiredUsersByLawFirm(lstAllUsers, 2, Convert.ToInt32(ddlLawFirm.SelectedValue));

                lstBoxLawyerUser.DataTextField = "Name";
                lstBoxLawyerUser.DataValueField = "ID";
                lstBoxLawyerUser.DataSource = lawyerUsers;
                lstBoxLawyerUser.DataBind();
                lstBoxLawyerUser.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScript", "rebindLawyerUser();", true);
                UpdatePanel6.Update();
                if (ddlLawFirm.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "ShowAddButton", "ShowLawFirmAddbutton();", true);
                }
            }
            else
            {
                //  bindAllLawfirmUser();
            }
        }

        protected void gvCaseAuditLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    gvCaseAuditLog.PageIndex = e.NewPageIndex;

                    BindNoticeAuditLogs();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlTaskUserInternal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlTaskUserInternal.SelectedValue)
                && ddlTaskUserInternal.SelectedValue != "0" && ddlTaskUserInternal.SelectedValue != "-1")
            {
                ddlTaskUserLawyerAndExternal.ClearSelection();
            }
        }

        protected void ddlTaskUserLawyerAndExternal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlTaskUserLawyerAndExternal.SelectedValue)
                && ddlTaskUserLawyerAndExternal.SelectedValue != "0" && ddlTaskUserLawyerAndExternal.SelectedValue != "-1")
            {
                ddlTaskUserInternal.ClearSelection();
            }
        }


        protected void GrdResponseEditDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteResposeEditDocument"))
            {
                long NoticeInstanceID = 0;
                long responseID = 0;
                string commandArgs = Convert.ToString(e.CommandArgument);

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    bool NoticeRespose = NoticeManagement.DeleteResponseEditDocument(Convert.ToInt32(commandArgs));
                    if (NoticeRespose)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["responseID"])))
                        {
                            responseID = Convert.ToInt32(ViewState["responseID"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["NoticeInstanceID"])))
                        {
                            NoticeInstanceID = Convert.ToInt32(ViewState["NoticeInstanceID"]);
                        }
                        var lstResponseDocument = NoticeManagement.GetNoticeResponseDocuments(NoticeInstanceID, responseID, "NR");
                        if (lstResponseDocument.Count > 0)
                        {
                            GrdResponseEditDocument.DataSource = lstResponseDocument;
                            GrdResponseEditDocument.DataBind();
                            divResposeEditdoc.Visible = true;
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowResponseDivForEdit();", true);
                    }
                }
            }
            else if (e.CommandName.Equals("DownloadResponseEditDocument"))
            {
                string commandArgs = Convert.ToString(e.CommandArgument);

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = NoticeManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }
                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                int idx = file.FileName.LastIndexOf('.');
                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                
                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowResponseDivForEdit();", true);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowResponseDivForEdit();", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "No Document Available for Download.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                        return;
                    }
                }
            }
            else if (e.CommandName.Equals("ViewResponseEditDocument"))
            {
                string commandArgs = Convert.ToString(e.CommandArgument);

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = NoticeManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;
                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }
                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                            }
                            string filePath1 = directoryPath + "/" + file.FileName;
                            DocumentPath = filePath1;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            lblMessage.Text = "";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowResponseDivForEdit();", true);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;

                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                lblMessage.Text = "";

                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowResponseDivForEdit();", true);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                    }
                }
            }
        }

        protected void grdTaskEditDoc_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteTaskEditDocument"))
            {
                long NoticeInstanceID = 0;
                long taskID = 0;
                string commandArgs = Convert.ToString(e.CommandArgument);

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    bool NoticeRespose = NoticeManagement.DeleteResponseEditDocument(Convert.ToInt32(commandArgs));
                    if (NoticeRespose)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["taskID"])))
                        {
                            taskID = Convert.ToInt32(ViewState["taskID"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
                        {
                            NoticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                        }
                        var lstResponseDocument = NoticeManagement.GetNoticeResponseDocuments(NoticeInstanceID, taskID, "NT");
                        if (lstResponseDocument.Count > 0)
                        {
                            grdTaskEditDoc.DataSource = lstResponseDocument;
                            grdTaskEditDoc.DataBind();
                            divResposeEditdoc.Visible = true;
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                    }
                }
            }
            else if (e.CommandName.Equals("DownloadTaskEditDocument"))
            {
                string commandArgs = Convert.ToString(e.CommandArgument);

                if (!string.IsNullOrEmpty(commandArgs))
                {

                    var file = NoticeManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS 
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (file != null)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    int i = 0;

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;

                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));

                                        i++;
                                    }
                                    
                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal 
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "No Document Available for Download.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                        return;
                    }
                }
            }
            else if (e.CommandName.Equals("ViewTaskEditDocument"))
            {
                string commandArgs = Convert.ToString(e.CommandArgument);

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = NoticeManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }
                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                            }
                            string filePath1 = directoryPath + "/" + file.FileName;
                            DocumentPath = filePath1;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            lblMessage.Text = "";

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            #endregion
                        }                        
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;

                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                lblMessage.Text = "";

                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                    }
                }
            }
        }

        protected void btnUploadNoticeDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    bool saveSuccess = false;
                    long newNoticeID = Convert.ToInt64(ViewState["noticeInstanceID"]);
                    int DocTypeID = -1;
                    if (newNoticeID > 0)
                    {
                        #region Upload Document

                        if (NoticeFileUpload.HasFiles)
                        {
                            tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = Convert.ToInt32(newNoticeID),
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,
                                DocTypeID = DocTypeID,
                                DocType = "N"
                            };

                            HttpFileCollection fileCollection = Request.Files;

                            if (fileCollection.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                string directoryPath = "";
                                String fileName = "";

                                if (newNoticeID > 0)
                                {
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection[i];

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            string[] keys1 = fileCollection.Keys[i].Split('$');

                                            if (keys1[keys1.Count() - 1].Equals("NoticeFileUpload"))
                                            {
                                                fileName = uploadedFile.FileName;
                                            }

                                            objNoticeDoc.FileName = fileName;

                                            //Get Document Version
                                            var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeDoc);

                                            caseDocVersion++;
                                            objNoticeDoc.Version = caseDocVersion + ".0";

                                            directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(newNoticeID) + "/NoticeDocument/" + objNoticeDoc.Version);

                                            if (!Directory.Exists(directoryPath))
                                                Directory.CreateDirectory(directoryPath);

                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                            Stream fs = uploadedFile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                            objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objNoticeDoc.FileKey = Convert.ToString(fileKey1);
                                            objNoticeDoc.VersionDate = DateTime.Now;
                                            objNoticeDoc.CreatedOn = DateTime.Now;
                                            objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                            DocumentManagement.Litigation_SaveDocFiles(fileList);
                                            saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);

                                            fileList.Clear();
                                        }
                                    }//End For Each   
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("N", newNoticeID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Document(s) Uploaded", true);
                                        BindNoticeRelatedDocuments_All(Convert.ToInt32(newNoticeID));
                                    }
                                }

                                if (saveSuccess)
                                {
                                    cvNoticeDocument.IsValid = false;
                                    cvNoticeDocument.ErrorMessage = "Document(s) Uploaded Successfully.";
                                    vsNoticeDocument.CssClass = "alert alert-success";
                                }
                                else
                                {
                                    cvNoticeDocument.IsValid = false;
                                    cvNoticeDocument.ErrorMessage = "Something went wrong, during document upload, Please try again";
                                    vsNoticeDocument.CssClass = "alert alert-danger";
                                }
                            }
                            else
                            {
                                cvNoticeDocument.IsValid = false;
                                cvNoticeDocument.ErrorMessage = "No document selected to upload";
                                vsNoticeDocument.CssClass = "alert alert-danger";
                            }
                        }
                        else
                        {
                            cvNoticeDocument.IsValid = false;
                            cvNoticeDocument.ErrorMessage = "No document selected to upload";
                            vsNoticeDocument.CssClass = "alert alert-danger";
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ShowNoticeDocType(string docType)
        {
            try
            {
                if (!string.IsNullOrEmpty(docType))
                {
                    if (docType.Trim() == "N")
                        return "Notice";
                    else if (docType.Trim() == "NR")
                        return "Response";
                    else if (docType.Trim() == "NT")
                        return "Task";
                    else
                        return "";
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowFinancialYear(string FYear)
        {
            try
            {
                if (!string.IsNullOrEmpty(FYear))
                {
                    return FYear.TrimEnd(',');
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }


        protected void grdUserAssignment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                bool deleteSuccess = false;

                if (e.CommandArgument != null && ViewState["noticeInstanceID"] != null)
                {
                    long noticeInstance = Convert.ToInt64(ViewState["noticeInstanceID"]);
                    if (e.CommandName.Equals("Delete_UserAssignment"))
                    {
                        string[] commandArgs = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                        int assignRecordID = Convert.ToInt32(commandArgs[0]);
                        long noticeInstanceID = Convert.ToInt64(commandArgs[1]);

                        if (assignRecordID != 0 && noticeInstanceID != 0)
                        {
                            deleteSuccess = NoticeManagement.DeleteNoticeUserAssignment(assignRecordID, noticeInstanceID);
                        }
                    }

                    if (deleteSuccess)
                    {
                        cvNoticeUserAssignmemt.IsValid = false;
                        cvNoticeUserAssignmemt.ErrorMessage = "User Assignment Detail Deleted";
                        vsNoticeUserAssign.CssClass = "alert alert-success";

                        BindNoticeUserAssignments(noticeInstance);
                    }
                    else
                    {
                        cvNoticeUserAssignmemt.IsValid = false;
                        cvNoticeUserAssignmemt.ErrorMessage = "Something went wrong there, Please try again";
                        vsNoticeUserAssign.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdUserAssignment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                LinkButton lnkDeleteUserAssignment = (LinkButton)e.Row.FindControl("lnkDeleteUserAssignment");
                if (lnkDeleteUserAssignment != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkDeleteUserAssignment);

                    if (ViewState["caseStatus"] != null)
                    {
                        if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                            lnkDeleteUserAssignment.Visible = false;
                        else
                            lnkDeleteUserAssignment.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                    {
                        lnkDeleteUserAssignment.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindNoticeUserAssignments(long noticeInstanceID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                List<SP_Litigation_NoticeCaseUserAssigned_Result> lstNoticeAssignments = new List<SP_Litigation_NoticeCaseUserAssigned_Result>();

                lstNoticeAssignments = NoticeManagement.GetNoticeUserAssignments(customerID, noticeInstanceID);

                grdUserAssignment.DataSource = lstNoticeAssignments;
                grdUserAssignment.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
        }


        #region Custom Field

        private void intializeDataTableCustomField(GridView gridViewCustomField, GridView gridViewCustomField_History)
        {
            try
            {
                DataTable dtCustomField = new DataTable();

                DataRow drowCustomField = null;

                dtCustomField.Columns.Add(new DataColumn("LableID", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Label", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("labelValue", typeof(string)));

                drowCustomField = dtCustomField.NewRow();

                drowCustomField["LableID"] = string.Empty;
                drowCustomField["Label"] = string.Empty;
                drowCustomField["labelValue"] = string.Empty;

                dtCustomField.Rows.Add(drowCustomField);

                ViewState["dataTableCustomFields"] = dtCustomField;

                gridViewCustomField.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                gridViewCustomField.DataBind();

                //To Hide row
                gridViewCustomField.Rows[0].Visible = false;
                gridViewCustomField.Rows[0].Controls.Clear();

                //lblAddNewGround.Visible = true;

                gridViewCustomField.Visible = true;
                gridViewCustomField_History.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void intializeDataTableCustomField_TaxLitigation(GridView gridViewCustomField, GridView gridViewCustomField_History)
        {
            try
            {
                DataTable dtCustomField = new DataTable();

                DataRow drowCustomField = null;

                dtCustomField.Columns.Add(new DataColumn("LableID", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Label", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("labelValue", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Interest", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Penalty", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Total", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("SettlementValue", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("ProvisionInBook", typeof(string)));

                drowCustomField = dtCustomField.NewRow();

                drowCustomField["LableID"] = string.Empty;
                drowCustomField["Label"] = string.Empty;
                drowCustomField["labelValue"] = string.Empty;
                drowCustomField["Interest"] = string.Empty;
                drowCustomField["Penalty"] = string.Empty;
                drowCustomField["Total"] = string.Empty;
                drowCustomField["SettlementValue"] = string.Empty;
                drowCustomField["ProvisionInBook"] = string.Empty;

                dtCustomField.Rows.Add(drowCustomField);

                ViewState["dataTableCustomFields"] = dtCustomField;

                gridViewCustomField.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                gridViewCustomField.DataBind();

                //To Hide row
                gridViewCustomField.Rows[0].Visible = false;
                gridViewCustomField.Rows[0].Controls.Clear();

                //lblAddNewGround.Visible = true;

                gridViewCustomField.Visible = true;
                gridViewCustomField_History.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        private void BindCustomFields(GridView gridViewCustomField, GridView gridViewCustomField_History)
        {
            try
            {
                int noticeInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
                {
                    noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                }
                if (ddlNoticeCategory.SelectedValue != null && ddlNoticeCategory.SelectedValue != "0")  //&& CaseInstanceID != 0
                {
                    List<SP_Litigation_GetCustomParameters_Result> lstCustomParameters = new List<SP_Litigation_GetCustomParameters_Result>();

                    if (noticeInstanceID != 0)
                    {
                        lstCustomParameters = CaseManagement.GetCustomsFields(Convert.ToInt32(AuthenticationHelper.CustomerID), noticeInstanceID, 2, Convert.ToInt32(ddlNoticeCategory.SelectedValue));

                        bool historyFlag = false;
                        if (ViewState["FlagHistory"] != null)
                        {
                            historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                        }

                        if (lstCustomParameters != null && lstCustomParameters.Count > 0)
                        {
                            ViewState["CustomefieldCount"] = lstCustomParameters.Count;

                            if (!historyFlag)
                            {
                                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                {
                                    lstCustomParameters.Add(LitigationManagement.GetColumnTotal(lstCustomParameters));
                                    divDeposits.Visible = true;
                                }
                                else
                                    divDeposits.Visible = false;

                                gridViewCustomField.DataSource = lstCustomParameters;
                                gridViewCustomField.DataBind();

                                //lblAddNewGround.Visible = true;

                                gridViewCustomField.Visible = true;
                                gridViewCustomField_History.Visible = false;
                            }
                            else if (historyFlag)
                            {
                                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                {
                                    lstCustomParameters.Add(LitigationManagement.GetColumnTotal(lstCustomParameters));
                                    divDeposits.Visible = true;
                                }
                                else
                                    divDeposits.Visible = false;

                                gridViewCustomField_History.DataSource = lstCustomParameters;
                                gridViewCustomField_History.DataBind();

                                //lblAddNewGround.Visible = false;

                                gridViewCustomField.Visible = false;
                                gridViewCustomField_History.Visible = true;
                            }
                        }
                        else
                        {
                            SP_Litigation_GetCustomParameters_Result obj = new SP_Litigation_GetCustomParameters_Result(); //initialize empty class that may contain properties
                            lstCustomParameters.Add(obj); //Add empty object to list

                            if (!historyFlag)
                            {
                                gridViewCustomField.DataSource = lstCustomParameters; /*Assign datasource to create one row with default values for the class you have*//*Assign datasource to create one row with default values for the class you have*/
                                gridViewCustomField.DataBind(); //Bind that empty source     

                                //To Hide row
                                gridViewCustomField.Rows[0].Visible = false;
                                gridViewCustomField.Rows[0].Controls.Clear();

                                //lblAddNewGround.Visible = true;

                                gridViewCustomField.Visible = true;
                                gridViewCustomField_History.Visible = false;
                            }
                            else if (historyFlag)
                            {
                                gridViewCustomField_History.DataSource = lstCustomParameters;
                                gridViewCustomField_History.DataBind();

                                //To Hide row
                                gridViewCustomField_History.Rows[0].Visible = false;
                                gridViewCustomField_History.Rows[0].Controls.Clear();

                                //lblAddNewGround.Visible = false;

                                gridViewCustomField.Visible = false;
                                gridViewCustomField_History.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                        {
                            //Replace with New
                            intializeDataTableCustomField_TaxLitigation(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                        }
                        else
                        {
                            intializeDataTableCustomField(grdCustomField, grdCustomField_History);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFieldDropDown(DropDownList ddlCustomField, GridView gridViewCustomField)
        {
            try
            {
                int noticeInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
                {
                    noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                }

                if (ddlNoticeCategory.SelectedValue != null && ddlNoticeCategory.SelectedValue != "" && ddlNoticeCategory.SelectedValue != "0") //&& CaseInstanceID != 0
                {
                    var customFields = CaseManagement.GetCustomsFieldsByCaseType(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(ddlNoticeCategory.SelectedValue));

                    if (customFields.Count > 0)
                    {
                        //lblAddNewGround.Visible = true;
                        gridViewCustomField.Visible = true;

                        if (ddlCustomField.Items.Count > 0)
                            ddlCustomField.Items.Clear();

                        ddlCustomField.DataTextField = "Label";
                        ddlCustomField.DataValueField = "ID";

                        ddlCustomField.DataSource = customFields;
                        ddlCustomField.DataBind();

                        ddlCustomField.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                        ViewState["ddlCustomFieldFilled"] = "1";
                    }
                    else
                    {
                        //lblAddNewGround.Visible = false;
                        gridViewCustomField.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlNoticeCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlNoticeCategory.SelectedValue != null && ddlNoticeCategory.SelectedValue != "0")
            {
                ViewState["ddlCustomFieldFilled"] = null;
                ViewState["dataTableCustomFields"] = null;

                ViewState["CustomefieldCount"] = null;

                ViewState["NoticeTypeUpdated"] = "false";

                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && CustomerID != 76)
                {
                    divDeposits.Visible = true;
                    BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                    emmamiusers.Visible = false;
                    grdCustomField.Visible = false;
                    grdCustomField_History.Visible = false;
                }
                else if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && CustomerID == 76)
                {
                    emmamiusers.Visible = true;
                    divDeposits.Visible = true;
                    grdCustomField.Visible = false;
                    grdCustomField_History.Visible = false;
                }
                else if (!(ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim())))
                {
                    divDeposits.Visible = false;
                    BindCustomFields(grdCustomField, grdCustomField_History);

                    grdCustomField_TaxLitigation.Visible = false;
                    grdCustomField_TaxLitigation_History.Visible = false;
                }
            }
            else
            {
                lnkAddNewNoticeCategoryModal.Visible = true;
            }
        }

        protected void grdCustomField_Common_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        TextBox tbxLabelValue = (TextBox)e.Row.FindControl("tbxLabelValue");

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            tbxLabelValue.Enabled = false;
                        }

                        //Hide Delete in Case of Total or Row with 0 ID
                        Label lblID = (Label)e.Row.FindControl("lblID");
                        LinkButton lnkBtnDeleteCustomField_TaxLitigation = (LinkButton)e.Row.FindControl("lnkBtnDeleteCustomField_TaxLitigation");

                        if (lblID != null && lnkBtnDeleteCustomField_TaxLitigation != null)
                        {
                            if (lblID.Text != "" && lblID.Text != "0")
                            {
                                e.Row.Enabled = true;
                                lnkBtnDeleteCustomField_TaxLitigation.Visible = true;
                            }
                            else
                            {
                                e.Row.Enabled = false;
                                lnkBtnDeleteCustomField_TaxLitigation.Visible = false;
                            }
                        }
                    }

                    if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        DropDownList ddlFieldName_Footer = (DropDownList)e.Row.FindControl("ddlFieldName_Footer");

                        if (ddlFieldName_Footer != null)
                        {
                            BindCustomFieldDropDown(ddlFieldName_Footer, gridView);

                            foreach (GridViewRow gvr in gridView.Rows)
                            {
                                Label lblID = (Label)gvr.FindControl("lblID");

                                if (lblID != null)
                                {
                                    if (lblID.Text != "")
                                    {
                                        if (ddlFieldName_Footer.Items.FindByValue(lblID.Text) != null)
                                            ddlFieldName_Footer.Items.Remove(ddlFieldName_Footer.Items.FindByValue(lblID.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    gridView.PageIndex = e.NewPageIndex;

                    if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                    {
                        BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                    }
                    else
                    {
                        BindCustomFields(grdCustomField, grdCustomField_History);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long caseInstanceID = 0;
                bool deleteSuccess = false;

                if (ViewState["noticeInstanceID"] != null && e.CommandName.Equals("DeleteCustomField") && e.CommandArgument != null && ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0 && ViewState["dataTableCustomFields"] != null)
                    {
                        GridViewRow gvRow = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                        if (gvRow != null)
                        {
                            //GridViewRow gvRow = (GridViewRow)(sender).Parent.Parent;
                            int index = gvRow.RowIndex;

                            DataTable dtCustomField = ViewState["dataTableCustomFields"] as DataTable;
                            dtCustomField.Rows[index].Delete();

                            ViewState["dataTableCustomFields"] = dtCustomField;
                            ViewState["CustomefieldCount"] = dtCustomField.Rows.Count;

                            GridView gridView = (GridView)sender;

                            if (gridView != null)
                            {
                                gridView.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                                gridView.DataBind();
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        int LableID = Convert.ToInt32(e.CommandArgument);

                        caseInstanceID = Convert.ToInt64(ViewState["noticeInstanceID"]);

                        if (LableID != 0 && caseInstanceID != 0)
                        {
                            deleteSuccess = CaseManagement.DeleteCustomsFieldByCaseID(1, caseInstanceID, LableID);
                            if (deleteSuccess)
                            {
                                LitigationManagement.CreateAuditLog("N", caseInstanceID, "tbl_NoticeCaseCustomParameter", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Deleted", true);

                                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                {
                                    BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                                }
                                else
                                {
                                    BindCustomFields(grdCustomField, grdCustomField_History);
                                }
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnAddCustomField_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    int lblID = 0;

                    DropDownList ddlFieldName_Footer = (DropDownList)grdCustomField.FooterRow.FindControl("ddlFieldName_Footer");
                    TextBox txtFieldValue_Footer = (TextBox)grdCustomField.FooterRow.FindControl("txtFieldValue_Footer");

                    if (ddlFieldName_Footer != null && txtFieldValue_Footer != null)
                    {
                        if ((int)ViewState["Mode"] == 0)
                        {
                            string lblName = ddlFieldName_Footer.SelectedItem.Text;

                            if (!string.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue))
                            {
                                lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);
                            }

                            if (ViewState["dataTableCustomFields"] != null && lblID != 0)
                            {
                                DataTable dtcurrentTableCumtomFields = (DataTable)ViewState["dataTableCustomFields"];

                                DataRow drNewRow = dtcurrentTableCumtomFields.NewRow();

                                drNewRow["LableID"] = lblID;
                                drNewRow["Label"] = lblName;
                                drNewRow["labelValue"] = txtFieldValue_Footer.Text;

                                //add new row to DataTable
                                dtcurrentTableCumtomFields.Rows.Add(drNewRow);

                                //Delete Rows with blank LblID (if Any)
                                dtcurrentTableCumtomFields = LitigationManagement.LoopAndDeleteBlankRows(dtcurrentTableCumtomFields);

                                ViewState["CustomefieldCount"] = dtcurrentTableCumtomFields.Rows.Count;

                                //Store the current data to ViewState
                                ViewState["CurrentTablePutCallDtls"] = dtcurrentTableCumtomFields;

                                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                {
                                    dtcurrentTableCumtomFields = LitigationManagement.GetColumnTotal(dtcurrentTableCumtomFields, true);

                                    //Rebind the Grid with the current data
                                    grdCustomField_TaxLitigation.DataSource = dtcurrentTableCumtomFields;
                                    grdCustomField_TaxLitigation.DataBind();
                                }
                                else
                                {
                                    //Rebind the Grid with the current data
                                    grdCustomField.DataSource = dtcurrentTableCumtomFields;
                                    grdCustomField.DataBind();
                                }
                            }
                        }//Add Mode End
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["noticeInstanceID"] != null)
                            {
                                long caseInstanceID = Convert.ToInt64(ViewState["noticeInstanceID"]);

                                if (txtFieldValue_Footer != null && ddlFieldName_Footer != null && caseInstanceID != 0)
                                {
                                    bool validateData = false;
                                    bool saveSuccess = false;

                                    if (!String.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue) && ddlFieldName_Footer.SelectedValue != "0")
                                    {
                                        if (txtFieldValue_Footer.Text != "")
                                        {
                                            validateData = true;
                                        }
                                    }

                                    if (validateData)
                                    {
                                        lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);

                                        if (lblID != 0)
                                        {
                                            tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                            {
                                                NoticeCaseType = 1,
                                                NoticeCaseInstanceID = caseInstanceID,
                                                LabelID = lblID,
                                                LabelValue = txtFieldValue_Footer.Text,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);

                                            //if (CaseManagement.IsExistCustomeFieldParameterValue(ObjParameter))
                                            //{
                                            //    CaseManagement.CreateCustomeFieldParameterValue(ObjParameter);
                                            //    saveSuccess = true;
                                            //}

                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("N", caseInstanceID, "tbl_NoticeCaseCustomParameter", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Added", true);

                                                if (ViewState["NoticeTypeUpdated"] != null)
                                                {
                                                    if (Convert.ToString(ViewState["NoticeTypeUpdated"]) == "false")
                                                    {
                                                        saveSuccess = NoticeManagement.UpdateNoticeType(caseInstanceID, Convert.ToInt32(ddlNoticeCategory.SelectedValue));
                                                        if (saveSuccess)
                                                        {
                                                            saveSuccess = LitigationManagement.DeletePreviousCustomParameter(1, caseInstanceID, Convert.ToInt32(ddlNoticeCategory.SelectedValue));

                                                            if (saveSuccess)
                                                                ViewState["NoticeTypeUpdated"] = "true";
                                                        }
                                                    }
                                                }

                                                //Re-Bind Case Custom Parameter Details
                                                if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                                {
                                                    BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                                                }
                                                else
                                                {
                                                    BindCustomFields(grdCustomField, grdCustomField_History);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }//Edit Mode End
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnAddCustomField_TaxLitigation_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    LinkButton lnkBtn = (LinkButton)sender;
                    GridViewRow gvRow = (GridViewRow)lnkBtn.NamingContainer;
                    GridView gridView = (GridView)gvRow.NamingContainer;

                    if (gridView != null)
                    {
                        int lblID = 0;
                        DropDownList ddlFieldName_Footer = (DropDownList)gridView.FooterRow.FindControl("ddlFieldName_Footer");

                        TextBox txtFieldValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtFieldValue_Footer");
                        TextBox txtInterestValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtInterestValue_Footer");
                        TextBox txtPenaltyValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtPenaltyValue_Footer");
                        TextBox tbxRowTotalValue_Footer = (TextBox)gridView.FooterRow.FindControl("tbxRowTotalValue_Footer");

                        TextBox tbxSettlement_Footer = (TextBox)gridView.FooterRow.FindControl("tbxSettlement_Footer");
                        TextBox tbxProvisionInbooks_Footer = (TextBox)gridView.FooterRow.FindControl("tbxProvisionInbooks_Footer");

                        if (ddlFieldName_Footer != null && txtFieldValue_Footer != null
                            && txtInterestValue_Footer != null && txtPenaltyValue_Footer != null && tbxRowTotalValue_Footer != null
                            && tbxSettlement_Footer != null && tbxProvisionInbooks_Footer != null)
                        {
                            if ((int)ViewState["Mode"] == 0)
                            {
                                if (!string.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue))
                                {
                                    lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);
                                }
                                string lblName = ddlFieldName_Footer.SelectedItem.Text;



                                if (ViewState["dataTableCustomFields"] != null && lblID != 0)
                                {
                                    DataTable dtcurrentTableCumtomFields = (DataTable)ViewState["dataTableCustomFields"];

                                    DataRow drNewRow = dtcurrentTableCumtomFields.NewRow();

                                    drNewRow["LableID"] = lblID;
                                    drNewRow["Label"] = lblName;
                                    drNewRow["labelValue"] = txtFieldValue_Footer.Text;
                                    drNewRow["Interest"] = txtInterestValue_Footer.Text;
                                    drNewRow["Penalty"] = txtPenaltyValue_Footer.Text;
                                    drNewRow["Total"] = tbxRowTotalValue_Footer.Text;
                                    drNewRow["SettlementValue"] = tbxSettlement_Footer.Text;
                                    drNewRow["ProvisionInbook"] = tbxProvisionInbooks_Footer.Text;

                                    //add new row to DataTable
                                    dtcurrentTableCumtomFields.Rows.Add(drNewRow);

                                    //Delete Rows with blank LblID (if Any)
                                    dtcurrentTableCumtomFields = LitigationManagement.LoopAndDeleteBlankRows(dtcurrentTableCumtomFields);

                                    ViewState["CustomefieldCount"] = dtcurrentTableCumtomFields.Rows.Count;

                                    //Store the current data to ViewState
                                    ViewState["CurrentTablePutCallDtls"] = dtcurrentTableCumtomFields;

                                    if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                    {
                                        dtcurrentTableCumtomFields = LitigationManagement.GetColumnTotal(dtcurrentTableCumtomFields, true);

                                        //Rebind the Grid with the current data
                                        gridView.DataSource = dtcurrentTableCumtomFields;
                                        gridView.DataBind();
                                    }
                                    else
                                    {
                                        //Rebind the Grid with the current data
                                        grdCustomField.DataSource = dtcurrentTableCumtomFields;
                                        grdCustomField.DataBind();
                                    }
                                }
                            }//Add Mode End
                            else if ((int)ViewState["Mode"] == 1)
                            {
                                if (ViewState["noticeInstanceID"] != null)
                                {
                                    long noticeInstanceID = Convert.ToInt64(ViewState["noticeInstanceID"]);

                                    if (txtFieldValue_Footer != null && ddlFieldName_Footer != null && noticeInstanceID != 0)
                                    {
                                        bool validateData = false;
                                        bool saveSuccess = false;

                                        if (!String.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue) && ddlFieldName_Footer.SelectedValue != "0")
                                        {
                                            if (txtFieldValue_Footer.Text != "")
                                            {
                                                validateData = true;
                                            }
                                        }

                                        if (validateData)
                                        {
                                            lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);

                                            if (lblID != 0)
                                            {
                                                tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                {
                                                    NoticeCaseType = 2,
                                                    NoticeCaseInstanceID = noticeInstanceID,
                                                    LabelID = lblID,
                                                    LabelValue = txtFieldValue_Footer.Text,

                                                    Penalty = txtPenaltyValue_Footer.Text,
                                                    Interest = txtInterestValue_Footer.Text,
                                                    Total = tbxRowTotalValue_Footer.Text,
                                                    SettlementValue = tbxSettlement_Footer.Text,
                                                    ProvisionInBook = tbxProvisionInbooks_Footer.Text,

                                                    IsDeleted = false,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);

                                                //if (CaseManagement.IsExistCustomeFieldParameterValue(ObjParameter))
                                                //{
                                                //    CaseManagement.CreateCustomeFieldParameterValue(ObjParameter);
                                                //    saveSuccess = true;
                                                //}

                                                if (saveSuccess)
                                                {
                                                    LitigationManagement.CreateAuditLog("N", Convert.ToInt32(ViewState["noticeInstanceID"]), "tbl_NoticeCaseCustomParameter", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Added", true);

                                                    if (ViewState["NoticeTypeUpdated"] != null)
                                                    {
                                                        if (Convert.ToString(ViewState["NoticeTypeUpdated"]) == "false")
                                                        {
                                                            saveSuccess = NoticeManagement.UpdateNoticeType(noticeInstanceID, Convert.ToInt32(ddlNoticeCategory.SelectedValue));
                                                            if (saveSuccess)
                                                            {
                                                                saveSuccess = LitigationManagement.DeletePreviousCustomParameter(2, noticeInstanceID, Convert.ToInt32(ddlNoticeCategory.SelectedValue));

                                                                if (saveSuccess)
                                                                    ViewState["NoticeTypeUpdated"] = "true";
                                                            }
                                                        }
                                                    }

                                                    //Re-Bind Case Custom Parameter Details
                                                    if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                                    {
                                                        BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                                                    }
                                                    else
                                                    {
                                                        BindCustomFields(grdCustomField, grdCustomField_History);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }//Edit Mode End
                        }
                    }//Grid Check
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void TextChangedInsideGridView_TextChanged(object sender, EventArgs e)
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            if (currentRow != null)
            {
                if (currentRow.RowType == DataControlRowType.DataRow)
                {
                    TextBox tbxLabelValue = (TextBox)currentRow.FindControl("tbxLabelValue");
                    TextBox tbxInterestValue = (TextBox)currentRow.FindControl("tbxInterestValue");
                    TextBox tbxPenaltyValue = (TextBox)currentRow.FindControl("tbxPenaltyValue");
                    TextBox tbxRowTotalValue = (TextBox)currentRow.FindControl("tbxRowTotalValue");

                    if (tbxLabelValue != null && tbxInterestValue != null && tbxPenaltyValue != null && tbxRowTotalValue != null)
                    {
                        tbxRowTotalValue.Text = (LitigationManagement.csvToNumber(tbxLabelValue.Text) +
                            LitigationManagement.csvToNumber(tbxInterestValue.Text) +
                            LitigationManagement.csvToNumber(tbxPenaltyValue.Text)).ToString("N2");
                    }
                }
                else if (currentRow.RowType == DataControlRowType.Footer)
                {
                    TextBox txtFieldValue_Footer = (TextBox)currentRow.FindControl("txtFieldValue_Footer");
                    TextBox txtInterestValue_Footer = (TextBox)currentRow.FindControl("txtInterestValue_Footer");
                    TextBox txtPenaltyValue_Footer = (TextBox)currentRow.FindControl("txtPenaltyValue_Footer");
                    TextBox tbxRowTotalValue_Footer = (TextBox)currentRow.FindControl("tbxRowTotalValue_Footer");

                    if (txtFieldValue_Footer != null && txtInterestValue_Footer != null && txtPenaltyValue_Footer != null && tbxRowTotalValue_Footer != null)
                    {
                        tbxRowTotalValue_Footer.Text = (LitigationManagement.csvToNumber(txtFieldValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtInterestValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtPenaltyValue_Footer.Text)).ToString("N2");
                    }
                }
            }
        }

        protected void grdCustomField_History_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsAllowed = (Label)e.Row.FindControl("lblIsAllowed");
                    DropDownList ddlGroundResult = (DropDownList)e.Row.FindControl("ddlGroundResult");

                    if (ddlGroundResult != null && lblIsAllowed != null)
                    {
                        if (lblIsAllowed.Text != null && lblIsAllowed.Text != "")
                        {
                            string isAllowed = "0";

                            if (lblIsAllowed.Text == "True")
                                isAllowed = "1";
                            else if (lblIsAllowed.Text == "False")
                                isAllowed = "0";

                            ddlGroundResult.ClearSelection();

                            if (ddlGroundResult.Items.FindByValue(isAllowed) != null)
                                ddlGroundResult.Items.FindByValue(isAllowed).Selected = true;
                        }
                    }

                    Label lblPenalty = (Label)e.Row.FindControl("lblPenalty");

                    if (lblPenalty != null)
                    {
                        if (string.IsNullOrEmpty(lblPenalty.Text))
                            lblPenalty.Visible = false;
                    }

                    Label lblInterest = (Label)e.Row.FindControl("lblInterest");

                    if (lblInterest != null)
                    {
                        if (string.IsNullOrEmpty(lblInterest.Text))
                            lblInterest.Visible = false;
                    }

                    Label lblSettlementValue = (Label)e.Row.FindControl("lblSettlementValue");

                    if (lblSettlementValue != null)
                    {
                        if (string.IsNullOrEmpty(lblSettlementValue.Text))
                            lblSettlementValue.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlGroundResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlCurrentGroundResult = (DropDownList)sender;

            if (ddlCurrentGroundResult != null)
            {
                if (ddlCurrentGroundResult.SelectedValue != null && ddlCurrentGroundResult.SelectedValue != "")
                {
                    if (ddlCurrentGroundResult.SelectedValue == "1") //1-Allowed
                    {
                        GridViewRow grdDropDownRow = ((GridViewRow)ddlCurrentGroundResult.Parent.Parent);
                        if (grdDropDownRow != null)
                        {
                            TextBox tbxSettlementValue = (TextBox)grdDropDownRow.FindControl("tbxSettlementValue");
                            if (tbxSettlementValue != null)
                                tbxSettlementValue.Visible = true;
                            else
                                tbxSettlementValue.Visible = false;
                        }
                    }
                }
            }
        }

        #endregion

        private bool saveNoticeToCaseTransfer(long oldNoticeInstanceID, out long newCaseID_Out)
        {
            bool saveSuccess = false;
            newCaseID_Out = 0;
            try
            {
                //STATUS UPDATE
                #region Status UPDATE ---To Closed

                int selectedStatusID = Convert.ToInt32(ddlNoticeStatus.SelectedValue);

                //Status Transaction Record - Which will Create or Update on Each Status Move
                tbl_LegalNoticeStatusTransaction newStatusTxnRecord = new tbl_LegalNoticeStatusTransaction()
                {
                    NoticeInstanceID = oldNoticeInstanceID,
                    StatusID = selectedStatusID,
                    StatusChangeOn = DateTime.Now,
                    IsActive = true,
                    IsDeleted = false,
                    UserID = AuthenticationHelper.UserID,
                    RoleID = 3,
                    CreatedBy = AuthenticationHelper.UserID,
                    UpdatedBy = AuthenticationHelper.UserID,
                };

                //Status Record - i.e. Notice Closure Record; Only Active on Notice Close otherwise DeActive 
                tbl_LegalNoticeStatus newStatusRecord = new tbl_LegalNoticeStatus()
                {
                    NoticeInstanceID = oldNoticeInstanceID,
                    StatusID = selectedStatusID,
                    CloseDate = DateTime.Now,
                    IsActive = true,
                    IsDeleted = false,

                    CreatedBy = AuthenticationHelper.UserID,
                    UpdatedBy = AuthenticationHelper.UserID,
                };

                if (tbxCloseRemark.Text != "")
                    newStatusRecord.ClosureRemark = tbxCloseRemark.Text;

                //Create or Update Status Transaction Records
                if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusTxnRecord))
                {
                    saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusTxnRecord);
                    saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusTxnRecord);
                }
                else
                    saveSuccess = NoticeManagement.UpdateNoticeStatusTransaction(newStatusTxnRecord);

                //Create or Update Status Record
                if (!NoticeManagement.ExistNoticeStatus(newStatusRecord))
                {
                    saveSuccess = NoticeManagement.CreateNoticeStatus(newStatusRecord);
                    if (saveSuccess)
                    {
                        LitigationManagement.CreateAuditLog("N", oldNoticeInstanceID, "tbl_LegalNoticeStatus", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Status Created", true);
                    }
                }
                else
                {
                    saveSuccess = NoticeManagement.UpdateNoticeStatus(newStatusRecord);
                    if (saveSuccess)
                    {
                        LitigationManagement.CreateAuditLog("N", oldNoticeInstanceID, "tbl_LegalNoticeStatus", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Status Updated", true);
                    }
                }

                #endregion

                #region Notice-Case Transfer
                if (saveSuccess)
                {
                    var noticeDetail = NoticeManagement.GetNoticeByID(Convert.ToInt32(oldNoticeInstanceID));

                    if (noticeDetail != null)
                    {
                        long NewCaseID = 0;

                        #region Create New Case
                        //New Case Record
                        tbl_LegalCaseInstance NewCaseRecord = new tbl_LegalCaseInstance()
                        {
                            IsDeleted = false,
                            CaseType = noticeDetail.NoticeType,
                            CaseRefNo = tbxAppealCaseNo.Text.Trim(),
                            //CaseRefNo = tbxRefNo.Text.Trim(),
                            //OpenDate = DateTimeExtensions.GetDate(txtCaseDate.Text),
                            Section = noticeDetail.Section,
                            CaseCategoryID = noticeDetail.NoticeCategoryID,
                            CaseTitle = noticeDetail.NoticeTitle,
                            CaseDetailDesc = noticeDetail.NoticeDetailDesc,
                            CustomerBranchID = noticeDetail.CustomerBranchID,
                            DepartmentID = noticeDetail.DepartmentID,
                            ContactPersonOfDepartment = noticeDetail.ContactPersonOfDepartment,
                            OwnerID = noticeDetail.OwnerID,

                            ClaimAmt = noticeDetail.ClaimAmt,
                            ProbableAmt = noticeDetail.ProbableAmt,
                            Provisionalamt = noticeDetail.Provisionalamt,
                            BankGurantee = noticeDetail.BankGurantee,
                            ProtestMoney = noticeDetail.ProtestMoney,
                            AssignmentType = noticeDetail.AssignmentType,
                            CaseRiskID = noticeDetail.NoticeRiskID,
                            PreDeposit = noticeDetail.PreDeposit,
                            PostDeposit = noticeDetail.PostDeposit,
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };

                        NewCaseID = CaseManagement.CreateCase(NewCaseRecord);

                        if (NewCaseID > 0)
                            saveSuccess = true;

                        #endregion

                        if (saveSuccess)
                        {
                            newCaseID_Out = NewCaseID;

                            // Notice Case Mapping Save Data
                            LitigationManagement.CreateAuditLog("N", oldNoticeInstanceID, "tbl_LegalNoticeInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Converted to New Case", true);
                            LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Created from Notice", true);

                            #region Notice Case Mapping Save Data
                            tbl_NoticeCaseMapping objNoticeCaseMapping = new tbl_NoticeCaseMapping()
                            {
                                NoticeCaseInstanceID = oldNoticeInstanceID,
                                NewCaseInstanceID = NewCaseID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                NoticeCaseType = 2
                            };

                            saveSuccess = CaseManagement.CreateNoticeToCaseMapping(objNoticeCaseMapping);

                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("N", NewCaseID, "tbl_NoticeCaseMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Case Mapping Created", true);
                            }
                            #endregion

                            //Case Status Transaction --Open
                            #region Status Transaction
                            tbl_LegalCaseStatusTransaction newCaseStatusRecord = new tbl_LegalCaseStatusTransaction()
                            {
                                CaseInstanceID = NewCaseID,
                                StatusID = 1,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false,
                                UserID = AuthenticationHelper.UserID,
                                RoleID = 3,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            if (!CaseManagement.ExistCaseStatusTransaction(newCaseStatusRecord))
                                saveSuccess = CaseManagement.CreateCaseStatusTransaction(newCaseStatusRecord);

                            #endregion

                            //Lawyer Mapping
                            #region Lawyer Mapping

                            var noticeLawyerMapping = NoticeManagement.GetNoticeLawyerMapping(Convert.ToInt32(oldNoticeInstanceID));

                            tbl_LegalCaseLawyerMapping objCaseLawyerMapping = new tbl_LegalCaseLawyerMapping()
                            {
                                CaseInstanceID = NewCaseID,
                                IsActive = true,
                                LawyerID = Convert.ToInt32(noticeLawyerMapping.LawyerID),
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };
                            saveSuccess = CaseManagement.CreateCaseLawyerMapping(objCaseLawyerMapping);
                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("N", NewCaseID, "tbl_LegalCaseLawyerMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Lawyer Mapping Created", true);
                            }
                            #endregion

                            #region Save Party Mapping

                            var ListofParty = CaseManagement.GetListOfParty(oldNoticeInstanceID, 2);

                            if (ListofParty.Count > 0)
                            {
                                List<tbl_PartyMapping> lstObjPartyMapping = new List<tbl_PartyMapping>();

                                ListofParty.ForEach(EachParty =>
                                {
                                    tbl_PartyMapping objPartyMapping = new tbl_PartyMapping()
                                    {
                                        CaseNoticeInstanceID = NewCaseID,
                                        IsActive = true,
                                        Type = 1,//1 as Case and 2 as Notice
                                        PartyID = Convert.ToInt32(EachParty.PartyID),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjPartyMapping.Add(objPartyMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdatePartyMapping(lstObjPartyMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", NewCaseID, "tbl_PartyMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Party Mapped", true);
                                }

                                //Refresh List
                                lstObjPartyMapping.Clear();
                                lstObjPartyMapping = null;
                            }
                            #endregion

                            #region Save Act Mapping
                            var ListofAct = CaseManagement.GetListOfAct(oldNoticeInstanceID, 1);

                            if (ListofAct.Count > 0)
                            {
                                List<tbl_ActMapping> lstObjActMapping = new List<tbl_ActMapping>();

                                ListofAct.ForEach(EachAct =>
                                {
                                    tbl_ActMapping objActMapping = new tbl_ActMapping()
                                    {
                                        CaseNoticeInstanceID = NewCaseID,
                                        IsActive = true,
                                        Type = 1, //1 as Case and 2 as Notice
                                        ActID = Convert.ToInt32(EachAct.ActID),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjActMapping.Add(objActMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateActMapping(lstObjActMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", NewCaseID, "tbl_ActMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Act Mapped", true);
                                }

                                //Refresh List
                                lstObjActMapping.Clear();
                                lstObjActMapping = null;
                            }
                            #endregion

                            #region Save Financial Year Mapping
                            var ListofFY = CaseManagement.GetListOfFY(oldNoticeInstanceID, 2);

                            if (ListofFY.Count > 0)
                            {
                                List<FinancialYearMapping> lstObjFYMapping = new List<FinancialYearMapping>();

                                ListofFY.ForEach(EachFYID =>
                                {
                                    FinancialYearMapping objFYMapping = new FinancialYearMapping()
                                    {
                                        FYID = Convert.ToString(EachFYID.FYID),
                                        Type = 2,//1 as Case and 2 as Notice
                                        CaseNoticeInstanceID = NewCaseID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjFYMapping.Add(objFYMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateFYMapping(lstObjFYMapping);

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "FinancialYearMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Financial Year Mapped.", false);
                                }
                                //Refresh List
                                lstObjFYMapping.Clear();
                                lstObjFYMapping = null;

                                ListofFY.Clear();
                                ListofFY = null;
                            }
                            #endregion

                            #region Save Opposition Lawyer Mapping

                            var ListofOppoLawyer = CaseManagement.GetListOfOppoLaywer(oldNoticeInstanceID, 2);

                            if (ListofOppoLawyer.Count > 0)
                            {
                                List<tbl_OppositionLawyerList> lstObjLawyerMapping = new List<tbl_OppositionLawyerList>();

                                ListofOppoLawyer.ForEach(EachLawyer =>
                                {
                                    tbl_OppositionLawyerList objLawyerMapping = new tbl_OppositionLawyerList()
                                    {
                                        CaseNoticeInstanceID = NewCaseID,
                                        LawyerID = Convert.ToInt32(EachLawyer.LawyerID),
                                        IsActive = true,
                                        Type = 1, //1 as Case and 2 as Notice                                        
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjLawyerMapping.Add(objLawyerMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateOppositionLawyerMapping(lstObjLawyerMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("N", NewCaseID, "tbl_OppositionLawyerList", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Opposition Lawyer Mapped", true);
                                }

                                //Refresh List
                                lstObjLawyerMapping.Clear();
                                lstObjLawyerMapping = null;
                            }
                            #endregion

                            //User Assignment
                            #region User Assignment

                            //Get Notice User Assignment
                            var lstNoticeUserAssignment = NoticeManagement.GetNoticeAssignment(Convert.ToInt32(oldNoticeInstanceID));

                            if (lstNoticeUserAssignment.Count > 0)
                            {
                                lstNoticeUserAssignment.ForEach(eachNoticeAssignedUserRecord =>
                                {
                                    tbl_LegalCaseAssignment newAssignment = new tbl_LegalCaseAssignment()
                                    {
                                        AssignmentType = eachNoticeAssignedUserRecord.AssignmentType,
                                        CaseInstanceID = NewCaseID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,

                                        UserID = eachNoticeAssignedUserRecord.UserID,
                                        RoleID = eachNoticeAssignedUserRecord.RoleID,
                                    };

                                    if (!CaseManagement.ExistCaseAssignment(newAssignment))
                                    {
                                        saveSuccess = CaseManagement.CreateCaseAssignment(newAssignment);
                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("N", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case-User Assignment Created", true);
                                        }
                                    }
                                });
                            }

                            #endregion
                        }


                    }
                }

                #endregion

                return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                newCaseID_Out = 0;
                return saveSuccess;
            }
        }

        protected void btnSaveCustomFieldCaseTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
                {
                    int oldNoticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                    bool dataValidateSuccess = false;

                    #region Data Validation

                    if (ddlNoticeCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                        dataValidateSuccess = false;
                    else
                        dataValidateSuccess = true;

                    if (grdCustomField_CaseTransfer.Rows.Count > 0)
                    {
                        foreach (GridViewRow eachRow in grdCustomField_CaseTransfer.Rows)
                        {
                            if (eachRow.RowType == DataControlRowType.DataRow)
                            {
                                DropDownList ddlGroundResult = (DropDownList)eachRow.FindControl("ddlGroundResult");

                                if (ddlGroundResult != null)
                                {
                                    if (ddlGroundResult.SelectedValue != "") //0-Disallowed, 1-Allowed
                                    {
                                        Label lblID = (Label)eachRow.FindControl("lblID");
                                        Label tbxLabelValue = (Label)eachRow.FindControl("tbxLabelValue");
                                        Label tbxInterestValue = (Label)eachRow.FindControl("tbxInterestValue");
                                        Label tbxPenaltyValue = (Label)eachRow.FindControl("tbxPenaltyValue");
                                        Label tbxRowTotalValue = (Label)eachRow.FindControl("tbxRowTotalValue");
                                        Label tbxProvisionInbooks = (Label)eachRow.FindControl("tbxProvisionInbooks");

                                        TextBox tbxSettlementValue = (TextBox)eachRow.FindControl("tbxSettlementValue");

                                        if (lblID != null && tbxLabelValue != null
                                            && tbxInterestValue != null && tbxPenaltyValue != null && tbxRowTotalValue != null && tbxSettlementValue != null && tbxProvisionInbooks != null)
                                        {
                                            if (lblID.Text != "")
                                            {
                                                if (tbxSettlementValue.Text != "")
                                                {
                                                    if (LitigationManagement.csvToNumber(tbxSettlementValue.Text) != 0)
                                                    {
                                                        dataValidateSuccess = true;
                                                    }
                                                    else
                                                    {
                                                        cvNoticeStatus.IsValid = false;
                                                        cvNoticeStatus.ErrorMessage = "Please enter Valid Number in Settlement Value";
                                                        ValidationSummary3.CssClass = "alert alert-danger";
                                                    }
                                                }
                                                else
                                                    dataValidateSuccess = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }//END ForEach
                    }
                    else
                        dataValidateSuccess = true;

                    #endregion

                    if (dataValidateSuccess)
                    {
                        bool saveSuccess = false;
                        long NewCaseID = 0;

                        saveSuccess = saveNoticeToCaseTransfer(oldNoticeInstanceID, out NewCaseID);

                        if (saveSuccess && NewCaseID != 0)
                        {
                            #region Save Custom Field

                            if (grdCustomField_CaseTransfer.Rows.Count > 0)
                            {
                                foreach (GridViewRow eachRow in grdCustomField_CaseTransfer.Rows)
                                {
                                    if (eachRow.RowType == DataControlRowType.DataRow)
                                    {
                                        DropDownList ddlGroundResult = (DropDownList)eachRow.FindControl("ddlGroundResult");

                                        if (ddlGroundResult != null)
                                        {
                                            if (ddlGroundResult.SelectedValue != "") //0-Disallowed, 1-Allowed
                                            {
                                                Label lblID = (Label)eachRow.FindControl("lblID");
                                                Label tbxLabelValue = (Label)eachRow.FindControl("tbxLabelValue");
                                                Label tbxInterestValue = (Label)eachRow.FindControl("tbxInterestValue");
                                                Label tbxPenaltyValue = (Label)eachRow.FindControl("tbxPenaltyValue");
                                                Label tbxRowTotalValue = (Label)eachRow.FindControl("tbxRowTotalValue");
                                                Label tbxProvisionInbooks = (Label)eachRow.FindControl("tbxProvisionInbooks");

                                                TextBox tbxSettlementValue = (TextBox)eachRow.FindControl("tbxSettlementValue");

                                                if (lblID != null && tbxLabelValue != null
                                                    && tbxInterestValue != null && tbxPenaltyValue != null && tbxRowTotalValue != null
                                                    && tbxSettlementValue != null && tbxProvisionInbooks != null)
                                                {
                                                    if (lblID.Text != "")
                                                    {
                                                        //Update OLD Notice parameters Settlement and Allowed Values
                                                        tbl_NoticeCaseCustomParameter oldNoticeCustomParameter = new tbl_NoticeCaseCustomParameter()
                                                        {
                                                            NoticeCaseType = 2,
                                                            NoticeCaseInstanceID = oldNoticeInstanceID,

                                                            LabelID = Convert.ToInt64(lblID.Text),
                                                            LabelValue = tbxLabelValue.Text,
                                                            Interest = tbxInterestValue.Text,
                                                            Penalty = tbxPenaltyValue.Text,
                                                            Total = tbxPenaltyValue.Text,
                                                            SettlementValue = tbxSettlementValue.Text,
                                                            ProvisionInBook = tbxProvisionInbooks.Text,

                                                            IsAllowed = Convert.ToBoolean(Convert.ToInt32(ddlGroundResult.SelectedValue)),

                                                            IsActive = true,
                                                            IsDeleted = false,
                                                            CreatedBy = AuthenticationHelper.UserID,
                                                            CreatedOn = DateTime.Now,
                                                            UpdatedBy = AuthenticationHelper.UserID,
                                                        };

                                                        saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(oldNoticeCustomParameter);

                                                        //Create NEW Case Custom Parameters only Dis-Allowed Values
                                                        if (ddlGroundResult.SelectedValue == "0")
                                                        {
                                                            tbl_NoticeCaseCustomParameter newCaseCustomParameter = new tbl_NoticeCaseCustomParameter()
                                                            {
                                                                NoticeCaseType = 1,
                                                                NoticeCaseInstanceID = NewCaseID,

                                                                LabelID = Convert.ToInt64(lblID.Text),
                                                                LabelValue = tbxLabelValue.Text,
                                                                Interest = tbxInterestValue.Text,
                                                                Penalty = tbxPenaltyValue.Text,
                                                                Total = tbxPenaltyValue.Text,
                                                                //SettlementValue = tbxSettlementValue.Text,
                                                                ProvisionInBook = tbxProvisionInbooks.Text,

                                                                IsActive = true,
                                                                IsDeleted = false,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedOn = DateTime.Now,
                                                                UpdatedBy = AuthenticationHelper.UserID,
                                                            };

                                                            saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(newCaseCustomParameter);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }//END FOR EACH 
                            }

                            #endregion
                        }
                        if (saveSuccess)
                        {
                            cvNoticeStatus.IsValid = false;
                            cvNoticeStatus.ErrorMessage = "Notice Transferred to New Case Successfully.";
                            ValidationSummary3.CssClass = "alert alert-success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showHideButtons(bool flag)
        {
            try
            {
                pnlLawRating.Enabled = flag;
                //divCaseHistory.Visible = flag;
                btnEditNoticeDetail.Visible = flag;
                lnkActDetails.Visible = flag;
                btnSendMailPopup.Visible = flag;
                lnkBtnEditUserAssignment.Visible = flag;
                btnAddPromotor.Visible = flag;
                //lnkApply.Visible = flag;
                lnkAddNewDoctype.Visible = flag;
                lnkLinkNotice.Visible = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void HideShowGridColumns(GridView gridView, string headerTextToMatch, bool flag)
        {
            try
            {
                if (gridView == null)
                {
                    return;
                }

                // Loop through all of the columns in the grid.
                for (int i = 0; i < gridView.Columns.Count; i++)
                {
                    String headerText = gridView.Columns[i].HeaderText;

                    //Show Hide Columns with Specific headerText
                    if (headerText == headerTextToMatch)
                        gridView.Columns[i].Visible = flag;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        #region Linked Notice

        //On Page
        private void BindLinkedNotices(long noticeInstanceID)
        {
            if (noticeInstanceID != 0)
            {
                var lstNoticeDetails = LitigationManagement.GetLinkedNoticeCaseList(Convert.ToInt32(AuthenticationHelper.CustomerID), noticeInstanceID, 2);

                grdLinkedNotices.DataSource = lstNoticeDetails;
                grdLinkedNotices.DataBind();

                if (lstNoticeDetails.Count > 0)
                    divLinkedCases.Visible = true;
                else
                    divLinkedCases.Visible = false;

                lstNoticeDetails.Clear();
                lstNoticeDetails = null;

                if (upLinkedNotices != null)
                    upLinkedNotices.Update();
            }
        }

        //On Popup
        private void BindNoticeListToLink(long noticeInstanceID)
        {
            try
            {
                if (noticeInstanceID != 0)
                {
                    int partyID = -1;
                    int deptID = -1;
                    int noticeStatus = -1;
                    string noticeType = string.Empty;
                    if (!string.IsNullOrEmpty(ddlLinkNoticeStatus.SelectedValue))
                    {
                        if (ddlLinkNoticeStatus.SelectedValue != "-1")
                        {
                            noticeStatus = Convert.ToInt32(ddlLinkNoticeStatus.SelectedValue);
                        }
                    }
                    List<int> branchList = new List<int>();

                    var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, noticeStatus, noticeType);

                    if (lstNoticeDetails.Count > 0)
                        lstNoticeDetails = lstNoticeDetails.Where(row => row.NoticeInstanceID != noticeInstanceID).ToList();

                    var lstAlreadyLinkedNotices = CaseManagement.GetLinkedCaseNoticeIDs(Convert.ToInt32(AuthenticationHelper.CustomerID), 2, noticeInstanceID);

                    if (lstAlreadyLinkedNotices.Count > 0)
                        lstNoticeDetails = lstNoticeDetails.Where(row => !lstAlreadyLinkedNotices.Contains(row.NoticeInstanceID)).ToList();

                    if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                    {
                        lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.RefNo.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                    }
                    grdNoticeList_LinkNotice.DataSource = lstNoticeDetails;
                    grdNoticeList_LinkNotice.DataBind();

                    lstNoticeDetails.Clear();
                    lstNoticeDetails = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveLinkNotice_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                List<long> lstNoticeInstanceIDsToLink = new List<long>();
                int totalRecordSaveCount = 0;

                if (ViewState["noticeInstanceID"] != null)
                {
                    long noticeInstanceID = Convert.ToInt64(ViewState["noticeInstanceID"]);

                    for (int i = 0; i < grdNoticeList_LinkNotice.Rows.Count; i++)
                    {
                        CheckBox chkRowLinkCases = (CheckBox)grdNoticeList_LinkNotice.Rows[i].FindControl("chkRowLinkCases");

                        if (chkRowLinkCases != null)
                        {
                            if (chkRowLinkCases.Checked)
                            {
                                Label lblNoticeInstanceID = (Label)grdNoticeList_LinkNotice.Rows[i].FindControl("lblNoticeInstanceID");

                                if (lblNoticeInstanceID != null)
                                {
                                    if (lblNoticeInstanceID.Text != "")
                                        lstNoticeInstanceIDsToLink.Add(Convert.ToInt64(lblNoticeInstanceID.Text));
                                }
                            }
                        }
                    }

                    if (lstNoticeInstanceIDsToLink.Count > 0)
                    {
                        lstNoticeInstanceIDsToLink.ForEach(eachCaseToLink =>
                        {
                            tbl_NoticeCaseLinking newRecord = new tbl_NoticeCaseLinking()
                            {
                                Type = 2,
                                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                NoticeOrCaseInstanceID = noticeInstanceID,
                                LinkedNoticeOrCaseInstanceID = eachCaseToLink,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,

                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                            };

                            saveSuccess = LitigationManagement.CreateUpdateNoticeCaseLinking(newRecord);

                            ////if Cross Linking also to be Save
                            //tbl_NoticeCaseLinking otherRecord = new tbl_NoticeCaseLinking()
                            //{
                            //    Type = 2,
                            //    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            //    NoticeOrCaseInstanceID = eachCaseToLink,
                            //    LinkedNoticeOrCaseInstanceID = noticeInstanceID,

                            //    IsActive = true,
                            //    CreatedBy = AuthenticationHelper.UserID,
                            //    CreatedOn = DateTime.Now,

                            //    UpdatedBy = AuthenticationHelper.UserID,
                            //    UpdatedOn = DateTime.Now,
                            //};

                            //saveSuccess =LitigationManagement.CreateUpdateNoticeCaseLinking(otherRecord);

                            if (saveSuccess)
                                totalRecordSaveCount++;
                        });
                    }

                    if (saveSuccess)
                    {
                        LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_NoticeCaseLinking", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, totalRecordSaveCount + " Notice(s) Linked", true);

                        BindNoticeListToLink(noticeInstanceID);
                        BindLinkedNotices(noticeInstanceID);

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptUnCheckAllGridCheckBox", "unCheckAll();", true);

                        cvLinkNotice.IsValid = false;
                        cvLinkNotice.ErrorMessage = totalRecordSaveCount + " Selected Notice(s) Linked Successfully.";
                        vsLinkCase.CssClass = "alert alert-success";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnsearchDoc_Click(object sender, EventArgs e)
        {
            BindNoticeRelatedDocuments_All(Convert.ToInt32(ViewState["noticeInstanceID"]));
        }
        public void DeleteCaseFile(int caseFileID)
        {
            try
            {
                if (caseFileID != 0)
                {
                    if (CaseManagement.DeleteCaseDocument(caseFileID, AuthenticationHelper.UserID))
                    {
                        cvNoticeDocument.IsValid = false;
                        cvNoticeDocument.ErrorMessage = "Document Deleted Successfully.";
                        cvNoticeDocument.CssClass = "alert alert-success";
                        BindFileTags();
                        if (ViewState["CaseInstanceID"] != null)
                        {
                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document Deleted", true);
                        }
                    }
                    else
                    {
                        BindFileTags();
                        cvNoticeDocument.IsValid = false;
                        cvNoticeDocument.ErrorMessage = "Something went wrong, Please try again.";
                        cvNoticeDocument.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                cvNoticePopUp.CssClass = "alert alert-danger";
            }
        }

        public void DownloadCaseDocument(int caseFileID)
        {
            try
            {
                var file = CaseManagement.GetCaseDocumentByID(caseFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document Downloaded", false);
                            applyCSStoFileTag_ListItems();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                cvNoticePopUp.CssClass = "alert alert-danger";
            }
        }

        protected void grdCaseDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                LinkButton lnkBtnDownLoadCaseDoc = (LinkButton)e.Row.FindControl("lnkBtnDownLoadCaseDoc");

                if (lnkBtnDownLoadCaseDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDownLoadCaseDoc);
                }

                LinkButton lnkBtnDeleteCaseDoc = (LinkButton)e.Row.FindControl("lnkBtnDeleteCaseDoc");
                if (lnkBtnDeleteCaseDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteCaseDoc);

                    if (ViewState["caseStatus"] != null)
                    {
                        if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                            lnkBtnDeleteCaseDoc.Visible = false;
                        else
                            lnkBtnDeleteCaseDoc.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                    {
                        lnkBtnDeleteCaseDoc.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLinkedNotices_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                bool deleteSuccess = false;

                if (e.CommandArgument != null && ViewState["noticeInstanceID"] != null)
                {
                    if (e.CommandName.Equals("ViewNoticeCasePopup"))
                    {
                        string[] commandArgs = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                        int NoticeCaseInstanceID = Convert.ToInt32(commandArgs[0]);
                        int NoticeCaseType = Convert.ToInt32(commandArgs[1]);
                        string HistoryFlag = " true";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptOpenRefPopUp", "OpenCaseNoticeHistoryPopup(" + NoticeCaseInstanceID + "," + NoticeCaseType + "," + HistoryFlag + ",'L'" + ");", true);
                    }
                    else if (e.CommandName.Equals("DeleteNoticeLinking"))
                    {
                        string[] commandArgs = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                        long NoticeCaseInstanceID = Convert.ToInt64(commandArgs[0]);
                        long LinkedNoticeOrCaseInstanceID = Convert.ToInt64(commandArgs[1]);
                        int NoticeCaseType = Convert.ToInt32(commandArgs[2]);

                        if (NoticeCaseInstanceID != 0 && LinkedNoticeOrCaseInstanceID != 0 && NoticeCaseType != 0)
                        {
                            deleteSuccess = LitigationManagement.DeleteNoticeCaseLinking(NoticeCaseInstanceID, LinkedNoticeOrCaseInstanceID, NoticeCaseType, AuthenticationHelper.UserID);

                            if (deleteSuccess)
                            {
                                BindLinkedNotices(NoticeCaseInstanceID);
                                LitigationManagement.CreateAuditLog("N", NoticeCaseInstanceID, "tbl_NoticeCaseLinking", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Linking Deleted", true);

                                BindNoticeListToLink(NoticeCaseInstanceID);
                                cvLinkedNotices.IsValid = false;
                                cvLinkedNotices.ErrorMessage = "Notice Linking Deleted Successfully.";
                                vsLinkedNotices.CssClass = "alert alert-success";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLinkedNotices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdLinkedNotices.PageIndex = e.NewPageIndex;
                    BindLinkedNotices(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLinkedNotices_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkBtnDeleteNoticeLinking = (LinkButton)e.Row.FindControl("lnkBtnDeleteNoticeLinking");
                    if (lnkBtnDeleteNoticeLinking != null)
                    {
                        if (ViewState["noticeStatus"] != null)
                        {
                            if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                                lnkBtnDeleteNoticeLinking.Visible = false;
                            else
                                lnkBtnDeleteNoticeLinking.Visible = true;
                        }

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            lnkBtnDeleteNoticeLinking.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        protected void lnkOpposiLawyer_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);

                var lawyerUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);

                //Opposition Lawyer list 
                lstBoxOppositionLawyer.DataTextField = "Name";
                lstBoxOppositionLawyer.DataValueField = "ID";

                lstBoxOppositionLawyer.DataSource = lawyerUsers;
                lstBoxOppositionLawyer.DataBind();
                lstBoxOppositionLawyer.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                lawyerUsers.Clear();
                lawyerUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkLawyers_Click(object sender, EventArgs e)
        {
            ddlLawFirm_SelectedIndexChanged(null, null);
        }


        protected void lnkbindLawfirmp_Click(object sender, EventArgs e)
        {
            BindLawyer();
            ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScript", "rebindLawyerUser();", true);
        }

        protected void lnkBindshowDocumentCase_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "active");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");
            liLawyerRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
            {
                int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                BindNoticeRelatedDocuments_All(noticeInstanceID);
                BindFileTags();
                applyCSStoFileTag_ListItems();
            }
        }

        protected void lnkAddNewDoctype_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
            {
                int NoticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                ScriptManager.RegisterStartupScript(this, GetType(), "LitiationCaseDoc", "OpenDoumentPopup(" + NoticeInstanceID + ");", true);
                applyCSStoFileTag_ListItems();
            }
        }

        protected void lnkApply_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
            {
                int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                BindNoticeRelatedDocuments_All(noticeInstanceID);
            }
        }

        protected void ddlLayerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty((ddlLayerType.SelectedValue).ToString()))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["noticeInstanceID"])))
                    {
                        int layerID = Convert.ToInt32(ddlLayerType.SelectedValue);
                        int NoticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                        if (layerID > 0)
                        {
                            var documentData = (from row in entities.sp_LiDisplayCriteriaRatingNotice(NoticeInstanceID, layerID, Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                select row).ToList();

                            if (documentData != null)
                            {
                                grdLawyerRating.PageIndex = 0;
                                grdLawyerRating.DataSource = documentData;
                                grdLawyerRating.DataBind();
                            }
                        }
                    }
                }
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                // string ID = null;
                string caseinstantID = Convert.ToString(ViewState["noticeInstanceID"]);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCriteriaRetingPopUp('" + ddlLayerType.SelectedValue + "','" + caseinstantID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void lnkBtnRebindRating_Click(object sender, EventArgs e)
        {
            lnkLawyerRating_Click(sender, e);
        }
        //Added by Ruchi
        protected void lstBoxFileTags_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                BindNoticeRelatedDocuments(noticeInstanceID);

                List<System.Web.UI.WebControls.ListItem> lstSelectedTags = new List<System.Web.UI.WebControls.ListItem>();

                foreach (System.Web.UI.WebControls.ListItem eachListItem in lstBoxFileTags.Items)
                {
                    if (eachListItem.Selected)
                        lstSelectedTags.Add(eachListItem);
                }

                //Re-Arrange File Tags
                var arrangedListItems = LitigationManagement.ReArrange_FileTags(lstBoxFileTags);

                //lstBoxFileTags.DataSource = arrangedListItems;
                lstBoxFileTags.DataBind();

                foreach (System.Web.UI.WebControls.ListItem eachListItem in lstSelectedTags)
                {
                    if (lstBoxFileTags.Items.FindByValue(eachListItem.Value) != null)
                        lstBoxFileTags.Items.FindByValue(eachListItem.Value).Selected = true;
                }

                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindNoticeRelatedDocuments(int noticeInstanceID)
        {
            List<System.Web.UI.WebControls.ListItem> selectedItems = LitigationManagement.GetSelectedItems(lstBoxFileTags);

            var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

            List<Sp_Litigation_CaseDocument_Result> lstContDocs = new List<Sp_Litigation_CaseDocument_Result>();

            lstContDocs = LitigationDocumentManagement.Sp_Litigation_CaseDocument_Result(noticeInstanceID, selectedFileTags, AuthenticationHelper.CustomerID);

            grdNoticeDocuments.DataSource = lstContDocs;
            grdNoticeDocuments.DataBind();

            Session["TotalRows"] = lstContDocs.Count;
            lstContDocs.Clear();
            lstContDocs = null;
            upNoticeDocUploadPopup.Update();
        }

        private void BindFileTags()
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long noticeInstanceID = Convert.ToInt64(ViewState["noticeInstanceID"]);

                    if (noticeInstanceID != 0)
                    {
                        var lstTags = LitigationDocumentManagement.GetDistinctFileTagsNotice(customerID, noticeInstanceID);

                        lstBoxFileTags.Items.Clear();

                        if (lstTags.Count > 0)
                        {
                            for (int i = 0; i < lstTags.Count; i++)
                            {
                                lstBoxFileTags.Items.Add(new System.Web.UI.WebControls.ListItem(lstTags[i], lstTags[i]));
                            }
                        }

                        applyCSStoFileTag_ListItems();
                    }
                    else
                    {
                        lstBoxFileTags.DataSource = null;
                        lstBoxFileTags.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void applyCSStoFileTag_ListItems()
        {
            foreach (System.Web.UI.WebControls.ListItem eachItem in lstBoxFileTags.Items)
            {
                if (eachItem.Selected)
                    eachItem.Attributes.Add("class", "label label-info-selected");
                else
                    eachItem.Attributes.Add("class", "label label-info");
                eachItem.Attributes.Add("onclick", "fcheckcontract(this)");
            }
        }
        public void BindNoticeRelatedDocuments_All(int noticeInstanceID)
        {
            try
            {
                int DocTypeID = -1;
                string TypeToSearch = string.Empty;
                string DisplayError = string.Empty;
                List<Sp_Litigation_CaseDocument_Result> lstNoticeDocs = new List<Sp_Litigation_CaseDocument_Result>();
                List<Sp_Litigation_CaseDocument_Result> CaseDataFiltered = new List<Sp_Litigation_CaseDocument_Result>();
                List<Sp_Litigation_CaseDocument_Result> CaseDataFinal = new List<Sp_Litigation_CaseDocument_Result>();
                lstNoticeDocs = NoticeManagement.GetNoticeDocumentMapping(noticeInstanceID, "", AuthenticationHelper.CustomerID);
                if (lstNoticeDocs.Count > 0)
                {
                    BindFileTags();
                    lstNoticeDocs = (from g in lstNoticeDocs
                                     group g by new
                                     {
                                         g.ID,
                                         g.DocType,
                                         g.FileName,
                                         g.Version,
                                         g.CreatedByText,
                                         g.CreatedOn,
                                         g.TypeName,
                                         g.FinancialYear
                                     } into GCS
                                     select new Sp_Litigation_CaseDocument_Result()
                                     {
                                         ID = GCS.Key.ID,
                                         DocType = GCS.Key.DocType,
                                         FileName = GCS.Key.FileName,
                                         Version = GCS.Key.Version,
                                         CreatedByText = GCS.Key.CreatedByText,
                                         CreatedOn = GCS.Key.CreatedOn,
                                         TypeName = GCS.Key.TypeName,
                                         FinancialYear = GCS.Key.FinancialYear

                                     }).ToList();

                    if (!string.IsNullOrEmpty(txtdocsearch.Text))
                    {
                        lstNoticeDocs = lstNoticeDocs.Where(row => row.FileName.ToLower().Contains(txtdocsearch.Text.ToLower())).ToList();
                    }

                    grdNoticeDocuments.DataSource = lstNoticeDocs;
                    grdNoticeDocuments.DataBind();
                }
                else
                {
                    grdNoticeDocuments.DataSource = null;
                    grdNoticeDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvNoticePopUp.IsValid = false;
                //cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                //VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        protected void lnkLinkNoticeFilter_Click(object sender, EventArgs e)
        {
            long noticeInstanceID = 0;
            if (ViewState["noticeInstanceID"] != null)
            {
                noticeInstanceID = Convert.ToInt64(ViewState["noticeInstanceID"]);
                BindNoticeListToLink(noticeInstanceID);
            }
        }
        //Added by renuka
        #region[Export to excel]
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditLogDetail");
                    DataTable ExcelData = null;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        if (ViewState["noticeInstanceID"] != null)
                        {

                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                            var Data = (from row in entities.SP_LitigationCaseNoticeAuditLog(customerID, noticeInstanceID, "N")
                                        where row.IsVisibleToUser == true
                                        select row).OrderByDescending(entry => entry.CreatedOn).ToList();


                            var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerID));
                            DataTable table = Data.ToDataTable();

                            DataView view = new System.Data.DataView(table);
                            List<SP_LitigationCaseNoticeAuditLog_Result> cb = new List<SP_LitigationCaseNoticeAuditLog_Result>();

                            ExcelData = view.ToTable("Selected", false, "Remark", "CreatedByUser", "CreatedOn");

                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);

                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;
                            }

                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Value = "Customer Name:";

                            exWorkSheet.Cells["B1"].Merge = true;
                            exWorkSheet.Cells["B1"].Value = cname;

                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Value = "Report Name:";

                            //exWorkSheet.Cells["B2:C2"].Merge = true;
                            exWorkSheet.Cells["B2"].Value = "audit Log Report";

                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                            //exWorkSheet.Cells["B3:C3"].Merge = true;
                            exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                            exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A4"].Value = "Sr.No.";
                            exWorkSheet.Cells["A4"].AutoFitColumns(20);

                            exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B4"].Value = "Remark";
                            exWorkSheet.Cells["B4"].AutoFitColumns(40);

                            exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C4"].Value = "Created By";
                            exWorkSheet.Cells["C4"].AutoFitColumns(40);

                            exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D4"].Value = "Created On";
                            exWorkSheet.Cells["D4"].AutoFitColumns(25);


                        }


                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 4 + ExcelData.Rows.Count, 4])
                    {

                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 2, 4 + ExcelData.Rows.Count, 4])
                    {
                        col[1, 2, 4 + ExcelData.Rows.Count, 4].Style.Numberformat.Format = "dd/MM/yyyy hh:mm:ss";
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AuditLogDetails.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        #endregion



        protected void rptPaymentDocmentView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AllinOneDocumentList = CaseManagement.GetCasePaymentDocumentByID(Convert.ToInt32(commandArgs[2]));

                    if (AllinOneDocumentList != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            if (AllinOneDocumentList.FilePath != null)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                    request.Key = AllinOneDocumentList.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                }
                                string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                DocumentPath = filePath1;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/LitigationDocuments";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                //string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                //Directory.CreateDirectory(Server.MapPath(DateFolder));
                                //if (!Directory.Exists(DateFolder))
                                //{
                                //    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                //}
                                //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                //string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                ////string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                string FileName = extension;
                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //if (AllinOneDocumentList.EnType == "M")
                                //{
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                //}
                                //else
                                //{
                                //bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                //}
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewerNew('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewNew();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptPaymentDocmentView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentView = (LinkButton)e.Item.FindControl("lblDocumentView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentView);
            }
        }
        //End

        protected bool ISDocumentVisible(int caseid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool result = false;
                    var details = (from row in entities.tbl_LitigationFileData
                                   where row.ID == caseid
                                   select row).FirstOrDefault();
                    if (details != null)
                    {
                        if (AuthenticationHelper.Role == "CADMN")
                        {
                            result = false;
                        }
                        else
                        {
                            result = true;
                        }

                    }


                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSNoticePopup.CssClass = "alert alert-danger";
            }
            return false;
        }
    }
}