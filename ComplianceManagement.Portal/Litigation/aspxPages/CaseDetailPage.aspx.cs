﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;
using System.Globalization;
using System.Diagnostics;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using Amazon.S3.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class CaseDetailPage : System.Web.UI.Page
    {
        public static string DocumentPath = "";
        public long CustomerID = AuthenticationHelper.CustomerID;
        public static List<int> LawyerTypeList = new List<int>();
        public bool CheckCaseClose;
        public string IsNoticeDate;
        protected string IsAdvocateBillShow;
        public static string RPAResult;
        public static string case_number;
        public static int case_type;
        public static string case_year;
        public static string court_type;
        public bool RPACustomerEnable;
        public static string BenchID;
        public static string StateID;
        public bool NewColumnsLitigation;
        public bool CompulsoaryPaymentFields;
        public bool ShowAdvocateBillPayment;
        public bool CaseLabel;
        public bool RiskType;

        protected void Page_Load(object sender, EventArgs e)
        {
           
            try
            {
                CaseLabel = CaseManagement.CheckForClient(Convert.ToInt32(CustomerID), "CaseNoticeLabel");
                RiskType = CaseManagement.CheckForClient(Convert.ToInt32(CustomerID), "RiskType");
                Page.Header.DataBind();
                IsNoticeDate = Convert.ToString(ConfigurationManager.AppSettings["IsNoticeDateCustID"]);
                //IsAdvocateBillShow = Convert.ToString(ConfigurationManager.AppSettings["IsAdvocateBill"]);
                RPACustomerEnable = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "RPA");
                NewColumnsLitigation = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "LitigationNewColumn");
                CompulsoaryPaymentFields = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "LitigationPaymentCompulsoaryfield");
                ShowAdvocateBillPayment = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowAdvocateBill");
             
                if (!IsPostBack)
                {   
                    BindAct();
                    BindParty();
                    BindCustomerBranches();
                    BindDepartment();
                    BindUsers();
                    BindLawyer();
                    BindCaseCategoryType();
                    BindCourt();
                    BindCaseResult();
                    BindArroverOne();
                    BindState();
                    BindJurisdiction();
                    BindFY();
                    BindFinancialYear();
                    BindAdvLawyer();
                    //clearAdvBillControls();
                  if (IsNoticeDate == Convert.ToString(AuthenticationHelper.CustomerID) )
                        {
                        divNoticeDate.Visible = true;
                    }
                    else
                    {
                        divNoticeDate.Visible = false;
                    }
                    if (RiskType == true)
                    {
                        divRisk.Visible = true;
                    }
                    else
                    {
                        divRisk.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var caseInstanceID = Request.QueryString["AccessID"];
                        if (caseInstanceID != "")
                        {
                            ViewState["CaseInstanceID"] = caseInstanceID;

                            if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                            {
                                ViewState["FlagHistory"] = "1";
                            }
                            else
                            {
                                ViewState["FlagHistory"] = null;
                            }

                            if (Convert.ToInt32(caseInstanceID) == 0)
                            {
                                liCaseHearing.Visible = false;
                                liCaseTask.Visible = false;
                                liCaseOrder.Visible = false;
                                liCaseStatus.Visible = false;
                                if (RPACustomerEnable)
                                {
                                    liOthers.Visible = true;
                                }
                                else
                                {
                                    liOthers.Visible = false;
                                }
                                liCaseRating.Visible = false;
                                liAuditLog.Visible = false;
                                liDocument.Visible = false;
                                btnAddCase_Click(sender, e);  //Add Detail 
                                //if (IsAdvocateBillShow == Convert.ToString(AuthenticationHelper.CustomerID))
                                if(ShowAdvocateBillPayment == true)
                                {
                                    liCaseAdvocateBill.Visible = false;
                                }
                               else
                                {
                                    liCaseAdvocateBill.Visible = false;
                                }

                                if (!string.IsNullOrEmpty(Request.QueryString["RPAResult"]) && Convert.ToString(Request.QueryString["RPAResult"])!="[]")
                                {
                                    var keys = Request.QueryString["RPAResult"];
                                    dynamic key = JsonConvert.DeserializeObject(keys);
                                    txtCaseYear.Text = key[0]["CaseYear"].Value;
                                    case_year = txtCaseYear.Text.Trim();
                                    tbxRefNo.Text= key[0]["CaseRefNo"].Value;
                                    tbxSection.Text = key[0]["Section"].Value;
                                    ddlCourt.SelectedValue = key[0]["CourtID"].Value;
                                    txtCaseDate.Text = key[0]["OpenDate"].Value.Trim();
                                    tbxInternalCaseNo.Text = key[0]["InternalCaseNo"].Value;
                                    if (txtCaseDate.Text != "")
                                    {
                                        try
                                        {
                                            DateTime caseyear = DateTime.ParseExact(txtCaseDate.Text, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                            string FinancialYear = "";
                                            if (caseyear.Month > 3)
                                            {
                                                FinancialYear = Convert.ToString(caseyear.Year) + "-" + Convert.ToString(caseyear.Year + 1);
                                                string FYID = Convert.ToString(CaseManagement.ValidataFY(FinancialYear.ToString().Trim()));
                                                if (FYID != "0" && FYID != "-1")
                                                {
                                                    DropDownListChosen1.SelectedValue = FYID;
                                                }
                                            }
                                            if (caseyear.Month <= 3)
                                            {
                                                FinancialYear = Convert.ToString(caseyear.Year - 1) + "-" + Convert.ToString(caseyear.Year);
                                                string FYId = Convert.ToString(CaseManagement.ValidataFY(FinancialYear.ToString().Trim()));
                                                if (FYId != "0" && FYId != "-1")
                                                {
                                                    DropDownListChosen1.SelectedValue = FYId;
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            throw ex;
                                        }
                                    }
                                    tbxJudge.Text = key[0]["Judge"].Value;
                                    tbxTitle.Text = key[0]["casetitle"].Value;
                                    tbxDescription.Text = key[0]["Description"].Value;


                                    string CaseType = Convert.ToString(key[0]["case_type"].Value);
                                    int CaseTypeID = Convert.ToInt32(key[0]["case_typeID"].Value);
                                 
                                    if (!String.IsNullOrEmpty(CaseType.Trim()))
                                    {
                                        tbl_CaseType ObjCaseTypeDetail = LitigationCourtAndCaseType.GetRPALegalCaseTypeDetailByID(CaseTypeID, CustomerID);

                                        if (ObjCaseTypeDetail != null)
                                        {
                                            ddlCaseCategory.SelectedValue = Convert.ToString(ObjCaseTypeDetail.RPACaseTypeID);
                                        }
                                        else
                                        {
                                            tbl_CaseType objCaseType = new tbl_CaseType();
                                            objCaseType.CaseType = CaseType.Trim();
                                            objCaseType.RPACaseTypeID = CaseTypeID;
                                            objCaseType.IsDeleted = false;
                                            objCaseType.CreatedOn = DateTime.UtcNow.Date;
                                            objCaseType.CreatedBy = AuthenticationHelper.UserID;
                                            objCaseType.CustomerID = (int)AuthenticationHelper.CustomerID;
                                            LitigationCourtAndCaseType.CreateLegalCaseTypeDetails(objCaseType);
                                            var Ltikey = "LitilstCaseCaseType-" + Convert.ToString((int)AuthenticationHelper.CustomerID);
                                            if (CacheHelper.Exists(Ltikey))
                                            {
                                                CacheHelper.Remove(Ltikey);
                                            }

                                            tbl_CaseType ObjCaseTyp = LitigationCourtAndCaseType.GetRPALegalCaseTypeDetailByID(CaseTypeID, CustomerID);
                                            if (ObjCaseTyp != null)
                                            {
                                                BindCaseCategoryType();
                                                ddlCaseCategory.SelectedValue = Convert.ToString(ObjCaseTyp.RPACaseTypeID);
                                            }
                                        }
                                        
                                    }
                                    BenchID= Convert.ToString(key[0]["BenchID"].Value);
                                    StateID= Convert.ToString(key[0]["State"].Value);
                                    txtBenchID.Text = BenchID;
                                    txtStateID.Text = StateID;

                                    string State = Convert.ToString(key[0]["State"].Value);
                                    int StateIDs = 0;

                                    if (!String.IsNullOrEmpty(State.Trim()))
                                    {
                                        StateIDs = GetStateIDByName(StateID.Trim());

                                        if (StateIDs != 0 && StateIDs != -1)
                                        {
                                            ddlState.SelectedValue= Convert.ToString(StateIDs);
                                        }
                                    }

                                    string ActID = Convert.ToString(key[0]["Act"].Value);
                                    string ActIDs = "";

                                    if (!String.IsNullOrEmpty(ActID.Trim()))
                                    {
                                        ActIDs = Convert.ToString(GetActIDByName(ActID.Trim())); ;

                                        if (ActIDs != "0" && ActIDs != "-1")
                                        {
                                            ddlAct.SelectedValue = ActIDs;
                                        }
                                    }

                                }
                              //bindAllLawfirmUser();
                                showHideButtons(false);
                                BindCaseStage();
                            }
                            else
                            {
                                BindRefNo();
                                BindCaseStage();
                                liCaseHearing.Visible = true;
                                liCaseTask.Visible = true;
                                liCaseOrder.Visible = true;
                                liCaseStatus.Visible = true;
                                //if (IsAdvocateBillShow == Convert.ToString(AuthenticationHelper.CustomerID))
                                if (ShowAdvocateBillPayment == true)
                                {
                                    liCaseAdvocateBill.Visible = true;
                                }
                                else
                                {
                                    liCaseAdvocateBill.Visible = false;
                                }

                                if (RPACustomerEnable)
                                {
                                    liOthers.Visible = true;
                                }
                                else
                                {
                                    liOthers.Visible = false;
                                }

                                btnEditCase_Click(sender, e); //Edit Detail
                                showHideButtons(true);
                                BindAdvocateBill(Convert.ToInt32(caseInstanceID), AuthenticationHelper.CustomerID);
                                List<int> ListOfUser = CaseManagement.UserCanCheckRating(Convert.ToInt32(caseInstanceID), 1);
                                if (ListOfUser.Contains(AuthenticationHelper.UserID))
                                {
                                    liCaseRating.Visible = false;
                                }
                            }

                            if (!string.IsNullOrEmpty(Request.QueryString["Flagtab"]))
                            {
                                string FlagActiveTab = Convert.ToString(Request.QueryString["Flagtab"]);

                                if (FlagActiveTab.Equals("H"))
                                {
                                    TabHearing_Click(sender, e);
                                    //OpenHearing();
                                }
                                else
                                {
                                    TabCase_Click(sender, e);
                                }
                            }
                            else
                            {
                                TabCase_Click(sender, e);
                            }
                        }
                    }
                    //grdAdvocateBillDocuments.DataSource = null;
                    //grdAdvocateBillDocuments.DataBind();
                    ViewState["TaskMode"] = "Add";
                    ViewState["CustomefieldCount"] = null;
                    applyCSStoFileTag_ListItems();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                //Show Hide Grid Control - Enable/Disable Form Controls
                if (ViewState["caseStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                    {
                        showHideButtons(false);
                        enableDisableCasePopUpControls(false);
                    }
                    else
                    {
                        showHideButtons(true);
                        enableDisableCasePopUpControls(true);
                    }
                }
                else
                {
                    enableDisableCasePopUpControls(true);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                {
                    ViewState["FlagHistory"] = "1";

                    enableDisableCasePopUpControls(false);
                    showHideButtons(false);
                }
                else
                {
                    ViewState["FlagHistory"] = null;
                    //showHideSaveButtons(true);
                }

                //ddlLawyerAdvocate.SelectedValue = "-1";
                //     clearAdvBillControls();
                //grdAdvocateBillDocuments.DataSource = null;
                //grdAdvocateBillDocuments.DataBind();

                ViewState["ListofFile"] = null;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "myPostBackScript12", "rebindAdvocateHearingRefNo();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "myPostBackScript", "rebindLawyerUser();", true);

            }
            catch (Exception ex)
            {

            }
        }
    
        public static int GetStateIDByName(string State)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var StateID = (from row in entities.States
                               where row.Name.ToUpper().Trim() == State.ToUpper().Trim()
                               && row.IsDeleted == false
                               select row.ID).FirstOrDefault();

                return Convert.ToInt32(StateID);
            }
        }
        public static int GetActIDByName(string Actname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actid = (from row in entities.Act_Litigation
                             where Actname.ToUpper().Trim().Contains(row.Name.ToUpper().Trim())
                             //where row.Name.ToUpper().Trim().Contains(Actname.ToUpper().Trim())
                             && row.IsDeleted == false
                             select row.ID).FirstOrDefault();

                return Convert.ToInt32(actid);
            }
        }
        public string ShowFinancialYear(string FYear)
        {
            try
            {
                if (!string.IsNullOrEmpty(FYear))
                {
                    return FYear.TrimEnd(',');
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        private void bindAllLawfirmUser()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);
            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in lstAllUsers
                             where row.IsDeleted == false
                             && row.IsActive == true
                             select row).ToList();

                if (query.Count > 0)
                {
                    query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID != null).ToList();
                }
                
                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                lstBoxLawyerUser.DataTextField = "Name";
                lstBoxLawyerUser.DataValueField = "ID";
                lstBoxLawyerUser.DataSource = lstUsers;
                lstBoxLawyerUser.DataBind();
                lstBoxLawyerUser.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                ListBoxLawyerHearing.DataTextField = "Name";
                ListBoxLawyerHearing.DataValueField = "ID";
                ListBoxLawyerHearing.DataSource = lstUsers;
                ListBoxLawyerHearing.DataBind();
            }
            // var lawyerUsers = LitigationUserManagement.GetRequiredUsersByLawFirm(lstAllUsers, 2, Convert.ToInt32(ddlLawFirm.SelectedValue));
            
            ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScript111", "rebindLawyerUser();", true);
            UpdatePanel6.Update();
            if (ddlLawFirm.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "ShowAddButtonLawfirm", "ShowLawFirmAddbutton();", true);
            }
        }
        private void BindFY()
        {
            try
            {
                ddlFY.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select FY", "0"));
            }
            catch (Exception ex)

            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //private void BindFinancialYear()
        //{
        //    try
        //    {
        //       DropDownListChosen1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select FY", "0"));


        //    }
        //    catch (Exception ex)

        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        public static object FillFnancialYear()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.FinancialYearDetails
                             where row.IsDeleted==false
                             select row).OrderByDescending(e => e.FinancialYear).ToList();

                int[] rejectStatus = { 43, 44, 45, 46 };
                if (CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "RPAFY"))
                {
                    var rejectList = query.Where(i => rejectStatus.Contains(i.Id));
                    query = query.Except(rejectList).ToList();
                }
                return query;
            }
        }

        public void BindFinancialYear()
        {

            DropDownListChosen1.DataValueField = "Id";
            DropDownListChosen1.DataTextField = "FinancialYear";
            DropDownListChosen1.DataSource = FillFnancialYear();
            DropDownListChosen1.DataBind();
            //DropDownListChosen1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Financial Year", "-1"));
        }

        private void BindState()
        {
            try
            {
                var key = "LitiallState-" + Convert.ToString(CustomerID);

                var getallState = (List<State>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null)
                {
                    getallState = LitigationManagement.getAllSatatevalue();
                    HttpContext.Current.Cache.Insert(key, getallState, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
                }

                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = getallState;
                ddlState.DataBind();

                ddlState.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select State", "0"));
            }
            catch (Exception ex)
            {

            }
        }
        private void BindJurisdiction()
        {
            try
            {
                var key = "LitiallJurisdiction-" + Convert.ToString(CustomerID);

                var getallJurisdiction = (object)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null)
                {
                    getallJurisdiction = LitigationManagement.getAllJurisdictionvalue();
                    HttpContext.Current.Cache.Insert(key, getallJurisdiction, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
                }

                ddlJurisdiction.DataTextField = "Name";
                ddlJurisdiction.DataValueField = "ID";

                ddlJurisdiction.DataSource = getallJurisdiction;
                ddlJurisdiction.DataBind();

                ddlJurisdiction.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Jurisdiction", "0"));
            }
            catch (Exception ex)
            {

            }
        }
        //private void BindLitigationDocType()
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //        var lstdocTypes = CaseManagement.GetLitigationDocTypes_All(customerID);

        //        ddlDocType.DataTextField = "Name";
        //        ddlDocType.DataValueField = "ID";
        //        ddlDocType.DataSource = lstdocTypes;
        //        ddlDocType.DataBind();
        //        ddlDocType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        public void OpenHearing()
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "active");
            liCaseOrder.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 3;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseResponses(Convert.ToInt32(ViewState["CaseInstanceID"]));
            }
        }


        #region Common
        protected void lnkActDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                {
                    var caseInstanceID = Request.QueryString["AccessID"];
                    if (caseInstanceID != "")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDocumentDialog(" + caseInstanceID + ",'C');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindOrderType()
        {
            try
            {
                var lstOrderType = LitigationManagement.GetAllType("O");

                ddlOrderType.DataTextField = "TypeName";
                ddlOrderType.DataValueField = "ID";

                ddlOrderType.DataSource = lstOrderType;
                ddlOrderType.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCaseResult()
        {
            try
            {
                var key = "LitiResultType-" + Convert.ToString(CustomerID);

                var lstResultType = (object)HttpContext.Current.Cache[key];
                int CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (HttpContext.Current.Cache[key] == null)
                {
                    if(CaseLabel==true)
                    {
                        lstResultType = LitigationManagement.GetAllType("Re"); 
                    }
                    else
                    {
                        var result = LitigationManagement.GetAllType("Re");
                        result = result.Where(entry => entry.customerID == null).ToList();
                        lstResultType = result;
                    }
                    HttpContext.Current.Cache.Insert(key, lstResultType, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
                }

                ddlCaseResult.DataTextField = "TypeName";
                ddlCaseResult.DataValueField = "ID";

                ddlCaseResult.DataSource = lstResultType;
                ddlCaseResult.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindArroverOne()
        {
            try
            {
                var lstApprover1 = LitigationManagement.GetAllAdvBillApprovers("Approver 1", Convert.ToInt32(AuthenticationHelper.CustomerID));

                var lstApprover2 = LitigationManagement.GetAllAdvBillApprovers("Approver 2", Convert.ToInt32(AuthenticationHelper.CustomerID));

                var lstApprover3 = LitigationManagement.GetAllAdvBillApprovers("Approver 3", Convert.ToInt32(AuthenticationHelper.CustomerID));

                ddlApprover1.DataTextField = "ApproverEmail";
                ddlApprover1.DataValueField = "ID";
                ddlApprover1.DataSource = lstApprover1;
                ddlApprover1.DataBind();
                ddlApprover1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Approver", "0"));

                ddlApprover2.DataTextField = "ApproverEmail";
                ddlApprover2.DataValueField = "ID";
                ddlApprover2.DataSource = lstApprover2;
                ddlApprover2.DataBind();
                ddlApprover2.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Approver", "0"));

                ddlApprover3.DataTextField = "ApproverEmail";
                ddlApprover3.DataValueField = "ID";
                ddlApprover3.DataSource = lstApprover3;
                ddlApprover3.DataBind();
                ddlApprover3.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Approver", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCaseStage()
        {
            try
            {
                var lstCaseStages = LitigationManagement.GetAllType("CS");
                lstCaseStages = lstCaseStages.Where(row => row.customerID == Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();
                ddlCaseStage.DataTextField = "TypeName";
                ddlCaseStage.DataValueField = "ID";

                ddlCaseStage.DataSource = lstCaseStages;
                ddlCaseStage.DataBind();

                ddlCaseStage.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Stage", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindAct()
        {
            var key = "LitiAct-" + Convert.ToString(CustomerID);

            //var actList = (object)HttpContext.Current.Cache[key];
            //if (HttpContext.Current.Cache[key] == null)
            //{
            //    actList = LitigationLaw.GetAllAct();
            //    HttpContext.Current.Cache.Insert(key, actList, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
            //}
            var actList = LitigationLaw.GetAllAct();

            ddlAct.DataTextField = "Name";
            ddlAct.DataValueField = "ID";

            ddlAct.DataSource = actList;
            ddlAct.DataBind();

            ddlAct.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
        }

        private void BindCaseCategoryType()
        {
            try
            {
                if (!RPACustomerEnable)
                {
                    var lstCaseCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);
                    ddlCaseCategory.DataTextField = "CaseType";
                    ddlCaseCategory.DataValueField = "ID";
                    ddlCaseCategory.DataSource = lstCaseCaseType;
                    ddlCaseCategory.DataBind();
                    ddlCaseCategory.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
                }
                else
                {
                    var lstCaseCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeDataForRPA(CustomerID);
                    ddlCaseCategory.DataTextField = "CaseType";
                    ddlCaseCategory.DataValueField = "RPACaseTypeID";
                    ddlCaseCategory.DataSource = lstCaseCaseType;
                    ddlCaseCategory.DataBind();
                    ddlCaseCategory.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int userId = -1;
                userId = AuthenticationHelper.UserID;
                int IsForBranch = UserManagement.ExistBranchAssignment(customerID);
                if (IsForBranch == 1)
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    // var branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchyManagementSatutoryNew(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }
                    tbxBranch.Text = "Select Entity/Location";
                    List<TreeNode> nodes = new List<TreeNode>();
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var BranchList = (from row in entities.LitigationEntitiesAssignments
                                          where row.UserID == userId
                                          select (int)row.BranchID).ToList();

                        //Comment by sachin on 8 JULY 2021
                        //List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                        //                                 join row1 in entities.tbl_LegalCaseInstance
                        //                                 on row.CaseInstanceID equals row1.ID
                        //                                 where row1.CustomerID == (int)customerID
                        //                                 && (row.UserID == userId || row1.OwnerID == userId || row1.CreatedBy == userId || BranchList.Contains(row1.CustomerBranchID))
                        //                                 select (int)row1.CustomerBranchID).ToList();

                        //List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                        //                                   join row1 in entities.tbl_LegalNoticeInstance
                        //                                   on row.NoticeInstanceID equals row1.ID
                        //                                   where row1.CustomerID == (int)customerID
                        //                                   && (row.UserID == userId || row1.OwnerID == userId || row1.CreatedBy == userId || BranchList.Contains(row1.CustomerBranchID))
                        //                                   select (int)row1.CustomerBranchID).ToList();

                        //List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                        //var LocationList = LegalCasbranchlist.Select(a => a).ToList();

                        //BindBranchesHierarchyNew(null, branch, nodes, LocationList);
                        BindBranchesHierarchyNew(null, branch, nodes, BranchList);
                    }
                   
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }

                    tvBranches.CollapseAll();
                }
                else
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    // var branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    List<NameValueHierarchy> branches;
                    //string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    //if (CacheHelper.Exists(key))
                    //{
                    //    CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    //}
                    //else
                    //{
                    //branches = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    branches = CustomerBranchManagement.GetAllAssignedEntitiesHierarchySatutory(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role);
                    //    CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    //}
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }
                    tbxBranch.Text = "Select Entity/Location";
                    List<TreeNode> nodes = new List<TreeNode>();

                    BindBranchesHierarchy(null, branch, nodes);
                   
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }

                    tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }

        private void BindBranchesHierarchyNew(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes, List<int> branchlst)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchyNew(node, item, nodes, branchlst);
                        if (branchlst.Contains(item.ID) == true)
                        {
                            nodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

            var key = "Litidepart-" + Convert.ToString(CustomerID);
            var obj = (List<Department>)HttpContext.Current.Cache[key];
            if (HttpContext.Current.Cache[key] == null)
            {
                obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);
                HttpContext.Current.Cache.Insert(key, obj, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
            }

            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";

            ddlDepartment.DataSource = obj;
            ddlDepartment.DataBind();

            ddlDepartment.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
        }

        public void BindCourt()
        {
            var key = "LitiCourt-" + Convert.ToString(CustomerID);
            var obj = (List<View_CourtMaster>)HttpContext.Current.Cache[key];
            if (HttpContext.Current.Cache[key] == null)
            {
                obj = LitigationCourtAndCaseType.BindAllCourtMasterData(CustomerID);
                HttpContext.Current.Cache.Insert(key, obj, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
            }

            ddlCourt.DataTextField = "CourtName";
            ddlCourt.DataValueField = "ID";

            ddlCourt.DataSource = obj;
            ddlCourt.DataBind();

            ddlCourt.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
        }


        public void BindRefNo()
        {
            if (ViewState["CaseInstanceID"] != null)
            {
                var obj = CaseManagement.GetAllRefNo(CustomerID, Convert.ToInt32(ViewState["CaseInstanceID"]));

                //Hearing Tab - Drop Down
                if (ddlTabHearingRef.Items.Count > 0)
                    ddlTabHearingRef.Items.Clear();

                ddlTabHearingRef.DataTextField = "HearingRefNo";
                ddlTabHearingRef.DataValueField = "ID";

                ddlTabHearingRef.DataSource = obj;
                ddlTabHearingRef.DataBind();

                ddlTabHearingRef.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", "-1"));
                ddlTabHearingRef.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                // Task Tab - Drop Down
                if (ddlHearingRefNo.Items.Count > 0)
                    ddlHearingRefNo.Items.Clear();

                ddlHearingRefNo.DataTextField = "HearingRefNo";
                ddlHearingRefNo.DataValueField = "ID";

                ddlHearingRefNo.DataSource = obj;
                ddlHearingRefNo.DataBind();

                ddlHearingRefNo.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", "-1"));
                ddlHearingRefNo.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                var newobj = CaseManagement.GetAllAdvocateRefNo(CustomerID, Convert.ToInt32(ViewState["CaseInstanceID"]));


                lstAdvHearingRefNo.DataTextField = "HearingRefNo";
                lstAdvHearingRefNo.DataValueField = "ID";

                lstAdvHearingRefNo.DataSource = newobj.Distinct();
                lstAdvHearingRefNo.DataBind();

                //lstAdvHearingRefNo.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", "-1"));
                //lstAdvHearingRefNo.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                //UpdatePanelAdvBill.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "myPostBackScript", "rebindAdvocateHearingRefNo();", true);
             

            }
        }

        public void BindLawyer()
        {

            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
            var key = "LitiLawyer-" + Convert.ToString(CustomerID);
            //var obj = (object)HttpContext.Current.Cache[key];
            //if (HttpContext.Current.Cache[key] == null)
            //{
              var obj = LawyerManagement.GetLawyerListForMapping(customerID);
                //HttpContext.Current.Cache.Insert(key, obj, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
            //}
            ddlLawFirm.DataTextField = "Name";
            ddlLawFirm.DataValueField = "ID";
            ddlLawFirm.DataSource = obj;
            ddlLawFirm.DataBind();
            ddlLawFirm.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

            DropDownListHearingLawFirm.DataTextField = "Name";
            DropDownListHearingLawFirm.DataValueField = "ID";
            DropDownListHearingLawFirm.DataSource = obj;
            DropDownListHearingLawFirm.DataBind();
        }

        public void BindParty()
        {
            var key = "LitiParty-" + Convert.ToString(CustomerID);

            //var obj = (List<tbl_PartyDetail>)HttpContext.Current.Cache[key];
            //if (HttpContext.Current.Cache[key] == null || obj.Count() == 0)
            //{
               var obj = LitigationLaw.GetLCPartyDetails(CustomerID);
            //    HttpContext.Current.Cache.Insert(key, obj, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
            //}

            //Drop-Down at Modal Pop-up
            ddlParty.DataTextField = "Name";
            ddlParty.DataValueField = "ID";

            ddlParty.DataSource = obj;
            ddlParty.DataBind();

            ddlParty.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
        }
  
        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //var lstAllUsers = LitigationUserManagement.GetLitigationUsers(customerID,);

                var key = "Litiusers-" + Convert.ToString(customerID);

                var lstAllUsers = (List<User>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null || lstAllUsers.Count() == 0)
                {
                    lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);
                    HttpContext.Current.Cache.Insert(key, lstAllUsers, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
                }

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();

                var bothUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 0);

                ddlOwner.DataValueField = "ID";
                ddlOwner.DataTextField = "Name";
                ddlOwner.DataSource = bothUsers;
                ddlOwner.DataBind();

                ddlCPDepartment.DataValueField = "ID";
                ddlCPDepartment.DataTextField = "Name";
                ddlCPDepartment.DataSource = bothUsers;
                ddlCPDepartment.DataBind();

                ddlCPDepartment.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Contact Person of Department", "0"));

                var internalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 1);

                lstBoxPerformer.DataTextField = "Name";
                lstBoxPerformer.DataValueField = "ID";

                lstBoxPerformer.DataSource = internalUsers;
                lstBoxPerformer.DataBind();

                ddlCPDepartment.DataValueField = "ID";
                ddlCPDepartment.DataTextField = "Name";
                ddlCPDepartment.DataSource = internalUsers;
                ddlCPDepartment.DataBind();

                ddlCPDepartment.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Contact Personof Department", "0"));

                //Task Assignment - For User Filter
                ddlTaskLawyerListInternal.DataTextField = "Name";
                ddlTaskLawyerListInternal.DataValueField = "ID";

                ddlTaskLawyerListInternal.DataSource = internalUsers;
                ddlTaskLawyerListInternal.DataBind();

                ddlTaskLawyerListInternal.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", "0"));

                ddlReviewer.DataValueField = "ID";
                ddlReviewer.DataTextField = "Name";
                ddlReviewer.DataSource = internalUsers;
                ddlReviewer.DataBind();

               

              
                var lawyerUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);

                //Opposition Lawyer list 
                lstBoxOppositionLawyer.DataTextField = "Name";
                lstBoxOppositionLawyer.DataValueField = "ID";

                lstBoxOppositionLawyer.DataSource = lawyerUsers;
                lstBoxOppositionLawyer.DataBind();
                lstBoxOppositionLawyer.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                var lawyerAndExternalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);

                ddlTaskUserExternal.DataValueField = "ID";
                ddlTaskUserExternal.DataTextField = "Name";
                ddlTaskUserExternal.DataSource = lawyerAndExternalUsers;
                ddlTaskUserExternal.DataBind();

                ddlTaskUserExternal.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                //lstBoxLawyerUser.DataTextField = "Name";
                //lstBoxLawyerUser.DataValueField = "ID";

                //lstBoxLawyerUser.DataSource = externalUsers;
                //lstBoxLawyerUser.DataBind();

                lstAllUsers.Clear();
                lstAllUsers = null;

                internalUsers.Clear();
                internalUsers = null;

                lawyerUsers.Clear();
                lawyerUsers = null;

                lawyerAndExternalUsers.Clear();
                lawyerAndExternalUsers = null;

                //Assignuser.Clear();
                //Assignuser = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindCaseAuditLogs()
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    var lstAuditLogs = LitigationManagement.GetCaseNoticeAuditLog(customerID, caseInstanceID, "C");

                    gvCaseAuditLog.DataSource = lstAuditLogs;
                    gvCaseAuditLog.DataBind();
                }
                else
                {
                    gvCaseAuditLog.DataSource = null;
                    gvCaseAuditLog.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindPaymentType(DropDownList ddl)
        {
            try
            {
                var key = "LitiPaymentType-" + Convert.ToString(CustomerID);

                var PaymentMasterList = (List<tbl_PaymentType>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null || PaymentMasterList.Count == 0)
                {
                    PaymentMasterList = LitigationPaymentType.GetAllPaymentMasterList();
                    HttpContext.Current.Cache.Insert(key, PaymentMasterList, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
                }

                ddl.DataValueField = "ID";
                ddl.DataTextField = "TypeName";

                ddl.DataSource = PaymentMasterList;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        protected void TabCase_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "active");
            liDocument.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseAdvocateBill.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liOthers.Attributes.Add("class", ""); 
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;

            if (ViewState["Mode"] != null)
            {
                if (Convert.ToInt32(ViewState["Mode"]) == 0)
                {
                    enableDisableCaseSummaryTabControls(true);

                    btnSave.Visible = true;
                    btnSave.Text = "Save";
                    btnClearCaseDetail.Visible = true;
                    //btnEditCaseDetail.Visible = false;
                    //lnkActDetails.Visible = false;
                    //lnkSendMailWithDoc.Visible = false;
                }
                else if (Convert.ToInt32(ViewState["Mode"]) == 1)
                {
                    enableDisableCaseSummaryTabControls(false);

                    btnSave.Visible = true;
                    btnSave.Text = "Update";
                    btnClearCaseDetail.Visible = false;
                    //btnEditCaseDetail.Visible = true;
                    //lnkActDetails.Visible = true;
                    //lnkSendMailWithDoc.Visible = true;
                }
            }
        }

        protected void TabDocument_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "active");
            liCaseTask.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseAdvocateBill.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liOthers.Attributes.Add("class", ""); 
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 2;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseRelatedDocuments_All();
                BindFileTags();
            }
        }

        protected void TabTask_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "active");
            liCaseHearing.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseAdvocateBill.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liOthers.Attributes.Add("class", "");
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 3;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);
            }
        }

        protected void TabHearing_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "active");
            liCaseOrder.Attributes.Add("class", "");
            liCaseAdvocateBill.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liOthers.Attributes.Add("class", ""); 
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 4;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseResponses(Convert.ToInt32(ViewState["CaseInstanceID"]));
            }
        }
        //Added by renuka
        protected void TabAdvocateBill_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseAdvocateBill.Attributes.Add("class", "active");
            liCaseStatus.Attributes.Add("class", "");
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            //BindOrderType();

            MainView.ActiveViewIndex = 6;
            BindRefNo();
            BindAdvLawyer();

            if (ViewState["CaseInstanceID"] != null)
            {
                BindAdvocateBill(Convert.ToInt32(ViewState["CaseInstanceID"]), AuthenticationHelper.CustomerID);
            }
        }

        //End
        protected void TabOrder_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "active");
            liCaseAdvocateBill.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liOthers.Attributes.Add("class", "");
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            BindOrderType();
         
            MainView.ActiveViewIndex = 5;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseOrders(Convert.ToInt32(ViewState["CaseInstanceID"]));
            }
        }

        protected void TabStatus_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseAdvocateBill.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "active");
            liOthers.Attributes.Add("class", "");
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 7;
            if (ViewState["CaseInstanceID"] != null)
            {
                int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                if (CaseManagement.IsExistCaseToCaseTrasfer(caseInstanceID))
                {
                    btnCaseTransfer.Visible = false;
                }

                BindCasePayments(caseInstanceID);
            }
            ViewState["PaymentMode"] = "Add";
        }

        protected void TabRating_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseAdvocateBill.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liOthers.Attributes.Add("class", "");
            liCaseRating.Attributes.Add("class", "active");
            lnkAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 8;

            if (!string.IsNullOrEmpty(ViewState["CaseInstanceID"].ToString()))
            {
                int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                bool CheckUserForRating = CaseManagement.CheckUserIsInternal(AuthenticationHelper.UserID);

                if (CheckUserForRating == false)
                {
                    //if (caseRecord.OwnerID == AuthenticationHelper.UserID)
                    //{
                    //    CheckUserForRating = true;
                    //}
                }

                if (CheckUserForRating)
                {
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var documentData = (from row in entities.sp_LiDisplayLawyerRatingCase(caseInstanceID)
                                                select row).ToList();

                            if (documentData != null)
                            {
                                grdLawyerRating.DataSource = null;
                                grdLawyerRating.DataBind();
                                ddlLayerType.DataValueField = "LawyerID";
                                ddlLayerType.DataTextField = "LawyerName";
                                ddlLayerType.DataSource = documentData;
                                ddlLayerType.DataBind();
                            }
                            //  BindgridforLayersrating(ddlLayerType.DataValueField);
                        }
                    }
                }
            }
        }

        private void BindgridforLayersrating(string dataValueField)
        {
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    string layerID = ddlLayerType.SelectedValue;
            //    int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
            //    if (layerID != "")
            //    {
            //        var documentData = (from row in entities.sp_LiDisplayCriteriaRatingCase(caseInstanceID, AuthenticationHelper.UserID,Convert.ToInt16 (layerID))
            //                            select row).ToList();

            //        if (documentData != null)
            //        {
            //            grdLawyerRating.DataSource = documentData;
            //            grdLawyerRating.DataBind();
            //        }
            //    }
            //}
        }

        //private void BindgridforLayersrating(List<sp_LiDisplayLawyerRatingCase_Result> documentData)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        string layerID = ddlLayerType.SelectedValue;
        //        int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
        //        if (layerID != null)
        //        {
        //            var documentData = (from row in entities.sp_LiDisplayLawyerRatingCase(caseInstanceID, AuthenticationHelper.UserID)
        //                                select row).ToList();

        //            if (documentData != null)
        //            {
        //                grdLawyerRating.DataSource = documentData;
        //                grdLawyerRating.DataBind();
        //            }
        //        }
        //    }
        //}

        protected void TabAuditLog_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "active");
            liCaseAdvocateBill.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 9;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseAuditLogs();
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                //ScriptManager.RegisterStartupScript(this.upCasePopup, this.upCasePopup.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }

        #endregion

        #region Master

        protected void btnfilldept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lblCategory_Click(object sender, EventArgs e)
        {
            BindCaseCategoryType();
        }

        protected void lblAct_Click(object sender, EventArgs e)
        {
            BindAct();
        }

        protected void lblParty_Click(object sender, EventArgs e)
        {
            BindParty();
        }

        protected void lnkBtnParty_Click(object sender, EventArgs e)
        {
            BindParty();
        }

        protected void lnkBtnAct_Click(object sender, EventArgs e)
        {
            BindAct();
        }

        protected void lnkBtnCategory_Click(object sender, EventArgs e)
        {
            BindCaseCategoryType();
        }

        protected void lnkBtnDept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lblCourt_Click(object sender, EventArgs e)
        {
            BindCourt();
        }

        protected void lnkAddRefNo_Click(object sender, EventArgs e)
        {
            BindRefNo();
        }

        protected void imgAddNewRefNo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);

                if (ViewState["CaseInstanceID"] != null)
                {
                    int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    if (caseInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptOpenRefPopUp", "OpenRefNoPopup(" + caseInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        protected void btnSaveRefNo_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    if (caseInstanceID != 0)
                    {
                        bool validateData = false;

                        if (tbxTaskHearingDate.Text != "")
                        {
                            validateData = true;
                        }
                        else
                        {
                            cvCasePopUpTask.IsValid = false;
                            cvCasePopUpTask.ErrorMessage = "Please provide hearing Date.";
                            ValidationSummary5.CssClass = "alert alert-danger";
                        }

                        if (validateData)
                        {
                            tbl_CaseHearingRef objNewRefNo = new tbl_CaseHearingRef()
                            {
                                CaseNoticeInstanceID = caseInstanceID,
                                HearingDate = DateTimeExtensions.GetDate(tbxTaskHearingDate.Text),
                                CustomerID = (int)CustomerID,
                                IsDeleted = false,

                                CreatedBy = AuthenticationHelper.UserID,
                            };

                            var result = CaseManagement.GetExistsRefNo(objNewRefNo);

                            if (result != -1)
                            {
                                objNewRefNo.HearingRefNo = "Hearing-" + tbxTaskHearingDate.Text;

                                var newID = CaseManagement.CreateNewRefNo(objNewRefNo);
                                if (newID > 0)
                                {
                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_CaseHearingRef", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Hearing Reference Created", true);

                                    BindRefNo();

                                    if (ddlHearingRefNo.Items.Count > 0)
                                    {
                                        if (ddlHearingRefNo.Items.FindByValue(newID.ToString()) != null)
                                        {
                                            ddlHearingRefNo.SelectedValue = newID.ToString();
                                            tbxTaskHearingDate.Text = "";
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTaskHearingDate", "ddlRefNoChange();", true);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvCasePopUpTask.IsValid = false;
                                cvCasePopUpTask.ErrorMessage = "Hearing Date already exists, Please select Hearing and continue with the task creation.";
                                ValidationSummary5.CssClass = "alert alert-danger";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        protected void btnTabSaveRefNo_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    long caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    DropDownListHearingLawFirm.ClearSelection();
                    if (ddlLawFirm.SelectedValue != "0")
                    {
                        DropDownListHearingLawFirm.SelectedValue = ddlLawFirm.SelectedValue;
                    }
                    DropDownListHearingLawFirm_SelectedIndexChanged(null, null);

                    int caseInstanceID1 = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    var lstCaseAssignment = CaseManagement.GetCaseAssignment(caseInstanceID1);

                    if (lstCaseAssignment.Count > 0)
                    {
                        ListBoxLawyerHearing.ClearSelection();

                        foreach (var eachAssignmentRecord in lstCaseAssignment)
                        {
                            if (ListBoxLawyerHearing.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                ListBoxLawyerHearing.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                        }
                    }

                    if (caseInstanceID != 0)
                    {
                        bool validateData = false;

                        if (tbxTabHearingDate.Text != "")
                        {
                            validateData = true;
                        }
                        else
                        {
                            cvCasePopUpResponse.IsValid = false;
                            cvCasePopUpResponse.ErrorMessage = "Please provide hearing Date.";
                            ValidationSummary1.CssClass = "alert alert-danger";
                        }

                        if (validateData)
                        {
                            tbl_CaseHearingRef objNewRefNo = new tbl_CaseHearingRef()
                            {
                                CaseNoticeInstanceID = caseInstanceID,
                                HearingDate = DateTimeExtensions.GetDate(tbxTabHearingDate.Text),
                                CustomerID = (int)CustomerID,
                                IsDeleted = false,

                                CreatedBy = AuthenticationHelper.UserID,
                            };

                            var result = CaseManagement.GetExistsRefNo(objNewRefNo);

                            if (result != -1)
                            {
                                objNewRefNo.HearingRefNo = "Hearing-" + tbxTabHearingDate.Text;

                                var newID = CaseManagement.CreateNewRefNo(objNewRefNo);
                                if (newID > 0)
                                {
                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_CaseHearingRef", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Hearing Reference Created", true);
                                    BindRefNo();

                                    if (ddlTabHearingRef.Items.Count > 0)
                                    {
                                        if (ddlTabHearingRef.Items.FindByValue(newID.ToString()) != null)
                                        {
                                            ddlTabHearingRef.SelectedValue = newID.ToString();
                                            tbxTabHearingDate.Text = "";
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTabHearingDate", "ddlTabHearingRefNoChange();", true);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvCasePopUpResponse.IsValid = false;
                                cvCasePopUpResponse.ErrorMessage = "Hearing Date already exists, Please select Hearing and continue with the task creation.";
                                ValidationSummary1.CssClass = "alert alert-danger";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpResponse.IsValid = false;
                cvCasePopUpResponse.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary1.CssClass = "alert alert-danger";
            }
        }

        public void generateRefNo(DateTime providedDate)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    long caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    if (caseInstanceID != 0)
                    {
                        tbl_CaseHearingRef objNewRefNo = new tbl_CaseHearingRef()
                        {
                            CaseNoticeInstanceID = caseInstanceID,
                            HearingDate = providedDate,
                            CustomerID = (int)CustomerID,
                            IsDeleted = false,

                            CreatedBy = AuthenticationHelper.UserID,
                        };

                        var result = CaseManagement.GetExistsRefNo(objNewRefNo);

                        if (result != -1)
                        {
                            objNewRefNo.HearingRefNo = "Hearing-" + tbxTabHearingDate.Text;

                            var newID = CaseManagement.CreateNewRefNo(objNewRefNo);

                            if (newID > 0)
                            {
                                BindRefNo();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTabHearingDate", "ddlTabHearingRefNoChange();", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Case Detail

        protected void btnClearCaseControls_Click(object sender, EventArgs e)
        {
            try
            {
                clearCaseControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearCaseControls()
        {
            try
            {
                txtCaseDate.Text = "";
                txtNoticeDate.Text = "";
                DropDownListChosen1.SelectedValue = "";
                //TextBox1.Text = "";
                tbxRefNo.Text = "";
                txtCaseYear.Text= "";
                txtBenchID.Text="";
                txtStateID.Text="";
                tbxTitle.Text = "";
                ddlAct.ClearSelection();
                tbxSection.Text = "";
                ddlCaseCategory.ClearSelection();
                ddlParty.ClearSelection();
                ddlCourt.ClearSelection();

                tbxJudge.Text = "";

                tbxDescription.Text = "";
                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                ddlDepartment.ClearSelection();
                ddlOwner.ClearSelection();
                ddlCaseRisk.ClearSelection();

                tbxClaimedAmt.Text = "";
                tbxProbableAmt.Text = "";
                tbxInternalCaseNo.Text = "";

                rblPotentialImpact.ClearSelection();
                tbxMonetory.Text = "";
                tbxNonMonetory.Text = "";
                tbxNonMonetoryYears.Text = "";

                txtPreDeposit.Text = "";
                txtPostDeposit.Text = "";

                CaseFileUpload.Attributes.Clear();

                ddlLawFirm.ClearSelection();

                lstBoxPerformer.ClearSelection();
                lstBoxLawyerUser.ClearSelection();
                lstBoxOppositionLawyer.ClearSelection();

                ddlReviewer.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableCaseSummaryTabControls(bool flag)
        {
            try
            {
                rbCaseInOutType.Enabled = flag;
                txtCaseDate.Enabled = flag;
                txtNoticeDate.Enabled = flag;
                DropDownListChosen1.Enabled = flag;
               // TextBox1.Enabled = flag;
                tbxRefNo.Enabled = flag;
                txtCaseYear.Enabled = flag;
                tbxInternalCaseNo.Enabled = flag;
                tbxTitle.Enabled = flag;
                ddlAct.Enabled = flag;
                tbxSection.Enabled = flag;
                ddlParty.Enabled = flag;
                ddlCaseCategory.Enabled = flag;
                ddlCourt.Enabled = flag;
                ddlCPDepartment.Enabled = flag;

                tbxJudge.Enabled = flag;

                tbxDescription.Enabled = flag;
                tbxBranch.Enabled = flag;
                ddlDepartment.Enabled = flag;
                ddlOwner.Enabled = flag;
                ddlCaseRisk.Enabled = flag;
                tbxClaimedAmt.Enabled = flag;
                tbxProbableAmt.Enabled = flag;
                txttaxDemand.Enabled = flag;
                txtfavourable.Enabled = flag;
                txtUnfavourable.Enabled = flag;
                txtIntrest.Enabled = flag;
                txtPenalty.Enabled = flag;
                txtProvisonbook.Enabled = flag;
                tbxCaseBudget.Enabled = flag;
                txtamountpaid.Enabled = flag;
                rblPotentialImpact.Enabled = flag;
                tbxMonetory.Enabled = flag;
                tbxNonMonetory.Enabled = flag;
                tbxNonMonetoryYears.Enabled = flag;

                txtPreDeposit.Enabled = flag;
                txtPostDeposit.Enabled = flag;

                //CaseFileUpload.Enabled = flag;
                ddlLawFirm.Enabled = flag;

                lstBoxPerformer.Enabled = flag;
                lstBoxOppositionLawyer.Enabled = flag;
                ddlReviewer.Enabled = flag;
                lstBoxLawyerUser.Enabled = flag;
                ddlAct.Enabled = flag;
                ddlParty.Enabled = flag;

                btnSave.Enabled = flag;
                btnClearCaseDetail.Enabled = flag;

                if (flag)
                {
                    ddlLawFirm.Attributes.Remove("disabled");
                    ddlState.Attributes.Remove("disabled");
                    ddlJurisdiction.Attributes.Remove("disabled");
                    lstBoxPerformer.Attributes.Remove("disabled");
                    lstBoxLawyerUser.Attributes.Remove("disabled");
                    lstBoxOppositionLawyer.Attributes.Remove("disabled");
                    ddlReviewer.Attributes.Remove("disabled");
                    ddlAct.Attributes.Remove("disabled");
                    ddlParty.Attributes.Remove("disabled");
                    ddlFY.Attributes.Remove("disabled");
                    btnSave.Attributes.Remove("disabled");
                    btnClearCaseDetail.Attributes.Remove("disabled");
                }
                else
                {
                    ddlState.Attributes.Add("disabled", "disabled");
                    ddlJurisdiction.Attributes.Add("disabled", "disabled");
                    ddlLawFirm.Attributes.Add("disabled", "disabled");
                    ddlFY.Attributes.Add("disabled", "disabled");
                    lstBoxPerformer.Attributes.Add("disabled", "disabled");
                    lstBoxLawyerUser.Attributes.Add("disabled", "disabled");
                    ddlReviewer.Attributes.Add("disabled", "disabled");
                    lstBoxOppositionLawyer.Attributes.Add("disabled", "disabled");
                    ddlAct.Attributes.Add("disabled", "disabled");
                    ddlParty.Attributes.Add("disabled", "disabled");

                    btnSave.Attributes.Add("disabled", "disabled");
                    btnClearCaseDetail.Attributes.Add("disabled", "disabled");
                }

                if (flag)
                {
                    divGridUserAssignment.Visible = false;
                    pnlCaseAssignment.Visible = flag;
                }
                else
                {
                    divGridUserAssignment.Visible = true;
                    pnlCaseAssignment.Visible = flag;
                }

                ////Custom Parameter
                //if (grdCustomField != null)
                //{
                //    grdCustomField.Enabled = flag;
                //    //grdCustomeField.ShowFooter = flag;

                //    //if (grdCustomeField.Columns[3] != null)
                //    //    grdCustomeField.Columns[3].Visible = flag;

                //    if (grdCustomField.Columns[5] != null)
                //        grdCustomField.Columns[5].Visible = flag;
                //}

                //Custom Parameter
                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                {
                    ////emmamiusers.Visible = true;
                    if (grdCustomField_TaxLitigation != null)
                    {
                        grdCustomField_TaxLitigation.Enabled = flag;
                        HideShowGridColumns(grdCustomField_TaxLitigation, "Action", flag);
                    }
                }
                else
                {
                    if (grdCustomField != null)
                    {
                        grdCustomField.Enabled = flag;
                        HideShowGridColumns(grdCustomField, "Action", flag);
                    }
                }
                if (RiskType == true)
                {
                    ddlRisk.Enabled = flag;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showHideButtons(bool flag)
        {
            try
            {
                pnlLawRating.Enabled = flag;
                //divCaseHistory.Visible = flag;
                btnEditCaseDetail.Visible = flag;
                lnkActDetails.Visible = flag;
                lnkSendMailWithDoc.Visible = flag;
                lnkBtnEditUserAssignment.Visible = flag;
                btnAddPromotor.Visible = flag;
                //lnkApply.Visible = flag;
                lnkAddNewDoctype.Visible = flag;
                lnkLinkCase.Visible = flag;
                LnkAddAdvocateBill.Visible = flag;
               
                //if (IsAdvocateBillShow == Convert.ToString(AuthenticationHelper.CustomerID))
                //{
                //    lnkCaseAdvocateBill.Visible = flag;
                //}
                    
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableCasePopUpControls(bool flag)
        {
            try
            {
                //pnlCase.Enabled = flag;
                pnlCaseAssignment.Enabled = flag;
                pnlTask.Enabled = flag;
                pnlCaseHearing.Enabled = flag;
                pnlOrder.Enabled = flag;
                Paneladvbill.Enabled = flag;
                btnSave.Enabled = flag;
                btnSaveStatus.Enabled = flag;
                //btnCaseTransfer.Enabled = flag;

                LnkAddAdvocateBill.Enabled = flag;
                btnEditCaseDetail.Enabled = flag;
                lnkActDetails.Enabled = flag;
                lnkSendMailWithDoc.Enabled = flag;
                lnkLinkCase.Enabled = flag;

             
                AddNewTaskDiv.Visible = flag;
                AddNewHearingDiv.Visible = flag;
                AddNewOrderDiv.Visible = flag;
                //divAdvocateBill.Visible = flag;

                //if (grdTaskActivity.Columns[7] != null)
                //    grdTaskActivity.Columns[7].Visible = flag;
                HideShowGridColumns(grdTaskActivity, "Action", flag);

                grdCasePayment.ShowFooter = flag;

                //if (grdCasePayment.Columns[6] != null)
                //    grdCasePayment.Columns[6].Visible = flag;
                HideShowGridColumns(grdCasePayment, "Action", flag);

                //Compliance Document List
                if (flag)
                {
                    lnkActDetails.Attributes.Remove("disabled");
                    lnkActDetails.Enabled = flag;
                }
                else
                {
                    lnkActDetails.Attributes.Add("disabled", "disabled"); //or try .Add("disabled","true");
                    lnkActDetails.Enabled = flag;
                }

                divCaseDocumentControls.Visible = flag;

                //User Assignment Grid Action Column
                if (grdUserAssignment != null)
                {
                    //if (grdUserAssignment.Columns[5] != null)
                    //    grdUserAssignment.Columns[5].Visible = flag;

                    HideShowGridColumns(grdUserAssignment, "Action", flag);
                }

                //Custom Parameter
                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                {
                    ////emmamiusers.Visible = true;
                    if (grdCustomField_TaxLitigation != null)
                    {
                        grdCustomField_TaxLitigation.Enabled = flag;
                        HideShowGridColumns(grdCustomField_TaxLitigation, "Action", flag);
                    }
                }
                else
                {
                    if (grdCustomField != null)
                    {
                        grdCustomField.Enabled = flag;
                        HideShowGridColumns(grdCustomField, "Action", flag);
                    }
                }
                long customerid = AuthenticationHelper.CustomerID;
                var customizedid = GetCustomizedCustomerid(customerid);
                if (customizedid == AuthenticationHelper.CustomerID)
                {
                    ddlCaseStage.Enabled = flag;
                    ddlCaseStatus.Enabled = flag;
                    ddlCaseResult.Enabled = flag;
                    tbxCaseCloseDate.Enabled = flag;
                    tbxCloseRemark.Enabled = flag;
                    btnSaveStatus.Enabled = flag;
                }   
               
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int GetCustomizedCustomerid(long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "HideEditButtonClosedCase"
                            && row.ClientID == customerid
                            select row.ClientID).FirstOrDefault();

                return data;
            }
        }
        protected void btnEditCaseControls_Click(object sender, EventArgs e)
        {
            try
            {
                enableDisableCaseSummaryTabControls(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        public void BindCaseUserAssignments(long caseInstanceID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                List<SP_Litigation_NoticeCaseUserAssigned_Result> lstCaseAssignments = new List<SP_Litigation_NoticeCaseUserAssigned_Result>();

                lstCaseAssignments = CaseManagement.GetCaseUserAssignments(customerID, caseInstanceID);

                grdUserAssignment.DataSource = lstCaseAssignments;
                grdUserAssignment.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCaseUserAssignmemt.IsValid = false;
                cvCaseUserAssignmemt.ErrorMessage = "Server Error Occurred. Please try again.";
                vsCaseUserAssign.CssClass = "alert alert-danger";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool formValidateSuccess = false;
                bool saveSuccess = false;

                int selectedUserCount = 0;
                int SelectPartylist = 0;
                int SelectActList = 0;
                string AssignedUserList = string.Empty;

                #region Data Validation

                if (rbCaseInOutType.SelectedValue != "")
                {
                    if (DropDownListChosen1.SelectedValue != "")
                    {
                        if (tbxRefNo.Text != "")
                        {
                            if (txtCaseDate.Text != "")
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachParty in ddlParty.Items)
                                {
                                    if (eachParty.Selected)
                                        SelectPartylist++;
                                }
                                if (SelectPartylist > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachAct in ddlAct.Items)
                                    {
                                        if (eachAct.Selected)
                                            SelectActList++;
                                    }
                                    if (SelectActList > 0)
                                    {
                                        if (tbxTitle.Text != "")
                                        {
                                            if (tbxDescription.Text != "")
                                            {
                                                if (ddlCourt.SelectedValue != "" && ddlCourt.SelectedValue != "-1")
                                                {
                                                    //if (tbxJudge.Text != "")
                                                    //{
                                                    if (tvBranches.SelectedValue != "" && tvBranches.SelectedValue != "-1")
                                                    {
                                                        if (ddlDepartment.SelectedValue != "" && ddlDepartment.SelectedValue != "-1")
                                                        {
                                                            if (ddlOwner.SelectedValue != "" && ddlOwner.SelectedValue != "-1")
                                                            {
                                                                foreach (System.Web.UI.WebControls.ListItem eachPerformer in lstBoxPerformer.Items)
                                                                {
                                                                    if (eachPerformer.Selected)
                                                                        selectedUserCount++;
                                                                }

                                                                foreach (System.Web.UI.WebControls.ListItem eachLawyerUser in lstBoxLawyerUser.Items)
                                                                {
                                                                    if (eachLawyerUser.Selected)
                                                                        selectedUserCount++;
                                                                }

                                                                if (selectedUserCount > 0)
                                                                {
                                                                    //if (!string.IsNullOrEmpty(ddlLawFirm.SelectedValue))
                                                                    //{
                                                                    ////if (ddlLawFirm.SelectedValue != "0")
                                                                    ////{
                                                                    formValidateSuccess = true;
                                                                    ////}
                                                                    ////else
                                                                    ////{
                                                                    ////    cvCasePopUp.IsValid = false;
                                                                    ////    cvCasePopUp.ErrorMessage = "Select Lawyer.";
                                                                    ////    VSCasePopup.CssClass = "alert alert-danger";
                                                                    ////}
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    cvCasePopUp.IsValid = false;
                                                                    //    cvCasePopUp.ErrorMessage = "Select Lawyer.";
                                                                    //    VSCasePopup.CssClass = "alert alert-danger";
                                                                    //}
                                                                }
                                                                else
                                                                {
                                                                    cvCasePopUp.IsValid = false;
                                                                    cvCasePopUp.ErrorMessage = "Select User to Assign.";
                                                                    VSCasePopup.CssClass = "alert alert-danger";
                                                                }
                                                            }
                                                            else
                                                            {
                                                                cvCasePopUp.IsValid = false;
                                                                cvCasePopUp.ErrorMessage = "Select Owner.";
                                                                VSCasePopup.CssClass = "alert alert-danger";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvCasePopUp.IsValid = false;
                                                            cvCasePopUp.ErrorMessage = "Select Department.";
                                                            VSCasePopup.CssClass = "alert alert-danger";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvCasePopUp.IsValid = false;
                                                        cvCasePopUp.ErrorMessage = "Select Entity/Location.";
                                                        VSCasePopup.CssClass = "alert alert-danger";
                                                    }
                                                    //}
                                                    //else
                                                    //{
                                                    //    cvCasePopUp.IsValid = false;
                                                    //    cvCasePopUp.ErrorMessage = "Provide Judge Detail.";
                                                    //}
                                                }
                                                else
                                                {
                                                    cvCasePopUp.IsValid = false;
                                                    cvCasePopUp.ErrorMessage = "Select Court.";
                                                    VSCasePopup.CssClass = "alert alert-danger";
                                                }
                                            }
                                            else
                                            {
                                                cvCasePopUp.IsValid = false;
                                                cvCasePopUp.ErrorMessage = "Please Provide Case Description.";
                                                VSCasePopup.CssClass = "alert alert-danger";
                                            }
                                        }
                                        else
                                        {
                                            cvCasePopUp.IsValid = false;
                                            cvCasePopUp.ErrorMessage = "Please Provide Case Title.";
                                            VSCasePopup.CssClass = "alert alert-danger";
                                        }
                                    }
                                    else
                                    {
                                        cvCasePopUp.IsValid = false;
                                        cvCasePopUp.ErrorMessage = "Select Act.";
                                        VSCasePopup.CssClass = "alert alert-danger";
                                    }
                                }
                                else
                                {
                                    cvCasePopUp.IsValid = false;
                                    cvCasePopUp.ErrorMessage = "Select Party.";
                                    VSCasePopup.CssClass = "alert alert-danger";
                                }
                            }
                            else
                            {
                                cvCasePopUp.IsValid = false;
                                cvCasePopUp.ErrorMessage = "Provide Case Date.";
                                VSCasePopup.CssClass = "alert alert-danger";
                            }
                        }
                        else
                        {
                            cvCasePopUp.IsValid = false;
                            cvCasePopUp.ErrorMessage = "Provide Court Case Number.";
                            VSCasePopup.CssClass = "alert alert-danger";
                        }
                    }
                    else
                    {
                        cvCasePopUp.IsValid = false;
                        cvCasePopUp.ErrorMessage = "Select Financial Year.";
                        VSCasePopup.CssClass = "alert alert-danger";
                    }
                }
                else
                {
                    cvCasePopUp.IsValid = false;
                    cvCasePopUp.ErrorMessage = "Select Case Type.";
                    VSCasePopup.CssClass = "alert alert-danger";
                }
                if (RiskType == true)
                {
                    if (ddlRisk.SelectedValue != "" && ddlRisk.SelectedValue != "-1")
                    {

                    }
                    else
                    {
                        //  selectedUserCount++;
                        formValidateSuccess = false; 
                        cvCasePopUp.IsValid = false;
                        cvCasePopUp.ErrorMessage = "Select Risk.";
                        VSCasePopup.CssClass = "alert alert-danger";
                    }
                }
                else
                {
                   
                }

                if (!cvCasePopUp.IsValid)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

                #endregion

                #region Save/Edit Code

                if (formValidateSuccess)
                {
                    long NewCaseID = 0;

                    List<int> lstCaseLawyer = new List<int>();
                    List<int> lstCasePerformerUser = new List<int>();
                    List<int> lstPartyMapping = new List<int>();
                    List<int> lstActMapping = new List<int>();
                    List<int> lstOppoLawyerMapping = new List<int>();
                    List<int> lstExternalLawyer = new List<int>();
                    List<int> CheckAssingedUserOld = new List<int>();
                    List<int> CheckAssingedUserNew = new List<int>();
                    List<long> UpdatedAssingedUser = new List<long>();
                    List<string> lstFinancialYearMapping = new List<string>();
                    // if(!(ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim())) && AuthenticationHelper.CustomerID == 76)
                    if (AuthenticationHelper.CustomerID != 76)
                    {
                        txttaxDemand.Text = "";
                        txtPenalty.Text = "";
                        txtIntrest.Text = "";
                        txtProvisonbook.Text = "";
                        txtamountpaid.Text = "";
                        ddlFY.SelectedValue = "0";
                        txtfavourable.Text = "";
                        txtUnfavourable.Text = "";
                    }
                    if (txttaxDemand.Text == "")
                    {
                        (txttaxDemand.Text) = "0";
                    }

                    if (txtfavourable.Text == "")
                    {
                        (txtfavourable.Text) = "0";
                    }
                    if (txtUnfavourable.Text == "")
                    {
                        (txtUnfavourable.Text) = "0";
                    }

                    if (txtamountpaid.Text == "")
                    {
                        (txtamountpaid.Text) = "0";
                    }
                    if (txtIntrest.Text == "")
                    {
                        txtIntrest.Text = "0";
                    }
                    if (txtPenalty.Text == "")
                    {
                        txtPenalty.Text = "0";
                    }
                    if (ddlState.SelectedValue == "")
                    {
                        ddlState.SelectedValue = "0";
                    }
                    if (ddlCPDepartment.SelectedValue == "")
                    {
                        ddlCPDepartment.SelectedValue = "0";
                    }
                    if (ddlJurisdiction.SelectedValue == "")
                    {
                        ddlJurisdiction.SelectedValue = "0";
                    }
                    if (ddlFY.SelectedValue == "")
                    {
                        ddlFY.SelectedValue = "0";
                    }
                    //if (DropDownListChosen1.SelectedValue == "")
                    //{
                    //    DropDownListChosen1.SelectedValue = "0";
                    //}
                    tbl_LegalCaseInstance NewCase = new tbl_LegalCaseInstance()
                    {
                        IsDeleted = false,
                        CaseType = rbCaseInOutType.SelectedValue,
                        CaseRefNo = tbxRefNo.Text.Trim(),
                        CaseYear = txtCaseYear.Text.Trim(),
                        BenchID = txtBenchID.Text.Trim(),
                        StateID = txtStateID.Text.Trim(),
                        OpenDate = DateTimeExtensions.GetDate(txtCaseDate.Text),
                        Section = tbxSection.Text.Trim(),
                        CaseCategoryID = Convert.ToInt32(ddlCaseCategory.SelectedValue),
                        CaseTitle = tbxTitle.Text.Trim(),
                        CaseDetailDesc = tbxDescription.Text.Trim(),
                        CustomerBranchID = Convert.ToInt32(tvBranches.SelectedValue),
                        DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                        ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue),
                        OwnerID = Convert.ToInt32(ddlOwner.SelectedValue),
                        CourtID = Convert.ToInt32(ddlCourt.SelectedValue),
                        Judge = tbxJudge.Text.Trim(),
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedBy = AuthenticationHelper.UserID,
                        InternalCaseNo = tbxInternalCaseNo.Text,
                        CustomerID = AuthenticationHelper.CustomerID,
                        Period = ddlFY.SelectedValue,
                        Taxdemand = Convert.ToDecimal(txttaxDemand.Text),
                        Intrest = Convert.ToDecimal(txtIntrest.Text),
                        Penalty = Convert.ToDecimal(txtPenalty.Text),
                        ProvisioninBook = txtProvisonbook.Text,
                        state = Convert.ToInt32(ddlState.SelectedValue),
                        Jurisdiction = Convert.ToInt32(ddlJurisdiction.SelectedValue),
                        amountpaid = Convert.ToDecimal(txtamountpaid.Text),
                        favourable = Convert.ToDecimal(txtfavourable.Text),
                        unfavourable = Convert.ToDecimal(txtUnfavourable.Text),
                        Remark=txtremark.Text,
                        RiskTypeID=Convert.ToInt32(ddlRisk.SelectedValue),

                       // FinancialYear = Convert.ToString(DropDownListChosen1.SelectedValue),

                };
                    if (!string.IsNullOrEmpty(txtNoticeDate.Text.Trim()))
                    {
                        //if (tbxReminderDate.Text != "")
                        NewCase.NoticeDate = DateTimeExtensions.GetDate(txtNoticeDate.Text);                    }

                    //if (IsNoticeDate == Convert.ToString(AuthenticationHelper.CustomerID))
                    //{
                    //     NewCase.NoticeDate = DateTimeExtensions.GetDate(txtNoticeDate.Text);
                    // }
                    //if (!string.IsNullOrEmpty(DropDownListChosen1.SelectedValue))
                    //{
                    //    NewCase.FinancialYear = Convert.ToString(DropDownListChosen1.SelectedValue);
                    //}

                    if (!string.IsNullOrEmpty(ddlCaseResult.SelectedValue))
                    {
                        if (ddlCaseResult.SelectedValue != "0")
                        {
                            NewCase.CaseResult = Convert.ToInt32(ddlCaseResult.SelectedValue);
                        }
                    }
                    else
                    {
                        NewCase.CaseResult = Convert.ToInt32(ddlCaseResult.Items.FindByText("In Progress").Value);
                    }
                    if (!string.IsNullOrEmpty(tbxCaseBudget.Text))
                        NewCase.CaseBudget = Convert.ToDecimal(tbxCaseBudget.Text);
                    else
                        NewCase.CaseBudget = 0;

                    if (ddlCaseRisk.SelectedValue != "" && ddlCaseRisk.SelectedValue != "-1")
                        NewCase.CaseRiskID = Convert.ToInt32(ddlCaseRisk.SelectedValue);

                    if (tbxClaimedAmt.Text != "")
                        NewCase.ClaimAmt = Convert.ToDecimal(tbxClaimedAmt.Text.Trim());

                    if (tbxProbableAmt.Text != "")
                        NewCase.ProbableAmt = Convert.ToDecimal(tbxProbableAmt.Text.Trim());

                    #region add provisional bank gurantee and protest money
                    if (txtprovisionalamt.Text != "")
                        NewCase.Provisionalamt = Convert.ToDecimal(txtprovisionalamt.Text.Trim());

                    if (txtbankgurantee.Text != "")
                        NewCase.BankGurantee = txtbankgurantee.Text;

                    if (txtprotestmoney.Text != "")
                        NewCase.ProtestMoney = Convert.ToDecimal(txtprotestmoney.Text.Trim());

                    if (txtRecovery.Text != "")
                        NewCase.RecoveryAmount = Convert.ToDecimal(txtRecovery.Text.Trim());

                   
                    #endregion

                    if (rblPotentialImpact.SelectedValue != "" && rblPotentialImpact.SelectedValue != "-1")
                        NewCase.ImpactType = rblPotentialImpact.SelectedValue;

                    if (tbxMonetory.Text != "")
                        NewCase.Monetory = tbxMonetory.Text;

                    if (tbxNonMonetory.Text != "")
                        NewCase.NonMonetory = tbxNonMonetory.Text;

                    if (tbxNonMonetoryYears.Text != "")
                        NewCase.Years = tbxNonMonetoryYears.Text;

                    //if (ddlPerformer.SelectedValue != "" && ddlPerformer.SelectedValue != "-1")
                    if (selectedUserCount > 0)
                        NewCase.AssignmentType = 1;

                    if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                        NewCase.AssignmentType = 2;

                    if (txtPreDeposit.Text != "")
                        NewCase.PreDeposit = Convert.ToDecimal(txtPreDeposit.Text.Trim());

                    if (txtPostDeposit.Text != "")
                        NewCase.PostDeposit = Convert.ToDecimal(txtPostDeposit.Text.Trim());

                    if ((int)ViewState["Mode"] == 0)
                    {
                        bool existCaseNo = false;
                        if (!RPACustomerEnable)
                        {
                            existCaseNo = CaseManagement.ExistsCourtCaseNo(tbxRefNo.Text, 0);
                        }
                        else
                        {
                            existCaseNo = CaseManagement.ExistsCourtCaseNoForRPA(tbxRefNo.Text,case_year);
                        }
                        if (!existCaseNo)
                        {
                            if (!CaseManagement.ExistsCase(NewCase.CaseTitle, 0))
                            {
                                NewCaseID = CaseManagement.CreateCase(NewCase);

                                if (NewCaseID > 0)
                                {
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Case Created", true);
                                    saveSuccess = true;
                                }
                            }
                            else
                            {
                                cvCasePopUp.IsValid = false;
                                cvCasePopUp.ErrorMessage = "Case with Same Title already Exists.";
                                VSCasePopup.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                        else
                        {
                            cvCasePopUp.IsValid = false;
                            cvCasePopUp.ErrorMessage = "Case with Same Court Case No already exists";
                            VSCasePopup.CssClass = "alert alert-danger";
                        }

                        if (saveSuccess)
                        {
                            //Case Status Transaction
                            #region Status Transaction
                            tbl_LegalCaseStatusTransaction newStatusRecord = new tbl_LegalCaseStatusTransaction()
                            {
                                CaseInstanceID = NewCaseID,
                                StatusID = 1,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false,
                                UserID = AuthenticationHelper.UserID,
                                RoleID = 3,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            if (!CaseManagement.ExistCaseStatusTransaction(newStatusRecord))
                                saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusRecord);

                            #endregion

                            #region Save Party Mapping

                            if (ddlParty.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachParty in ddlParty.Items)
                                {
                                    if (eachParty.Selected)
                                        lstPartyMapping.Add(Convert.ToInt32(eachParty.Value));
                                }
                            }

                            if (lstPartyMapping.Count > 0)
                            {
                                List<tbl_PartyMapping> lstObjPartyMapping = new List<tbl_PartyMapping>();

                                lstPartyMapping.ForEach(EachParty =>
                                {
                                    tbl_PartyMapping objPartyMapping = new tbl_PartyMapping()
                                    {
                                        CaseNoticeInstanceID = NewCaseID,
                                        IsActive = true,
                                        Type = 1,//1 as Case and 2 as Notice
                                        PartyID = Convert.ToInt32(EachParty),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjPartyMapping.Add(objPartyMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdatePartyMapping(lstObjPartyMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_PartyMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Opponent Detail Created", true);
                                }
                                //Refresh List
                                lstObjPartyMapping.Clear();
                                lstObjPartyMapping = null;

                                lstPartyMapping.Clear();
                                lstPartyMapping = null;
                            }
                            #endregion

                            #region Save Act Mapping

                            if (ddlAct.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachAct in ddlAct.Items)
                                {
                                    if (eachAct.Selected)
                                        lstActMapping.Add(Convert.ToInt32(eachAct.Value));
                                }
                            }

                            if (lstActMapping.Count > 0)
                            {
                                List<tbl_ActMapping> lstObjActMapping = new List<tbl_ActMapping>();

                                lstActMapping.ForEach(EachParty =>
                                {
                                    tbl_ActMapping objActMapping = new tbl_ActMapping()
                                    {
                                        CaseNoticeInstanceID = NewCaseID,
                                        IsActive = true,
                                        Type = 1,//1 as Case and 2 as Notice
                                        ActID = Convert.ToInt32(EachParty),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjActMapping.Add(objActMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateActMapping(lstObjActMapping);

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_ActMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Act Mapped.", false);
                                }
                                //Refresh List
                                lstObjActMapping.Clear();
                                lstObjActMapping = null;

                                lstActMapping.Clear();
                                lstActMapping = null;
                            }
                            #endregion

                            
                            #region Save Financial Year Mappping
                            if (DropDownListChosen1.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachFY in DropDownListChosen1.Items)
                                {
                                    if (eachFY.Selected)
                                        lstFinancialYearMapping.Add(Convert.ToString(eachFY.Value));
                                }
                            }

                            if (lstFinancialYearMapping.Count > 0)
                            {
                                List<FinancialYearMapping> lstObjFYMapping = new List<FinancialYearMapping>();

                                lstFinancialYearMapping.ForEach(EachFYID =>
                                {
                                    FinancialYearMapping objFYMapping = new FinancialYearMapping()
                                    {
                                        FYID = Convert.ToString(EachFYID),
                                        Type = 1,//1 as Case and 2 as Notice
                                        CaseNoticeInstanceID = NewCaseID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjFYMapping.Add(objFYMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateFYMapping(lstObjFYMapping);

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "FinancialYearMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Financial Year Mapped.", false);
                                }
                                //Refresh List
                                lstObjFYMapping.Clear();
                                lstObjFYMapping = null;

                                lstFinancialYearMapping.Clear();
                                lstFinancialYearMapping = null;
                            }
                            #endregion
                           
                            #region Save Opposition Lawyer Mapping

                            if (lstBoxOppositionLawyer.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem LawyerList in lstBoxOppositionLawyer.Items)
                                {
                                    if (LawyerList.Selected)
                                        lstOppoLawyerMapping.Add(Convert.ToInt32(LawyerList.Value));
                                }
                            }

                            if (lstOppoLawyerMapping.Count > 0)
                            {
                                List<tbl_OppositionLawyerList> lstObjLawyerMapping = new List<tbl_OppositionLawyerList>();

                                lstOppoLawyerMapping.ForEach(EachParty =>
                                {
                                    tbl_OppositionLawyerList objLawyerMapping = new tbl_OppositionLawyerList()
                                    {
                                        CaseNoticeInstanceID = NewCaseID,
                                        IsActive = true,
                                        Type = 1,//1 as Case and 2 as Notice
                                        LawyerID = Convert.ToInt32(EachParty),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    lstObjLawyerMapping.Add(objLawyerMapping);
                                });

                                saveSuccess = CaseManagement.CreateUpdateOppositionLawyerMapping(lstObjLawyerMapping);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_OppositionLawyerList", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Opposition Lawyer Mapped.", true);
                                }
                                //Refresh List
                                lstOppoLawyerMapping.Clear();
                                lstOppoLawyerMapping = null;

                                lstObjLawyerMapping.Clear();
                                lstObjLawyerMapping = null;
                            }
                            #endregion

                            #region Save Custom Field

                            //if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                            //{
                            //Select Which Grid to Loop based on Selected Category/Type
                            GridView gridViewToCollectData = null;

                            if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && AuthenticationHelper.CustomerID != 76)
                            {
                                gridViewToCollectData = grdCustomField_TaxLitigation;
                            }

                            else
                            {
                                gridViewToCollectData = grdCustomField;
                            }

                            if (gridViewToCollectData != null)
                            {
                                for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                {
                                    Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                    Label lblFY = (Label)gridViewToCollectData.Rows[i].FindControl("lblFY");
                                    TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                    if (lblID != null && tbxLabelValue != null)
                                    {
                                        if (!string.IsNullOrEmpty(lblID.Text))
                                        {
                                            if (lblID.Text != "0")
                                            {
                                                tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                {
                                                    NoticeCaseType = 1,
                                                    NoticeCaseInstanceID = NewCaseID,
                                                    FYear = lblFY.Text,
                                                    LabelID = Convert.ToInt32(lblID.Text),
                                                    LabelValue = tbxLabelValue.Text,
                                                    IsDeleted = false,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                                {
                                                    TextBox tbxInterestValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxInterestValue");
                                                    TextBox tbxPenaltyValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxPenaltyValue");
                                                    TextBox tbxTotalValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxRowTotalValue");
                                                    TextBox tbxProvisionInbooks = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxProvisionInbooks");
                                                    TextBox tbxSettlementValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxSettlement");

                                                    if (tbxInterestValue != null && tbxPenaltyValue != null && tbxTotalValue != null
                                                        && tbxProvisionInbooks != null && tbxSettlementValue != null)
                                                    {
                                                        if (tbxInterestValue.Text != "")
                                                            ObjParameter.Interest = tbxInterestValue.Text;

                                                        if (tbxPenaltyValue.Text != "")
                                                            ObjParameter.Penalty = tbxPenaltyValue.Text;

                                                        if (tbxTotalValue.Text != "")
                                                            ObjParameter.Total = tbxTotalValue.Text;

                                                        if (tbxSettlementValue.Text != "")
                                                            ObjParameter.SettlementValue = tbxSettlementValue.Text;

                                                        if (tbxProvisionInbooks.Text != "")
                                                            ObjParameter.ProvisionInBook = tbxProvisionInbooks.Text;
                                                    }
                                                }

                                                ObjParameter.CreatedBy = AuthenticationHelper.UserID;
                                                ObjParameter.CreatedOn = DateTime.Now;

                                                ObjParameter.UpdatedBy = AuthenticationHelper.UserID;
                                                ObjParameter.UpdatedOn = DateTime.Now;

                                                saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);
                                            }//ID-0 Check
                                        }
                                    }
                                }//End For Each
                            }

                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_NoticeCaseCustomParameter", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Custom Parameter(s) Saved.", true);
                            }
                            //}

                            #endregion

                            //Lawyer Mapping
                            #region Lawyer Mapping
                            if (ddlLawFirm.SelectedValue == "")
                            {
                                ddlLawFirm.SelectedValue = "0";
                            }


                            tbl_LegalCaseLawyerMapping objCaseLawyerMapping = new tbl_LegalCaseLawyerMapping()
                            {
                                CaseInstanceID = NewCaseID,
                                IsActive = true,
                                LawyerID = Convert.ToInt32(ddlLawFirm.SelectedValue),
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            saveSuccess = CaseManagement.CreateCaseLawyerMapping(objCaseLawyerMapping);

                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseLawyerMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Law Firm Mapped", true);
                            }

                            #endregion

                            //User Assignment
                            #region User Assignment

                            //De-Active All Previous Assignment
                            CaseManagement.DeActiveCaseAssignments(NewCaseID, AuthenticationHelper.UserID);

                            if (lstBoxPerformer.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachPerformer in lstBoxPerformer.Items)
                                {
                                    if (eachPerformer.Selected)
                                    {
                                        lstCasePerformerUser.Add(Convert.ToInt32(eachPerformer.Value));
                                        AssignedUserList += "," + eachPerformer.Text;
                                    }
                                }
                                if (lstCasePerformerUser.Count > 0)
                                {
                                    lstCasePerformerUser.ForEach(EachPerformer =>
                                    {
                                        tbl_LegalCaseAssignment newAssignment = new tbl_LegalCaseAssignment()
                                        {
                                            AssignmentType = NewCase.AssignmentType,
                                            CaseInstanceID = NewCaseID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            IsLawyer = false,
                                            UserID = Convert.ToInt32(EachPerformer),
                                            RoleID = 3,
                                        };

                                        saveSuccess = CaseManagement.CreateUpdateCaseAssignment(newAssignment);

                                        //if (!CaseManagement.ExistCaseAssignment(newAssignment))
                                        //{
                                        //    saveSuccess = CaseManagement.CreateCaseAssignment(newAssignment);
                                        //    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Created", true);
                                        //}
                                        //else
                                        //{
                                        //    saveSuccess = CaseManagement.UpdateCaseAssignments(newAssignment);                                            
                                        //    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Updated", true);
                                        //}
                                    });

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Performer Assignment Created", true);
                                    }
                                }
                            }

                            if (lstBoxLawyerUser.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachLawyerUser in lstBoxLawyerUser.Items)
                                {
                                    if (eachLawyerUser.Selected)
                                    {
                                        lstExternalLawyer.Add(Convert.ToInt32(eachLawyerUser.Value));
                                        AssignedUserList += "," + eachLawyerUser.Text;
                                    }
                                }
                                if (lstExternalLawyer.Count > 0)
                                {
                                    lstExternalLawyer.ForEach(EachPerformer =>
                                    {
                                        tbl_LegalCaseAssignment newAssignment = new tbl_LegalCaseAssignment()
                                        {
                                            AssignmentType = NewCase.AssignmentType,
                                            CaseInstanceID = NewCaseID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            IsLawyer = true,
                                            UserID = Convert.ToInt32(EachPerformer),
                                            RoleID = 3,
                                        };

                                        saveSuccess = CaseManagement.CreateUpdateCaseAssignment(newAssignment);

                                        //if (!CaseManagement.ExistCaseAssignment(newAssignment))
                                        //{
                                        //    saveSuccess = CaseManagement.CreateCaseAssignment(newAssignment);                                          
                                        //    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Created", true);
                                        //}
                                        //else
                                        //{
                                        //    saveSuccess = CaseManagement.UpdateCaseAssignments(newAssignment);                                           
                                        //    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Updated", true);
                                        //}
                                    });

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Performer Assignment Created", true);
                                    }
                                }
                            }

                            if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                            {
                                tbl_LegalCaseAssignment newReviewerAssignment = new tbl_LegalCaseAssignment()
                                {
                                    AssignmentType = NewCase.AssignmentType,
                                    CaseInstanceID = NewCaseID,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    IsLawyer = false,
                                    UserID = Convert.ToInt32(ddlReviewer.SelectedValue),
                                    RoleID = 4,
                                };

                                saveSuccess = CaseManagement.CreateUpdateCaseAssignment(newReviewerAssignment);

                                //if (!CaseManagement.ExistCaseAssignment(newReviewerAssignment))
                                //    saveSuccess = CaseManagement.CreateCaseAssignment(newReviewerAssignment);
                                //else
                                //    saveSuccess = CaseManagement.UpdateCaseAssignments(newReviewerAssignment);
                            }

                            #endregion
                        }

                        #region Litigation Reminder Log
                        if (saveSuccess)
                        {
                            try
                            {
                                if (NewCase != null)
                                {
                                    lstCasePerformerUser.ForEach(EachPerformer =>
                                    {
                                        User User = UserManagement.GetByID(EachPerformer);

                                        if (User != null)
                                        {
                                            if (User.Email != null && User.Email != "")
                                            {

                                                tbl_LitigationReminderLog objRemind = new tbl_LitigationReminderLog()
                                                {
                                                    UserID = EachPerformer,
                                                    Role = 3,
                                                    TriggerType = "LitigationCaseAssignment",
                                                    TriggerDate = DateTime.Now
                                                };

                                                CaseManagement.SaveLitigationReminderMail(objRemind);
                                            }
                                        }
                                    }); //End For Each - Performer User
                                    lstExternalLawyer.ForEach(EachPerformer =>
                                    {
                                        User User = UserManagement.GetByID(EachPerformer);

                                        if (User != null)
                                        {
                                            if (User.Email != null && User.Email != "")
                                            {

                                                tbl_LitigationReminderLog objRemind = new tbl_LitigationReminderLog()
                                                {
                                                    UserID = EachPerformer,
                                                    Role = 3,
                                                    TriggerType = "LitigationCaseAssignment",
                                                    TriggerDate = DateTime.Now
                                                };

                                                CaseManagement.SaveLitigationReminderMail(objRemind);
                                            }
                                        }
                                    }); //End For Each - Performer User

                                    lstCasePerformerUser.Clear();
                                    lstCasePerformerUser = null;
                                    lstExternalLawyer.Clear();
                                    lstExternalLawyer = null;
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LitigationReminderLog", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Email Sent to Assigned Users", true);
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                        #endregion

                        if (saveSuccess)
                        {
                            #region Mail Data
                            List<long> OwnerMailID = new List<long>();
                            List<string> OwnerMailList = new List<string>();
                            List<string> AssignedUseerMailID = new List<string>();
                            List<string> ExcludedMailID = new List<string>();
                            User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                            CheckAssingedUserOld = CaseManagement.GetAllAssingedUserList(NewCaseID, AuthenticationHelper.UserID);

                            if (CheckAssingedUserOld.Count > 0)
                            {
                                foreach (var item in CheckAssingedUserOld)
                                {
                                    if (CheckAssingedUserOld.Contains(item))
                                    {
                                        UpdatedAssingedUser.Add(Convert.ToInt64(item));
                                    }
                                }
                            }
                            bool islawyer = false;
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                //var Query = (from row in entities.CaseLawayerEmails
                                //             where row.Customerid==AuthenticationHelper.CustomerID
                                //             && row.IsActive==false
                                //             select row).FirstOrDefault();

                                //if (Query !=null)
                                //{
                                //    islawyer = true;
                                //}
                            }
                            if (islawyer)
                            {
                                //   AssignedUseerMailID = CaseManagement.GetAssignedUserAndOwnerMailexceptLawyer(UpdatedAssingedUser);
                                if (ddlOwner.SelectedValue != "" && ddlOwner.SelectedValue != "-1")
                                {
                                    OwnerMailID.Add(Convert.ToInt64(ddlOwner.SelectedValue));
                                }
                                // OwnerMailList = CaseManagement.GetAssignedUserAndOwnerMailexceptLawyer(OwnerMailID);
                            }
                            else
                            {
                                AssignedUseerMailID = CaseManagement.GetAssignedUserAndOwnerMail(UpdatedAssingedUser);
                                if (ddlOwner.SelectedValue != "" && ddlOwner.SelectedValue != "-1")
                                {
                                    OwnerMailID.Add(Convert.ToInt64(ddlOwner.SelectedValue));
                                }
                                OwnerMailList = CaseManagement.GetAssignedUserAndOwnerMail(OwnerMailID);
                            }

                            var caseRecord = CaseManagement.GetCaseByID(Convert.ToInt32(NewCaseID));
                            var Locations = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(caseRecord.CustomerBranchID)))
                            {
                                Locations = CaseManagement.GetLocationByCaseInstanceID(caseRecord.CustomerBranchID);
                            }

                            List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                            List<string> UniqueMail = new List<string>();
                            if (OwnerMailList.Count > 0)
                            {
                                foreach (var item in OwnerMailList)
                                {
                                    if (!UniqueMail.Contains(item))
                                    {
                                        UniqueMail.Add(item);
                                    }
                                }
                            }
                            if (MgmUser.Count > 0)
                            {
                                foreach (var item in MgmUser)
                                {
                                    if (!UniqueMail.Contains(item))
                                    {
                                        UniqueMail.Add(item);
                                    }
                                }
                            }
                            // Remove assigned user if exist in mgm
                            if (AssignedUseerMailID.Count > 0)
                            {
                                foreach (var item in UniqueMail)
                                {
                                    if (AssignedUseerMailID.Contains(item))
                                    {
                                        AssignedUseerMailID.Remove(item);
                                        continue;
                                    }
                                }
                                string CaseTitleMerge = caseRecord.CaseTitle;
                                string FinalCaseTitle = string.Empty;
                                if (CaseTitleMerge.Length > 50)
                                {
                                    FinalCaseTitle = CaseTitleMerge.Substring(0, 50);
                                    FinalCaseTitle = FinalCaseTitle + "...";
                                }
                                else
                                {
                                    FinalCaseTitle = CaseTitleMerge;
                                }

                                var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                string CasePriority = string.Empty;
                                if (NewCase.CaseRiskID != null)
                                {
                                    if (NewCase.CaseRiskID == 1)
                                        CasePriority = "High";
                                    else if (NewCase.CaseRiskID == 2)
                                        CasePriority = "Medium";
                                    else if (NewCase.CaseRiskID == 3)
                                        CasePriority = "Low";
                                }
                                AssignedUserList = AssignedUserList.TrimStart(',');
                                string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                                string accessURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    accessURL = Urloutput.URL;
                                }
                                else
                                {
                                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }
                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_CaseAssignment
                                                       .Replace("@User", username)
                                                       .Replace("@CaseRefNo", NewCase.CaseRefNo)
                                                       .Replace("@CaseTitle", NewCase.CaseTitle)
                                                       .Replace("@CaseDetailDesc", NewCase.CaseDetailDesc)
                                                       .Replace("@Location", Locations)
                                                       .Replace("@AssignedBy", AssignedUserList)
                                                       .Replace("@Category", ddlCaseCategory.SelectedItem.Text)
                                                       .Replace("@From", cname.Trim())
                                                       .Replace("@PortalURL", Convert.ToString(accessURL));

                                ExcludedMailID = CaseManagement.Mailallowedornotcheck("Case Creation", CustomerID);

                                if (ExcludedMailID.Count > 0)
                                {
                                    foreach (var item in UniqueMail.ToList())
                                    {
                                        if (ExcludedMailID.Contains(item))
                                        {
                                            UniqueMail.Remove(item);
                                            continue;
                                        }
                                    }

                                    if (AssignedUseerMailID.Count > 0)
                                    {
                                        foreach (var item in AssignedUseerMailID.ToList())
                                        {
                                            if (ExcludedMailID.Contains(item))
                                            {
                                                AssignedUseerMailID.Remove(item);
                                                continue;
                                            }
                                        }
                                    }
                                }

                                try
                                {
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(AssignedUseerMailID), UniqueMail, null, "Litigation Notification Case Assigned - " + FinalCaseTitle, message);
                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                            }
                            #endregion

                            cvCasePopUp.IsValid = false;
                            cvCasePopUp.ErrorMessage = "Case Created Successfully.";
                            VSCasePopup.CssClass = "alert alert-success";
                            liCaseHearing.Visible = true;
                            liCaseTask.Visible = true;
                            liCaseOrder.Visible = true;
                            liCaseStatus.Visible = true;
                            liOthers.Visible = true;
                            liCaseRating.Visible = true;
                            liAuditLog.Visible = true;
                            liDocument.Visible = true;
                            //if (IsAdvocateBillShow == Convert.ToString(AuthenticationHelper.CustomerID))
                            if (ShowAdvocateBillPayment == true)
                            {
                                liCaseAdvocateBill.Visible = true;
                            }

                            ViewState["Mode"] = 1;
                            ViewState["CaseInstanceID"] = NewCaseID;
                            if (!string.IsNullOrEmpty(Request.QueryString["RPAHCData"]) && Convert.ToString(Request.QueryString["RPAHCData"]) != "[]")
                            {
                                var keys = Request.QueryString["RPAHCData"];
                                dynamic key = JsonConvert.DeserializeObject(keys);

                                foreach (var item in key)
                                {
                                    if (item["hearingDate"].Value != "" && item["hearingDate"].Value != null)
                                    {
                                        if (NewCaseID != 0 || NewCaseID != -1)
                                        {
                                            tbl_CaseHearingRef TBL_CHR = new tbl_CaseHearingRef();
                                            TBL_CHR.CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                            TBL_CHR.IsDeleted = false;
                                            TBL_CHR.CaseNoticeInstanceID = NewCaseID;
                                            if (!string.IsNullOrEmpty(item["hearingDate"].Value))
                                            {
                                                TBL_CHR.HearingRefNo = "Hearing-" + Convert.ToDateTime(item["hearingDate"].Value).ToString("dd-MM-yyyy");
                                            }
                                            TBL_CHR.HearingDate = DateTimeExtensions.GetDate(Convert.ToDateTime(item["hearingDate"].Value).ToString("dd/MM/yyyy"));
                                            TBL_CHR.CreatedOn = DateTime.Now;
                                            TBL_CHR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;

                                            var result = CaseManagement.GetExistsRefNo(TBL_CHR);

                                            if (result != -1)
                                            {
                                                var newHearingRefID = CaseManagement.CreateNewRefNo(TBL_CHR);

                                                if (newHearingRefID > 0)
                                                //saveSuccess = true;
                                                {
                                                    tbl_LegalCaseResponse TBL_LCR = new tbl_LegalCaseResponse();
                                                    TBL_LCR.IsActive = true;
                                                    TBL_LCR.CaseInstanceID = NewCaseID;
                                                    TBL_LCR.RefID = newHearingRefID;
                                                    TBL_LCR.ResponseDate = DateTimeExtensions.GetDate(Convert.ToDateTime(item["hearingDate"].Value).ToString("dd/MM/yyyy"));
                                                    TBL_LCR.Description = item["description"].Value;
                                                    TBL_LCR.UserID = Portal.Common.AuthenticationHelper.UserID;
                                                    TBL_LCR.RoleID = 3;
                                                    TBL_LCR.Remark = item["remarks"].Value;

                                                    if (!String.IsNullOrEmpty(item["nextDateHearing"].Value))
                                                        TBL_LCR.ReminderDate = DateTimeExtensions.GetDate(Convert.ToDateTime(item["nextDateHearing"].Value).ToString("dd/MM/yyyy"));

                                                    TBL_LCR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                                    TBL_LCR.CreatedOn = DateTime.Now;
                                                    TBL_LCR.CreatedByText = Portal.Common.AuthenticationHelper.User;

                                                    var newResponseID = CaseManagement.CreateCaseResponseLog(TBL_LCR);
                                                    if (newResponseID > 0)
                                                    {
                                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseResponse", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Created By Excel Upload", true);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(Request.QueryString["RPAHCResultData"]) && Convert.ToString(Request.QueryString["RPAHCResultData"]) != "[]")
                            {
                                var keys = Request.QueryString["RPAHCResultData"];
                                dynamic key = JsonConvert.DeserializeObject(keys);

                                ddlCaseStatus.SelectedValue = key[0]["CaseStatus"].Value;

                                string CaseStage = key[0]["CaseStage"].Value;

                                if (!string.IsNullOrEmpty(CaseStage))
                                {
                                    int casestageID = GetCaseStageIDByName(CaseStage.Trim());
                                    if (casestageID == 0 || casestageID == -1)
                                    {
                                        // errorMessage.Add("Please Correct the Case Stage or Case Stage not Defined in the System at row number - " + (count + 1) + "");
                                        tbl_TypeMaster _objCaseStage = new tbl_TypeMaster()
                                        {
                                            TypeName = CaseStage,
                                            customerID = (int)AuthenticationHelper.CustomerID,
                                            TypeCode = "CS",
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.UtcNow.Date,

                                        };
                                        CaseManagement.CreateCaseStageDetails(_objCaseStage);
                                        BindCaseStage();
                                        ddlCaseStage.SelectedValue = Convert.ToString(GetCaseStageIDByName(CaseStage.Trim()));
                                    }
                                    else
                                    {
                                        ddlCaseStage.SelectedValue = Convert.ToString(casestageID);
                                    }
                                }

                                // ddlCaseResult.SelectedValue = key[0]["CaseResult"].Value;
                                tbxCaseCloseDate.Text = key[0]["CaseCloseDate"].Value;

                                bool saveSuccess1 = false;
                                int caseresult = 0;
                                string CaseStatus = string.Empty;

                                if (!String.IsNullOrEmpty(ddlCaseStatus.SelectedValue) && ddlCaseStatus.SelectedValue != "0")
                                {
                                    if (!String.IsNullOrEmpty(ddlCaseStage.SelectedValue) && ddlCaseStage.SelectedValue != "0")
                                    {
                                        long caseInstanceID = 0;
                                        caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                                        if (caseInstanceID != 0)
                                        {
                                            int selectedStatusID = Convert.ToInt32(ddlCaseStatus.SelectedValue);
                                            int selectedStageID = Convert.ToInt32(ddlCaseStage.SelectedValue);

                                            //Update Case Stage in Case Instance Table
                                            saveSuccess1 = CaseManagement.UpdateCaseStage(caseInstanceID, selectedStageID, AuthenticationHelper.UserID);

                                            if (saveSuccess1)
                                            {
                                                LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Stage Updated", true);

                                                //Status Transaction Record - Which will Create or Update on Each Status Move
                                                tbl_LegalCaseStatusTransaction newStatusTxnRecord = new tbl_LegalCaseStatusTransaction()
                                                {
                                                    CaseInstanceID = caseInstanceID,
                                                    StatusID = selectedStatusID,
                                                    StatusChangeOn = DateTime.Now,
                                                    IsActive = true,
                                                    IsDeleted = false,
                                                    UserID = AuthenticationHelper.UserID,
                                                    RoleID = 3,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                    StatusRemark = tbxCloseRemark.Text
                                                };

                                                //Status Record - i.e. Case Closure Record; Only Active on Case Close otherwise DeActive 
                                                tbl_LegalCaseStatus newStatusRecord = new tbl_LegalCaseStatus()
                                                {
                                                    CaseInstanceID = caseInstanceID,
                                                    StatusID = selectedStatusID,
                                                    CloseDate = DateTime.Now,
                                                    IsActive = true,
                                                    IsDeleted = false,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };
                                                if (!string.IsNullOrEmpty(ddlCaseResult.SelectedValue))
                                                {
                                                    if (ddlCaseResult.SelectedValue != "0")
                                                    {
                                                        caseresult = Convert.ToInt32(ddlCaseResult.SelectedValue);
                                                        CaseStatus = ddlCaseStatus.SelectedItem.Text;
                                                    }
                                                }
                                                else
                                                {
                                                    caseresult = Convert.ToInt32(ddlCaseResult.Items.FindByText("In Progress").Value);
                                                    CaseStatus = "Close";
                                                }

                                                tbl_LegalCaseInstance objcaseInstance = new tbl_LegalCaseInstance()
                                                {
                                                    ID = caseInstanceID,
                                                    CaseResult = caseresult
                                                };

                                                saveSuccess1 = CaseManagement.UpdateCaseResult(objcaseInstance);
                                                if (saveSuccess1)
                                                {
                                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Result Updated", true);
                                                }
                                                if (tbxCloseRemark.Text != "")
                                                    newStatusRecord.ClosureRemark = tbxCloseRemark.Text;

                                                if (tbxCaseCloseDate.Text != "")
                                                    newStatusRecord.CloseDate = Convert.ToDateTime(tbxCaseCloseDate.Text);

                                                if (ddlCaseStatus.SelectedValue != "3") //Open or In Progress
                                                {
                                                    if (!CaseManagement.ExistCaseStatusTransaction(newStatusTxnRecord))
                                                    {
                                                        saveSuccess1 = CaseManagement.DeActiveCaseStatusTransaction(newStatusTxnRecord);
                                                        saveSuccess1 = CaseManagement.CreateCaseStatusTransaction(newStatusTxnRecord);
                                                    }
                                                    else
                                                        saveSuccess1 = CaseManagement.UpdateCaseStatusTransaction(newStatusTxnRecord);

                                                    //If Exists Case Closure Record then DeActive it
                                                    if (CaseManagement.ExistCaseStatus(newStatusRecord))
                                                    {
                                                        saveSuccess1 = CaseManagement.UpdateCaseStatus(newStatusRecord);
                                                        if (saveSuccess1)
                                                        {
                                                            LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseStatus", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Status Updated", true);
                                                        }
                                                    }
                                                }
                                                else if (ddlCaseStatus.SelectedValue == "3") //Close
                                                {
                                                    if (tbxCaseCloseDate.Text != "")
                                                    {
                                                        //Create or Update Status Transaction Records
                                                        if (!CaseManagement.ExistCaseStatusTransaction(newStatusTxnRecord))
                                                        {
                                                            saveSuccess1 = CaseManagement.DeActiveCaseStatusTransaction(newStatusTxnRecord);
                                                            saveSuccess1 = CaseManagement.CreateCaseStatusTransaction(newStatusTxnRecord);
                                                        }
                                                        else
                                                            saveSuccess1 = CaseManagement.UpdateCaseStatusTransaction(newStatusTxnRecord);

                                                        //Create or Update Status Record
                                                        if (!CaseManagement.ExistCaseStatus(newStatusRecord))
                                                        {
                                                            saveSuccess1 = CaseManagement.CreateCaseStatus(newStatusRecord);
                                                            if (saveSuccess1)
                                                            {
                                                                LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseStatus", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Status Created", true);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            saveSuccess1 = CaseManagement.UpdateCaseStatus(newStatusRecord);
                                                            if (saveSuccess1)
                                                            {
                                                                LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseStatus", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Status Updated", true);
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                            if (saveSuccess1)
                                            {
                                                ViewState["caseStatus"] = selectedStatusID;
                                            }
                                        }
                                    }
                                }
                            }

                            enableDisableCaseSummaryTabControls(false);

                            btnSave.Visible = true;
                            btnSave.Text = "Update";
                            btnClearCaseDetail.Visible = true;

                            showHideButtons(true);

                            BindCaseUserAssignments(NewCaseID);
                        }
                    }//Add Code End

                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (ViewState["CaseInstanceID"] != null)
                        {
                            NewCaseID = Convert.ToInt32(ViewState["CaseInstanceID"]); //Selected Case ID
                            NewCase.ID = NewCaseID;

                            bool existCaseNo = CaseManagement.ExistsCourtCaseNo(tbxRefNo.Text, NewCaseID);
                            if (!existCaseNo)
                            {
                                if (CaseManagement.ExistsCase(NewCase.CaseTitle, NewCaseID))
                                {
                                    saveSuccess = CaseManagement.UpdateCase(NewCase,Convert.ToString(AuthenticationHelper.CustomerID),IsNoticeDate);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Details Updated", true);
                                        saveSuccess = true;
                                    }
                                }
                                else
                                {
                                    cvCasePopUp.IsValid = false;
                                    cvCasePopUp.ErrorMessage = "Case with Same Title already Exists.";
                                    VSCasePopup.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvCasePopUp.IsValid = false;
                                cvCasePopUp.ErrorMessage = "Case with Same Court Case No already exists";
                                VSCasePopup.CssClass = "alert alert-danger";
                            }

                            if (saveSuccess)
                            {
                                //Case Status Transaction
                                #region Status Transaction
                                if ((int)ViewState["Mode"] == 0)
                                {
                                    tbl_LegalCaseStatusTransaction newStatusRecord = new tbl_LegalCaseStatusTransaction()
                                    {
                                        CaseInstanceID = NewCaseID,
                                        StatusID = 1,
                                        StatusChangeOn = DateTime.Now,
                                        IsActive = true,
                                        IsDeleted = false,
                                        UserID = AuthenticationHelper.UserID,
                                        RoleID = 3,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                    };
                              

                                if (!CaseManagement.ExistCaseStatusTransaction(newStatusRecord))
                                {
                                    saveSuccess = CaseManagement.DeActiveCaseStatusTransaction(newStatusRecord);
                                    saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusRecord);
                                }
                               }


                                #endregion

                                #region Save Party Mapping

                                if (ddlParty.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachParty in ddlParty.Items)
                                    {
                                        if (eachParty.Selected)
                                            lstPartyMapping.Add(Convert.ToInt32(eachParty.Value));
                                    }
                                }

                                if (lstPartyMapping.Count > 0)
                                {
                                    List<tbl_PartyMapping> lstObjPartyMapping = new List<tbl_PartyMapping>();

                                    saveSuccess = CaseManagement.DeActiveExistingPartyMapping(NewCaseID, 1);

                                    lstPartyMapping.ForEach(EachParty =>
                                    {
                                        tbl_PartyMapping objPartyMapping = new tbl_PartyMapping()
                                        {
                                            CaseNoticeInstanceID = NewCaseID,
                                            IsActive = true,
                                            Type = 1,//1 as Case and 2 as Notice
                                            PartyID = Convert.ToInt32(EachParty),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstObjPartyMapping.Add(objPartyMapping);
                                    });

                                    saveSuccess = CaseManagement.CreateUpdatePartyMapping(lstObjPartyMapping);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_PartyMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Opponent Detail Updated", true);
                                    }
                                    //Refresh List
                                    lstObjPartyMapping.Clear();
                                    lstObjPartyMapping = null;

                                    lstPartyMapping.Clear();
                                    lstPartyMapping = null;
                                }
                                #endregion

                                #region Save Act Mapping

                                if (ddlAct.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachAct in ddlAct.Items)
                                    {
                                        if (eachAct.Selected)
                                            lstActMapping.Add(Convert.ToInt32(eachAct.Value));
                                    }
                                }

                                if (lstActMapping.Count > 0)
                                {
                                    List<tbl_ActMapping> lstObjActMapping = new List<tbl_ActMapping>();
                                    saveSuccess = CaseManagement.DeActiveExistingActMapping(NewCaseID, 1);

                                    lstActMapping.ForEach(EachParty =>
                                    {
                                        tbl_ActMapping objActMapping = new tbl_ActMapping()
                                        {
                                            CaseNoticeInstanceID = NewCaseID,
                                            IsActive = true,
                                            Type = 1,//1 as Case and 2 as Notice
                                            ActID = Convert.ToInt32(EachParty),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstObjActMapping.Add(objActMapping);
                                    });

                                    saveSuccess = CaseManagement.CreateUpdateActMapping(lstObjActMapping);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_ActMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Act Detail Updated", true);
                                    }
                                    //Refresh List
                                    lstObjActMapping.Clear();
                                    lstObjActMapping = null;

                                    lstActMapping.Clear();
                                    lstActMapping = null;
                                }
                                #endregion

                                #region Save Financial Year Mappping
                                if (DropDownListChosen1.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachFY in DropDownListChosen1.Items)
                                    {
                                        if (eachFY.Selected)
                                            lstFinancialYearMapping.Add(Convert.ToString(eachFY.Value));
                                    }
                                }

                                if (lstFinancialYearMapping.Count > 0)
                                {
                                    List<FinancialYearMapping> lstObjFYMapping = new List<FinancialYearMapping>();

                                    saveSuccess = CaseManagement.DeActiveExistingFYMapping(NewCaseID, 1);
                                    lstFinancialYearMapping.ForEach(EachFYID =>
                                    {
                                        FinancialYearMapping objFYMapping = new FinancialYearMapping()
                                        {
                                            FYID = Convert.ToString(EachFYID),
                                            Type = 1,//1 as Case and 2 as Notice
                                            CaseNoticeInstanceID = NewCaseID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstObjFYMapping.Add(objFYMapping);
                                    });

                                    saveSuccess = CaseManagement.CreateUpdateFYMapping(lstObjFYMapping);

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "FinancialYearMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Financial Year Mapped.", false);
                                    }
                                    //Refresh List
                                    lstObjFYMapping.Clear();
                                    lstObjFYMapping = null;

                                    lstFinancialYearMapping.Clear();
                                    lstFinancialYearMapping = null;
                                }
                                #endregion

                                #region Save Opposition Lawyer Mapping

                                if (lstBoxOppositionLawyer.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem LawyerList in lstBoxOppositionLawyer.Items)
                                    {
                                        if (LawyerList.Selected)
                                            lstOppoLawyerMapping.Add(Convert.ToInt32(LawyerList.Value));
                                    }
                                    saveSuccess = CaseManagement.DeActiveExistingOppoLawyerMapping(NewCaseID, 1);
                                }

                                if (lstOppoLawyerMapping.Count > 0)
                                {
                                    List<tbl_OppositionLawyerList> lstObjLawyerMapping = new List<tbl_OppositionLawyerList>();
                                    
                                    lstOppoLawyerMapping.ForEach(EachParty =>
                                    {
                                        tbl_OppositionLawyerList objLawyerMapping = new tbl_OppositionLawyerList()
                                        {
                                            CaseNoticeInstanceID = NewCaseID,
                                            IsActive = true,
                                            Type = 1,//1 as Case and 2 as Notice
                                            LawyerID = Convert.ToInt32(EachParty),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstObjLawyerMapping.Add(objLawyerMapping);
                                    });

                                    saveSuccess = CaseManagement.CreateUpdateOppositionLawyerMapping(lstObjLawyerMapping);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_OppositionLawyerList", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Opposition Lawyer Updated", true);
                                    }
                                    //Refresh List
                                    lstOppoLawyerMapping.Clear();
                                    lstOppoLawyerMapping = null;

                                    lstObjLawyerMapping.Clear();
                                    lstObjLawyerMapping = null;
                                }
                                #endregion

                                //Lawyer Mapping
                                #region Lawyer Mapping                                

                                if (!string.IsNullOrEmpty(ddlLawFirm.SelectedValue))
                                {
                                    if (ddlLawFirm.SelectedValue != "0")
                                    {
                                        List<tbl_LegalCaseLawyerMapping> lstObjCaseMapping = new List<tbl_LegalCaseLawyerMapping>();

                                        saveSuccess = CaseManagement.DeActiveExistingCaseLawyerMapping(NewCaseID);

                                        if (saveSuccess)
                                        {
                                            tbl_LegalCaseLawyerMapping objCaseLawyerMapping = new tbl_LegalCaseLawyerMapping()
                                            {
                                                CaseInstanceID = NewCaseID,
                                                IsActive = true,
                                                LawyerID = Convert.ToInt32(ddlLawFirm.SelectedValue),
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            lstObjCaseMapping.Add(objCaseLawyerMapping);

                                            saveSuccess = CaseManagement.UpdateCaseLawyerMapping(objCaseLawyerMapping);

                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseLawyerMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Lawyer Mapping Updated", true);
                                            }
                                        }
                                    }
                                }

                                #endregion

                                #region Update Custom Field
                                saveSuccess = LitigationManagement.DeActiveExistingeCustomsFieldByNoticeOrCaseID(1, NewCaseID);

                                //if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                //{
                                //Select Which Grid to Loop based on Selected Category/Type
                                GridView gridViewToCollectData = null;

                                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && AuthenticationHelper.CustomerID != 76)
                                {
                                    gridViewToCollectData = grdCustomField_TaxLitigation;
                                }

                                else
                                {
                                    gridViewToCollectData = grdCustomField;
                                }

                                for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                {
                                    Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                    TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                    if (lblID != null && tbxLabelValue != null)
                                    {
                                        if (!string.IsNullOrEmpty(lblID.Text) && !string.IsNullOrEmpty(tbxLabelValue.Text))
                                        {
                                            if (lblID.Text != "0")
                                            {
                                                tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                {
                                                    NoticeCaseType = 1,
                                                    NoticeCaseInstanceID = NewCaseID,

                                                    LabelID = Convert.ToInt32(lblID.Text),
                                                    LabelValue = tbxLabelValue.Text,

                                                    IsActive = true,
                                                    IsDeleted = false,
                                                };

                                                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                                {
                                                    TextBox tbxInterestValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxInterestValue");
                                                    TextBox tbxPenaltyValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxPenaltyValue");
                                                    TextBox tbxTotalValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxRowTotalValue");
                                                    TextBox tbxProvisionInbooks = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxProvisionInbooks");
                                                    TextBox tbxSettlementValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxSettlement");

                                                    if (tbxInterestValue != null && tbxPenaltyValue != null && tbxTotalValue != null
                                                        && tbxProvisionInbooks != null && tbxSettlementValue != null)
                                                    {
                                                        if (tbxInterestValue.Text != "")
                                                            ObjParameter.Interest = tbxInterestValue.Text;

                                                        if (tbxPenaltyValue.Text != "")
                                                            ObjParameter.Penalty = tbxPenaltyValue.Text;

                                                        if (tbxTotalValue.Text != "")
                                                            ObjParameter.Total = tbxTotalValue.Text;

                                                        if (tbxSettlementValue.Text != "")
                                                            ObjParameter.SettlementValue = tbxSettlementValue.Text;

                                                        if (tbxProvisionInbooks.Text != "")
                                                            ObjParameter.ProvisionInBook = tbxProvisionInbooks.Text;
                                                    }
                                                }

                                                ObjParameter.CreatedBy = AuthenticationHelper.UserID;
                                                ObjParameter.CreatedOn = DateTime.Now;

                                                ObjParameter.UpdatedBy = AuthenticationHelper.UserID;
                                                ObjParameter.UpdatedOn = DateTime.Now;

                                                saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);
                                            } //ID-0 Check
                                        }
                                    }
                                }//END FOR LOOP - grdCustomFields
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_NoticeCaseCustomParameter", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Updated.", true);
                                }
                                //}
                                #endregion

                                //User Assignment ---Able to Edit User Assignment Only After Re-Assign
                                #region User Assignment  
                                //CheckAssingedUserOld = CaseManagement.GetAllAssingedUserList(NewCaseID, AuthenticationHelper.UserID);
                                //De-Active All Previous Assignment
                                CaseManagement.DeActiveCaseAssignments(NewCaseID, AuthenticationHelper.UserID);

                                if (lstBoxPerformer.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachPerformer in lstBoxPerformer.Items)
                                    {
                                        if (eachPerformer.Selected)
                                            lstCasePerformerUser.Add(Convert.ToInt32(eachPerformer.Value));
                                    }

                                    if (lstCasePerformerUser.Count > 0)
                                    {
                                        lstCasePerformerUser.ForEach(EachPerformer =>
                                        {
                                            tbl_LegalCaseAssignment newAssignment = new tbl_LegalCaseAssignment()
                                            {
                                                AssignmentType = NewCase.AssignmentType,
                                                CaseInstanceID = NewCaseID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UserID = Convert.ToInt32(EachPerformer),
                                                RoleID = 3,
                                                IsLawyer = false
                                            };

                                            saveSuccess = CaseManagement.CreateUpdateCaseAssignment(newAssignment);

                                            //if (!CaseManagement.ExistCaseAssignment(newAssignment))
                                            //{
                                            //    saveSuccess = CaseManagement.CreateCaseAssignment(newAssignment);
                                            //    if (saveSuccess)
                                            //    {
                                            //        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Created", true);
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    saveSuccess = CaseManagement.UpdateCaseAssignments(newAssignment);
                                            //    if (saveSuccess)
                                            //    {
                                            //        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Updated", true);
                                            //    }
                                            //}
                                        });

                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Performer Assignment Updated", true);
                                        }
                                    }
                                }

                                lstCasePerformerUser.Clear();

                                if (lstBoxLawyerUser.Items.Count > 0)
                                {
                                    foreach (System.Web.UI.WebControls.ListItem eachLawyerUser in lstBoxLawyerUser.Items)
                                    {
                                        if (eachLawyerUser.Selected)
                                            lstCasePerformerUser.Add(Convert.ToInt32(eachLawyerUser.Value));
                                    }

                                    if (lstCasePerformerUser.Count > 0)
                                    {
                                        lstCasePerformerUser.ForEach(EachPerformer =>
                                        {
                                            tbl_LegalCaseAssignment newAssignment = new tbl_LegalCaseAssignment()
                                            {
                                                AssignmentType = NewCase.AssignmentType,
                                                CaseInstanceID = NewCaseID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UserID = Convert.ToInt32(EachPerformer),
                                                RoleID = 3,
                                                IsLawyer = true
                                            };

                                            saveSuccess = CaseManagement.CreateUpdateCaseAssignment(newAssignment);

                                            //if (!CaseManagement.ExistCaseAssignment(newAssignment))
                                            //{
                                            //    saveSuccess = CaseManagement.CreateCaseAssignment(newAssignment);
                                            //    if (saveSuccess)
                                            //    {
                                            //        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Created", true);
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    saveSuccess = CaseManagement.UpdateCaseAssignments(newAssignment);
                                            //    if (saveSuccess)
                                            //    {
                                            //        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Updated", true);
                                            //    }
                                            //}
                                        });

                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Performer Assignment Updated", true);
                                        }
                                    }
                                }

                                if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                                {
                                    tbl_LegalCaseAssignment newReviewerAssignment = new tbl_LegalCaseAssignment()
                                    {
                                        AssignmentType = NewCase.AssignmentType,
                                        CaseInstanceID = NewCaseID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                        IsLawyer = false,
                                        UserID = Convert.ToInt32(ddlReviewer.SelectedValue),
                                        RoleID = 4,
                                    };

                                    saveSuccess = CaseManagement.CreateUpdateCaseAssignment(newReviewerAssignment);

                                    //if (!CaseManagement.ExistCaseAssignment(newReviewerAssignment))
                                    //    saveSuccess = CaseManagement.CreateCaseAssignment(newReviewerAssignment);
                                    //else
                                    //    saveSuccess = CaseManagement.UpdateCaseAssignments(newReviewerAssignment);
                                }

                                #endregion

                                //Upload Document
                                #region Upload Document

                                //if (CaseFileUpload.HasFiles)
                                //{
                                //    tbl_LitigationFileData objCaseDoc = new tbl_LitigationFileData()
                                //    {
                                //        NoticeCaseInstanceID = Convert.ToInt32(NewCaseID),
                                //        CreatedBy = AuthenticationHelper.UserID,
                                //        CreatedByText = AuthenticationHelper.User,
                                //        IsDeleted = false,
                                //        DocType = "C"
                                //    };

                                //    HttpFileCollection fileCollection = Request.Files;

                                //    if (fileCollection.Count > 0)
                                //    {
                                //        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                //        int customerID = -1;
                                //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                //        string directoryPath = "";
                                //        String fileName = "";

                                //        if (NewCaseID > 0)
                                //        {
                                //            for (int i = 0; i < fileCollection.Count; i++)
                                //            {
                                //                HttpPostedFile uploadedFile = fileCollection[i];

                                //                if (uploadedFile.ContentLength > 0)
                                //                {
                                //                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                //                    if (keys1[keys1.Count() - 1].Equals("CaseFileUpload"))
                                //                    {
                                //                        fileName = uploadedFile.FileName;
                                //                    }

                                //                    objCaseDoc.FileName = fileName;

                                //                    //Get Document Version
                                //                    var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objCaseDoc);

                                //                    caseDocVersion++;
                                //                    objCaseDoc.Version = caseDocVersion + ".0";

                                //                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(NewCaseID) + "/CaseDocument/" + objCaseDoc.Version);

                                //                    if (!Directory.Exists(directoryPath))
                                //                        Directory.CreateDirectory(directoryPath);

                                //                    Guid fileKey1 = Guid.NewGuid();
                                //                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                //                    Stream fs = uploadedFile.InputStream;
                                //                    BinaryReader br = new BinaryReader(fs);
                                //                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                //                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                //                    objCaseDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                //                    objCaseDoc.FileKey = fileKey1.ToString();
                                //                    objCaseDoc.VersionDate = DateTime.Now;
                                //                    objCaseDoc.CreatedOn = DateTime.Now;

                                //                    DocumentManagement.SaveDocFiles(fileList);
                                //                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objCaseDoc);

                                //                    fileList.Clear();
                                //                }

                                //            }//End For Each 

                                //            if (saveSuccess)
                                //            {
                                //                LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document(s) Uploaded", true);
                                //            }
                                //        }

                                //        if (saveSuccess)
                                //        {
                                //            BindCaseRelatedDocuments(Convert.ToInt32(NewCaseID));
                                //        }
                                //    }
                                //}

                                #endregion
                            }

                            if (saveSuccess)
                            {
                                UpdatedAssingedUser.Clear();
                                CheckAssingedUserNew.Clear();
                                #region Send Mail to managment, owner, assigned user
                                //CheckAssingedUserNew = CaseManagement.GetAllAssingedUserList(NewCaseID, AuthenticationHelper.UserID);
                                //if (CheckAssingedUserNew.Count > 0)
                                //{
                                //    if (CheckAssingedUserOld.Count > 0)
                                //    {
                                //        foreach (var item in CheckAssingedUserNew)
                                //        {
                                //            if (CheckAssingedUserOld.Contains(item))
                                //            {
                                //            }
                                //            else
                                //            {
                                //                UpdatedAssingedUser.Add(Convert.ToInt64(item));
                                //            }
                                //        }
                                //    }
                                //}
                                //if (UpdatedAssingedUser.Count > 0)
                                //{
                                //    List<long> OwnerMailID = new List<long>();
                                //    List<string> OwnerMailList = new List<string>();
                                //    User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                                //    List<string> AssignedUseerMailID = CaseManagement.GetAssignedUserAndOwnerMail(UpdatedAssingedUser);
                                //    if (ddlOwner.SelectedValue != "" && ddlOwner.SelectedValue != "-1")
                                //    {
                                //        OwnerMailID.Add(Convert.ToInt64(ddlOwner.SelectedValue));
                                //    }
                                //    OwnerMailList = CaseManagement.GetAssignedUserAndOwnerMail(OwnerMailID);
                                //    var caseRecord = CaseManagement.GetCaseByID(Convert.ToInt32(NewCaseID));
                                //    var Locations = string.Empty;
                                //    if (!string.IsNullOrEmpty(Convert.ToString(caseRecord.CustomerBranchID)))
                                //    {
                                //        Locations = CaseManagement.GetLocationByCaseInstanceID(caseRecord.CustomerBranchID);
                                //    }

                                //    List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                                //    List<string> UniqueMail = new List<string>();
                                //    if (OwnerMailList.Count > 0)
                                //    {
                                //        foreach (var item in OwnerMailList)
                                //        {
                                //            if (!UniqueMail.Contains(item))
                                //            {
                                //                UniqueMail.Add(item);
                                //            }
                                //        }
                                //    }
                                //    if (MgmUser.Count > 0)
                                //    {
                                //        foreach (var item in MgmUser)
                                //        {
                                //            if (!UniqueMail.Contains(item))
                                //            {
                                //                UniqueMail.Add(item);
                                //            }
                                //        }
                                //    }

                                //    string CaseTitleMerge = caseRecord.CaseTitle;
                                //    string FinalCaseTitle = string.Empty;
                                //    if (CaseTitleMerge.Length > 50)
                                //    {
                                //        FinalCaseTitle = CaseTitleMerge.Substring(0, 50);
                                //        FinalCaseTitle = FinalCaseTitle + "...";
                                //    }
                                //    else
                                //    {
                                //        FinalCaseTitle = CaseTitleMerge;
                                //    }

                                //    var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                //    if (AssignedUseerMailID.Count > 0)
                                //    {
                                //        string CasePriority = string.Empty;

                                //        if (NewCase.CaseRiskID != null)
                                //        {
                                //            if (NewCase.CaseRiskID == 1)
                                //                CasePriority = "High";
                                //            else if (NewCase.CaseRiskID == 2)
                                //                CasePriority = "Medium";
                                //            else if (NewCase.CaseRiskID == 3)
                                //                CasePriority = "Low";
                                //        }

                                //        string accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                //        string username = string.Format("{0} {1}", User.FirstName, User.LastName);

                                //        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_CaseAssignment
                                //                                .Replace("@User", username)
                                //                                .Replace("@CaseTitle", NewCase.CaseTitle)
                                //                                .Replace("@CaseDetailDesc", NewCase.CaseDetailDesc)
                                //                                .Replace("@Priority", CasePriority)
                                //                                .Replace("@AccessURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                //                                .Replace("@From", cname.Trim())
                                //                                .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(AssignedUseerMailID), UniqueMail, null, "Litigation Notification-Case Assigned", message);
                                //    }
                                //}
                                #endregion
                                cvCasePopUp.IsValid = false;
                                cvCasePopUp.ErrorMessage = "Case Details Updated Successfully.";
                                VSCasePopup.CssClass = "alert alert-success";
                                enableDisableCaseSummaryTabControls(false);

                                btnSave.Visible = true;
                                btnSave.Text = "Update";
                                btnClearCaseDetail.Visible = true;
                                showHideButtons(true);
                                BindCaseUserAssignments(NewCaseID);
                            }
                        }
                    }//Edit Code End
                    if (AuthenticationHelper.CustomerID == 76)
                    {
                        MisSummary.AddUpdateMisSummary(NewCase);
                    }
                }


                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }

        public static int GetCaseStageIDByName(string CaseStagename)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CaseStageID = (from row in entities.tbl_TypeMaster
                                   where row.TypeName.ToUpper().Trim() == CaseStagename.ToUpper().Trim()
                                   && row.IsActive == true && row.TypeCode == "CS"
                                   && row.customerID == AuthenticationHelper.CustomerID
                                   select row.ID).FirstOrDefault();

                return Convert.ToInt32(CaseStageID);
            }
        }
        protected void grdCaseDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdCaseDocuments.PageIndex = e.NewPageIndex;
                    BindCaseRelatedDocuments_All();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCaseDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadCaseDoc"))
                    {
                        DownloadCaseDocument(Convert.ToInt32(e.CommandArgument));
                    }
                    else if (e.CommandName.Equals("DeleteCaseDoc"))
                    {
                        DeleteCaseFile(Convert.ToInt32(e.CommandArgument));

                        //Bind Case Related Documents
                        if (ViewState["CaseInstanceID"] != null)
                        //BindCaseRelatedDocuments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                        {
                            BindCaseRelatedDocuments_All();
                            BindFileTags();
                        }
                    }
                    else if (e.CommandName.Equals("ViewCaseOrder"))
                    {
                        var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                    request.Key = AllinOneDocumentList.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                }
                                string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                DocumentPath = filePath1;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + DocumentPath + "');", true);
                                lblMessage.Text = "";                                
                                #endregion
                            }
                            else
                            {
                                #region Normal
                                string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                                if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }
                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (AllinOneDocumentList.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    DocumentPath = FileName;
                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + DocumentPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }
      

        protected void grdAdvocateBill_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (ViewState["CaseInstanceID"] != null)
                    {
                        int CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        int advocateBillID = Convert.ToInt32(e.CommandArgument);

                        ViewState["AdvocateBillID"] = advocateBillID;

                        if (e.CommandName.Equals("DeleteAdvocateBill"))
                        {
                            if (CaseInstanceID != 0 && advocateBillID != 0)
                            {
                                DeleteCaseAdvocateBill(CaseInstanceID, advocateBillID);
                                var lstadvDocument = CaseManagement.GetCaseAdvBillDocuments(advocateBillID, CaseInstanceID, "CA");
                                if (lstadvDocument.Count > 0)
                                {
                                    DeleteCaseAdvocateBillDocument(CaseInstanceID, advocateBillID);
                                }
                                   
                               
                                if (ViewState["CaseInstanceID"] != null)
                                {
                                    LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_CaseAdvocateBill", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Advocate Bill Deleted", true);
                                }
                                //Re-Bind AdvocateBill
                                BindAdvocateBill(Convert.ToInt32(ViewState["CaseInstanceID"]), AuthenticationHelper.CustomerID);
                            }
                        }
                        else if (e.CommandName.Equals("EditCaseAdvocateBill"))
                        {
                            if (CaseInstanceID != 0 && advocateBillID != 0)
                            {
                                var CaseAdvocatebill = GetCaseAdvocatebillDetailsByID(CaseInstanceID, advocateBillID);
                                if (CaseInstanceID != 0 && advocateBillID != 0)
                                {


                                    tbxAdvInvoiceno.Text = CaseAdvocatebill.InvoiceNo;
                                    tbxAdvInvoiceAmount.Text = Convert.ToString(CaseAdvocatebill.InvoiceAmount);
                                    ddlLawyerAdvocate.SelectedItem.Text = CaseAdvocatebill.Lawyer;
                                    if (CaseAdvocatebill.ApproverID1!=null)
                                    {
                                        ddlApprover1.SelectedValue = CaseAdvocatebill.ApproverID1.ToString();
                                    }
                                    if (CaseAdvocatebill.ApproverID2!=null)
                                    {
                                        ddlApprover2.SelectedValue = CaseAdvocatebill.ApproverID2.ToString();
                                    }
                                    if (CaseAdvocatebill.ApproverID3 != null)
                                    {
                                        ddlApprover3.SelectedValue = CaseAdvocatebill.ApproverID3.ToString();
                                    }
                                    tbxAdvRemark.Text = CaseAdvocatebill.Remark;
                                    tbxinvoicedate.Text = CaseAdvocatebill.InvoiceDate != null ? CaseAdvocatebill.InvoiceDate.Value.ToString("dd-MM-yyyy") : " ";
                                    txtcurrency.Text = CaseAdvocatebill.Currency;
                                    var data = CaseAdvocatebill.HearingRef;
                                  
                                    if(CaseAdvocatebill.InvoiceAmount >= 300000)
                                    {
                                        divApprover3.Visible = true;
                                        ddlApprover3.Visible = true;
                                    }
                                    else
                                    {
                                        divApprover3.Visible = false;
                                        ddlApprover3.Visible = false;
                                    }
                                    #region List of Hearingref
                                    var ListofHearingref = CaseManagement.GetListOfHearing(CaseAdvocatebill.NoticeCaseInstanceID, CaseAdvocatebill.ID);
                                    string[] values = data.Split(',');
                                    lstAdvHearingRefNo.ClearSelection();
                                    if (lstAdvHearingRefNo.Items.Count > 0)
                                    {
                                        if (!string.IsNullOrEmpty(ListofHearingref.ToString()))
                                        {
                                            foreach (var item in values)
                                            {
                                                if (lstAdvHearingRefNo.Items.FindByText(item.ToString()) != null)
                                                    lstAdvHearingRefNo.Items.FindByText(item.ToString()).Selected = true;
                                            }
                                        }
                                    }
                                    #endregion
                                 
                                    var lstadvDocument = CaseManagement.GetCaseAdvBillDocuments(advocateBillID,CaseInstanceID,"CA");
                                    if (lstadvDocument.Count > 0)
                                    {
                                        grdAdvocateBillDocuments.DataSource = lstadvDocument;
                                        grdAdvocateBillDocuments.DataBind();
                                        divAdveditBilldocument.Visible = true;
                                    }
                                    else
                                    {
                                        divAdveditBilldocument.Visible = false;
                                    }
                                    ViewState["Mode"] = 1;

                                    long creatoradvid= EnableAdvocatebill(advocateBillID);
                                    if (creatoradvid != AuthenticationHelper.UserID)
                                    {
                                        btnAdvocateBillSave.Enabled = false;
                                        btnAdvocateBillClear.Enabled = false;
                                    }
                                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script5", "HidShowAdvocateBillDivForEdit();", true);
                                }

                            }
                        }
                        else if (e.CommandName.Equals("DownloadCaseAdvocatebill"))
                        {
                            if (CaseInstanceID != 0 && advocateBillID != 0)
                            {
                                var lstCaseadvbillDocument = CaseManagement.GetCaseadvbillDocuments(CaseInstanceID, advocateBillID, "CA");

                                if (lstCaseadvbillDocument.Count > 0)
                                {

                                    #region Normal
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        foreach (var file in lstCaseadvbillDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                            }
                                        }
                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=AdvocateBillDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Advocate Bill Document(s) Downloaded", true);
                                    }
                                    #endregion

                                }
                                else
                                {
                                    cvCasePopUpTask.IsValid = false;
                                    cvCasePopUpTask.ErrorMessage = "No Document Available for Download.";
                                    ValidationSummary5.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                        }
                        else if (e.CommandName.Equals("ViewCaseAdvocatebill"))
                        {
                            var lstCaseAdvDocument = CaseManagement.GetCaseAdvBillDocuments(advocateBillID, CaseInstanceID, "CA");
                            if (lstCaseAdvDocument != null)
                            {

                                #region Normal

                                List<tbl_LitigationFileData> entitiesData = lstCaseAdvDocument.Where(entry => entry.Version != null).ToList();
                                if (lstCaseAdvDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                    entityData.Version = "1.0";
                                    entityData.NoticeCaseInstanceID = CaseInstanceID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in lstCaseAdvDocument)
                                    {
                                        rptAdvBilldocumentview.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptAdvBilldocumentview.DataBind();
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            DocumentPath = FileName;

                                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                            lblMessage.Text = "";

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenAdvBillDocviewer('" + DocumentPath + "');", true);
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenAdvBilldocfileReview();", true);
                                        }
                                        break;
                                    }
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                                #endregion
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator2.IsValid = false;
                CustomValidator2.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary13.CssClass = "alert alert-danger";
            }

        }
        protected void grdCaseDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                LinkButton lnkBtnDownLoadCaseDoc = (LinkButton)e.Row.FindControl("lnkBtnDownLoadCaseDoc");

                if (lnkBtnDownLoadCaseDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDownLoadCaseDoc);
                }

                LinkButton lnkBtnDeleteCaseDoc = (LinkButton)e.Row.FindControl("lnkBtnDeleteCaseDoc");
                if (lnkBtnDeleteCaseDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteCaseDoc);

                    if (ViewState["caseStatus"] != null)
                    {
                        if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                            lnkBtnDeleteCaseDoc.Visible = false;
                        else
                            lnkBtnDeleteCaseDoc.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                    {
                        lnkBtnDeleteCaseDoc.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DeleteCaseFile(int caseFileID)
        {
            try
            {
                if (caseFileID != 0)
                {
                    if (CaseManagement.DeleteCaseDocument(caseFileID, AuthenticationHelper.UserID))
                    {
                        cvCaseDocument.IsValid = false;
                        cvCaseDocument.ErrorMessage = "Document Deleted Successfully.";
                        vsCaseDocument.CssClass = "alert alert-success";
                        BindFileTags();
                        if (ViewState["CaseInstanceID"] != null)
                        {
                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document Deleted", true);
                        }
                    }
                    else
                    {
                        BindFileTags();
                        cvCaseDocument.IsValid = false;
                        cvCaseDocument.ErrorMessage = "Something went wrong, Please try again.";
                        vsCaseDocument.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }

        protected void btnAddCase_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                enableDisableCasePopUpControls(true);
                lnkActDetails.Visible = false;
                clearCaseControls();

                btnSave.Enabled = true;

                btnSave.Text = "Save";
                btnSave.Visible = true;

                btnClearCaseDetail.Visible = true;
                //btnEditCaseDetail.Visible = false;
                //lnkActDetails.Visible = false;
                //lnkSendMailWithDoc.Visible = false;
                //lnkLinkCase.Visible = false;
                divLinkedCases.Visible = false;

                //lblAddNewGround.Visible = false;

                grdCaseDocuments.DataSource = null;
                grdCaseDocuments.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditCase_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    if (caseInstanceID != 0)
                    {
                        ViewState["Mode"] = 1;
                        var caseRecord = CaseManagement.GetCaseByID(caseInstanceID);

                        #region Act
                        var ListofAct = CaseManagement.GetListOfAct(caseInstanceID, 1);
                        ddlAct.ClearSelection();
                        if (ddlAct.Items.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(ListofAct.ToString()))
                            {
                                foreach (var item in ListofAct)
                                {
                                    if (ddlAct.Items.FindByValue(item.ActID.ToString()) != null)
                                        ddlAct.Items.FindByValue(item.ActID.ToString()).Selected = true;
                                }
                            }
                        }
                        #endregion

                        #region Financial Year
                        var ListofFY = CaseManagement.GetListOfFY(caseInstanceID, 1);
                        DropDownListChosen1.ClearSelection();
                        if (DropDownListChosen1.Items.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(ListofFY.ToString()))
                            {
                                foreach (var item in ListofFY)
                                {
                                    if (DropDownListChosen1.Items.FindByValue(item.FYID.ToString()) != null)
                                        DropDownListChosen1.Items.FindByValue(item.FYID.ToString()).Selected = true;
                                }
                            }
                        }
                        #endregion

                        #region Party
                        var ListofParty = CaseManagement.GetListOfParty(caseInstanceID, 1);
                        ddlParty.ClearSelection();
                        if (ddlParty.Items.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(ListofParty.ToString()))
                            {
                                foreach (var item in ListofParty)
                                {
                                    if (ddlParty.Items.FindByValue(item.PartyID.ToString()) != null)
                                        ddlParty.Items.FindByValue(item.PartyID.ToString()).Selected = true;
                                }
                            }
                        }

                        #endregion

                        #region Opposition Lawyer
                        var ListofOppoLawyer = CaseManagement.GetListOfOppoLaywer(caseInstanceID, 1);
                        lstBoxOppositionLawyer.ClearSelection();
                        if (lstBoxOppositionLawyer.Items.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(ListofOppoLawyer.ToString()))
                            {
                                foreach (var item in ListofOppoLawyer)
                                {
                                    if (lstBoxOppositionLawyer.Items.FindByValue(item.LawyerID.ToString()) != null)
                                        lstBoxOppositionLawyer.Items.FindByValue(item.LawyerID.ToString()).Selected = true;
                                }
                            }
                        }
                        #endregion

                        if (caseRecord != null)
                        {
                            enableDisableCaseSummaryTabControls(false);

                            btnSave.Visible = true;
                            btnSave.Text = "Update";
                            btnClearCaseDetail.Visible = false;
                            btnEditCaseDetail.Visible = true;
                            lnkActDetails.Visible = true;
                            lnkSendMailWithDoc.Visible = true;

                            if (caseRecord.CaseType != null)
                            {
                                if (caseRecord.CaseType.ToString() == "I")
                                    rbCaseInOutType.SelectedValue = "I";
                                else if (caseRecord.CaseType.ToString() == "O")
                                    rbCaseInOutType.SelectedValue = "O";
                            }

                            if (caseRecord.OpenDate != null)
                                txtCaseDate.Text = Convert.ToDateTime(caseRecord.OpenDate).ToString("dd-MM-yyyy");
                            //if (IsNoticeDate == Convert.ToString(AuthenticationHelper.CustomerID))
                            //{
                                if (caseRecord.NoticeDate != null)
                               txtNoticeDate.Text = Convert.ToDateTime(caseRecord.NoticeDate).ToString("dd-MM-yyyy");

                            //}

                            tbxRefNo.Text = caseRecord.CaseRefNo;
                            txtCaseYear.Text = caseRecord.CaseYear;
                            txtBenchID.Text = caseRecord.BenchID;
                            txtStateID.Text = caseRecord.StateID;
                            txtIntrest.Text = (caseRecord.Intrest).ToString();

                            if (caseRecord.ProvisioninBook != null)
                                txtProvisonbook.Text = (caseRecord.ProvisioninBook).ToString();

                            txttaxDemand.Text = (caseRecord.Taxdemand).ToString();
                            txtamountpaid.Text = (caseRecord.amountpaid).ToString();
                            txtfavourable.Text = (caseRecord.favourable).ToString();
                            txtUnfavourable.Text = (caseRecord.unfavourable).ToString();
                            txtPenalty.Text = (caseRecord.Penalty).ToString();
                            ddlState.SelectedValue = (caseRecord.state).ToString();
                            ddlJurisdiction.SelectedValue = (caseRecord.Jurisdiction).ToString();
                            ddlFY.SelectedValue = caseRecord.Period;
                            tbxTitle.Text = caseRecord.CaseTitle;
                            tbxInternalCaseNo.Text = caseRecord.InternalCaseNo;
                            txtremark.Text = caseRecord.Remark;
                            tbxSection.Text = caseRecord.Section;
                            tbxCaseBudget.Text = Convert.ToString(caseRecord.CaseBudget);
                           // DropDownListChosen1.SelectedValue = Convert.ToString(caseRecord.FinancialYear);
                            //TextBox1.Text = Convert.ToDateTime(caseRecord.FinancialYear).ToString("dd-MM-yyyy");

                            // TextBox1.Text = Convert.ToString(FinancialYear);


                            if (caseRecord.CaseCategoryID != null)
                            {
                                ddlCaseCategory.ClearSelection();

                                if (ddlCaseCategory.Items.FindByValue(caseRecord.CaseCategoryID.ToString()) != null)
                                    ddlCaseCategory.SelectedValue = caseRecord.CaseCategoryID.ToString();
                            }

                            if (caseRecord.CourtID != null)
                            {
                                ddlCourt.ClearSelection();

                                if (ddlCourt.Items.FindByValue(caseRecord.CourtID.ToString()) != null)
                                    ddlCourt.SelectedValue = caseRecord.CourtID.ToString();
                            }

                            tbxJudge.Text = caseRecord.Judge;

                            tbxDescription.Text = caseRecord.CaseDetailDesc;

                            if (caseRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == caseRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == caseRecord.CustomerBranchID.ToString())
                                        {
                                            item1.Selected = true;
                                        }
                                        foreach (TreeNode item2 in item1.ChildNodes)
                                        {
                                            if (item2.Value == caseRecord.CustomerBranchID.ToString())
                                            {
                                                item2.Selected = true;
                                            }
                                        }
                                    }
                                        
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(caseRecord.DepartmentID.ToString()) != null)
                                ddlDepartment.SelectedValue = caseRecord.DepartmentID.ToString();

                            if (caseRecord.ContactPersonOfDepartment != null)
                            {
                                ddlCPDepartment.ClearSelection();

                                if (ddlCPDepartment.Items.FindByValue(caseRecord.ContactPersonOfDepartment.ToString()) != null)
                                    ddlCPDepartment.SelectedValue = caseRecord.ContactPersonOfDepartment.ToString();
                            }

                            if (caseRecord.OwnerID != null)
                            {
                                ddlOwner.ClearSelection();

                                if (ddlOwner.Items.FindByValue(caseRecord.OwnerID.ToString()) != null)
                                    ddlOwner.SelectedValue = caseRecord.OwnerID.ToString();
                            }

                            if (caseRecord.CaseRiskID != null)
                            {
                                ddlCaseRisk.ClearSelection();

                                if (ddlCaseRisk.Items.FindByValue(caseRecord.CaseRiskID.ToString()) != null)
                                    ddlCaseRisk.SelectedValue = caseRecord.CaseRiskID.ToString();
                            }

                            if (caseRecord.ClaimAmt != null)
                                tbxClaimedAmt.Text = caseRecord.ClaimAmt.ToString(); //Convert.ToDecimal(caseRecord.ClaimAmt).ToString();
                            else
                                tbxClaimedAmt.Text = "";

                            if (caseRecord.ProbableAmt != null)
                                tbxProbableAmt.Text = caseRecord.ProbableAmt.ToString(); // Convert.ToDecimal(caseRecord.ProbableAmt).ToString(); 
                            else
                                tbxProbableAmt.Text = "";

                            #region Edit bank gurantee,provisional and protest amt
                            if (caseRecord.ProtestMoney != null)
                                txtprotestmoney.Text = caseRecord.ProtestMoney.ToString();
                            else
                                txtprotestmoney.Text = "";

                            if (caseRecord.RecoveryAmount != null)
                                txtRecovery.Text = caseRecord.RecoveryAmount.ToString();
                            else
                                txtRecovery.Text = "";


                           

                            if (caseRecord.Provisionalamt != null)
                                txtprovisionalamt.Text = caseRecord.Provisionalamt.ToString();
                            else
                                txtprovisionalamt.Text = "";

                            if (caseRecord.BankGurantee != null)
                                txtbankgurantee.Text = caseRecord.BankGurantee;
                            else
                                txtbankgurantee.Text = "";
                            #endregion

                            //Potential Impact
                            if (caseRecord.ImpactType != null)
                            {
                                if (caseRecord.ImpactType.ToString() == "M")
                                    rblPotentialImpact.SelectedValue = "M";
                                else if (caseRecord.ImpactType.ToString() == "N")
                                    rblPotentialImpact.SelectedValue = "N";
                                else if (caseRecord.ImpactType.ToString() == "B")
                                    rblPotentialImpact.SelectedValue = "B";
                            }

                            if (caseRecord.Monetory != null)
                                tbxMonetory.Text = caseRecord.Monetory.ToString();
                            else
                                tbxMonetory.Text = "";

                            if (caseRecord.NonMonetory != null)
                                tbxNonMonetory.Text = caseRecord.NonMonetory.ToString();
                            else
                                tbxNonMonetory.Text = "";

                            if (caseRecord.Years != null)
                                tbxNonMonetoryYears.Text = caseRecord.Years.ToString();
                            else
                                tbxNonMonetoryYears.Text = "";

                            if (caseRecord.PreDeposit != null)
                                txtPreDeposit.Text = Convert.ToDecimal(caseRecord.PreDeposit).ToString();
                            else
                                txtPreDeposit.Text = "";

                            if (caseRecord.PostDeposit != null)
                                txtPostDeposit.Text = Convert.ToDecimal(caseRecord.PostDeposit).ToString();
                            else
                                txtPostDeposit.Text = "";

                            #region Lawyer Mapping
                            var lstCaseLawyer = CaseManagement.GetCaseLawyerMapping(caseInstanceID);

                            if (lstCaseLawyer != null)
                            {
                                if (ddlLawFirm.Items.FindByValue(lstCaseLawyer.LawyerID.ToString()) != null)
                                    ddlLawFirm.SelectedValue = Convert.ToString(lstCaseLawyer.LawyerID);
                            }

                            #endregion
                            if (caseRecord.RiskTypeID != null)
                            {
                                ddlRisk.ClearSelection();
                                if (ddlRisk.Items.FindByValue(caseRecord.RiskTypeID.ToString()) != null)
                                    ddlRisk.SelectedValue = caseRecord.RiskTypeID.ToString();
                            }

                            #region Assignment

                            BindCaseUserAssignments(caseInstanceID);

                            ddlLawFirm_SelectedIndexChanged(sender, e);
                            var lstCaseAssignment = CaseManagement.GetCaseAssignment(caseInstanceID);

                            if (lstCaseAssignment.Count > 0)
                            {
                                lstBoxPerformer.ClearSelection();
                                ddlReviewer.ClearSelection();
                                lstBoxLawyerUser.ClearSelection();

                                foreach (var eachAssignmentRecord in lstCaseAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                        if (lstBoxLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 4)
                                    {
                                        if (ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }

                            #endregion

                            //Case Stage
                            if (caseRecord.CaseStageID != null)
                            {
                                ddlCaseStage.ClearSelection();

                                if (ddlCaseStage.Items.FindByValue(caseRecord.CaseStageID.ToString()) != null)
                                    ddlCaseStage.SelectedValue = caseRecord.CaseStageID.ToString();
                            }

                            //Case Status 
                            var StatusDetails = CaseManagement.GetCaseStatusDetail(caseInstanceID);

                            if (StatusDetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.TxnStatusID)))
                                {
                                    ddlCaseStatus.ClearSelection();

                                    if (ddlCaseStatus.Items.FindByValue(StatusDetails.TxnStatusID.ToString()) != null)
                                        ddlCaseStatus.SelectedValue = StatusDetails.TxnStatusID.ToString();

                                    ViewState["caseStatus"] = Convert.ToInt32(StatusDetails.TxnStatusID);

                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.CloseDate)))
                                {
                                    tbxCaseCloseDate.Text = Convert.ToDateTime(StatusDetails.CloseDate).ToString("MM-dd-yyyy");
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.ClosureRemark)))
                                {
                                    tbxCloseRemark.Text = StatusDetails.ClosureRemark;
                                }
                            }

                            //Case Result
                            if (caseRecord.CaseResult != null)
                            {
                                ddlCaseResult.ClearSelection();

                                if (ddlCaseResult.Items.FindByValue(caseRecord.CaseResult.ToString()) != null)
                                    ddlCaseResult.SelectedValue = caseRecord.CaseResult.ToString();
                            }

                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "changeCaseStatus", "ddlStatusChange();", true);
                            //Case Status Log--End

                            //Bind Case Related Documents
                            //BindCaseRelatedDocuments(caseInstanceID);

                            //Bind Case History
                            BindCaseHistory(caseInstanceID);

                            //Bind Linked Case(s) if(Any)
                            BindLinkedCases(Convert.ToInt64(caseInstanceID));

                            //Bind Case To Link
                            BindCaseListToLink(Convert.ToInt64(caseInstanceID));

                            //Bind Case Hearing Details
                            BindCaseResponses(caseInstanceID);

                            //Bind Case Task Details
                            BindCaseTasks(caseInstanceID, grdTaskActivity);

                            //Bind Case Order Details
                            BindCaseOrders(caseInstanceID);

                            //Lawyer Rating
                            bool CheckUserForRating = false;
                            CheckUserForRating = CaseManagement.CheckUserIsInternal(AuthenticationHelper.UserID);

                            if (CheckUserForRating == false)
                            {
                                if (caseRecord.OwnerID == AuthenticationHelper.UserID)
                                {
                                    CheckUserForRating = true;
                                }
                            }

                            if (CheckUserForRating)
                            {
                                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                //{
                                //    string layerID = ddlLayerType.SelectedValue;
                                //    if (layerID !=null)
                                //    {
                                //        var documentData = (from row in entities.sp_LiDisplayLawyerRatingCase(caseInstanceID, AuthenticationHelper.UserID, 0)
                                //                            select row).ToList();

                                //        if (documentData != null)
                                //        {
                                //            grdLawyerRating.DataSource = documentData;
                                //            grdLawyerRating.DataBind();
                                //        }
                                //    }
                                //}
                            }

                            //Bind Case Payment Details
                            BindCasePayments(caseInstanceID);

                            //Bind Custom Field
                            if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && AuthenticationHelper.CustomerID != 76)
                            {
                                BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                            }

                            else
                            {
                                BindCustomFields(grdCustomField, grdCustomField_History);
                            }

                            BindMailDocumentList(caseInstanceID);
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DownloadCaseDocument(int caseFileID)
        {
            try
            {
                var file = CaseManagement.GetCaseDocumentByID(caseFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }
                            if (file != null)
                            {
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }

                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));

                                Response.BinaryWrite(DocumentManagement.ReadDocFiles(Server.MapPath(directoryPath) + "\\" + file.FileName)); // create the file
                                                                
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document Downloaded", false);
                                applyCSStoFileTag_ListItems();
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (filePath != null && File.Exists(filePath))
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document Downloaded", false);
                                applyCSStoFileTag_ListItems();
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }

        #endregion

        #region Case-Hearing

        public void BindCaseResponses(int caseInstanceID)
        {
            try
            {
                var lstCaseResponses = CaseManagement.GetCaseResponseDetails(caseInstanceID);
                lstCaseResponses = lstCaseResponses.OrderByDescending(entry => entry.HearingDate).ToList();

                grdResponseLog.DataSource = lstCaseResponses;
                grdResponseLog.DataBind();

                lstCaseResponses.Clear();
                lstCaseResponses = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpResponse.IsValid = false;
                cvCasePopUpResponse.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary1.CssClass = "alert alert-danger";
            }
        }

        protected void btnHearingClear_Click(object sender, EventArgs e)
        {
            try
            {
                clearResponseControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveHearing_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    long newResponseID = 0;
                    long caseInstanceID = 0;
                    int DocTypeID = -1;
                    caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    tbl_CaseHearingRef refNoDetail = null;

                    if (!String.IsNullOrEmpty(ddlTabHearingRef.SelectedValue) && ddlTabHearingRef.SelectedValue != "0" && ddlTabHearingRef.SelectedValue != "-1")
                    {
                        if (tbxResponseDesc.Text != "")
                        {
                            var refID = Convert.ToInt32(ddlTabHearingRef.SelectedValue);

                            if (refID != 0)
                            {
                                refNoDetail = CaseManagement.GetRefNoDetail(refID);

                                if (refNoDetail != null)
                                    if (refNoDetail.HearingDate != null)
                                        validateData = true;
                            }
                        }
                        else
                        {
                            cvCasePopUpResponse.IsValid = false;
                            cvCasePopUpResponse.ErrorMessage = "Provide Case Hearing Description.";
                            ValidationSummary1.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                    else
                    {
                        cvCasePopUpResponse.IsValid = false;
                        cvCasePopUpResponse.ErrorMessage = "Select Hearing.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                        return;
                    }

                    HttpFileCollection fileCollection = Request.Files;
                    bool isBlankFile = false;
                    if (fileCollection.Count > 0)
                    {
                        string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < InvalidFileTypes.Length; j++)
                                        {
                                            if (ext == "." + InvalidFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                    
                    if (validateData && isBlankFile == false)
                    {
                        tbl_LegalCaseResponse newRecord = new tbl_LegalCaseResponse()
                        {
                            IsActive = true,
                            CaseInstanceID = caseInstanceID,
                            Description = tbxResponseDesc.Text,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                        };
                        if (!string.IsNullOrEmpty(tbxReminderDate.Text.Trim()))
                        {
                            //if (tbxReminderDate.Text != "")
                            newRecord.ReminderDate = DateTimeExtensions.GetDate(tbxReminderDate.Text);
                        }
                        #region add reminder me on date
                        if (!string.IsNullOrEmpty(tbxRemindMeOn.Text.Trim()))
                        {
                            //if (tbxReminderDate.Text != "")
                            newRecord.ReminderMeONDate = DateTimeExtensions.GetDate(tbxRemindMeOn.Text);
                        }
                        #endregion
                        if (refNoDetail != null)
                        {
                            newRecord.RefID = refNoDetail.ID;

                            if (refNoDetail.HearingDate != null)
                            {
                                newRecord.ResponseDate = refNoDetail.HearingDate;
                            }
                        }

                        if (tbxResponseRemark.Text != "")
                            newRecord.Remark = tbxResponseRemark.Text;

                        if (txtHearingBudget.Text != "")
                            newRecord.HearingBudget = txtHearingBudget.Text;

                        if (DropDownListHearingLawFirm.SelectedValue != "0")
                            newRecord.HearingLawFirm= DropDownListHearingLawFirm.SelectedValue;


                        string AssignedUserList = string.Empty;

                        if (ListBoxLawyerHearing.Items.Count > 0)
                        {
                            foreach (System.Web.UI.WebControls.ListItem eachLawyerUser in ListBoxLawyerHearing.Items)
                            {
                                if (eachLawyerUser.Selected)
                                {
                                    AssignedUserList += eachLawyerUser.Value + ",";
                                }
                            }

                            newRecord.HearingLawyer = AssignedUserList.TrimEnd(',');
                        }

                        #region Mail Data
                        User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                        string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                        List<string> CaseOwnerandIUser = CaseManagement.getCaseOwnerAndInternalUser(Convert.ToInt32(caseInstanceID), AuthenticationHelper.CustomerID);
                        var caseRecord = CaseManagement.GetCaseByID(Convert.ToInt32(caseInstanceID));
                        var Locations = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(caseRecord.CustomerBranchID)))
                        {
                            Locations = CaseManagement.GetLocationByCaseInstanceID(caseRecord.CustomerBranchID);
                        }

                        List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                        List<string> ExcludedMailID = new List<string>();
                        List<string> UniqueMail = new List<string>();
                        if (CaseOwnerandIUser.Count > 0)
                        {
                            foreach (var item in CaseOwnerandIUser)
                            {
                                if (MgmUser.Count > 0)
                                {
                                    if (!MgmUser.Contains(item))
                                    {
                                        UniqueMail.Add(item);
                                    }
                                }
                                else
                                    UniqueMail.Add(item);
                            }                            
                        }
             
                        string CaseTitleMerge = caseRecord.CaseTitle;
                        string FinalCaseTitle = string.Empty;
                        if (CaseTitleMerge.Length > 50)
                        {
                            FinalCaseTitle = CaseTitleMerge.Substring(0, 50);
                            FinalCaseTitle = FinalCaseTitle + "...";
                        }
                        else
                        {
                            FinalCaseTitle = CaseTitleMerge;
                        }

                        #endregion end mail

                        string nextHearingDate = string.Empty;

                        if (!string.IsNullOrEmpty(tbxReminderDate.Text.Trim()))
                            nextHearingDate = DateTimeExtensions.GetDate(tbxReminderDate.Text).ToString("dd-MM-yyyy");

                        if (Convert.ToString(ViewState["HearingMode"]) == "Edit")
                        {
                            newRecord.ID = Convert.ToInt32(tbxResponseID.Text);
                            //if (IsAdvocateBillShow == Convert.ToString(AuthenticationHelper.CustomerID))
                            if (ShowAdvocateBillPayment == true)
                            {
                                string Gethearingname = LitigationManagement.GetHearingRef(Convert.ToInt32(newRecord.ID), Convert.ToInt32(caseInstanceID));
                                bool isCaseHearingExists = LitigationManagement.ExistsCaseHeringForAdvocateBill(Gethearingname, Convert.ToInt32(caseInstanceID));
                                if (!isCaseHearingExists)
                                {
                                    newResponseID = CaseManagement.UpdateCaseResponseLog(newRecord);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Unable to update Hearing as the Advocate Bill is generated against it.');", true);
                                }
                            }
                            else
                            {
                                newResponseID = CaseManagement.UpdateCaseResponseLog(newRecord);
                            }


                            #region Send Mail to managment after Hearing update

                            var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                            if (UniqueMail.Count > 0)
                            {
                                string accessURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    accessURL = Urloutput.URL;
                                }
                                else
                                {
                                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }

                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_HearingUpdatedToMgm
                                                      .Replace("@UpdatedUser", username)
                                                      .Replace("@CaseRefNo", caseRecord.CaseRefNo)
                                                      .Replace("@CaseTitle", caseRecord.CaseTitle)
                                                      .Replace("@Location", Locations)
                                                      .Replace("@HearingDate", (refNoDetail.HearingDate).ToString("dd-MM-yyyy"))
                                                      .Replace("@NextHearingDate", nextHearingDate)
                                                      .Replace("@HearingDescription", tbxResponseDesc.Text)
                                                      .Replace("@AccessURL", Convert.ToString(accessURL)) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                      .Replace("@From", cname.Trim())
                                                      .Replace("@PortalURL", Convert.ToString(accessURL));

                                ExcludedMailID = CaseManagement.Mailallowedornotcheck("Hearing Update", CustomerID);

                                if (ExcludedMailID.Count > 0)
                                {
                                    foreach (var item in UniqueMail.ToList())
                                    {
                                        if (ExcludedMailID.Contains(item))
                                        {
                                            UniqueMail.Remove(item);
                                            continue;
                                        }
                                    }

                                    if (MgmUser.Count > 0)
                                    {
                                        foreach (var item in MgmUser.ToList())
                                        {
                                            if (ExcludedMailID.Contains(item))
                                            {
                                                MgmUser.Remove(item);
                                                continue;
                                            }
                                        }
                                    }
                                }

                                try
                                {
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), MgmUser, null, "Litigation Hearing Update-Case Title-" + FinalCaseTitle, message);
                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }

                            }
                            #endregion

                            if (newResponseID > 0)
                                LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseResponse", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Detail Updated", true);

                            GrdHearingEditDocument.DataSource = null;
                            GrdHearingEditDocument.DataBind();
                            divHearingEditdoc.Visible = false;
                            ViewState["HearingMode"] = "Add";

                        }
                        else
                        {
                            newResponseID = CaseManagement.CreateCaseResponseLog(newRecord);

                            #region Mail send Create
                            var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (UniqueMail.Count > 0)
                            {
                                string accessURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    accessURL = Urloutput.URL;
                                }
                                else
                                {
                                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }

                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_HearingAddedToMgm
                                                     .Replace("@UpdatedUser", username)
                                                     .Replace("@CaseRefNo", caseRecord.CaseRefNo)
                                                     .Replace("@CaseTitle", caseRecord.CaseTitle)
                                                     .Replace("@Location", Locations)
                                                     .Replace("@HearingDate", (refNoDetail.HearingDate).ToString("dd-MM-yyyy"))
                                                     .Replace("@NextHearingDate", nextHearingDate)
                                                     .Replace("@HearingDescription", tbxResponseDesc.Text)
                                                     .Replace("@AccessURL", Convert.ToString(accessURL)) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                     .Replace("@From", cname.Trim())
                                                     .Replace("@PortalURL", Convert.ToString(accessURL));

                                ExcludedMailID = CaseManagement.Mailallowedornotcheck("On Add Hearing", CustomerID);

                                if (ExcludedMailID.Count > 0)
                                {
                                    foreach (var item in UniqueMail.ToList())
                                    {
                                        if (ExcludedMailID.Contains(item))
                                        {
                                            UniqueMail.Remove(item);
                                            continue;
                                        }

                                    }
                                    if (MgmUser.Count > 0)
                                    {
                                        foreach (var item in MgmUser.ToList())
                                        {
                                            if (ExcludedMailID.Contains(item))
                                            {
                                                MgmUser.Remove(item);
                                                continue;
                                            }
                                        }
                                    }
                                }

                                try
                                {
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), MgmUser, null, "Litigation Hearing Added-Case Title-" + FinalCaseTitle, message);
                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                                #endregion
                                if (newResponseID > 0)
                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseResponse", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Created", true);
                            }
                        }

                        if (newResponseID > 0)
                            saveSuccess = true;
                    }

                    else
                    {
                        cvCasePopUpResponse.IsValid = false;
                        cvCasePopUpResponse.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                    }

                    if (saveSuccess)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS Document

                            if (fuResponseDocUpload.HasFiles)
                            {
                                tbl_LitigationFileData objHearingDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = caseInstanceID,
                                    DocTypeInstanceID = newResponseID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "CH"
                                };

                                HttpFileCollection fileCollection1 = Request.Files;

                                if (fileCollection1.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                    int customerID = -1;
                                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                    string directoryPath = "";
                                    string fileName = "";

                                    if (newResponseID > 0)
                                    {
                                        for (int i = 0; i < fileCollection1.Count; i++)
                                        {
                                            HttpPostedFile uploadedFile = fileCollection1[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection1.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("fuResponseDocUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                //Get Document Version
                                                var caseHearingDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objHearingDoc);

                                                caseHearingDocVersion++;
                                                objHearingDoc.Version = caseHearingDocVersion + ".0";
                                                
                                                directoryPath = "LitigationDocuments\\" + customerID + "\\Cases\\" + Convert.ToInt32(caseInstanceID) + "\\"  + "Hearing\\" + objHearingDoc.Version;
                                                
                                                IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                if (!di.Exists)
                                                {
                                                    di.Create();
                                                }

                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                                string p_strPath = string.Empty;
                                                string dirpath = string.Empty;
                                                //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                                                //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                                dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;
                                                
                                                //if (File.Exists(p_strPath))
                                                //    File.Delete(p_strPath);

                                                if (!Directory.Exists(dirpath))
                                                {
                                                    Directory.CreateDirectory(dirpath);
                                                }
                                                FileStream objFileStrm = File.Create(p_strPath);
                                                objFileStrm.Close();
                                                File.WriteAllBytes(p_strPath, bytes);

                                                Guid fileKey1 = Guid.NewGuid();

                                                string AWSpath = directoryPath + "\\" + uploadedFile.FileName;

                                                objHearingDoc.FileName = fileName;
                                                objHearingDoc.FilePath = directoryPath.Replace(@"\", "/");
                                                objHearingDoc.FileKey = fileKey1.ToString();
                                                objHearingDoc.VersionDate = DateTime.Now;
                                                objHearingDoc.CreatedOn = DateTime.Now;
                                                objHearingDoc.FileSize = uploadedFile.ContentLength;

                                                FileInfo localFile = new FileInfo(p_strPath);
                                                S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                if (!s3File.Exists)
                                                {
                                                    using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                    {
                                                        localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                    }
                                                }
                                                //DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                                saveSuccess = CaseManagement.CreateCaseDocumentMapping(objHearingDoc);

                                                Filelist1.Clear();
                                            }
                                        }//End For Each   

                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Uploaded", true);
                                        }

                                    }
                                }
                            }

                            #endregion

                        }
                        else
                        {
                            #region Normal Document

                            if (fuResponseDocUpload.HasFiles)
                            {
                                tbl_LitigationFileData objHearingDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = caseInstanceID,
                                    DocTypeInstanceID = newResponseID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "CH"
                                };

                                HttpFileCollection fileCollection1 = Request.Files;

                                if (fileCollection1.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                    int customerID = -1;
                                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                    string directoryPath = "";
                                    string fileName = "";

                                    if (newResponseID > 0)
                                    {
                                        for (int i = 0; i < fileCollection1.Count; i++)
                                        {
                                            HttpPostedFile uploadedFile = fileCollection1[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection1.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("fuResponseDocUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                //Get Document Version
                                                var caseHearingDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objHearingDoc);

                                                caseHearingDocVersion++;
                                                objHearingDoc.Version = caseHearingDocVersion + ".0";

                                                directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(caseInstanceID) + "/Hearing/" + objHearingDoc.Version);

                                                if (!Directory.Exists(directoryPath))
                                                    Directory.CreateDirectory(directoryPath);

                                                Guid fileKey1 = Guid.NewGuid();
                                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                objHearingDoc.FileName = fileName;
                                                objHearingDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                objHearingDoc.FileKey = fileKey1.ToString();
                                                objHearingDoc.VersionDate = DateTime.Now;
                                                objHearingDoc.CreatedOn = DateTime.Now;
                                                objHearingDoc.FileSize = uploadedFile.ContentLength;
                                                DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                                saveSuccess = CaseManagement.CreateCaseDocumentMapping(objHearingDoc);

                                                Filelist1.Clear();
                                            }
                                        }//End For Each   

                                        if (saveSuccess)
                                        {
                                            LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Uploaded", true);
                                        }

                                    }
                                }
                            }

                            #endregion
                        }
                        //Update Status
                        #region Status Transaction

                        tbl_LegalCaseStatusTransaction newStatusRecord = new tbl_LegalCaseStatusTransaction()
                        {
                            CaseInstanceID = caseInstanceID,
                            StatusID = 2,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            IsDeleted = false,
                            UserID = AuthenticationHelper.UserID,
                            RoleID = 3,
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };

                        if (!CaseManagement.ExistCaseStatusTransaction(newStatusRecord))
                        {
                            saveSuccess = CaseManagement.DeActiveCaseStatusTransaction(newStatusRecord);
                            saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusRecord);
                        }

                        #endregion

                        if (saveSuccess)
                        {
                            clearResponseControls();
                            GrdHearingEditDocument.DataSource = null;
                            GrdHearingEditDocument.DataBind();
                            CvHearingSaveMsg.IsValid = false;
                            CvHearingSaveMsg.ErrorMessage = "Hearing Details Saved Successfully.";
                            ValidationSummary9.CssClass = "alert alert-success";
                        }

                        //Re-Bind Case Action Log Details
                        BindCaseResponses(Convert.ToInt32(ViewState["CaseInstanceID"]));

                        if (saveSuccess)
                        {
                            if (tbxReminderDate.Text != "")
                                generateRefNo(DateTimeExtensions.GetDate(tbxReminderDate.Text));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void clearResponseControls()
        {
            try
            {
                ddlTabHearingRef.SelectedValue = "0";
                tbxResponseDesc.Text = "";
                tbxResponseRemark.Text = "";
                tbxReminderDate.Text = "";
                tbxRemindMeOn.Text = "";
                txtHearingBudget.Text = "";

                DropDownListHearingLawFirm.ClearSelection();
                if (ddlLawFirm.SelectedValue != "0")
                {
                    DropDownListHearingLawFirm.SelectedValue = ddlLawFirm.SelectedValue;
                }
                DropDownListHearingLawFirm_SelectedIndexChanged(null, null);

                int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                var lstCaseAssignment = CaseManagement.GetCaseAssignment(caseInstanceID);

                if (lstCaseAssignment.Count > 0)
                {
                    ListBoxLawyerHearing.ClearSelection();

                    foreach (var eachAssignmentRecord in lstCaseAssignment)
                    {
                        if (ListBoxLawyerHearing.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                            ListBoxLawyerHearing.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                    }
                }

                fuResponseDocUpload.Attributes.Clear();
                GrdHearingEditDocument.DataSource = null;
                GrdHearingEditDocument.DataBind();
                divHearingEditdoc.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public bool DeleteCaseResponse(long caseInstanceID, int caseResponseID)
        {
            try
            {
                if (caseResponseID != 0)
                {
                    //Delete Hearing with Documents
                    if (CaseManagement.DeleteCaseResponseLog(caseInstanceID, caseResponseID, AuthenticationHelper.UserID))
                    {
                        LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseResponse", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Detail with Document(s) Deleted", true);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpResponse.IsValid = false;
                cvCasePopUpResponse.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary1.CssClass = "alert alert-danger";
                return false;
            }
        }

        protected void grdResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdResponseLog.PageIndex = e.NewPageIndex;

                    BindCaseResponses(Convert.ToInt32(ViewState["CaseInstanceID"]));

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);//HearingID
                        int CaseInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && CaseInstanceID != 0)
                        {
                            var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, responseID, "CH");

                            if (lstResponseDocument.Count > 0)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                        string directoryPath = "~/TempDocuments/AWS/" + User;

                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        foreach (var file in lstResponseDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                        }
                                        int i = 0;
                                        foreach (var file in lstResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                string str = filename[0] + i + "." + ext;

                                                responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));

                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        int i = 0;
                                        foreach (var file in lstResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                cvCasePopUpResponse.IsValid = false;
                                cvCasePopUpResponse.ErrorMessage = "No Document Available for Download.";
                                ValidationSummary1.CssClass = "alert alert-danger";
                                return;
                            }

                            //DownloadCaseDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                        }
                    }
                    else if (e.CommandName.Equals("EditCaseOrderHearing"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]); //HearingID
                        int CaseInstanceID = Convert.ToInt32(commandArgs[1]);
                        ViewState["responseID"] = responseID;

                        if (responseID != 0 && CaseInstanceID != 0)
                        {
                            var Caseresponse = CaseManagement.GetCaseResponseDetailsByID(CaseInstanceID, responseID);
                            if (Caseresponse != null)
                            {
                                tbxResponseID.Text = Convert.ToString(responseID);
                                ddlTabHearingRef.SelectedValue = Convert.ToString(Caseresponse.RefID);
                                tbxResponseDesc.Text = Caseresponse.Description;
                                if (Caseresponse.ReminderDate != null)
                                    tbxReminderDate.Text = Convert.ToDateTime(Caseresponse.ReminderDate).ToString("dd-MM-yyyy");
                                #region edit reminder me on date
                                if (Caseresponse.ReminderMeONDate != null)
                                    tbxRemindMeOn.Text = Convert.ToDateTime(Caseresponse.ReminderMeONDate).ToString("dd-MM-yyyy");

                                #endregion

                                tbxResponseRemark.Text = Caseresponse.Remark;

                                txtHearingBudget.Text = Caseresponse.HearingBudget;
                                
                                if (!string.IsNullOrEmpty(Caseresponse.HearingLawFirm))
                                {
                                    DropDownListHearingLawFirm.SelectedValue = Caseresponse.HearingLawFirm;
                                    DropDownListHearingLawFirm_SelectedIndexChanged(sender, e);
                                }

                                if (!string.IsNullOrEmpty(Caseresponse.HearingLawyer))
                                {
                                    ListBoxLawyerHearing.ClearSelection();

                                    var lstCaseAssignment = Caseresponse.HearingLawyer.Split(',');

                                    foreach (var eachAssignmentRecord in lstCaseAssignment)
                                    {
                                        if (ListBoxLawyerHearing.Items.FindByValue(eachAssignmentRecord.ToString()) != null)
                                            ListBoxLawyerHearing.Items.FindByValue(eachAssignmentRecord.ToString()).Selected = true;

                                    }
                                }

                                var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, responseID, "CH");
                                if (lstResponseDocument.Count > 0)
                                {
                                    GrdHearingEditDocument.DataSource = lstResponseDocument;
                                    GrdHearingEditDocument.DataBind();
                                    divHearingEditdoc.Visible = true;
                                }
                                ViewState["HearingMode"] = "Edit";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowHearingDivForEdit();", true);
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteResponse"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]); //HearingID
                        int CaseInstanceID = Convert.ToInt32(commandArgs[1]);
                        string Gethearingname = LitigationManagement.GetHearingRef(responseID, CaseInstanceID);
                        if (responseID != 0 && CaseInstanceID != 0)
                        {

                            //if (IsAdvocateBillShow == Convert.ToString(AuthenticationHelper.CustomerID))
                            if (ShowAdvocateBillPayment == true)
                            {
                                bool isCaseHearingExists = LitigationManagement.ExistsCaseHeringForAdvocateBill(Gethearingname, CaseInstanceID);
                                if (!isCaseHearingExists)
                                {
                                    var deleteSuccess = DeleteCaseResponse(CaseInstanceID, responseID); //Parameter - ResponseID and caseInstanceID

                                    if (deleteSuccess)
                                    {
                                        CvHearingSaveMsg.IsValid = false;
                                        CvHearingSaveMsg.ErrorMessage = "Hearing Details Deleted Successfully.";
                                        ValidationSummary1.CssClass = "alert alert-success";
                                    }
                                    else
                                    {
                                        cvCasePopUpResponse.IsValid = false;
                                        cvCasePopUpResponse.ErrorMessage = "Something went wrong, Please try again.";
                                        ValidationSummary1.CssClass = "alert alert-danger";
                                    }

                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Unable to delete Hearing as the Advocate Bill is generated against it.');", true);
                                }
                            }
                            else
                            {
                                var deleteSuccess = DeleteCaseResponse(CaseInstanceID, responseID); //Parameter - ResponseID and caseInstanceID

                            }


                            //Re-Bind Case Responses
                            if (ViewState["CaseInstanceID"] != null)
                            {
                                BindCaseResponses(Convert.ToInt32(ViewState["CaseInstanceID"]));
                            }

                            
                        }
                    }
                    else if (e.CommandName.Equals("ViewCaseOrderHearing"))
                    {

                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);//HearingID
                        int CaseInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && CaseInstanceID != 0)
                        {
                            var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, responseID, "CH");

                            if (lstResponseDocument != null)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS

                                    List<tbl_LitigationFileData> entitiesData = lstResponseDocument.Where(entry => entry.Version != null).ToList();
                                    if (lstResponseDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                        entityData.Version = "1.0";
                                        entityData.NoticeCaseInstanceID = CaseInstanceID;
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptDocmentVersionView.DataBind();

                                        foreach (var file in lstResponseDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }

                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {

                                                string extension = System.IO.Path.GetExtension(file.FileName);
                                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                                {
                                                    lblMessage.Text = "";
                                                    lblMessage.Text = "Zip file can't view please download it";
                                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                                }
                                                else
                                                {
                                                    string filePath1 = directoryPath + "/" + file.FileName;
                                                    DocumentPath = filePath1;
                                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                                    lblMessage.Text = "";
                                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                                }                                                
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                            }
                                            break;
                                        }
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal

                                    List<tbl_LitigationFileData> entitiesData = lstResponseDocument.Where(entry => entry.Version != null).ToList();
                                    if (lstResponseDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                        entityData.Version = "1.0";
                                        entityData.NoticeCaseInstanceID = CaseInstanceID;
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        foreach (var file in lstResponseDocument)
                                        {
                                            rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                            rptDocmentVersionView.DataBind();
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string Folder = "~/TempFiles";
                                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                                string DateFolder = Folder + "/" + File;

                                                string extension = System.IO.Path.GetExtension(filePath);

                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                DocumentPath = FileName;

                                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                                lblMessage.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                            }
                                            break;
                                        }
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }

        protected void grdResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadResponseDoc = (LinkButton)e.Row.FindControl("lnkBtnDownLoadResponseDoc");

            if (lnkBtnDownLoadResponseDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadResponseDoc);
            }

            LinkButton lnkBtnDeleteResponse = (LinkButton)e.Row.FindControl("lnkBtnDeleteResponse");
            if (lnkBtnDeleteResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteResponse);

                if (ViewState["caseStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                        lnkBtnDeleteResponse.Visible = false;
                    else
                        lnkBtnDeleteResponse.Visible = true;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                {
                    lnkBtnDeleteResponse.Visible = false;
                }
            }

            //Bind Response Related Tasks

            if (ViewState["CaseInstanceID"] != null)
            {
                long caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                if (caseInstanceID != 0)
                {
                    GridView grdResTaskActivity = e.Row.FindControl("grdResTaskActivity") as GridView;

                    HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;

                    if (grdResTaskActivity != null)
                    {
                        Label lblRefID = (Label)e.Row.FindControl("lblRefID");

                        if (lblRefID != null)
                        {
                            long refID = Convert.ToInt32(lblRefID.Text);

                            if (refID != 0)
                            {
                                var dataBindSuccess = BindCaseResponseTasks(caseInstanceID, refID, grdResTaskActivity);

                                if (imgCollapseExpand != null)
                                {
                                    if (dataBindSuccess)
                                        imgCollapseExpand.Visible = true;
                                    else
                                        imgCollapseExpand.Visible = false;
                                }
                            }
                        }
                    }

                }
            } //Bind Response Task - END
        }

        protected void grdResponseLog_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                //e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        protected void upResponseDocUpload_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "bindAddCollapse", "imgExpandCollapse();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ShowCaseResponseDocCount(long caseInstanceID, long noticeResponseID)
        {
            try
            {
                var docCount = CaseManagement.GetCaseResponseDocuments(caseInstanceID, noticeResponseID, "CH").Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }


        public string ShowCaseDocType(string docType)
        {
            try
            {
                if (!string.IsNullOrEmpty(docType))
                {
                    if (docType.Trim() == "C")
                        return "Case";
                    else if (docType.Trim() == "CH")
                        return "Hearing";
                    else if (docType.Trim() == "CT")
                        return "Task";
                    else if (docType.Trim() == "CO")
                        return "Order";
                    else
                        return "";
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public int ShowLawyerRating(decimal savedRating)
        {
            int returnRating = 0;
            try
            {
                returnRating = Convert.ToInt32(savedRating);
                return returnRating;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return returnRating;
            }
        }

        #endregion

        #region Case-Task-Activity
        public void clearTaskControls()
        {
            try
            {
                ddlHearingRefNo.ClearSelection();
                ddlTaskLawyerListInternal.ClearSelection();
                tbxTaskTitle.Text = "";
                tbxTaskDueDate.Text = "";
                tbxTaskDesc.Text = "";
                tbxTaskRemark.Text = "";
                ddlTaskPriority.ClearSelection();
                ddlTaskUserExternal.ClearSelection();
                tbxExpOutcome.Text = "";
                grdTaskEditDoc.DataSource = null;
                grdTaskEditDoc.DataBind();
                DivTaskEdit.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public void BindCaseTasks(int caseInstanceID, GridView grd)
        {
            try
            {
                var lstCaseTasks = LitigationTaskManagement.GetTaskDetails(caseInstanceID, "C");

                grd.DataSource = lstCaseTasks;
                grd.DataBind();

                lstCaseTasks.Clear();
                lstCaseTasks = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        public bool BindCaseResponseTasks(long caseInstanceID, long refID, GridView grd)
        {
            try
            {
                var lstResponseTasks = LitigationTaskManagement.GetResponseTaskDetails(caseInstanceID, refID, "C");

                if (lstResponseTasks != null && lstResponseTasks.Count > 0)
                {
                    grd.DataSource = lstResponseTasks;
                    grd.DataBind();

                    lstResponseTasks.Clear();
                    lstResponseTasks = null;

                    return true;
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();

                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpResponse.IsValid = false;
                cvCasePopUpResponse.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary1.CssClass = "alert alert-danger";
                return false;
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnEditTaskDoc = (LinkButton)e.Row.FindControl("lnkBtnEditTaskDoc");
                LinkButton lnkBtnResCloseTask = (LinkButton)e.Row.FindControl("lnkBtnResCloseTask");
                LinkButton lnkBtnResDeleteTask = (LinkButton)e.Row.FindControl("lnkBtnResDeleteTask");
                LinkButton lnkBtnResTaskReminder = (LinkButton)e.Row.FindControl("lnkBtnResTaskReminder");

                if (lnkBtnEditTaskDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnEditTaskDoc);
                }

                LinkButton lnkBtnTaskResponse = (LinkButton)e.Row.FindControl("lnkBtnTaskResponse");

                if (lnkBtnTaskResponse != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskResponse);
                }

                LinkButton lnkBtnDeleteTask = (LinkButton)e.Row.FindControl("lnkBtnDeleteTask");

                if (lnkBtnDeleteTask != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteTask);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                {
                    if (lnkBtnResCloseTask != null)
                    {
                        lnkBtnResCloseTask.Visible = false;
                    }
                    if (lnkBtnResDeleteTask != null)
                    {
                        lnkBtnResDeleteTask.Visible = false;
                    }
                    if (lnkBtnResTaskReminder != null)
                    {
                        lnkBtnResTaskReminder.Visible = false;
                    }
                    //if (lnkBtnEditTaskDoc != null)
                    //{
                    //    lnkBtnEditTaskDoc.Visible = false;
                    //}
                }

                Label lblTaskStatus = e.Row.FindControl("lblTaskStatus") as Label;

                HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;

                if (lblTaskStatus != null)
                {
                    if (lblTaskStatus.Text != "" && lblTaskStatus.Text != "Open")
                    {
                        GridView gvTaskResponses = e.Row.FindControl("gvTaskResponses") as GridView;

                        if (gvTaskResponses != null)
                        {
                            if (grdTaskActivity.DataKeys.Count > 0)
                            {
                                if (grdTaskActivity.DataKeys[e.Row.RowIndex].Value != null)
                                {
                                    int taskID = 0;
                                    taskID = Convert.ToInt32(grdTaskActivity.DataKeys[e.Row.RowIndex].Value);

                                    var dataBindSuccess = BindTaskResponses(taskID, gvTaskResponses);

                                    if (imgCollapseExpand != null)
                                        if (dataBindSuccess)
                                            imgCollapseExpand.Visible = true;
                                        else
                                            imgCollapseExpand.Visible = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (imgCollapseExpand != null)
                            imgCollapseExpand.Visible = false;
                    }

                    //Hide Close Task Button
                    LinkButton lnkBtnCloseTask = e.Row.FindControl("lnkBtnCloseTask") as LinkButton;
                    if (lnkBtnCloseTask != null)
                    {
                        if (lblTaskStatus.Text != "" && lblTaskStatus.Text == "Closed")
                            lnkBtnCloseTask.Visible = false;
                        else
                            lnkBtnCloseTask.Visible = true;
                    }
                }
            }
        }

        protected void grdTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdTaskActivity.PageIndex = e.NewPageIndex;

                    //Re-Bind Case Related Tasks
                    BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdResTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    GridView grdResTaskActivity = (sender as GridView);

                    if (grdResTaskActivity != null)
                    {
                        grdResTaskActivity.PageIndex = e.NewPageIndex;

                        //Re-Bind Hearing Related Tasks
                        BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdResTaskActivity);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (ViewState["CaseInstanceID"] != null)
                    {
                        int CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);


                        if (e.CommandName.Equals("DeleteTask"))
                        {
                            int taskID = Convert.ToInt32(e.CommandArgument);
                            DeleteTask(taskID, AuthenticationHelper.CustomerID);

                            //Re-Bind Case Task
                            BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);
                        }
                        else if (e.CommandName.Equals("EditTaskDoc"))
                        {
                            int taskID = Convert.ToInt32(e.CommandArgument);
                            ViewState["taskID"] = taskID;

                            if (taskID != 0)
                            {
                                var Caseresponse = NoticeManagement.GetNoticeTaskDetailByTaskID(CaseInstanceID, taskID, "C", AuthenticationHelper.CustomerID);
                                if (Caseresponse != null)
                                {
                                    tbxTaskDueDate.Text = Convert.ToString(Caseresponse.ScheduleOnDate);
                                    tbxTaskTitle.Text = Caseresponse.TaskTitle;
                                    tbxTaskDesc.Text = Caseresponse.TaskDesc;
                                    tbxExpOutcome.Text = Caseresponse.ExpOutcome;
                                    ddlTaskPriority.SelectedValue = Convert.ToString(Caseresponse.PriorityID);
                                    tbxTaskRemark.Text = Caseresponse.Remark;
                                    tbxTaskID.Text = Convert.ToString(Caseresponse.ID);
                                    ddlHearingRefNo.SelectedValue = Convert.ToString(Caseresponse.RefID);
                                    var FindIntText = ddlTaskLawyerListInternal.Items.FindByValue(Convert.ToString(Caseresponse.AssignTo));
                                    var FindExtText = ddlTaskUserExternal.Items.FindByValue(Convert.ToString(Caseresponse.AssignTo));
                                    if (FindIntText != null)
                                    {
                                        ddlTaskLawyerListInternal.SelectedValue = Convert.ToString(Caseresponse.AssignTo);
                                    }
                                    if (FindExtText != null)
                                    {
                                        ddlTaskUserExternal.SelectedValue = Convert.ToString(Caseresponse.AssignTo);
                                    }


                                    var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, taskID, "CT");
                                    if (lstResponseDocument.Count > 0)
                                    {
                                        grdTaskEditDoc.DataSource = lstResponseDocument;
                                        grdTaskEditDoc.DataBind();
                                        DivTaskEdit.Visible = true;
                                        ViewState["TaskMode"] = "Edit";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                                    }
                                }
                            }
                        }
                        else if (e.CommandName.Equals("CloseTask"))
                        {
                            int taskID = Convert.ToInt32(e.CommandArgument);
                            //Update Task Status to Closed and Expire URL
                            LitigationTaskManagement.UpdateTaskStatus(taskID, 3, AuthenticationHelper.UserID, AuthenticationHelper.CustomerID); //Status 3 - Closed

                            //Re-Bind Case Task
                            BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);
                        }
                        else if (e.CommandName.Equals("TaskReminder")) //Send Reminder or Re-generate URL
                        {
                            int taskID = Convert.ToInt32(e.CommandArgument);
                            if (taskID != 0)
                            {
                                string accessURL = string.Empty;
                                bool sendSuccess = false;

                                string PortalURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    PortalURL = Urloutput.URL;
                                }
                                else
                                {
                                    PortalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }

                                //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                                accessURL = Convert.ToString(PortalURL) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                    CryptographyManagement.Encrypt(taskID.ToString()) +
                                    "&NID=" + CryptographyManagement.Encrypt(CaseInstanceID.ToString());

                                ////Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                                //accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                //    CryptographyManagement.Encrypt(taskID.ToString()) +
                                //    "&NID=" + CryptographyManagement.Encrypt(CaseInstanceID.ToString());

                                //Get Task Record
                                var taskRecord = LitigationTaskManagement.GetTaskDetailByTaskID(CaseInstanceID, taskID, "C", AuthenticationHelper.CustomerID);

                                if (taskRecord != null)
                                {
                                    sendSuccess = SendTaskAssignmentMail(taskRecord, accessURL, AuthenticationHelper.User);

                                    if (sendSuccess)
                                    {
                                        cvCasePopUpTask.ErrorMessage = "An Email containing task detail and access URL to provide response sent to assignee.";
                                        ValidationSummary5.CssClass = "alert alert-danger";
                                        taskRecord.AccessURL = accessURL;
                                        taskRecord.UpdatedBy = AuthenticationHelper.UserID;
                                        sendSuccess = LitigationTaskManagement.UpdateTaskAccessURL(taskRecord.ID, taskRecord);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        public void DeleteTask(int taskID, long CustomerID)
        {
            try
            {
                if (taskID != 0)
                {
                    if (LitigationTaskManagement.DeleteTask(taskID, AuthenticationHelper.UserID, CustomerID))
                    {
                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_TaskScheduleOn", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Deleted", true);
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Task Details Deleted Successfully.";
                        ValidationSummary5.CssClass = "alert alert-success";
                    }
                    else
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        public bool SendTaskAssignmentMail(tbl_TaskScheduleOn taskRecord, string accessURL, string assignedBy)
        {
            try
            {
                List<string> OwnerMailList = new List<string>();
                List<string> ExcludedMailID = new List<string>();
                User NoticeOwner = null;
                User TaskAssignedUserDetail = null;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                #region Mail Data
                User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                var CaseRecord = CaseManagement.GetCaseByID(Convert.ToInt32(taskRecord.NoticeCaseInstanceID));

                if (!string.IsNullOrEmpty(Convert.ToString(CaseRecord.OwnerID)))
                {
                    NoticeOwner = UserManagement.GetByID(Convert.ToInt32(CaseRecord.OwnerID));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(taskRecord.AssignTo)))
                {
                    TaskAssignedUserDetail = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));
                }
                var Locations = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(CaseRecord.CustomerBranchID)))
                {
                    Locations = CaseManagement.GetLocationByCaseInstanceID(CaseRecord.CustomerBranchID);
                }
                OwnerMailList.Add(NoticeOwner.Email);
                List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                List<string> UniqueMail = new List<string>();
                if (OwnerMailList.Count > 0)
                {
                    foreach (var item in OwnerMailList)
                    {
                        if (!UniqueMail.Contains(item))
                        {
                            UniqueMail.Add(item);
                        }
                    }
                }
                if (MgmUser.Count > 0)
                {
                    foreach (var item in MgmUser)
                    {
                        if (!UniqueMail.Contains(item))
                        {
                            UniqueMail.Add(item);
                        }
                    }
                }

                string CaseTitleMerge = CaseRecord.CaseTitle;
                string FinalNoticeTitle = string.Empty;
                if (CaseTitleMerge.Length > 50)
                {
                    FinalNoticeTitle = CaseTitleMerge.Substring(0, 50);
                    FinalNoticeTitle = FinalNoticeTitle + "...";
                }
                else
                {
                    FinalNoticeTitle = CaseTitleMerge;
                }

                #endregion end mail

                if (taskRecord != null)
                {
                    User UserAssigeed = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));
                    var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                    if (UserAssigeed != null)
                    {
                        if (UserAssigeed.Email != null && UserAssigeed.Email != "")
                        {
                            string assignedToUserName = string.Format("{0} {1}", UserAssigeed.FirstName, UserAssigeed.LastName);
                            string PortalURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                PortalURL = Urloutput.URL;
                            }
                            else
                            {
                                PortalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_CaseTaskAssignment
                                                                       .Replace("@User", username)
                                                                       .Replace("@CaseRefNo", CaseRecord.CaseRefNo)
                                                                       .Replace("@CaseTitle", CaseRecord.CaseTitle)
                                                                       .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                                       .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                                       .Replace("@Location", Locations)
                                                                       .Replace("@AssignedBy", assignedToUserName)
                                                                       .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                                                                       .Replace("@From", cname.Trim())
                                                                       .Replace("@PortalURL", Convert.ToString(PortalURL));

                            ExcludedMailID = CaseManagement.Mailallowedornotcheck("Task Assigned", CustomerID);

                            if (ExcludedMailID.Count > 0)
                            {
                                foreach (var item in UniqueMail.ToList())
                                {
                                    if (ExcludedMailID.Contains(item))
                                    {
                                        UniqueMail.Remove(item);
                                        continue;
                                    }
                                }
                            }
                            try
                            {
                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { TaskAssignedUserDetail.Email }), UniqueMail, null, "Litigation Notification Task Assigned - " + FinalNoticeTitle, message);
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        protected void btnTaskSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    bool isBlankFile = false;
                    int AssignedToUser = -1;
                    int DocTypeID = -1;
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < InvalidFileTypes.Length; j++)
                                        {
                                            if (ext == "." + InvalidFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }


                    if (!String.IsNullOrEmpty(ddlHearingRefNo.SelectedValue) && ddlHearingRefNo.SelectedValue != "0")
                    {
                        if (tbxTaskTitle.Text != "")
                        {
                            if (tbxTaskDueDate.Text != "")
                            {
                                if ((!String.IsNullOrEmpty(ddlTaskLawyerListInternal.SelectedValue) && ddlTaskLawyerListInternal.SelectedValue != "0") ||
                                    (!String.IsNullOrEmpty(ddlTaskUserExternal.SelectedValue) && ddlTaskUserExternal.SelectedValue != "0"))
                                {
                                    if (!String.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                                    {
                                        if (tbxTaskDesc.Text != "")
                                        {
                                            if (isBlankFile == false)
                                            {
                                                validateData = true;
                                                if (!String.IsNullOrEmpty(ddlTaskLawyerListInternal.SelectedValue) && ddlTaskLawyerListInternal.SelectedValue != "0")
                                                    AssignedToUser = Convert.ToInt32(ddlTaskLawyerListInternal.SelectedValue);
                                                else if (!String.IsNullOrEmpty(ddlTaskUserExternal.SelectedValue) && ddlTaskUserExternal.SelectedValue != "0")
                                                    AssignedToUser = Convert.ToInt32(ddlTaskLawyerListInternal.SelectedValue);
                                            }
                                            else
                                            {
                                                cvCasePopUpTask.IsValid = false;
                                                cvCasePopUpTask.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                                                ValidationSummary5.CssClass = "alert alert-danger";
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            cvCasePopUpTask.IsValid = false;
                                            cvCasePopUpTask.ErrorMessage = "Provide Task Description.";
                                            ValidationSummary5.CssClass = "alert alert-danger";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        cvCasePopUpTask.IsValid = false;
                                        cvCasePopUpTask.ErrorMessage = "Select Task Priority.";
                                        ValidationSummary5.CssClass = "alert alert-danger";
                                        return;
                                    }
                                }
                                else
                                {
                                    cvCasePopUpTask.IsValid = false;
                                    cvCasePopUpTask.ErrorMessage = "Please Select at least one User either Internal or Lawyer/External for task assignment.";
                                    ValidationSummary5.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvCasePopUpTask.IsValid = false;
                                cvCasePopUpTask.ErrorMessage = "Provide Task Due Date.";
                                ValidationSummary5.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                        else
                        {
                            cvCasePopUpTask.IsValid = false;
                            cvCasePopUpTask.ErrorMessage = "Provide Task Title.";
                            ValidationSummary5.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                    else
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Select Hearing or if you do not want to map task with hearing, then please select 'Not Applicable'.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                        //return;
                    }

                    tbl_TaskScheduleOn newRecord = new tbl_TaskScheduleOn();


                    if (validateData)
                    {
                        newRecord.IsActive = true;
                        newRecord.TaskType = "C";
                        newRecord.NoticeCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        newRecord.ScheduleOnDate = DateTimeExtensions.GetDate(tbxTaskDueDate.Text);
                        newRecord.TaskTitle = tbxTaskTitle.Text.Trim();
                        newRecord.TaskDesc = tbxTaskDesc.Text.Trim();
                        newRecord.ExpOutcome = tbxExpOutcome.Text;
                        newRecord.StatusID = 1;
                        newRecord.CustomerID = customerID;
                        newRecord.CreatedBy = AuthenticationHelper.UserID;
                        newRecord.CreatedByText = AuthenticationHelper.User;
                        newRecord.LinkCreatedOn = DateTime.Now;
                        newRecord.URLExpired = false;

                        if (!String.IsNullOrEmpty(ddlHearingRefNo.SelectedValue) && ddlHearingRefNo.SelectedValue != "0")
                            newRecord.RefID = Convert.ToInt32(ddlHearingRefNo.SelectedValue);

                        if (AssignedToUser != -1)
                            newRecord.AssignTo = AssignedToUser;

                        if (!String.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                            newRecord.PriorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);

                        if (tbxTaskRemark.Text != "")
                            newRecord.Remark = tbxTaskRemark.Text.Trim();

                        if (Convert.ToString(ViewState["TaskMode"]) == "Edit")
                        {
                            newRecord.ID = Convert.ToInt32(tbxTaskID.Text);
                            saveSuccess = LitigationTaskManagement.UpdateTask(newRecord);
                            grdTaskEditDoc.DataSource = null;
                            grdTaskEditDoc.DataBind();
                            DivTaskEdit.Visible = false;
                        }
                        else
                        {
                            if (!LitigationTaskManagement.ExistNoticeCaseTaskTitle(tbxTaskTitle.Text.Trim(), "C", (long)newRecord.NoticeCaseInstanceID, customerID))
                            {
                                saveSuccess = LitigationTaskManagement.CreateTask(newRecord);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Task Created", true);
                                }
                            }
                            else
                            {
                                saveSuccess = false;
                                CvTaskSaveMsg.IsValid = false;
                                CvTaskSaveMsg.ErrorMessage = "Task with same title already exists.";
                                ValidationSummary8.CssClass = "alert alert-danger";
                                tbxTaskTitle.Focus();
                                return;
                            }
                        }

                        if (saveSuccess)
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (AWSData != null)
                            {
                                //Save Task Related Uploaded Documents
                                #region AWS Upload Document

                                if (fuTaskDocUpload.HasFiles)
                                {
                                    tbl_LitigationFileData objTaskDoc = new tbl_LitigationFileData()
                                    {
                                        NoticeCaseInstanceID = newRecord.NoticeCaseInstanceID,
                                        DocTypeInstanceID = newRecord.ID, //TaskID
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedByText = AuthenticationHelper.User,
                                        IsDeleted = false,
                                        DocTypeID = DocTypeID,
                                        DocType = "CT"
                                    };

                                    HttpFileCollection fileCollection1 = Request.Files;

                                    if (fileCollection1.Count > 0)
                                    {
                                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                        string directoryPath = "";
                                        string fileName = "";

                                        if (newRecord.ID > 0)
                                        {
                                            for (int i = 0; i < fileCollection1.Count; i++)
                                            {
                                                HttpPostedFile uploadedFile = fileCollection1[i];

                                                if (uploadedFile.ContentLength > 0)
                                                {
                                                    string[] keys1 = fileCollection1.Keys[i].Split('$');

                                                    if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                    {
                                                        fileName = uploadedFile.FileName;
                                                    }
                                                    objTaskDoc.FileName = fileName;
                                                    //Get Document Version
                                                    var caseTaskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objTaskDoc);

                                                    caseTaskDocVersion++;
                                                    objTaskDoc.Version = caseTaskDocVersion + ".0";

                                                    directoryPath = "LitigationDocuments\\" + customerID + "\\Cases\\" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "\\Task\\" + objTaskDoc.Version;
                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                    string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                                    string p_strPath = string.Empty;
                                                    string dirpath = string.Empty;
                                                    //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                                                    //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;
                                                    //if (File.Exists(p_strPath))
                                                    //    File.Delete(p_strPath);

                                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                    p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                                    dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;


                                                    if (!Directory.Exists(dirpath))
                                                    {
                                                        Directory.CreateDirectory(dirpath);
                                                    }
                                                    FileStream objFileStrm = File.Create(p_strPath);
                                                    objFileStrm.Close();
                                                    File.WriteAllBytes(p_strPath, bytes);

                                                    ////directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "/Task/" + objTaskDoc.Version);
                                                    ////if (!Directory.Exists(directoryPath))
                                                    ////    Directory.CreateDirectory(directoryPath);
                                                    ////Guid fileKey1 = Guid.NewGuid();

                                                    //string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                    //Stream fs = uploadedFile.InputStream;
                                                    //BinaryReader br = new BinaryReader(fs);
                                                    //Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                    //Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                                    Guid fileKey1 = Guid.NewGuid();
                                                    string AWSpath = directoryPath + "\\" + uploadedFile.FileName;

                                                    objTaskDoc.FileName = fileName;
                                                    objTaskDoc.FilePath = directoryPath.Replace(@"\", "/");
                                                    objTaskDoc.FileKey = fileKey1.ToString();
                                                    objTaskDoc.VersionDate = DateTime.Now;
                                                    objTaskDoc.CreatedOn = DateTime.Now;
                                                    objTaskDoc.FileSize = uploadedFile.ContentLength;

                                                    FileInfo localFile = new FileInfo(p_strPath);
                                                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                    if (!di.Exists)
                                                    {
                                                        di.Create();
                                                    }
                                                    S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                    if (!s3File.Exists)
                                                    {
                                                        using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                        {
                                                            localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                        }
                                                    }
                                                    //DocumentManagement.Litigation_SaveDocFiles(Filelist1);

                                                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objTaskDoc);

                                                    Filelist1.Clear();
                                                }
                                            }
                                            //End For Each      
                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Document(s) Uploaded.", true);
                                            }

                                        }
                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                #region Upload Document

                                if (fuTaskDocUpload.HasFiles)
                                {
                                    tbl_LitigationFileData objTaskDoc = new tbl_LitigationFileData()
                                    {
                                        NoticeCaseInstanceID = newRecord.NoticeCaseInstanceID,
                                        DocTypeInstanceID = newRecord.ID, //TaskID
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedByText = AuthenticationHelper.User,
                                        IsDeleted = false,
                                        DocTypeID = DocTypeID,
                                        DocType = "CT"
                                    };

                                    HttpFileCollection fileCollection1 = Request.Files;

                                    if (fileCollection1.Count > 0)
                                    {
                                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                        string directoryPath = "";
                                        string fileName = "";

                                        if (newRecord.ID > 0)
                                        {
                                            for (int i = 0; i < fileCollection1.Count; i++)
                                            {
                                                HttpPostedFile uploadedFile = fileCollection1[i];

                                                if (uploadedFile.ContentLength > 0)
                                                {
                                                    string[] keys1 = fileCollection1.Keys[i].Split('$');

                                                    if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                    {
                                                        fileName = uploadedFile.FileName;
                                                    }

                                                    //Get Document Version
                                                    var caseTaskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objTaskDoc);

                                                    caseTaskDocVersion++;
                                                    objTaskDoc.Version = caseTaskDocVersion + ".0";

                                                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "/Task/" + objTaskDoc.Version);

                                                    if (!Directory.Exists(directoryPath))
                                                        Directory.CreateDirectory(directoryPath);

                                                    Guid fileKey1 = Guid.NewGuid();
                                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                    objTaskDoc.FileName = fileName;
                                                    objTaskDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                    objTaskDoc.FileKey = fileKey1.ToString();
                                                    objTaskDoc.VersionDate = DateTime.Now;
                                                    objTaskDoc.CreatedOn = DateTime.Now;
                                                    objTaskDoc.FileSize = uploadedFile.ContentLength;
                                                    DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objTaskDoc);

                                                    Filelist1.Clear();
                                                }
                                            }
                                            //End For Each      
                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_TaskScheduleOn", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Document(s) Uploaded.", true);
                                            }

                                        }
                                    }
                                }

                                #endregion
                            }
                        }
                    }
                    
                    if (saveSuccess)
                    {
                        string accessURL = string.Empty;
                        string PortalURL = string.Empty;
                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (Urloutput != null)
                        {
                            PortalURL = Urloutput.URL;
                        }
                        else
                        {
                            PortalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }


                        CvTaskSaveMsg.IsValid = false;
                        CvTaskSaveMsg.ErrorMessage = "Task Saved Successfully.";
                        ValidationSummary8.CssClass = "alert alert-success";
                        //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                        if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(newRecord.AssignTo), customerID))
                            accessURL = Convert.ToString(PortalURL) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                          CryptographyManagement.Encrypt(newRecord.ID.ToString()) +
                          "&NID=" + CryptographyManagement.Encrypt(newRecord.NoticeCaseInstanceID.ToString());
                        else
                            accessURL = Convert.ToString(PortalURL);
                        //if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(newRecord.AssignTo), customerID))
                        //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                        //  CryptographyManagement.Encrypt(newRecord.ID.ToString()) +
                        //  "&NID=" + CryptographyManagement.Encrypt(newRecord.NoticeCaseInstanceID.ToString());
                        //else
                        //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);

                        saveSuccess = SendTaskAssignmentMail(newRecord, accessURL, AuthenticationHelper.User);

                        if (saveSuccess)
                        {
                            CvTaskSaveMsg.IsValid = false;
                            CvTaskSaveMsg.ErrorMessage = "Task Saved Successfully. An Email containing task detail and access URL to provide response sent to assignee.";
                            ValidationSummary8.CssClass = "alert alert-success";
                            newRecord.AccessURL = accessURL;
                            saveSuccess = LitigationTaskManagement.UpdateTaskAccessURL(newRecord.ID, newRecord);
                        }

                        clearTaskControls();

                        //Re-Bind Notice Task Details
                        BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAdvocateBillSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    bool isBlankFile = false;
                    //int AssignedToUser = -1;
                    int DocTypeID = -1;
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    List<string> lstCasehearingrefIDMapping = new List<string>();


                    if (!String.IsNullOrEmpty(lstAdvHearingRefNo.SelectedValue) && lstAdvHearingRefNo.SelectedValue != "0")
                    {
                        if (tbxAdvInvoiceAmount.Text != "")
                        {
                            if (tbxAdvInvoiceno.Text != "")
                            {
                                if ((!String.IsNullOrEmpty(ddlLawyerAdvocate.SelectedItem.Text)) && ddlLawyerAdvocate.SelectedItem.Text != "Select User")
                                {
                                    if (!String.IsNullOrEmpty(ddlApprover1.SelectedItem.Text) && ddlApprover1.SelectedItem.Text != "Select Approver") {
                                        if (!String.IsNullOrEmpty(ddlApprover2.SelectedItem.Text) && ddlApprover2.SelectedItem.Text != "Select Approver") 
                                        {
                                            if (Math.Floor(Convert.ToDecimal(tbxAdvInvoiceAmount.Text)) >= 300000)
                                            {
                                                if (!String.IsNullOrEmpty(ddlApprover3.SelectedItem.Text) && ddlApprover3.SelectedItem.Text != "Select Approver")
                                                {
                                                    if (isBlankFile == false)
                                                    {
                                                        validateData = true;

                                                    }
                                                    else
                                                    {
                                                        CustomValidator2.IsValid = false;
                                                        CustomValidator2.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                                                        ValidationSummary13.CssClass = "alert alert-danger";
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    CustomValidator2.IsValid = false;
                                                    CustomValidator2.ErrorMessage = "Please Provide Approver 3.";
                                                    ValidationSummary13.CssClass = "alert alert-danger";
                                                    return;
                                                }

                                            }
                                            else
                                            {
                                                if (isBlankFile == false)
                                                {
                                                    validateData = true;

                                                }
                                                else
                                                {
                                                    CustomValidator2.IsValid = false;
                                                    CustomValidator2.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                                                    ValidationSummary13.CssClass = "alert alert-danger";
                                                    return;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            CustomValidator2.IsValid = false;
                                            CustomValidator2.ErrorMessage = "Please Provide Approver 2.";
                                            ValidationSummary13.CssClass = "alert alert-danger";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        CustomValidator2.IsValid = false;
                                        CustomValidator2.ErrorMessage = "Please Provide Approver 1.";
                                        ValidationSummary13.CssClass = "alert alert-danger";
                                        return;
                                    }
                                }
                                else
                                {
                                    CustomValidator2.IsValid = false;
                                    CustomValidator2.ErrorMessage = "Please Provide Law Firm.";
                                    ValidationSummary13.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                CustomValidator2.IsValid = false;
                                CustomValidator2.ErrorMessage = "Provide Invoice No.";
                                ValidationSummary13.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                        else
                        {
                            CustomValidator2.IsValid = false;
                            CustomValidator2.ErrorMessage = "Provide Invoice Amount.";
                            ValidationSummary13.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                    else
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Select Hearing";
                        ValidationSummary13.CssClass = "alert alert-danger";
                        return;
                    }

                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < InvalidFileTypes.Length; j++)
                                        {
                                            if (ext == "." + InvalidFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (validateData && isBlankFile == false)
                    {
                        string casehearing = "";
                        if (lstAdvHearingRefNo.Items.Count > 0)
                        {
                            foreach (System.Web.UI.WebControls.ListItem eachHeringRef in lstAdvHearingRefNo.Items)
                            {
                                if (eachHeringRef.Selected)
                                {
                                    lstCasehearingrefIDMapping.Add(Convert.ToString(eachHeringRef.Text));
                                    casehearing += eachHeringRef.Text + ",";
                                }

                            }
                        }
                        tbl_CaseAdvocateBill obj = new tbl_CaseAdvocateBill();
                        List<tbl_CaseAdvocateBill> lstobj = new List<tbl_CaseAdvocateBill>();

                        if (lstCasehearingrefIDMapping.Count > 0)
                        {
                            obj.NoticeCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                            obj.HearingRef = Convert.ToString(casehearing);
                            obj.InvoiceNo = tbxAdvInvoiceno.Text;
                            obj.InvoiceAmount = Convert.ToDecimal(tbxAdvInvoiceAmount.Text);
                            obj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            obj.CreatedOn = System.DateTime.Now;
                            obj.IsActive = true;
                            obj.status = "Open";
                            obj.ApproverID1 = Convert.ToInt32(ddlApprover1.SelectedValue);
                            obj.ApproverID2 = Convert.ToInt32(ddlApprover2.SelectedValue);
                            if (!string.IsNullOrEmpty(ddlApprover3.SelectedValue))
                            {
                                obj.ApproverID3 = Convert.ToInt32(ddlApprover3.SelectedValue);
                            }
                            obj.Remark = tbxAdvRemark.Text;
                            obj.Currency = txtcurrency.Text;

                            obj.Lawyer = Convert.ToString(ddlLawyerAdvocate.SelectedItem);
                            obj.InvoiceDate = DateTimeExtensions.GetDate(tbxinvoicedate.Text);

                            ViewState["CaseHearingref"] = Convert.ToString(casehearing);

                            lstobj.Add(obj);


                            if ((int)ViewState["Mode"] == 0)
                            {
                                bool chkcashearing = CaseManagement.ExistsAdvocateBill(Convert.ToInt32(ViewState["CaseInstanceID"]), Convert.ToString(ViewState["CaseHearingref"]));
                                if (!chkcashearing)
                                {
                                    saveSuccess = CaseManagement.CreateAdvocateBillCaseHearing(lstobj, 0);
                                    if (saveSuccess)
                                    {
                                        string accessURL = string.Empty;
                                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                        if (Urloutput != null)
                                        {
                                            accessURL = Urloutput.URL;
                                        }
                                        else
                                        {
                                            accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                        }

                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            var data = (from row in entities.SP_LitigationCaseAdvocateBill(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                        where row.NoticeCaseInstanceID == Convert.ToInt32(ViewState["CaseInstanceID"])
                                                        select row).FirstOrDefault();


                                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_AdvocateBill
                                                                                  .Replace("@CaseRefNo", data.CaseNo)
                                                                                  .Replace("@CaseTitle", data.Title)
                                                                                  .Replace("@Location", data.BranchName)
                                                                                  .Replace("@InvoiceNo", tbxAdvInvoiceno.Text)
                                                                                  .Replace("@InvoiceAmount", tbxAdvInvoiceAmount.Text)
                                                                                  .Replace("@HearingDate", Convert.ToString(casehearing))
                                                                                  .Replace("@HearingDescription", data.NoticeCaseDesc)
                                                                                  .Replace("@AccessURL", Convert.ToString(accessURL)) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                                                  .Replace("@From", cname.Trim())
                                                                                  .Replace("@PortalURL", Convert.ToString(accessURL));

                                            List<string> UniqueMail = new List<string>();
                                            var lstApprover1 = LitigationManagement.GetAllAdvBillApprovers("Approver 1", Convert.ToInt32(AuthenticationHelper.CustomerID));
                                            UniqueMail.Add(lstApprover1[0].ApproverEmail);
                                            try
                                            {
                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), null, null, "Litigation Notification  -You have recieved an Advocate Bill for Approval", message);
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }
                                        }
                                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_CaseAdvocateBill", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Advocate Bill Created", true);
                                    }
                                }
                                else
                                {
                                    saveSuccess = false;
                                    CustomValidator1.IsValid = false;
                                    CustomValidator1.ErrorMessage = "Advocate Bill of case hearing already exist.";
                                    ValidationSummary12.CssClass = "alert alert-danger";
                                }
                            }
                            else
                            {
                                int advbillid = Convert.ToInt32(ViewState["AdvocateBillID"]);
                                obj.ID = Convert.ToInt32(ViewState["AdvocateBillID"]);
                                saveSuccess = CaseManagement.UpdateAdvocateBillCaseHearing(lstobj, obj.ID);
                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_CaseAdvocateBill", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Advocate Bill Updated", true);
                                }
                                saveSuccess = true;
                                CustomValidator1.IsValid = false;
                                CustomValidator1.ErrorMessage = "Advocate Bill Updated Successfully.";
                                ValidationSummary12.CssClass = "alert alert-success";
                                BindAdvocateBill(Convert.ToInt32(ViewState["CaseInstanceID"]), AuthenticationHelper.CustomerID);
                                clearAdvBillControls();

                            }

                            BindAdvocateBill(Convert.ToInt32(ViewState["CaseInstanceID"]), AuthenticationHelper.CustomerID);
                            clearAdvBillControls();

                        }

                        if (obj.ID > 0)
                            saveSuccess = true;
                        if (saveSuccess)
                        {

                            #region Upload Document

                            if (fuAdvocateBillDocUpload.HasFiles)
                            {

                                tbl_LitigationFileData objDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]),
                                    DocTypeInstanceID = Convert.ToInt64(obj.ID),
                                    //DocTypeInstanceID=item.ID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocTypeID = DocTypeID,
                                    DocType = "CA"
                                };

                                HttpFileCollection fileCollection1 = Request.Files;

                                if (fileCollection1.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                    string directoryPath = "";
                                    string fileName = "";


                                    for (int i = 0; i < fileCollection1.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection1[i];

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            string[] keys1 = fileCollection1.Keys[i].Split('$');

                                            if (keys1[keys1.Count() - 1].Equals("fuAdvocateBillDocUpload"))
                                            {
                                                fileName = uploadedFile.FileName;
                                            }

                                            //Get Document Version
                                            var caseAdvDocVersion = CaseManagement.ExistsCaseAdvocateBillDocumentReturnVersion(objDoc);

                                            caseAdvDocVersion++;
                                            objDoc.Version = caseAdvDocVersion + ".0";

                                            directoryPath = Server.MapPath("~/LitigationAdvocateDocuments/" + customerID + "/Cases/" + Convert.ToInt32(ViewState["CaseInstanceID"]) + "/AdvocateBill/" + objDoc.Version);

                                            if (!Directory.Exists(directoryPath))
                                                Directory.CreateDirectory(directoryPath);

                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                            Stream fs = uploadedFile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                            objDoc.FileName = fileName;
                                            objDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objDoc.FileKey = fileKey1.ToString();
                                            objDoc.VersionDate = DateTime.Now;
                                            objDoc.CreatedOn = DateTime.Now;
                                            objDoc.FileSize = uploadedFile.ContentLength;
                                            DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                            saveSuccess = CaseManagement.CreateCaseAdvocateBillDocumentMapping(objDoc);
                                            clearAdvBillControls();
                                            Filelist1.Clear();
                                        }
                                    }
                                    //End For Each      
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Advocate Bill Document(s) Uploaded.", true);
                                    }
                                    saveSuccess = true;
                                    CustomValidator1.IsValid = false;
                                    CustomValidator1.ErrorMessage = "Advocate Bill Added Successfully.";
                                    ValidationSummary12.CssClass = "alert alert-success";
                                    BindAdvocateBill(Convert.ToInt32(ViewState["CaseInstanceID"]), AuthenticationHelper.CustomerID);
                                }


                            }

                            #endregion

                            if ((int)ViewState["Mode"] == 1)
                            {
                                saveSuccess = true;
                                CustomValidator1.IsValid = false;
                                CustomValidator1.ErrorMessage = "Advocate Bill Updated Successfully.";
                                ValidationSummary12.CssClass = "alert alert-success";
                                BindAdvocateBill(Convert.ToInt32(ViewState["CaseInstanceID"]), AuthenticationHelper.CustomerID);
                            }

                            return;

                        }

                        clearAdvBillControls();

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static long EnableAdvocatebill(long advbillid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_CaseAdvocateBill
                            where row.ID==advbillid
                            select row.CreatedBy).FirstOrDefault();


                return Convert.ToInt32(data);
            }
        }
        protected void btnClearTask_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public bool BindTaskResponses(int taskID, GridView grd)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grd.DataSource = lstTaskResponses;
                    grd.DataBind();

                    return true;
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();

                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
                return false;
            }
        }

        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Response Deleted Successfully.";
                        ValidationSummary5.CssClass = "alert alert-success";
                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_TaskResponse", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Response Deleted", true);
                    }
                    else
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Document(s) Downloaded", false);
                                }
                            }
                            else
                            {
                                cvCasePopUpTask.IsValid = false;
                                cvCasePopUpTask.ErrorMessage = "No Document Available for Download.";
                                ValidationSummary5.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]), grdTaskActivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        protected void grdTaskResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton)e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton)e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["caseStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                {
                    lnkBtnDeleteTaskResponse.Visible = false;
                }
            }
        }

        #endregion

        #region Case-Status
        protected void btnSaveStatus_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                int caseresult = 0;
                string CaseStatus = string.Empty;
                List<string> ExcludedMailID = new List<string>();

                if (ViewState["CaseInstanceID"] != null)
                {
                    if (!String.IsNullOrEmpty(ddlCaseStatus.SelectedValue) && ddlCaseStatus.SelectedValue != "0")
                    {
                        if (!String.IsNullOrEmpty(ddlCaseStage.SelectedValue) && ddlCaseStage.SelectedValue != "0")
                        {
                            long caseInstanceID = 0;
                            caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                            if (caseInstanceID != 0)
                            {
                                int selectedStatusID = Convert.ToInt32(ddlCaseStatus.SelectedValue);
                                int selectedStageID = Convert.ToInt32(ddlCaseStage.SelectedValue);

                                //Update Case Stage in Case Instance Table
                                saveSuccess = CaseManagement.UpdateCaseStage(caseInstanceID, selectedStageID, AuthenticationHelper.UserID);

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Stage Updated", true);

                                    //Status Transaction Record - Which will Create or Update on Each Status Move
                                    tbl_LegalCaseStatusTransaction newStatusTxnRecord = new tbl_LegalCaseStatusTransaction()
                                    {
                                        CaseInstanceID = caseInstanceID,
                                        StatusID = selectedStatusID,
                                        StatusChangeOn = DateTime.Now,
                                        IsActive = true,
                                        IsDeleted = false,
                                        UserID = AuthenticationHelper.UserID,
                                        RoleID = 3,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                        StatusRemark = tbxCloseRemark.Text
                                    };

                                    //Status Record - i.e. Case Closure Record; Only Active on Case Close otherwise DeActive 
                                    tbl_LegalCaseStatus newStatusRecord = new tbl_LegalCaseStatus()
                                    {
                                        CaseInstanceID = caseInstanceID,
                                        StatusID = selectedStatusID,
                                        CloseDate = DateTime.Now,
                                        IsActive = true,
                                        IsDeleted = false,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                    };
                                    if (!string.IsNullOrEmpty(ddlCaseResult.SelectedValue))
                                    {
                                        if (ddlCaseResult.SelectedValue != "0")
                                        {
                                            caseresult = Convert.ToInt32(ddlCaseResult.SelectedValue);
                                            CaseStatus = ddlCaseStatus.SelectedItem.Text;
                                        }
                                    }
                                    else
                                    {
                                        caseresult = Convert.ToInt32(ddlCaseResult.Items.FindByText("In Progress").Value);
                                        CaseStatus = "Close";
                                    }

                                    tbl_LegalCaseInstance objcaseInstance = new tbl_LegalCaseInstance()
                                    {
                                        ID = caseInstanceID,
                                        CaseResult = caseresult
                                    };

                                    saveSuccess = CaseManagement.UpdateCaseResult(objcaseInstance);
                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Result Updated", true);
                                    }
                                    if (tbxCloseRemark.Text != "")
                                        newStatusRecord.ClosureRemark = tbxCloseRemark.Text;
                                      
                                    if (tbxCaseCloseDate.Text != "")
                                        // newStatusRecord.CloseDate = Convert.ToDateTime(tbxCaseCloseDate.Text);
                                        newStatusRecord.CloseDate= DateTime.ParseExact(tbxCaseCloseDate.Text, "MM-dd-yyyy", CultureInfo.InvariantCulture);
                                    //RCT.Challanpaiddate != null ? RCT.Challanpaiddate.Value.ToString("dd-MM-yyyy") : " ";
                                    if (ddlCaseStatus.SelectedValue != "3") //Open or In Progress
                                    {
                                        if (!CaseManagement.ExistCaseStatusTransaction(newStatusTxnRecord))
                                        {
                                            saveSuccess = CaseManagement.DeActiveCaseStatusTransaction(newStatusTxnRecord);
                                            saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusTxnRecord);
                                        }
                                        else
                                            saveSuccess = CaseManagement.UpdateCaseStatusTransaction(newStatusTxnRecord);

                                        //If Exists Case Closure Record then DeActive it
                                        if (CaseManagement.ExistCaseStatus(newStatusRecord))
                                        {
                                            saveSuccess = CaseManagement.UpdateCaseStatus(newStatusRecord);
                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseStatus", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Status Updated", true);
                                            }
                                        }
                                    }
                                    else if (ddlCaseStatus.SelectedValue == "3") //Close
                                    {
                                        if (tbxCaseCloseDate.Text != "")
                                        {
                                            //Create or Update Status Transaction Records
                                            if (!CaseManagement.ExistCaseStatusTransaction(newStatusTxnRecord))
                                            {
                                                saveSuccess = CaseManagement.DeActiveCaseStatusTransaction(newStatusTxnRecord);
                                                saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusTxnRecord);
                                            }
                                            else
                                                saveSuccess = CaseManagement.UpdateCaseStatusTransaction(newStatusTxnRecord);

                                            //Create or Update Status Record
                                            if (!CaseManagement.ExistCaseStatus(newStatusRecord))
                                            {
                                                saveSuccess = CaseManagement.CreateCaseStatus(newStatusRecord);
                                                if (saveSuccess)
                                                {
                                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseStatus", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Status Created", true);
                                                }
                                            }
                                            else
                                            {
                                                saveSuccess = CaseManagement.UpdateCaseStatus(newStatusRecord);
                                                if (saveSuccess)
                                                {
                                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseStatus", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Status Updated", true);
                                                }
                                            }
                                            //if (ddlCaseStatus.SelectedValue == "3")
                                            //{
                                            long CustomerID = AuthenticationHelper.CustomerID;
                                                var customizedid = GetCustomizedCustomerid(CustomerID);
                                                if (customizedid == AuthenticationHelper.CustomerID)
                                                {
                                                    ddlCaseStage.Enabled = false;
                                                    ddlCaseStatus.Enabled = false;
                                                    ddlCaseResult.Enabled = false;
                                                    tbxCaseCloseDate.Enabled = false;
                                                    tbxCloseRemark.Enabled = false;
                                                     btnSaveStatus.Enabled = false;
                                                }
                                            //}
                                            if (saveSuccess)
                                            {
                                                #region Mail Data
                                                User User = UserManagement.GetByID(AuthenticationHelper.UserID);
                                                string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                                                List<string> CaseOwnerandIUser = CaseManagement.getCaseOwnerAndInternalUser(Convert.ToInt32(caseInstanceID), AuthenticationHelper.CustomerID);
                                                var caseRecord = CaseManagement.GetCaseByID(Convert.ToInt32(caseInstanceID));
                                                var Locations = string.Empty;
                                                if (!string.IsNullOrEmpty(Convert.ToString(caseRecord.CustomerBranchID)))
                                                {
                                                    Locations = CaseManagement.GetLocationByCaseInstanceID(caseRecord.CustomerBranchID);
                                                }

                                                List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                                                List<string> UniqueMail = new List<string>();
                                                if (CaseOwnerandIUser.Count > 0)
                                                {
                                                    foreach (var item in CaseOwnerandIUser)
                                                    {
                                                        if (MgmUser.Count > 0)
                                                        {
                                                            if (!MgmUser.Contains(item))
                                                            {
                                                                UniqueMail.Add(item);
                                                            }
                                                        }
                                                        else
                                                            UniqueMail.Add(item);
                                                    }
                                                }
                               
                                                string CaseTitleMerge = caseRecord.CaseTitle;
                                                string FinalCaseTitle = string.Empty;
                                                if (CaseTitleMerge.Length > 50)
                                                {
                                                    FinalCaseTitle = CaseTitleMerge.Substring(0, 50);
                                                    FinalCaseTitle = FinalCaseTitle + "...";
                                                }
                                                else
                                                {
                                                    FinalCaseTitle = CaseTitleMerge;
                                                }

                                                #endregion end mail

                                                #region Send Mail to managment Case Closed

                                                var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                                if (UniqueMail.Count > 0)
                                                {
                                                    string PortalURL = string.Empty;
                                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                                    if (Urloutput != null)
                                                    {
                                                        PortalURL = Urloutput.URL;
                                                    }
                                                    else
                                                    {
                                                        PortalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                                    }

                                                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_CaseClose
                                                                           .Replace("@UpdatedUser", username)
                                                                           .Replace("@CaseRefNo", caseRecord.CaseRefNo)
                                                                           .Replace("@CaseTitle", caseRecord.CaseTitle)
                                                                           .Replace("@Location", Locations)
                                                                           .Replace("@CaseCloseDate", tbxCaseCloseDate.Text)
                                                                           .Replace("@CaseRemark", tbxCloseRemark.Text)
                                                                           .Replace("@CaseStatus", CaseStatus)
                                                                           .Replace("@AccessURL", Convert.ToString(PortalURL)) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                                           .Replace("@From", cname.Trim())
                                                                           .Replace("@PortalURL", Convert.ToString(PortalURL));

                                                    ExcludedMailID = CaseManagement.Mailallowedornotcheck("Case Closed", CustomerID);

                                                    if (ExcludedMailID.Count > 0)
                                                    {
                                                        foreach (var item in UniqueMail.ToList())
                                                        {
                                                            if (ExcludedMailID.Contains(item))
                                                            {
                                                                UniqueMail.Remove(item);
                                                                continue;
                                                            }
                                                        }
                                                    }
                                                    if (ExcludedMailID.Count > 0)
                                                    {
                                                        foreach (var item in MgmUser.ToList())
                                                        {
                                                            if (ExcludedMailID.Contains(item))
                                                            {
                                                                MgmUser.Remove(item);
                                                                continue;
                                                            }
                                                        }
                                                    }

                                                    try
                                                    {
                                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), MgmUser, null, "Litigation Case Closed- " + FinalCaseTitle, message);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            cvCaseStatus.IsValid = false;
                                            cvCaseStatus.ErrorMessage = "Please Provide Close Date.";
                                            ValidationSummary3.CssClass = "alert alert-danger";
                                            CheckCaseClose = false;
                                            return;
                                        }
                                    }
                                }

                              
                                if (saveSuccess)
                                {
                                    ViewState["caseStatus"] = selectedStatusID;

                                    cvCaseStatus.IsValid = false;
                                    cvCaseStatus.ErrorMessage = "Details Saved Successfully.";
                                    ValidationSummary3.CssClass = "alert alert-success";
                                    CheckCaseClose = false;
                                    return;
                                }


                            }
                        }
                        else
                        {
                            cvCaseStatus.IsValid = false;
                            cvCaseStatus.ErrorMessage = "Select Case Stage.";
                            ValidationSummary3.CssClass = "alert alert-danger";
                            CheckCaseClose = false;
                            return;
                        }
                    }
                    else
                    {
                        cvCaseStatus.IsValid = false;
                        cvCaseStatus.ErrorMessage = "Select Case Status.";
                        ValidationSummary3.CssClass = "alert alert-danger";
                        CheckCaseClose = false;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Case-Payment
        public void BindCasePayments(int caseInstanceID)
        {
            try
            {
                //List<tbl_NoticeCasePayment> lstCasePayments = new List<tbl_NoticeCasePayment>();
                List<SP_Litigation_NoticeCasePayment_Result> lstCasePayments = new List<SP_Litigation_NoticeCasePayment_Result>();

                lstCasePayments = LitigationManagement.GetCasePaymentDetails(caseInstanceID, "C");

                if (lstCasePayments != null && lstCasePayments.Count > 0)
                {
                    grdCasePayment.DataSource = lstCasePayments;
                    grdCasePayment.DataBind();
                }
                else
                {
                    SP_Litigation_NoticeCasePayment_Result obj = new SP_Litigation_NoticeCasePayment_Result(); //initialize empty class that may contain properties
                    lstCasePayments.Add(obj); //Add empty object to list

                    grdCasePayment.DataSource = lstCasePayments; /*Assign datasource to create one row with default values for the class you have*/
                    grdCasePayment.DataBind(); //Bind that empty source                    

                    //To Hide row
                    grdCasePayment.Rows[0].Visible = false;
                    grdCasePayment.Rows[0].Controls.Clear();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }
        protected void grdCasePayment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                TextBox tbxPaymentDate = (TextBox)grdCasePayment.FooterRow.FindControl("tbxPaymentDate");
                DropDownList ddlPaymentType = (DropDownList)grdCasePayment.FooterRow.FindControl("ddlPaymentType");
                DropDownList ddlLawyer = (DropDownList)grdCasePayment.FooterRow.FindControl("ddlLawyer");
                DropDownList ddlHearingID = (DropDownList)grdCasePayment.FooterRow.FindControl("ddlHearingID");
                TextBox tbxAmount = (TextBox)grdCasePayment.FooterRow.FindControl("tbxAmount");
                TextBox tbxPaymentRemark = (TextBox)grdCasePayment.FooterRow.FindControl("tbxPaymentRemark");
                TextBox tbxAmountPaid = (TextBox)grdCasePayment.FooterRow.FindControl("tbxAmountPaid");
                TextBox tbxInvoiceNo = (TextBox)grdCasePayment.FooterRow.FindControl("tbxInvoiceNo");
                TextBox tbxPaymentID = (TextBox)grdCasePayment.FooterRow.FindControl("tbxPaymentID");
                TextBox tbxAmountTaxPaid = (TextBox)grdCasePayment.FooterRow.FindControl("tbxAmountTaxPaid");
                int CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                int payId = Convert.ToInt32(e.CommandArgument);
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeletePayment"))
                    {
                        DeletePaymentLog(Convert.ToInt32(e.CommandArgument));
                        //Re-Bind Case Payments
                        if (ViewState["CaseInstanceID"] != null)
                        {
                            BindCasePayments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                            ViewState["PaymentMode"] = "Add";
                        }
                    }

                    #region edit Payment
                    else if (e.CommandName.Equals("EditPayment"))
                    {
                        if (CaseInstanceID != 0 && payId != 0)
                        {
                            var PaymentData = CaseManagement.GetPaymentDetailsByID(CaseInstanceID, payId);
                            tbxInvoiceNo.Text = Convert.ToString(PaymentData.InvoiceNo);
                            tbxPaymentID.Text = Convert.ToString(payId);
                            tbxPaymentDate.Text = Convert.ToDateTime(PaymentData.PaymentDate).ToString("dd-MM-yyyy");
                            ddlPaymentType.SelectedValue = PaymentData.PaymentID.ToString();
                            tbxAmount.Text = Convert.ToString(PaymentData.Amount);
                            tbxAmountPaid.Text = Convert.ToString(PaymentData.AmountPaid);
                            //ddlLawyer.SelectedItem.Text = PaymentData.Lawyer.ToString();
                            tbxPaymentRemark.Text = PaymentData.Remark;
                            //ddlHearingID.SelectedValue = PaymentData.HearingID.ToString();
                            if (PaymentData.AmountTax != null)
                                tbxAmountTaxPaid.Text = Convert.ToString(PaymentData.AmountTax);

                            if (PaymentData.Lawyer != null)
                                ddlLawyer.SelectedItem.Text = PaymentData.Lawyer.ToString();

                            if (PaymentData.HearingID != null)
                                ddlHearingID.SelectedValue = PaymentData.HearingID.ToString();

                           
                            ViewState["PaymentMode"] = "Edit";


                        }
                    }
                    #region viewdocument
                    else if (e.CommandName.Equals("ViewCasePayment"))
                    {


                        var AllinOneDocumentList = CaseManagement.GetCasePaymentDocumentByID(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS
                                if (AllinOneDocumentList.FilePath != null)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                        request.Key = AllinOneDocumentList.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                    }
                                    string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                    DocumentPath = filePath1;
                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewNew('" + DocumentPath + "');", true);
                                    lblMessage.Text = "";                                    
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal
                                string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                                if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }
                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (AllinOneDocumentList.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    DocumentPath = FileName;
                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewNew('" + DocumentPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewNew();", true);
                        }
                    }
                    #endregion


                    #endregion
                    if (e.CommandName.Equals("ConverttopdfPayment"))
                        {
                            try
                            {
                                DateTime todaysdate = DateTime.Now;
                                string filename = "PaymentOrder" + todaysdate.ToString("dd-MM-yyyy") + ".pdf";

                                var obj = CaseManagement.GetcasedetailofPayment(Convert.ToInt32(e.CommandArgument));
                                if (obj != null)
                                {

                                    var _objcaserefdetails = CaseManagement.GetcaseHearingDetails(Convert.ToInt32(obj.HearingID));
                                    if (_objcaserefdetails.Count > 0)
                                    {
                                        Document document = new Document(PageSize.A4, 30, 30, 10, 10);

                                        System.IO.MemoryStream msReport = new System.IO.MemoryStream();
                                        try
                                        {
                                            PdfWriter writer = PdfWriter.GetInstance(document, msReport);
                                            document.Open();
                                            //leave a gap before and after the table
                                            foreach (var item in _objcaserefdetails)
                                            {
                                                document.NewPage();
                                                Paragraph TitlePhrasePh = new Paragraph("Case Payment Slip");
                                                TitlePhrasePh.Alignment = Element.ALIGN_CENTER;
                                                document.Add(TitlePhrasePh);
                                                Phrase phrNewLine = new Phrase("\n");
                                                document.Add(phrNewLine);
                                                PdfPCell cell = new PdfPCell();
                                                cell.Colspan = 2;
                                                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                                                PdfPTable table = new PdfPTable(2);
                                                table.TotalWidth = 350f;
                                                //fix the absolute width of the table
                                                table.LockedWidth = false;
                                                //relative col widths in proportions - 1/3 and 2/3
                                                float[] widths = new float[] { 4f, 10f };
                                                table.SetWidths(widths);
                                                table.HorizontalAlignment = 1;
                                                table.AddCell(cell);
                                                table.AddCell("Invoice No");
                                                table.AddCell(item.InvoiceNo);
                                                table.AddCell("Lawyer");
                                                table.AddCell(item.Lawyer);
                                                table.AddCell("Payment Date");
                                                table.AddCell(String.Format("{0:dd-MM-yyyy}", item.Date));
                                                //table.AddCell("Form");
                                                //table.AddCell("");
                                                table.AddCell("Case Title");
                                                table.AddCell(item.CaseTitle);
                                                table.AddCell("Case Description");
                                                table.AddCell(item.Matterinbreef);
                                                table.AddCell("Date of Hearing");
                                                table.AddCell((String.Format("{0:dd-MM-yyyy}", item.DateOfHearing)));
                                                //table.AddCell("Advocate who attended the matter ");
                                                //table.AddCell("");
                                                table.AddCell("Fees Agreed");
                                                table.AddCell((item.FeesAggred).ToString());
                                                table.AddCell("Fees Claimed");
                                                table.AddCell((item.FeesClaimed).ToString());
                                                table.AddCell("Payment Type");
                                                table.AddCell(item.PaymentType);
                                                table.AddCell("Hearing Description");
                                                table.AddCell(item.DetailofHearing);
                                                table.AddCell("Next Date of Hearing");
                                                table.AddCell((String.Format("{0:dd-MM-yyyy}", item.Nextdateofhearing)));
                                                table.AddCell("Remark");
                                                table.AddCell(item.Remark);
                                                table.AddCell("Created By");
                                                table.AddCell(item.CreatedByText);
                                            document.Add(table);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            cvCasePayment.IsValid = false;
                                            cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                                            ValidationSummary4.CssClass = "alert alert-danger";
                                        }
                                        document.Close();

                                        Response.Clear();
                                        Response.ContentType = "application/pdf";
                                        Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                                        Response.BinaryWrite(msReport.ToArray());
                                        Response.Flush();
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                                        upCasePayment.Update();
                                    }

                                    else
                                    {
                                        cvCasePayment.IsValid = false;
                                        cvCasePayment.ErrorMessage = "No data Found";
                                        ValidationSummary4.CssClass = "alert alert-danger";
                                    }
                                }
                                else
                                {
                                    cvCasePayment.IsValid = false;
                                    cvCasePayment.ErrorMessage = "No data Found";
                                    ValidationSummary4.CssClass = "alert alert-danger";
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                        }
                        else if (e.CommandName.Equals("DownloadCasePaymentDoc"))
                        {
                            DownloadCasePaymentDocument(Convert.ToInt32(e.CommandArgument));
                        }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }
        public void DownloadCasePaymentDocument(int caseFileID)
        {
            try
            {
                var file = CaseManagement.GetCasePaymentDocumentByID(caseFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }
                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                            }
                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                            if (filePath != null && File.Exists(filePath))
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                                
                                Response.BinaryWrite(DocumentManagement.ReadDocFiles(filePath)); // create the file
                                
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationPaymentFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Payment Document Downloaded", false);
                                applyCSStoFileTag_ListItems();
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (filePath != null && File.Exists(filePath))
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationPaymentFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Payment Document Downloaded", false);
                                applyCSStoFileTag_ListItems();
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }
        protected void grdCasePayment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnDeletePayment = (LinkButton)e.Row.FindControl("lnkBtnDeletePayment");
                if (lnkBtnDeletePayment != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeletePayment);
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlPaymentType = (DropDownList)e.Row.FindControl("ddlPaymentType");

                if (ddlPaymentType != null)
                    BindPaymentType(ddlPaymentType);
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlHearingID = (DropDownList)e.Row.FindControl("ddlHearingID");

                if (ddlHearingID != null)
                    BindHearingID(ddlHearingID);
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlLawyer = (DropDownList)e.Row.FindControl("ddlLawyer");

                if (ddlLawyer != null)
                    BindLawyers(ddlLawyer);
            }
        }

        private void BindLawyers(DropDownList ddlLawyer)
        {
            var LayerList = LitigationPaymentType.getLawyerList(Convert.ToInt32(AuthenticationHelper.CustomerID));
            ddlLawyer.DataValueField = "ID";
            ddlLawyer.DataTextField = "Name";

            ddlLawyer.DataSource = LayerList;
            ddlLawyer.DataBind();
            ddlLawyer.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Lawyer", "-1"));

        }

        private void BindAdvLawyer()
        {
            //var LayerList = LitigationPaymentType.getLawyerList(Convert.ToInt32(AuthenticationHelper.CustomerID));
            var LayerList = LawyerManagement.GetLawyerListForMapping(Convert.ToInt32(AuthenticationHelper.CustomerID));


            ddlLawyerAdvocate.DataValueField = "ID";
            ddlLawyerAdvocate.DataTextField = "Name";

            ddlLawyerAdvocate.DataSource = LayerList;
            ddlLawyerAdvocate.DataBind();
            ddlLawyerAdvocate.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select User", "-1"));
        }
        private void BindHearingID(DropDownList ddl)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    string caseinstantID = ViewState["CaseInstanceID"].ToString();
                    var HearingList = LitigationPaymentType.GetAllHearingIDList(Convert.ToInt32(caseinstantID), Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (HearingList.Count > 0)
                    {
                        ddl.DataValueField = "ID";
                        ddl.DataTextField = "HearingRefNo";

                        ddl.DataSource = HearingList;
                        ddl.DataBind();

                      
                    }
                    // ddl.Items.Insert(0, "Hearing");
                    // ddl.Items.Add(new System.Web.UI.WebControls.ListItem("Hearing", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        protected void grdCasePayment_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdCasePayment.PageIndex = e.NewPageIndex;

                    //Re-Bind Case Payments
                    BindCasePayments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnPaymentSave_Click(object sender, EventArgs e)
        {
            TextBox tbxPaymentDate = (TextBox)grdCasePayment.FooterRow.FindControl("tbxPaymentDate");
            DropDownList ddlPaymentType = (DropDownList)grdCasePayment.FooterRow.FindControl("ddlPaymentType");
            DropDownList ddlLawyer = (DropDownList)grdCasePayment.FooterRow.FindControl("ddlLawyer");
            DropDownList ddlHearingID = (DropDownList)grdCasePayment.FooterRow.FindControl("ddlHearingID");
            TextBox tbxAmount = (TextBox)grdCasePayment.FooterRow.FindControl("tbxAmount");
            TextBox tbxPaymentRemark = (TextBox)grdCasePayment.FooterRow.FindControl("tbxPaymentRemark");
            TextBox tbxAmountPaid = (TextBox)grdCasePayment.FooterRow.FindControl("tbxAmountPaid");
            TextBox tbxInvoiceNo = (TextBox)grdCasePayment.FooterRow.FindControl("tbxInvoiceNo");
            TextBox tbxPaymentID = (TextBox)grdCasePayment.FooterRow.FindControl("tbxPaymentID");
            FileUpload fu = (FileUpload)grdCasePayment.FooterRow.FindControl("fuSampleFile");
            TextBox tbxAmountTaxPaid = (TextBox)grdCasePayment.FooterRow.FindControl("tbxAmountTaxPaid");
            long newPaymentID = 0;
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    if (tbxPaymentDate != null && ddlPaymentType != null && ddlLawyer != null && tbxAmount != null && tbxPaymentRemark != null && tbxAmountPaid != null)
                    {
                        bool validateData = false;
                        bool saveSuccess = false;

                        if (!string.IsNullOrEmpty(tbxPaymentDate.Text.Trim()))
                        {
                            if (!String.IsNullOrEmpty(ddlPaymentType.SelectedValue))
                            {
                                if (tbxAmount.Text != "")
                                {
                                    if (!string.IsNullOrEmpty(tbxPaymentRemark.Text.Trim()))
                                    {
                                        try
                                        {
                                            Convert.ToDecimal(tbxAmount.Text);
                                            validateData = true;
                                        }
                                        catch (Exception ex)
                                        {
                                            validateData = false;
                                        }
                                    }
                                }
                            }
                        }
                                             
                        #region Upload Document
                        HttpFileCollection fileCollection = Request.Files;
                        if (fileCollection.Count > 0)
                        {
                            bool isBlankFile = false;
                            string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadfile = null;
                                uploadfile = fileCollection[i];
                                int filelength = uploadfile.ContentLength;
                                string fileName = Path.GetFileName(uploadfile.FileName);
                                string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                                if (!string.IsNullOrEmpty(fileName))
                                {
                                    if (filelength == 0)
                                    {
                                        isBlankFile = true;
                                        break;
                                    }
                                    else if (ext == "")
                                    {
                                        isBlankFile = true;
                                        break;
                                    }
                                    else
                                    {
                                        if (ext != "")
                                        {
                                            for (int j = 0; j < InvalidFileTypes.Length; j++)
                                            {
                                                if (ext == "." + InvalidFileTypes[j])
                                                {
                                                    isBlankFile = true;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            
                        }

                        #endregion

                        if (validateData)
                        {

                            tbl_NoticeCasePayment newRecord = new tbl_NoticeCasePayment()
                            {
                                NoticeOrCase = "C",
                                IsActive = true,
                                NoticeOrCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]),
                                PaymentDate = DateTimeExtensions.GetDate(tbxPaymentDate.Text),
                                PaymentID = Convert.ToInt32(ddlPaymentType.SelectedValue),
                                Amount = Convert.ToDecimal(tbxAmount.Text),
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                AmountPaid = Convert.ToDecimal(tbxAmountPaid.Text),
                                Lawyer = ddlLawyer.SelectedItem.ToString()

                                //HearingID = Convert.ToInt32(ddlHearingID.SelectedValue)
                            };
                            //string invoiceNo = LitigationPaymentType.GetInvoiceNo();
                            if (tbxAmountTaxPaid.Text != null && tbxAmountTaxPaid.Text != "")
                                newRecord.AmountTax = Convert.ToDecimal(tbxAmountTaxPaid.Text);

                            if (!string.IsNullOrEmpty(tbxInvoiceNo.Text.Trim()))
                            {
                                newRecord.InvoiceNo = tbxInvoiceNo.Text;
                            }
                            if (tbxPaymentRemark.Text != "")
                                newRecord.Remark = tbxPaymentRemark.Text;
                            if (ddlHearingID.SelectedValue == "" || ddlHearingID.SelectedValue == "-1")
                            {
                                newRecord.HearingID = null;
                            }
                            else
                            {
                                newRecord.HearingID = Convert.ToInt32(ddlHearingID.SelectedValue);
                            }

                            #region edit
                            if (Convert.ToString(ViewState["PaymentMode"]) != "Edit")
                            {
                                saveSuccess = CaseManagement.CreateCasePaymentLog(newRecord);
                            }

                            if (Convert.ToString(ViewState["PaymentMode"]) == "Edit")
                            {
                                newRecord.ID = Convert.ToInt32(tbxPaymentID.Text);
                                newPaymentID = CaseManagement.UpdatePayment(newRecord);
                                HttpFileCollection fileCollectionnew = Request.Files;
                                var fname = GetOldFilename(newRecord.ID);
                                if (fu.FileName == "")
                                {

                                }
                                else if (fu.FileName != fname)
                                {
                                    RemoveOldDocument(newRecord.ID);
                                }
                                else
                                {

                                }
                            }
                            ViewState["Newpaymentid"] = newRecord.ID;
                            //ViewState["invoiceno"] = newRecord.InvoiceNo;
                            ViewState["invoiceno"] = newRecord.ID;
                            if (newPaymentID > 0)
                                saveSuccess = true;
                            #endregion

                        //saveSuccess = CaseManagement.CreateCasePaymentLog(newRecord);
                        }

                        if (saveSuccess)
                        {
                            uploadDocuments(Convert.ToInt32(ViewState["CaseInstanceID"]),  Request.Files, "NoticeCaseFileUpload", Convert.ToInt32(ViewState["Newpaymentid"]),Convert.ToInt32(ViewState["invoiceno"]));
                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_NoticeCasePayment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Payment Detail Added", true);
                            cvCasePayment.IsValid = false;
                            cvCasePayment.ErrorMessage = "Payment Details Saved Successfully.";
                            ValidationSummary4.CssClass = "alert alert-success";
                            //Re-Bind Case Payment Log Details
                            BindCasePayments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                            ViewState["PaymentMode"] = "Add";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static string GetOldFilename(long paymentid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_LitigationPaymentFileData
                            where row.NoticeCasePaymentId == paymentid
                            select row.FileName).SingleOrDefault();
                return data;
            }
        }
        public static void RemoveOldDocument(long paymentid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var query = (from row in entities.tbl_LitigationPaymentFileData
                                 where row.NoticeCasePaymentId == paymentid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        entities.tbl_LitigationPaymentFileData.Remove(query);
                        entities.SaveChanges();

                    }



                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        protected bool uploadDocuments(long contractID, HttpFileCollection fileCollection, string fileUploadControlName,long paymentid,long invoiceno)
        {
            bool saveSuccess = false;
            try
            {
                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                if (AWSData != null)
                {
                    #region Upload Document


                    if (ViewState["CaseInstanceID"] != null)
                    {

                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        string Flag = "PaymentCases";
                        string directoryPath = string.Empty;
                        string fileName = string.Empty;
                        string docType = string.Empty;


                        tbl_LitigationPaymentFileData objNoticeCaseDoc = new tbl_LitigationPaymentFileData()
                        {
                            NoticeCasePaymentId = paymentid,
                            NoticeCaseInstanceID = Convert.ToInt32(contractID),
                            InvoiceNo = invoiceno,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                            IsDeleted = false,
                            DocType = "C",

                        };

                        if (fileCollection.Count > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                            
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadedFile = fileCollection[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                    if (keys1[keys1.Count() - 1].Equals("LitigationFileUpload"))
                                    {
                                        fileName = uploadedFile.FileName;
                                    }

                                    //Get Document Version
                                    var caseDocVersion = CaseManagement.ExistsCasePaymentDocumentReturnVersion(objNoticeCaseDoc);

                                    caseDocVersion++;
                                    objNoticeCaseDoc.Version = caseDocVersion + ".0";

                                    directoryPath = "LitigationDocuments\\" + customerID + "\\" + Flag + "\\" + Convert.ToInt32(contractID) + "\\" + Flag + "Document\\" + objNoticeCaseDoc.Version;
                                    
                                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                    if (!di.Exists)
                                    {
                                        di.Create();
                                    }

                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                    string p_strPath = string.Empty;
                                    string dirpath = string.Empty;
                                    //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                                    //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;
                                    //if (File.Exists(p_strPath))
                                    //    File.Delete(p_strPath);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                    dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;

                                    if (!Directory.Exists(dirpath))
                                    {
                                        Directory.CreateDirectory(dirpath);
                                    }
                                    FileStream objFileStrm = File.Create(p_strPath);
                                    objFileStrm.Close();
                                    File.WriteAllBytes(p_strPath, bytes);

                                    Guid fileKey1 = Guid.NewGuid();

                                    string AWSpath = directoryPath + "\\" + uploadedFile.FileName;

                                    objNoticeCaseDoc.FilePath = directoryPath.Replace(@"\", "/");                                    
                                    objNoticeCaseDoc.FileKey = fileKey1.ToString();
                                    objNoticeCaseDoc.VersionDate = DateTime.Now;
                                    objNoticeCaseDoc.CreatedOn = DateTime.Now;
                                    objNoticeCaseDoc.FileSize = uploadedFile.ContentLength;
                                    objNoticeCaseDoc.FileName = uploadedFile.FileName;
                                    FileInfo localFile = new FileInfo(p_strPath);
                                    S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                    if (!s3File.Exists)
                                    {
                                        using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                        {
                                            localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                        }
                                    }
                                    //DocumentManagement.Litigation_SaveDocFiles(fileList);
                                    int FileID = CaseManagement.CreateCaseDocumentPaymentMappingGetID(objNoticeCaseDoc);
                                    if (FileID != 0)
                                    {
                                        saveSuccess = true;
                                    }

                                    List<tbl_Litigation_FileDataTagsMapping> lstFileTagMapping = new List<tbl_Litigation_FileDataTagsMapping>();


                                    tbl_Litigation_FileDataTagsMapping objFileTagMapping = new tbl_Litigation_FileDataTagsMapping()
                                    {
                                        FileID = FileID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                        UpdatedOn = DateTime.Now,
                                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    };
                                    lstFileTagMapping.Add(objFileTagMapping);


                                    if (lstFileTagMapping.Count > 0)
                                    {
                                        saveSuccess = CaseManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                    }

                                    fileList.Clear();
                                }
                            }//End For Each
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Upload Document


                    if (ViewState["CaseInstanceID"] != null)
                    {

                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        string Flag = "PaymentCases";
                        string directoryPath = string.Empty;
                        string fileName = string.Empty;
                        string docType = string.Empty;


                        tbl_LitigationPaymentFileData objNoticeCaseDoc = new tbl_LitigationPaymentFileData()
                        {
                            NoticeCasePaymentId = paymentid,
                            NoticeCaseInstanceID = Convert.ToInt32(contractID),
                            InvoiceNo = invoiceno,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                            IsDeleted = false,
                            DocType = "C",

                        };

                        if (fileCollection.Count > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();


                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadedFile = fileCollection[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                    if (keys1[keys1.Count() - 1].Equals("LitigationFileUpload"))
                                    {
                                        fileName = uploadedFile.FileName;
                                    }

                                    //Get Document Version
                                    var caseDocVersion = CaseManagement.ExistsCasePaymentDocumentReturnVersion(objNoticeCaseDoc);

                                    caseDocVersion++;
                                    objNoticeCaseDoc.Version = caseDocVersion + ".0";

                                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/" + Flag + "/" + Convert.ToInt32(contractID) + "/" + Flag + "Document/" + objNoticeCaseDoc.Version);

                                    if (!Directory.Exists(directoryPath))
                                        Directory.CreateDirectory(directoryPath);

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                    objNoticeCaseDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    objNoticeCaseDoc.FileKey = fileKey1.ToString();
                                    objNoticeCaseDoc.VersionDate = DateTime.Now;
                                    objNoticeCaseDoc.CreatedOn = DateTime.Now;
                                    objNoticeCaseDoc.FileSize = uploadedFile.ContentLength;
                                    objNoticeCaseDoc.FileName = uploadedFile.FileName;
                                    DocumentManagement.Litigation_SaveDocFiles(fileList);
                                    int FileID = CaseManagement.CreateCaseDocumentPaymentMappingGetID(objNoticeCaseDoc);
                                    if (FileID != 0)
                                    {
                                        saveSuccess = true;
                                    }

                                    List<tbl_Litigation_FileDataTagsMapping> lstFileTagMapping = new List<tbl_Litigation_FileDataTagsMapping>();


                                    tbl_Litigation_FileDataTagsMapping objFileTagMapping = new tbl_Litigation_FileDataTagsMapping()
                                    {
                                        FileID = FileID,
                                        IsActive = true,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                        UpdatedOn = DateTime.Now,
                                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    };
                                    lstFileTagMapping.Add(objFileTagMapping);


                                    if (lstFileTagMapping.Count > 0)
                                    {
                                        saveSuccess = CaseManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                    }

                                    fileList.Clear();
                                }
                            }//End For Each
                        }
                    }
                    #endregion
                }
                return saveSuccess;
                   
                //return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        #region[Export to excel]
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {

                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditLogDetail");
                        DataTable ExcelData = null;
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {



                            if (ViewState["CaseInstanceID"] != null)
                            {
                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                                var Data = (from row in entities.SP_LitigationCaseNoticeAuditLog(customerID, caseInstanceID, "C")
                                            where row.IsVisibleToUser == true
                                            select row).OrderByDescending(entry => entry.CreatedOn).ToList();
                       
                            var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerID));
                            DataTable table = Data.ToDataTable();

                            DataView view = new System.Data.DataView(table);
                            List<SP_LitigationCaseNoticeAuditLog_Result> cb = new List<SP_LitigationCaseNoticeAuditLog_Result>();

                            ExcelData = view.ToTable("Selected", false, "Remark", "CreatedByUser", "CreatedOn");

                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);

                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;
                            }

                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Value = "Customer Name:";

                            exWorkSheet.Cells["B1"].Merge = true;
                            exWorkSheet.Cells["B1"].Value = cname;

                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Value = "Report Name:";

                            //exWorkSheet.Cells["B2:C2"].Merge = true;
                            exWorkSheet.Cells["B2"].Value = "audit Log Report";

                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                            //exWorkSheet.Cells["B3:C3"].Merge = true;
                            exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");


                            exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A4"].Value = "Sr.No.";
                            exWorkSheet.Cells["A4"].AutoFitColumns(20);

                            exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B4"].Value = "Remark";
                            exWorkSheet.Cells["B4"].AutoFitColumns(40);

                            exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C4"].Value = "Created By";
                            exWorkSheet.Cells["C4"].AutoFitColumns(40);

                            exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D4"].Value = "Created On";
                            exWorkSheet.Cells["D4"].AutoFitColumns(25);
                           

                        }


                        }
                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 4 + ExcelData.Rows.Count,4])
                        {

                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                        }
                    using (ExcelRange col = exWorkSheet.Cells[1, 2, 4 + ExcelData.Rows.Count, 4])
                    {
                        col[1, 2, 4+ ExcelData.Rows.Count, 4].Style.Numberformat.Format = "dd/MM/yyyy hh:mm:ss";
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=AuditLogDetails.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }

                //}
                //else
                //{

                //    cvCaseAuditLog.IsValid = false;
                //    cvCaseAuditLog.ErrorMessage = "Please select customer";



                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        #endregion
        public void DeletePaymentLog(int noticePaymentID)
        {
            try
            {
                if (noticePaymentID != 0)
                {
                    if (CaseManagement.DeleteCasePaymentLog(noticePaymentID, AuthenticationHelper.UserID))
                    {
                        cvCasePayment.IsValid = false;
                        cvCasePayment.ErrorMessage = "Payment Details Deleted Successfully.";
                        ValidationSummary4.CssClass = "alert alert-success";

                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_NoticeCasePayment", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Payment Detail Deleted", true);
                    }
                    else
                    {
                        cvCasePayment.IsValid = false;
                        cvCasePayment.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary4.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        #endregion

        #region Case-Order

        public void clearOrderControls()
        {
            try
            {
                tbxOrderDate.Text = "";
                tbxOrderTitle.Text = "";
                tbxOrderTitle.Text = "";
                tbxOrderDesc.Text = "";
                tbxOrderRemark.Text = "";

                fuCaseOrderDocUpload.Attributes.Clear();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindCaseOrders(int caseInstanceID)
        {
            try
            {
                var lstCaseOrders = CaseManagement.GetCaseOrderDetails(caseInstanceID);

                grdCaseOrder.DataSource = lstCaseOrders;
                grdCaseOrder.DataBind();

                lstCaseOrders.Clear();
                lstCaseOrders = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCaseOrderPopup.IsValid = false;
                cvCaseOrderPopup.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        public string ShowOrderType(int orderTypeID)
        {
            try
            {
                return CaseManagement.GetOrderTypeByID(orderTypeID, "O");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowOrderDocCount(long CaseInstanceID, long orderID)
        {
            try
            {
                var docCount = CaseManagement.GetCaseOrderDocuments(CaseInstanceID, orderID, "CO").Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        public string ShowadvbillDocCount(long CaseInstanceID, long advbillID)
        {
            try
            {
                var docCount = CaseManagement.GetCaseadvbillDocuments(CaseInstanceID, advbillID, "CA").Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected void grdCaseOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOrderType = (Label)e.Row.FindControl("lblOrderType");

                if (lblOrderType != null)
                {
                    lblOrderType.ToolTip = lblOrderType.Text;
                }

                LinkButton lnkBtnDownloadOrderDoc = (LinkButton)e.Row.FindControl("lnkBtnDownloadOrderDoc");

                if (lnkBtnDownloadOrderDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDownloadOrderDoc);
                }

                LinkButton lnkBtnDeleteOrder = (LinkButton)e.Row.FindControl("lnkBtnDeleteOrder");

                if (lnkBtnDeleteOrder != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteOrder);

                    if (ViewState["caseStatus"] != null)
                    {
                        if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                            lnkBtnDeleteOrder.Visible = false;
                        else
                            lnkBtnDeleteOrder.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                    {
                        lnkBtnDeleteOrder.Visible = false;
                    }
                }
            }
        }

        protected void grdCaseOrder_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdCaseOrder.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Related Documents
                    BindCaseOrders(Convert.ToInt32(ViewState["CaseInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCaseOrder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (ViewState["CaseInstanceID"] != null)
                    {
                        int CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        int OrderID = Convert.ToInt32(e.CommandArgument);

                        ViewState["OrderID"] = OrderID;

                        if (e.CommandName.Equals("DeleteOrder"))
                        {
                            if (CaseInstanceID != 0 && OrderID != 0)
                            {
                                DeleteCaseOrder(CaseInstanceID, OrderID);

                                //Re-Bind Case Orders
                                BindCaseOrders(Convert.ToInt32(ViewState["CaseInstanceID"]));
                            }
                        }
                        else if (e.CommandName.Equals("EditCaseOrder"))
                        {
                            if (CaseInstanceID != 0 && OrderID != 0)
                            {
                                var CaseOrder = CaseManagement.GetCaseOrderDetailsByID(CaseInstanceID, OrderID);
                                if (CaseInstanceID != 0 && OrderID != 0)
                                {
                                    tbxOrderID.Text = Convert.ToString(OrderID);
                                    ddlOrderType.SelectedValue = Convert.ToString(CaseOrder.OrderTypeID);
                                    tbxOrderTitle.Text = CaseOrder.OrderTitle;
                                    tbxOrderDesc.Text = CaseOrder.OrderDesc;
                                    tbxOrderRemark.Text = CaseOrder.OrderRemark;
                                    //Convert.ToInt32(ViewState["CaseInstanceID"]) = CaseOrder.CaseInstanceID;
                                    tbxOrderDate.Text = Convert.ToDateTime(CaseOrder.OrderDate).ToString("dd-MM-yyyy");

                                    var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, OrderID, "CO");
                                    if (lstResponseDocument.Count > 0)
                                    {
                                        GrdOrderEditDocument.DataSource = lstResponseDocument;
                                        GrdOrderEditDocument.DataBind();
                                        divOrderEditdoc.Visible = true;
                                    }
                                    ViewState["OrderMode"] = "Edit";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script5", "HidShowOrderDivForEdit();", true);
                                }
                                //Re-Bind Case Orders
                                //BindCaseOrders(Convert.ToInt32(ViewState["CaseInstanceID"]));
                            }
                        }
                        else if (e.CommandName.Equals("DownloadCaseOrder"))
                        {
                            if (CaseInstanceID != 0 && OrderID != 0)
                            {
                                var lstCaseOrderDocument = CaseManagement.GetCaseOrderDocuments(CaseInstanceID, OrderID, "CO");

                                if (lstCaseOrderDocument.Count > 0)
                                {
                                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                    if (AWSData != null)
                                    {
                                        #region AWS
                                        using (ZipFile responseDocZip = new ZipFile())
                                        {
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                            
                                            string directoryPath = "~/TempDocuments/AWS/" + User;
                                            
                                            if (!Directory.Exists(directoryPath))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                                            }
                                            foreach (var item in lstCaseOrderDocument)
                                            {
                                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                                {
                                                    GetObjectRequest request = new GetObjectRequest();
                                                    request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                                    request.Key = item.FileName;
                                                    GetObjectResponse response = client.GetObject(request);
                                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                                }
                                            }
                                            int i = 0;
                                            foreach (var file in lstCaseOrderDocument)
                                            {
                                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                                
                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    int idx = file.FileName.LastIndexOf('.');
                                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                                    }
                                                }
                                                i++;
                                            }
                                            var zipMs = new MemoryStream();
                                            responseDocZip.Save(zipMs);
                                            zipMs.Position = 0;
                                            byte[] Filedata = zipMs.ToArray();
                                            Response.Buffer = true;
                                            Response.ClearContent();
                                            Response.ClearHeaders();
                                            Response.Clear();
                                            Response.ContentType = "application/zip";
                                            Response.AddHeader("content-disposition", "attachment; filename=CaseOrderDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                            Response.BinaryWrite(Filedata);
                                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Order Document(s) Downloaded", true);
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Normal
                                        using (ZipFile responseDocZip = new ZipFile())
                                        {
                                            foreach (var file in lstCaseOrderDocument)
                                            {
                                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    int idx = file.FileName.LastIndexOf('.');
                                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                    {
                                                        if (file.EnType == "M")
                                                        {
                                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                        else
                                                        {
                                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                    }
                                                }
                                            }
                                            var zipMs = new MemoryStream();
                                            responseDocZip.Save(zipMs);
                                            zipMs.Position = 0;
                                            byte[] Filedata = zipMs.ToArray();
                                            Response.Buffer = true;
                                            Response.ClearContent();
                                            Response.ClearHeaders();
                                            Response.Clear();
                                            Response.ContentType = "application/zip";
                                            Response.AddHeader("content-disposition", "attachment; filename=CaseOrderDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                            Response.BinaryWrite(Filedata);
                                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Order Document(s) Downloaded", true);
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    cvCasePopUpTask.IsValid = false;
                                    cvCasePopUpTask.ErrorMessage = "No Document Available for Download.";
                                    ValidationSummary5.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                        }
                        else if (e.CommandName.Equals("ViewCaseOrder"))
                        {
                            var lstCaseOrderDocument = CaseManagement.GetCaseOrderDocuments(CaseInstanceID, OrderID, "CO");

                            if (lstCaseOrderDocument != null)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS
                                    List<tbl_LitigationFileData> entitiesData = lstCaseOrderDocument.Where(entry => entry.Version != null).ToList();
                                    if (lstCaseOrderDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                        entityData.Version = "1.0";
                                        entityData.DocTypeInstanceID = OrderID;
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptDocmentVersionView.DataBind();

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        foreach (var file in lstCaseOrderDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string extension = System.IO.Path.GetExtension(file.FileName);
                                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                                {
                                                    lblMessage.Text = "";
                                                    lblMessage.Text = "Zip file can't view please download it";
                                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview();", true);
                                                }
                                                else
                                                {
                                                    string filePath1 = directoryPath + "/" + file.FileName;
                                                    DocumentPath = filePath1;
                                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                                    lblMessage.Text = "";
                                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                                }                                                
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                            }
                                            break;
                                        }
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal
                                    List<tbl_LitigationFileData> entitiesData = lstCaseOrderDocument.Where(entry => entry.Version != null).ToList();
                                    if (lstCaseOrderDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                        entityData.Version = "1.0";
                                        entityData.DocTypeInstanceID = OrderID;
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        foreach (var file in lstCaseOrderDocument)
                                        {
                                            rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                            rptDocmentVersionView.DataBind();
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string Folder = "~/TempFiles";
                                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                                string DateFolder = Folder + "/" + File;

                                                string extension = System.IO.Path.GetExtension(filePath);

                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                DocumentPath = FileName;

                                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                                lblMessage.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                            }
                                            break;
                                        }
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary5.CssClass = "alert alert-danger";
            }
        }

        public void DeleteCaseOrder(long caseInstanceID, int orderID)
        {
            try
            {
                if (orderID != 0)
                {
                    if (CaseManagement.DeleteCaseOrderLog(caseInstanceID, orderID, AuthenticationHelper.UserID, "CO"))
                    {
                        LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LegalCaseOrder", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Order Deleted", true);
                        cvCaseOrderPopup.IsValid = false;
                        cvCaseOrderPopup.ErrorMessage = "Order Detail Deleted Successfully.";
                        ValidationSummary2.CssClass = "alert alert-success";
                    }
                    else
                    {
                        cvCaseOrderPopup.IsValid = false;
                        cvCaseOrderPopup.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary2.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCaseOrderPopup.IsValid = false;
                cvCaseOrderPopup.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        protected void btnUploadCaseDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    bool saveSuccess = false;
                    long caseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);
                    int DocTypeID = -1;
                    if (caseInstanceID > 0)
                    {
                        #region Upload Document

                        if (CaseFileUpload.HasFiles)
                        {
                            tbl_LitigationFileData objCaseDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = caseInstanceID,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,
                                DocTypeID = DocTypeID,
                                DocType = "C"
                            };

                            HttpFileCollection fileCollection = Request.Files;

                            if (fileCollection.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                string directoryPath = "";
                                String fileName = "";

                                if (caseInstanceID > 0)
                                {
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection[i];

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            string[] keys1 = fileCollection.Keys[i].Split('$');

                                            if (keys1[keys1.Count() - 1].Equals("CaseFileUpload"))
                                            {
                                                fileName = uploadedFile.FileName;
                                            }

                                            objCaseDoc.FileName = fileName;

                                            //Get Document Version
                                            var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objCaseDoc);

                                            caseDocVersion++;
                                            objCaseDoc.Version = caseDocVersion + ".0";

                                            directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(caseInstanceID) + "/CaseDocument/" + objCaseDoc.Version);

                                            if (!Directory.Exists(directoryPath))
                                                Directory.CreateDirectory(directoryPath);

                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                            Stream fs = uploadedFile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                            objCaseDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objCaseDoc.FileKey = fileKey1.ToString();
                                            objCaseDoc.VersionDate = DateTime.Now;
                                            objCaseDoc.CreatedOn = DateTime.Now;
                                            objCaseDoc.FileSize = uploadedFile.ContentLength;
                                            DocumentManagement.Litigation_SaveDocFiles(fileList);
                                            saveSuccess = CaseManagement.CreateCaseDocumentMapping(objCaseDoc);

                                            fileList.Clear();
                                        }

                                    }//End For Each  

                                    if (saveSuccess)
                                    {
                                        LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document(s) Uploaded", true);
                                        BindCaseRelatedDocuments_All();
                                    }
                                }
                            }

                            if (saveSuccess)
                            {
                                cvCaseDocument.IsValid = false;
                                cvCaseDocument.ErrorMessage = "Document(s) Uploaded Successfully";
                                vsCaseDocument.CssClass = "alert alert-success";
                            }
                            else
                            {
                                cvCaseDocument.IsValid = false;
                                cvCaseDocument.ErrorMessage = "Something went wrong, during document upload, Please try again";
                                vsCaseDocument.CssClass = "alert alert-danger";
                            }
                        }
                        else
                        {
                            cvCaseDocument.IsValid = false;
                            cvCaseDocument.ErrorMessage = "No document selected to upload";
                            vsCaseDocument.CssClass = "alert alert-danger";
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnOrderSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    bool isBlankFile = false;
                    int DocTypeID = -1;
                    if (!string.IsNullOrEmpty(ddlOrderType.SelectedValue) && ddlOrderType.SelectedValue != "-1")
                    {
                        if (tbxOrderTitle.Text != "")
                        {
                            if (tbxOrderDate.Text != "")
                            {
                                if (tbxOrderDesc.Text != "")
                                {
                                    validateData = true;
                                }
                                else
                                {
                                    cvCasePopUpTask.IsValid = false;
                                    cvCasePopUpTask.ErrorMessage = "Provide Order Description.";
                                    ValidationSummary5.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvCasePopUpTask.IsValid = false;
                                cvCasePopUpTask.ErrorMessage = "Provide Order Date.";
                                ValidationSummary5.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                        else
                        {
                            cvCasePopUpTask.IsValid = false;
                            cvCasePopUpTask.ErrorMessage = "Provide Order Title.";
                            ValidationSummary5.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                    else
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Select Order Type.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                        return;
                    }


                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < InvalidFileTypes.Length; j++)
                                        {
                                            if (ext == "." + InvalidFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                   
                    tbl_LegalCaseOrder newRecord = new tbl_LegalCaseOrder();

                    if (validateData && isBlankFile==false)
                    {
                        long newOrderID = 0;

                        newRecord.IsActive = true;
                        newRecord.OrderTypeID = Convert.ToInt32(ddlOrderType.SelectedValue);
                        newRecord.OrderTitle = tbxOrderTitle.Text.Trim();
                        newRecord.OrderDesc = tbxOrderDesc.Text.Trim();
                        newRecord.CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        newRecord.OrderDate = DateTimeExtensions.GetDate(tbxOrderDate.Text);
                        newRecord.CreatedBy = AuthenticationHelper.UserID;
                        newRecord.CreatedByText = AuthenticationHelper.User;
                        newRecord.UserID = AuthenticationHelper.UserID;
                        newRecord.RoleID = 3;

                        if (tbxOrderRemark.Text != "")
                            newRecord.OrderRemark = tbxOrderRemark.Text.Trim();

                        //newOrderID = CaseManagement.CreateCaseOrderLog(newRecord);
                        
                      //  var neworID = CaseManagement.GetOrderinfo(Convert.ToInt32(newOrderID));
                        if (Convert.ToString(ViewState["OrderMode"]) == "Edit")
                         
                        {
                            newRecord.ID = Convert.ToInt32(tbxOrderID.Text);
                            newOrderID = CaseManagement.UpdateCaseOrderLog(newRecord);
                        }
                        else
                        {
                            newOrderID = CaseManagement.CreateCaseOrderLog(newRecord);
                        }

                        if (newOrderID > 0)
                            saveSuccess = true;

                        if (saveSuccess)
                        {
                            LitigationManagement.CreateAuditLog("C", newRecord.CaseInstanceID, "tbl_LegalCaseOrder", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Order Added", true);

                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS Upload Document

                                if (fuCaseOrderDocUpload.HasFiles)
                                {
                                    tbl_LitigationFileData objOrderDoc = new tbl_LitigationFileData()
                                    {
                                        NoticeCaseInstanceID = newRecord.CaseInstanceID,
                                        DocTypeInstanceID = newRecord.ID, //OrderID
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedByText = AuthenticationHelper.User,
                                        IsDeleted = false,
                                        DocTypeID = DocTypeID,
                                        DocType = "CO"
                                    };

                                    if (fileCollection.Count > 0)
                                    {
                                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                        int customerID = -1;
                                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string directoryPath = "";
                                        String fileName = "";

                                        if (newRecord.ID > 0)
                                        {
                                            for (int i = 0; i < fileCollection.Count; i++)
                                            {
                                                HttpPostedFile uploadedFile = fileCollection[i];

                                                if (uploadedFile.ContentLength > 0)
                                                {
                                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                                    if (keys1[keys1.Count() - 1].Equals("fuCaseOrderDocUpload"))
                                                    {
                                                        fileName = uploadedFile.FileName;
                                                    }

                                                    objOrderDoc.FileName = fileName;

                                                    //Get Document Version
                                                    var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objOrderDoc);

                                                    caseDocVersion++;
                                                    objOrderDoc.Version = caseDocVersion + ".0";
                                                    
                                                    directoryPath = "LitigationDocuments\\" + customerID + "\\Cases\\" + Convert.ToInt32(newRecord.CaseInstanceID) + "\\" + "Order\\" + objOrderDoc.Version;
                                                    
                                                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                    if (!di.Exists)
                                                    {
                                                        di.Create();
                                                    }

                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                    string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                                    string p_strPath = string.Empty;
                                                    string dirpath = string.Empty;
                                                    //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                                                    //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;
                                                    //if (File.Exists(p_strPath))
                                                    //    File.Delete(p_strPath);

                                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                    p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                                                    dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;
                                                    
                                                    if (!Directory.Exists(dirpath))
                                                    {
                                                        Directory.CreateDirectory(dirpath);
                                                    }
                                                    FileStream objFileStrm = File.Create(p_strPath);
                                                    objFileStrm.Close();
                                                    File.WriteAllBytes(p_strPath, bytes);

                                                    Guid fileKey1 = Guid.NewGuid();

                                                    string AWSpath = directoryPath + "\\" + uploadedFile.FileName;

                                                    objOrderDoc.FilePath = directoryPath.Replace(@"\", "/");
                                                    objOrderDoc.FileKey = fileKey1.ToString();
                                                    objOrderDoc.VersionDate = DateTime.Now;
                                                    objOrderDoc.CreatedOn = DateTime.Now;
                                                    objOrderDoc.FileSize = uploadedFile.ContentLength;
                                                    FileInfo localFile = new FileInfo(p_strPath);
                                                    S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                    if (!s3File.Exists)
                                                    {
                                                        using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                        {
                                                            localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                        }
                                                    }
                                                    //DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objOrderDoc);

                                                    fileList.Clear();
                                                }

                                            }//End For Each  
                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("C", newRecord.CaseInstanceID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Order Document(s) Uploaded", true);
                                            }
                                        }
                                    }
                                    ViewState["HearingMode"] = "Add";
                                }
                                else
                                {
                                    ViewState["HearingMode"] = "Add";
                                }

                                #endregion
                            }
                            else
                            {
                                #region Normal Upload Document

                                if (fuCaseOrderDocUpload.HasFiles)
                                {
                                    tbl_LitigationFileData objOrderDoc = new tbl_LitigationFileData()
                                    {
                                        NoticeCaseInstanceID = newRecord.CaseInstanceID,
                                        DocTypeInstanceID = newRecord.ID, //OrderID
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedByText = AuthenticationHelper.User,
                                        IsDeleted = false,
                                        DocTypeID = DocTypeID,
                                        DocType = "CO"
                                    };

                                    if (fileCollection.Count > 0)
                                    {
                                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                        int customerID = -1;
                                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string directoryPath = "";
                                        String fileName = "";

                                        if (newRecord.ID > 0)
                                        {
                                            for (int i = 0; i < fileCollection.Count; i++)
                                            {
                                                HttpPostedFile uploadedFile = fileCollection[i];

                                                if (uploadedFile.ContentLength > 0)
                                                {
                                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                                    if (keys1[keys1.Count() - 1].Equals("fuCaseOrderDocUpload"))
                                                    {
                                                        fileName = uploadedFile.FileName;
                                                    }

                                                    objOrderDoc.FileName = fileName;

                                                    //Get Document Version
                                                    var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objOrderDoc);

                                                    caseDocVersion++;
                                                    objOrderDoc.Version = caseDocVersion + ".0";

                                                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Cases/" + newRecord.CaseInstanceID + "/Order/" + objOrderDoc.Version);

                                                    if (!Directory.Exists(directoryPath))
                                                        Directory.CreateDirectory(directoryPath);

                                                    Guid fileKey1 = Guid.NewGuid();
                                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                    objOrderDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                    objOrderDoc.FileKey = fileKey1.ToString();
                                                    objOrderDoc.VersionDate = DateTime.Now;
                                                    objOrderDoc.CreatedOn = DateTime.Now;
                                                    objOrderDoc.FileSize = uploadedFile.ContentLength;
                                                    DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                    saveSuccess = CaseManagement.CreateCaseDocumentMapping(objOrderDoc);

                                                    fileList.Clear();
                                                }

                                            }//End For Each  
                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("C", newRecord.CaseInstanceID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Order Document(s) Uploaded", true);
                                            }
                                        }
                                    }
                                    ViewState["HearingMode"] = "Add";
                                }
                                else
                                {
                                    ViewState["HearingMode"] = "Add";
                                }

                                #endregion
                            }
                        }
                    }
                    else
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                    }

                    if (saveSuccess)
                    {
                        CvOrderSaveMsg.IsValid = false;
                        CvOrderSaveMsg.ErrorMessage = "Order Details Saved Successfully.";
                        ValidationSummary10.CssClass = "alert alert-success";
                        GrdOrderEditDocument.DataSource = null;
                        GrdOrderEditDocument.DataBind();
                        clearOrderControls();
                        //Re-Bind Case Orders Details
                        BindCaseOrders(Convert.ToInt32(ViewState["CaseInstanceID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnOrderClear_Click(object sender, EventArgs e)
        {
            try
            {
                clearOrderControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        protected void lnkAddNewUser_Click(object sender, EventArgs e)
        {
            BindUsers();
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(commandArgs[2]));

                    if (AllinOneDocumentList != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS                            
                            if (AllinOneDocumentList.FilePath != null)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                    request.Key = AllinOneDocumentList.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                }
                                string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                DocumentPath = filePath1;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }
                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                string FileName = DateFolder + "/" + User + "" + extension;
                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptPaymentDocmentView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AllinOneDocumentList = CaseManagement.GetCasePaymentDocumentByID(Convert.ToInt32(commandArgs[2]));

                    if (AllinOneDocumentList != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS                            
                            if (AllinOneDocumentList.FilePath != null)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                    request.Key = AllinOneDocumentList.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                }
                                string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                DocumentPath = filePath1;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewerNew('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/LitigationDocuments";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                //string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                //Directory.CreateDirectory(Server.MapPath(DateFolder));
                                //if (!Directory.Exists(DateFolder))
                                //{
                                //    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                //}
                                //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                //string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                ////string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                string FileName = extension;
                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                //if (AllinOneDocumentList.EnType == "M")
                                //{
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                //}
                                //else
                                //{
                                //bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                //}
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewerNew('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewNew();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptPaymentDocmentView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentView = (LinkButton)e.Item.FindControl("lblDocumentView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentView);
            }
        }
        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void grdLawyerRating_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Rating Rating = (Rating)e.Row.FindControl("LawyerRating");

                if (Rating != null)
                {

                }
            }
        }

        protected void grdLawyerRating_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ViewState["CaseInstanceID"].ToString()))
                {
                    if (!string.IsNullOrEmpty(ddlLayerType.SelectedValue))
                    {
                        int NoticeCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        grdLawyerRating.PageIndex = e.NewPageIndex;
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var documentData = (from row in entities.sp_LiDisplayCriteriaRatingCase(NoticeCaseInstanceID, Convert.ToInt32(ddlLayerType.SelectedValue), Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                select row).ToList();

                            if (documentData != null)
                            {
                                grdLawyerRating.DataSource = documentData;
                                grdLawyerRating.DataBind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveLawRating_Click(object sender, EventArgs e)
        {
            try
            {
                int lblLawyerID = 0;
                long caseInstanceID = 0;
                if (!string.IsNullOrEmpty((ViewState["CaseInstanceID"]).ToString()))
                {
                    caseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);

                    List<tbl_LawyerListRating> objMainList = new List<tbl_LawyerListRating>();

                    for (int i = 0; i < grdLawyerRating.Rows.Count; i++)
                    {
                        //int lblLawyerID = (Label) grdLawyerRating.Rows[i].Cells[3].FindControl("lblLawyerID");
                        // TextBox tbxRating = (TextBox) grdLawyerRating.Rows[i].Cells[2].FindControl("tbxLawyerRating");
                        int lblType = 1;
                        Label lblCriteriaID = (Label)grdLawyerRating.Rows[i].Cells[3].FindControl("lblCriteriaID");
                        lblLawyerID = Convert.ToInt32(ddlLayerType.SelectedValue);
                        Rating LawyerRating = (Rating)grdLawyerRating.Rows[i].Cells[2].FindControl("LawyerRating");

                        if (caseInstanceID != 0 && lblLawyerID > 0 && lblType > 0 && LawyerRating != null && lblCriteriaID != null)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(LawyerRating.CurrentRating)))
                            {
                                //if (Convert.ToDecimal(tbxRating.Text) <= 5)
                                if (Convert.ToDecimal(LawyerRating.CurrentRating) <= 5)
                                {
                                    if (Convert.ToDecimal(LawyerRating.CurrentRating) != 0)
                                    {
                                        tbl_LawyerListRating objLawRating = new tbl_LawyerListRating()
                                        {
                                            Type = Convert.ToInt32(lblType),
                                            CaseNoticeID = caseInstanceID,
                                            LawyerID = lblLawyerID,
                                            Rating = Convert.ToDecimal(LawyerRating.CurrentRating),

                                            CreatedBy = AuthenticationHelper.UserID,
                                            UserID = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            IsActive = true,
                                            CriteriaRatingID = Convert.ToInt32(lblCriteriaID.Text)
                                        };

                                        objMainList.Add(objLawRating);
                                    }
                                }
                                else
                                {
                                    CvValidLaywRating.IsValid = false;
                                    CvValidLaywRating.ErrorMessage = "Rating value should be between 0 to 5(i.e. 0.5 or 4)";
                                    ValidationSummary6.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                        }
                    }

                    bool saveSuccess = false;
                    if (objMainList.Count > 0)
                        saveSuccess = CaseManagement.SaveLaywerRatingListData(objMainList);

                    if (saveSuccess)
                    {
                        LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LawyerListRating", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Rating Assigned to Lawyer(s)", true);

                        CvValidLaywRating.IsValid = false;
                        CvValidLaywRating.ErrorMessage = "Rating Saved Successfully";
                        ValidationSummary6.CssClass = "alert alert-success";
                    }

                    if (saveSuccess)
                    {
                        objMainList.ForEach(eachRatingRecord =>
                        {
                            decimal LawyerRating = CaseManagement.GetLawyerRating(Convert.ToInt32(eachRatingRecord.LawyerID));

                            if (!string.IsNullOrEmpty(Convert.ToString(LawyerRating)))
                            {
                                tbl_LawyerFinalRating objNewRate = new tbl_LawyerFinalRating()
                                {
                                    LawyerID = Convert.ToInt32(eachRatingRecord.LawyerID),
                                    Rating = LawyerRating,
                                    IsActive = true
                                };

                                if (CaseManagement.CheckIsExistLawyerRating(objNewRate))
                                {
                                    objNewRate.CreatedBy = AuthenticationHelper.UserID;
                                    objNewRate.CreatedOn = DateTime.Now;
                                    CaseManagement.CreateLawyerFinalRating(objNewRate);
                                    //CvValidLaywRating.IsValid = false;
                                    //CvValidLaywRating.ErrorMessage = "Rating Saved Successfully";
                                }
                                else
                                {
                                    objNewRate.UpdatedBy = AuthenticationHelper.UserID;
                                    objNewRate.UpdatedOn = DateTime.Now;
                                    CaseManagement.UpdateLawyerFinalRating(objNewRate);
                                    //CvValidLaywRating.IsValid = false;
                                    //CvValidLaywRating.ErrorMessage = "Rating Updated Successfully";
                                }
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdShowDocumentList_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void grdShowDocumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdShowDocumentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdShowDocumentList.PageIndex = e.NewPageIndex;

                    //Re-Bind Case Payments
                    BindMailDocumentList(Convert.ToInt32(ViewState["CaseInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void chkAllDocument_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)grdShowDocumentList.HeaderRow.FindControl("chkAllDocument");

            foreach (GridViewRow row in grdShowDocumentList.Rows)
            {
                CheckBox checkDoc = (CheckBox)row.FindControl("chkDocument");

                if (ChkBoxHeader.Checked)
                    checkDoc.Checked = true;
                else
                    checkDoc.Checked = false;
            }

            ShowSelectedRecords(sender, e);
        }

        protected void chkDocument_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)grdShowDocumentList.HeaderRow.FindControl("chkAllDocument");
            int Count = 0;
            foreach (GridViewRow row in grdShowDocumentList.Rows)
            {
                CheckBox checkDoc = (CheckBox)row.FindControl("chkDocument");

                if (checkDoc.Checked)
                {
                    Count++;
                }
            }

            if (Count == grdShowDocumentList.Rows.Count)
                ChkBoxHeader.Checked = true;
            else
                ChkBoxHeader.Checked = false;

            ShowSelectedRecords(sender, e);
        }

        private void ShowCheckBoxCheckedValue()
        {
            try
            {
                ArrayList DocumentList = (ArrayList)ViewState["ListofFile"];

                if (DocumentList != null && DocumentList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdShowDocumentList.Rows)
                    {
                        Label lblID = (Label)gvrow.FindControl("lblID");
                        Label lblFileName = (Label)gvrow.FindControl("lblFileName");

                        if (DocumentList.Contains(lblID.Text))
                        {
                            CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkDocument");
                            myCheckBox.Checked = true;
                        }
                    }
                    if (DocumentList.Count > 0)
                    {
                        lblTotalSelected.Text = DocumentList.Count + " Selected";
                        btnSendDocumentMail.Visible = true;
                    }
                    else if (DocumentList.Count == 0)
                    {
                        lblTotalSelected.Text = "";
                        btnSendDocumentMail.Visible = false;
                    }
                }
                else
                {
                    lblTotalSelected.Text = "";
                    btnSendDocumentMail.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            lblTotalSelected.Text = "";

            GetCheckBoxValue();

            ShowCheckBoxCheckedValue();
        }

        private void GetCheckBoxValue()
        {
            try
            {
                ArrayList DocumentList = new ArrayList();
                ArrayList DocumentFileNameList = new ArrayList();

                List<Tuple<string, string>> lstDocsToAttach = new List<Tuple<string, string>>();

                foreach (GridViewRow GridDoc in grdShowDocumentList.Rows)
                {
                    Label lblID = (Label)GridDoc.FindControl("lblID");
                    Label lblFileName = (Label)GridDoc.FindControl("lblFileName");
                    Label lblFilePath = (Label)GridDoc.FindControl("lblFilePath");

                    bool result = ((CheckBox)GridDoc.FindControl("chkDocument")).Checked;

                    if (result && !string.IsNullOrEmpty(lblFilePath.Text) && !string.IsNullOrEmpty(lblFileName.Text))
                    {


                        var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(lblID.Text));
                        if (AllinOneDocumentList != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string folderPath = "~/TempFiles/" + DateTime.Now.ToString("ddMMyyyy");
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (!Directory.Exists(folderPath))
                                    Directory.CreateDirectory(Server.MapPath(folderPath));
                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                // string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                string User = AllinOneDocumentList.FileName + "" + FileDate;
                                string FileName = folderPath + "/" + User + "" + extension;
                                //var fname= User + "" + extension;
                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                DocumentList.Add(lblID.Text);
                                DocumentFileNameList.Add(lblFilePath.Text);
                                lstDocsToAttach.Add(new Tuple<string, string>(DocumentPath, FileName));
                                ViewState["ListofFile"] = DocumentList;
                                ViewState["docsToAttach"] = lstDocsToAttach;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindMailDocumentList(int CaseNoticeInstanceID)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(CaseNoticeInstanceID)))
            {
                //var DocList = CaseManagement.GettDocumentList(CaseNoticeInstanceID, "C");
                var DocList = CaseManagement.GetCaseDocumentMapping(CaseNoticeInstanceID, "", AuthenticationHelper.CustomerID);
                if (DocList.Count > 0)
                {
                    DocList = (from g in DocList
                               group g by new
                               {
                                   g.ID,
                                   g.DocType,
                                   g.FileName,
                                   g.Version,
                                   g.CreatedByText,
                                   g.CreatedOn,
                                   g.FilePath
                               } into GCS
                               select new Sp_Litigation_CaseDocument_Result()
                               {
                                   ID = GCS.Key.ID,
                                   DocType = GCS.Key.DocType,
                                   FileName = GCS.Key.FileName,
                                   Version = GCS.Key.Version,
                                   CreatedByText = GCS.Key.CreatedByText,
                                   CreatedOn = GCS.Key.CreatedOn,
                                   FilePath = GCS.Key.FilePath
                               }).ToList();
                }
                grdShowDocumentList.DataSource = DocList;
                grdShowDocumentList.DataBind();
            }
        }

        private void BindCaseHistory(int caseInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstCaseHistory = (from row in entities.sp_LiShowNoticeCaseHistory(caseInstanceID)
                                      select row).ToList();

                if (lstCaseHistory != null)
                {
                    grdCaseHistory.DataSource = lstCaseHistory;
                    grdCaseHistory.DataBind();

                    if (lstCaseHistory.Count > 0)
                        divCaseHistory.Visible = true;
                    else
                        divCaseHistory.Visible = false;
                }
            }
        }

        private void BindLinkedCases(long caseInstanceID)
        {
            if (caseInstanceID != 0)
            {
                var lstCaseDetails = LitigationManagement.GetLinkedNoticeCaseList(Convert.ToInt32(AuthenticationHelper.CustomerID), caseInstanceID, 1);

                grdLinkedCases.DataSource = lstCaseDetails;
                grdLinkedCases.DataBind();

                if (lstCaseDetails.Count > 0)
                    divLinkedCases.Visible = true;
                else
                    divLinkedCases.Visible = false;

                lstCaseDetails.Clear();
                lstCaseDetails = null;

                upLinkedCases.Update();
            }
        }

        private void BindCaseListToLink(long caseInstanceID)
        {
            try
            {
                if (caseInstanceID != 0)
                {

                    int partyID = -1;
                    int deptID = -1;
                    int caseStatus = -1;
                    string caseType = string.Empty;
                    string financialyear = string.Empty;
                    if (!string.IsNullOrEmpty(ddlLinkCaseStatus.SelectedValue))
                    {
                        if (ddlLinkCaseStatus.SelectedValue != "-1")
                        {
                            caseStatus = Convert.ToInt32(ddlLinkCaseStatus.SelectedValue);
                        }
                    }

                    List<int> branchList = new List<int>();

                    var lstCaseDetails = CaseManagement.GetAssignedCaseList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType);

                    if (lstCaseDetails.Count > 0)
                        lstCaseDetails = lstCaseDetails.Where(row => row.CaseInstanceID != caseInstanceID).ToList();

                    var lstAlreadyLinkedCases = CaseManagement.GetLinkedCaseNoticeIDs(Convert.ToInt32(AuthenticationHelper.CustomerID), 1, caseInstanceID);

                    if (lstAlreadyLinkedCases.Count > 0)
                        lstCaseDetails = lstCaseDetails.Where(row => !lstAlreadyLinkedCases.Contains(row.CaseInstanceID)).ToList();

                    if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                    {
                        lstCaseDetails = lstCaseDetails.Where(entry => entry.CaseRefNo != null).ToList();
                        lstCaseDetails = lstCaseDetails.Where(entry => entry.CaseTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.CaseRefNo.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                    }

                    grdCaseList_LinkCase.DataSource = lstCaseDetails;
                    grdCaseList_LinkCase.DataBind();

                    lstCaseDetails.Clear();
                    lstCaseDetails = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSendDocumentMail_Click(object sender, EventArgs e)
        {
            string messageAll = string.Empty;
            int CaseInstanceID = 0;
            if (ViewState["CaseInstanceID"] != null)
            {
                CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                var CaseDetailinfo = CaseManagement.GetCaseByID(CaseInstanceID);
                string PortalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    PortalURL = Urloutput.URL;
                }
                else
                {
                    PortalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                messageAll = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_DocumentWithSummary
                                      .Replace("@message", tbxMailMsg.Text)
                                      .Replace("@CaseNo", Convert.ToString(CaseDetailinfo.CaseRefNo))
                                      .Replace("@InternalCaseNo", CaseDetailinfo.InternalCaseNo)
                                      .Replace("@CaseTitle", CaseDetailinfo.CaseTitle)
                                      .Replace("@CaseDescription", CaseDetailinfo.CaseDetailDesc)
                                      .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                                      .Replace("@PortalURL", Convert.ToString(PortalURL));
                //messageAll = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_DocumentWithSummary
                //                        .Replace("@message", tbxMailMsg.Text)
                //                        .Replace("@CaseNo", Convert.ToString(CaseDetailinfo.CaseRefNo))
                //                        .Replace("@InternalCaseNo", CaseDetailinfo.InternalCaseNo)
                //                        .Replace("@CaseTitle", CaseDetailinfo.CaseTitle)
                //                        .Replace("@CaseDescription", CaseDetailinfo.CaseDetailDesc)
                //                        .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
            }
            try
            {
                if (tbxMailTo.Text != "")
                {
                    bool sendMailSuccess = false;
                    List<string> lstTO = new List<string>();
                    List<string> lstCc = new List<string>();
                    List<string> lstBcc = new List<string>();

                    string strReceiver = string.Empty;

                    strReceiver = tbxMailTo.Text;
                    string[] Multiple = strReceiver.Split(',');

                    foreach (string multiple_email in Multiple)
                    {
                        if (multiple_email != "")
                            lstTO.Add(multiple_email.Trim());
                    }

                    List<Tuple<string, string>> attachmentwithPath = new List<Tuple<string, string>>();

                    string folderPath = string.Empty;
                    if (ViewState["docsToAttach"] != null)
                    {
                        attachmentwithPath = (List<Tuple<string, string>>)ViewState["docsToAttach"];

                        try
                        {
                            SendGridEmailManager.SendGridNewsLetterMail1(ConfigurationManager.AppSettings["SenderEmailAddress"], Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]), lstTO, lstCc, lstBcc, "Litigation Case Summary with Documents", messageAll, attachmentwithPath);
                            sendMailSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            sendMailSuccess = false;
                            CvDocListWithMail.IsValid = false;
                            CvDocListWithMail.ErrorMessage = "Something went wrong. Please try again.";
                            ValidationSummary7.CssClass = "alert alert-danger";
                        }

                        if (sendMailSuccess)
                        {
                            CvDocListWithMail.IsValid = false;
                            CvDocListWithMail.ErrorMessage = "E-Mail Sent Successfully.";
                            ValidationSummary7.CssClass = "alert alert-success";
                            lblTotalSelected.Text = "";
                            grdShowDocumentList.DataSource = null;
                            grdShowDocumentList.DataBind();
                            tbxMailMsg.Text = "";
                            tbxMailTo.Text = "";
                            btnSendDocumentMail.Visible = false;
                            LitigationManagement.CreateAuditLog("C", CaseInstanceID, "tbl_LegalCaseInstance", "Email", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Summary with Document(s) Sent as Email", true);
                        }
                    }
                }
                else
                {
                    CvDocListWithMail.IsValid = false;
                    CvDocListWithMail.ErrorMessage = "Required Subject Line and From Name.";
                    ValidationSummary7.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvDocListWithMail.IsValid = false;
                CvDocListWithMail.ErrorMessage = "Something went wrong. Please try again.";
                ValidationSummary7.CssClass = "alert alert-danger";
            }
        }

        private void intializeDataTableCustomField(GridView gridViewCustomField, GridView gridViewCustomField_History)
        {
            try
            {
                DataTable dtCustomField = new DataTable();

                DataRow drowCustomField = null;

                dtCustomField.Columns.Add(new DataColumn("LableID", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Label", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("labelValue", typeof(string)));

                drowCustomField = dtCustomField.NewRow();

                drowCustomField["LableID"] = string.Empty;
                drowCustomField["Label"] = string.Empty;
                drowCustomField["labelValue"] = string.Empty;

                dtCustomField.Rows.Add(drowCustomField);

                ViewState["dataTableCustomFields"] = dtCustomField;

                gridViewCustomField.Visible = true;
                gridViewCustomField_History.Visible = false;

                gridViewCustomField.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                gridViewCustomField.DataBind();

                //To Hide row
                gridViewCustomField.Rows[0].Visible = false;
                gridViewCustomField.Rows[0].Controls.Clear();

                //lblAddNewGround.Visible = true;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void intializeDataTableCustomField_TaxLitigation(GridView gridViewCustomField, GridView gridViewCustomField_History)
        {
            try
            {
                DataTable dtCustomField = new DataTable();

                DataRow drowCustomField = null;
                dtCustomField.Columns.Add(new DataColumn("lblID", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("LableID", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Label", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("FYear", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("labelValue", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Interest", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Penalty", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Total", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("SettlementValue", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("ProvisionInbook", typeof(string)));

                drowCustomField = dtCustomField.NewRow();
                drowCustomField["lblID"] = string.Empty;
                drowCustomField["LableID"] = string.Empty;
                drowCustomField["Label"] = string.Empty;
                drowCustomField["FYear"] = string.Empty;
                drowCustomField["labelValue"] = string.Empty;
                drowCustomField["Interest"] = string.Empty;
                drowCustomField["Penalty"] = string.Empty;
                drowCustomField["Total"] = string.Empty;
                drowCustomField["SettlementValue"] = string.Empty;
                drowCustomField["ProvisionInbook"] = string.Empty;

                dtCustomField.Rows.Add(drowCustomField);

                ViewState["dataTableCustomFields"] = dtCustomField;

                gridViewCustomField.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                gridViewCustomField.DataBind();

                //To Hide row
                gridViewCustomField.Rows[0].Visible = false;
                gridViewCustomField.Rows[0].Controls.Clear();

                //lblAddNewGround.Visible = true;

                gridViewCustomField.Visible = true;
                gridViewCustomField_History.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFields(GridView gridViewCustomField, GridView gridViewCustomField_History)
        {
            try
            {
                int CaseInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
                {
                    CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                }
                if (ddlCaseCategory.SelectedValue != null && ddlCaseCategory.SelectedValue != "0")  //&& CaseInstanceID != 0
                {
                    List<SP_Litigation_GetCustomParameters_Result> lstCustomParameters = new List<SP_Litigation_GetCustomParameters_Result>();

                    if (CaseInstanceID != 0)
                    {
                        lstCustomParameters = CaseManagement.GetCustomsFields(Convert.ToInt32(AuthenticationHelper.CustomerID), CaseInstanceID, 1, Convert.ToInt32(ddlCaseCategory.SelectedValue));

                        bool historyFlag = false;
                        if (ViewState["FlagHistory"] != null)
                        {
                            historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                        }

                        if (lstCustomParameters != null && lstCustomParameters.Count > 0)
                        {
                            ViewState["CustomefieldCount"] = lstCustomParameters.Count;

                            if (!historyFlag)
                            {
                                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                {
                                    lstCustomParameters.Add(LitigationManagement.GetColumnTotal(lstCustomParameters));

                                }
                                

                                    gridViewCustomField.DataSource = lstCustomParameters;
                                gridViewCustomField.DataBind();

                                //lblAddNewGround.Visible = true;

                                gridViewCustomField.Visible = true;
                                gridViewCustomField_History.Visible = false;
                            }
                            else if (historyFlag)
                            {
                                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                {
                                    lstCustomParameters.Add(LitigationManagement.GetColumnTotal(lstCustomParameters));

                                }
                                else


                                    gridViewCustomField_History.DataSource = lstCustomParameters;
                                gridViewCustomField_History.DataBind();

                                //lblAddNewGround.Visible = false;

                                gridViewCustomField.Visible = false;
                                gridViewCustomField_History.Visible = true;
                            }
                        }
                        else
                        {
                            SP_Litigation_GetCustomParameters_Result obj = new SP_Litigation_GetCustomParameters_Result(); //initialize empty class that may contain properties
                            lstCustomParameters.Add(obj); //Add empty object to list

                            if (!historyFlag)
                            {
                                gridViewCustomField.DataSource = lstCustomParameters; /*Assign datasource to create one row with default values for the class you have*//*Assign datasource to create one row with default values for the class you have*/
                                gridViewCustomField.DataBind(); //Bind that empty source     

                                //To Hide row
                                gridViewCustomField.Rows[0].Visible = false;
                                gridViewCustomField.Rows[0].Controls.Clear();

                                //lblAddNewGround.Visible = true;

                                gridViewCustomField.Visible = true;
                                gridViewCustomField_History.Visible = false;
                            }
                            else if (historyFlag)
                            {
                                gridViewCustomField_History.DataSource = lstCustomParameters;
                                gridViewCustomField_History.DataBind();

                                //To Hide row
                                gridViewCustomField_History.Rows[0].Visible = false;
                                gridViewCustomField_History.Rows[0].Controls.Clear();

                                //lblAddNewGround.Visible = false;

                                gridViewCustomField.Visible = false;
                                gridViewCustomField_History.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                        {
                            ////emmamiusers.Visible = true;
                            //Replace with New
                            intializeDataTableCustomField_TaxLitigation(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                        }
                        else
                        {
                            intializeDataTableCustomField(grdCustomField, grdCustomField_History);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFieldDropDown(DropDownList ddlCustomField, GridView gridViewCustomField)
        {
            try
            {
                int CaseInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
                {
                    CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                }

                if (ddlCaseCategory.SelectedValue != null && ddlCaseCategory.SelectedValue != "" && ddlCaseCategory.SelectedValue != "0") //&& CaseInstanceID != 0
                {
                    var customFields = CaseManagement.GetCustomsFieldsByCaseType(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(ddlCaseCategory.SelectedValue));

                    if (customFields.Count > 0)
                    {
                        //lblAddNewGround.Visible = true;
                        gridViewCustomField.Visible = true;

                        if (ddlCustomField.Items.Count > 0)
                            ddlCustomField.Items.Clear();

                        ddlCustomField.DataTextField = "Label";
                        ddlCustomField.DataValueField = "ID";

                        ddlCustomField.DataSource = customFields;
                        ddlCustomField.DataBind();

                        ////ddlCustomField.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                        ViewState["ddlCustomFieldFilled"] = "1";
                    }
                    else
                    {
                        //lblAddNewGround.Visible = false;
                        gridViewCustomField.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlCaseCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCaseCategory.SelectedValue != null && ddlCaseCategory.SelectedValue != "0")
            {
                ViewState["ddlCustomFieldFilled"] = null;
                ViewState["dataTableCustomFields"] = null;

                ViewState["CustomefieldCount"] = null;

                ViewState["CaseTypeUpdated"] = "false";
                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && AuthenticationHelper.CustomerID == 76)
                {

                    ////BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                    grdCustomField.Visible = false;
                    grdCustomField_History.Visible = false;
                    ////emmamiusers.Visible = true;

                }
                else if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && AuthenticationHelper.CustomerID != 76)
                {

                    BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                    grdCustomField.Visible = false;
                    grdCustomField_History.Visible = false;
                    //emmamiusers.Visible = false;
                }
                else if (!(ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim())))
                {
                    BindCustomFields(grdCustomField, grdCustomField_History);

                    grdCustomField_TaxLitigation.Visible = false;
                    grdCustomField_TaxLitigation_History.Visible = false;
                    //emmamiusers.Visible = false;

                }
            }
            else
            {
                lnkAddNewCaseCategoryModal.Visible = true;
                //emmamiusers.Visible = false;
            }
        }

        protected void grdCustomField_Common_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        TextBox tbxLabelValue = (TextBox)e.Row.FindControl("tbxLabelValue");

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            tbxLabelValue.Enabled = false;
                        }

                        //Hide Delete in Case of Total or Row with 0 ID
                        Label lblID = (Label)e.Row.FindControl("lblID");
                        LinkButton lnkBtnDeleteCustomField_TaxLitigation = (LinkButton)e.Row.FindControl("lnkBtnDeleteCustomField_TaxLitigation");


                        if (lblID != null && lnkBtnDeleteCustomField_TaxLitigation != null)
                        {
                            if (lblID.Text != "" && lblID.Text != "0")
                            {
                                e.Row.Enabled = true;
                                lnkBtnDeleteCustomField_TaxLitigation.Visible = true;
                            }
                            else
                            {
                                e.Row.Enabled = false;
                                lnkBtnDeleteCustomField_TaxLitigation.Visible = false;
                            }
                        }
                    }

                    if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        DropDownList ddlFieldName_Footer = (DropDownList)e.Row.FindControl("ddlFieldName_Footer");

                        if (ddlFieldName_Footer != null)
                        {
                            BindCustomFieldDropDown(ddlFieldName_Footer, gridView);

                            foreach (GridViewRow gvr in gridView.Rows)
                            {
                                Label lblID = (Label)gvr.FindControl("lblID");

                                if (lblID != null)
                                {
                                    if (lblID.Text != "")
                                    {
                                        if (ddlFieldName_Footer.Items.FindByValue(lblID.Text) != null)
                                            ddlFieldName_Footer.Items.Remove(ddlFieldName_Footer.Items.FindByValue(lblID.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    gridView.PageIndex = e.NewPageIndex;

                    if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && AuthenticationHelper.CustomerID != 76)
                    {
                        ////emmamiusers.Visible = true;
                        BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                    }

                    else
                    {
                        BindCustomFields(grdCustomField, grdCustomField_History);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long caseInstanceID = 0;
                bool deleteSuccess = false;

                if (ViewState["CaseInstanceID"] != null && e.CommandName.Equals("DeleteCustomField") && e.CommandArgument != null && ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0 && ViewState["dataTableCustomFields"] != null)
                    {
                        GridViewRow gvRow = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                        if (gvRow != null)
                        {
                            //GridViewRow gvRow = (GridViewRow)(sender).Parent.Parent;
                            int index = gvRow.RowIndex;

                            DataTable dtCustomField = ViewState["dataTableCustomFields"] as DataTable;
                            dtCustomField.Rows[index].Delete();

                            ViewState["dataTableCustomFields"] = dtCustomField;
                            ViewState["CustomefieldCount"] = dtCustomField.Rows.Count;

                            GridView gridView = (GridView)sender;

                            if (gridView != null)
                            {
                                gridView.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                                gridView.DataBind();
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        int LableID = Convert.ToInt32(e.CommandArgument);

                        caseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);

                        if (LableID != 0 && caseInstanceID != 0)
                        {
                            deleteSuccess = CaseManagement.DeleteCustomsFieldByCaseID(1, caseInstanceID, LableID);
                            if (deleteSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_NoticeCaseCustomParameter", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Deleted", true);

                                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && AuthenticationHelper.CustomerID != 76)
                                {
                                    ////emmamiusers.Visible = true;
                                    BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                                }

                                else
                                {
                                    BindCustomFields(grdCustomField, grdCustomField_History);
                                }
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnAddCustomField_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    int lblID = 0;

                    DropDownList ddlFieldName_Footer = (DropDownList)grdCustomField.FooterRow.FindControl("ddlFieldName_Footer");
                    TextBox txtFieldValue_Footer = (TextBox)grdCustomField.FooterRow.FindControl("txtFieldValue_Footer");

                    if (ddlFieldName_Footer != null && txtFieldValue_Footer != null)
                    {
                        if ((int)ViewState["Mode"] == 0)
                        {
                            string lblName = ddlFieldName_Footer.SelectedItem.Text;
                            if (!string.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue))
                            {
                                lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);
                            }

                            if (ViewState["dataTableCustomFields"] != null && lblID != 0)
                            {
                                DataTable dtcurrentTableCumtomFields = (DataTable)ViewState["dataTableCustomFields"];

                                DataRow drNewRow = dtcurrentTableCumtomFields.NewRow();

                                drNewRow["LableID"] = lblID;
                                drNewRow["Label"] = lblName;
                                drNewRow["labelValue"] = txtFieldValue_Footer.Text;

                                //add new row to DataTable
                                dtcurrentTableCumtomFields.Rows.Add(drNewRow);

                                //Delete Rows with blank LblID (if Any)
                                dtcurrentTableCumtomFields = LitigationManagement.LoopAndDeleteBlankRows(dtcurrentTableCumtomFields);

                                ViewState["CustomefieldCount"] = dtcurrentTableCumtomFields.Rows.Count;

                                //Store the current data to ViewState
                                ViewState["CurrentTablePutCallDtls"] = dtcurrentTableCumtomFields;

                                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                {
                                    dtcurrentTableCumtomFields = LitigationManagement.GetColumnTotal(dtcurrentTableCumtomFields, true);
                                    ////emmamiusers.Visible = true;
                                    //Rebind the Grid with the current data
                                    grdCustomField_TaxLitigation.DataSource = dtcurrentTableCumtomFields;
                                    grdCustomField_TaxLitigation.DataBind();
                                }
                                else
                                {
                                    //Rebind the Grid with the current data
                                    grdCustomField.DataSource = dtcurrentTableCumtomFields;
                                    grdCustomField.DataBind();
                                }
                            }
                        }//Add Mode End
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["CaseInstanceID"] != null)
                            {
                                long caseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);

                                if (txtFieldValue_Footer != null && ddlFieldName_Footer != null && caseInstanceID != 0)
                                {
                                    bool validateData = false;
                                    bool saveSuccess = false;

                                    if (!String.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue) && ddlFieldName_Footer.SelectedValue != "0")
                                    {
                                        if (txtFieldValue_Footer.Text != "")
                                        {
                                            validateData = true;
                                        }
                                    }

                                    if (validateData)
                                    {
                                        lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);

                                        if (lblID != 0)
                                        {
                                            tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                            {
                                                NoticeCaseType = 1,
                                                NoticeCaseInstanceID = caseInstanceID,
                                                LabelID = lblID,
                                                LabelValue = txtFieldValue_Footer.Text,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);

                                            //if (CaseManagement.IsExistCustomeFieldParameterValue(ObjParameter))
                                            //{
                                            //    CaseManagement.CreateCustomeFieldParameterValue(ObjParameter);
                                            //    saveSuccess = true;
                                            //}

                                            if (saveSuccess)
                                            {
                                                LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_NoticeCaseCustomParameter", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Added", true);

                                                if (ViewState["CaseTypeUpdated"] != null)
                                                {
                                                    if (ViewState["CaseTypeUpdated"].ToString() == "false")
                                                    {
                                                        saveSuccess = CaseManagement.UpdateCaseType(caseInstanceID, Convert.ToInt32(ddlCaseCategory.SelectedValue));
                                                        if (saveSuccess)
                                                        {
                                                            saveSuccess = LitigationManagement.DeletePreviousCustomParameter(1, caseInstanceID, Convert.ToInt32(ddlCaseCategory.SelectedValue));

                                                            if (saveSuccess)
                                                                ViewState["CaseTypeUpdated"] = "true";
                                                        }
                                                    }
                                                }

                                                //Re-Bind Case Custom Parameter Details
                                                if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                                {
                                                    BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                                                    ////emmamiusers.Visible = true;
                                                }
                                                else
                                                {
                                                    BindCustomFields(grdCustomField, grdCustomField_History);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }//Edit Mode End
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnAddCustomField_TaxLitigation_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    LinkButton lnkBtn = (LinkButton)sender;
                    GridViewRow gvRow = (GridViewRow)lnkBtn.NamingContainer;
                    GridView gridView = (GridView)gvRow.NamingContainer;

                    if (gridView != null)
                    {
                        int lblID = 0;
                        DropDownList ddlFieldName_Footer = (DropDownList)gridView.FooterRow.FindControl("ddlFieldName_Footer");

                        TextBox txtFieldValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtFieldValue_Footer");
                        TextBox txtInterestValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtInterestValue_Footer");
                        TextBox txtPenaltyValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtPenaltyValue_Footer");
                        TextBox tbxRowTotalValue_Footer = (TextBox)gridView.FooterRow.FindControl("tbxRowTotalValue_Footer");

                        TextBox tbxSettlement_Footer = (TextBox)gridView.FooterRow.FindControl("tbxSettlement_Footer");
                        TextBox tbxProvisionInbooks_Footer = (TextBox)gridView.FooterRow.FindControl("tbxProvisionInbooks_Footer");

                        if (ddlFieldName_Footer != null && txtFieldValue_Footer != null
                            && txtInterestValue_Footer != null && txtPenaltyValue_Footer != null && tbxRowTotalValue_Footer != null
                            && tbxSettlement_Footer != null && tbxProvisionInbooks_Footer != null)
                        {
                            if ((int)ViewState["Mode"] == 0)
                            {
                                string lblName = ddlFieldName_Footer.SelectedItem.Text;
                                // lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);
                                if (!string.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue))
                                {
                                    lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);
                                }
                                if (ViewState["dataTableCustomFields"] != null && lblID != 0)
                                {
                                    DataTable dtcurrentTableCumtomFields = (DataTable)ViewState["dataTableCustomFields"];

                                    DataRow drNewRow = dtcurrentTableCumtomFields.NewRow();

                                    drNewRow["LableID"] = lblID;
                                    drNewRow["Label"] = lblName;
                                    drNewRow["labelValue"] = txtFieldValue_Footer.Text;
                                    drNewRow["Interest"] = txtInterestValue_Footer.Text;
                                    drNewRow["Penalty"] = txtPenaltyValue_Footer.Text;
                                    drNewRow["Total"] = tbxRowTotalValue_Footer.Text;
                                    drNewRow["SettlementValue"] = tbxSettlement_Footer.Text;
                                    drNewRow["ProvisionInbook"] = tbxProvisionInbooks_Footer.Text;

                                    //add new row to DataTable
                                    dtcurrentTableCumtomFields.Rows.Add(drNewRow);

                                    //Delete Rows with blank LblID (if Any)
                                    dtcurrentTableCumtomFields = LitigationManagement.LoopAndDeleteBlankRows(dtcurrentTableCumtomFields);

                                    ViewState["CustomefieldCount"] = dtcurrentTableCumtomFields.Rows.Count;

                                    //Store the current data to ViewState
                                    ViewState["CurrentTablePutCallDtls"] = dtcurrentTableCumtomFields;

                                    if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                    {
                                        dtcurrentTableCumtomFields = LitigationManagement.GetColumnTotal(dtcurrentTableCumtomFields, true);

                                        //Rebind the Grid with the current data
                                        gridView.DataSource = dtcurrentTableCumtomFields;
                                        gridView.DataBind();
                                    }
                                    else
                                    {
                                        //Rebind the Grid with the current data
                                        grdCustomField.DataSource = dtcurrentTableCumtomFields;
                                        grdCustomField.DataBind();
                                    }
                                }
                            }//Add Mode End
                            else if ((int)ViewState["Mode"] == 1)
                            {
                                if (ViewState["CaseInstanceID"] != null)
                                {
                                    long caseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);

                                    if (txtFieldValue_Footer != null && ddlFieldName_Footer != null && caseInstanceID != 0)
                                    {
                                        bool validateData = false;
                                        bool saveSuccess = false;

                                        if (!String.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue) && ddlFieldName_Footer.SelectedValue != "0")
                                        {
                                            if (txtFieldValue_Footer.Text != "")
                                            {
                                                validateData = true;
                                            }
                                        }

                                        if (validateData)
                                        {
                                            lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);

                                            if (lblID != 0)
                                            {
                                                tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                {
                                                    NoticeCaseType = 1,
                                                    NoticeCaseInstanceID = caseInstanceID,
                                                    LabelID = lblID,
                                                    LabelValue = txtFieldValue_Footer.Text,

                                                    Penalty = txtPenaltyValue_Footer.Text,
                                                    Interest = txtInterestValue_Footer.Text,
                                                    Total = tbxRowTotalValue_Footer.Text,
                                                    SettlementValue = tbxSettlement_Footer.Text,
                                                    ProvisionInBook = tbxProvisionInbooks_Footer.Text,

                                                    IsDeleted = false,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);

                                                //if (CaseManagement.IsExistCustomeFieldParameterValue(ObjParameter))
                                                //{
                                                //    CaseManagement.CreateCustomeFieldParameterValue(ObjParameter);
                                                //    saveSuccess = true;
                                                //}

                                                if (saveSuccess)
                                                {
                                                    LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_NoticeCaseCustomParameter", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Added", true);

                                                    if (ViewState["CaseTypeUpdated"] != null)
                                                    {
                                                        if (ViewState["CaseTypeUpdated"].ToString() == "false")
                                                        {
                                                            saveSuccess = CaseManagement.UpdateCaseType(caseInstanceID, Convert.ToInt32(ddlCaseCategory.SelectedValue));
                                                            if (saveSuccess)
                                                            {
                                                                saveSuccess = LitigationManagement.DeletePreviousCustomParameter(1, caseInstanceID, Convert.ToInt32(ddlCaseCategory.SelectedValue));

                                                                if (saveSuccess)
                                                                    ViewState["CaseTypeUpdated"] = "true";
                                                            }
                                                        }
                                                    }

                                                    //Re-Bind Case Custom Parameter Details
                                                    if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                                    {
                                                        BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                                                        ////emmamiusers.Visible = true;
                                                    }
                                                    else
                                                    {
                                                        BindCustomFields(grdCustomField, grdCustomField_History);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }//Edit Mode End
                        }
                    }//Grid Check
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void TextChangedInsideGridView_TextChanged(object sender, EventArgs e)
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            if (currentRow != null)
            {
                if (currentRow.RowType == DataControlRowType.DataRow)
                {
                    TextBox tbxLabelValue = (TextBox)currentRow.FindControl("tbxLabelValue");
                    TextBox tbxInterestValue = (TextBox)currentRow.FindControl("tbxInterestValue");
                    TextBox tbxPenaltyValue = (TextBox)currentRow.FindControl("tbxPenaltyValue");
                    TextBox tbxRowTotalValue = (TextBox)currentRow.FindControl("tbxRowTotalValue");

                    if (tbxLabelValue != null && tbxInterestValue != null && tbxPenaltyValue != null && tbxRowTotalValue != null)
                    {
                        tbxRowTotalValue.Text = (LitigationManagement.csvToNumber(tbxLabelValue.Text) +
                            LitigationManagement.csvToNumber(tbxInterestValue.Text) +
                            LitigationManagement.csvToNumber(tbxPenaltyValue.Text)).ToString();
                    }
                }
                else if (currentRow.RowType == DataControlRowType.Footer)
                {
                    TextBox txtFieldValue_Footer = (TextBox)currentRow.FindControl("txtFieldValue_Footer");
                    TextBox txtInterestValue_Footer = (TextBox)currentRow.FindControl("txtInterestValue_Footer");
                    TextBox txtPenaltyValue_Footer = (TextBox)currentRow.FindControl("txtPenaltyValue_Footer");
                    TextBox tbxRowTotalValue_Footer = (TextBox)currentRow.FindControl("tbxRowTotalValue_Footer");

                    if (txtFieldValue_Footer != null && txtInterestValue_Footer != null && txtPenaltyValue_Footer != null && tbxRowTotalValue_Footer != null)
                    {
                        tbxRowTotalValue_Footer.Text = (LitigationManagement.csvToNumber(txtFieldValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtInterestValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtPenaltyValue_Footer.Text)).ToString();
                    }
                }
            }
        }

        protected void grdCustomField_History_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsAllowed = (Label)e.Row.FindControl("lblIsAllowed");
                    DropDownList ddlGroundResult = (DropDownList)e.Row.FindControl("ddlGroundResult");

                    if (ddlGroundResult != null && lblIsAllowed != null)
                    {
                        if (lblIsAllowed.Text != null && lblIsAllowed.Text != "")
                        {
                            string isAllowed = "0";

                            if (lblIsAllowed.Text == "True")
                                isAllowed = "1";
                            else if (lblIsAllowed.Text == "False")
                                isAllowed = "0";

                            ddlGroundResult.ClearSelection();

                            if (ddlGroundResult.Items.FindByValue(isAllowed) != null)
                                ddlGroundResult.Items.FindByValue(isAllowed).Selected = true;
                        }
                    }

                    Label lblPenalty = (Label)e.Row.FindControl("lblPenalty");

                    if (lblPenalty != null)
                    {
                        if (string.IsNullOrEmpty(lblPenalty.Text))
                            lblPenalty.Visible = false;
                    }

                    Label lblInterest = (Label)e.Row.FindControl("lblInterest");

                    if (lblInterest != null)
                    {
                        if (string.IsNullOrEmpty(lblInterest.Text))
                            lblInterest.Visible = false;
                    }

                    Label lblSettlementValue = (Label)e.Row.FindControl("lblSettlementValue");

                    if (lblSettlementValue != null)
                    {
                        if (string.IsNullOrEmpty(lblSettlementValue.Text))
                            lblSettlementValue.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnCaseTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
                {
                    int OldCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    if (ddlCaseCategory.SelectedValue != "" && ddlCaseCategory.SelectedValue != "0")
                    {
                        int caseTypeID = 0;
                        caseTypeID = Convert.ToInt32(ddlCaseCategory.SelectedValue);

                        if (caseTypeID != 0)
                        {
                            var lstCustomFieldsCaseWise = CaseManagement.GetCustomsFields(Convert.ToInt32(AuthenticationHelper.CustomerID), OldCaseInstanceID, 1, caseTypeID);
                            tbl_CaseType ObjCaseTypeDetail = LitigationCourtAndCaseType.GetLegalCaseTypeDetailByID(caseTypeID, CustomerID);

                            if (ObjCaseTypeDetail != null)
                            {
                                bool IsCarryForwardAllowed = false;

                                if (ObjCaseTypeDetail.IsCarryForwardAllowed)
                                    IsCarryForwardAllowed = Convert.ToBoolean(ObjCaseTypeDetail.IsCarryForwardAllowed);

                                if (IsCarryForwardAllowed && lstCustomFieldsCaseWise.Count > 0)
                                {
                                    //Open Custom Field Grid                                   
                                    grdCustomField_CaseTransfer.DataSource = lstCustomFieldsCaseWise;
                                    grdCustomField_CaseTransfer.DataBind();

                                    if (lstCustomFieldsCaseWise.Count <= 0)
                                        divCustomField.Visible = false;
                                    else
                                        divCustomField.Visible = true;
                                }

                                else //Not Allowed then Case to Case transfer, add all custom fields asl 
                                {
                                    bool saveSuccess = false;
                                    long NewCaseID = 0;
                                    CheckCaseClose = true;
                                    //btnSaveStatus_Click(sender, e);
                                    //if (CheckCaseClose)
                                    //{
                                    //    saveSuccess = saveCaseToCaseTransfer(OldCaseInstanceID, out NewCaseID);
                                    //}
                                    //else
                                    //{
                                    //    return;
                                    //}
                                    saveSuccess = saveCaseToCaseTransfer(OldCaseInstanceID, out NewCaseID);
                                    if (saveSuccess && NewCaseID != 0)
                                    {
                                        #region Save Custom Field

                                        if (lstCustomFieldsCaseWise.Count > 0)
                                        {
                                            foreach (var item in lstCustomFieldsCaseWise)
                                            {
                                                tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                {
                                                    NoticeCaseType = 1,
                                                    NoticeCaseInstanceID = NewCaseID,
                                                    LabelID = item.LableID,
                                                    LabelValue = item.labelValue,
                                                    Interest = item.Interest,
                                                    Penalty = item.Penalty,
                                                    Total = item.Total,
                                                    SettlementValue = item.SettlementValue,
                                                    ProvisionInBook = item.ProvisionInBook,
                                                    IsActive = true,
                                                    IsDeleted = false,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                };

                                                saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);
                                            }
                                        }
                                        #endregion
                                    }
                                    long CustomerID = AuthenticationHelper.CustomerID;
                                    var customizedid = GetCustomizedCustomerid(CustomerID);
                                    if (customizedid == AuthenticationHelper.CustomerID)
                                    {
                                        ddlCaseStage.Enabled = false;
                                        ddlCaseStatus.Enabled = false;
                                        ddlCaseResult.Enabled = false;
                                        tbxCaseCloseDate.Enabled = false;
                                        tbxCloseRemark.Enabled = false;
                                        btnCaseTransfer.Enabled = false;
                                        btnSaveStatus.Enabled = false;
                                        btnSave.Enabled = false;
                                        tbxAppealCaseNo.Enabled = false;
                                    }
                                    if (saveSuccess)
                                    {
                                        tbxAppealCaseNo.Text = string.Empty;
                                        cvCaseStatus.IsValid = false;
                                        cvCaseStatus.ErrorMessage = "Case Transfer to New Court Successfully";
                                        ValidationSummary3.CssClass = "alert alert-success";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveCustomFieldCaseTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
                {
                    int OldCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    bool dataValidateSuccess = false;

                    #region Data Validation

                    if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                        dataValidateSuccess = false;
                    else
                        dataValidateSuccess = true;

                    if (grdCustomField_CaseTransfer.Rows.Count > 0)
                    {
                        foreach (GridViewRow eachRow in grdCustomField_CaseTransfer.Rows)
                        {
                            if (eachRow.RowType == DataControlRowType.DataRow)
                            {
                                DropDownList ddlGroundResult = (DropDownList)eachRow.FindControl("ddlGroundResult");

                                if (ddlGroundResult != null)
                                {
                                    if (ddlGroundResult.SelectedValue != "") //0-Disallowed, 1-Allowed
                                    {
                                        Label lblID = (Label)eachRow.FindControl("lblID");
                                        TextBox tbxLabelValue = (TextBox)eachRow.FindControl("tbxLabelValue");
                                        TextBox tbxSettlementValue = (TextBox)eachRow.FindControl("tbxSettlementValue");

                                        TextBox tbxPenaltyValue = (TextBox)eachRow.FindControl("tbxPenaltyValue");
                                        TextBox tbxInterestValue = (TextBox)eachRow.FindControl("tbxInterestValue");

                                        if (lblID != null && tbxLabelValue != null && tbxSettlementValue != null && tbxPenaltyValue != null && tbxInterestValue != null)
                                        {
                                            if (lblID.Text != "")
                                            {
                                                if (tbxLabelValue.Text != "")
                                                {
                                                    if (tbxSettlementValue.Text != "")
                                                    {
                                                        if (tbxPenaltyValue.Text != "")
                                                        {
                                                            if (LitigationManagement.csvToNumber(tbxPenaltyValue.Text) != 0)
                                                            {
                                                                if (tbxInterestValue.Text != "")
                                                                {
                                                                    if (LitigationManagement.csvToNumber(tbxInterestValue.Text) != 0)
                                                                    {
                                                                        dataValidateSuccess = true;
                                                                    }
                                                                    else
                                                                    {
                                                                        cvCaseStatus.IsValid = false;
                                                                        cvCaseStatus.ErrorMessage = "Please enter Valid Number in Interest";
                                                                        ValidationSummary3.CssClass = "alert alert-danger";
                                                                    }
                                                                }
                                                                else
                                                                    dataValidateSuccess = true;
                                                            }
                                                            else
                                                            {
                                                                cvCaseStatus.IsValid = false;
                                                                cvCaseStatus.ErrorMessage = "Please enter Valid Number in Penalty";
                                                                ValidationSummary3.CssClass = "alert alert-danger";
                                                            }
                                                        }
                                                        else
                                                            dataValidateSuccess = true;
                                                    }
                                                    else
                                                    {
                                                        cvCaseStatus.IsValid = false;
                                                        cvCaseStatus.ErrorMessage = "Settlement Value can not be empty";
                                                        ValidationSummary3.CssClass = "alert alert-danger";
                                                    }
                                                }
                                                else
                                                {
                                                    cvCaseStatus.IsValid = false;
                                                    cvCaseStatus.ErrorMessage = "Value can not be empty";
                                                    ValidationSummary3.CssClass = "alert alert-danger";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }//END ForEach
                    }
                    else
                        dataValidateSuccess = true;

                    #endregion

                    if (dataValidateSuccess)
                    {
                        bool saveSuccess = false;
                        long NewCaseID = 0;

                        saveSuccess = saveCaseToCaseTransfer(OldCaseInstanceID, out NewCaseID);

                        if (saveSuccess && NewCaseID != 0)
                        {
                            #region Save Custom Field

                            if (grdCustomField_CaseTransfer.Rows.Count > 0)
                            {
                                foreach (GridViewRow eachRow in grdCustomField_CaseTransfer.Rows)
                                {
                                    if (eachRow.RowType == DataControlRowType.DataRow)
                                    {
                                        DropDownList ddlGroundResult = (DropDownList)eachRow.FindControl("ddlGroundResult");

                                        if (ddlGroundResult != null)
                                        {
                                            if (ddlGroundResult.SelectedValue != "") //0-Disallowed, 1-Allowed
                                            {
                                                Label lblID = (Label)eachRow.FindControl("lblID");
                                                TextBox tbxLabelValue = (TextBox)eachRow.FindControl("tbxLabelValue");
                                                TextBox tbxInterestValue = (TextBox)eachRow.FindControl("tbxInterestValue");
                                                TextBox tbxPenaltyValue = (TextBox)eachRow.FindControl("tbxPenaltyValue");
                                                TextBox tbxTotalValue = (TextBox)eachRow.FindControl("tbxTotalValue");
                                                TextBox tbxProvisionInbooks = (TextBox)eachRow.FindControl("tbxProvisionInbooks");
                                                TextBox tbxSettlementValue = (TextBox)eachRow.FindControl("tbxSettlementValue");

                                                if (lblID != null && tbxLabelValue != null
                                                    && tbxInterestValue != null && tbxPenaltyValue != null && tbxTotalValue != null
                                                    && tbxSettlementValue != null && tbxProvisionInbooks != null)
                                                {
                                                    if (lblID.Text != "")
                                                    {
                                                        //Update OLD Case parameters Settlement and Allowed Values
                                                        tbl_NoticeCaseCustomParameter oldCaseCustomParameter = new tbl_NoticeCaseCustomParameter()
                                                        {
                                                            NoticeCaseType = 1,
                                                            NoticeCaseInstanceID = OldCaseInstanceID,

                                                            LabelID = Convert.ToInt64(lblID.Text),
                                                            LabelValue = tbxLabelValue.Text,
                                                            Interest = tbxInterestValue.Text,
                                                            Penalty = tbxPenaltyValue.Text,
                                                            Total = tbxPenaltyValue.Text,
                                                            SettlementValue = tbxSettlementValue.Text,
                                                            ProvisionInBook = tbxProvisionInbooks.Text,

                                                            IsAllowed = Convert.ToBoolean(Convert.ToInt32(ddlGroundResult.SelectedValue)),

                                                            IsActive = true,
                                                            IsDeleted = false,
                                                            CreatedBy = AuthenticationHelper.UserID,
                                                            CreatedOn = DateTime.Now,
                                                            UpdatedBy = AuthenticationHelper.UserID,
                                                        };

                                                        saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(oldCaseCustomParameter);

                                                        //Create NEW Case Custom Parameters only Dis-Allowed Values
                                                        if (ddlGroundResult.SelectedValue == "0")
                                                        {
                                                            tbl_NoticeCaseCustomParameter newCaseCustomParameter = new tbl_NoticeCaseCustomParameter()
                                                            {
                                                                NoticeCaseType = 1,
                                                                NoticeCaseInstanceID = NewCaseID,

                                                                LabelID = Convert.ToInt64(lblID.Text),
                                                                LabelValue = tbxLabelValue.Text,
                                                                Interest = tbxInterestValue.Text,
                                                                Penalty = tbxPenaltyValue.Text,
                                                                Total = tbxPenaltyValue.Text,
                                                                SettlementValue = tbxSettlementValue.Text,
                                                                ProvisionInBook = tbxProvisionInbooks.Text,

                                                                IsActive = true,
                                                                IsDeleted = false,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedOn = DateTime.Now,
                                                                UpdatedBy = AuthenticationHelper.UserID,
                                                            };

                                                            saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(newCaseCustomParameter);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }//END FOR EACH 
                            }

                            #endregion
                        }
                        if (saveSuccess)
                        {
                            cvCaseStatus.IsValid = false;
                            cvCaseStatus.ErrorMessage = "Case Transfer to New Court Successfully.";
                            ValidationSummary3.CssClass = "alert alert-success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool saveCaseToCaseTransfer(int OldCaseInstanceID, out long newCaseID_Out)
        {
            bool saveSuccess = false;
            List<string> lstFinancialYearMapping = new List<string>();

            newCaseID_Out = 0;
            try
            {
                var caseRecord = CaseManagement.GetCaseByID(OldCaseInstanceID);

                if (caseRecord != null)
                {
                    long NewCaseID = 0;

                    #region Save Case Record

                    tbl_LegalCaseInstance NewCase = new tbl_LegalCaseInstance()
                    {
                        IsDeleted = false,
                        CaseType = caseRecord.CaseType,
                        CaseRefNo = tbxAppealCaseNo.Text.Trim(),
                        //OpenDate = caseRecord.OpenDate,
                        InternalCaseNo = caseRecord.InternalCaseNo,
                        Section = caseRecord.Section,
                        CaseCategoryID = caseRecord.CaseCategoryID,
                        CaseTitle = caseRecord.CaseTitle + ",",
                        CaseDetailDesc = caseRecord.CaseDetailDesc,
                        CustomerBranchID = caseRecord.CustomerBranchID,
                        DepartmentID = caseRecord.DepartmentID,
                        ContactPersonOfDepartment = caseRecord.ContactPersonOfDepartment,
                        OwnerID = caseRecord.OwnerID,
                        //CourtID = caseRecord.CourtID,
                        CaseRiskID = caseRecord.CaseRiskID,
                        ClaimAmt = caseRecord.ClaimAmt,
                        ProbableAmt = caseRecord.ProbableAmt,
                        ImpactType = caseRecord.ImpactType,
                        Monetory = caseRecord.Monetory,
                        NonMonetory = caseRecord.NonMonetory,
                        Years = caseRecord.Years,
                        AssignmentType = caseRecord.AssignmentType,
                        PreDeposit = caseRecord.PreDeposit,
                        PostDeposit = caseRecord.PostDeposit,
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedBy = AuthenticationHelper.UserID,
                      //  FinancialYear = caseRecord.FinancialYear,
                    };

                    NewCaseID = CaseManagement.CreateCase(NewCase);
                    if (NewCaseID > 0)
                        saveSuccess = true;

                    #endregion

                    if (saveSuccess)
                    {
                        newCaseID_Out = NewCaseID;

                        LitigationManagement.CreateAuditLog("C", OldCaseInstanceID, "tbl_LegalCaseInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Transferred to New Case", true);
                        LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Created from Old Case", true);

                        #region Notice Case Mapping Save Data
                        tbl_NoticeCaseMapping objNoticeCaseMapping = new tbl_NoticeCaseMapping()
                        {
                            NoticeCaseType = 1,
                            NoticeCaseInstanceID = OldCaseInstanceID,
                            NewCaseInstanceID = NewCaseID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                        };

                        saveSuccess = CaseManagement.CreateNoticeToCaseMapping(objNoticeCaseMapping);

                        if (saveSuccess)
                        {
                            LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_NoticeCaseMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case to Case Mapping Created", true);
                        }

                        #endregion

                        #region Status Transaction
                        tbl_LegalCaseStatusTransaction newStatusRecord = new tbl_LegalCaseStatusTransaction()
                        {
                            CaseInstanceID = NewCaseID,
                            StatusID = 1,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            IsDeleted = false,
                            UserID = AuthenticationHelper.UserID,
                            RoleID = 3,
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };

                        if (!CaseManagement.ExistCaseStatusTransaction(newStatusRecord))
                            saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusRecord);

                        #endregion

                        #region Save Party Mapping

                        var ListofParty = CaseManagement.GetListOfParty(OldCaseInstanceID, 1);

                        if (ListofParty.Count > 0)
                        {
                            List<tbl_PartyMapping> lstObjPartyMapping = new List<tbl_PartyMapping>();

                            ListofParty.ForEach(EachParty =>
                            {
                                tbl_PartyMapping objPartyMapping = new tbl_PartyMapping()
                                {
                                    CaseNoticeInstanceID = NewCaseID,
                                    IsActive = true,
                                    Type = 1,//1 as Case and 2 as Notice
                                    PartyID = Convert.ToInt32(EachParty.PartyID),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };

                                lstObjPartyMapping.Add(objPartyMapping);
                            });

                            saveSuccess = CaseManagement.CreateUpdatePartyMapping(lstObjPartyMapping);
                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_PartyMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Party Mapped", true);
                            }

                            //Refresh List
                            lstObjPartyMapping.Clear();
                            lstObjPartyMapping = null;
                        }
                        #endregion

                        #region Save Act Mapping
                        var ListofAct = CaseManagement.GetListOfAct(OldCaseInstanceID, 1);

                        if (ListofAct.Count > 0)
                        {
                            List<tbl_ActMapping> lstObjActMapping = new List<tbl_ActMapping>();

                            ListofAct.ForEach(EachAct =>
                            {
                                tbl_ActMapping objActMapping = new tbl_ActMapping()
                                {
                                    CaseNoticeInstanceID = NewCaseID,
                                    IsActive = true,
                                    Type = 1,//1 as Case and 2 as Notice
                                    ActID = Convert.ToInt32(EachAct.ActID),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };

                                lstObjActMapping.Add(objActMapping);
                            });

                            saveSuccess = CaseManagement.CreateUpdateActMapping(lstObjActMapping);
                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_ActMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Act Mapped", true);
                            }

                            //Refresh List
                            lstObjActMapping.Clear();
                            lstObjActMapping = null;
                        }
                        #endregion

                        #region Save Financial Mapping
                        var ListofFY = CaseManagement.GetListOfFY(OldCaseInstanceID, 1);
                        if (ListofFY.Count > 0)
                        {
                            foreach(var item in ListofFY)
                            {
                                lstFinancialYearMapping.Add(item.FYID);
                            }
                            List<FinancialYearMapping> lstObjFYMapping = new List<FinancialYearMapping>();

                            lstFinancialYearMapping.ForEach(EachFYID =>
                            {
                                FinancialYearMapping objFYMapping = new FinancialYearMapping()
                                {
                                    FYID = Convert.ToString(EachFYID),
                                    Type = 1,//1 as Case and 2 as Notice
                                    CaseNoticeInstanceID = NewCaseID,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };

                                lstObjFYMapping.Add(objFYMapping);
                            });

                            saveSuccess = CaseManagement.CreateUpdateFYMapping(lstObjFYMapping);

                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", NewCaseID, "FinancialYearMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Financial Year Mapped.", false);
                            }
                            //Refresh List
                            lstObjFYMapping.Clear();
                            lstObjFYMapping = null;

                            ListofFY.Clear();
                            lstFinancialYearMapping.Clear();
                            ListofFY = null;
                        }
                        #endregion

                        #region Save Opposition Lawyer Mapping

                        var ListofOppoLawyer = CaseManagement.GetListOfOppoLaywer(OldCaseInstanceID, 1);

                        if (ListofOppoLawyer.Count > 0)
                        {
                            List<tbl_OppositionLawyerList> lstObjLawyerMapping = new List<tbl_OppositionLawyerList>();

                            ListofOppoLawyer.ForEach(EachLawyer =>
                            {
                                tbl_OppositionLawyerList objLawyerMapping = new tbl_OppositionLawyerList()
                                {
                                    CaseNoticeInstanceID = NewCaseID,
                                    IsActive = true,
                                    Type = 1,//1 as Case and 2 as Notice
                                    LawyerID = Convert.ToInt32(EachLawyer.LawyerID),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };

                                lstObjLawyerMapping.Add(objLawyerMapping);
                            });

                            saveSuccess = CaseManagement.CreateUpdateOppositionLawyerMapping(lstObjLawyerMapping);
                            if (saveSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_OppositionLawyerList", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Opposition Lawyer Mapped", true);
                            }

                            //Refresh List
                            lstObjLawyerMapping.Clear();
                            lstObjLawyerMapping = null;
                        }
                        #endregion

                        #region Lawyer Mapping
                        var lstCaseLawyer = CaseManagement.GetCaseLawyerMapping(OldCaseInstanceID);
                        tbl_LegalCaseLawyerMapping objCaseLawyerMapping = new tbl_LegalCaseLawyerMapping()
                        {
                            CaseInstanceID = NewCaseID,
                            IsActive = true,
                            LawyerID = Convert.ToInt32(lstCaseLawyer.LawyerID),
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };

                        saveSuccess = CaseManagement.CreateCaseLawyerMapping(objCaseLawyerMapping);
                        if (saveSuccess)
                        {
                            LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseLawyerMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Lawyer Mapped", true);
                        }
                        #endregion

                        #region User Assignment

                        var lstCaseAssignment = CaseManagement.GetCaseAssignment(OldCaseInstanceID);

                        if (lstCaseAssignment.Count > 0)
                        {
                            lstCaseAssignment.ForEach(EachPerformer =>
                            {
                                tbl_LegalCaseAssignment newAssignment = new tbl_LegalCaseAssignment()
                                {
                                    AssignmentType = caseRecord.AssignmentType,
                                    CaseInstanceID = NewCaseID,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    IsLawyer = false,
                                    UserID = Convert.ToInt32(EachPerformer.UserID),
                                    RoleID = EachPerformer.RoleID,
                                };
                                if (!CaseManagement.ExistCaseAssignment(newAssignment))
                                {
                                    saveSuccess = CaseManagement.CreateCaseAssignment(newAssignment);
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Created", true);
                                }
                                else
                                {
                                    saveSuccess = CaseManagement.UpdateCaseAssignments(newAssignment);
                                    LitigationManagement.CreateAuditLog("C", NewCaseID, "tbl_LegalCaseAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Assignment Updated", true);
                                }
                            });
                        }

                        #endregion
                    }
                }
                return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }
        
       protected void lnkOthers_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liOthers.Attributes.Add("class", "active");
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 1;

            if ((int)ViewState["Mode"] == 1)
            {
                case_year = txtCaseYear.Text.Trim();
                BenchID = txtBenchID.Text.Trim();
                StateID = txtStateID.Text.Trim();
            }
            case_number = tbxRefNo.Text;
            case_type = Convert.ToInt32(ddlCaseCategory.SelectedValue);
            court_type = ddlCourt.SelectedItem.Text;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "clickOther", "liOthers_click('1');", true);
        }

        protected void ddlGroundResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlCurrentGroundResult = (DropDownList)sender;

            if (ddlCurrentGroundResult != null)
            {
                if (ddlCurrentGroundResult.SelectedValue != null && ddlCurrentGroundResult.SelectedValue != "")
                {
                    if (ddlCurrentGroundResult.SelectedValue == "1") //1-Allowed
                    {
                        GridViewRow grdDropDownRow = ((GridViewRow)ddlCurrentGroundResult.Parent.Parent);
                        if (grdDropDownRow != null)
                        {
                            TextBox tbxSettlementValue = (TextBox)grdDropDownRow.FindControl("tbxSettlementValue");
                            if (tbxSettlementValue != null)
                                tbxSettlementValue.Visible = true;
                            else
                                tbxSettlementValue.Visible = false;
                        }
                    }
                }
            }
        }

        protected void grdCaseHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("ViewNoticeCasePopup"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int NoticeCaseInstanceID = Convert.ToInt32(commandArgs[0]);
                        int NoticeCaseType = Convert.ToInt32(commandArgs[1]);
                        string HistoryFlag = " true";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptOpenRefPopUp", "OpenCaseNoticeHistoryPopup(" + NoticeCaseInstanceID + "," + NoticeCaseType + "," + HistoryFlag + "," + "'H'" + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCaseHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdCaseHistory.PageIndex = e.NewPageIndex;
                    BindCaseHistory(Convert.ToInt32(ViewState["CaseInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void ddlLawFirm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLawFirm.SelectedValue))
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);
                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();

                var lawyerUsers = LitigationUserManagement.GetRequiredUsersByLawFirm(lstAllUsers, 2, Convert.ToInt32(ddlLawFirm.SelectedValue));

                lstBoxLawyerUser.DataTextField = "Name";
                lstBoxLawyerUser.DataValueField = "ID";
                lstBoxLawyerUser.DataSource = lawyerUsers;
                lstBoxLawyerUser.DataBind();
                lstBoxLawyerUser.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

              


                ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScript89", "rebindLawyerUser();", true);
                UpdatePanel6.Update();
                if (ddlLawFirm.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "ShowAddButtonLawfirm", "ShowLawFirmAddbutton();", true);
                }
            }
            else
            {
            //    bindAllLawfirmUser();
            }
        }

        protected void GrdHearingEditDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteHearingEditDocument"))
            {
                long CaseInstanceID = 0;
                long responseID = 0;
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    bool Caseresponse = CaseManagement.DeleteHearingEditDocument(Convert.ToInt32(commandArgs));
                    if (Caseresponse)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["responseID"])))
                        {
                            responseID = Convert.ToInt32(ViewState["responseID"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
                        {
                            CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        }
                        var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, responseID, "CH");
                        if (lstResponseDocument.Count > 0)
                        {
                            GrdHearingEditDocument.DataSource = lstResponseDocument;
                            GrdHearingEditDocument.DataBind();
                            divHearingEditdoc.Visible = true;
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowHearingDivForEdit();", true);
                    }
                }
            }
            else if (e.CommandName.Equals("DownloadHearingEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = CaseManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }
                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                    }
                                }


                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                            }
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowHearingDivForEdit();", true);
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                }


                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                            }
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowHearingDivForEdit();", true);
                            #endregion
                        }
                    }
                    else
                    {
                        cvCasePopUpResponse.IsValid = false;
                        cvCasePopUpResponse.ErrorMessage = "No Document Available for Download.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                        return;
                    }
                }
            }
            else if (e.CommandName.Equals("ViewHearingEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = CaseManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }

                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                            }
                            string filePath1 = directoryPath + "/" + file.FileName;
                            DocumentPath = filePath1;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);                            
                            lblMessage.Text = "";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowHearingDivForEdit();", true);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);                            
                            #endregion
                        }
                        else
                        {
                            #region Normal

                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;

                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                lblMessage.Text = "";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowHearingDivForEdit();", true);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                    }
                }
            }
        }

        protected void GrdOrderEditDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteOrderEditDocument"))
            {
                long CaseInstanceID = 0;
                long OrderID = 0;
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    bool Caseresponse = CaseManagement.DeleteOrderEditDocument(Convert.ToInt32(commandArgs));
                    if (Caseresponse)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["OrderID"])))
                        {
                            OrderID = Convert.ToInt32(ViewState["OrderID"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
                        {
                            CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        }
                        var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, OrderID, "CO");
                        if (lstResponseDocument.Count > 0)
                        {
                            GrdOrderEditDocument.DataSource = lstResponseDocument;
                            GrdOrderEditDocument.DataBind();
                            divOrderEditdoc.Visible = true;
                        }
                    }
                }
            }
            else if (e.CommandName.Equals("DownloadOrderEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = CaseManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS 
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }
                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                    }
                                }
                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal 
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                }


                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            #endregion
                        }
                    }
                    else
                    {
                        cvCasePopUpResponse.IsValid = false;
                        cvCasePopUpResponse.ErrorMessage = "No Document Available for Download.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                        return;
                    }

                    // DownloadCaseDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                }
            }
            else if (e.CommandName.Equals("ViewOrderEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = CaseManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region Aws
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }

                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                            }
                            string filePath1 = directoryPath + "/" + file.FileName;
                            DocumentPath = filePath1;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);                            
                            lblMessage.Text = "";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;

                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                lblMessage.Text = "";

                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                    }
                }
            }
        }

        protected void gvCaseAuditLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    gvCaseAuditLog.PageIndex = e.NewPageIndex;

                    BindCaseAuditLogs();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskEditDoc_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteTaskEditDocument"))
            {
                long CaseInstanceID = 0;
                long taskID = 0;
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    bool NoticeRespose = NoticeManagement.DeleteResponseEditDocument(Convert.ToInt32(commandArgs));
                    if (NoticeRespose)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["taskID"])))
                        {
                            taskID = Convert.ToInt32(ViewState["taskID"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
                        {
                            CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        }
                        var lstResponseDocument = NoticeManagement.GetNoticeResponseDocuments(CaseInstanceID, taskID, "CT");
                        if (lstResponseDocument.Count > 0)
                        {
                            grdTaskEditDoc.DataSource = lstResponseDocument;
                            grdTaskEditDoc.DataBind();
                            DivTaskEdit.Visible = true;
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                    }
                }
            }
            else if (e.CommandName.Equals("DownloadTaskEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = NoticeManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }
                                int i = 0;
                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string ext = Path.GetExtension(file.FileName);
                                    string[] filename = file.FileName.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];
                                    string str = filename[0] + i + "." + ext;

                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));

                                    i++;
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "No Document Available for Download.";
                        ValidationSummary5.CssClass = "alert alert-danger";
                        return;
                    }
                }
            }
            else if (e.CommandName.Equals("ViewTaskEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = NoticeManagement.ShowandDownloadFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }
                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                            {
                                GetObjectRequest request = new GetObjectRequest();
                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                request.Key = file.FileName;
                                GetObjectResponse response = client.GetObject(request);
                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                            }
                            string filePath1 = directoryPath + "/" + file.FileName;
                            DocumentPath = filePath1;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            lblMessage.Text = "";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            
                            #endregion
                        }
                        else
                        {
                            #region Normal

                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;

                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                lblMessage.Text = "";

                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "HidShowTaskDivForEdit();", true);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                    }
                }
            }
        }

        protected void grdUserAssignment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkDeleteUserAssignment = (LinkButton)e.Row.FindControl("lnkDeleteUserAssignment");
                    if (lnkDeleteUserAssignment != null)
                    {
                        var scriptManager = ScriptManager.GetCurrent(this.Page);
                        scriptManager.RegisterPostBackControl(lnkDeleteUserAssignment);

                        if (ViewState["caseStatus"] != null)
                        {
                            if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                                lnkDeleteUserAssignment.Visible = false;
                            else
                                lnkDeleteUserAssignment.Visible = true;
                        }

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            lnkDeleteUserAssignment.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdUserAssignment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                bool deleteSuccess = false;

                if (e.CommandArgument != null && ViewState["CaseInstanceID"] != null)
                {
                    long caseInstance = Convert.ToInt64(ViewState["CaseInstanceID"]);
                    if (e.CommandName.Equals("Delete_UserAssignment"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int assignRecordID = Convert.ToInt32(commandArgs[0]);
                        long caseInstanceID = Convert.ToInt64(commandArgs[1]);

                        if (assignRecordID != 0 && caseInstanceID != 0)
                        {
                            deleteSuccess = CaseManagement.DeleteCaseUserAssignment(assignRecordID, caseInstanceID);
                        }
                    }

                    if (deleteSuccess)
                    {
                        cvCaseUserAssignmemt.IsValid = false;
                        cvCaseUserAssignmemt.ErrorMessage = "User Assignment Detail Deleted";
                        vsCaseUserAssign.CssClass = "alert alert-danger";
                        BindCaseUserAssignments(caseInstance);
                    }
                    else
                    {
                        cvCaseUserAssignmemt.IsValid = false;
                        cvCaseUserAssignmemt.ErrorMessage = "Something went wrong there, Please try again";
                        vsCaseUserAssign.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearCaseUserAssignmentControls_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveLinkCase_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                List<long> lstCaseInstanceIDsToLink = new List<long>();
                int totalRecordSaveCount = 0;

                if (ViewState["CaseInstanceID"] != null)
                {
                    long caseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);

                    for (int i = 0; i < grdCaseList_LinkCase.Rows.Count; i++)
                    {
                        CheckBox chkRowLinkCases = (CheckBox)grdCaseList_LinkCase.Rows[i].FindControl("chkRowLinkCases");

                        if (chkRowLinkCases != null)
                        {
                            if (chkRowLinkCases.Checked)
                            {
                                Label lblCaseInstanceID = (Label)grdCaseList_LinkCase.Rows[i].FindControl("lblCaseInstanceID");

                                if (lblCaseInstanceID != null)
                                {
                                    if (lblCaseInstanceID.Text != "")
                                        lstCaseInstanceIDsToLink.Add(Convert.ToInt64(lblCaseInstanceID.Text));
                                }
                            }
                        }
                    }

                    if (lstCaseInstanceIDsToLink.Count > 0)
                    {
                        lstCaseInstanceIDsToLink.ForEach(eachCaseToLink =>
                        {
                            tbl_NoticeCaseLinking newRecord = new tbl_NoticeCaseLinking()
                            {
                                Type = 1,
                                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                NoticeOrCaseInstanceID = caseInstanceID,
                                LinkedNoticeOrCaseInstanceID = eachCaseToLink,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,

                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                            };

                            saveSuccess = LitigationManagement.CreateUpdateNoticeCaseLinking(newRecord);

                            ////if Cross Linking also to be Save
                            //tbl_NoticeCaseLinking otherRecord = new tbl_NoticeCaseLinking()
                            //{
                            //    Type = 1,
                            //    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            //    NoticeOrCaseInstanceID = eachCaseToLink,
                            //    LinkedNoticeOrCaseInstanceID = caseInstanceID,

                            //    IsActive = true,
                            //    CreatedBy = AuthenticationHelper.UserID,
                            //    CreatedOn = DateTime.Now,

                            //    UpdatedBy = AuthenticationHelper.UserID,
                            //    UpdatedOn = DateTime.Now,
                            //};

                            //saveSuccess =LitigationManagement.CreateUpdateNoticeCaseLinking(otherRecord);

                            if (saveSuccess)
                                totalRecordSaveCount++;
                        });
                    }

                    if (saveSuccess)
                    {
                        LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_NoticeCaseLinking", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, totalRecordSaveCount + " Case(s) Linked", true);

                        BindCaseListToLink(caseInstanceID);
                        BindLinkedCases(caseInstanceID);

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptUnCheckAllGridCheckBox", "unCheckAll();", true);

                        cvLinkCase.IsValid = false;
                        cvLinkCase.ErrorMessage = totalRecordSaveCount + " Selected Case(s) Linked Successfully";
                        vsLinkCase.CssClass = "alert alert-success";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void grdLinkedCases_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                bool deleteSuccess = false;

                if (e.CommandArgument != null && ViewState["CaseInstanceID"] != null)
                {
                    if (e.CommandName.Equals("ViewNoticeCasePopup"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int NoticeCaseInstanceID = Convert.ToInt32(commandArgs[0]);
                        int NoticeCaseType = Convert.ToInt32(commandArgs[1]);
                        string HistoryFlag = " true";


                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptOpenRefPopUp", "OpenCaseNoticeHistoryPopup(" + NoticeCaseInstanceID + "," + NoticeCaseType + "," + HistoryFlag + ",'L'" + ");", true);
                    }
                    else if (e.CommandName.Equals("DeleteCaseLinking"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long NoticeCaseInstanceID = Convert.ToInt64(commandArgs[0]);
                        long LinkedNoticeOrCaseInstanceID = Convert.ToInt64(commandArgs[1]);
                        int NoticeCaseType = Convert.ToInt32(commandArgs[2]);

                        if (NoticeCaseInstanceID != 0 && LinkedNoticeOrCaseInstanceID != 0 && NoticeCaseType != 0)
                        {
                            deleteSuccess = LitigationManagement.DeleteNoticeCaseLinking(NoticeCaseInstanceID, LinkedNoticeOrCaseInstanceID, NoticeCaseType, AuthenticationHelper.UserID);

                            if (deleteSuccess)
                            {
                                BindLinkedCases(NoticeCaseInstanceID);
                                LitigationManagement.CreateAuditLog("C", NoticeCaseInstanceID, "tbl_NoticeCaseLinking", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Linking Deleted", true);

                                BindCaseListToLink(NoticeCaseInstanceID);
                                cvLinkedCases.IsValid = false;
                                cvLinkedCases.ErrorMessage = "Case Linking Deleted Successfully";
                                vsLinkCase.CssClass = "alert alert-success";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLinkedCases_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdLinkedCases.PageIndex = e.NewPageIndex;
                    BindLinkedCases(Convert.ToInt32(ViewState["CaseInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLinkedCases_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkBtnDeleteCaseLinking = (LinkButton)e.Row.FindControl("lnkBtnDeleteCaseLinking");
                    if (lnkBtnDeleteCaseLinking != null)
                    {
                        if (ViewState["caseStatus"] != null)
                        {
                            if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                                lnkBtnDeleteCaseLinking.Visible = false;
                            else
                                lnkBtnDeleteCaseLinking.Visible = true;
                        }

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            lnkBtnDeleteCaseLinking.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void HideShowGridColumns(GridView gridView, string headerTextToMatch, bool flag)
        {
            try
            {
                if (gridView == null)
                {
                    return;
                }

                // Loop through all of the columns in the grid.
                for (int i = 0; i < gridView.Columns.Count; i++)
                {
                    String headerText = gridView.Columns[i].HeaderText;

                    //Show Hide Columns with Specific headerText
                    if (headerText == headerTextToMatch)
                        gridView.Columns[i].Visible = flag;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkOpposiLawyer_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);

                var lawyerUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);

                //Opposition Lawyer list 
                lstBoxOppositionLawyer.DataTextField = "Name";
                lstBoxOppositionLawyer.DataValueField = "ID";

                lstBoxOppositionLawyer.DataSource = lawyerUsers;
                lstBoxOppositionLawyer.DataBind();
                lstBoxOppositionLawyer.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));

                lawyerUsers.Clear();
                lawyerUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkLawyers_Click(object sender, EventArgs e)
        {
            ddlLawFirm_SelectedIndexChanged(null, null);
        }

        protected void lnkLawFirmBind_Click(object sender, EventArgs e)
        {
            BindLawyer();
            ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScriptd", "rebindLawyerUser();", true);
        }

        protected void lnkApply_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
            {
                int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                BindCaseRelatedDocuments(caseInstanceID);
            }
        }

        protected void lnkBindshowDocumentCase_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "active");
            liCaseTask.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");
            liOthers.Attributes.Add("class", "");
            liCaseRating.Attributes.Add("class", "");
            lnkAuditLog.Attributes.Add("class", "");

            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
            {
                int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                BindCaseRelatedDocuments(caseInstanceID);
                applyCSStoFileTag_ListItems();
                BindFileTags();
                // BindLitigationDocType();
            }
        }

        protected void lnkAddNewDoctype_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
            {
                int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                ScriptManager.RegisterStartupScript(this, GetType(), "LitiationCaseDoc", "OpenDoumentPopup(" + caseInstanceID + ");", true);
                applyCSStoFileTag_ListItems();
            }
        }

        protected void ddlLayerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty((ddlLayerType.SelectedValue).ToString()))
                {
                    if (!string.IsNullOrEmpty((ViewState["CaseInstanceID"]).ToString()))
                    {
                        int layerID = Convert.ToInt32(ddlLayerType.SelectedValue);
                        int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        if (layerID > 0)
                        {
                            var documentData = (from row in entities.sp_LiDisplayCriteriaRatingCase(caseInstanceID, layerID, Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                select row).ToList();

                            if (documentData != null)
                            {
                                grdLawyerRating.PageIndex = 0;
                                grdLawyerRating.DataSource = documentData;
                                grdLawyerRating.DataBind();
                            }
                        }
                    }
                }
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string caseinstantID = ViewState["CaseInstanceID"].ToString();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCriteriaRetingPopUp('" + ddlLayerType.SelectedValue + "','" + caseinstantID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void lnkBtnRebindRating_Click(object sender, EventArgs e)
        {
            TabRating_Click(sender, e);
        }
        //Added by Ruchi
        protected void lstBoxFileTags_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int caseinstantid = Convert.ToInt32(ViewState["CaseInstanceID"]);
                BindCaseRelatedDocuments(caseinstantid);

                List<System.Web.UI.WebControls.ListItem> lstSelectedTags = new List<System.Web.UI.WebControls.ListItem>();

                foreach (System.Web.UI.WebControls.ListItem eachListItem in lstBoxFileTags.Items)
                {
                    if (eachListItem.Selected)
                        lstSelectedTags.Add(eachListItem);
                }

                //Re-Arrange File Tags
                var arrangedListItems = LitigationManagement.ReArrange_FileTags(lstBoxFileTags);

                //lstBoxFileTags.DataSource = arrangedListItems;
                lstBoxFileTags.DataBind();

                foreach (System.Web.UI.WebControls.ListItem eachListItem in lstSelectedTags)
                {
                    if (lstBoxFileTags.Items.FindByValue(eachListItem.Value) != null)
                        lstBoxFileTags.Items.FindByValue(eachListItem.Value).Selected = true;
                }

                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //Added by Ruchi
        protected void lnkBtnUploadDocument_Click(object sender, EventArgs e)
        {
            var contractID = CryptographyManagement.Encrypt(ViewState["CaseInstanceID"].ToString());
            var FlagID = 1;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenUploadDocumentPopup('" + contractID + "','" + FlagID + "');", true);
        }
        protected void btnsearchDoc_Click(object sender, EventArgs e)
        {
            BindCaseRelatedDocuments_All();
        }
        public void applyCSStoFileTag_ListItems()
        {
            foreach (System.Web.UI.WebControls.ListItem eachItem in lstBoxFileTags.Items)
            {
                if (eachItem.Selected)
                    eachItem.Attributes.Add("class", "label label-info-selected");
                else
                    eachItem.Attributes.Add("class", "label label-info");
                eachItem.Attributes.Add("onclick", "fcheckcontract(this)");
            }
        }
        private void BindCaseRelatedDocuments_All()
        {
            try
            {
                int DocTypeID = -1;
                int caseinstantid = Convert.ToInt32(ViewState["CaseInstanceID"]);
                string DisplayError = string.Empty;
                string TypeToSearch = string.Empty;
                List<Sp_Litigation_CaseDocument_Result> lstCaseDocs = new List<Sp_Litigation_CaseDocument_Result>();
                List<Sp_Litigation_CaseDocument_Result> CaseDataFiltered = new List<Sp_Litigation_CaseDocument_Result>();
                List<Sp_Litigation_CaseDocument_Result> CaseDataFinal = new List<Sp_Litigation_CaseDocument_Result>();
                lstCaseDocs = CaseManagement.GetCaseDocumentMapping(caseinstantid, "", AuthenticationHelper.CustomerID);
                #region Remove by Ruchi for header tag
                //if (!string.IsNullOrEmpty(ddlDocType.SelectedValue))
                //{
                //    if (ddlDocType.SelectedValue != "-1")
                //    {
                //        DocTypeID = Convert.ToInt32(ddlDocType.SelectedValue);
                //        lstCaseDocs = lstCaseDocs.Where(row => row.DocTypeID == DocTypeID).ToList();
                //    }
                //}
                //if (!string.IsNullOrEmpty(tbxFilter.Text.Trim()))
                //{
                //    TypeToSearch = tbxFilter.Text;
                //    string[] FileTagsName = TypeToSearch.Trim().Split(',');

                //if (FileTagsName.Length > 0)
                //{
                //    for (int j = 0; j < FileTagsName.Length; j++)
                //    {
                //        CaseDataFiltered = lstCaseDocs.Where(entry => entry.FileTag.ToLower().Contains(((FileTagsName[j].Trim()).ToLower())) || entry.FileName.ToLower().Contains(((FileTagsName[j].Trim()).ToLower())) || entry.TypeName.ToLower().Contains(((FileTagsName[j].Trim()).ToLower()))).ToList();
                //        foreach (var item in CaseDataFiltered)
                //        {
                //            CaseDataFinal.Add(item);
                //        }
                //        if (CaseDataFiltered.Count <= 0)
                //        {
                //            DisplayError += "," + FileTagsName[j].Trim();
                //        }
                //        CaseDataFiltered.Clear();
                //    }
                //}
                //if (CaseDataFinal.Count > 0)
                //{
                //    CaseDataFinal = (from g in CaseDataFinal
                //                     group g by new
                //                     {
                //                         g.ID,
                //                         g.DocType,
                //                         g.FileName,
                //                         g.Version,
                //                         g.CreatedByText,
                //                         g.CreatedOn
                //                     } into GCS
                //                     select new Sp_Litigation_CaseDocument_Result()
                //                     {
                //                         ID = GCS.Key.ID,
                //                         DocType = GCS.Key.DocType,
                //                         FileName = GCS.Key.FileName,
                //                         Version = GCS.Key.Version,
                //                         CreatedByText = GCS.Key.CreatedByText,
                //                         CreatedOn = GCS.Key.CreatedOn,
                //                     }).ToList();

                //    if (!string.IsNullOrEmpty(DisplayError))
                //    {
                //        DisplayError = DisplayError.TrimStart(',');
                //        cvCaseDocument.IsValid = false;
                //        cvCaseDocument.ErrorMessage = "Document does not exist with tag name" + " - " + DisplayError + ".";
                //        vsCaseDocument.CssClass = "alert alert-danger";
                //    }
                //    grdCaseDocuments.DataSource = CaseDataFinal;
                //    grdCaseDocuments.DataBind();
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(DisplayError))
                //    {
                //        DisplayError = DisplayError.TrimStart(',');
                //        cvCaseDocument.IsValid = false;
                //        cvCaseDocument.ErrorMessage = "Document does not exist with tag name, filename, document type" + " - " + DisplayError + ".";
                //        vsCaseDocument.CssClass = "alert alert-danger";
                //    }
                //    grdCaseDocuments.DataSource = null;
                //    grdCaseDocuments.DataBind();
                //}
                //}
                //else
                //{
                #endregion Remove by Ruchi for header tag
                if (lstCaseDocs.Count > 0)
                {
                    BindFileTags();
                    lstCaseDocs = (from g in lstCaseDocs
                                   group g by new
                                   {
                                       g.ID,
                                       g.DocType,
                                       g.FileName,
                                       g.Version,
                                       g.CreatedByText,
                                       g.CreatedOn,
                                       g.TypeName,
                                       g.FinancialYear,
                                       
                                   } into GCS
                                   select new Sp_Litigation_CaseDocument_Result()
                                   {
                                       ID = GCS.Key.ID,
                                       DocType = GCS.Key.DocType,
                                       FileName = GCS.Key.FileName,
                                       Version = GCS.Key.Version,
                                       CreatedByText = GCS.Key.CreatedByText,
                                       CreatedOn = GCS.Key.CreatedOn,
                                       TypeName = GCS.Key.TypeName,
                                       FinancialYear = GCS.Key.FinancialYear,
                                   }).ToList();

                    if (!string.IsNullOrEmpty(txtdocsearch.Text))
                    {
                        lstCaseDocs = lstCaseDocs.Where(row => row.FileName.ToLower().Contains(txtdocsearch.Text.ToLower())).ToList();
                    }
                    grdCaseDocuments.DataSource = lstCaseDocs;
                    grdCaseDocuments.DataBind();


                }
                else
                {
                    grdCaseDocuments.DataSource = null;
                    grdCaseDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        //Added by Ruchi
        public void BindCaseRelatedDocuments(int caseInstanceID)
        {
            try
            {

                //Added by Ruchi
                List<System.Web.UI.WebControls.ListItem> selectedItems = LitigationManagement.GetSelectedItems(lstBoxFileTags);

                var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                List<Sp_Litigation_CaseDocument_Result> lstContDocs = new List<Sp_Litigation_CaseDocument_Result>();

                lstContDocs = LitigationDocumentManagement.Sp_Litigation_CaseDocument_Result(caseInstanceID, selectedFileTags, AuthenticationHelper.CustomerID);

                grdCaseDocuments.DataSource = lstContDocs;
                grdCaseDocuments.DataBind();

                Session["TotalRows"] = lstContDocs.Count;
                //bindPageNumber(1);
                lstContDocs.Clear();
                lstContDocs = null;
                upCaseDocUploadPopup.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
        }
        private void BindFileTags()
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long CaseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);

                    if (CaseInstanceID != 0)
                    {
                        var lstTags = LitigationDocumentManagement.GetDistinctFileTagsCase(customerID, CaseInstanceID);

                        lstBoxFileTags.Items.Clear();

                        if (lstTags.Count > 0)
                        {
                            for (int i = 0; i < lstTags.Count; i++)
                            {
                                lstBoxFileTags.Items.Add(new System.Web.UI.WebControls.ListItem(lstTags[i], lstTags[i]));
                            }
                        }

                        applyCSStoFileTag_ListItems();
                    }
                    else
                    {
                        lstBoxFileTags.DataSource = null;
                        lstBoxFileTags.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkLinkCaseFilter_Click(object sender, EventArgs e)
        {
            long CaseInstanceID = 0;
            if (ViewState["CaseInstanceID"] != null)
            {
                CaseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);
                BindCaseListToLink(CaseInstanceID);
            }
        }

        protected void lnkBtnAddCustomField1_textletigation(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    LinkButton lnkBtn = (LinkButton)sender;
                    GridViewRow gvRow = (GridViewRow)lnkBtn.NamingContainer;
                    GridView gridView = (GridView)gvRow.NamingContainer;
                    string lblName = string.Empty;
                    if (gridView != null)
                    {
                        int lblID = 0;
                        //DropDownList ddlFieldName_Footer = (DropDownList)gridView.FooterRow.FindControl("ddlFieldName_Footer");
                        DropDownList ddlFY_Footer = (DropDownList)gridView.FooterRow.FindControl("ddlFY_Footer");
                        TextBox txtFieldValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtFieldValue_Footer");
                        TextBox txtInterestValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtInterestValue_Footer");
                        TextBox txtPenaltyValue_Footer = (TextBox)gridView.FooterRow.FindControl("txtPenaltyValue_Footer");
                        TextBox tbxRowTotalValue_Footer = (TextBox)gridView.FooterRow.FindControl("tbxRowTotalValue_Footer");
                        TextBox tbxSettlement_Footer = (TextBox)gridView.FooterRow.FindControl("tbxSettlement_Footer");
                        TextBox tbxProvisionInbooks_Footer = (TextBox)gridView.FooterRow.FindControl("tbxProvisionInbooks_Footer");
                        //if (ddlFieldName_Footer.SelectedItem.Text == "")
                        //{
                        //    lblName = ddlFieldName_Footer.Items[1].Value;

                        //}
                        //else
                        //{
                        //    lblName = ddlFieldName_Footer.SelectedItem.Text;
                        //}
                        if (txtFieldValue_Footer != null
                            && txtInterestValue_Footer != null && txtPenaltyValue_Footer != null && tbxRowTotalValue_Footer != null
                            && tbxSettlement_Footer != null && tbxProvisionInbooks_Footer != null)
                        {
                            if ((int)ViewState["Mode"] == 0)
                            {

                                string lblFY = ddlFY_Footer.SelectedItem.Text;
                                // lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);
                                //if (!string.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue))
                                //{
                                ////if (!string.IsNullOrEmpty(lblName))
                                ////{
                                ////    lblID = Convert.ToInt32(ddlFieldName_Footer.Items[1].Value);
                                ////}
                                if (ViewState["dataTableCustomFields"] != null)
                                {
                                    DataTable dtcurrentTableCumtomFields = (DataTable)ViewState["dataTableCustomFields"];

                                    DataRow drNewRow = dtcurrentTableCumtomFields.NewRow();

                                    ////drNewRow["LableID"] = lblID;
                                    ////drNewRow["Label"] = lblName;
                                    drNewRow["FYear"] = lblFY;
                                    drNewRow["labelValue"] = txtFieldValue_Footer.Text;
                                    drNewRow["Interest"] = txtInterestValue_Footer.Text;
                                    drNewRow["Penalty"] = txtPenaltyValue_Footer.Text;
                                    drNewRow["Total"] = tbxRowTotalValue_Footer.Text;
                                    drNewRow["SettlementValue"] = tbxSettlement_Footer.Text;
                                    drNewRow["ProvisionInbook"] = tbxProvisionInbooks_Footer.Text;

                                    //add new row to DataTable
                                    dtcurrentTableCumtomFields.Rows.Add(drNewRow);

                                    //Delete Rows with blank LblID (if Any)
                                    dtcurrentTableCumtomFields = LitigationManagement.LoopAndDeleteBlankRows(dtcurrentTableCumtomFields);

                                    ViewState["CustomefieldCount"] = dtcurrentTableCumtomFields.Rows.Count;

                                    //Store the current data to ViewState
                                    ViewState["CurrentTablePutCallDtls"] = dtcurrentTableCumtomFields;

                                    if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()))
                                    {
                                        dtcurrentTableCumtomFields = LitigationManagement.GetColumnTotal(dtcurrentTableCumtomFields, true);

                                        //Rebind the Grid with the current data
                                        gridView.DataSource = dtcurrentTableCumtomFields;
                                        gridView.DataBind();
                                    }
                                    else
                                    {
                                        //Rebind the Grid with the current data
                                        grdCustomField.DataSource = dtcurrentTableCumtomFields;
                                        grdCustomField.DataBind();
                                    }
                                }
                            }//Add Mode End
                            else if ((int)ViewState["Mode"] == 1)
                            {
                                if (ViewState["CaseInstanceID"] != null)
                                {
                                    long caseInstanceID = Convert.ToInt64(ViewState["CaseInstanceID"]);

                                    if (txtFieldValue_Footer != null && caseInstanceID != 0)
                                    {

                                        if (caseInstanceID != 0)
                                        {
                                            bool validateData = true;
                                            bool saveSuccess = false;

                                            //if (!String.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue) && ddlFieldName_Footer.SelectedValue != "0")
                                            //{
                                            //    if (txtFieldValue_Footer.Text != "")
                                            //    {
                                            //        validateData = true;
                                            //    }
                                            //}

                                            if (validateData)
                                            {
                                                ////lblID = Convert.ToInt32(ddlFieldName_Footer.SelectedValue);

                                                ////if (lblID != 0)
                                                ////{
                                                tbl_NoticeCaseCustomParameter ObjParameter = new tbl_NoticeCaseCustomParameter()
                                                {
                                                    NoticeCaseType = 1,
                                                    NoticeCaseInstanceID = caseInstanceID,
                                                    LabelID = lblID,
                                                    LabelValue = txtFieldValue_Footer.Text,
                                                    Penalty = txtPenaltyValue_Footer.Text,
                                                    Interest = txtInterestValue_Footer.Text,
                                                    Total = tbxRowTotalValue_Footer.Text,
                                                    SettlementValue = tbxSettlement_Footer.Text,
                                                    ProvisionInBook = tbxProvisionInbooks_Footer.Text,
                                                    FYear = ddlFY_Footer.SelectedValue,
                                                    IsDeleted = false,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                saveSuccess = CaseManagement.CreateUpdateCustomsFieldNoticeOrCaseWise(ObjParameter);

                                                //if (CaseManagement.IsExistCustomeFieldParameterValue(ObjParameter))
                                                //{
                                                //    CaseManagement.CreateCustomeFieldParameterValue(ObjParameter);
                                                //    saveSuccess = true;
                                                //}

                                                if (saveSuccess)
                                                {
                                                    LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_NoticeCaseCustomParameter", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Parameter Added", true);

                                                    if (ViewState["CaseTypeUpdated"] != null)
                                                    {
                                                        if (ViewState["CaseTypeUpdated"].ToString() == "false")
                                                        {
                                                            saveSuccess = CaseManagement.UpdateCaseType(caseInstanceID, Convert.ToInt32(ddlCaseCategory.SelectedValue));
                                                            if (saveSuccess)
                                                            {
                                                                saveSuccess = LitigationManagement.DeletePreviousCustomParameter(1, caseInstanceID, Convert.ToInt32(ddlCaseCategory.SelectedValue));

                                                                if (saveSuccess)
                                                                    ViewState["CaseTypeUpdated"] = "true";
                                                            }
                                                        }
                                                    }

                                                    //Re-Bind Case Custom Parameter Details
                                                    if (ddlCaseCategory.SelectedItem.Text.ToUpper().Trim().Contains("Tax".ToUpper().Trim()) && AuthenticationHelper.CustomerID != 76)
                                                    {
                                                        BindCustomFields(grdCustomField_TaxLitigation, grdCustomField_TaxLitigation_History);
                                                        ////emmamiusers.Visible = true;
                                                    }

                                                    else
                                                    {
                                                        BindCustomFields(grdCustomField, grdCustomField_History);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }//Edit Mode End
                        }
                    }//Grid Check
                }

                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool ISDocumentVisible(int caseid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool result = false;
                    var details = (from row in entities.tbl_LitigationFileData
                                   where row.ID == caseid
                                   select row).FirstOrDefault();
                    if(details!=null)
                    {
                        if(AuthenticationHelper.Role== "CADMN")
                        {
                            result = true;
                        }
                        else
                        {
                            result = true;
                        }
                  
                    }
                  
                        
                    return result;
                }
            }
            catch(Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                VSCasePopup.CssClass = "alert alert-danger";
            }
            return false;
        }

        public void BindAdvocateBill(long caseinstanceid,long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_LitigationCaseAdvocateBill(Convert.ToInt32(customerid))
                            where row.NoticeCaseInstanceID == Convert.ToInt32(caseinstanceid)
                            select row).ToList();

                grdAdvocateBill.DataSource = data;
                grdAdvocateBill.DataBind();
            }
        }

      
        public static tbl_CaseAdvocateBill GetCaseAdvocatebillDetailsByID(int caseInstanceID, int AdvbillID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_CaseAdvocateBill
                                   where row.NoticeCaseInstanceID == caseInstanceID
                                   && row.ID == AdvbillID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }
        public void DeleteCaseAdvocateBill(long caseInstanceID, int AdvocateBillID)
        {
            try
            {
                if (AdvocateBillID != 0)
                {
                    if (CaseManagement.DeleteCaseAdvocateBill(caseInstanceID, AdvocateBillID, AuthenticationHelper.UserID))
                    {
                        LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_CaseAdvocateBill", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Advocate Bill Deleted", true);
                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Advocate Bill Deleted Successfully.";
                        ValidationSummary12.CssClass = "alert alert-success";
                    }
                    else
                    {
                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary12.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCaseOrderPopup.IsValid = false;
                cvCaseOrderPopup.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }
        public void DeleteCaseAdvocateBillDocument(long caseInstanceID, int AdvocateBillID)
        {
            try
            {
                if (AdvocateBillID != 0)
                {
                    if (CaseManagement.DeleteCaseAdvocateBillDocument(caseInstanceID, AdvocateBillID, AuthenticationHelper.UserID))
                    {
                        LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LitigationFileData", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Advocate Bill document Deleted", true);
                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Advocate Bill Deleted Successfully.";
                        ValidationSummary12.CssClass = "alert alert-success";
                    }
                    else
                    {
                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Something went wrong, Please try again.";
                        ValidationSummary12.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCaseOrderPopup.IsValid = false;
                cvCaseOrderPopup.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }
        protected void grdAdvocateBill_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdAdvocateBill.PageIndex = e.NewPageIndex;

                    //Re-Bind Case Advocate bill
                    BindAdvocateBill(Convert.ToInt32(ViewState["CaseInstanceID"]), CustomerID);
              
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAdvocateBillClear_Click(object sender, EventArgs e)
        {
            clearAdvBillControls();
            divAdveditBilldocument.Visible = false;
        }
        public void clearAdvBillControls()
        {
            try
            {
                lstAdvHearingRefNo.ClearSelection();
                tbxAdvInvoiceAmount.Text = "";
                tbxAdvInvoiceno.Text = "";
                tbxAdvRemark.Text = "";
                tbxinvoicedate.Text = "";
                ddlLawyerAdvocate.SelectedItem.Text = "Select User";
                ddlApprover1.ClearSelection();
                ddlApprover2.ClearSelection();
                ddlApprover3.ClearSelection();
                txtcurrency.Text = "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }



        protected void LnkAddAdvocateBill_Click(object sender, EventArgs e)
        {
            ViewState["Mode"] = 0;
            clearAdvBillControls();
            divAdveditBilldocument.Visible = false;
        }

        protected void btnAdvbillExport_Click(object sender, EventArgs e)
        {
            try
            {

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AdvocateBillDetail");
                    DataTable ExcelData = null;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {



                        if (ViewState["CaseInstanceID"] != null)
                        {
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                       
                            var Data = (from row in entities.SP_LitigationCaseAdvocateBill(Convert.ToInt32(customerID))
                                        where row.NoticeCaseInstanceID == Convert.ToInt32(caseInstanceID)
                                        select row).ToList();


                            var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerID));
                            DataTable table = Data.ToDataTable();

                            DataView view = new System.Data.DataView(table);
                            List<tbl_CaseAdvocateBill> cb = new List<tbl_CaseAdvocateBill>();

                            ExcelData = view.ToTable("Selected", false, "CaseNo", "Title", "HearingRef", "InvoiceNo", "InvoiceAmount", "Lawyer", "Approver1Email", "Approver2Email", "Approver3Email", "Status", "Remark", "InvoiceDate", "Currency");

                            ExcelData.Columns.Add("Sr.No", typeof(int)).SetOrdinal(0);

                            int rowCount = 0;
                            int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["Sr.No"] = ++rowCount;
                            }

                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Value = "Customer Name:";

                            exWorkSheet.Cells["B1"].Merge = true;
                            exWorkSheet.Cells["B1"].Value = cname;

                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Value = "Report Name:";

                            //exWorkSheet.Cells["B2:C2"].Merge = true;
                            exWorkSheet.Cells["B2"].Value = "Advocate Bill Report";

                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                            //exWorkSheet.Cells["B3:C3"].Merge = true;
                            exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");


                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "Sr.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(20);
                            exWorkSheet.Cells["A5"].Style.WrapText = true;

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Case No";
                            exWorkSheet.Cells["B5"].AutoFitColumns(40);
                            exWorkSheet.Cells["B5"].Style.WrapText = true;

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "Case Title";
                            exWorkSheet.Cells["C5"].AutoFitColumns(40);
                            exWorkSheet.Cells["C5"].Style.WrapText = true;

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "Hearing";
                            exWorkSheet.Cells["D5"].AutoFitColumns(30);
                            exWorkSheet.Cells["D5"].Style.WrapText = true;

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "Invoice No";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);
                            exWorkSheet.Cells["E5"].Style.WrapText = true;

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Invoice Amount";
                            exWorkSheet.Cells["F5"].AutoFitColumns(30);
                            exWorkSheet.Cells["F5"].Style.WrapText = true;


                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Lawyer";
                            exWorkSheet.Cells["G5"].AutoFitColumns(30);
                            exWorkSheet.Cells["G5"].Style.WrapText = true;

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "Approver 1";
                            exWorkSheet.Cells["H5"].AutoFitColumns(30);
                            exWorkSheet.Cells["H5" + count1].Style.WrapText = true;

                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["I5"].Value = "Approver 2";
                            exWorkSheet.Cells["I5"].AutoFitColumns(30);
                            exWorkSheet.Cells["I5" + count1].Style.WrapText = true;

                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["J5"].Value = "Approver 3";
                            exWorkSheet.Cells["J5"].AutoFitColumns(30);
                            exWorkSheet.Cells["J5"].Style.WrapText = true;

                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["K5"].Value = "Status";
                            exWorkSheet.Cells["K5"].AutoFitColumns(30);
                            exWorkSheet.Cells["K5"].Style.WrapText = true;

                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["L5"].Value = "Remark";
                            exWorkSheet.Cells["L5"].AutoFitColumns(60);
                            exWorkSheet.Cells["L5"].Style.WrapText = true;

                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["M5"].Value = "Invoice Date";
                            exWorkSheet.Cells["M5"].AutoFitColumns(20);
                            exWorkSheet.Cells["M5"].Style.WrapText = true;

                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["N5"].Value = "Currency";
                            exWorkSheet.Cells["N5"].AutoFitColumns(20);
                            exWorkSheet.Cells["N5"].Style.WrapText = true;

                        }


                    }
                   
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 5 + ExcelData.Rows.Count, 14])
                    {

                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 7, 5 + ExcelData.Rows.Count, 12])
                    {
                        col[1, 7, 5 + ExcelData.Rows.Count, 12].Style.Numberformat.Format = "dd/MM/yyyy";
                    }



                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AdvocateBillReport.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }

                //}
                //else
                //{

                //    cvCaseAuditLog.IsValid = false;
                //    cvCaseAuditLog.ErrorMessage = "Please select customer";



                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void grdAdvocateBillDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteAdvBillEditDocument"))
            {
                long CaseInstanceID = 0;
                long AdvbillID = 0;
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    bool Caseadvbill = CaseManagement.DeleteAdvBillEditDocument(Convert.ToInt32(commandArgs));
                    if (Caseadvbill)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AdvocateBillID"])))
                        {
                            AdvbillID = Convert.ToInt32(ViewState["AdvocateBillID"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CaseInstanceID"])))
                        {
                            CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        }
                        var lstResponseDocument = CaseManagement.GetCaseAdvBillDocuments(CaseInstanceID, AdvbillID, "CA");
                        if (lstResponseDocument.Count > 0)
                        {
                            grdAdvocateBillDocuments.DataSource = lstResponseDocument;
                            grdAdvocateBillDocuments.DataBind();
                            divAdveditBilldocument.Visible = true;
                        }
                        else
                        {
                            grdAdvocateBillDocuments.DataSource = lstResponseDocument;
                            grdAdvocateBillDocuments.DataBind();
                            divAdveditBilldocument.Visible = true;
                        }

                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Advocate Bill Document Deleted Successfully.";
                        ValidationSummary12.CssClass = "alert alert-success";
                        BindAdvocateBill(Convert.ToInt32(CaseInstanceID), AuthenticationHelper.CustomerID);
                    }
                }
            }
            else if (e.CommandName.Equals("DownloadAdvBillEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = CaseManagement.ShowandDownloadAdvbillFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                       
                            #region Normal 
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                }


                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=AdvocateBillDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            #endregion
                       
                    }
                    else
                    {
                        cvCasePopUpResponse.IsValid = false;
                        cvCasePopUpResponse.ErrorMessage = "No Document Available for Download.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                        return;
                    }

                    // DownloadCaseDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                }
            }
            else if (e.CommandName.Equals("ViewAdvBillEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = CaseManagement.ShowandDownloadAdvbillFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {
                       
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;

                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                lblMessage.Text = "";

                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenAdvBillDocviewer('" + DocumentPath + "');", true);
                            }
                            #endregion
                       
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenAdvBilldocfileReview();", true);
                    }
                }
            }
        }

        protected void rptAdvBilldocumentview_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    int advbillid = Convert.ToInt32(commandArgs[0]);
                    int casedocumentid = Convert.ToInt32(commandArgs[2]);


                    var AllinOneDocumentList = CaseManagement.GetCaseAdvBillDocumentByID(advbillid, casedocumentid);
                    //var AllinOneDocumentList = CaseManagement.GetCaseAdvBillDocumentByID(Convert.ToInt32(e.CommandArgument));

                    if (AllinOneDocumentList != null)
                    {
                      
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }
                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                string FileName = DateFolder + "/" + User + "" + extension;
                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenAdvBillDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenAdvBilldocfileReview();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptAdvBilldocumentview_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblAdvDocumentVersionView = (LinkButton)e.Item.FindControl("lblAdvDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblAdvDocumentVersionView);
            }
        }

        protected void grdAdvocateBill_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //LinkButton lnkBtnDeleteAdvocateBill = (LinkButton)e.Row.FindControl("lnkBtnDeleteAdvocateBill");
            //LinkButton lnkBtnEditCaseAdvocateBill = (LinkButton)e.Row.FindControl("lnkBtnEditCaseAdvocateBill");

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnDeleteAdvocateBill = (LinkButton)e.Row.FindControl("lnkBtnDeleteAdvocateBill");
                LinkButton lnkBtnEditCaseAdvocateBill = (LinkButton)e.Row.FindControl("lnkBtnEditCaseAdvocateBill");


                if (lnkBtnEditCaseAdvocateBill != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnEditCaseAdvocateBill);
                }
                if (lnkBtnDeleteAdvocateBill != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteAdvocateBill);

                    int CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                    int AdvBillID = 0;
                    AdvBillID = Convert.ToInt32(grdAdvocateBill.DataKeys[e.Row.RowIndex].Value);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var data = (from row in entities.tbl_CaseAdvocateBill
                                    where row.NoticeCaseInstanceID == CaseInstanceID
                                    && row.ID == AdvBillID && row.status == "Approved"
                                    select row.status).ToList();

                        var rejectdata = (from row in entities.tbl_CaseAdvocateBill
                                          where row.NoticeCaseInstanceID == CaseInstanceID
                                          && row.ID == AdvBillID && (row.status == "Rejected" || row.status=="Reassigned")
                                          select row.status).ToList();

                        if (data.Count > 0)
                        {
                            lnkBtnDeleteAdvocateBill.Visible = false;
                            lnkBtnEditCaseAdvocateBill.Visible = false;
                        }
                        if (rejectdata.Count > 0)
                        {
                            lnkBtnDeleteAdvocateBill.Visible = false;
                        }
                    }

                }
            }
        }

        protected void DropDownListHearingLawFirm_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);
            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.LitigationRoleID != null).ToList();
            if (!string.IsNullOrEmpty(DropDownListHearingLawFirm.SelectedValue))
            {
                var lawyerUsers = LitigationUserManagement.GetRequiredUsersByLawFirm(lstAllUsers, 2, Convert.ToInt32(DropDownListHearingLawFirm.SelectedValue));

                ListBoxLawyerHearing.DataTextField = "Name";
                ListBoxLawyerHearing.DataValueField = "ID";
                ListBoxLawyerHearing.DataSource = lawyerUsers;
                ListBoxLawyerHearing.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScript89", "rebindHearingLawyer();", true);
                UpdatePanel16.Update();
            }
            else
            {
                var query = (from row in lstAllUsers
                             where row.IsDeleted == false
                             && row.IsActive == true
                             select row).ToList();

                if (query.Count > 0)
                {
                    query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID != null).ToList();
                }

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                ListBoxLawyerHearing.DataTextField = "Name";
                ListBoxLawyerHearing.DataValueField = "ID";
                ListBoxLawyerHearing.DataSource = lstUsers;
                ListBoxLawyerHearing.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScript89", "rebindHearingLawyer();", true);
            UpdatePanel16.Update();
        }

        protected void lnkAddhearing_Click(object sender, EventArgs e)
        {
            clearResponseControls();
        }

        protected void tbxAdvInvoiceAmount_TextChanged(object sender, EventArgs e)
        {
            long total = 0;
            if(!String.IsNullOrEmpty(tbxAdvInvoiceAmount.Text))
            {
                total = long.Parse(tbxAdvInvoiceAmount.Text);
                if (total >= 300000)
                {
                    divApprover3.Visible = true;
                    ddlApprover3.Visible = true;
                }
                else
                {
                    divApprover3.Visible = false;
                    ddlApprover3.Visible = false;
                }
            }
            else
            {
                divApprover3.Visible = false;
                ddlApprover3.Visible = false;
            }
           
            //if(tbxAdvInvoiceAmount.Text >= 300000)
            //{
            //    ddlApprover1.Visible = true;
            //}
            //else
            //{
            //    ddlApprover1.Visible = false;
            //}
        }
    }
}