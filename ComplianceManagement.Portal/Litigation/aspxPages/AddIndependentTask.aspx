﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddIndependentTask.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.AddIndependentTask" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->

    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="../../NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            BindControls();
        });

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=tbxTaskDueDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                    });
            });
        }

        function OpenAddUserDetailPop() {
            $('#AddUserPopUp').modal('show');
            $('#IframeAddUser').attr('src', "../../Litigation/Masters/AddUser.aspx");
        }

        function OpenDocviewer(file) {
            $('#DocumentReviewPopUp1').modal('show');
            $('#CaseDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
    </script>

</head>
<body style="background-color:white">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:UpdatePanel ID="updategrid" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="VsAddTasValidate" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="AddTaskValidationGroup" />
                                <asp:CustomValidator ID="CvAddTasValidate" runat="server" EnableClientScript="False"
                                    ValidationGroup="AddTaskValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                <asp:ValidationSummary ID="VsAddTasValidateSuccess" runat="server" Display="none" class="alert alert-block alert-success fade in"
                                    ValidationGroup="AddTaskValidationGroup1" />
                                <asp:CustomValidator ID="CustomValidatorSuccess" runat="server" EnableClientScript="False"
                                    ValidationGroup="AddTaskValidationGroup1" Display="none" class="alert alert-block alert-success fade in" />
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Task Title</label>
                                    <asp:TextBox ID="tbxTaskTitle" runat="server" CssClass="form-control" Width="85.5%" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvTaskTitle" ErrorMessage="Provide Task Title"
                                        ControlToValidate="tbxTaskTitle" runat="server" ValidationGroup="AddTaskValidationGroup" Display="None" />
                                    <asp:RegularExpressionValidator ValidationGroup="AddTaskValidationGroup" Display="None" ControlToValidate="tbxTaskTitle" ID="revTaskTitle"
                                        ValidationExpression="^[\s\S]{1,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Task Title."></asp:RegularExpressionValidator>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Task Description</label>
                                    <asp:TextBox ID="tbxTaskDesc" runat="server" CssClass="form-control" Style="width: 85.5%; min-height: 115px;" TextMode="MultiLine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvTaskDesc" ErrorMessage="Provide Task Description"
                                        ControlToValidate="tbxTaskDesc" runat="server" ValidationGroup="AddTaskValidationGroup" Display="None" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6 input-group date">
                                    <div>
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 24.5%; display: block; float: left; font-size: 13px; color: #333;">
                                            Due Date</label>
                                    </div>

                                    <div class="col-md-6 input-group date" style="width: 72%">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar color-black"></span>
                                        </span>
                                        <asp:TextBox runat="server" placeholder="Due Date" class="form-control" ID="tbxTaskDueDate" AutoComplete="off"/>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvTaskDueDate" ErrorMessage="Provide Due Date"
                                        ControlToValidate="tbxTaskDueDate" runat="server" ValidationGroup="AddTaskValidationGroup" Display="None" />
                                </div>

                                <div class="form-group col-md-6">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Priority</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlTaskPriorityADD" DataPlaceHolder="Select Task Priority" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        CssClass="form-control" Width="70%">
                                        <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Expected Outcome</label>
                                    <asp:TextBox ID="tbxExpOutcome" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                        Assign To</label>
                                    <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333;">Internal User</label>
                                    <asp:DropDownListChosen ID="ddlTaskLawyerListInternal" CssClass="form-control" runat="server" Width="51%" AutoPostBack="true">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="form-group col-md-6">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">External User</label>
                                    <div style="float: left; width: 70%">
                                        <asp:DropDownListChosen runat="server" ID="ddlTaskUserExternal" DataPlaceHolder="Select User" Onchange="ddlTaskUserChange()" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            CssClass="form-control" Width="90%" />
                                       <%-- <asp:RequiredFieldValidator ID="rfvTaskUser" ErrorMessage="Please Select User to Assign Task"
                                            ControlToValidate="ddlTaskUserExternal" runat="server" ValidationGroup="AddTaskValidationGroup" Display="None" />--%>
                                    </div>
                                    <div style="float: right; text-align: center; width: 10%; margin-top: -5%;">
                                        <img id="lnkShowAddNewOwnerModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddUserDetailPop()" alt="Add New User" title="Add New User" />
                                        <asp:LinkButton ID="lnkBtnAssignUser" OnClick="lnkBtnAssignUser_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                    <asp:TextBox ID="tbxTaskRemark" runat="server" CssClass="form-control" Width="85.5%"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Relevant Document(s)</label>
                                    <div style="width: 100%;">
                                        <div style="width: 50%; float: left;">
                                            <asp:FileUpload ID="fuTaskDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                        </div>
                                    </div>
                                    <asp:TextBox ID="tbxTaskID" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row" runat="server" id="DivTaskEdit" visible="false">
                                <asp:GridView ID="grdTaskEditDoc" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                    Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="grdTaskEditDoc_RowCommand" OnRowDataBound="grdTaskEditDoc_RowDataBound"
                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                    <%--OnRowCommand="grdTaskEditDoc_RowCommand"--%>
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="FileName" ItemStyle-Width="50%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="TaskEditDocUpdate" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DeleteTaskEditDocument"
                                                            ID="lnkbtnTaskDelete" runat="server" OnClientClick="return confirm('Are you certain you want to Delete this file?');">
                                                             <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="delete" title="delete Documents" />
                                                        </asp:LinkButton>
                                                        <asp:LinkButton
                                                            CommandArgument='<%# Eval("ID")%>' CommandName="DownloadTaskEditDocument"
                                                            ID="lnkDownloadTaskEditDocument" runat="server">
                                                             <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                        </asp:LinkButton>

                                                        <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                            AutoPostBack="true" CommandName="ViewTaskEditDocument"
                                                            ID="lnkViewTaskEditDocument" runat="server">
                                                            <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                        </asp:LinkButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lnkbtnTaskDelete" />
                                                        <asp:PostBackTrigger ControlID="lnkDownloadTaskEditDocument" />
                                                        <%--<asp:PostBackTrigger ControlID="lnkViewTaskEditDocument" />--%>
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <EmptyDataTemplate>
                                        No Response Submitted yet.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12" style="text-align: center;">
                                    <asp:Button Text="Save" runat="server" ID="btnTaskSave" CssClass="btn btn-primary" OnClick="btnTaskSave_Click" 
                                        ValidationGroup="AddTaskValidationGroup"></asp:Button><%--OnClientClick="Displays()"--%>
                                    <asp:Button Text="Clear" runat="server" ID="btnTaskClear" CssClass="btn btn-primary" OnClick="btnTaskClear_Click" />
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnTaskSave" />
                        </Triggers>
                    </asp:UpdatePanel>
    </div>
    </form>

    <%--Add User--%>
    <div class="modal fade" id="AddUserPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content" style="width:114%">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Add New User</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;margin-left:15px">
                    <iframe src="about:blank" id="IframeAddUser" frameborder="0" runat="server" width="100%" height="405px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div>
                <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom">
                                    View Document(s)</label>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="CaseDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     <script type="text/javascript">
        function ddlTaskUserChange() {
            var selectedTaskUserID = $("#<%=ddlTaskUserExternal.ClientID %>").val();
            if (selectedTaskUserID != null) {
                if (selectedTaskUserID == 0) {
                    $("#lnkShowAddNewOwnerModal").show();
                }
                else {
                    $("#lnkShowAddNewOwnerModal>").hide();
                }
            }
        }

        function ClosePopUser() {
            $('#AddUserPopUp').modal('hide');
            document.getElementById('<%= lnkBtnAssignUser.ClientID %>').click();
        }
    </script>
</body>
</html>
