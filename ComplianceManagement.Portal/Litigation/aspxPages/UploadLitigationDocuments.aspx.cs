﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3.IO;
using Amazon.S3;
using Amazon;
using Microsoft.IdentityModel.Protocols;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class UploadLitigationDocuments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["NoticeCaseID"]))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Flag"]))
                    {
                        TxtFlag.Text = Request.QueryString["Flag"].ToString();
                    }
                }
                BindLitigationDocType();
                BindFinancialYear();
            }
        }
        private void BindLitigationDocType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstdocTypes = CaseManagement.GetLitigationDocTypes_All(customerID);

                ddlDocType.DataTextField = "Name";
                ddlDocType.DataValueField = "ID";

                ddlDocType.DataSource = lstdocTypes;
                ddlDocType.DataBind();
                ddlDocType.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        #region Save multi financial year
      
        public static object FillFnancialYear()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {      
                var query = (from row in entities.FinancialYearDetails
                             where row.IsDeleted==false
                             select row).OrderByDescending(e => e.FinancialYear).ToList();

                int[] rejectStatus = { 43, 44, 45, 46 };
                if (CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "RPAFY"))
                {
                    var rejectList = query.Where(i => rejectStatus.Contains(i.Id));
                    query = query.Except(rejectList).ToList();
                }

                return query;
            }
        }

        public void BindFinancialYear()
        {

            DropDownListChosen1.DataValueField = "Id";
            DropDownListChosen1.DataTextField = "FinancialYear";
            DropDownListChosen1.DataSource = FillFnancialYear();
            DropDownListChosen1.DataBind();
            //DropDownListChosen1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Financial Year", "-1"));
        }

        #endregion

        protected void btnUploadContractDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["NoticeCaseID"]))
                {
                    int DocTypeID = -1;
                    bool saveSuccess = false;
                    long NoticeCaseInstanceID = Convert.ToInt64(Request.QueryString["NoticeCaseID"]);
                    string docType = string.Empty;
                   
                    if (Convert.ToString(Request.QueryString["Flag"]) == "Cases")
                    {
                        docType = "C";
                    }
                    else
                    {
                        docType = "N";
                    }
                    if (!string.IsNullOrEmpty(ddlDocType.SelectedValue))
                    {
                        if (ddlDocType.SelectedValue != "-1")
                        {
                            DocTypeID = Convert.ToInt32(ddlDocType.SelectedValue);
                        }
                    }
                    if (NoticeCaseInstanceID > 0)
                    {

                        #region Upload Document
                        HttpFileCollection fileCollection = Request.Files;
                        if (fileCollection.Count > 0)
                        {
                            bool isBlankFile = false;
                            string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadfile = null;
                                uploadfile = fileCollection[i];
                                int filelength = uploadfile.ContentLength;
                                string fileName = Path.GetFileName(uploadfile.FileName);
                                string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                                if (!string.IsNullOrEmpty(fileName))
                                {
                                    if (filelength == 0)
                                    {
                                        isBlankFile = true;
                                        break;
                                    }
                                    else if (ext == "")
                                    {
                                        isBlankFile = true;
                                        break;
                                    }
                                    else
                                    {
                                        if (ext != "")
                                        {
                                            for (int j = 0; j < InvalidFileTypes.Length; j++)
                                            {
                                                if (ext == "." + InvalidFileTypes[j])
                                                {
                                                    isBlankFile = true;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            if (isBlankFile == false)
                            {
                                saveSuccess = uploadDocuments(NoticeCaseInstanceID, DocTypeID, Request.Files, "NoticeCaseFileUpload", null, null, txtDocTags.Text.Trim());

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog(docType, NoticeCaseInstanceID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice/Case Document(s) Uploaded", true);
                                }
                                if (saveSuccess)
                                {
                                    cvLitigationDocument.IsValid = false;
                                    cvLitigationDocument.ErrorMessage = "Document(s) uploaded successfully";
                                    vsContractDocument.CssClass = "alert alert-success";
                                }
                                else
                                {
                                    cvLitigationDocument.IsValid = false;
                                    cvLitigationDocument.ErrorMessage = "Something went wrong, during document upload, Please try again";
                                    vsContractDocument.CssClass = "alert alert-danger";
                                }
                            }
                            else
                            {
                                cvLitigationDocument.IsValid = false;
                                cvLitigationDocument.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                                vsContractDocument.CssClass = "alert alert-danger";
                            }
                        }
                        
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

      

        protected bool uploadDocuments(long contractID, int DocTypeID, HttpFileCollection fileCollection, string fileUploadControlName, long? taskID, long? taskResponseID, string fileTags)
        {
            bool saveSuccess = false;
            try
            {
                var AWSData = AmazonS3.GetAWSStorageDetail(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (AWSData != null)
                {
                    #region AWS Upload Document

                    if (!string.IsNullOrEmpty(Request.QueryString["NoticeCaseID"]))
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["Flag"]))
                        {
                            long NoticeCaseInstanceID = Convert.ToInt64(Request.QueryString["NoticeCaseID"]);
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                            string Flag = Convert.ToString(Request.QueryString["Flag"]);
                            string directoryPath = string.Empty;
                            string fileName = string.Empty;
                            string docType = string.Empty;
                            string FYName = string.Empty;
                            string FYName1 = string.Empty;
                            List<string> lstFinancialYearMapping = new List<string>();
                            if (Convert.ToString(Request.QueryString["Flag"]) == "Cases")
                            {
                                docType = "C";
                            }
                            else
                            {
                                docType = "N";
                            }


                            #region Save Financial Year Mappping
                            if (DropDownListChosen1.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachFY in DropDownListChosen1.Items)
                                {
                                    if (eachFY.Selected)
                                    {
                                        lstFinancialYearMapping.Add(Convert.ToString(eachFY.Text));
                                        FYName += eachFY.Text.ToString() + " ,";
                                    }
                                }

                                //FYName += eachFY.ToString() + " ,";
                                FYName1 = FYName;
                            }


                            #endregion



                            tbl_LitigationFileData objNoticeCaseDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = Convert.ToInt64(Request.QueryString["NoticeCaseID"]),
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,
                                DocType = docType,
                                DocTypeID = DocTypeID,
                                FinancialYear = FYName,
                                PhysicalLocation = txtphysicalLocation.Text,
                                FileNO = txtFileno.Text,

                            };

                            if (fileCollection.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                if (NoticeCaseInstanceID > 0)
                                {
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection[i];

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            string[] keys1 = fileCollection.Keys[i].Split('$');

                                            if (keys1[keys1.Count() - 1].Equals("LitigationFileUpload"))
                                            {
                                                fileName = uploadedFile.FileName;
                                            }

                                            objNoticeCaseDoc.FileName = fileName;

                                            //Get Document Version
                                            var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeCaseDoc);

                                            caseDocVersion++;
                                            objNoticeCaseDoc.Version = caseDocVersion + ".0";

                                            directoryPath = "LitigationDocuments\\" + customerID + "\\" + Flag + "\\" + Convert.ToInt32(NoticeCaseInstanceID) + "\\" + Flag + "Document\\" + objNoticeCaseDoc.Version;
                                            //directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/" + Flag + "/" + Convert.ToInt32(NoticeCaseInstanceID) + "/" + Flag + "Document/" + objNoticeCaseDoc.Version);
                                            IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                            S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                            if (!di.Exists)
                                            {
                                                di.Create();
                                            }

                                            Stream fs = uploadedFile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                            string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                                            string p_strPath = string.Empty;
                                            string dirpath = string.Empty;
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                            p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;                                            
                                            dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;
                                            //if (File.Exists(p_strPath))
                                            //    File.Delete(p_strPath);
                                            if (!Directory.Exists(dirpath))
                                            {
                                                Directory.CreateDirectory(dirpath);
                                            }
                                            FileStream objFileStrm = File.Create(p_strPath);
                                            objFileStrm.Close();
                                            File.WriteAllBytes(p_strPath, bytes);

                                            Guid fileKey1 = Guid.NewGuid();

                                            string AWSpath = directoryPath + "\\" + uploadedFile.FileName;

                                            objNoticeCaseDoc.FilePath = directoryPath.Replace(@"\", "/"); 
                                            objNoticeCaseDoc.FileKey = fileKey1.ToString();
                                            objNoticeCaseDoc.VersionDate = DateTime.Now;
                                            objNoticeCaseDoc.CreatedOn = DateTime.Now;
                                            objNoticeCaseDoc.FileSize = uploadedFile.ContentLength;
                                            //DocumentManagement.Litigation_SaveDocFiles(fileList);

                                            FileInfo localFile = new FileInfo(p_strPath);
                                            S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                            if (!s3File.Exists)
                                            {
                                                using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                {
                                                    localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                }
                                            }

                                            if (localFile.Exists) //check file exsit or not
                                            {
                                                //string dirpath1 = string.Empty;
                                                //string p_strPath1 = @"" + storagedrive;
                                                //string asd = Convert.ToString(AuthenticationHelper.UserID);
                                                //string asds = @"\asd"; 
                                                //string asd1 = Convert.ToString(uploadedFile.FileName);
                                                //string Final = p_strPath1 + "\\" +asd + "\\" + asd1;                                                
                                                ////String myPath = @"D:\GitProjects\05 oct  2020\Avacom Solution\hrplus_diy_workingsolution_live\ComplianceManagement.Portal\TempFiles\38\Payslip_ASL020 - Aug 2020.pdf";
                                                //File.Delete(Final);
                                                //String files = p_strPath;
                                                //File.Delete(files);                                                
                                                //localFile.Delete();
                                            }

                                            int FileID = CaseManagement.CreateCaseDocumentMappingGetID(objNoticeCaseDoc);
                                            if (FileID != 0)
                                            {
                                                saveSuccess = true;
                                            }
                                            if (string.IsNullOrEmpty(fileTags))
                                                fileTags = fileName;

                                            if (saveSuccess & !string.IsNullOrEmpty(fileTags))
                                            {
                                                string[] arrFileTags = fileTags.Trim().Split(',');

                                                if (arrFileTags.Length > 0)
                                                {
                                                    List<tbl_Litigation_FileDataTagsMapping> lstFileTagMapping = new List<tbl_Litigation_FileDataTagsMapping>();

                                                    for (int j = 0; j < arrFileTags.Length; j++)
                                                    {
                                                        tbl_Litigation_FileDataTagsMapping objFileTagMapping = new tbl_Litigation_FileDataTagsMapping()
                                                        {
                                                            FileID = FileID,
                                                            FileTag = arrFileTags[j].Trim(),
                                                            IsActive = true,
                                                            CreatedBy = AuthenticationHelper.UserID,
                                                            CreatedOn = DateTime.Now,
                                                            UpdatedBy = AuthenticationHelper.UserID,
                                                            UpdatedOn = DateTime.Now,
                                                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                                        };
                                                        lstFileTagMapping.Add(objFileTagMapping);
                                                    }

                                                    if (lstFileTagMapping.Count > 0)
                                                    {
                                                        saveSuccess = CaseManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                                    }
                                                }
                                            }
                                            fileList.Clear();
                                        }
                                    }//End For Each
                                }
                            }
                           
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Upload Document
                    if (!string.IsNullOrEmpty(Request.QueryString["NoticeCaseID"]))
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["Flag"]))
                        {
                           
                            long NoticeCaseInstanceID = Convert.ToInt64(Request.QueryString["NoticeCaseID"]);
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                            string Flag = Convert.ToString(Request.QueryString["Flag"]);
                            string directoryPath = string.Empty;
                            string fileName = string.Empty;
                            string docType = string.Empty;
                            string FYName = string.Empty;
                            string FYName1 = string.Empty;
                            List<string> lstFinancialYearMapping = new List<string>();
                            if (Convert.ToString(Request.QueryString["Flag"]) == "Cases")
                            {
                                docType = "C";
                            }
                            else
                            {
                                docType = "N";
                            }


                            #region Save Financial Year Mappping
                            if (DropDownListChosen1.Items.Count > 0)
                            {
                                foreach (System.Web.UI.WebControls.ListItem eachFY in DropDownListChosen1.Items)
                                {
                                    if (eachFY.Selected)
                                    {
                                        lstFinancialYearMapping.Add(Convert.ToString(eachFY.Text));
                                        FYName += eachFY.Text.ToString() + " ,";
                                    }
                                }

                                //FYName += eachFY.ToString() + " ,";
                                FYName1 = FYName;
                            }


                            #endregion



                            tbl_LitigationFileData objNoticeCaseDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = Convert.ToInt64(Request.QueryString["NoticeCaseID"]),
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,
                                DocType = docType,
                                DocTypeID = DocTypeID,
                                FinancialYear = FYName,
                                PhysicalLocation = txtphysicalLocation.Text,
                                FileNO = txtFileno.Text,

                            };

                            if (fileCollection.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                if (NoticeCaseInstanceID > 0)
                                {
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection[i];

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            string[] keys1 = fileCollection.Keys[i].Split('$');

                                            if (keys1[keys1.Count() - 1].Equals("LitigationFileUpload"))
                                            {
                                                fileName = uploadedFile.FileName;
                                            }

                                            objNoticeCaseDoc.FileName = fileName;

                                            //Get Document Version
                                            var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeCaseDoc);

                                            caseDocVersion++;
                                            objNoticeCaseDoc.Version = caseDocVersion + ".0";

                                            directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/" + Flag + "/" + Convert.ToInt32(NoticeCaseInstanceID) + "/" + Flag + "Document/" + objNoticeCaseDoc.Version);

                                            if (!Directory.Exists(directoryPath))
                                                Directory.CreateDirectory(directoryPath);

                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                            Stream fs = uploadedFile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                            objNoticeCaseDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objNoticeCaseDoc.FileKey = fileKey1.ToString();
                                            objNoticeCaseDoc.VersionDate = DateTime.Now;
                                            objNoticeCaseDoc.CreatedOn = DateTime.Now;
                                            objNoticeCaseDoc.FileSize = uploadedFile.ContentLength;
                                            DocumentManagement.Litigation_SaveDocFiles(fileList);
                                            int FileID = CaseManagement.CreateCaseDocumentMappingGetID(objNoticeCaseDoc);
                                            if (FileID != 0)
                                            {
                                                saveSuccess = true;
                                            }
                                            if (string.IsNullOrEmpty(fileTags))
                                                fileTags = fileName;

                                            if (saveSuccess & !string.IsNullOrEmpty(fileTags))
                                            {
                                                string[] arrFileTags = fileTags.Trim().Split(',');

                                                if (arrFileTags.Length > 0)
                                                {
                                                    List<tbl_Litigation_FileDataTagsMapping> lstFileTagMapping = new List<tbl_Litigation_FileDataTagsMapping>();

                                                    for (int j = 0; j < arrFileTags.Length; j++)
                                                    {
                                                        tbl_Litigation_FileDataTagsMapping objFileTagMapping = new tbl_Litigation_FileDataTagsMapping()
                                                        {
                                                            FileID = FileID,
                                                            FileTag = arrFileTags[j].Trim(),
                                                            IsActive = true,
                                                            CreatedBy = AuthenticationHelper.UserID,
                                                            CreatedOn = DateTime.Now,
                                                            UpdatedBy = AuthenticationHelper.UserID,
                                                            UpdatedOn = DateTime.Now,
                                                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                                        };
                                                        lstFileTagMapping.Add(objFileTagMapping);
                                                    }

                                                    if (lstFileTagMapping.Count > 0)
                                                    {
                                                        saveSuccess = CaseManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                                    }
                                                }
                                            }
                                            fileList.Clear();
                                        }
                                    }//End For Each
                                }
                            }
                           
                        }
                    }
                    #endregion
                }
                return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }


        //[WebMethod]
        //public static List<ListItem> GetDocTypes()
        //{
        //    int customerID = -1;
        //    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //    List<ListItem> lstdocumentType = new List<ListItem>();

        //    var lstAllDocTypes = CaseManagement.GetContractDocTypes_All(customerID);
        //if (lstAllDocTypes.Count > 0)
        //{
        //    for (int i = 0; i < lstAllDocTypes.Count; i++)
        //    {
        //        lstdocumentType.Add(new ListItem
        //        {
        //            Value = lstAllDocTypes.ToList()[i].ID.ToString(),
        //            Text = lstAllDocTypes.ToList()[i].TypeName,
        //        });
        //    }

        //    lstdocumentType.Add(new ListItem
        //    {
        //        Value = "0",
        //        Text = "Add New",
        //    });
        //}

        //    return lstdocumentType;
        //}

        protected void lnkBindocType_Click(object sender, EventArgs e)
        {
            BindLitigationDocType();
        }
    }
}