﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddUser" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <script type="text/javascript">
        function CloseMe() {
            window.parent.ClosePopUser();
        }
        $(function () {
            $("#tbxFirstName").keypress(function (e) {
                var keyCode = e.keyCode || e.which;

                $("#lblError").html("");

                //Regex for Valid Characters i.e. Alphabets.
                var regex = /^[A-Za-z, ]+$/;

                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only Alphabets allowed.");
                }

                return isValid;
            });
        });

        $(function () {
            $("#tbxLastName").keypress(function (e) {
                var keyCode = e.keyCode || e.which;

                $("#lblErrorlastname").html("");

                //Regex for Valid Characters i.e. Alphabets.
                var regex = /^[A-Za-z, ]+$/;

                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only Alphabets allowed.");
                }

                return isValid;
            });
        });


    </script>
    <style type="text/css">
        .chosen-container {
            width: 250px;
        }

        .ul {
            height: 150px;
        }
    </style>

</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="LitigationAddUser" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upUserDetail" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="NewAddLaywerValidate" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="NewAddLaywerValidate" Display="none" class="alert alert-block alert-danger fade in" />
                            <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                ValidationGroup="NewAddLaywerValidate" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div>
                            <div style="margin-bottom: 0px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    First Name</label>
                                <asp:TextBox runat="server" ID="tbxFirstName" Style="width: 250px;" CssClass="form-control" MaxLength="100" />                              
                                <span id="lblError" style="color: red;margin-left: 2px;"></span> 
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="First Name can not be empty."
                                    ControlToValidate="tbxFirstName" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid First Name."
                                    ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                            </div>

                            <div style="margin-bottom: 0px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Last Name</label>
                                <asp:TextBox runat="server" ID="tbxLastName" Style="width: 250px;" CssClass="form-control" MaxLength="100" />
                                 <span id="lblErrorlastname" style="color: red;margin-left: 2px;"></span> 
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Last Name can not be empty."
                                    ControlToValidate="tbxLastName" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid Last Name."
                                    ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                            </div>

                            <div style="margin-bottom: 15px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Email</label>
                                <asp:TextBox runat="server" ID="tbxEmail" Style="width: 250px;" MaxLength="200" CssClass="form-control" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Email can not be empty."
                                    ControlToValidate="tbxEmail" runat="server" ValidationGroup="NewAddLaywerValidate" Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid email."
                                    ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                            </div>

                            <div style="margin-bottom: 15px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Contact No</label>
                                <asp:TextBox runat="server" ID="tbxContactNo" Style="width: 250px;" CssClass="form-control"
                                    MaxLength="32" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Contact Number can not be empty."
                                    ControlToValidate="tbxContactNo" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid contact number."
                                    ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter only 10 digit contact number."
                                    ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>

                            <div id="divUserType" style="margin-bottom: 15px; display: none;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    User Type</label>
                                <asp:RadioButtonList ID="rbCheckType" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbCheckType_SelectedIndexChanged" AutoPostBack="true"
                                    Style="padding: 0px; margin: 0px; width: 250px;">
                                    <asp:ListItem Text="Internal" Value="Internal"></asp:ListItem>
                                    <asp:ListItem Text="External" Value="External" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                            <div runat="server" id="divLawyer" style="margin-bottom: 15px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Lawyer Firm
                                </label>
                                <asp:DropDownListChosen runat="server" ID="ddlLawyerList" Style="padding: 0px; margin: 0px; width: 250px;" CssClass="form-control"
                                    AllowSingleDeselect="false" DisableSearchThreshold="1" />
                            </div>

                            <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                    ValidationGroup="NewAddLaywerValidate" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                            </div>

                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 15px;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 100px"></div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
