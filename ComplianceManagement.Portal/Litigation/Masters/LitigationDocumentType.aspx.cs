﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class LitigationDocumentType : System.Web.UI.Page
    {
        private long CutomerID = AuthenticationHelper.CustomerID;
        protected bool flag;
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 10;
        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>)Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            if (!IsPostBack)
            {
                BindLitigationDocumentTypes();
                bindPageNumber();
                hideAddControls();
            }
        }

        public void hideAddControls()
        {
            try
            {
                if (authpage != null)
                {
                    if (authpage.Addval == false)
                    {
                        btnAddPromotor.Visible = false;
                    }
                    else
                    {
                        btnAddPromotor.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void lnkApply_Click(object sender, EventArgs e)
        {
            BindLitigationDocumentTypes();
            bindPageNumber();
        }

        private void BindLitigationDocumentTypes()
        {
            try
            {
                flag = false;
                var TypeList = CaseManagement.GetLitigationDocumentTypes(CutomerID);
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    TypeList = TypeList.Where(entry => entry.TypeName.ToLower().Contains(((tbxFilter.Text).ToLower()).Trim())).ToList();                                    
                }
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            TypeList = TypeList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            TypeList = TypeList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (TypeList.Count > 0)
                {
                    grdDocType.DataSource = TypeList;
                    grdDocType.DataBind();
                    Session["TotalRows"] = TypeList.Count;
                }
                else
                {
                    grdDocType.DataSource = TypeList;
                    grdDocType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdDocType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindLitigationDocumentTypes();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdDocType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displayed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdDocType.PageIndex = chkSelectedPage - 1;
            grdDocType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindLitigationDocumentTypes();
            ShowGridDetail();
        }
          
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        
        protected void grdDocType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdDocType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var TypeList = CaseManagement.GetLitigationDocumentTypes(CutomerID);
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    TypeList = TypeList.Where(entry => entry.TypeName.ToLower().Contains(((tbxFilter.Text).ToLower()).Trim())).ToList();
                }
                if (TypeList.Count > 0)
                    TypeList = TypeList.OrderBy(entry => entry.TypeName).ToList();

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    TypeList = TypeList.Where(entry => entry.TypeName.ToLower().Contains((tbxFilter.Text).ToLower())).ToList();
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    TypeList = TypeList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    TypeList = TypeList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdDocType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdDocType.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;
                if (TypeList.Count > 0)
                {
                    grdDocType.DataSource = TypeList;
                    grdDocType.DataBind();
                    Session["TotalRows"] = TypeList.Count;
                }
                else
                {
                    grdDocType.DataSource = TypeList;
                    grdDocType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdDocType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;

                if (e.CommandName.Equals("EditDocument"))
                {
                    long docTypeID = Convert.ToInt64(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["ContDocTypeID"] = docTypeID;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocTypePopup('" + docTypeID + "');", true);
                }
                else if (e.CommandName.Equals("DELETEDocType"))
                {
                    long docTypeID = Convert.ToInt64(e.CommandArgument);

                    if (CaseManagement.CheckDocTypeAssignmentExist(docTypeID))
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('Document Type can not be delete. One or more case/notice document are assigned to this Document Type');", true);
                    }
                    else
                    {
                        bool deleteSuccess = CaseManagement.DeleteDocType(docTypeID, CustomerID);

                        if (deleteSuccess)
                        {
                            BindLitigationDocumentTypes();
                            bindPageNumber();

                            cvPageDocType.IsValid = false;
                            cvPageDocType.ErrorMessage = "Document Type Deleted Successfully";
                            vsPageDocType.CssClass = "alert alert-success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdDocType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (authpage != null)
                {
                    if (authpage.Deleteval == false)
                    {
                        LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("lnkDeleteDocType");
                        LinkButton2.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("lnkDeleteDocType");
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Modify == false)
                    {
                        LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("lnkEditDocType");
                        LinkButton1.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("lnkEditDocType");
                        LinkButton1.Visible = true;
                    }
                    if (authpage.Deleteval == false && authpage.Modify == false)
                    {
                        grdDocType.Columns[2].Visible = false;
                    }
                }
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdDocType.PageIndex = 0;
                BindLitigationDocumentTypes();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               // cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}