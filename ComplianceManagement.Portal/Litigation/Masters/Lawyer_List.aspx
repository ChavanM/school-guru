﻿<%@ Page Title="Law Firm Master" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="Lawyer_List.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.Lawyer_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        element.style {
            display: inline-block;
            position: relative;
            width: 390px;
            height: 32px !important;
        }

        .chosen-results {
            height: 75px !important;
        }

        div#ContentPlaceHolder1_ddlLawyerTypes_sl {
            width: 390px !important;
            height: 35px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        a {
            color: #0055b3;
            text-decoration: none;
        }
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").unbind('click');
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        });


        function txtclick() {
            $("#divBranches").toggle("blind", null, 500, function () { });
        }
        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / Law Firm');
        });
        function openModal() {
            $('#Newaddremider').modal('show');
            return true;
        };
        function OpenLaywerModel() {
            $('#divUserDialog').modal('show');
            return true;
        };
        function CloseLaywerModel() {
            $('#divUserDialog').modal('hide');
        };

        function ReloadWindow() {
            location.reload();
        };

        function CloseWin() {
            $('#Newaddremider').modal('hide');
        };

        function OpenDepartmentPopup(a) {
            $('#AddDepartmentPopUp').modal('show');
            $('#ContentPlaceHolder1_IframeDepartment').attr('src', "../../Litigation/Masters/AddDepartMent.aspx?DepartmentID=" + a);
        }

        function OpenLawFirmPopupModel() {
            $('#AddLawFirmModelPopup').modal('show');
            $('#ContentPlaceHolder1_IFLawFirm').attr('src', "../../Litigation/Masters/AddLawFirm.aspx");
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <div style="margin: 5px">
                    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in" 
                                    EnableClientScript="False" ValidationGroup="ComplianceInstanceValidationGroup" Display="None"/>
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            </div>
                            <div class="col-md-3 colpadding0 entrycount">
                                <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search" CssClass="form-control" OnTextChanged="tbxtypeTofilter_TextChanged" AutoPostBack="true"/>
                            </div>

                            <div style="text-align: left; ">
                                <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" Width="8%" style="margin-left:15px" OnClick="lnkBtnApplyFilter_Click" data-toggle="tooltip" ToolTip="Apply" data-placement="bottom"/>
                                <asp:LinkButton CssClass="btn btn-primary" OnClientClick="openModal();" runat="server" ID="btnAddLaywer" style="float:right" OnClick="btnAddLaywer_Click" data-toggle="tooltip" ToolTip="Add New Law Firm">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                            </div>
                            <%--<i class='fa fa-plus'></i>--%>

                            <div class="panel" style="margin-bottom: 4px;">
                                <asp:GridView runat="server" ID="grdLawyer" AutoGenerateColumns="false" AllowPaging="true" AutoPostBack="true" AllowSorting="true"
                                    CssClass="table" GridLines="none" PageSize="10" OnSorting="grdLawyer_Sorting" Width="100%" ShowHeaderWhenEmpty="true"
                                    DataKeyNames="ID" OnRowCommand="grdLawyer_RowCommand" OnRowDataBound="grdLawyer_RowDataBound" OnRowCreated="grdLawyer_RowCreated">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr" HeaderStyle-Width="15px" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Law Firm" ItemStyle-Width="20%" SortExpression="FirstName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label runat="server" Visible="false" ID="lblID" Text='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("FirstName") %>' ToolTip='<%# Eval("FirstName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name" ItemStyle-Width="20%" Visible="false">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LastName") %>' ToolTip='<%# Eval("LastName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" ItemStyle-Width="20%" SortExpression="Email">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Email") %>' ToolTip='<%# Eval("Email") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contact Number" ItemStyle-Width="20%" SortExpression="ContactNumber">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContactNumber") %>' ToolTip='<%# Eval("ContactNumber") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Specialization" ItemStyle-Width="20%" SortExpression="Specilisation">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Specilisation") %>' ToolTip='<%# Eval("Specilisation") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Lawyer(s)" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbkID" runat="server" Visible="false"
                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Click to see List of Lawyer(s)" CommandName="VIEW_LawyerList" CommandArgument='<%# Eval("ID") %>'>Lawyer(s)</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-Width="5%" HeaderText="Action">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                    <asp:LinkButton runat="server" ToolTip="Edit Law Firm Details" data-toggle="tooltip" data-placement="bottom" CommandName="EDIT_Lawyer" OnClientClick="openModal();" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'>
                                                    <img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit" /></asp:LinkButton>
                                                    <asp:LinkButton Visible="false" runat="server" CommandName="DELETE_Lawyer" ToolTip="Delete Law Firm" data-toggle="tooltip" data-placement="bottom" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                                        OnClientClick="return confirm('Are you certain you want to delete this Law Firm?');">
                                                    <img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete" /></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ToolTip="Add New Lawyer" data-toggle="tooltip" data-placement="bottom" CommandName="Add_Lawyer" OnClientClick="OpenLaywerModel();" ID="lbtAddLawyer" CommandArgument='<%# Eval("ID") %>'>
                                                    <img src='<%# ResolveUrl("../../Images/add_icon_new.png")%>' alt="Add" /></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>No Record Found</EmptyDataTemplate>
                                </asp:GridView>

                            </div>

                        
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-8 colpadding0">
                            <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                <p style="padding-right: 0px !Important;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>&nbsp;- 
                                    <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                    <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                        </div>
                                 <div class="col-md-2 colpadding0">
                                     <div style="float: left; width: 60%">
                                <p class="clsPageNo">Show</p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Text="5" />
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2 colpadding0" style="float: right;">
                            <div style="float: left; width: 60%">
                                <p class="clsPageNo">Page No:</p>
                            </div>
                            <div style="float: right; width: 40%">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                    </div>
                            </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
                <%-- </section>--%>
            </div>
        </div>
    </div>


    <div class="modal fade" id="Newaddremider" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px">
                <div class="modal-header">
                    <label style="width: 15px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Law Firm</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">×</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upLawyerPop" runat="server" UpdateMode="Conditional" OnLoad="upLawyerPop_Load">
                        <ContentTemplate>
                            <div style="margin: 5px">

                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="NewAddLaywerValidate" />
                                    <asp:CustomValidator ID="CustomModifyAsignment" runat="server" EnableClientScript="False"
                                        ValidationGroup="NewAddLaywerValidate" Display="None" />
                                </div>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Law Firm</label>
                                        <asp:TextBox runat="server" ID="tbxFirstName" Style="width: 390px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty."
                                            ControlToValidate="tbxFirstName" runat="server" ValidationGroup="NewAddLaywerValidate"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; display: none;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Last Name</label>
                                        <asp:TextBox runat="server" ID="tbxLastName" Style="width: 390px;" CssClass="form-control"
                                            MaxLength="100" />
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Email</label>
                                        <asp:TextBox runat="server" ID="tbxEmail" Style="width: 390px;" MaxLength="200" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Email can not be empty."
                                            ControlToValidate="tbxEmail" runat="server" ValidationGroup="NewAddLaywerValidate"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                            ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid email."
                                            ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Contact No</label>
                                        <asp:TextBox runat="server" ID="tbxContactNo" Style="width: 390px;" CssClass="form-control"
                                            MaxLength="32" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Contact Number can not be empty."
                                            ControlToValidate="tbxContactNo" runat="server" ValidationGroup="NewAddLaywerValidate"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                            ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid contact number."
                                            ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                            ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter only 10 digit contact no."
                                            ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Address</label>
                                        <asp:TextBox runat="server" ID="tbxAddress" Style="width: 390px;" MaxLength="500" CssClass="form-control"
                                            TextMode="MultiLine" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Court Type</label>
                                        <asp:DropDownCheckBoxes ID="ddlLawyerTypes" runat="server" AutoPostBack="true" Visible="true"
                                            CssClass="form-control m-bot15" AddJQueryReference="false" UseButtons="false" UseSelectAllNode="false"
                                            Style="padding: 0px; margin: 0px; width: 390px; height: 37px;">
                                            <Style SelectBoxWidth="320" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Type" />
                                        </asp:DropDownCheckBoxes>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Specialization</label>
                                        <asp:TextBox runat="server" ID="tbxSpecilisation" Style="width: 390px;" MaxLength="500" CssClass="form-control" />
                                    </div>
                                   <div id="divBankdetails" runat="server" style="margin-bottom: 7px">
                                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Bank Details</label>
                                        <asp:TextBox runat="server" ID="txtBankDetails"  Style="width: 390px;" MaxLength="500" CssClass="form-control" />
                                   </div>
                                       <div id="divPanNo" runat="server" style="margin-bottom: 7px">
                                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            PAN No</label>
                                        <asp:TextBox runat="server" ID="txtPanNo"  Style="width: 390px;" MaxLength="500" CssClass="form-control" />
                                       <asp:RegularExpressionValidator ID="rgxPANCard" Display="None" runat="server"
                                            ValidationGroup="NewAddLaywerValidate" ErrorMessage="Invalid PAN Number."
                                            ControlToValidate="txtPanNo"  ValidationExpression="([A-Z]){5}([0-9]){4}([A-Z]){1}$"></asp:RegularExpressionValidator>
<%--                                          <asp:RegularExpressionValidator ID="rgxPANCard" runat="server" ValidationExpression="([A-Z]){5}([0-9]){4}([A-Z]){1}$" ControlToValidate="txtPanNo" ErrorMessage="Invalid PAN Number" CssClass = "error"></asp:RegularExpressionValidator>--%>
                                   </div>

                                    <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="NewAddLaywerValidate" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="ReloadWindow();" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 15px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                </div>
                                <div class="clearfix" style="height: 100px"></div>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal Pop-Up  --%>
    <div class="modal fade" id="divUserDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left: 14%;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content" style="width: 85%; height: auto;">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Lawyer</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <%--<vit:audituserdetailscontrol runat="server" id="udcInputForm" />--%>
                    <asp:UpdatePanel ID="upUsersPopup" runat="server" UpdateMode="Conditional" OnLoad="upUsersPopup_Load">
                        <ContentTemplate>
                            <div>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="vsUserPopup" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="UserPopupValidationGroup" />
                                    <asp:CustomValidator ID="cvUserPopup" runat="server" EnableClientScript="False"
                                        ErrorMessage="Email already exists." ValidationGroup="UserPopupValidationGroup" Display="None" />
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            First Name</label>
                                        <asp:TextBox runat="server" ID="tbxFirstNameUser" Style="width: 70%;" CssClass="form-control" MaxLength="100" />
                                        <span id="lblError" style="color: red; margin-left: 113px;"></span>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="First Name can not be empty."
                                            ControlToValidate="tbxFirstNameUser" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Last Name</label>
                                        <asp:TextBox runat="server" ID="tbxLastNameUser" Style="width: 70%;" CssClass="form-control"
                                            MaxLength="100" />
                                        <span id="lblError1" style="color: red; margin-left: 113px;"></span>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Last Name can not be empty."
                                            ControlToValidate="tbxLastNameUser" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Designation</label>
                                        <asp:TextBox runat="server" ID="tbxDesignation" Style="width: 70%;" CssClass="form-control"
                                            MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Designation can not be empty."
                                            ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid designation."
                                            ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Email</label>
                                        <asp:TextBox runat="server" ID="tbxEmailUser" Style="width: 70%;" MaxLength="200" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Email can not be empty."
                                            ControlToValidate="tbxEmailUser" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid email."
                                            ControlToValidate="tbxEmailUser" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Contact No</label>
                                        <asp:TextBox runat="server" ID="tbxContactNoUser" Style="width: 70%;" CssClass="form-control"
                                            MaxLength="32" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Contact Number can not be empty."
                                            ControlToValidate="tbxContactNoUser" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid contact number."
                                            ControlToValidate="tbxContactNoUser" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers" TargetControlID="tbxContactNoUser" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter only 10 digit contact no."
                                            ControlToValidate="tbxContactNoUser" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divddlLayerList">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Law Firm
                                            </label>
                                            <asp:DropDownList runat="server" ID="ddlLayerList" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15" onchange="ShowLawFirmAddbutton()"/>
                                             <img id="lnkShowAddNewLawFirmModal" style="float: right; display: none; margin-top: -30px" src="../../Images/add_icon_new.png"
                                                onclick="OpenLawFirmPopupModel();" alt="Add New Lawyer" title="Add New Lawyer" />
                                             <asp:LinkButton ID="lnkLawFirmBind" OnClick="lnkLawFirmBind_Click" Style="float: right; display: none;" runat="server"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Department</label>
                                        <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15" onchange="ddlDepartmentChange()"/>
                                       <img id="lnkAddNewDepartmentModal" style="float: right; display: none; margin-top: -30px" src="../../Images/add_icon_new.png" onclick="OpenDepartmentPopup('')" alt="Add New Department" title="Add New Department" /><br />
                                        <asp:CheckBox runat="server" ID="chkHead" Style="display: none; padding: 0px; margin: 0px; height: 30px; width: 10px; color: #333; margin-left: 165px" Text="Is Department Head" />
                                        <asp:CompareValidator ID="CVDept" ErrorMessage="Please select department." ControlToValidate="ddlDepartment"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup" Display="None" />
                                        <asp:LinkButton ID="lnkBtnDept" OnClick="lnkBtnDept_Click" Style="float: right; display: none;" runat="server"></asp:LinkButton>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Address</label>
                                        <asp:TextBox runat="server" ID="tbxAddressUser" Style="width: 70%;" MaxLength="500"
                                            TextMode="MultiLine" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divLitigationRole">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Role (Litigation)</label>
                                            <asp:DropDownList runat="server" ID="ddlLitigationRole" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlLitigationRole_SelectedIndexChanged" />
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select User Role." ControlToValidate="ddlLitigationRole"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">

                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Specialization</label>
                                        <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control" Style="width: 70%;" MaxLength="500" />
                                    </div>
                                </div>

                                <div id="div1" style="margin-bottom: 7px; display: none;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        User Type</label>
                                    <asp:RadioButtonList ID="rbCheckType" runat="server" RepeatDirection="Horizontal"
                                        Style="padding: 0px; margin: 0px; width: 390px;">
                                        <%--OnSelectedIndexChanged="rbCheckType_SelectedIndexChanged" AutoPostBack="true"--%>
                                        <asp:ListItem class="radio-inline" Text="Internal" Value="Internal" Selected="True"></asp:ListItem>
                                        <asp:ListItem class="radio-inline" Text="External" Value="External"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divCustomer">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Customer</label>
                                            <asp:DropDownList runat="server" ID="ddlCustomerPopup" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPopup_SelectedIndexChanged" />
                                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please Select Customer."
                                                ControlToValidate="ddlCustomerPopup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="UserPopupValidationGroup" Display="None" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divReportingTo">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Reporting to</label>
                                            <asp:DropDownList runat="server" ID="ddlReportingTo" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control m-bot15" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divCustomerBranch">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Location</label>
                                            <asp:TextBox runat="server" ID="tbxBranch" onclick="txtclick()" Style="padding: 5px; margin: 0px; width: 70%;" CausesValidation="true"
                                                CssClass="form-control" />
                                            <div style="margin-left: 21%; position: absolute; z-index: 10" id="divBranches">
                                                <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="100px" Width="112%"
                                                    Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                                </asp:TreeView>
                                            </div>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Please select Location."
                                                ControlToValidate="tbxBranch" runat="server" ValidationGroup="UserPopupValidationGroup" InitialValue="Select Location"
                                                Display="None" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">

                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Profile Picture</label>
                                        <asp:FileUpload ID="UserImageUpload" runat="server" ForeColor="#333" />
                                        <asp:Button ID="btnUpload" runat="server" Text="Upload" Visible="false" OnClick="Upload" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            &nbsp;
                                        </label>
                                        <asp:Image ID="ImageShow" runat="server" Height="100" Width="70%" ImageUrl="~/UserPhotos/DefaultImage.png" Visible="false" />
                                    </div>
                                </div>

                                <div style="margin-bottom: 7px">
                                </div>

                                <div style="margin-bottom: 7px">
                                </div>

                                <div style="margin-bottom: 7px">
                                </div>

                                <asp:Repeater runat="server" ID="repParameters">
                                    <ItemTemplate>
                                        <div style="margin-bottom: 7px">
                                            <asp:Label runat="server" ID="lblName" Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;"
                                                Text='<%# Eval("Name")  + ":"%>' />
                                            <asp:TextBox runat="server" ID="tbxValue" Style="height: 20px; width: 390px;" Text='<%# Eval("Value") %>'
                                                MaxLength='<%# Eval ("Length") %>' />
                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ValueID") %>' />
                                            <asp:HiddenField runat="server" ID="hdnEntityParameterID" Value='<%# Eval("ParameterID") %>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <div class="clearfix"></div>

                                <div style="margin-bottom: 7px; margin-top: 10px; text-align: center;">
                                    <%--float: right; margin-right: 257px; --%>;
                                    <asp:Button Text="Save" runat="server" ID="SaveLawyer" CssClass="btn btn-primary" OnClick="SaveLawyer_Click"
                                        ValidationGroup="UserPopupValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="CloseLaywerPopUp" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>

                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 25px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                                <div class="clearfix" style="height: 50px"></div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                            <%--<asp:AsyncPostBackTrigger ControlID="btnUpload" EventName="Click" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Department Popup--%>
    <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Add New Department</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDepartment" overflow="hidden" frameborder="0" runat="server" width="100%" height="209px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddLawFirmModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p0" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom" id="Lawfirmmodel">
                        Add/Edit Law Firm</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IFLawFirm" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function ClosePopDepartment() {
            $('#AddDepartmentPopUp').modal('hide');
            document.getElementById('<%= lnkBtnDept.ClientID %>').click();
        }

        function CloseLawFirmModal() {
            $('#AddLawFirmModelPopup').modal('hide');
            document.getElementById('<%= lnkLawFirmBind.ClientID %>').click();
        }

        function ddlDepartmentChange() {
            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == 0) {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }
         
        function ShowLawFirmAddbutton() {
            var selectedPartyID = $("#<%=ddlLayerList.ClientID %>").val();
            if (selectedPartyID != null) {
                if (selectedPartyID == "0") {
                    $("#lnkShowAddNewLawFirmModal").show();
                }
                else {
                    $("#lnkShowAddNewLawFirmModal").hide();
                }
            }
        }
    </script>
</asp:Content>
