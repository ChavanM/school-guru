﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="EntitiesAssignmentMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.EntitiesAssignmentMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        function fopenpopup() {
            $('#divContDocTypeDialog').modal('show');
        }

        function OpenEntitiesPopup(Flag) {
            $('#AddentitiesPopUp').modal('show');
            //$('#ContentPlaceHolder1_IframeEntities').attr('src', "../../Litigation/Masters/AddEntitiesAssignment.aspx");
            $('#ContentPlaceHolder1_IframeEntities').attr('src', "../../Litigation/Masters/AddEntitiesAssignment.aspx?Flag=" + Flag);
        }

        function CloseEntitiesPopup() {
            $('#AddentitiesPopUp').modal('hide');
            <%-- document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();--%>
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
    
    
      <script language="javascript" type="text/javascript">

       
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');                            
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
        //added by sandesh end

</script>


    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
  
    </style>
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {

            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });
        }
        
    </script>

      <script type="text/javascript">
    $(function () {
        $("[id*=tvBranches] input[type=checkbox]").bind("click", function () {
            var table = $(this).closest("table");
            if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                //Is Parent CheckBox
                var childDiv = table.next();
                var isChecked = $(this).is(":checked");
                $("input[type=checkbox]", childDiv).each(function () {
                    if (isChecked) {
                        $(this).attr("checked", "checked");
                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            } else {
                //Is Child CheckBox
                var parentDIV = $(this).closest("DIV");
                if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                    $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                } else {
                    $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                }
            }
        });


        $('#BodyContent_tbxFilterLocation').keyup(function () {
            FnSearch();
        });
    })
</script>

      <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .excelgriddisplay{
            display:none;
        }
               .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }

        .dd_chk_select {
            height: 30px !important;  
            text-align: center;          
            border-radius: 5px;
        }

        div.dd_chk_select div#caption
        {
            margin-top: 5px;
        }
    </style>

      <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });

        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    
        
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
           fhead('Entities Assignment');
        });

      </script>


     <%-- <script type="text/javascript">
 
           function showProgress() {              
            var updateProgress = $get("<%# updateProgress.ClientID %>");
            updateProgress.style.display = "block";
        }
    </script>--%>

      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
         <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <asp:ValidationSummary ID="vsEntities" runat="server" Display="none" class="alert alert-block alert-danger fade in" 
                    ValidationGroup="EntitiesValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="EntitiesValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

               <div class="col-md-12" style="margin: 0.4%;margin-left: -2%;">
                    <div class="col-md-3">

                        <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlUser"  DisableSearchThreshold="3"
                            DataPlaceHolder="Select User" class="form-control" Width="100%" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" />
                    </div>
                    <div class="col-md-3">

                        <div id="FilterLocationdiv" runat="server" style="margin-left: -7%;">


                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 32px; width: 300px;"
                                CssClass="form-control" />
                            <div style="margin-left: 1px; position: absolute; z-index: 10;" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px"
                                    Style="overflow: auto" ShowLines="true" ShowCheckBoxes="All" onclick="OnTreeClick(event)">
                                </asp:TreeView>

                                <div id="bindelement" style="background: white; height: 292px; display: none; width: 390px; border: 1px solid; overflow: auto;"></div>

                                <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" Text="Select" />
                                <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear" />
                            </div>
                        </div>
                    </div>
                    <div>

                        <asp:TextBox runat="server" ID="tbxFilter" AutoComplete="off" CssClass="form-control" MaxLength="50" Visible="false"
                            PlaceHolder="Type Name to Search" AutoPostBack="true" Width="70%" />
                    </div>
                    <div>
                        <asp:LinkButton ID="lnkBtn_RebindGrid" Style="display: none;" runat="server"></asp:LinkButton>
                    </div>
                    
                    <div class="col-md-2 colpadding0 text-right" style="float: right;">
                        <asp:LinkButton runat="server" ID="btnAddNew" margin-right="-40px"
                            data-toggle="tooltip" data-placement="bottom" ToolTip="Assign New Entities" OnClick="btnAddNew_Click" CssClass="btn btn-primary">
                            <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                    </div>

                </div>
           

            <div style="margin-bottom: 4px">
                <asp:GridView runat="server" ID="grdEntitiesAssignment" AutoGenerateColumns="false" AllowSorting="true" onrowcommand="grdEntitiesAssignment_RowCommand"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" ShowHeaderWhenEmpty="true"
                    DataKeyNames="ID" OnSorting="grdEntitiesAssignment_Sorting" OnRowCreated="grdEntitiesAssignment_RowCreated" OnRowDataBound="grdEntitiesAssignment_RowDataBound">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Branch" HeaderText="Entity/Branch/Location" SortExpression="Branch" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="60%" />
                          <asp:BoundField DataField="UserName" HeaderText="User Name" SortExpression="UserName" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40%" />
                      
                           <asp:TemplateField  HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:LinkButton  ID="LinkButton2" runat="server" CommandName="DELETE_Entities"
                                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete Entity assignment?');"><img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete Entities" title="Delete Entities Assignment" /></asp:LinkButton>
                                        </ItemTemplate>                            
                                    </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">                                
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width:100%; float: right; margin-right:-106%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <%--<div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                          class="form-control" Width="100%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                        </asp:DropDownListChosen>
                    </div>--%>
                    <div class="col-md-2 colpadding0" style="float: right;">
                        <div style="float: left; width: 50%">
                            <p class="clsPageNo">Page</p>
                        </div>
                        <div style="float: left; width: 50%">
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                class="form-control m-bot15" Width="100%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
            
          <%--  <div class="row">
                <div class="col-md-12 colpadding0" style="margin-top: 1%;">
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Previous" CssClass="btn btn-primary" ToolTip="Go to Previous Master (Entities)" data-toggle="tooltip"  Width="100%" runat="server" ID="LnkbtnPrevious" />
                        </div>
                        <div class="col-md-10 colpadding0">
                        </div>
                    </div>
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-10 colpadding0">
                        </div>
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Next" CssClass="btn btn-primary" runat="server" ToolTip="Go to Next Master (Entities)" data-toggle="tooltip"  ID="LnkbtnNext" Style="float: right; width: 100%;" />
                        </div>
                    </div>
                </div>
            </div>      --%>      
        </div>
      </div>
            </ContentTemplate>
    </asp:UpdatePanel>
 
  <div class="modal fade" id="AddentitiesPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label id="lblName" runat="server" class="modal-header-custom">
                        </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeEntities" runat="server" frameborder="0" width="100%" height="300px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
