﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class CaseStageMaster : System.Web.UI.Page
    {
        private long CutomerID = AuthenticationHelper.CustomerID;
        protected bool flag;
        protected tbl_PageAuthorizationMaster authpage;        
        protected int pageid =9;
        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>) Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            if (!IsPostBack)
            {
                BindCaseStageType();
                bindPageNumber();
                hideAddControls();
            }
        }

        public void hideAddControls()
        {

            if (authpage != null)
            {
                if (authpage.Addval == false)
                {
                    btnAddPromotor.Visible = false;
                }
                else
                {
                    btnAddPromotor.Visible = true;
                }
            }
        }

        private void BindCaseStageType()
        {
            try
            {
                flag = false;
                var TypeList = CaseManagement.GetCaseStageType(Convert.ToInt32(CutomerID), null);
                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text.Trim()))
                {
                    TypeList = TypeList.Where(entry => entry.TypeName.ToLower().Contains(((tbxtypeTofilter.Text.Trim()).ToLower()))).ToList();
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            TypeList = TypeList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            TypeList = TypeList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (TypeList.Count > 0)
                {
                    Session["TotalRows"] = TypeList.Count;
                    grdcaseStageType.DataSource = TypeList;
                    grdcaseStageType.DataBind();
                }
                else
                {
                    grdcaseStageType.DataSource = TypeList;
                    grdcaseStageType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdcaseStageType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (authpage != null)
                {
                    if (authpage.Deleteval == false)
                    {
                        LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("LinkButton2");
                        LinkButton2.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("LinkButton2");
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Modify == false)
                    {
                        LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("LinkButton1");
                        LinkButton1.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("LinkButton1");
                        LinkButton1.Visible = true;
                    }
                    if (authpage.Deleteval == false && authpage.Modify == false)
                    {
                        grdcaseStageType.Columns[2].Visible = false;
                    }
                }
            }
        }

        protected void grdcaseStageType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;

                if (e.CommandName.Equals("EDIT_CaseStageType"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CaseStageTypeID"] = ID;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCaseStageTypePopup('" + ID + "');", true);
                    tbl_TypeMaster Objcases = CaseManagement.GetCaseStageDetailByID(ID, CustomerID);
                    if (Objcases != null)
                    {

                    }
                }
                else if (e.CommandName.Equals("DELETE_CaseStageType"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CaseStageTypeID"] = ID;
                    bool CheckRef = CaseManagement.CheckRefcasestageexist(ID, CustomerID);
                    if (!CheckRef)
                    {
                        bool _objdeletCaseStage = CaseManagement.DeleteCaseStageDetailByID(ID, CustomerID);
                        if (_objdeletCaseStage == true)
                        {
                            BindCaseStageType();
                            bindPageNumber();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('Case Stage can not be Delete. One or more cases are assigned to this case stage');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }


        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindCaseStageType();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCaseStageTypePopup('" + ID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void grdcaseStageType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var TypeList = CaseManagement.GetCaseStageType(Convert.ToInt32(CutomerID), null);
                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text.Trim()))
                {
                    TypeList = TypeList.Where(entry => entry.TypeName.ToLower().Contains(((tbxtypeTofilter.Text.Trim()).ToLower()))).ToList();
                }

                List<object> dataSource = new List<object>();
                foreach (var casestageinfo in TypeList)
                {
                    dataSource.Add(new
                    {
                        casestageinfo.TypeName,
                        casestageinfo.ID
                    });
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;
                foreach (DataControlField field in grdcaseStageType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdcaseStageType.Columns.IndexOf(field);
                    }
                }
                flag = true;

                if (dataSource.Count > 0)
                {
                    Session["TotalRows"] = dataSource.Count;
                    grdcaseStageType.DataSource = dataSource;
                    grdcaseStageType.DataBind();
                }
                else
                {
                    grdcaseStageType.DataSource = dataSource;
                    grdcaseStageType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        protected void grdcaseStageType_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdcaseStageType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCaseStageType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdcaseStageType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdcaseStageType.PageIndex = chkSelectedPage - 1;
            grdcaseStageType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindCaseStageType(); ShowGridDetail();
        }

        protected void tbxtypeTofilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdcaseStageType.PageIndex = 0;
                BindCaseStageType();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
    }
}