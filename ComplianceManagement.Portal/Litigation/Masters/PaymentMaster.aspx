﻿<%@ Page Title="Payment Type Master" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="PaymentMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.PaymentMaster" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        tbxtypeTofilter
           .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>

    <script type="text/javascript">
        function openPopup() {
            $('#divPaymentDialog').modal('show');
        }

        var removeMyIFrame = function () {
            $('#MyiFrame').remove();
        }

        function OpenPaymentPopup(PaymentID) {
            $('#AddPaymentPopUp').modal('show');
            $('#ContentPlaceHolder1_IframePayment').attr('src', "../../Litigation/Masters/AddPayment.aspx?PaymentID=" + PaymentID);
        }

        function ClosePopPayment() {
            $('#AddPaymentPopUp').modal('hide');
            location.reload();
        }
        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters/ Payment Type');
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upPaymentMaster" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <section class="panel">    
                            
                         <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PaymentPageValidationGroup" />
                            <asp:CustomValidator ID="cvPage" runat="server" EnableClientScript="False"
                                ValidationGroup="PaymentPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                         </div>   
                                         
                         <div class="col-md-12 colpadding0">
                           
                             <div class="col-md-3 colpadding0 entrycount">
                                <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search" CssClass="form-control" />
                                </div>
                             <div class="col-md-2 colpadding0"></div>
                            <div class="col-md-7 colpadding0" style="text-align:right">
                                <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" Width="10%" OnClick="lnkBtnApplyFilter_Click" data-toggle="tooltip" ToolTip="Apply"/>
                                <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClick="btnAddPayment_Click"
                                    ID="btnAddPayment" data-toggle="tooltip" ToolTip="Add New Payment Type">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                            </div>
                        </div>

                         <div style="margin-bottom: 4px">           
                            <asp:GridView runat="server" ID="grdPayment" AutoGenerateColumns="false" AllowSorting="true" OnRowCommand="grdPayment_RowCommand"
                               PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" ShowHeaderWhenEmpty="true"
                                 DataKeyNames="ID" OnSorting="grdPayment_Sorting" OnRowCreated="grdPayment_RowCreated" OnRowDataBound="grdPayment_RowDataBound">
                                <Columns>
                                     <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                     <ItemTemplate>
                                                      <%#Container.DataItemIndex+1 %>
                                     </ItemTemplate>
                                     </asp:TemplateField>
                                    <asp:BoundField DataField="TypeName" HeaderText="Payment Type" SortExpression="TypeName" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="87%" />                        
                                    <asp:TemplateField  HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Payment" ToolTip="Edit Payment Type" data-toggle="tooltip"
                                                CommandArgument='<%# Eval("ID") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit Observation"/></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_Payment" CommandArgument='<%# Eval("ID") %>' ToolTip="Delete Payment Type" data-toggle="tooltip"
                                                OnClientClick="return confirm('Are you certain you want to delete this Payment Type?');">
                                                <img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete Payment Type"/></asp:LinkButton>
                                        </ItemTemplate>                            
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />  
                                 <PagerSettings Visible="false" />             
                               <EmptyDataTemplate>
                                   No Record Found
                               </EmptyDataTemplate>
                            </asp:GridView>                     
                        </div>

                         <div class="col-md-12 colpadding0">
                            <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>&nbsp;- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                             <div class="col-md-2 colpadding0">
                                 <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">                        
                                    <asp:ListItem Text="5"/>
                                    <asp:ListItem Text="10" Selected="True"/>
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                                 </div>
                            <div class="col-md-2 colpadding0" style="float:right;">
                                <div style="float:left;width:50%">
                                    <p class="clsPageNo">Page</p>
                                </div>
                                <div style="float:left;width:50%">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                     class="form-control m-bot15" Width="100%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                    </asp:DropDownListChosen>
                                </div>
                            </div>   
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />                 
                        </div>                                
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="AddPaymentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 30%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Payment</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent();">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframePayment" frameborder="0" runat="server" width="100%" height="210px"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

