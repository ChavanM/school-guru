﻿<%@ Page Title="Document Type Master :: Litigation" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="LitigationDocumentType.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.LitigationDocumentType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('Masters/ Document Type');
        });
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
        </script>
    <script type="text/javascript">
        
        function fopenpopup() {
            $('#AddDocumentTypePopUp').modal('show');
            $("#<%=IframeDocType.ClientID %>").attr('src', "../../Litigation/Masters/AddDocumentType.aspx");
         }

        function OpenDocTypePopup(LitigationDocId) {
             $('#AddDocumentTypePopUp').modal('show');
             $("#<%=IframeDocType.ClientID %>").attr('src', "../../Litigation/Masters/AddDocumentType.aspx?LitigationDocId=" + LitigationDocId);
        }

        function CloseDocTypePopup() {
            $('#AddDocumentTypePopUp').modal('hide');
            location.reload();
        }
        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
    <style type="text/css">
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <asp:UpdatePanel ID="UpDocType" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
            <div class="row">
                <asp:ValidationSummary ID="vsPageDocType" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="PageDocTypeValidationGroup" />
                <asp:CustomValidator ID="cvPageDocType" runat="server" EnableClientScript="False"
                    ValidationGroup="PageDocTypeValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-4 colpadding0">
                        <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50"
                            AutoComplete="off" placeholder="Type to Search" OnTextChanged="tbxFilter_TextChanged" AutoPostBack="true" />
                    </div>
                    <div class="col-md-6 colpadding0">
                    </div>
                    <div class="col-md-2 colpadding0">
                        <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" ID="btnAddPromotor" Style="float: right; margin-left: 3%;" OnClientClick="fopenpopup();"  data-toggle="tooltip" ToolTip="Add New Document Type">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>

                        <asp:LinkButton Text="Apply" CssClass="btn btn-primary" OnClick="lnkApply_Click" Style="float: right;"
                            runat="server" ID="lnkApply" data-toggle="tooltip" ToolTip="Apply"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <asp:GridView runat="server" ID="grdDocType" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" AllowSorting="true" GridLines="none" Width="100%"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" DataKeyNames="ID" OnRowDataBound="grdDocType_RowDataBound"
                    OnRowCreated="grdDocType_RowCreated" OnSorting="grdDocType_Sorting" OnRowCommand="grdDocType_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="TypeName" HeaderText="Document Type" SortExpression="TypeName" ItemStyle-Width="88%" />

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditDocType" runat="server" CommandName="EditDocument" ToolTip=" Edit Document Type" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID") %>'>
                                    <img src='<%# ResolveUrl("/Images/edit_icon_new.png")%>' alt="Edit" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteDocType" runat="server" CommandName="DELETEDocType" ToolTip=" Delete Document Type" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Document Type?');">
                                    <img src='<%# ResolveUrl("/Images/delete_icon_new.png")%>' alt="Delete" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width: 23%; float: right; margin-right: 3%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5"/>
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
                            </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="lnkApply" />
                </Triggers>
                </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="AddDocumentTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 250px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Document Type</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDocType" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
