﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class Lawyer_List : System.Web.UI.Page
    {
        protected bool flag;
        private long CustomerID = AuthenticationHelper.CustomerID;
        public static List<int> LawyerTypeList = new List<int>();
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageID = 2;
        public bool bankDetails;
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>) Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageID
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            bankDetails = CaseManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "BankDetails");
            if (bankDetails == true)
            {
                divBankdetails.Visible = true;
                divPanNo.Visible = true;
            }
            else
            {
                divBankdetails.Visible = false;
                divPanNo.Visible = false;
            }
            if (!IsPostBack)
            {
                BindData();
                BindLawyerType();
                ViewState["Mode"] = 0;
                BindCustomers(ddlCustomerPopup);
                BindDepartment();
                BindRoles();
                BingLawyerList();
                bindPageNumber();
                flag = false;
                hideAddControls();
                //  ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
        }

        public void hideAddControls()
        {
            if (authpage != null)
            {
                if (authpage.Addval == false)
                {
                    btnAddLaywer.Visible = false;
                }
                else
                {
                    btnAddLaywer.Visible = true;
                }
            }
        }
        private void BindData()
        {
            try
            {
                long CustomerId = AuthenticationHelper.CustomerID;

                var lstLawyer = LawyerManagement.GetLawyerListAllNew(CustomerId);

                if (lstLawyer.Count > 0)
                    lstLawyer = lstLawyer.OrderBy(entry => entry.FirstName).ToList();
                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    lstLawyer = lstLawyer.Where(entry => entry.FirstName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                    || entry.LastName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) 
                    || entry.Email.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) 
                    || entry.ContactNumber.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                    || entry.Specilisation.ToLower().Contains((tbxtypeTofilter.Text).ToLower().Trim())).ToList();
                }
              
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstLawyer = lstLawyer.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstLawyer = lstLawyer.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (lstLawyer.Count > 0)
                {
                    grdLawyer.DataSource = lstLawyer;
                    Session["TotalRows"] = lstLawyer.Count;
                    grdLawyer.DataBind();
                }
                else
                {
                    grdLawyer.DataSource = lstLawyer;
                    grdLawyer.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void BindLawyerType()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                var lstlawyerType = LawyerManagement.FillLawyerTypeList(customerID);

                ddlLawyerTypes.DataTextField = "CourtType";
                ddlLawyerTypes.DataValueField = "CourtID";

                ddlLawyerTypes.Items.Clear();

                ddlLawyerTypes.DataSource = lstlawyerType;
                ddlLawyerTypes.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdLawyer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long customerID = AuthenticationHelper.CustomerID;
                if (e.CommandName.Equals("EDIT_Lawyer"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["StateID"] = ID;
                    ViewState["LawyerID"] = Convert.ToInt32(e.CommandArgument);
                    var LawyerID = LawyerManagement.LawyerDataGetByID(ID);
                    tbxFirstName.Text = LawyerID.FirstName;
                    tbxLastName.Text = LawyerID.LastName;
                    tbxEmail.Text = LawyerID.Email;
                    tbxContactNo.Text = LawyerID.ContactNumber;
                    tbxAddress.Text = LawyerID.Address;
                    tbxSpecilisation.Text = LawyerID.Specilisation;
                    txtBankDetails.Text = LawyerID.BankDetails;
                    txtPanNo.Text = LawyerID.PanCard;
                    var lawyermap = LitigationLaw.GetLawyerCourtMappingData(ID);
                    foreach (var item in lawyermap)
                    {
                        var aa = item.CourtID;
                        ddlLawyerTypes.Items.FindByValue(Convert.ToString(aa)).Selected = true;
                    }
                    upLawyerPop.Update();
                    BindData();
                }
                else if (e.CommandName.Equals("DELETE_Lawyer"))
                {
                    int lawFirmID = Convert.ToInt32(e.CommandArgument);
                    
                    if (CaseManagement.CheckLawFirmMappingExist(lawFirmID))
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('Law Firm can not be delete. One or more case/notice/Lawyer are assigned to this Law Firm');", true);
                    }
                    else
                    {
                        LawyerManagement.DeleteLawyerData(lawFirmID, customerID);
                        LawyerCourtMapping objCourt = new LawyerCourtMapping();
                        objCourt.LawyerID = lawFirmID;
                        LawyerManagement.UpdateLawMappingDataFalseAll(objCourt);
                        upLawyerPop.Update();
                        BindData();
                        bindPageNumber();
                    }
                }
                else if (e.CommandName.Equals("Add_Lawyer"))
                {
                    ddlLayerList.SelectedValue = Convert.ToString(e.CommandArgument);
                    AddNewUser();
                }
                else if (e.CommandName.Equals("VIEW_LawyerList"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect("~/Litigation/Masters/LitigationUser_List.aspx?FirmID=" + ID, false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdLawyer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblID = (e.Row.FindControl("lblID") as Label);
                    LinkButton lbkID = (e.Row.FindControl("lbkID") as LinkButton);

                    List<object> lstUsers = new List<object>();
                    lstUsers = LitigationUserManagement.GetLitigationUsersByLawyerID(Convert.ToInt32(CustomerID), Convert.ToInt32(lblID.Text));
                    if (lstUsers.Count > 0)
                    {
                        lbkID.Visible = true;
                    }
                    else
                    {
                        lbkID.Visible = false;
                    }

                    if (authpage != null)
                    {
                        if (authpage.Deleteval == false)
                        {
                            LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("lbtnDelete");
                            LinkButton2.Visible = false;
                        }
                        else
                        {
                            LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("lbtnDelete");
                            LinkButton2.Visible = true;
                        }
                        if (authpage.Modify == false)
                        {
                            LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("lbtnEdit");
                            LinkButton1.Visible = false;
                        }
                        else
                        {
                            LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("lbtnEdit");
                            LinkButton1.Visible = true;
                        }
                        if (authpage.Modify == false && authpage.Deleteval == false)
                        {
                            grdLawyer.Columns[2].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdLawyer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdLawyer.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;

                tbl_Lawyer objlawyer = new tbl_Lawyer()
                {
                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    Address = tbxAddress.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    IsActive = true,
                    CustomerID = Convert.ToInt32(CustomerID),
                    Specilisation = tbxSpecilisation.Text,
                    BankDetails=txtBankDetails.Text,
                    PanCard=txtPanNo.Text
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objlawyer.ID = Convert.ToInt32(ViewState["LawyerID"]);//Need to change DeptID
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (LawyerManagement.LawyerDatacheckExist(objlawyer.FirstName, objlawyer.LastName, CustomerID))
                    {
                        CustomModifyAsignment.IsValid = false;
                        CustomModifyAsignment.ErrorMessage = "Lawyer Name Already Exists";
                    }
                    else
                    {
                        LawyerManagement.CreateLawyerData(objlawyer);
                        LawyerTypeList.Clear();
                        for (int i = 0; i < ddlLawyerTypes.Items.Count; i++)
                        {
                            if (ddlLawyerTypes.Items[i].Selected)
                            {
                                LawyerTypeList.Add(Convert.ToInt32(ddlLawyerTypes.Items[i].Value));
                            }
                        }
                        LawyerCourtMapping objCourt = new LawyerCourtMapping();
                        var LawyerID = LawyerManagement.GetlawyerID(objlawyer);
                        foreach (var aItem in LawyerTypeList)
                        {
                            objCourt.CourtID = aItem;
                            objCourt.LawyerID = LawyerID.ID;
                            objCourt.IsActive = true;
                            LawyerManagement.CreateLawyerMapping(objCourt);
                        }
                        CustomModifyAsignment.IsValid = false;
                        CustomModifyAsignment.ErrorMessage = "Law Firm Details Saved Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                        tbxFirstName.Text = string.Empty;
                        tbxLastName.Text = string.Empty;
                        tbxEmail.Text = string.Empty;
                        tbxContactNo.Text = string.Empty;
                        tbxAddress.Text = string.Empty;
                        tbxSpecilisation.Text = string.Empty;
                        txtBankDetails.Text = string.Empty;
                        txtPanNo.Text = string.Empty;
                        ddlLawyerTypes.Items.Clear();
                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    LawyerManagement.UpdatelawyerData(objlawyer);
                    LawyerTypeList.Clear();
                    for (int i = 0; i < ddlLawyerTypes.Items.Count; i++)
                    {
                        if (ddlLawyerTypes.Items[i].Selected)
                        {
                            LawyerTypeList.Add(Convert.ToInt32(ddlLawyerTypes.Items[i].Value));
                        }
                    }
                    LawyerCourtMapping objCourt = new LawyerCourtMapping();
                    objCourt.LawyerID = objlawyer.ID;
                    LawyerManagement.UpdateLawMappingDataFalseAll(objCourt);
                    foreach (var aItem in LawyerTypeList)
                    {
                        objCourt.CourtID = aItem;
                        objCourt.LawyerID = objlawyer.ID;
                        objCourt.IsActive = true;
                        if (LitigationLaw.CheckLawyercourtMappingExist(objCourt))
                        {
                            LawyerManagement.UpdateLawMappingData(objCourt);
                        }
                        else
                        {
                            LawyerManagement.CreateLawyerMapping(objCourt);
                        }
                    }
                    CustomModifyAsignment.IsValid = false;
                    CustomModifyAsignment.ErrorMessage = "Details Updated Successfully.";
                    ValidationSummary1.CssClass = "alert alert-success";
                }
                upLawyerPop.Update();
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdLawyer.PageIndex = chkSelectedPage - 1;

            grdLawyer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData();
            ShowGridDetail();
        }

        protected void btnAddLaywer_Click(object sender, EventArgs e)
        {
            tbxFirstName.Text = string.Empty;
            tbxLastName.Text = string.Empty;
            tbxEmail.Text = string.Empty;
            tbxContactNo.Text = string.Empty;
            tbxAddress.Text = string.Empty;
            // ddlLawyerTypes.Items.Clear();
            upLawyerPop.Update();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                //DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void upLawyerPop_Load(object sender, EventArgs e)
        {

        }

        protected void upUsersPopup_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlLitigationRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlLitigationRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");

                if (divCustomer.Visible && AuthenticationHelper.Role != "CADMN")
                {
                    //ddlCustomerPopup.SelectedValue = "-1";
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }
                if (divCustomerBranch.Visible)
                {
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                if (divReportingTo.Visible)
                {
                    BindReportingTo();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlCustomerPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                BindReportingTo();
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Branch";
                ScriptManager.RegisterStartupScript(this.upUsersPopup, this.upUsersPopup.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void SaveLawyer_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);

                int? LaywerID = null;

                if (!string.IsNullOrEmpty(ddlLayerList.SelectedValue))
                {
                    if (ddlLayerList.SelectedValue != "-1")
                    {
                        LaywerID = Convert.ToInt32(ddlLayerList.SelectedValue);
                    }
                }

                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstNameUser.Text.Trim(),
                    LastName = tbxLastNameUser.Text.Trim(),
                    Designation = tbxDesignation.Text.Trim(),
                    Email = tbxEmailUser.Text.Trim(),
                    ContactNumber = tbxContactNoUser.Text.Trim(),
                    Address = tbxAddressUser.Text.Trim(),
                    //RoleID = getproductcOMPLIANCE,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    LawyerFirmID = LaywerID,
                };

                if (chkHead.Checked)
                    user.IsHead = true;
                else
                    user.IsHead = false;

                if (divCustomer.Visible)
                {
                    user.CustomerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }

                if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                {
                    user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                }

                if (ddlLitigationRole.SelectedValue != "" && ddlLitigationRole.SelectedValue != "-1")
                {
                    user.LitigationRoleID = Convert.ToInt32(ddlLitigationRole.SelectedValue);
                }

                List<UserParameterValue> parameters = new List<UserParameterValue>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox) entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField) entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));

                    parameters.Add(new UserParameterValue()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }

                #endregion

                #region Audit User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    FirstName = tbxFirstNameUser.Text.Trim(),
                    LastName = tbxLastNameUser.Text.Trim(),
                    Designation = tbxDesignation.Text.Trim(),
                    Email = tbxEmailUser.Text.Trim(),
                    ContactNumber = tbxContactNoUser.Text.Trim(),
                    Address = tbxAddressUser.Text.Trim(),
                    //RoleID = getproductrisk,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    LawyerFirmID = LaywerID
                };

                if (chkHead.Checked)
                    mstUser.IsHead = true;
                else
                    mstUser.IsHead = false;

                if (divCustomer.Visible)
                {
                    mstUser.CustomerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    mstUser.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }

                if (ddlLitigationRole.SelectedValue != "" && ddlLitigationRole.SelectedValue != "-1")
                {
                    mstUser.LitigationRoleID = Convert.ToInt32(ddlLitigationRole.SelectedValue);
                }

                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox) entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField) entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));

                    parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }
                #endregion

                if (rbCheckType.SelectedValue == "Internal")
                {
                    user.IsExternal = false;
                    mstUser.IsExternal = false;
                }
                else if (rbCheckType.SelectedValue == "External")
                {
                    if (ddlLayerList.SelectedValue != "" && ddlLayerList.SelectedValue != "-1")
                    {
                        user.IsExternal = true;
                        mstUser.IsExternal = true;
                    }
                    else
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "Select Lawyer Firm, if User Type is External";
                        return;
                    }
                }

                bool result = false;
                int resultValue = 0;
                if ((int) ViewState["Mode"] == 0)
                {
                    //Check other User with same email
                    bool emailExists;
                    UserManagement.Exists(user, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        return;
                    }

                    UserManagementRisk.Exists(mstUser, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        return;
                    }

                    string passwordText = Util.CreateRandomPassword(10);

                    user.CreatedBy = AuthenticationHelper.UserID;
                    user.CreatedByText = AuthenticationHelper.User;
                    user.Password = Util.CalculateAESHash(passwordText);
                    user.RoleID = Convert.ToInt32(user.LitigationRoleID);

                    string message = getEmailMessage(user, passwordText);

                    mstUser.CreatedBy = AuthenticationHelper.UserID;
                    mstUser.CreatedByText = AuthenticationHelper.User;
                    mstUser.Password = Util.CalculateAESHash(passwordText);
                    mstUser.RoleID = Convert.ToInt32(mstUser.LitigationRoleID);

                    resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    if (resultValue > 0)
                    {
                        result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (result == false)
                        {
                            UserManagement.deleteUser(resultValue);
                        }
                    }
                }
                else if ((int) ViewState["Mode"] == 1)
                {
                    int userID = -1;
                    userID = Convert.ToInt32(ViewState["UserID"]);

                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                    mstUser.ID = Convert.ToInt32(ViewState["UserID"]);

                    //Check other User with same email
                    bool emailExists;
                    UserManagement.Exists(user, out emailExists);

                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        return;
                    }

                    UserManagementRisk.Exists(mstUser, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        return;
                    }

                    //Get Existing Users AuditorID and Compliance-Audit Product RoleID
                    User complianceUser = UserManagement.GetByID(userID);
                    mst_User auditUser = UserManagementRisk.GetByID(userID);

                    if (complianceUser != null)
                    {
                        user.AuditorID = complianceUser.AuditorID;
                        user.RoleID = complianceUser.RoleID;
                    }

                    if (auditUser != null)
                    {
                        mstUser.AuditorID = auditUser.AuditorID;
                        mstUser.RoleID = auditUser.RoleID;
                    }

                    //result = UserManagement.Update(user, parameters);
                    //result = UserManagementRisk.Update(mstUser, parametersRisk);

                    result = LitigationUserManagement.UpdateUserLitigation(user, parameters);
                    result = LitigationUserManagement.UpdateAuditDBUserLitigation(mstUser, parametersRisk);

                    if (tbxEmail.Text.Trim() != complianceUser.Email && result)
                    {
                        string message = SendNotificationEmailChanged(user);
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    }
                }
                else
                {
                    cvUserPopup.IsValid = false;
                    cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
                }

                if (result)
                {
                    if (UserImageUpload.HasFile)
                    {
                        string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);

                        string filepath = "~/UserPhotos/" + fileName;

                        UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                        UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                    }

                    if (result)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "Lawyer Details Saved Successfully.";
                        vsUserPopup.CssClass = "alert alert-success";
                        upUsersPopup.Update();
                    }
                }

                //ScriptManager.RegisterStartupScript(this.upUsersPopup, this.upUsersPopup.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void Upload(object sender, EventArgs e)
        {
            if (UserImageUpload.HasFile)
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

                string ext = System.IO.Path.GetExtension(UserImageUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    cvUserPopup.IsValid = false;
                    cvUserPopup.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                }
            }
        }

        private void BindReportingTo()
        {
            try
            {
                ddlReportingTo.DataTextField = "Name";
                ddlReportingTo.DataValueField = "ID";

                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlLitigationRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(Convert.ToInt32(ddlCustomerPopup.SelectedValue), roleCode);
                ddlReportingTo.DataBind();

                ddlReportingTo.Items.Insert(0, new ListItem("Select Reporting to person", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;
                var branchs = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(ddlCustomerPopup.SelectedValue));
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }
                tbxBranch.Text = "Select Location";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private string getEmailMessage(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                              .Replace("@Username", user.Email)
                              .Replace("@User", username)
                              .Replace("@PortalURL", Convert.ToString(portalURL))
                              .Replace("@Password", passwordText)
                              .Replace("@From", ReplyEmailAddressName)
                              .Replace("@URL", Convert.ToString(portalURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
                return null;
            }
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                      .Replace("@Username", user.Email)
                                      .Replace("@User", username)
                                      .Replace("@PortalURL", Convert.ToString(portalURL))
                                      .Replace("@From", ReplyEmailAddressName)
                                      .Replace("@URL", Convert.ToString(portalURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return null;
        }

        private void BindDepartment()
        {
            try
            {
                if (ddlCustomerPopup.SelectedValue != "" && ddlCustomerPopup.SelectedValue != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    ddlDepartment.DataTextField = "Name";
                    ddlDepartment.DataValueField = "ID";
                    ddlDepartment.DataSource = CompDeptManagement.FillDepartment(customerID);
                    ddlDepartment.DataBind();
                    ddlDepartment.Items.Insert(0, new ListItem("Select Department", "-1"));
                    ddlDepartment.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindRoles()
        {
            try
            {
                ddlLitigationRole.DataTextField = "Name";
                ddlLitigationRole.DataValueField = "ID";

                var roles = RoleManagement.GetLitigationRoles(true);
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }

                else if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }
                else
                {
                    roles = roles.Where(entry => entry.Code.Equals("EXCT")).ToList();                    
                }

                ddlLitigationRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlLitigationRole.DataBind();
                ddlLitigationRole.Items.Insert(0, new ListItem("Select User Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindCustomers(DropDownList ddlitoFill)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlitoFill.DataTextField = "Name";
                ddlitoFill.DataValueField = "ID";

                ddlitoFill.DataSource = CustomerManagement.GetAll(customerID);
                ddlitoFill.DataBind();

                //ddlitoFill.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BingLawyerList()
        {
            try
            {
                var AllData = LawyerManagement.GetLawyerListAll(CustomerID);
                var users = (from row in AllData
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                ddlLayerList.DataTextField = "Name";
                ddlLayerList.DataValueField = "ID";
                ddlLayerList.DataSource = users;
                ddlLayerList.DataBind();

                ddlLayerList.Items.Insert(0, new ListItem("Select Lawyer", "-1"));
                ddlLayerList.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        {
            try
            {
                if (userParameterValues == null)
                {
                    userParameterValues = new List<UserParameterValueInfo>();
                    var userParameters = UserManagement.GetAllUserParameters();
                    userParameters.ForEach(entry =>
                    {
                        userParameterValues.Add(new UserParameterValueInfo()
                        {
                            UserID = -1,
                            ParameterID = entry.ID,
                            ValueID = -1,
                            Name = entry.Name,
                            DataType = (DataType) entry.DataType,
                            Length = (int) entry.Length,
                            Value = string.Empty
                        });
                    });
                }

                repParameters.DataSource = userParameterValues;
                repParameters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void AddNewUser()
        {
            try
            {
                tbxFirstNameUser.Text = tbxLastNameUser.Text = tbxDesignation.Text = tbxEmailUser.Text = tbxContactNoUser.Text = tbxAddressUser.Text = string.Empty;
                tbxEmailUser.Enabled = true;
                ddlCustomerPopup.Enabled = true;
                ddlDepartment.ClearSelection();

                ddlLitigationRole.ClearSelection();
                ddlLitigationRole.SelectedValue = "-1";
                ddlLitigationRole_SelectedIndexChanged(null, null);

                BindParameters();
                upUsersPopup.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            BindData(); bindPageNumber();
        }

        protected void grdLawyer_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdLawyer_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long CustomerId = AuthenticationHelper.CustomerID;

                var lstLawyer = LawyerManagement.GetLawyerListAllNew(CustomerId);

                if (lstLawyer.Count > 0)
                    lstLawyer = lstLawyer.OrderBy(entry => entry.FirstName).ToList();

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    lstLawyer = lstLawyer.Where(entry => entry.FirstName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.LastName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.Email.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.ContactNumber.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.Specilisation.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstLawyer = lstLawyer.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstLawyer = lstLawyer.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;
                foreach (DataControlField field in grdLawyer.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLawyer.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;
                if (lstLawyer.Count > 0)
                {
                    grdLawyer.DataSource = lstLawyer;
                    Session["TotalRows"] = lstLawyer.Count;
                    grdLawyer.DataBind();
                }
                else
                {
                    grdLawyer.DataSource = lstLawyer;
                    grdLawyer.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void lnkBtnDept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lnkLawFirmBind_Click(object sender, EventArgs e)
        {
            BingLawyerList();
        }

        protected void tbxtypeTofilter_TextChanged(object sender, EventArgs e)
        {
            try
            {     
                grdLawyer.PageIndex = 0; 
                BindData();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}