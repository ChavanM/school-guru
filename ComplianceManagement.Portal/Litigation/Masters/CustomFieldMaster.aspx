﻿<%@ Page Title="Custom Parameter Master" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="CustomFieldMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.CustomFieldMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        div#ContentPlaceHolder1_ddlState_chosen {
            width: 250px !important;
        }

        div#ContentPlaceHolder1_ddlCity_chosen {
            width: 250px !important;
        }

        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
    <script type="text/javascript">
        function fopenpopup() {
            $('#DivAddLCPartyDetails').modal('show');
        }
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters/ Custom Parameter');
        });

        function OpenCustomFieldPopup(lblValue, TypeId, ID) {

            $('#AddCustomeFieldDetailsPop').modal('show');
            $('#ContentPlaceHolder1_IframeCustomFieldDetial').attr('src', "../../Litigation/Masters/AddCustomField.aspx?lblValue=" + lblValue + "&TypeID=" + TypeId + "&ID=" + ID);
        }

        function ClosePopCustomField() {
            $('#AddCustomeFieldDetailsPop').modal('hide');
            //location.reload();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <div style="margin-bottom: 4px; margin-top: 5px;" />
                
                <div class="col-md-3 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlType" AutoPostBack="true" DataPlaceHolder="Select Type" AllowSingleDeselect="false"
                        class="form-control m-bot15" Width="100%" Height="30px">
                    </asp:DropDownListChosen>
                </div>
                <div class="col-md-3 colpadding0" style="margin-left: 5px;">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search" CssClass="form-control" OnTextChanged="tbxtypeTofilter_TextChanged" AutoPostBack="true"/>
                    </div>
                <div style="text-align: right">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" Width="7%" OnClick="lnkBtnApplyFilter_Click" data-toggle="tooltip" ToolTip="Apply"/>
                   <asp:LinkButton  Text=" Add New" CssClass="btn btn-primary" runat="server" ID="btnAdd" OnClick="btnAdd_Click" data-toggle="tooltip" ToolTip="Add New Custom Parameter">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                </div>
                <div style="margin-bottom: 4px">
                    <asp:UpdatePanel ID="UpCustFields" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView runat="server" ID="grdCustomFieldList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true" OnSorting="grdCustomFieldList_Sorting"
                               PageSize="10" OnRowCreated="grdCustomFieldList_RowCreated" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                                 DataKeyNames="ID" OnRowCommand="grdCustomFieldList_RowCommand" OnRowDataBound="grdCustomFieldList_RowDataBound">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Type" ItemStyle-Width="20%" SortExpression="CaseType" >
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseType") %>' ToolTip='<%# Eval("CaseType") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Name" ItemStyle-Width="60%" SortExpression="Lable">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Lable") %>' ToolTip='<%# Eval("Lable") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%">
                                        <ItemTemplate>

                                            <asp:LinkButton ID="lblEdit" runat="server" CommandName="EDIT_Field" ToolTip="Edit Custom Parameter" data-toggle="tooltip"
                                                CommandArgument='<%# Eval("Lable") +","+ Eval("TypeID")+","+ Eval("ID") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit Details" /></asp:LinkButton>
                                            <asp:LinkButton ID="lblDelete" runat="server" CommandName="DELETE_Field" ToolTip="Delete Custom Field" data-toggle="tooltip"
                                                CommandArgument='<%# Eval("ID") +","+ Eval("TypeID") %>' OnClientClick="return confirm('Are you certain you want to delete this Custom Parameter?');"><img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete Details" /></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>&nbsp;- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                    <div class="col-md-2 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                        </div>
                    <div class="col-md-2 colpadding0" style="float: right;">
                        <div style="float: left; width: 50%">
                            <p class="clsPageNo">Page</p>
                        </div>
                        <div style="float: left; width: 50%">
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddCustomeFieldDetailsPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 260px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Add/Edit Custom Parameter</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">x</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeCustomFieldDetial" frameborder="0" runat="server" width="100%" height="300px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
