﻿using AjaxControlToolkit;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddLawFirm : System.Web.UI.Page
    {
        public static List<int> LawyerTypeList = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCourtType();
                ViewState["Mode"] = 0;
            }
        }

        public void BindCourtType()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                var lstlawyerType = LawyerManagement.FillLawyerTypeList(customerID);

                ddlLawyerTypes.DataTextField = "CourtType";
                ddlLawyerTypes.DataValueField = "CourtID";

                ddlLawyerTypes.Items.Clear();

                ddlLawyerTypes.DataSource = lstlawyerType;
                ddlLawyerTypes.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void btnSaveLawFirm_Click(object sender, EventArgs e)
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;

                tbl_Lawyer objlawyer = new tbl_Lawyer()
                {
                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    Address = tbxAddress.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    IsActive = true,
                    CustomerID = Convert.ToInt32(CustomerID),
                    Specilisation = tbxSpecilisation.Text
                };

                if (LawyerManagement.LawyerDatacheckExist(objlawyer.FirstName, objlawyer.LastName, CustomerID))
                {
                    CustomModifyAsignment.IsValid = false;
                    CustomModifyAsignment.ErrorMessage = "Lawyer Name Already Exists";
                }
                else
                {
                    LawyerManagement.CreateLawyerData(objlawyer);
                    LawyerTypeList.Clear();
                    for (int i = 0; i < ddlLawyerTypes.Items.Count; i++)
                    {
                        if (ddlLawyerTypes.Items[i].Selected)
                        {
                            LawyerTypeList.Add(Convert.ToInt32(ddlLawyerTypes.Items[i].Value));
                        }
                    }
                    LawyerCourtMapping objCourt = new LawyerCourtMapping();
                    var LawyerID = LawyerManagement.GetlawyerID(objlawyer);
                    foreach (var aItem in LawyerTypeList)
                    {
                        objCourt.CourtID = aItem;
                        objCourt.LawyerID = LawyerID.ID;
                        objCourt.IsActive = true;
                        LawyerManagement.CreateLawyerMapping(objCourt);
                    }
                    CustomModifyAsignment.IsValid = false;
                    CustomModifyAsignment.ErrorMessage = "Law Firm Details Saved Successfully."; 
                    ValidationSummary1.CssClass = "alert alert-success";
                    tbxFirstName.Text = string.Empty;
                    tbxLastName.Text = string.Empty;
                    tbxEmail.Text = string.Empty;
                    tbxContactNo.Text = string.Empty;
                    tbxAddress.Text = string.Empty;
                    tbxSpecilisation.Text = string.Empty;
                    ddlLawyerTypes.ClearSelection();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "RebindLawFirmMultiSelect", "RebindPopupddllayer();", true);
                upLawyerPop.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
    }
}