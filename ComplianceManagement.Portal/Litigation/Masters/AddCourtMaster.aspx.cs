﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Windows.Forms;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddCourtMaster : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BindCountry();
                    BindCaseTypeDate();

                    if (!string.IsNullOrEmpty(Request.QueryString["CourtID"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["CourtID"]);
                        tbl_CourtMaster Objcourt = LitigationCourtAndCaseType.GetCourtMasterDatabyID(ID, CustomerID);

                        tbxCourtName.Text = Objcourt.CourtName;
                        ddlCourtType.SelectedValue = Convert.ToString(Objcourt.CourtType);
                        ddlCountry.SelectedValue = Convert.ToString(Objcourt.Country);
                        ddlCountry_SelectedIndexChanged(sender, e);
                        ddlStateList.SelectedValue = Convert.ToString(Objcourt.State);
                        ddlStateList_SelectedIndexChanged(sender, e);
                        ddlcity.SelectedValue = Convert.ToString(Objcourt.City);
                        tbxAddress.Text = Objcourt.Address;
                        ViewState["Mode"] = 1;
                        ViewState["CourtID"] = ID;
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        public void BindCaseTypeDate()
        {
            var CTypeList = LitigationCourtAndCaseType.GetAllCourtTypeData(CustomerID);

            ddlCourtType.DataTextField = "CourtType";
            ddlCourtType.DataValueField = "CourtID";

            ddlCourtType.DataSource = CTypeList;
            ddlCourtType.DataBind();
            ddlCourtType.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
            ddlCourtType.Items.Insert(0, new ListItem("Select Court Type", "-1"));
        }
        public void BindCountry()
        {
            var CountryList = CityStateCountryManagement.GetAllContryData();

            ddlCountry.DataTextField = "Name";
            ddlCountry.DataValueField = "ID";

            ddlCountry.DataSource = CountryList;
            ddlCountry.DataBind();
            //  ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));
        }

        public void BindStateCountryWise(int CountryID)
        {
            ddlStateList.DataTextField = "Name";
            ddlStateList.DataValueField = "ID";
            var StateListcountrywise = CityStateCountryManagement.GetAllStateCountryWise(CountryID);
            ddlStateList.DataSource = StateListcountrywise;
            ddlStateList.DataBind();
            // ddlStateList.Items.Insert(0, new ListItem("Select State", "-1"));
        }

        public void BindCityStateWise(int StateID)
        {
            ddlcity.DataTextField = "CityName";
            ddlcity.DataValueField = "CityID";
            var CityList = CityStateCountryManagement.GetAllcity();
            CityList = CityList.Where(Entry => Entry.StateID == Convert.ToInt32(StateID)).Distinct().ToList();
            ddlcity.DataSource = CityList;
            ddlcity.DataBind();
            ddlcity.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
            ddlcity.Items.Insert(0, new ListItem("Select City", "-1"));
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                if (ddlCountry.SelectedValue != "-1")
                {
                    BindStateCountryWise(Convert.ToInt32(ddlCountry.SelectedValue));
                }
            }
        }

        protected void ddlStateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlStateList.SelectedValue))
            {
                if (ddlStateList.SelectedValue != "-1")
                {
                    BindCityStateWise(Convert.ToInt32(ddlStateList.SelectedValue));
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int CourtTypeVal = -1;
                int CountryVal = -1;
                int StateVal = -1;
                int CityVal = -1;
                if (!string.IsNullOrEmpty(ddlCourtType.SelectedValue))
                {
                    CourtTypeVal = Convert.ToInt32(ddlCourtType.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
                {
                    CountryVal = Convert.ToInt32(ddlCountry.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlStateList.SelectedValue))
                {
                    StateVal = Convert.ToInt32(ddlStateList.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlcity.SelectedValue))
                {
                    CityVal = Convert.ToInt32(ddlcity.SelectedValue);
                }

                tbl_CourtMaster objCase = new tbl_CourtMaster()
                {
                    CourtName = tbxCourtName.Text.Trim(),
                    CourtType = CourtTypeVal,
                    Country = CountryVal,
                    State = StateVal,
                    City = CityVal,
                    Address = tbxAddress.Text.Trim(),
                    CustomerID = Convert.ToInt32(CustomerID)
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objCase.ID = Convert.ToInt32(ViewState["CourtID"]);
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (LitigationCourtAndCaseType.CheckCourtExist(objCase))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Court Name already exists.";
                    }
                    else
                    {
                        objCase.CreatedBy = AuthenticationHelper.UserID;
                        objCase.CreatedOn = DateTime.Now;
                        objCase.IsDeleted = false;
                        LitigationCourtAndCaseType.CreateCourtMasterDetails(objCase);
                        var key = "LitiCourt-" + Convert.ToString(AuthenticationHelper.CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Court Detail Save Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";

                        tbxCourtName.Text = string.Empty;
                        ddlCourtType.SelectedIndex = -1;
                        ddlCountry.SelectedIndex = -1;
                        ddlStateList.SelectedIndex = -1;
                        ddlcity.SelectedIndex = -1;
                        tbxAddress.Text = string.Empty;

                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    objCase.UpdatedBy = AuthenticationHelper.UserID;
                    objCase.UpdatedOn = DateTime.Now;
                    LitigationCourtAndCaseType.UpdateCourtMasterDetials(objCase);
                    var key = "LitiCourt-" + Convert.ToString(AuthenticationHelper.CustomerID);
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Remove(key);
                    }
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Court Detail Updated Successfully";
                    ValidationSummary1.CssClass = "alert alert-success";

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }

        protected void lnkRebindCity_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlStateList.SelectedValue))
            {
                if (ddlStateList.SelectedValue != "-1")
                {
                    BindCityStateWise(Convert.ToInt32(ddlStateList.SelectedValue));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCity", "restoreSelectedCity();", true);
                    upPromotor.Update();
                }
            }
        }

        protected void lnkRebindCourtType_Click(object sender, EventArgs e)
        {
            
                   BindCaseTypeDate();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCourt", "restoreSelectedCourt();", true);
            upPromotor.Update();



        }

        protected void lnkBtnCity_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxCity.Text != "")
                {
                    bool success = true;
                    int stateid = -1;
                    if (!string.IsNullOrEmpty(ddlStateList.SelectedValue))
                    {
                        stateid = Convert.ToInt32(ddlStateList.SelectedValue);
                    }
                    int newCityID = 0;
                    City newCity = new City()
                    {
                        Name = tbxCity.Text.Trim(),
                        StateId = stateid,
                        IsDeleted = false,
                    };

                    if (!StateCityManagement.ExitsCity(newCity))
                    {
                        CityValidator.IsValid = false;

                        CityValidator.ErrorMessage = "City with same name already exists.";
                        success = false;
                        return;
                    }

                    if (success)
                    {
                        newCityID = StateCityManagement.CreateCity(newCity);

                        if (newCityID != 0)
                        {
                            CityValidator.IsValid = false;
                            CityValidator.ErrorMessage = "City Save Successfully.";
                            ValidationSummary1.CssClass = "alert alert-success";
                            BindCityStateWise(Convert.ToInt32(ddlStateList.SelectedValue));
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCity", "restoreSelectedCity();", true);
                            upPromotor.Update();

                        }
                    }


                }
                tbxCity.Text = " ";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CityValidator.IsValid = false;
                CityValidator.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

       /*protected void lnkBtnCourtType_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCourtType.Text != "")
                {
                    bool success = true;
                    int stateid = -1;
                    if (!string.IsNullOrEmpty(ddlCourtType.SelectedValue))
                    {
                        stateid = Convert.ToInt32(ddlCourtType.SelectedValue);
                    }
                    //int newCourtTypeID = 0;
                    tbl_CourtType objCourt = new tbl_CourtType()
                    {      
                        CourtType = txtCourtType.Text.Trim(), 
                       Createdby =  AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                       // UpdatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    if (LitigationCourtAndCaseType.CheckCourtTypeExist(objCourt))
                    {
                        CourtTypeValidator.IsValid = false;

                        CourtTypeValidator.ErrorMessage = "Court Type with same name already exists.";
                        success = false;
                        return;
                    }

                    if (success)
                    {
                        LitigationCourtAndCaseType.CreateCourtTypeDetails( objCourt);                        
                            CourtTypeValidator.IsValid = false;
                            CourtTypeValidator.ErrorMessage = "Court Type Save Successfully.";
                       
                    }


                }
                txtCourtType.Text = " ";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CourtTypeValidator.IsValid = false;
                CourtTypeValidator.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }*/



    }

}