﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="AddNoticeStage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddNoticeStage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 400px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .toolbar {
            float: left;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }
            .k-pager-wrap > .k-link > .k-icon {
                margin-top: 6px;
                color: inherit;
            }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }
        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
            background: white;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
            margin-top: 6px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }
        .k-widget > span.k-invalid,
        input.k-invalid {
            border: 1px solid red !important;
        }

        .k-widget > span.k-invalid,
        textarea.k-invalid {
            border: 1px solid red !important;
        }

        .k-widget.k-tooltip-validation {
            display: none !important;
        }
    </style>

    <script id="template" type="text/x-kendo-template">
     <div class="toolbar.create" style="float: right;margin-right: 1px;">
        <a class="k-button k-grid-add">Add Notice Stage</a>
    </div>
    <span class="k-textbox k-grid-search " style="margin-left: 6px;">
        <input id="txtSearch" autocomplete="off" placeholder="Search..." title="Search..." class="k-input">
    </span>
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / Notice Stage');

            Bindgrid();

            $("#txtSearch").on('input', function (e) {

                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };

                columns.forEach(function (x) {
                    if (x.field == "NoticeStage") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);
                grid.dataSource.filter(filter);
                if ($("#txtSearch").val() == "") {
                    $('.k-grid-add').show();
                }
                else {
                    $('.k-grid-add').hide();
                }
            });

        });
        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: '<% =Path%>Litigation/KendoNoticeStage?CustId=<% =CustId%>',
                },
                update: {
                    url: "<% =Path%>Litigation/Edit_NoticeStages",
                },
                create: {
                    url: "<% =Path%>Litigation/Add_NoticeStages?CreatedBy=<%=UId%>&CustomerID=<%=CustId%>",
                },
                destroy: {
                    url: "<% =Path%>Litigation/Delete_NoticeStagesByID"
                }
            },
            pageSize: 10,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        NoticeStage: {
                            validation: {
                                required: true,
                                productnamevalidation: function (input) {
                                    if (input.filter("[required]").length && $.trim(input.val()) == "") {
                                        input.attr("data-productnamevalidation-msg", "Stage Name cannot be empty");
                                        return false;
                                    }
                                    return true;
                                }

                            }
                        },
                    }
                },
            },
            requestEnd: onRequestEnd
        });

        function onRequestEnd(e) {
            if (e.type == "update") {
                if (e.response[0].Message == "True") {
                    alert(e.response[0].MessageType + " - Notice Stage Updated Successfully");
                    e.sender.read();
                }
                else {
                    alert("Error Updating Notice Stage - " + e.response[0].MessageType);
                    e.sender.read();
                }
            }
            if (e.type == "create") {
                if (e.response[0].Message == "True") {
                    alert(e.response[0].MessageType + " - Notice Stage Created Successfully");
                    e.sender.read();
                }
                else {
                    alert(e.response[0].MessageType + " - Notice Stage already exist" );
                    e.sender.read();
                }
            }
            if (e.type == "destroy") {
                if (e.response[0].Message == "False") {
                    alert(e.response[0].MessageType + " is linked with Notice(s).Cannot perform delete operation.");
                    window.location.reload();
                }
            }
        }

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                toolbar: [
                    { template: kendo.template($("#template").html()) }
                ],
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        field: "NoticeStage", title: 'Notice Stage',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "70%"
                    },
                    <%if (FlagIsApp != "EXCT") %> <%{%>
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "edit" },
                            { name: "destroy", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "destroy" }], title: "Action", lock: true, width: "20%;", headerAttributes: {
                                style: "border-right: solid 1px #ceced2;text-align: center;"
                            }
                    }
                     <%}%>
                ],
                editable: "inline"

            });


            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "" && content != "  ") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-delete",
                content: function (e) {
                    return "Delete";
                }
            });
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div id="grid" style="margin:4px;"></div>
    </div>

</asp:Content>
