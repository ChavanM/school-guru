﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddPayment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["PaymentID"]))
                {
                    btnSave.Text = "Update";
                    int PaymentID = Convert.ToInt32(Request.QueryString["PaymentID"]);
                    tbl_PaymentType RPD = LitigationPaymentType.PaymentMasterGetByID(PaymentID);
                    txtFName.Text = RPD.TypeName;
                    ViewState["Mode"] = 1;
                    ViewState["PaymentID"] = PaymentID;
                }
                else
                {
                    btnSave.Text = "Save";
                    ViewState["Mode"] = 0;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_PaymentType Paymentmaster = new tbl_PaymentType()
                {
                    TypeName = txtFName.Text.Trim(),
                    IsDeleted = false,
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    Paymentmaster.ID = Convert.ToInt32(ViewState["PaymentID"]);
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (LitigationPaymentType.PaymentExists(Paymentmaster))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Payment Type Already Exists";
                    }
                    else
                    {
                        LitigationPaymentType.CreatePaymentMaster(Paymentmaster);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Payment Type Saved Successfully";
                        ValidationSummary1.CssClass = "alert alert-success";
                        txtFName.Text = string.Empty;
                    }
                }
                else if ((int) ViewState["Mode"] == 1)
                {
                    if (LitigationPaymentType.PaymentExists(Paymentmaster))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Payment Type Already Exists";
                    }
                    else
                    {
                        LitigationPaymentType.UpdatePaymentMaster(Paymentmaster);

                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Payment Type Updated Successfully";
                        ValidationSummary1.CssClass = "alert alert-success";

                    }                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDeptPopup.IsValid = false;
                cvDeptPopup.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}