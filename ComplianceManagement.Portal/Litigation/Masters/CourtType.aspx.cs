﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class CourtType : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";

                BindCourtType();
                bindPageNumber();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void BindCourtType()
        {
            try
            {
                var courtTypeList = LitigationCourtAndCaseType.GetAllCourtTypeData(CustomerID);

                if (courtTypeList.Count > 0)
                    courtTypeList = courtTypeList.OrderBy(entry => entry.CourtType).ToList();

                Session["TotalRows"] = null;
                grdCourtType.DataSource = courtTypeList;
                Session["TotalRows"] = courtTypeList.Count;
                grdCourtType.DataBind();

                //upPromotorList.Update();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCourtType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCourtType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCourtType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                string ID = null;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenAddCourtPopUp('" + ID + "');", true);
                ViewState["Mode"] = 0;
                tbxCourtType.Text = string.Empty;
                //upPromotor.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCourtType.PageIndex = chkSelectedPage - 1;
            grdCourtType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindCourtType();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                tbl_CourtType objCourt = new tbl_CourtType()
                {
                    CourtType = tbxCourtType.Text,
                    CustomerID=(int)CustomerID
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objCourt.CourtID = Convert.ToInt32(ViewState["CourtID"]);
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (LitigationCourtAndCaseType.CheckCourtTypeExist(objCourt))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Court Type Already Exists";
                    }
                    else
                    {
                        objCourt.Createdby = AuthenticationHelper.UserID;
                        objCourt.CreatedOn = DateTime.Now;
                        objCourt.IsDeleted = false;
                        LitigationCourtAndCaseType.CreateCourtTypeDetails(objCourt);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Court Type Saved Successfully";
                        tbxCourtType.Text = string.Empty;
                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    objCourt.Updatedby = AuthenticationHelper.UserID;
                    objCourt.UpdatedOn = DateTime.Now;
                    LitigationCourtAndCaseType.UpdateCourtTypeDetails(objCourt);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Court Type Updated Successfully";
                }
                
                BindCourtType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCourtType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCourtType_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdCourtType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = 0;

                if (e.CommandName.Equals("EDIT_CourtType"))
                {
                    ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CourtID"] = ID;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myModalCourtType", "OpenAddCourtPopUp('" + ID + "');", true);
                    //tbl_CourtType objCourt = LitigationCourtAndCaseType.GetCourtTypeDetailByID(ID, CustomerID);
                    //tbxCourtType.Text = objCourt.CourtType;
                    //upPromotor.Update();
                }
                else if (e.CommandName.Equals("DELETE_CourtType"))
                {
                    ID = Convert.ToInt32(e.CommandArgument);
                    LitigationCourtAndCaseType.DeleteCourtTypeDetail(ID, CustomerID);
                    BindCourtType();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}