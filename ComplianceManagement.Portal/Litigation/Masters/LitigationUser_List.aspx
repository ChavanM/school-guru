﻿<%@ Page Title="User Master" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="LitigationUser_List.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.LitigationUser_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").unbind('click');
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        });

        function txtclick() {
            $("#divBranches").toggle("blind", null, 500, function () { });
        }

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / User');
        });

        function openModalControl() {
            $('#divUserDialog').modal('show');
            return true;
        }
        function OpenDepartmentPopup(a) {
            $('#AddDepartmentPopUp').modal('show');
            $('#ContentPlaceHolder1_IframeDepartment').attr('src', "../../Litigation/Masters/AddDepartMent.aspx?DepartmentID=" + a);
        }

        function OpenLawFirmPopupModel() {
            $('#AddLawFirmModelPopup').modal('show');
            $('#ContentPlaceHolder1_IFLawFirm').attr('src', "/Litigation/Masters/AddLawFirm.aspx");
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div style="margin: 5px">

                    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none"
                                    class="alert alert-block alert-danger fade in" ValidationGroup="UserPageValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="UserPageValidationGroup" Display="None" />
                            </div>

                            <div class="col-md-12 colpadding0">
                                    
                                <div class="col-md-3 colpadding0 entrycount" style="width: 20%">
                                    <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50"
                                        PlaceHolder="Type to Search" OnTextChanged="tbxFilter_TextChanged1" AutoPostBack="true" /><%--OnTextChanged="tbxFilter_TextChanged" --%>
                                </div>
                                     
                                <div class="col-md-2 colpadding0 entrycount" style="margin-left: 1%; width: 15%">
                                    <asp:DropDownListChosen runat="server" ID="ddlUserType" class="form-control m-bot15" DataPlaceHolder="User Type"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPage_SelectedIndexChanged" Width="90%"
                                        AllowSingleDeselect="false" DisableSearchThreshold="1">
                                        <asp:ListItem Value="0" Text="Internal"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Lawyer"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="External"></asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div>


                                <div class="col-md-3 colpadding0 entrycount" style="width: 20%">
                                    <asp:DropDownListChosen runat="server" ID="ddlLawyerListPage" class="form-control m-bot15" Width="90%"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPage_SelectedIndexChanged" AllowSingleDeselect="false" DisableSearchThreshold="1">
                                    </asp:DropDownListChosen>
                                </div>
                                 <div class="col-md-2 colpadding0 entrycount" style="width: 13%;float: left">
                                      <asp:LinkButton Text="Apply" CssClass="btn btn-primary" OnClick="lnkApply_Click"
                                        runat="server" ID="LinkButton1" data-toggle="tooltip" ToolTip="Apply"></asp:LinkButton>
                                     </div>
                                <div class="col-md-3 colpadding0 entrycount" style="width: 31%">
                                    <asp:DropDownList runat="server" ID="ddlCustomerPage" class="form-control m-bot15" Width="90%"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPage_SelectedIndexChanged" Visible="false" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Select Customer."
                                        ControlToValidate="ddlCustomerPage" runat="server" ValidationGroup="UserPageValidationGroup"
                                        Display="None" />
                                </div>
                                <div class="col-md-2 colpadding0 entrycount" style="width: 5%;margin-left: 180px;">
                                    <asp:LinkButton Text="Back" CssClass="btn btn-primary" OnClick="lblBack_Click"
                                        Visible="false" runat="server" ID="lblBack" />
                                </div>
                               
                                
                                <div class="col-md-2 colpadding0 entrycount" style="width: 5%;float: right;margin-right: 20px;">
                                   
                                    <asp:LinkButton CssClass="btn btn-primary" OnClientClick="openModalControl();"
                                        runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" data-toggle="tooltip" ToolTip="Add New User">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                </div>
                                  
                            </div>

                            <div class="clearfix"></div>

                            <div class="panel col-md-12 colpadding0" style="margin-bottom: 4px;">
                                <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" AllowPaging="true" AutoPostBack="true" AllowSorting="true"
                                    CssClass="table" GridLines="none" PageSize="10" OnSorting="grdUser_Sorting" Width="100%" ShowHeaderWhenEmpty="true"
                                    DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnRowCreated="grdUser_RowCreated" OnRowDataBound="grdUser_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-Width="15%"
                                            ItemStyle-Width="15%" SortExpression="FirstName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />


                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false"
                                            ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" SortExpression="LastName" />

                                        <asp:TemplateField HeaderText="Email" HeaderStyle-Width="20%" ItemStyle-Width="20%" SortExpression="Email">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Email") %>' ToolTip='<%# Eval("Email") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="ContactNumber" SortExpression="ContactNumber" HeaderText="Contact" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                        <%--SortExpression="ContactNumber" --%>

                                        <asp:BoundField DataField="Role" SortExpression="Role" HeaderText="Litigation Role" HeaderStyle-Width="15%" ItemStyle-Width="15%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                        <%--SortExpression="Role"--%>

                                        <asp:TemplateField HeaderText="Rating" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval ("Rating")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ID") %>' ID="lbtnChangeStatus"
                                                    ToolTip="Click to toggle the status..." data-toggle="tooltip" data-placement="bottom" OnClientClick="return confirm('Are you certain you want to change the status of this User?');"><%# (Convert.ToBoolean(Eval("IsActive"))) ? "Active" : "Disabled" %></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="8%" ItemStyle-Width="8%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                    <asp:LinkButton runat="server" ToolTip="Edit user" data-toggle="tooltip" data-placement="bottom" CommandName="EDIT_USER" OnClientClick="openModalControl()" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit User" /></asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="DELETE_USER" ToolTip="Delete user" data-toggle="tooltip" data-placement="bottom" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                                        OnClientClick="return confirm('Are you certain you want to delete this User?');">
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete User"/></asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="RESET_PASSWORD" ID="lbtnReset" CommandArgument='<%# Eval("ID") %>'
                                                        OnClientClick="return confirm('Are you certain you want to reset password for this user?');" ToolTip="Change user password" data-toggle="tooltip" data-placement="bottom">
                                                        <img src='<%# ResolveUrl("~/Images/reset_password_new.png")%>' alt="Reset Password"/></asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="UNLOCK_USER" ToolTip="Unlock user" data-toggle="tooltip" data-placement="bottom" CommandArgument='<%# Eval("ID") %>'
                                                        Visible='<%# IsLocked((string)Eval("Email")) %>'>
                                                        <img src='<%# ResolveUrl("~/Images/permissions_icon.png")%>' alt="Unblock User"/></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-8 colpadding0">
                                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>&nbsp;- 
                                    <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                    <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                  <div class="col-md-2 colpadding0" style="margin-right: -94px;" >
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 38%; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5"/>
                                        <asp:ListItem Text="10" Selected="True" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>
                                              <div class="col-md-2 colpadding0" style="float: right;">
                                    <div style="float: left; width: 50%">
                                        <p class="clsPageNo">Page</p>
                                    </div>
                                    <div style="float: left; width: 50%">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>

    <%-- Modal Pop-Up  --%>
    <div class="modal fade" id="divUserDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left: 14%;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content" style="width: 85%; height: auto;">
                <div class="modal-header">
                    &nbsp;
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add New User</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <%--<vit:audituserdetailscontrol runat="server" id="udcInputForm" />--%>
                    <asp:UpdatePanel ID="upUsersPopup" runat="server" UpdateMode="Conditional" OnLoad="upUsersPopup_Load">
                        <ContentTemplate>
                            <div>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="vsUserPopup" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="UserPopupValidationGroup" />
                                    <asp:CustomValidator ID="cvUserPopup" runat="server" EnableClientScript="False"
                                        ErrorMessage="Email already exists." ValidationGroup="UserPopupValidationGroup" Display="None" />
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            First Name</label>
                                        <asp:TextBox runat="server" ID="tbxFirstName" Style="width: 70%;" CssClass="form-control" MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="First Name can not be empty."
                                            ControlToValidate="tbxFirstName" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid first name."
                                            ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Last Name</label>
                                        <asp:TextBox runat="server" ID="tbxLastName" Style="width: 70%;" CssClass="form-control"
                                            MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Last Name can not be empty."
                                            ControlToValidate="tbxLastName" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid last name."
                                            ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Designation</label>
                                        <asp:TextBox runat="server" ID="tbxDesignation" Style="width: 70%;" CssClass="form-control"
                                            MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Designation can not be empty."
                                            ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid designation."
                                            ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Email</label>
                                        <asp:TextBox runat="server" ID="tbxEmail" Style="width: 70%;" MaxLength="200" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Email can not be empty."
                                            ControlToValidate="tbxEmail" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid email."
                                            ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Contact No</label>
                                        <asp:TextBox runat="server" ID="tbxContactNo" Style="width: 70%;" CssClass="form-control"
                                            MaxLength="10" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Contact Number can not be empty."
                                            ControlToValidate="tbxContactNo" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid contact number."
                                            ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter only 10 digit contact number."
                                            ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divddlLayerList">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Law Firm
                                            </label>
                                            <asp:DropDownList runat="server" ID="ddlLayerList" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15"
                                            onchange="ShowLawFirmAddbutton()"/>
                                            <img id="lnkShowAddNewLawFirmModal" style="float: right; display: none; margin-top: -30px" src="../../Images/add_icon_new.png"
                                                onclick="OpenLawFirmPopupModel();" alt="Add New Lawyer" title="Add New Lawyer" />
                                        </div>
                                        <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                            <asp:LinkButton ID="lnkLawFirmBind" OnClick="lnkLawFirmBind_Click" Style="float: right; display: none;" runat="server"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-top: -4px;">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Department</label>
                                        <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15" onchange="ddlDepartmentChange()"/><br />
                                        <img id="lnkAddNewDepartmentModal" style="float: right; display: none; margin-top: -50px" src="../../Images/add_icon_new.png" onclick="OpenDepartmentPopup('')" alt="Add New Department" title="Add New Department" />
                                        <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                            <asp:LinkButton ID="lnkBtnDept" OnClick="lnkBtnDept_Click" Style="float: right; display: none;" runat="server"></asp:LinkButton>
                                        </div>
                                        <asp:CompareValidator ID="CVDept" ErrorMessage="Please select department." ControlToValidate="ddlDepartment"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup" Display="None" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Address</label>
                                        <asp:TextBox runat="server" ID="tbxAddress" Style="width: 70%;" MaxLength="500"
                                            TextMode="MultiLine" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-top: -24px;">
                                        <div runat="server" id="divLitigationRole">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Role (Litigation)</label>
                                            <asp:DropDownList runat="server" ID="ddlLitigationRole" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlLitigationRole_SelectedIndexChanged" />
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select User Role." ControlToValidate="ddlLitigationRole"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6" style="margin-top: -12px;">

                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Specialization</label>
                                        <asp:TextBox runat="server" ID="tbxSpecilisation" CssClass="form-control" Style="width: 70%;" MaxLength="500" />

                                    </div>
                                </div>


                                <div id="div1" style="margin-bottom: 7px; display: none;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        User Type</label>
                                    <asp:RadioButtonList ID="rbCheckType" runat="server" RepeatDirection="Horizontal"
                                        Style="padding: 0px; margin: 0px; width: 390px;">
                                        <%--OnSelectedIndexChanged="rbCheckType_SelectedIndexChanged" AutoPostBack="true"--%>
                                        <asp:ListItem class="radio-inline" Text="Internal" Value="Internal" Selected="True"></asp:ListItem>
                                        <asp:ListItem class="radio-inline" Text="External" Value="External"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>

                                <div style="display: none;">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Department Head</label>
                                    <asp:CheckBox runat="server" ID="chkHead" Style="padding: 0px; margin: 0px; height: 30px; width: 70%; color: #333;" />
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-top: -10px;">

                                        <div runat="server" id="divCustomer">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Customer</label>
                                            <asp:DropDownList runat="server" ID="ddlCustomerPopup" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPopup_SelectedIndexChanged" />
                                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please Select Customer."
                                                ControlToValidate="ddlCustomerPopup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="UserPopupValidationGroup" Display="None" />
                                        </div>



                                    </div>
                                    <div class="form-group col-md-6">

                                        <div runat="server" id="divReportingTo">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Reporting to</label>
                                            <asp:DropDownList runat="server" ID="ddlReportingTo" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control m-bot15" />
                                        </div>

                                    </div>
                                </div>



                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-top: -9px;">
                                        <div runat="server" id="divCustomerBranch">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Location</label>
                                            <asp:TextBox runat="server" ID="tbxBranch" onclick="txtclick()" Style="padding: 5px; margin: 0px; width: 70%;" CausesValidation="true"
                                                CssClass="form-control" />
                                            <div style="margin-left: 17%; position: absolute; z-index: 10" id="divBranches">
                                                <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="100px" Width="100%"
                                                    Style="overflow: auto; margin-left: 11%;" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                                </asp:TreeView>
                                            </div>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please select Location."
                                                ControlToValidate="tbxBranch" runat="server" ValidationGroup="UserPopupValidationGroup" InitialValue="Select Location"
                                                Display="None" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Profile Picture</label>
                                        <asp:FileUpload ID="UserImageUpload" runat="server" ForeColor="#333" />
                                        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="Upload" Visible="false" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            &nbsp;
                                        </label>
                                        <asp:Image ID="ImageShow" runat="server" Height="100" Width="100" ImageUrl="~/UserPhotos/DefaultImage.png" Visible="false" />
                                    </div>
                                </div>

                                <asp:Repeater runat="server" ID="repParameters">
                                    <ItemTemplate>
                                        <div style="margin-bottom: 7px">
                                            <asp:Label runat="server" ID="lblName" Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;"
                                                Text='<%# Eval("Name")  + ":"%>' />
                                            <asp:TextBox runat="server" ID="tbxValue" Style="height: 20px; width: 390px;" Text='<%# Eval("Value") %>'
                                                MaxLength='<%# Eval ("Length") %>' />
                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ValueID") %>' />
                                            <asp:HiddenField runat="server" ID="hdnEntityParameterID" Value='<%# Eval("ParameterID") %>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <div class="clearfix"></div>

                                <div style="margin-bottom: 7px; margin-top: 10px; text-align: center;">
                                    <%--float: right; margin-right: 257px; --%>;
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                        ValidationGroup="UserPopupValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>

                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 25px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                                <div class="clearfix" style="height: 50px"></div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                            <%--<asp:AsyncPostBackTrigger ControlID="btnUpload" EventName="Click" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Department Popup--%>
    <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Add New Department</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDepartment" frameborder="0" runat="server" width="100%" height="150px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddLawFirmModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p0" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom" id="Lawfirmmodel">
                        Add New Law Firm</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IFLawFirm" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function ClosePopDepartment() {
            $('#AddDepartmentPopUp').modal('hide');
            document.getElementById('<%= lnkBtnDept.ClientID %>').click();
        }

        function CloseLawFirmModal() {
            $('#AddLawFirmModelPopup').modal('hide');
            document.getElementById('<%= lnkLawFirmBind.ClientID %>').click();
        }

        function ddlDepartmentChange() {
            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == 0) {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }

        function ShowLawFirmAddbutton() {
            var selectedPartyID = $("#<%=ddlLayerList.ClientID %>").val();
            if (selectedPartyID != null) {
                if (selectedPartyID == "0") {
                    $("#lnkShowAddNewLawFirmModal").show();
                }
                else {
                    $("#lnkShowAddNewLawFirmModal").hide();
                }
            }
        }
    </script>
</asp:Content>
