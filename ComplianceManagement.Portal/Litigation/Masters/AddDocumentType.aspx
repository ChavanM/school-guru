﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddDocumentType.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddDocumentType" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <script type="text/javascript">
        function CloseMe() {
            window.parent.CloseDocTypePopup();
        }
        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }
        $(function () {
            $("#tbxDocumentType").keypress(function (e) {
                var keyCode = e.keyCode || e.which;

                $("#lblError").html("");

                //Regex for Valid Characters i.e. Alphabets.
                var regex = /^[a-zA-Z\s-, ]+$/;

                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only alphabets allowed.");
                }

                return isValid;
            });
        });


    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ContractAddDocType" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="updocumenttype" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div class="row form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div class="row form-group">
                            <div class="form-group required col-md-4">
                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 120px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Document Type</label>
                                <asp:TextBox runat="server" ID="tbxDocumentType" CssClass="form-control" Style="width: 300px;" MaxLength="100" />    
                                  <span id="lblError" style="color: red;margin-left: 0px;"></span>                      
                                <asp:RequiredFieldValidator ID="rfvDocumentType" ErrorMessage="Required Document Type Name" ControlToValidate="tbxDocumentType"
                                    runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                            </div>
                        </div>

                        <div class="row form-group text-center">
                            <div class="col-md-12">
                                <asp:Button  runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                ValidationGroup="PromotorValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" 
                                    OnClientClick="CloseMe();" />
                            </div>
                        </div>
                       
                       <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>                     
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>