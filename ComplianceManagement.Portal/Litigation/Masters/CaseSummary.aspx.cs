﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class CaseSummary : System.Web.UI.Page
    {
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 12;
        protected bool flag;
        private long CustomerID = AuthenticationHelper.CustomerID;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>)Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            if (!IsPostBack)
            {
                BindCaseSummary();
                bindPageNumber();
                hideAddControls();
            }
        }
        public void hideAddControls()
        {
            try
            {
                //var Records = (List<tbl_PageAuthorizationMaster>)Cache.Get("Page_authorizeddata");
                if (authpage != null)
                {
                    if (authpage.Addval == false)
                    {
                        btnAddSpecimen.Visible = false;
                    }
                    else
                    {
                        btnAddSpecimen.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCaseSummary()
        {
            try
            {
                string FilterSummary = string.Empty;
                List<SP_CaseSummary_Result> lstCaseSpeciman = new List<SP_CaseSummary_Result>();
                if(tbxtypeTofilter.Text!="" && tbxtypeTofilter!=null)
                {
                    FilterSummary = tbxtypeTofilter.Text.Trim();
                }
                lstCaseSpeciman = LitigationManagement.GetCaseSpecimanDetails(Convert.ToInt32(AuthenticationHelper.CustomerID), FilterSummary);

                if (lstCaseSpeciman != null && lstCaseSpeciman.Count > 0)
                {
                    grd_Summary.DataSource = lstCaseSpeciman;
                    grd_Summary.DataBind();
                    Session["TotalRows"] = lstCaseSpeciman.Count;
                }
                else
                {
                    grd_Summary.DataSource = new string[] { };
                    grd_Summary.DataBind(); //Bind that empty source      
                    Session["TotalRows"] = lstCaseSpeciman.Count;

                }
            }
            catch (Exception ex)
            {

            }

        }

        protected void grd_Summary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;

                if (e.CommandName.Equals("EDIT_CaseSummary"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CaseSummaryID"] = ID;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCaseSummary('" + ID + "');", true);
                }
                else if (e.CommandName.Equals("DELETE_CaseSummary"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CaseSummaryID"] = ID;
                    bool CheckRef = LitigationManagement.CheckCaseSummarydtls(ID, CustomerID);
                    if (CheckRef)
                    {
                        bool _objdeletCaseSummary = LitigationManagement.DeleteCaseSummaryDetailByID(ID, CustomerID);
                        if (_objdeletCaseSummary == true)
                        {
                            BindCaseSummary();
                            bindPageNumber();
                            cvCasePayment.IsValid = false;
                            cvCasePayment.ErrorMessage = "Case Summary Deleted Succesfully";
                        }

                    }
                    else
                    {
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('Case Stage can not be Delete. One or more cases are assigned to this case stage');", true);
                        cvCasePayment.IsValid = false;
                        cvCasePayment.ErrorMessage = "Case Summary Details Not Found";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grd_Summary.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCaseSummary();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grd_Summary.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grd_Summary.PageIndex = chkSelectedPage - 1;
            grd_Summary.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindCaseSummary(); ShowGridDetail();
            //bindPageNumber();
        }

        protected void grd_Summary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlParticulars = (DropDownList)e.Row.FindControl("ddlParticulars");

                if (ddlParticulars != null)
                    BindParticulars(ddlParticulars);
            }
           
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (authpage != null)
                    {
                        if (authpage.Deleteval == false)
                        {
                            LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("LinkButton2");
                            LinkButton2.Visible = false;
                        }
                        else
                        {
                            LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("LinkButton2");
                            LinkButton2.Visible = true;
                        }
                        if (authpage.Modify == false)
                        {
                            LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("LinkButton1");
                            LinkButton1.Visible = false;
                        }
                        else
                        {
                            LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("LinkButton1");
                            LinkButton1.Visible = true;
                        }
                        if (authpage.Modify == false && authpage.Deleteval == false)
                        {
                        grd_Summary.Columns[11].Visible = false;
                        }
                    }
                }
           
        }

        private void BindParticulars(DropDownList ddlParticulars)
        {
            var ParticularsList = LitigationPaymentType.getParticularLists(Convert.ToInt32(AuthenticationHelper.CustomerID));
            ddlParticulars.DataValueField = "ID";
            ddlParticulars.DataTextField = "Name";

            ddlParticulars.DataSource = ParticularsList;
            ddlParticulars.DataBind();
        }



        protected void btnAddSummary_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCaseSummary('" + ID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void grd_Summary_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var TypeList = LitigationManagement.GetCaseSpecimanDetails(Convert.ToInt32(AuthenticationHelper.CustomerID),null);
                ////if (!string.IsNullOrEmpty(tbxtypeTofilter.Text.Trim()))
                ////{
                ////    TypeList = TypeList.Where(entry => entry.TypeName.ToLower().Contains(((tbxtypeTofilter.Text.Trim()).ToLower()))).ToList();
                ////}

                List<object> dataSource = new List<object>();
                foreach (var casestageinfo in TypeList)
                {
                    dataSource.Add(new
                    {
                        casestageinfo.Perticulars,
                        casestageinfo.StateName,
                        casestageinfo.DisputedAmt,
                        casestageinfo.AmountPaid,
                        casestageinfo.Expense,
                        casestageinfo.Favourable,
                        casestageinfo.Unfavourable,
                        casestageinfo.Total,
                        casestageinfo.Interest,
                        casestageinfo.Penalty,
                        casestageinfo.Id
                    });
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;
                foreach (DataControlField field in grd_Summary.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grd_Summary.Columns.IndexOf(field);
                    }
                }
                ////flag = true;

                if (dataSource.Count > 0)
                {
                    Session["TotalRows"] = dataSource.Count;
                    grd_Summary.DataSource = dataSource;
                    grd_Summary.DataBind();
                }
                else
                {
                    grd_Summary.DataSource = dataSource;
                    grd_Summary.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                ////if (flag == true)
                ////{
                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../../Images/down_arrow1.png";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../../Images/up_arrow1.png";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
        }

        protected void lbtsearchSummary_Click(object sender, EventArgs e)
        {
            try
            {
                BindSummaryDtls();
                bindPageNumber();
            }
            catch(Exception ex)
            {

            }
        }

        private void BindSummaryDtls()
        {
            BindCaseSummary();
        }

        protected void grd_Summary_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
    }
}
