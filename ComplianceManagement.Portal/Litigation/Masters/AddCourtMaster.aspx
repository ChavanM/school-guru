﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCourtMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddCourtMaster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <script type="text/javascript">


        function ddlCityChange() {
            debugger;
            var selectedCityID = $('#ddlcity').find("option:selected").val();
            if (selectedCityID != null) {
                if (selectedCityID == "0") {
                    $("#lnkShowAddNewCityModal").show();
                }
                else {
                    $("#lnkShowAddNewCityModal").hide();
                }
            }
        }

        function ddlCourtTypeChange() {
            debugger;
            var selectedCityID = $('#ddlCourtType').find("option:selected").val();
            if (selectedCityID != null) {
                if (selectedCityID == "0") {
                    $("#lnkShowAddNewCourtTypeModal").show();
                }
                else {
                    $("lnkShowAddNewCourtTypeModal").hide();
                }

            }
        }
        function OpenAddCityPopup() {
            $('#AddCityPopUp').modal('show');
            // $('#IframeCity').attr('src', "../../ContractProduct/Masters/VendorMaster.aspx");
        }

        function OpenAddCourtTypePopup() {
            $('#AddCourtTypePopUp').modal('show');
            // $('#IframeCity').attr('src', "../../ContractProduct/Masters/VendorMaster.aspx");
            $('#IframeAddCourtType').attr('src', "../../Litigation/Masters/AddCourt.aspx?/CourtID" );
        }

        function CloseMe() {
               window.parent.CloseCourtMasterPopUp();
        }
        $(function () {
            $("#tbxCourtName").keypress(function (e) {
                var keyCode = e.keyCode || e.which;

                $("#lblError").html("");

                //Regex for Valid Characters i.e. Alphabets.
                // var regex = /^[a-zA-Z\s-, ]+$/;
                var regex = /^[a-zA-Z0-9,. - : ]*$/

                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only alphabets allowed.");
                }

                return isValid;
            });
        });
        function CloseCourtTypePopUp() {
            $('#AddCourtTypePopUp').modal('hide');

            var ddlCourtType = $('#<%=ddlCourtType.ClientID %>');
           if (ddlCourtType != null || ddlContractType != undefined) {
                ddlCourtType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlCourttType.trigger("chosen:updated");
                ddlCourttType.trigger("liszt:updated");
           }
         

           document.getElementById('<%= lnkRebindCourtType.ClientID %>').click();
            restoreSelectedCourt();

            // $.ajax({
            //     type: "POST",
            //     url: "/ContractProduct//Masters/AddType.aspx/getTypeID",
            //     data: '{}',
            //     contentType: "application/json; charset=utf-8",
            //     dataType: "json",
            //     success: function (response) {
            //         if (response != '') {
            //             var typeID = response;
            //             bindContractTypes(typeID);
            //         }
            //     },
            //     error:
            //function (XMLHttpRequest, textStatus, errorThrown) { },
            // });
        }
        
        function RefreshParent() {
            var checkPath = window.parent.location.href.toLowerCase();
            if (checkPath.toLowerCase().indexOf("casedetailpage.aspx") >= 0) {
                if (window.parent.location.href.toLowerCase().indexOf('casedetailpage.aspx') == -1)
                    window.parent.location.href = window.parent.location.href;
            }
            if (checkPath.toLowerCase().indexOf("noticedetailpage.aspx") >= 0) {
                if (window.parent.location.href.toLowerCase().indexOf('noticedetailpage.aspx') == -1)
                    window.parent.location.href = window.parent.location.href;
            }
        }

        //$(function () {
        //    $("#tbxCourtName").keypress(function (e) {
        //        var keyCode = e.keyCode || e.which;

        //        $("#lblError").html("");

        //        //Regex for Valid Characters i.e. Alphabets.
        //        //var regex = /^[A-Za-z]+$/;
        //        var regex = /^[a-zA-Z0-9,. - : ]*$/

        //        //Validate TextBox value against the Regex.
        //        var isValid = regex.test(String.fromCharCode(keyCode));
        //        if (!isValid) {
        //            $("#lblError").html("");
        //        }

        //        return isValid;
        //    });
        //});
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:HiddenField ID="hdnSelectedCity" runat="server" />
            <asp:HiddenField ID="hdnSelectedCourt" runat="server" />
            <asp:ScriptManager ID="LitigationCourtMaster" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div style="margin-bottom: 7px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Court Name</label>
                            <asp:TextBox runat="server" ID="tbxCourtName" CssClass="form-control" Style="width: 65%;" MaxLength="100" />
                            <span id="lblError" style="color: red;margin-left: 132px;"></span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Court Name can not be empty." ControlToValidate="tbxCourtName"
                                runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Court Type</label>
                            <asp:DropDownListChosen runat="server" ID="ddlCourtType" class="form-control m-bot15" Width="65%" Height="32px" onchange="ddlCourtTypeChange()"
                                 Style="background: none;" DataPlaceHolder="Court Type" AllowSingleDeselect="false" DisableSearchThreshold="5">
                            </asp:DropDownListChosen>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Court Type can not be empty." ControlToValidate="ddlCourtType"
                                runat="server" ValidationGroup="ddlCourtType" Display="None" />
                            <div style="float: right; text-align: center; width: 5%; margin-top: 1%;">
                                    <img id="lnkShowAddNewCourtTypeModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddCourtTypePopup()" alt="Add New Court Type" title="Add New Court Type" />
                                <asp:LinkButton runat="server" ID="lnkRebindCourtType" style="display:none" OnClick="lnkRebindCourtType_Click"></asp:LinkButton>
                                </div>

                         </div>
                        <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Country</label>
                            <asp:DropDownListChosen runat="server" ID="ddlCountry" class="form-control m-bot15" Width="65%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                AutoPostBack="true" Style="background: none;" DataPlaceHolder="Country" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Country can not be empty." ControlToValidate="ddlCountry"
                                runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                State</label>
                            <asp:DropDownListChosen runat="server" ID="ddlStateList" class="form-control m-bot15" Width="65%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                AutoPostBack="true" Style="background: none;" DataPlaceHolder="State" OnSelectedIndexChanged="ddlStateList_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="State can not be empty." ControlToValidate="ddlStateList"
                                runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />--%>
                        </div>
                        <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                City</label>
                            <asp:DropDownListChosen runat="server" ID="ddlcity" class="form-control m-bot15" Width="65%" Height="32px" onchange="ddlCityChange()"
                                 Style="background: none;" DataPlaceHolder="City" AllowSingleDeselect="false" DisableSearchThreshold="5"/>
                               
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select City or Select 'Not Applicable'"
                                        ControlToValidate="ddlcity" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                        Display="None" />
                           
                        <div style="float: right; text-align: center; width: 5%; margin-top: 1%;">
                                    <img id="lnkShowAddNewCityModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddCityPopup()" alt="Add New Opponent" title="Add New Opponent" />

                                </div>
                        </div>
                        <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Address</label>
                            <asp:TextBox runat="server" ID="tbxAddress" CssClass="form-control" Style="width: 65%;" TextMode="MultiLine" MaxLength="100" />
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Address can not be empty." ControlToValidate="tbxAddress"
                                runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />--%>
                        </div>
                        <div style="text-align: center; margin-bottom: 7px; text-align: center; margin-top: 10px">
                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                ValidationGroup="PromotorValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();RefreshParent();" />
                        </div>
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                        </div>
                        <div class="clearfix" style="height: 50px">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

       <div class="modal fade" id="AddCityPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 90%; height: 40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom">
                            Add New City</label>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="cityValidationSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="CityAddPopupValidationGroup" />
                                    <asp:CustomValidator ID="CityValidator" runat="server" EnableClientScript="False"
                                        ValidationGroup="CityAddPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>
                                <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                                    <label style="width: 3%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                    <label style="width: 27%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                        City</label>
                                    <asp:TextBox runat="server" ID="tbxCity" CssClass="form-control" Style="width: 68%;" MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="City can not be empty." ControlToValidate="tbxCity"
                                        runat="server" ValidationGroup="CityAddPopupValidationGroup" Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                    <div style="margin-bottom: 7px; text-align: center">

                                        <asp:LinkButton runat="server" ID="lnkRebindCity" style="display:none" OnClick="lnkRebindCity_Click"></asp:LinkButton>
                                        <asp:Button Text="Save" runat="server" ID="Button1" OnClick="lnkBtnCity_Click" CssClass="btn btn-primary" ValidationGroup="CityAddPopupValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancelCityPopUp" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMeCity();" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="lnkRebindCity" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>   

          

         <div class="modal fade" id="AddCourtTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 90%; height: 40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom">
                            Add New Court Type</label>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    </div>
                   <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddCourtType" frameborder="0" runat="server" width="100%" height="150px"></iframe>
                    </div>
                    </div>
                </div>
             </div>
                       
                  
    </form>

    <script type="text/javascript">
       function restoreSelectedCity() {
            debugger;

            var selectedStr = $('#<% =hdnSelectedCity.ClientID %>').val();

            //alert(selectedStr);

            $('[id*=ddlCity]').multiselect('rebuild');

            var value = selectedStr.split(",");
            for (var i = 0; i < value.length; i++) {
                $("#ddlCity option[value=" + value[i] + "]").prop('selected', true);
            }
          
            $('[id*=ddlCity]').multiselect('refresh');
        }
        function CloseMeCity() {
            debugger;
	        $('#tbxCity').val('');
	        $('#AddCityPopUp').modal('hide');
	        document.getElementById('<%=lnkRebindCity.ClientID %>').click();
            restoreSelectedCity();
        }

       function restoreSelectedCourt() {
            debugger;

            var selectedStr = $('#<% =hdnSelectedCourt.ClientID %>').val();

            //alert(selectedStr);

           $('[id*=ddlCourtType]').multiselect('rebuild');

            var value = selectedStr.split(",");
            for (var i = 0; i < value.length; i++) {
                $("#ddlCourtType option[value=" + value[i] + "]").prop('selected', true);
            }
          
            $('[id*=ddlCourtType]').multiselect('refresh');
        }
        function CloseMeCourtType() {
            debugger;
            $('#txtCourtType').val('');
            $('#AddCourtTypePopUp').modal('hide');
            document.getElementById('<%= lnkRebindCourtType.ClientID %>').click();
            restoreSelectedCourt();
        }
       
    </script>
</body>
</html>
