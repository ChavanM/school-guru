﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="CriteriaMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.CriteriaMaster" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //Add criteria Popup
        function OpenCriteriaRetingPopUp(CriteriaId) {
            $('#AddCriteriaMasterPopUp').modal('show');
            $('#IframeCriteriaMastre').attr('src', "../../Litigation/Masters/ADDCriteria.aspx?CriteriaID=" + CriteriaId + "");
        }
        function CloseCriteriaPopUp() {
            $('#AddCriteriaMasterPopUp').modal('hide');
            RefreshParent();
        }
        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / Rating Criteria');
        });
    </script>
    <style type="text/css">
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <div style="margin-bottom: 4px" />
                <div class="col-md-3 colpadding0 entrycount">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search" CssClass="form-control" OnTextChanged="tbxtypeTofilter_TextChanged" AutoPostBack="true"/>
                </div>
                <div style="text-align: right">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" Width="7%" OnClick="lnkBtnApplyFilter_Click"  data-toggle="tooltip" ToolTip="Apply"/>
                    <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" ID="btnAddPromotor" OnClick="btnAddPromotor_Click"  data-toggle="tooltip" ToolTip="Add New Rating Criteria">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                </div>
                <div style="margin-bottom: 4px">
                    <asp:GridView runat="server" ID="grdCriteriaMaster" AutoGenerateColumns="false"  OnRowDataBound="grdCriteriaMaster_RowDataBound" ShowHeaderWhenEmpty="true"
                    AllowSorting="true"  AllowPaging="true" PageSize="10"  AutoPostBack="true"  CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID" OnRowCommand="grdCriteriaMaster_RowCommand" OnSorting="grdCriteriaMaster_Sorting" OnRowCreated="grdCriteriaMaster_RowCreated">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Criteria" SortExpression="Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="87%" />
                              <%--<asp:BoundField DataField="TypeCode" HeaderText=" CaseStageTypeCode" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40%"  />--%>
                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Criteria" ToolTip="Edit Rating Criteria" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit"/></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_Criteria" ToolTip="Delete Rating Criteria" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Rating Criteria?');">
                                                <img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete"/></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
 <div class="row">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-8 colpadding0">
                             <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                            </div>

                          <div class="col-md-3 colpadding0">                           
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width: 30%; float: right;margin-right:3%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" />
                                    <asp:ListItem Text="10" Selected="True" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                           </div>
                            <div class="col-md-1 colpadding0" style="float: right;">                                   
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                        OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                                    </asp:DropDownListChosen>                              
                            </div>
                                           
                    </div>
                      <asp:HiddenField ID="TotalRows" runat="server" Value="0" />       
                </div>          

            </div>
        </div>
    </div>
    <div class="modal fade" id="AddCriteriaMasterPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 30%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                                Criteria</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -9px;" onclick="CloseLayerRatingCriteriaPage();">&times;</button>
                </div>
               <div class="modal-body">
                            <iframe id="IframeCriteriaMastre" overflow-y="hideen" src="about:blank" style="width:100%;height:200px" frameborder="0"></iframe>
                        </div>
            </div>
        </div>
    </div>
</asp:Content>
