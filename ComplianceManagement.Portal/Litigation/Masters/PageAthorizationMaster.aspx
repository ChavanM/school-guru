﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="PageAthorizationMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.PageAthorizationMaster" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / PageAuthorizationMasters');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upPageAuthorizatonMaster" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <section class="panel">    
                           <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PageAutorizationValidationGroup" />
                            <asp:CustomValidator ID="cvPage" runat="server" EnableClientScript="False"
                                ValidationGroup="PageAutorizationValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                         </div>   
                           <div style="margin-bottom: 4px"> 
                               <div class="row">
                             <div class="col-md-12">
                                 <div class="col-md-6">
                                  <asp:DropDownListChosen runat="server" ID="ddlUserType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                   CssClass="form-control" Width="50%"  AutoPostBack="True" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged">
                                   </asp:DropDownListChosen>
                                   
                             </div>
                                 </div>
                                   </div> 
                               <div class="row">
                                   <div class="col-md-12">
                            <asp:GridView runat="server" ID="grdPageAuthorization" AutoGenerateColumns="false" AllowSorting="true" OnRowDataBound="grdPageAuthorization_RowDataBound"
                               PageSize="12" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" ShowHeaderWhenEmpty="true"
                                 DataKeyNames="ID" OnRowCommand="grdPageAuthorization_RowCommand">
                                <Columns>
                                     <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                     <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                                     </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField  DataField="Name" HeaderText="PageName" ItemStyle-Width="15%" HeaderStyle-Width="68%"/>
                                     <%--<asp:BoundField  DataField="ID"  ItemStyle-Width="15%" HeaderStyle-Width="68%" Visible="false"/>--%>
                                    <asp:TemplateField HeaderText="ADD" >  
                   <ItemTemplate>
                 <asp:CheckBox ID="chkADD" runat="server" 
                            AutoPostBack="true" 
                  Checked='<%# bool.Parse(Eval("ADDval").ToString()) %>'/>
                   </ItemTemplate>
                    
                </asp:TemplateField>          
                                     <asp:TemplateField HeaderText="UPDATE" >  
                   <ItemTemplate>
                 <asp:CheckBox ID="chkupdate" runat="server" 
                            AutoPostBack="true" 
                             Checked='<%# bool.Parse(Eval("UpdateVal").ToString()) %>'/>
                   </ItemTemplate>
                    
                </asp:TemplateField>          
                                     <asp:TemplateField HeaderText="DELETE" >  
                   <ItemTemplate>
                 <asp:CheckBox ID="chkDelete" runat="server" 
                            AutoPostBack="true" 
                           Checked='<%# bool.Parse(Eval("Deleteval").ToString()) %>'/>
                   </ItemTemplate>
                    
                </asp:TemplateField>          
                                     <asp:TemplateField HeaderText="VIEW" >  
                   <ItemTemplate>
                 <asp:CheckBox ID="chkView" runat="server" 
                            AutoPostBack="true" 
                   Checked='<%# bool.Parse(Eval("ViewVal").ToString()) %>' />
                   </ItemTemplate>
                    
                </asp:TemplateField>            
                    <asp:TemplateField HeaderText="ID" Visible="false">  
                   <ItemTemplate>
                <asp:Label ID="PageID" runat="server" Text='<%# Eval("ID")%>' Visible="false"></asp:Label >
                   </ItemTemplate>
                    
                </asp:TemplateField>            
<%--                                    <asp:TemplateField  HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Department"
                                                CommandArgument='<%# Eval("ID") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit Observation" title="Edit Department Details" /></asp:LinkButton>
                                            <asp:LinkButton Visible="false" ID="LinkButton2" runat="server" CommandName="DELETE_Department"
                                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Department Details?');"><img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete Department Details" title="Delete Department Details" /></asp:LinkButton>
                                        </ItemTemplate>                            
                                    </asp:TemplateField>--%>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />  
                                 <PagerSettings Visible="false" />             
                               <EmptyDataTemplate>
                                   No Record Found
                               </EmptyDataTemplate>
                            </asp:GridView>                     
                        </div></div>
<%--                    <div class="col-md-12 colpadding0">
                        <div class="col-md-offset-6">
                        <div class="col-md-6 colpadding0">
                         <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float:right"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                           </div>
                   
                        <div class="col-md-6 colpadding0" style="float: right;">
                            <div style="float: left; width: 25%">
                                <p class="clsPageNo">Page</p>
                            </div>
                            <div style="float: left;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No"
                                    OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    </div>
                        </div>--%>
                                                                                           <div class="row">
                                                                <div class="form-group col-md-12" style="text-align: center; width: 30%;margin-left: 358px;">
                                                                    <div style="text-align: center">
                                                                        <asp:Button Text="Save" runat="server" ID="btnSavePageAutorization" CssClass="btn btn-primary" OnClick="btnSavePageAutorization_Click"
                                                                            ValidationGroup="ValidLayerRating" CausesValidation="true"></asp:Button>
                                                                    </div>
                                                                </div>
                                                            </div>

                    </section>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
