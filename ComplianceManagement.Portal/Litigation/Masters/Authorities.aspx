﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="Authorities.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.Authorities" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
    <script type="text/javascript">
        function fopenpopup() {
            $('#divCourtTypeDialog').modal('show');
        }
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / Authority');
        });

        function OpenAddCourtPopUp(CourtID) {
            $('#AddCourtDataPopUp').modal('show');
            $('#ContentPlaceHolder1_IframeCourt').attr('src', "../../Litigation/Masters/AddAuthority.aspx?AuthId=" + CourtID);
            return false;
        }

        function OpenNewPopUp() {
            $('#AddCourtDataPopUp').modal('show');
            $('#ContentPlaceHolder1_IframeCourt').attr('src', "../../Litigation/Masters/AddAuthority.aspx");
            return false;
        }

        function CloseCourtMasterPopUp() {
            $('#AddCourtDataPopUp').modal('hide');
            RefreshParent();
            //location.reload();
        }
        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                
                        <div style="margin-bottom: 4px" />

                <div class="col-md-3 colpadding0 entrycount">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search" CssClass="form-control" Visible="false" />
                </div>

                <div style="text-align: right">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" Width="7%" OnClick="lnkBtnApplyFilter_Click" Visible="false" data-toggle="tooltip" ToolTip="Apply"/>
                    <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" ID="btnAddPromotor" OnClientClick="return OpenNewPopUp();"   data-toggle="tooltip" ToolTip="Add New Authority">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                </div>
                <%--<i class='fa fa-plus'></i>--%>
                <div style="margin-bottom: 4px">
                    <asp:GridView runat="server" ID="grdCourtMaster" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true" OnSorting="grdCourtMaster_Sorting"
                        PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID"
                         OnRowCommand="grdCourtMaster_RowCommand" OnRowCreated="grdCourtMaster_RowCreated" OnRowDataBound="grdCourtMaster_RowDataBound">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        
                            <asp:BoundField DataField="Name" SortExpression="Name" HeaderText="Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="45%" />
                            <asp:BoundField DataField="Designation" SortExpression="Designation" HeaderText="Designation" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="45%" />
                           

                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtcourMedit" runat="server" CommandName="EDIT_Court" ToolTip="Edit Authority" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit Authority Type" /></asp:LinkButton>
                                    <asp:LinkButton ID="lbtcourMdelete" runat="server" CommandName="DELETE_Court" ToolTip="Delete Authority" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to Delete this Authority Type?');"><img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete Authority Type Details"/></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>



                </div>

                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>&nbsp;- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 colpadding0" style="float: right;">
                        <div style="float: left; width: 50%">
                            <p class="clsPageNo" style="color: #666">Page</p>
                        </div>
                        <div style="float: left; width: 50%">
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddCourtDataPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Authority</label>
                    <button type="button" class="close" aria-hidden="true" data-dismiss="modal" onclick="RefreshParent()">x</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeCourt" runat="server" width="100%" height="260px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>



</asp:Content>