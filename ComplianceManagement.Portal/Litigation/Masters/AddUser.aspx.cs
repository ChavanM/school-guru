﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Configuration;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Mode"] = 0;

                BindLawyer();
            }
        }

        public void BindLawyer()
        {
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            var obj = LawyerManagement.GetLawyerListForMapping(customerID);

            ddlLawyerList.DataTextField = "Name";
            ddlLawyerList.DataValueField = "ID";

            ddlLawyerList.DataSource = obj;
            ddlLawyerList.DataBind();

            ddlLawyerList.Items.Insert(0, new ListItem("Select", "-1"));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {                
                long ContactNo = 0;
                bool MailValid = false;
                string MailID = string.Empty;

                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if(!string.IsNullOrEmpty(tbxContactNo.Text.Trim()))
                {
                    ContactNo = Convert.ToInt64(tbxContactNo.Text.Trim());
                }
                if (!string.IsNullOrEmpty(tbxEmail.Text.Trim()))
                {
                    MailID = tbxEmail.Text.Trim();
                }               

                if (ZohoCRMAPI.IsValidEmail(MailID))
                {
                    MailValid = true;
                }

                int LawyerID = -1;

                if (!string.IsNullOrEmpty(ddlLawyerList.SelectedValue))
                {
                    if (ddlLawyerList.SelectedValue != "-1")
                    {
                        LawyerID = Convert.ToInt32(ddlLawyerList.SelectedValue);
                    }
                }

                #region Compliance User
                User user = new User()
                {
                    IsActive=true,
                    IsExternal = true,
                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    CustomerID = customerID,                    
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    RoleID = 7,
                    LitigationRoleID=7,
                    LawyerFirmID= LawyerID,
                };

                #endregion

                #region Audit User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    IsActive = true,
                    IsExternal = true,
                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    CustomerID = customerID,                    
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    RoleID = 7,
                    LitigationRoleID = 7,
                    LawyerFirmID = LawyerID,
                };

                #endregion

                bool emailExists;

                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "User with Same Email already Exists.";
                    return;
                }

                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "User with Same Email already Exists.";
                    return;
                }

                bool result = false;
                int resultValue = 0;
                if ((int) ViewState["Mode"] == 0)
                {
                    string passwordText = Util.CreateRandomPassword(10); 
                                      
                    user.Password = Util.CalculateAESHash(passwordText);
                    mstUser.Password = Util.CalculateAESHash(passwordText);
                    //string message = getEmailMessage(user, passwordText);

                    if (Convert.ToString(ContactNo).Length == 10 && (Convert.ToString(ContactNo).StartsWith("9") || Convert.ToString(ContactNo).StartsWith("8") || Convert.ToString(ContactNo).StartsWith("7")))
                    {
                        if (MailValid)
                        {
                            resultValue = LitigationUserManagement.CreateNewUserCompliance(user);
                            if (resultValue > 0)
                            {
                                result = LitigationUserManagement.CreateNewUserAudit(mstUser);

                                if (!result)
                                {
                                    UserManagement.deleteUser(resultValue);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "User Created Successfully.";
                                    ValidationSummary1.CssClass = "alert alert-success";
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Invalid Email, Please enter Email in valid format.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Invalid Contact Number or Contact Number should starts with 9, 8 or 7.";
                    }
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbCheckType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbCheckType.SelectedValue == "Internal")
            {
                divLawyer.Visible = false;
            }
            else if (rbCheckType.SelectedValue == "External")
            {
                divLawyer.Visible = true;
            }
        }

        private string getEmailMessage(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                       .Replace("@Username", user.Email)
                                       .Replace("@User", username)
                                       .Replace("@PortalURL", Convert.ToString(portalURL))
                                       .Replace("@Password", passwordText)
                                       .Replace("@From", ReplyEmailAddressName)
                                       .Replace("@URL", Convert.ToString(portalURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return null;
            }
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                      .Replace("@Username", user.Email)
                                      .Replace("@User", username)
                                      .Replace("@PortalURL", Convert.ToString(portalURL))
                                      .Replace("@From", ReplyEmailAddressName)
                                      .Replace("@URL", Convert.ToString(portalURL));

                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }
    }
}