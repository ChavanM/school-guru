﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class EntitiesAssignmentMaster : System.Web.UI.Page
    {
        protected bool flag;
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 7;
        public static List<long> locationList = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                BindUser();
                BindLocationFilter();
                tbxFilterLocation.Text = " Select Entity/Location";
                BindEntitiesList();
                bindPageNumber();
                ShowGridDetail();


            }
            lblName.InnerText = "Assign New Entity";
        }
        public void BindUser()
        {
            int customerID = -1;
            int complianceProductType = 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                complianceProductType = AuthenticationHelper.ComplianceProductType;
                //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
            }
           
            var users = UserManagement.GetAllLitigationManagmentUser(customerID, complianceProductType);

            ddlUser.DataValueField = "ID";
            ddlUser.DataTextField = "Name";
            ddlUser.DataSource = users;
            ddlUser.DataBind();

            ddlUser.Items.Insert(0, new ListItem("Select User", "-1"));

          

        }
        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                //TreeNode node = new TreeNode("< All >", "-1");
                //node.Selected = true;
                //tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation.CollapseAll();
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                string Flag = "Save";
                //btnAddNew.Text = "Save";
                //lblName.InnerText = "Add new Entities Assignment";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenEntitiesPopup('" + Flag + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                BindEntitiesList();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
            BindEntitiesList();
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdEntitiesAssignment.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindEntitiesList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdEntitiesAssignment.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void BindEntitiesList()
        {
            try
            {

              
                int branchID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlUser.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlUser.SelectedValue);
                }
           

                locationList.Clear();
                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                List<LitigationAssignmentEntitiesView> masterlist = new List<LitigationAssignmentEntitiesView>();
                if (locationList.Count > 0)
                {
                    masterlist = SelectAllEntitiesList(userID, locationList);
                }
                else
                {
                   masterlist = SelectAllEntities(branchID,userID);
                }


                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            masterlist = masterlist.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            masterlist = masterlist.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (masterlist.Count > 0)
                {
                    grdEntitiesAssignment.DataSource = masterlist;
                    Session["TotalRows"] = masterlist.Count;
                    grdEntitiesAssignment.DataBind();
                }
                else
                {
                    grdEntitiesAssignment.DataSource = masterlist;
                    grdEntitiesAssignment.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<LitigationAssignmentEntitiesView> SelectAllEntities(int branchId = -1, int userID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.LitigationAssignmentEntitiesViews
                                                   select row).ToList();

                var branchIds = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false 
                                 select row.ID).ToList();

                if (branchIds != null)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => branchIds.Contains((int)entry.BranchID)).ToList();
                }
                if (branchId != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.BranchID == branchId).ToList();
                }
                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }

                return ComplianceTransactionEntity;
            }
        }
        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != AuthenticationHelper.CustomerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != AuthenticationHelper.CustomerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public static List<LitigationAssignmentEntitiesView> SelectAllEntitiesList(int userID, List<long> CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.LitigationAssignmentEntitiesViews
                                                   select row).ToList();

                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }

                if (CustomerBranchID.Count > 0)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => CustomerBranchID.Contains((int)entry.BranchID)).ToList();
                }
                return ComplianceTransactionEntity;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdEntitiesAssignment.PageIndex = chkSelectedPage - 1;

                //SelectedPageNo.Text = (chkSelectedPage).ToString();
                grdEntitiesAssignment.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindEntitiesList();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdEntitiesAssignment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int EntitiesassignmentID = 0;
            if (e.CommandName.Equals("DELETE_Entities"))
            {
                EntitiesassignmentID = Convert.ToInt32(e.CommandArgument);
                DeleteEntitiesAssignment(EntitiesassignmentID);
                BindEntitiesList(); 
                bindPageNumber();
            }
        }

        protected void DeleteEntitiesAssignment(long EntitiesassignmentID)
        {
           
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Entitiesdata = (from row in entities.LitigationEntitiesAssignments
                                   where row.ID == EntitiesassignmentID
                                    select row).SingleOrDefault();

                
                   entities.LitigationEntitiesAssignments.Remove(Entitiesdata);
                   entities.SaveChanges();

            }
            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = "Entity assigned to user deleted successfully.";
            vsEntities.CssClass = "alert alert-success";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

        }
        protected void grdEntitiesAssignment_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdEntitiesAssignment_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdEntitiesAssignment_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEntitiesList();
            bindPageNumber();
        }

       
    }
}