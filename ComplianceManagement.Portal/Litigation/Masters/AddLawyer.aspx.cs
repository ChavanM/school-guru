﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddLawyer : System.Web.UI.Page
    {
        protected bool flag;
        private long CustomerID = AuthenticationHelper.CustomerID;
        public static List<int> LawyerTypeList = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Mode"] = 0;
                BindCustomers(ddlCustomerPopup);
                BindDepartment();
                BindRoles();
                BingLawyerList();
                flag = false;
                ddlLitigationRole_SelectedIndexChanged(null, null);
            }
        }

        private void BindCustomers(DropDownList ddlitoFill)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlitoFill.DataTextField = "Name";
                ddlitoFill.DataValueField = "ID";

                ddlitoFill.DataSource = CustomerManagement.GetAll(customerID);
                ddlitoFill.DataBind();

                //ddlitoFill.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void BingLawyerList()
        {
            try
            {
                var AllData = LawyerManagement.GetLawyerListAll(CustomerID);
                var users = (from row in AllData
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                ddlLayerList.DataTextField = "Name";
                ddlLayerList.DataValueField = "ID";
                ddlLayerList.DataSource = users;
                ddlLayerList.DataBind();

                ddlLayerList.Items.Insert(0, new ListItem("Select Lawyer", "-1"));
                ddlLayerList.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindDepartment()
        {
            try
            {
                if (ddlCustomerPopup.SelectedValue != "" && ddlCustomerPopup.SelectedValue != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    ddlDepartment.DataTextField = "Name";
                    ddlDepartment.DataValueField = "ID";
                    ddlDepartment.DataSource = CompDeptManagement.FillDepartment(customerID);
                    ddlDepartment.DataBind();
                    ddlDepartment.Items.Insert(0, new ListItem("Select Department", "-1"));
                    ddlDepartment.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindRoles()
        {
            try
            {
                var CheckRole = string.Empty;
                ddlLitigationRole.DataTextField = "Name";
                ddlLitigationRole.DataValueField = "ID";

                var roles = RoleManagement.GetLitigationRoles(true);
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }

                else if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }
                else
                {
                    roles = roles.Where(entry => entry.Code.Equals("EXCT")).ToList();
                }

                ddlLitigationRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlLitigationRole.DataBind();
                ddlLitigationRole.Items.Insert(0, new ListItem("Select User Role", "-1"));

                ddlLitigationRole.Enabled = true;
                //if (CheckRole == "EXCT")
                //{
                //    ddlLitigationRole.SelectedItem.Text = "Non - Admin";
                //    ddlLitigationRole.Enabled = false;
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;
                var branchs = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(ddlCustomerPopup.SelectedValue));
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }
                tbxBranch.Text = "Select Location";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void upUsersPopup_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlLitigationRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlLitigationRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");

                if (divCustomer.Visible && AuthenticationHelper.Role != "CADMN")
                {
                    //ddlCustomerPopup.SelectedValue = "-1";
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }
                if (divCustomerBranch.Visible)
                {
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                if (divReportingTo.Visible)
                {
                    BindReportingTo();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlCustomerPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                BindReportingTo();
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindReportingTo()
        {
            try
            {
                ddlReportingTo.DataTextField = "Name";
                ddlReportingTo.DataValueField = "ID";

                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlLitigationRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(Convert.ToInt32(ddlCustomerPopup.SelectedValue), roleCode);
                ddlReportingTo.DataBind();

                ddlReportingTo.Items.Insert(0, new ListItem("Select Reporting to person", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Branch";
                ScriptManager.RegisterStartupScript(this.upUsersPopup, this.upUsersPopup.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private string getEmailMessage(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                       .Replace("@Username", user.Email)
                                       .Replace("@User", username)
                                       .Replace("@PortalURL", Convert.ToString(portalURL))
                                       .Replace("@Password", passwordText)
                                       .Replace("@From", ReplyEmailAddressName)
                                       .Replace("@URL", Convert.ToString(portalURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
                return null;
            }
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                       .Replace("@Username", user.Email)
                                       .Replace("@User", username)
                                       .Replace("@PortalURL", Convert.ToString(portalURL))
                                       .Replace("@From", ReplyEmailAddressName)
                                       .Replace("@URL", Convert.ToString(portalURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return null;
        }
        protected void SaveLawyer_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);

                int? LaywerID = null;

                if (!string.IsNullOrEmpty(ddlLayerList.SelectedValue))
                {
                    if (ddlLayerList.SelectedValue != "-1")
                    {
                        LaywerID = Convert.ToInt32(ddlLayerList.SelectedValue);
                    }
                }

                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstNameUser.Text.Trim(),
                    LastName = tbxLastNameUser.Text.Trim(),
                    Designation = tbxDesignation.Text.Trim(),
                    Email = tbxEmailUser.Text.Trim(),
                    ContactNumber = tbxContactNoUser.Text.Trim(),
                    Address = tbxAddressUser.Text.Trim(),
                    //RoleID = getproductcOMPLIANCE,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    LawyerFirmID = LaywerID,
                };

                if (chkHead.Checked)
                    user.IsHead = true;
                else
                    user.IsHead = false;

                if (divCustomer.Visible)
                {
                    user.CustomerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }

                if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                {
                    user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                }

                if (ddlLitigationRole.SelectedValue != "" && ddlLitigationRole.SelectedValue != "-1")
                {
                    user.LitigationRoleID = Convert.ToInt32(ddlLitigationRole.SelectedValue);
                }

                List<UserParameterValue> parameters = new List<UserParameterValue>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox) entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField) entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));

                    parameters.Add(new UserParameterValue()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }

                #endregion

                #region Audit User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    FirstName = tbxFirstNameUser.Text.Trim(),
                    LastName = tbxLastNameUser.Text.Trim(),
                    Designation = tbxDesignation.Text.Trim(),
                    Email = tbxEmailUser.Text.Trim(),
                    ContactNumber = tbxContactNoUser.Text.Trim(),
                    Address = tbxAddressUser.Text.Trim(),
                    //RoleID = getproductrisk,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    LawyerFirmID = LaywerID
                };

                if (chkHead.Checked)
                    mstUser.IsHead = true;
                else
                    mstUser.IsHead = false;

                if (divCustomer.Visible)
                {
                    mstUser.CustomerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    mstUser.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }

                if (ddlLitigationRole.SelectedValue != "" && ddlLitigationRole.SelectedValue != "-1")
                {
                    mstUser.LitigationRoleID = Convert.ToInt32(ddlLitigationRole.SelectedValue);
                }

                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox) entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField) entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));

                    parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }
                #endregion

                if (rbCheckType.SelectedValue == "Internal")
                {
                    user.IsExternal = false;
                    mstUser.IsExternal = false;
                }
                else if (rbCheckType.SelectedValue == "External")
                {
                    if (ddlLayerList.SelectedValue != "" && ddlLayerList.SelectedValue != "-1")
                    {
                        user.IsExternal = true;
                        mstUser.IsExternal = true;
                    }
                    else
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "Select Lawyer Firm, if User Type is External";
                        return;
                    }
                }

                bool result = false;
                int resultValue = 0;
                if ((int) ViewState["Mode"] == 0)
                {
                    //Check other User with same email
                    bool emailExists;
                    UserManagement.Exists(user, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        return;
                    }

                    UserManagementRisk.Exists(mstUser, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        return;
                    }

                    string passwordText = Util.CreateRandomPassword(10);

                    user.CreatedBy = AuthenticationHelper.UserID;
                    user.CreatedByText = AuthenticationHelper.User;
                    user.Password = Util.CalculateAESHash(passwordText);
                    user.RoleID = Convert.ToInt32(user.LitigationRoleID);

                    string message = getEmailMessage(user, passwordText);

                    mstUser.CreatedBy = AuthenticationHelper.UserID;
                    mstUser.CreatedByText = AuthenticationHelper.User;
                    mstUser.Password = Util.CalculateAESHash(passwordText);
                    mstUser.RoleID = Convert.ToInt32(mstUser.LitigationRoleID);

                    resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    var key = "LitiLawyer-" + Convert.ToString(CustomerID);
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Remove(key);
                    }
                    if (resultValue > 0)
                    {
                        result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (result == false)
                        {
                            UserManagement.deleteUser(resultValue);
                        }
                    }
                }
                else if ((int) ViewState["Mode"] == 1)
                {
                    int userID = -1;
                    userID = Convert.ToInt32(ViewState["UserID"]);

                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                    mstUser.ID = Convert.ToInt32(ViewState["UserID"]);

                    //Check other User with same email
                    bool emailExists;
                    UserManagement.Exists(user, out emailExists);

                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        return;
                    }

                    UserManagementRisk.Exists(mstUser, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        return;
                    }

                    //Get Existing Users AuditorID and Compliance-Audit Product RoleID
                    User complianceUser = UserManagement.GetByID(userID);
                    mst_User auditUser = UserManagementRisk.GetByID(userID);

                    if (complianceUser != null)
                    {
                        user.AuditorID = complianceUser.AuditorID;
                        user.RoleID = complianceUser.RoleID;
                    }

                    if (auditUser != null)
                    {
                        mstUser.AuditorID = auditUser.AuditorID;
                        mstUser.RoleID = auditUser.RoleID;
                    }

                    //result = UserManagement.Update(user, parameters);
                    //result = UserManagementRisk.Update(mstUser, parametersRisk);

                    result = LitigationUserManagement.UpdateUserLitigation(user, parameters);
                    result = LitigationUserManagement.UpdateAuditDBUserLitigation(mstUser, parametersRisk);

                    var key = "LitiLawyer-" + Convert.ToString(CustomerID);
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Remove(key);
                    }

                    if (tbxEmailUser.Text.Trim() != complianceUser.Email && result)
                    {
                        string message = SendNotificationEmailChanged(user);
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    }
                }
                else
                {
                    cvUserPopup.IsValid = false;
                    cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
                }

                if (result)
                {
                    if (UserImageUpload.HasFile)
                    {
                        string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);

                        string filepath = "~/UserPhotos/" + fileName;

                        UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                        UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                    }

                    if (result)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "Lawyer Details Saved Successfully.";
                        vsUserPopup.CssClass = "alert alert-success";
                        upUsersPopup.Update();
                    }
                }

                //ScriptManager.RegisterStartupScript(this.upUsersPopup, this.upUsersPopup.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void lnkBtnDept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lnkLawFirmBind_Click(object sender, EventArgs e)
        {
            BingLawyerList();
        }
    }
}