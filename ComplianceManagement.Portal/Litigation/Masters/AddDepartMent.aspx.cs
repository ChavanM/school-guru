﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddDepartMent : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["DepartmentID"]))
                {
                    int DeptID = Convert.ToInt32(Request.QueryString["DepartmentID"]);
                    Department RPD = CompDeptManagement.DepartmentMasterGetByID(DeptID, CustomerID);//, CustomerID
                    txtFName.Text = RPD.Name;
                    ViewState["Mode"] = 1;
                    ViewState["DeptID"] = DeptID;
                }
                else
                {
                    ViewState["Mode"] = 0;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Department objDeptcompliance = new Department()
                {
                    Name = txtFName.Text.Trim(),
                    IsDeleted = false,
                    CustomerID= (int)CustomerID
                };
                mst_Department objDeptaudit = new mst_Department()
                {
                    Name = txtFName.Text.Trim(),
                    IsDeleted = false,
                    CustomerID = (int) CustomerID
                };
                if ((int) ViewState["Mode"] == 1)
                {
                    objDeptcompliance.ID = Convert.ToInt32(ViewState["DeptID"]);
                    objDeptaudit.ID = Convert.ToInt32(ViewState["DeptID"]);
                }
                if ((int) ViewState["Mode"] == 0)
                {
                    if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Already Exists";
                    }
                    else
                    {
                        CompDeptManagement.CreateDepartmentMaster(objDeptcompliance);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Added Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                        txtFName.Text = string.Empty;
                        var key = "Litidepart-" + Convert.ToString(CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                    }
                    if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Already Exists.";
                    }
                    else
                    {
                        CompDeptManagement.CreateDepartmentMasterAudit(objDeptaudit);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Added Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                        txtFName.Text = string.Empty;
                        var key = "Litidepart-" + Convert.ToString(CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                    }
                }
                else if ((int) ViewState["Mode"] == 1)
                {
                    if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Already Exists.";
                    }
                    else
                    {
                        CompDeptManagement.UpdateDepartmentMaster(objDeptcompliance);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department updated successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                        var key = "Litidepart-" + Convert.ToString(CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                    }
                    if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department already Exists.";
                    }
                    else
                    {
                        CompDeptManagement.UpdateDepartmentMasterAudit(objDeptaudit);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department updated successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                        var key = "Litidepart-" + Convert.ToString(CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDeptPopup.IsValid = false;
                cvDeptPopup.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancelDeptPopUp_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}