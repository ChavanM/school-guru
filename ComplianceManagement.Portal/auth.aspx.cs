﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using AjaxControlToolkit;
using Common.Logging.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class AuthenticationPage : System.Web.UI.Page
    {
        string ipaddress = string.Empty;
        string Macaddress = string.Empty;
        //protected string Approveruser_Roles;
        //protected List<Int32> roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    bool ValidUserFlag = false;
                    string TL_Profile_ID_Decrypted = string.Empty;
                    string TL_AuthKey_Decrypted = string.Empty;

                    //string TL_Profile_ID = HttpUtility.UrlEncode(HttpContext.Current.Request.QueryString["ID"].ToString()); 
                    //string TL_AuthKey_Encrypted = HttpUtility.UrlEncode(HttpContext.Current.Request.QueryString["Auth"].ToString());

                    string TL_Profile_ID = Request.QueryString["ID"];
                    string TL_AuthKey_Encrypted = Request.QueryString["Auth"];
                    //string TL_User_ID = Request.Form["ID"];

                    if (!String.IsNullOrEmpty(TL_Profile_ID) && !String.IsNullOrEmpty(TL_AuthKey_Encrypted))
                    {
                        string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];

                        //string TL_User_ID_encrypted = CryptographyHandler.encrypt(TL_Profile_ID.Trim(), TLConnectKey);
                        //TL_User_ID_Decrypted = CryptographyHandler.decrypt(TL_User_ID_encrypted.Trim(), TLConnectKey);

                        TL_Profile_ID_Decrypted = CryptographyHandler.decrypt(TL_Profile_ID, TLConnectKey);

                        if (TL_Profile_ID_Decrypted.Equals("Error"))
                        {
                            string strURL = Page.Request.Url.AbsoluteUri;
                            if (!string.IsNullOrEmpty(strURL))
                            {
                                int startIndex = strURL.IndexOf("ID=");
                                int endIndex = strURL.IndexOf("&Auth");

                                TL_Profile_ID = strURL.Substring(startIndex + 3, (endIndex - startIndex) - 3);
                                TL_Profile_ID_Decrypted = CryptographyHandler.decrypt(TL_Profile_ID, TLConnectKey);
                            }
                        }

                        TL_AuthKey_Decrypted = CryptographyHandler.decrypt(TL_AuthKey_Encrypted, TLConnectKey);

                        //TL_Profile_ID_Decrypted = CryptographyHandler.decrypt(TL_Profile_ID.Trim(), TLConnectKey);
                        //TL_AuthKey_Decrypted = CryptographyHandler.decrypt(TL_AuthKey_Encrypted.Trim(), TLConnectKey);
                        //TL_AuthKey_Decrypted = TL_AuthKey_Encrypted;

                        if (!string.IsNullOrEmpty(TL_Profile_ID_Decrypted.Trim()) && !string.IsNullOrEmpty(TL_AuthKey_Decrypted.Trim()))
                        {
                            //Boolean ValidUserFlag = Business.RLCS.RLCSManagement.CheckUserIDAuthentication(TL_User_ID_Decrypted);
                            ValidUserFlag = Business.RLCS.RLCSManagement.CheckProfileID_Authentication(TL_Profile_ID_Decrypted);

                            if (ValidUserFlag)
                            {
                                ProcessAuthenticationInformation(TL_Profile_ID_Decrypted.Trim(), TL_AuthKey_Decrypted.Trim());
                                Session["AuthKey"] = TL_AuthKey_Decrypted;
                            }
                        }
                    }

                    if (ValidUserFlag)
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Authentication Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void ProcessAuthenticationInformation(string TLConnect_profileID, string authKey)
        {
            try
            {
                #region If No EmailID
                RLCS_User_Mapping user = null;
                //if (UserManagement.IsValidUserID(TL_Connect_USer_ID, out user))
                if (UserManagement.IsValidProfileID(TLConnect_profileID, out user))
                {
                    if (user != null)
                    {
                        try
                        {
                            Macaddress = Util.GetMACAddress();
                            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            if (ipaddress == "" || ipaddress == null)
                                ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                        if (user.IsActive)
                        {
                            Session["userID"] = user.AVACOM_UserID;
                            Session["ContactNo"] = user.ContactNumber;
                            Session["Email"] = user.Email;
                            Session["RLCS_userID"] = null;
                            Session["RLCS_userID"] = user.UserID;

                            Session["RLCS_ProfileID"] = null;
                            Session["RLCS_ProfileID"] = user.ProfileID;

                            MaintainLoginDetail objData = new MaintainLoginDetail()
                            {
                                UserId = Convert.ToInt32(user.AVACOM_UserID),
                                Email = user.Email,
                                CreatedOn = DateTime.Now,
                                IPAddress = ipaddress,
                                MACAddress = Macaddress,
                                LoginFrom = "WT",
                                ProfileID = user.ProfileID
                            };
                            UserManagement.Create(objData);

                            Session["RM"] = false;
                            Session["EA"] = Util.CalculateAESHash(user.Email.Trim());
                            Session["MA"] = Util.CalculateAESHash(Macaddress.Trim());

                            RLCSProcessAuthenticationInformation(user, TLConnect_profileID, authKey);
                            Session["RM"] = null;
                            Session["EA"] = null;
                            Session["MA"] = null;
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Your Account is Disabled.";
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void RLCSProcessAuthenticationInformation(RLCS_User_Mapping user, string profileID, string authKey)
        {
            try
            {
                int cid = UserManagement.GetCustomerIDRLCS(user.CustomerID);
                if (cid != -1)
                {
                    string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string role = user.AVACOM_UserRole;
                    int checkInternalapplicable = 2;
                    int checkTaskapplicable = 2;
                    int checkVerticalapplicable = 2;
                    int checkLabelApplicable = 2;
                    bool IsPaymentCustomer = false;

                    int complianceProdType = 1;                    
                    //complianceProdType = RLCS_Master_Management.GetComplianceProductType(cid);                    
                    
                    Customer c = CustomerManagement.GetByID(cid);
                    if (c != null)
                    {
                        var IsInternalComplianceApplicable = c.IComplianceApplicable;
                        if (IsInternalComplianceApplicable != -1 && IsInternalComplianceApplicable != null)
                        {
                            checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                        }
                        var IsTaskApplicable = c.TaskApplicable;
                        if (IsTaskApplicable != -1 && IsTaskApplicable != null)
                        {
                            checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                        }
                        var IsVerticlApplicable = c.VerticalApplicable;
                        if (IsVerticlApplicable != null && IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }
                        if (c.IsPayment != null)
                        {
                            IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                        }

                        var IsLabelApplicable = c.IsLabelApplicable;
                        if (IsLabelApplicable != -1 && IsLabelApplicable != null)
                        {
                            checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                        }

                        if (c.ComplianceProductType != null)
                        {
                            complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                        }                        
                    }

                    if (role.Equals("SADMN"))
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, 'C', cid, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                        Response.Redirect("~/Users/UserSummary.aspx", false);
                    }
                    else if (role.Equals("IMPT"))
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, 'C', cid, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                        Response.Redirect("~/Common/CompanyStructure.aspx", false);
                    }
                    else
                    {
                        ProductMappingStructure _obj = new ProductMappingStructure();
                        var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(cid));
                        if (ProductMappingDetails.Count >= 1)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSCompliance(cid, user, role, name, checkInternalapplicable, checkTaskapplicable, profileID, authKey, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }                       
                        else
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, "C", cid, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                        }
                    }
                    Session["ChangePassword"] = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAutontication_Click(object sender, EventArgs e)
        {

        }
    }
}