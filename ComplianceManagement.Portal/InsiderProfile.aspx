﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="InsiderProfile.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InsiderProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">
    .k-i-arrow-60-down{
        margin-top:5px;
    }
    .k-i-calendar{
        margin-top:5px;
    }
    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
    .tableheadercustomcolor{
        background:#f2f2f2;
    }
</style>
    <style>
        body{
        color:#666;
        font-weight:300;
    }
        .form-group.required .control-label:after { 
   content:"*";
   color:red;
}
        html .k-grid tr:hover {
    background: #E4F7FB;
}
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-sm-12" style="padding:0">
                        <div id="tabstrip">
                            <ul>
                                <li class="k-state-active">
                                    My Profile
                                </li>
                                            <%    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "HDCS") {  %>
            

        
                                <li>
                                    Other Profile
                                </li>
                                    <% } %>
                            </ul>
                            <div>
                                <form class="form-inline"  enctype="multipart/form-data" method="post" >
                                    <div class="row" style="margin-bottom: 15px;">
                                    <div class="col-sm-9"></div>
                                    <div class="col-sm-2">
                                    <button  style="float:right;"  id="button11" type="button" class="btn btn-primary"  onclick="EditableHD()" >Edit</button> 
                                        </div>
                                    </div>
            <div class="col-sm-12" style="padding:0;">
                
                                    <div class="col-sm-6  required " style="padding:0" >
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">First Name </label>
                <span id="change1" class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="firstname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="First Name" autocomplete="off" readonly ></span>
            </div>
            <div class="col-sm-6  required " style="padding:0" >
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="LastName">Last Name </label>
                <span id="change2"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="lastname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Last Name" autocomplete="off" readonly></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="Pan">PAN </label>
                <span id="change3" class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="pan" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="PAN" autocomplete="off" readonly></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="Mobile">Mobile </label>
                <span id="change4"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="mobile" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Mobile" autocomplete="off"readonly ></span>
            </div>
            
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="email">Company Email </label>
                <span id="change5"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="email" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Email" autocomplete="off"  readonly></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="designation">Designation </label>
                <span id="change6"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="designation" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Designation" autocomplete="off"readonly ></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="department">Department </label>
                <span id="change7" class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="department" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Department" autocomplete="off"readonly ></span>
            </div>
            
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="reportto">Report To </label>
                <span id="change8"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="reportto" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Report To" autocomplete="off" readonly></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="nationality">Nationality </label>
                <span id="change9"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="nationality" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Nationality" autocomplete="off"readonly ></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Date of Joining </label>
                <div id="change10" class="col-sm-4" style="padding:0"><input id="datepicker1" value="10/10/2011" title="datepicker"  disabled/></div>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;visibility:hidden"> 
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="address">Address</label>
                 <span class="col-sm-8 k-widget k-textbox" style="width:50%" ><textarea id="address1" style="width: 100%;" required data-required-msg="Please enter a text." data-max-msg="Enter value between 1 and 200" readonly ></textarea></span>
            </div>

            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="address">Address</label>
                <span  id="change11" class="col-sm-8 k-widget k-textbox" style="width:50%" ><textarea id="address" style="width: 100%;" required data-required-msg="Please enter a text." data-max-msg="Enter value between 1 and 200" readonly></textarea></span>
            </div>
            
            <div class="col-sm-12" style="padding:0;margin-top:5px;">
                <label class="col-sm-2 control-label" style="font-weight:450;padding-top: 10px;" for="relation">Qualifications </label>
                <!--<input type="button" id="more_fields_qualification" value="+" onClick="add_fields_qualification()" />-->
                <div class="col-sm-4" style="padding-top: 10px;padding-left:0"><input type="button" id="more_fields_qualification" value="+" onclick="add_fields_qualification()"disabled /></div>
            </div>
            <div class="col-sm-12" style="padding:0;margin-top:5px;display:inline-block;">
                <div class="col-sm-6" style="font-weight:450;">Qualification & Institution Details</div>
            </div>
            <div class="wrapper-comp-setting" id="flashcard-list-qualification">
            <!-- <div class="col-sm-12 fc-item-qualification" style="padding:0;margin-top:5px;">
                <div class="col-sm-6" style="padding:0;margin-top:5px;padding-left:15px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="qualification1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Qualification" autocomplete="off" ></span>
                </div>
                <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)"> 
             </div> -->
            </div>
            <div class="col-sm-12" style="padding:0;margin-top:15px;">
                <label class="col-sm-2 control-label" style="font-weight:450;padding-top: 5px;" for="relation">Previous Employer</label>
                <div class="col-sm-4" style="padding-left:0"><input type="button" id="more_fields_employee" value="+" onclick="add_fields_employee()"disabled /></div>
                
            </div>
            <div class="col-sm-12" style="padding:0;margin-top:0px;display:inline-block;">
                <div class="col-sm-2" style="font-weight:450;">Name</div>
                <div class="col-sm-2" style="font-weight:450;">From</div>
                <div class="col-sm-2" style="font-weight:450;">To</div>
            </div>
            <div class="wrapper-comp-setting" id="flashcard-list-employee">
            <!--<div class="col-sm-12 fc-item-employee" style="padding:0;margin-top:5px;">
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:15px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="employer1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <input id="from1" value="10/10/2011" title="datepicker" style="width:100%" />
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <input id="to1" value="10/10/2011" title="datepicker" style="width:100%" />
                </div>
                <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)"> 
             </div> -->
            </div>
            
            <div class="col-sm-12" style="padding:0;margin-top:15px;">
                <label class="col-sm-2 control-label" style="font-weight:450;padding-top: 5px;" for="relation">Relations</label>
               <div class="col-sm-4" style="padding-left:0"><input type="button" id="more_fields" value="+" onclick="add_fields()" disabled /></div>
                
            </div>
            <div class="col-sm-12" style="padding:0;margin-top:5px;display:inline-block">
                <div class="col-sm-2" style="font-weight:450;">Relation Type</div>
                <div class="col-sm-2" style="font-weight:450;">Name</div>
                <div class="col-sm-2" style="font-weight:450;">Dependant</div>
                <div class="col-sm-2" style="font-weight:450;">ID Type</div>
                <div class="col-sm-2" style="font-weight:450;">ID Code</div>
                
            </div>
            <div class="wrapper-comp-setting" id="flashcard-list">
            <!--
                <div class="col-sm-12 fc-item" style="padding:0;margin-top:5px;">
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:15px;">
                    <input  id="relationDropdown1" style="width:100%" data-placeholder="Relation Type" >
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="relation1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <input  id="dependant1" style="width:100%" data-placeholder="Relation Type" >
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="idtype1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Type" autocomplete="off" ></span>
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="idcode1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>
                </div>
                <input style="margin-top:10px;margin-left:15px;" type="button" value="-" onclick="removeRow(this)"> 
             </div> -->
            </div>
                <div id="save1" style="text-align:center;display:none">
             
                <button style="margin-top:10px;" type="submit" onclick="saveProfile(event)" class="btn btn-primary">Save</button>
                </div>
        </div>
                </form>
                            </div>
                            <div>
                                 <div class="col-sm-12" style="padding:0;margin-bottom: 25px">
                                     <div class="toolbar" style=" margin-left: 13px;">  
                    
                   
                                    <label class="control-label col-sm-1" style="font-weight:450;padding-top: 5px;padding-left:0" for="firstName">Select User:</label>
                                    <input  id="dropdownName" style="width:25.5%;margin-left: 88px;" data-placeholder="Name" />
                                    <!-- <%    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == 79) {  %>    <% } %>-->
                                    <button  style="float:right;"  id="button" type="button" class="btn btn-primary"  onclick="Editable()" >Edit</button> 
                                                                         
                                    </div>
                                 </div>
                                <div class="col-sm-12" style="padding:0;">
                
                                    <div class="col-sm-6  required " style="padding:0" >
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">First Name </label>
                <span id="ochange1" class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="ofirstname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="First Name" autocomplete="off" readonly ></span>
            </div>
            <div class="col-sm-6  required " style="padding:0" >
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="LastName">Last Name </label>
                <span id="ochange2"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="olastname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Last Name" autocomplete="off" readonly></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="Pan">PAN </label>
                <span id="ochange3"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="opan" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="PAN" autocomplete="off" readonly ></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="Mobile">Mobile </label>
                <span id="ochange4"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="omobile" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Mobile" autocomplete="off" readonly ></span>
            </div>
            
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="email">Company Email </label>
                <span id="ochange5" class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="oemail" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Email" autocomplete="off"  readonly></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="designation">Designation </label>
                <span id="ochange6"class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="odesignation" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Designation" autocomplete="off" readonly></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="department">Department </label>
                <span id="ochange7" class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="odepartment" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Department" autocomplete="off" readonly ></span>
            </div>
            
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="reportto">Report To </label>
                <span id="ochange8" class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="oreportto" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Report To" autocomplete="off"readonly ></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="nationality">Nationality </label>
                <span id="ochange9" class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="onationality" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Nationality" autocomplete="off" readonly></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Date of Joining </label>
                <div  class="col-sm-4" style="padding:0"><input id="odatepicker1" value="10/10/2011" title="odatepicker1" disabled /></div>
            </div>
            
            <div class="col-sm-6" style="padding:0;margin-top:5px;visibility:hidden;"> 
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="address">Address</label>
                 <span class="col-sm-8 k-widget k-textbox" style="width:30%" ><textarea id="oaddress1" style="width: 100%;" required data-required-msg="Please enter a text." data-max-msg="Enter value between 1 and 200" ></textarea></span>
            </div>
            <div class="col-sm-6" style="padding:0;margin-top:5px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="address">Address</label>
                <span id="ochange11"class="col-sm-8 k-widget k-textbox" style="width:50%" ><textarea id="oaddress" style="width: 100%;" required data-required-msg="Please enter a text." data-max-msg="Enter value between 1 and 200"  readonly></textarea></span>
            </div>
            
            
            
            <div class="col-sm-12" style="padding:0;margin-top:5px;">
                <label class="col-sm-2 control-label" style="font-weight:450;padding-top: 10px;" for="relation">Qualifications </label>
                <!--<input type="button" id="more_fields_qualification" value="+" onClick="add_fields_qualification()" />-->
                <div class="col-sm-4" style="padding-top: 10px;padding-left:0"><input type="button" id="omore_fields_qualification" value="+" onclick="oadd_fields_qualification()" disabled /></div>
            </div>
            <div class="col-sm-12" style="padding:0;margin-top:5px;display:inline-block;">
                <div class="col-sm-6" style="font-weight:450;">Qualification & Institution Details</div>
            </div>
            <div class="wrapper-comp-setting" id="oflashcard-list-qualification">
            <!-- <div class="col-sm-12 fc-item-qualification" style="padding:0;margin-top:5px;">
                <div class="col-sm-6" style="padding:0;margin-top:5px;padding-left:15px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="qualification1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Qualification" autocomplete="off" ></span>
                </div>
                <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)"> 
             </div> -->
            </div>
            <div class="col-sm-12" style="padding:0;margin-top:15px;">
                <label class="col-sm-2 control-label" style="font-weight:450;padding-top: 5px;" for="relation">Previous Employer</label>
                <div class="col-sm-4" style="padding-left:0"><input type="button" id="omore_fields_employee" value="+" onclick="oadd_fields_employee()" disabled/></div>
                
            </div>
            <div class="col-sm-12" style="padding:0;margin-top:0px;display:inline-block;">
                <div class="col-sm-2" style="font-weight:450;">Name</div>
                <div class="col-sm-2" style="font-weight:450;">From</div>
                <div class="col-sm-2" style="font-weight:450;">To</div>
            </div>
            <div class="wrapper-comp-setting" id="oflashcard-list-employee">
            <!--<div class="col-sm-12 fc-item-employee" style="padding:0;margin-top:5px;">
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:15px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="employer1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <input id="from1" value="10/10/2011" title="datepicker" style="width:100%" />
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <input id="to1" value="10/10/2011" title="datepicker" style="width:100%" />
                </div>
                <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)"> 
             </div> -->
            </div>
            
            <div class="col-sm-12" style="padding:0;margin-top:15px;">
                <label class="col-sm-2 control-label" style="font-weight:450;padding-top: 5px;" for="relation">Relations</label>
               <div class="col-sm-4" style="padding-left:0"><input type="button" id="omore_fields" value="+" onclick="oadd_fields()" disabled/></div>
                
            </div>
            <div class="col-sm-12" style="padding:0;margin-top:5px;display:inline-block">
                <div class="col-sm-2" style="font-weight:450;">Relation Type</div>
                <div class="col-sm-2" style="font-weight:450;">Name</div>
                <div class="col-sm-2" style="font-weight:450;">Dependant</div>
                <div class="col-sm-2" style="font-weight:450;">ID Type</div>
                <div class="col-sm-2" style="font-weight:450;">ID Code</div>
                
            </div>
            <div class="wrapper-comp-setting" id="oflashcard-list">
            <!--
                <div class="col-sm-12 fc-item" style="padding:0;margin-top:5px;">
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:15px;">
                    <input  id="relationDropdown1" style="width:100%" data-placeholder="Relation Type" >
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="relation1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <input  id="dependant1" style="width:100%" data-placeholder="Relation Type" >
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="idtype1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Type" autocomplete="off" ></span>
                </div>
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="idcode1" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>
                </div>
                <input style="margin-top:10px;margin-left:15px;" type="button" value="-" onclick="removeRow(this)"> 
             </div> -->
            </div>
                
        </div>
                               
  
        <div class="col-sm-12" id="saveEditProfile" style="padding:0;text-align: center;display:none">
            
                  
                    <button   id="button1" type="button" class="btn btn-primary"  onclick="UpdateProfile()" >Save</button> 
           
        </div>
                                </div>


                            </div>
                            
                        </div>
        </div>
    <div class="col-sm-12" >
        
    </div>
    <script>
        function Editable(){
            alert("If Details already shared with the same user then Name, Pan, Email can't be change.")
            var fname =document.getElementById("ofirstname").value;
            var lname =document.getElementById("olastname").value;
            $("#omore_fields_qualification").prop("disabled", false);
            $("#omore_fields_employee").prop("disabled", false);
            $("#omore_fields").prop("disabled", false);
           
            var concernname=fname+' '+lname
            //alert(concernname)
            var customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
            
            
            
             document.getElementById('saveEditProfile').style.display = "block";
             
             document.getElementById('omobile').removeAttribute('readonly');
             document.getElementById('odepartment').removeAttribute('readonly');
             document.getElementById('odesignation').removeAttribute('readonly');
             document.getElementById('onationality').removeAttribute('readonly');
            // $( "#odatepicker2" ). datepicker( "disabled", false );
             $("#odatepicker1").data('kendoDatePicker').enable(true);
            
             document.getElementById('omobile').removeAttribute('readonly');
             document.getElementById('oreportto').removeAttribute('readonly');
             document.getElementById('oaddress').removeAttribute('readonly');
            // $('#onationality').prop('contentEditable', 'true')
            //$('#odoj').prop('contentEditable', 'true')
             document.getElementById("ochange4").style.borderColor  = "green"
             document.getElementById("ochange6").style.borderColor  = "green"
             document.getElementById("ochange7").style.borderColor  = "green"
             document.getElementById("ochange8").style.borderColor  = "green"
             document.getElementById("ochange9").style.borderColor  = "green"
            
             document.getElementById("ochange11").style.borderColor  = "green"
            
            $.ajax({
                
                 type: 'post',
                 url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getedit/',
                 content: 'application/json;charset=utf-8',
                 processData: true,
                 data: { "ConcernedName": concernname,"customer_id":customer_id},
                 success: function (data) {
                    
                     if(data.msg == 'not exists'){
                         //alert('coming');
                         
                         document.getElementById('ofirstname').removeAttribute('readonly');
                         document.getElementById('olastname').removeAttribute('readonly');
                         document.getElementById('opan').removeAttribute('readonly');

                         document.getElementById("ochange1").style.borderColor  = "green"
                         document.getElementById("ochange2").style.borderColor  = "green"
                         document.getElementById("ochange3").style.borderColor  = "green"
                         
                         document.getElementById("ochange5").style.borderColor  = "red"
                        
                        
                         
                        
                        // document.getElementById("ofirstname").style.color = "#666"
                        
                     }
                     else{
                        // alert('not coming');
                        
                         document.getElementById("ochange1").style.borderColor  = "red"
                         document.getElementById("ochange2").style.borderColor  = "red"
                         document.getElementById("ochange3").style.borderColor  = "red"
                         document.getElementById("ochange5").style.borderColor  = "red"
                         
                         
                     }


                },
                error: function (e, t) { alert('something went wrong'); }
            })

             return false;
        }
        function EditableHD(){
            alert("If Details already shared with the same user then Name, Pan, Email can't be change.")
            var fname =document.getElementById("firstname").value;
            var lname =document.getElementById("lastname").value;
            $("#more_fields_qualification").prop("disabled", false);
            $("#more_fields_employee").prop("disabled", false);
            $("#more_fields").prop("disabled", false);
            var concernname=fname+' '+lname
            //alert(concernname)
            var customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
            
            
            
             document.getElementById('save1').style.display = "block";
             
             document.getElementById('mobile').removeAttribute('readonly');
             document.getElementById('department').removeAttribute('readonly');
             document.getElementById('designation').removeAttribute('readonly');
             document.getElementById('nationality').removeAttribute('readonly');
            // $( "#odatepicker2" ). datepicker( "disabled", false );
             $("#datepicker1").data('kendoDatePicker').enable(true);
           
             document.getElementById('reportto').removeAttribute('readonly');
             document.getElementById('address').removeAttribute('readonly');
            // $('#onationality').prop('contentEditable', 'true')
            //$('#odoj').prop('contentEditable', 'true')
             document.getElementById("change4").style.borderColor  = "green"
             document.getElementById("change6").style.borderColor  = "green"
             document.getElementById("change7").style.borderColor  = "green"
             document.getElementById("change8").style.borderColor  = "green"
             document.getElementById("change9").style.borderColor  = "green"
             //document.getElementById("change10").style.borderColor  = "green"
             document.getElementById("change11").style.borderColor  = "green"
            
            $.ajax({
                
                 type: 'post',
                 url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getedit/',
                 content: 'application/json;charset=utf-8',
                 processData: true,
                 data: { "ConcernedName": concernname,"customer_id":customer_id},
                 success: function (data) {
                    
                     if(data.msg == 'not exists'){
                         //alert('coming');
                         
                         document.getElementById('firstname').removeAttribute('readonly');
                         document.getElementById('lastname').removeAttribute('readonly');
                         document.getElementById('pan').removeAttribute('readonly');

                         document.getElementById("change1").style.borderColor  = "green"
                         document.getElementById("change2").style.borderColor  = "green"
                         document.getElementById("change3").style.borderColor  = "green"
                         
                         document.getElementById("change5").style.borderColor  = "red"
                        
                        
                         
                        
                        // document.getElementById("ofirstname").style.color = "#666"
                        
                     }
                     else{
                        // alert('not coming');
                        
                         document.getElementById("change1").style.borderColor  = "red"
                         document.getElementById("change2").style.borderColor  = "red"
                         document.getElementById("change3").style.borderColor  = "red"
                         document.getElementById("change5").style.borderColor  = "red"
                         
                         
                     }


                },
                error: function (e, t) { alert('something went wrong'); }
            })

             return false;
        }
          
        function UpdateProfile(){
           // e.preventDefault();
           // alert('SAVE')
            var user_id=$('#dropdownName').val();
            var qualificationList = [];
            var employerNameList = [];
            var employerFromList = [];
            var employerToList = [];
            var relationTypeList = [];
            var relationNameList = [];
            var relationDependantList = [];
            var relationIDTypeList = [];
            var relationIDCodeList = [];

            var fname =document.getElementById("ofirstname").value;
            var lname =document.getElementById("olastname").value;
            var designation =document.getElementById("odesignation").value;
            var department =document.getElementById("odepartment").value;
            var reportto = document.getElementById("oreportto").innerText;
            var qualificationList = document.getElementById("oreportto").value;
            var nationality =document.getElementById("onationality").value;
            var dateofjoining =$("#odatepicker1").val(); 
            var pan =document.getElementById("opan").value; 
             
            var mobile=document.getElementById("omobile").value;
            var address =document.getElementById("oaddress").value;
            // alert(mobile)

             $.ajax({
                
                 type: 'post',
                 url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>updateprofile/',
                 content: 'application/json;charset=utf-8',
                 processData: true,
                 data: { "user_id": user_id, "designation": designation, "pan": pan, "mobile": mobile, "department": department, "designation": designation, "reportto": reportto, "qualification": qualificationList, "nationality": nationality, "dateofjoining": dateofjoining,"employername":employerNameList,"employerfrom":employerFromList,"employerto":employerToList,"relationname":relationNameList,"relationTypeList":relationTypeList,"relationdependant":relationDependantList,"relationidtype":relationIDTypeList,"relationidcode":relationIDCodeList,"address":address,"fname":fname,"lname":lname },
                 success: function (data) {
                    //alert('coming');
                     alert('Successfully Edited');
                     document.getElementById("ochange4").style.borderColor  = "#dedee0"
                     document.getElementById("ochange6").style.borderColor  = "#dedee0"
                     document.getElementById("ochange7").style.borderColor  = "#dedee0"
                     document.getElementById("ochange8").style.borderColor  = "#dedee0"
                     document.getElementById("ochange9").style.borderColor  = "#dedee0"
                     document.getElementById("ochange1").style.borderColor  = "#dedee0"
                     document.getElementById("ochange2").style.borderColor  = "#dedee0"
                     document.getElementById("ochange3").style.borderColor  = "#dedee0"
                     document.getElementById("ochange5").style.borderColor  = "#dedee0"
                    
                     document.getElementById('omobile').setAttribute('readonly',true);
                     $("#odatepicker1").data('kendoDatePicker').enable(false);
            
                     document.getElementById('saveEditProfile').style.display = "none";
           

                     document.getElementById('ofirstname').setAttribute('readonly',true);
           
                     document.getElementById('olastname').setAttribute('readonly',true);
                     document.getElementById('oaddress').setAttribute('readonly',true);
                     document.getElementById('onationality').setAttribute('readonly',true);
                     document.getElementById('opan').setAttribute('readonly',true);
                     document.getElementById('oreportto').setAttribute('readonly',true);
                     document.getElementById('odepartment').setAttribute('readonly',true);
                     document.getElementById('odesignation').setAttribute('readonly',true);
                     document.getElementById('onationality').setAttribute('readonly',true);
            
                     document.getElementById("ochange11").style.borderColor  = "#dedee0"


                },
                error: function (e, t) { alert('something went wrong'); }
            })

             return false;
        }
    </script>
    <script>
        $(document).ready(function () {
            // create DatePicker from input HTML element
            $("#tabstrip").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                }
            });
            $("#datepicker1").kendoDatePicker();
            $("#odatepicker1").kendoDatePicker();

            $("#from1").kendoDatePicker();
            $("#to1").kendoDatePicker();
            $("#pagetype").text("My Profile");
            getProfile(<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>);
        });
       </script>

    <script>
        //var addFieldButton = document.getElementById("more_fields");
        var flashCardList = document.getElementById('flashcard-list');
        var fc_number = 1;

        //var addFieldButtonEmployee = document.getElementById("more_fields_employee");
        var flashCardListEmployee = document.getElementById('flashcard-list-employee');
        var fc_number_employee = 1;

        // var addFieldButtonQualification = document.getElementById("more_fields_qualification");
        var flashCardListQualification = document.getElementById('flashcard-list-qualification');
        var fc_number_qualification = 1;

        function add_fields() {
            fc_number++;
            var fields = '<div class="col-sm-12 fc-item" style="padding:0;margin-top:5px;"><div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:15px;">\n\
                <input  id = "relationDropdown'+fc_number+'" style = "width:100%" data-placeholder="Relation Type" >\n\
                </div >\n\
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="relation'+ fc_number +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>\n\
                </div>\n\
                    <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                        <input id="dependant'+ fc_number +'" style="width:100%" data-placeholder="Relation Type" >\n\
                </div>\n\
                        <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                            <span class=" k-widget k-textbox" style="width: 100%;"><input id="idtype'+ fc_number +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Type" autocomplete="off" ></span>\n\
                </div>\n\
                            <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                                <span class=" k-widget k-textbox" style="width: 100%;"><input id="idcode'+ fc_number +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>\n\
                </div>\n\
                <input style="margin-top:10px;margin-left:15px;" type="button" value="-" onclick="removeRow(this)"></div>';
            flashCardList.insertAdjacentHTML("beforeend", fields);
            $("#relationDropdown"+fc_number+"").kendoDropDownList({
                placeholder: "",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Father", value: "Father" },
                    { text: "Mother", value: "Mother" },
                    { text: "Spouse", value: "Spouse" },
                    { text: "Brother", value: "Brother" },
                    { text: "Sister", value: "Sister" },
                    { text: "Son", value: "Son" },
                    { text: "Daughter", value: "Daughter" },
                    { text: "Material Financial Relationship", value: "Material Financial Relationship" }
                ]
            });
            $("#dependant" + fc_number + "").kendoDropDownList({
                placeholder: "",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Yes", value: "Yes" },
                    { text: "No", value: "No" },
                    { text: "Not Applicable", value: "Not Applicable" }
                ]
            });
        }
        function add_fields_employee() {
            fc_number_employee++;
            var fieldsemployee = '<div class="col-sm-12 fc-item-employee" style="padding:0;margin-top:5px;">\n\
                <div class="col-sm-2" style = "padding:0;margin-top:5px;padding-left:15px;" >\n\
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="employer'+ fc_number_employee +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>\n\
                </div>\n\
                    <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                        <input id="from'+fc_number_employee+'" value="10/10/2011" title="datepicker" style="width:100%" />\n\
                </div>\n\
                        <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                            <input id="to'+ fc_number_employee +'" value="10/10/2011" title="datepicker" style="width:100%" />\n\
                </div>\n\
                            <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)">\n\
             </div>';
            flashCardListEmployee.insertAdjacentHTML("beforeend", fieldsemployee);
            $("#from" + fc_number_employee + "").kendoDatePicker();
            $("#to" + fc_number_employee + "").kendoDatePicker();
        }
        function add_fields_qualification() {
            fc_number_qualification++;
            var fieldsqualification = '<div class="wrapper-comp-setting" id="flashcard-list-qualification">\n\
                <div class="col-sm-12 fc-item-qualification" style = "padding:0;margin-top:5px;" >\n\
                    <div class="col-sm-6" style="padding:0;margin-top:5px;padding-left:15px;">\n\
                        <span class=" k-widget k-textbox" style="width: 100%;"><input id="qualification'+ fc_number_qualification +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Qualification" autocomplete="off" ></span>\n\
                </div>\n\
                        <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)">\n\
             </div>\n\
                    </div>';
            flashCardListQualification.insertAdjacentHTML("beforeend", fieldsqualification);
        }
        
        //addFieldButtonQualification.addEventListener("click", add_fields_qualification);
       // addFieldButtonEmployee.addEventListener("click", add_fields_employee);
        //addFieldButton.addEventListener("click", add_fields);
        
        function removeRow(input) {
                input.parentNode.remove()
        }

    </script>
    
     <script>
        //var addFieldButton = document.getElementById("more_fields");
        var oflashCardList = document.getElementById('oflashcard-list');
        var ofc_number = 1;

        //var addFieldButtonEmployee = document.getElementById("more_fields_employee");
        var oflashCardListEmployee = document.getElementById('oflashcard-list-employee');
        var ofc_number_employee = 1;

        // var addFieldButtonQualification = document.getElementById("more_fields_qualification");
        var oflashCardListQualification = document.getElementById('oflashcard-list-qualification');
        var ofc_number_qualification = 1;

        function oadd_fields() {
            ofc_number++;
            var fields = '<div class="col-sm-12 fc-item" style="padding:0;margin-top:5px;"><div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:15px;">\n\
                <input  id = "orelationDropdown'+ofc_number+'" style = "width:100%" data-placeholder="Relation Type" >\n\
                </div >\n\
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="orelation'+ ofc_number +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>\n\
                </div>\n\
                    <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                        <input id="odependant1'+ ofc_number +'" style="width:100%" data-placeholder="Relation Type" >\n\
                </div>\n\
                        <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                            <span class=" k-widget k-textbox" style="width: 100%;"><input id="oidtype'+ ofc_number +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Type" autocomplete="off" ></span>\n\
                </div>\n\
                            <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                                <span class=" k-widget k-textbox" style="width: 100%;"><input id="oidcode'+ ofc_number +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>\n\
                </div>\n\
                <input style="margin-top:10px;margin-left:15px;" type="button" value="-" onclick="removeRow(this)"></div>';
            oflashCardList.insertAdjacentHTML("beforeend", fields);
            $("#orelationDropdown"+ofc_number+"").kendoDropDownList({
                placeholder: "",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Father", value: "Father" },
                    { text: "Mother", value: "Mother" },
                    { text: "Spouse", value: "Spouse" },
                    { text: "Brother", value: "Brother" },
                    { text: "Sister", value: "Sister" },
                    { text: "Son", value: "Son" },
                    { text: "Daughter", value: "Daughter" },
                    { text: "Material Financial Relationship", value: "Material Financial Relationship" }
                ]
            });
            $("#odependant1" + ofc_number + "").kendoDropDownList({
                placeholder: "",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Yes", value: "Yes" },
                    { text: "No", value: "No" },
                    { text: "Not Applicable", value: "Not Applicable" }
                ]
            });
        }
        function oadd_fields_employee() {
            ofc_number_employee++;
            var ofieldsemployee = '<div class="col-sm-12 fc-item-employee" style="padding:0;margin-top:5px;">\n\
                <div class="col-sm-2" style = "padding:0;margin-top:5px;padding-left:15px;" >\n\
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="oemployer'+ ofc_number_employee +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>\n\
                </div>\n\
                    <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                        <input id="ofrom'+ofc_number_employee+'" value="10/10/2011" title="datepicker" style="width:100%" />\n\
                </div>\n\
                        <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                            <input id="oto'+ ofc_number_employee +'" value="10/10/2011" title="datepicker" style="width:100%" />\n\
                </div>\n\
                            <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="oremoveRow(this)">\n\
             </div>';
            oflashCardListEmployee.insertAdjacentHTML("beforeend", ofieldsemployee);
            $("#ofrom" + ofc_number_employee + "").kendoDatePicker();
            $("#oto" + ofc_number_employee + "").kendoDatePicker();
        }
        function oadd_fields_qualification() {
            ofc_number_qualification++;
            var ofieldsqualification = '<div class="wrapper-comp-setting" id="flashcard-list-qualification">\n\
                <div class="col-sm-12 fc-item-qualification" style = "padding:0;margin-top:5px;" >\n\
                    <div class="col-sm-6" style="padding:0;margin-top:5px;padding-left:15px;">\n\
                        <span class=" k-widget k-textbox" style="width: 100%;"><input id="oqualification'+ ofc_number_qualification +'" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Qualification" autocomplete="off" ></span>\n\
                </div>\n\
                        <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)">\n\
             </div>\n\
                    </div>';
            oflashCardListQualification.insertAdjacentHTML("beforeend", ofieldsqualification);
        }
        
        //addFieldButtonQualification.addEventListener("click", add_fields_qualification);
       // addFieldButtonEmployee.addEventListener("click", add_fields_employee);
        //addFieldButton.addEventListener("click", add_fields);
        
        function oremoveRow(input) {
                input.parentNode.remove()
        }

    </script>
    
    <script>
        function getProfile(user_id) {
            $.ajax({
                type: 'Get',
                url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>profile/?user_id=' + user_id,
                content: 'application/json;charset=utf-8',
                processData: true,
                success: function (result) {
                    if (result.data != 'profile not exists') {
                        //alert(JSON.stringify(result.data))
                        $('#designation').val(result.data['Designation']);
                        $('#department').val(result.data['Department']);
                        $('#reportto').val(result.data['ReportTo']);
                        $('#qualification').val(result.data['Qualification']);
                        $('#nationality').val(result.data['Nationality']);
                        $('#email').val(result.data['email']);
                        $('#mobile').val(result.data['mobile']);
                        $('#firstname').val(result.data['firstname']);
                        $('#lastname').val(result.data['lastname']);
                        $('#pan').val(result.data['pan']);
                        $('#address').val(result.data['address']);
                        $("#datepicker1").kendoDatePicker({
                            value: result.data['DateOfJoining']
                            
                        });
                        var qualx = result.data['qual']; 
                        if (qualx.length > 0) {
                            get_fields_qualification(qualx);
                        }
                        var empx = result.data['employername'];
                        var empfrom = result.data['employerfrom'];
                        var empto = result.data['employerto'];
                        if (empx.length > 0) {
                            get_fields_employer(empx,empfrom,empto);
                        }
                        var relx = result.data['relationname'];
                        var reltype = result.data['relationTypeList'];
                        var reldependant = result.data['relationdependant'];
                        var relidtype = result.data['relationidtype'];
                        var relidcode = result.data['relationidcode'];
                        if (relx.length > 0) {
                            get_fields_relation(relx, reltype, reldependant, relidtype, relidcode);
                        }
                        //  $('#dateofjoining').val(result.data['DateOfJoining']);
                    } else {

                    }

                },
                error: function (e, t) { }
            })

           
        }
    </script>
    <script>
        $("#relationDropdown1").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Father", value: "Father" },
                { text: "Mother", value: "Mother" },
                { text: "Spouse", value: "Spouse" },
                { text: "Brother", value: "Brother" },
                { text: "Sister", value: "Sister" },
                { text: "Son", value: "Son" },
                { text: "Daughter", value: "Daughter" },
                { text: "Material Financial Relationship", value: "Material Financial Relationship" }
            ]
        });
        $("#dependant1").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Yes", value: "Yes" },
                { text: "No", value: "No" },
                { text: "Not Applicable", value: "Not Applicable" }
            ]
        });

    </script>
    <script>
         function saveProfile(e) {
             e.preventDefault();
             //alert('coming1');
             var qualificationList = [];
             var employerNameList = [];
             var employerFromList = [];
             var employerToList = [];
             var relationTypeList = [];
             var relationNameList = [];
             var relationDependantList = [];
             var relationIDTypeList = [];
             var relationIDCodeList = [];
             for (var i = 1; i <= fc_number_qualification; i++) {
                 var tx = $("#qualification" + i + "").val();
                 if (tx != undefined) {
                     qualificationList.push(tx);
                 }

             }
             for (var i = 1; i <= fc_number_employee; i++) {
                 var tx1 = $("#employer" + i + "").val();
                 if (tx1 != undefined) {
                     employerNameList.push(tx1);
                     employerFromList.push($("#from" + i + "").val());
                     employerToList.push($("#to" + i + "").val());
                 }

             }
             for (var i = 1; i <= fc_number; i++) {
                 var tx2 = $("#relation" + i + "").val();
                 if (tx2 != undefined) {
                     relationNameList.push(tx2);
                     relationTypeList.push($("#relationDropdown" + i + "").val());
                     relationDependantList.push($("#dependant" + i + "").val());
                     relationIDTypeList.push($("#idtype" + i + "").val());
                     relationIDCodeList.push($("#idcode" + i + "").val());
                 }

             }
             //alert(JSON.stringify(qualificationList));
             var user_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
             var designation = $('#designation').val();
             var department = $('#department').val();
             var reportto = $('#reportto').val();
             //var qualification = $('#qualification').val();
             var nationality = $('#nationality').val();
             var dateofjoining = $('#datepicker1').val();
             var pan = $('#pan').val();
             var mobile = $('#mobile').val();
             var address = $('#address').val();
             $.ajax({
                 type: 'post',
                 url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>createprofile/',
                 content: 'application/json;charset=utf-8',
                 processData: true,
                 data: { "user_id": user_id, "designation": designation, "pan": pan, "mobile": mobile, "department": department, "designation": designation, "reportto": reportto, "qualification": qualificationList, "nationality": nationality, "dateofjoining": dateofjoining,"employername":employerNameList,"employerfrom":employerFromList,"employerto":employerToList,"relationname":relationNameList,"relationTypeList":relationTypeList,"relationdependant":relationDependantList,"relationidtype":relationIDTypeList,"relationidcode":relationIDCodeList,"address":address },
                 success: function (data) {
                    //alert('coming');
                    alert('Successfully Edited');

                    document.getElementById("change4").style.borderColor  = "#dedee0"
                    document.getElementById("change6").style.borderColor  = "#dedee0"
                    document.getElementById("change7").style.borderColor  = "#dedee0"
                    document.getElementById("change8").style.borderColor  = "#dedee0"
                    document.getElementById("change9").style.borderColor  = "#dedee0"
                    document.getElementById("change1").style.borderColor  = "#dedee0"
                    document.getElementById("change2").style.borderColor  = "#dedee0"
                    document.getElementById("change3").style.borderColor  = "#dedee0"
                    document.getElementById("change5").style.borderColor  = "#dedee0"
                    
                    document.getElementById('mobile').setAttribute('readonly',true);
                    $("#datepicker1").data('kendoDatePicker').enable(false);
            
                    document.getElementById('save1').style.display = "none";
           

                    document.getElementById('firstname').setAttribute('readonly',true);
           
                    document.getElementById('lastname').setAttribute('readonly',true);
                    document.getElementById('address').setAttribute('readonly',true);
                    document.getElementById('nationality').setAttribute('readonly',true);
                    document.getElementById('pan').setAttribute('readonly',true);
                    document.getElementById('reportto').setAttribute('readonly',true);
                    document.getElementById('department').setAttribute('readonly',true);
                    document.getElementById('designation').setAttribute('readonly',true);
                    document.getElementById('nationality').setAttribute('readonly',true);

                    $("#more_fields_qualification").prop("disabled", true);
                    $("#more_fields_employee").prop("disabled", true);
                    $("#more_fields").prop("disabled", true);

                },
                error: function (e, t) { alert('something went wrong'); }
            })

             return false;
         }
    </script>
     <script>
        function oget_fields_qualification(oqualx) {
            var     ofieldsqualification = '';
            ofc_number_qualification = oqualx.length;
            for (var i = 0; i < oqualx.length; i++) {
                var cq = i + 1;
                ofieldsqualification = '<div class="wrapper-comp-setting" id="oflashcard-list-qualification">\n\
                <div class="col-sm-12 fc-item-qualification" style = "padding:0;margin-top:5px;" >\n\
                    <div class="col-sm-6" style="padding:0;margin-top:5px;padding-left:15px;">\n\
                        <span class=" k-widget k-textbox" style="width: 100%;"><input id="oqualification'+ cq + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Qualification" autocomplete="off" ></span>\n\
                </div>\n\
                        <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)">\n\
             </div><div>';
                oflashCardListQualification.insertAdjacentHTML("beforeend", ofieldsqualification);

                $("#oqualification" + cq).val(oqualx[i])
            }
            
        }
        function oget_fields_employer(oempx, empfrom, empto) {
            var ofieldsemp = '';
            ofc_number_employee = oempx.length;
            for (var i = 0; i < oempx.length; i++) {
                var ce = i + 1;
                ofieldsemp = '<div class="col-sm-12 fc-item-employee" style="padding:0;margin-top:5px;">\n\
                <div class="col-sm-2" style = "padding:0;margin-top:5px;padding-left:15px;" >\n\
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="oemployer'+ ce + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>\n\
                </div>\n\
                    <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                        <input id="ofrom'+ ce + '" value="10/10/2011" title="datepicker" style="width:100%" />\n\
                </div>\n\
                        <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                            <input id="oto'+ ce + '" value="10/10/2011" title="datepicker" style="width:100%" />\n\
                </div>\n\
                            <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)">\n\
             </div>';
                oflashCardListEmployee.insertAdjacentHTML("beforeend", ofieldsemp);
                $("#oemployer" + ce).val(oempx[i]);
                $("#ofrom" + ce + "").kendoDatePicker();
                $("#oto" + ce + "").kendoDatePicker();
                $("#ofrom" + ce).data("kendoDatePicker").value(empfrom[i]);
                $("#oto" + ce).data("kendoDatePicker").value(empto[i]);
            }

        }
        function oget_fields_relation(orelx, reltype, reldependant, relidtype, relidcode) {
            var ofieldsrelation = '';
            ofc_number = orelx.length;
            for (var i = 0; i < orelx.length; i++) {
                var cr = i + 1;
                ofieldsrelation = '<div class="col-sm-12 fc-item" style="padding:0;margin-top:5px;"><div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:15px;">\n\
                <input  id = "orelationDropdown'+ cr + '" style = "width:100%" data-placeholder="Relation Type" >\n\
                </div >\n\
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="orelation'+ cr + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>\n\
                </div>\n\
                    <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                        <input id="odependant'+ cr + '" style="width:100%" data-placeholder="Relation Type" >\n\
                </div>\n\
                        <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                            <span class=" k-widget k-textbox" style="width: 100%;"><input id="oidtype'+ cr + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Type" autocomplete="off" ></span>\n\
                </div>\n\
                            <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                                <span class=" k-widget k-textbox" style="width: 100%;"><input id="oidcode'+ cr + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>\n\
                </div>\n\
                <input style="margin-top:10px;margin-left:15px;" type="button" value="-" onclick="removeRow(this)"></div>';
                oflashCardList.insertAdjacentHTML("beforeend", ofieldsrelation);
                $("#orelationDropdown" + cr + "").kendoDropDownList({
                    placeholder: "",
                    dataTextField: "text",
                    dataValueField: "value",
                    checkboxes: true,
                    checkAll: true,
                    autoClose: true,
                    dataSource: [
                        { text: "Father", value: "Father" },
                        { text: "Mother", value: "Mother" },
                        { text: "Spouse", value: "Spouse" },
                        { text: "Brother", value: "Brother" },
                        { text: "Sister", value: "Sister" },
                        { text: "Son", value: "Son" },
                        { text: "Daughter", value: "Daughter" },
                        { text: "Material Financial Relationship", value: "Material Financial Relationship" }
                    ]
                });
                $("#odependant" + cr + "").kendoDropDownList({
                    placeholder: "",
                    dataTextField: "text",
                    dataValueField: "value",
                    checkboxes: true,
                    checkAll: true,
                    autoClose: true,
                    dataSource: [
                        { text: "Yes", value: "Yes" },
                        { text: "No", value: "No" },
                        { text: "Not Applicable", value: "Not Applicable" }
                    ]
                });
                
                $("#orelationDropdown" + cr).data("kendoDropDownList").value(reltype[i]);
                $("#orelation" + cr).val(relx[i]);
                $("#odependant" + cr).data("kendoDropDownList").value(reldependant[i]);
                $("#oidtype" + cr).val(relidtype[i]);
                $("#oidcode" + cr).val(relidcode[i]);
            }

        }
    </script>
    <script>
        function get_fields_qualification(qualx) {
            var fieldsqualification = '';
            fc_number_qualification = qualx.length;
            for (var i = 0; i < qualx.length; i++) {
                var cq = i + 1;
                fieldsqualification = '<div class="wrapper-comp-setting" id="flashcard-list-qualification">\n\
                <div class="col-sm-12 fc-item-qualification" style = "padding:0;margin-top:5px;" >\n\
                    <div class="col-sm-6" style="padding:0;margin-top:5px;padding-left:15px;">\n\
                        <span class=" k-widget k-textbox" style="width: 100%;"><input id="qualification'+ cq + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Qualification" autocomplete="off" ></span>\n\
                </div>\n\
                        <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)">\n\
             </div><div>';
                flashCardListQualification.insertAdjacentHTML("beforeend", fieldsqualification);

                $("#qualification" + cq).val(qualx[i])
            }
            
        }
        function get_fields_employer(empx, empfrom, empto) {
            var fieldsemp = '';
            fc_number_employee = empx.length;
            for (var i = 0; i < empx.length; i++) {
                var ce = i + 1;
                fieldsemp = '<div class="col-sm-12 fc-item-employee" style="padding:0;margin-top:5px;">\n\
                <div class="col-sm-2" style = "padding:0;margin-top:5px;padding-left:15px;" >\n\
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="employer'+ ce + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>\n\
                </div>\n\
                    <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                        <input id="from'+ ce + '" value="10/10/2011" title="datepicker" style="width:100%" />\n\
                </div>\n\
                        <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                            <input id="to'+ ce + '" value="10/10/2011" title="datepicker" style="width:100%" />\n\
                </div>\n\
                            <input style="margin-top:10px;margin-left:15px" type="button" value="-" onclick="removeRow(this)">\n\
             </div>';
                flashCardListEmployee.insertAdjacentHTML("beforeend", fieldsemp);
                $("#employer" + ce).val(empx[i]);
                $("#from" + ce + "").kendoDatePicker();
                $("#to" + ce + "").kendoDatePicker();
                $("#from" + ce).data("kendoDatePicker").value(empfrom[i]);
                $("#to" + ce).data("kendoDatePicker").value(empto[i]);
            }

        }
        function get_fields_relation(relx, reltype, reldependant, relidtype, relidcode) {
            var fieldsrelation = '';
            fc_number = relx.length;
            for (var i = 0; i < relx.length; i++) {
                var cr = i + 1;
                fieldsrelation = '<div class="col-sm-12 fc-item" style="padding:0;margin-top:5px;"><div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:15px;">\n\
                <input  id = "relationDropdown'+ cr + '" style = "width:100%" data-placeholder="Relation Type" >\n\
                </div >\n\
                <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                    <span class=" k-widget k-textbox" style="width: 100%;"><input id="relation'+ cr + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>\n\
                </div>\n\
                    <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                        <input id="dependant'+ cr + '" style="width:100%" data-placeholder="Relation Type" >\n\
                </div>\n\
                        <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                            <span class=" k-widget k-textbox" style="width: 100%;"><input id="idtype'+ cr + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Type" autocomplete="off" ></span>\n\
                </div>\n\
                            <div class="col-sm-2" style="padding:0;margin-top:5px;padding-left:30px;">\n\
                                <span class=" k-widget k-textbox" style="width: 100%;"><input id="idcode'+ cr + '" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>\n\
                </div>\n\
                <input style="margin-top:10px;margin-left:15px;" type="button" value="-" onclick="removeRow(this)"></div>';
                flashCardList.insertAdjacentHTML("beforeend", fieldsrelation);
                $("#relationDropdown" + cr + "").kendoDropDownList({
                    placeholder: "",
                    dataTextField: "text",
                    dataValueField: "value",
                    checkboxes: true,
                    checkAll: true,
                    autoClose: true,
                    dataSource: [
                        { text: "Father", value: "Father" },
                        { text: "Mother", value: "Mother" },
                        { text: "Spouse", value: "Spouse" },
                        { text: "Brother", value: "Brother" },
                        { text: "Sister", value: "Sister" },
                        { text: "Son", value: "Son" },
                        { text: "Daughter", value: "Daughter" },
                        { text: "Material Financial Relationship", value: "Material Financial Relationship" }
                    ]
                });
                $("#dependant" + cr + "").kendoDropDownList({
                    placeholder: "",
                    dataTextField: "text",
                    dataValueField: "value",
                    checkboxes: true,
                    checkAll: true,
                    autoClose: true,
                    dataSource: [
                        { text: "Yes", value: "Yes" },
                        { text: "No", value: "No" },
                        { text: "Not Applicable", value: "Not Applicable" }
                    ]
                });
                
                $("#relationDropdown" + cr).data("kendoDropDownList").value(reltype[i]);
                $("#relation" + cr).val(relx[i]);
                $("#dependant" + cr).data("kendoDropDownList").value(reldependant[i]);
                $("#idtype" + cr).val(relidtype[i]);
                $("#idcode" + cr).val(relidcode[i]);
            }

        }
    </script>
    <script>
        $("#dropdownName").kendoDropDownList({
            dataTextField: "Name",
            dataValueField: "ID",
            dataSource: {
                transport: {
                    read: {
                        url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getuserpList/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>&user_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>",
                     }
                },
                requestEnd: function (e) {
                    e.response.unshift({ Name: "Select User", ID: "-1" });
                } 
            },
            //dataBound: onDataBoundE,
            
            change: function (e) {
               // alert(e.sender.value());
                document.getElementById('saveEditProfile').style.display = "none";
                
                if (e.sender.value() == "-1") {
                    $('#odesignation').text('');
                    $('#odepartment').text('');
                    $('#oreportto').text('');
                    //$('#qualification').val(result.data['Qualification']);
                    $('#onationality').text('');
                    $('#oemail').text('');
                    $('#omobile').text('');
                    $('#ofirstname').text('');
                    $('#olastname').text('');
                    $('#opan').text('');
                    $('#oaddress').text('');
                    $("#odoj").text('');
                    $("#tqual tbody").empty();
                    $("#temployer tr:gt(1)").empty();
                    $("#trelation tr:gt(1)").remove();
                } else {
                   // alert('else')
                    $("#tqual tbody").empty();
                    $("#temployer tr:gt(1)").empty();
                    $("#trelation tr:gt(1)").remove();
                   
                    getOtherProfile(e.sender.value());
                }
                
            }
        });
        /*
        $("#dropdownName").getKendoDropDownList().dataSource.add({
            Name: "Select User",
            ID: "-1"
        });
        */
    </script>
    
    <script>
        function getOtherProfile(user_id) {
            // alert('user_id',user_id)
            //$('#omobile').prop('readonly', false);

            var qualificationList = [];
            var employerNameList = [];
            var employerFromList = [];
            var employerToList = [];
            var relationTypeList = [];
            var relationNameList = [];
            var relationDependantList = [];
            var relationIDTypeList = [];
            var relationIDCodeList = [];
            for (var i = 1; i <= ofc_number_qualification; i++) {
                var tx = $("#oqualification" + i + "").val();
                if (tx != undefined) {
                    qualificationList.push(tx);
                }

            }
            for (var i = 1; i <= ofc_number_employee; i++) {
                var tx1 = $("#oemployer" + i + "").val();
                if (tx1 != undefined) {
                    employerNameList.push(tx1);
                    employerFromList.push($("#from" + i + "").val());
                    employerToList.push($("#to" + i + "").val());
                }

            }
            for (var i = 1; i <= ofc_number; i++) {
                var tx2 = $("#orelation" + i + "").val();
                if (tx2 != undefined) {
                    relationNameList.push(tx2);
                    relationTypeList.push($("#orelationDropdown" + i + "").val());
                    relationDependantList.push($("#odependant" + i + "").val());
                    relationIDTypeList.push($("#oidtype" + i + "").val());
                    relationIDCodeList.push($("#oidcode" + i + "").val());
                }

            }

            document.getElementById('omobile').setAttribute('readonly',true);
             $("#odatepicker1").data('kendoDatePicker').enable(false);
            
            
           

            document.getElementById('ofirstname').setAttribute('readonly',true);
           
            document.getElementById('olastname').setAttribute('readonly',true);
            document.getElementById('oaddress').setAttribute('readonly',true);
            document.getElementById('onationality').setAttribute('readonly',true);
            document.getElementById('opan').setAttribute('readonly',true);
            document.getElementById('oreportto').setAttribute('readonly',true);
            document.getElementById('odepartment').setAttribute('readonly',true);
            document.getElementById('odesignation').setAttribute('readonly',true);
            document.getElementById('onationality').setAttribute('readonly',true);
           // $( "#odatepicker2" ). datepicker( "disabled", true );
            
           
            // $('#onationality').prop('contentEditable', 'true')
            //$('#odoj').prop('contentEditable', 'true')
            document.getElementById("ochange4").style.borderColor  = "#dedee0"
            document.getElementById("ochange6").style.borderColor  = "#dedee0"
            document.getElementById("ochange7").style.borderColor  = "#dedee0"
            document.getElementById("ochange8").style.borderColor  = "#dedee0"
            document.getElementById("ochange9").style.borderColor  = "#dedee0"
            document.getElementById("ochange1").style.borderColor  = "#dedee0"
            document.getElementById("ochange2").style.borderColor  = "#dedee0"
            document.getElementById("ochange3").style.borderColor  = "#dedee0"
            document.getElementById("ochange5").style.borderColor  = "#dedee0"
            
            
            document.getElementById("ochange11").style.borderColor  = "#dedee0"
            $.ajax({
                type: 'Get',
                url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>profile/?user_id=' + user_id,
                content: 'application/json;charset=utf-8',
                processData: true,
                success: function (result) {
                    if (result.data != 'profile not exists') {
                        //alert(JSON.stringify(result.data))
                        $('#odesignation').val(result.data['Designation']);
                        $('#odepartment').val(result.data['Department']);
                        $('#oreportto').val(result.data['ReportTo']);
                        $('#oqualification').val(result.data['Qualification']);
                        $('#onationality').val(result.data['Nationality']);
                        $('#oemail').val(result.data['email']);
                        $('#omobile').val(result.data['mobile']);
                        $('#ofirstname').val(result.data['firstname']);
                        $('#olastname').val(result.data['lastname']);
                        $('#opan').val(result.data['pan']);
                        $('#oaddress').val(result.data['address']);
                        $("#odatepicker1").kendoDatePicker({
                            value: result.data['DateOfJoining']
                            
                        });
                        var qualx = result.data['qual']; 
                        if (qualx.length > 0) {
                            oget_fields_qualification(qualx);
                        }
                        var empx = result.data['oemployername'];
                        var empfrom = result.data['oemployerfrom'];
                        var empto = result.data['oemployerto'];
                        if (empx.length > 0) {
                            oget_fields_employer(empx,empfrom,empto);
                        }
                        var relx = result.data['orelationname'];
                        var reltype = result.data['orelationTypeList'];
                        var reldependant = result.data['orelationdependant'];
                        var relidtype = result.data['orelationidtype'];
                        var relidcode = result.data['orelationidcode'];
                        if (relx.length > 0) {
                            oget_fields_relation(relx, reltype, reldependant, relidtype, relidcode);
                        }
                        
                       
                        
                        //  $('#dateofjoining').val(result.data['DateOfJoining']);
                    } else {

                    }

                },
                error: function (e, t) { }
            })


        }
    </script>
    <script>
        function get_fields_otherqualification(qualx) {
            for (var i = 0; i < qualx.length; i++) {
                $("#tqual").append('<tr><td> ' + qualx[i]+'</td></tr>');
            }
            
        }
        function get_fields_otheremployer(empx, empfrom, empto) {
            for (var i = 0; i < empx.length; i++) {
                $("#temployer").append('<tr><td> ' + empx[i] + '</td><td> ' + empfrom[i] + '</td><td> ' + empto[i] + '</td></tr>');
            }
        }
        function get_fields_otherrelation(relx, reltype, reldependant, relidtype, relidcode) {
            for (var i = 0; i < relx.length; i++) {
                $("#trelation").append('<tr><td> ' + reltype[i] + '</td><td> ' + relx[i] + '</td><td> ' + reldependant[i] + '</td><td> ' + relidtype[i] + '</td><td> ' + relidcode[i] + '</td></tr>');
            }
        }
    </script>

</asp:Content>
