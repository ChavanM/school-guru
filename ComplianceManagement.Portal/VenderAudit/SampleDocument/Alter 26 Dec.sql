
--table AuditCheklist -> AuditChecklist

--table AuditCheklistComplianceMapping - Filed-> CheacklistID

--table AuditCheklistComplianceMapping -> AuditChecklistComplianceMapping
------------------------------------------------------------------------------


Create PROCEDURE Sp_GetAuditCheckListCompliance
	-- Add the parameters for the stored procedure here
	@checklistid bigint,
	@Flag varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @Flag='S'
		Begin
			select
				ACCM.ChecklistID,C.ID as TaskID,
				replace(replace(replace(replace(replace(replace(C.ShortDescription,char(9),' '),char(34),' '),char(13),' '),char(13),' '),char(20),' '),CHAR(10), '')  as ShortDescription ,
				replace(replace(replace(replace(replace(replace(C.Description,char(9),' '),char(34),' '),char(13),' '),char(13),' '),char(20),' '),CHAR(10), '')  as Description 
			from 
				AuditCheklistComplianceMapping ACCM
				inner join Compliance C
				on ACCM.ComplianceID=c.ID
			where 
				ACCM.IsActive=0 
				AND ACCM.ComplianceType='S'
				AND C.IsDeleted=0 
				AND C.Status is null
				AND ACCM.CheacklistID=@checklistid
		End
     else 
	   Begin
	   select * from InternalCompliance
	         select
				ACCM.ChecklistID,IC.ID as TaskID,
				replace(replace(replace(replace(replace(replace(IC.IShortDescription,char(9),' '),char(34),' '),char(13),' '),char(13),' '),char(20),' '),CHAR(10), '')  as ShortDescription ,
				'' as Description 
			from 
				AuditCheklistComplianceMapping ACCM
				inner join InternalCompliance IC
				on ACCM.ComplianceID=IC.ID
			where 
				ACCM.IsActive=0 
				AND ACCM.ComplianceType='I'
				AND IC.IsDeleted=0 				
				AND ACCM.CheacklistID=@checklistid
	   End

END
GO