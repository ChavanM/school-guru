﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages
{
    public partial class MgmtAuditMyReportNew : System.Web.UI.Page
    {
        //static bool ReviewerFlag;
        protected static List<Int32> roles;
        protected string Reviewername;
        protected string Performername;
        public static string CompDocReviewPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BindLocationFilter();
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    btnSearch_Click(sender, e);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        protected void upDownloadList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<int> GetAuditorAssignedLocationList(int UserID, int custID)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditDetails
                             where row.CustomerID == custID
                             select row).Distinct().ToList();

                if (query != null)
                    LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();

                return LocationList;
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);

                var LocationList = GetAuditorAssignedLocationList(AuthenticationHelper.UserID, customerID);
                //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviewerComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    TextBox EscDays = (TextBox)e.Row.FindControl("txtEscDays");
                    TextBox IntreimDays = (TextBox)e.Row.FindControl("txtInterimDays");
                    Button btnAdd = (Button)e.Row.FindControl("btnAdd");

                    if (EscDays.Text != "" && IntreimDays.Text != "")
                    {
                        btnAdd.Text = "Update";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviewerComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FillComplianceDocuments();
        }

        private void BindAuditCheckList(long AuditDetailID)
        {
            try
            {
                grdChecklist.DataSource = null;
                grdChecklist.DataBind();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CustomerBranchID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var auditDetail = (from row in entities.AuditCheckListMappings
                                       join row1 in entities.AuditDetails on row.AuditDetailID equals row1.ID
                                       join row2 in entities.AuditChecklists on row.ChecklistID equals row2.ID
                                       join row3 in entities.AuditCategories on row2.CategoryID equals row3.ID
                                       where row.Isactive == false && row1.CustomerID == CustomerBranchID
                                       && row1.ID == AuditDetailID
                                       select new
                                       {
                                           CheckListMappingID = row.ID
                                                    ,
                                           AuditDetailID = row1.ID
                                                    ,
                                           CheckListID = row2.ID
                                                    ,
                                           CheckListName = row2.Name
                                                    ,
                                           CategoryName = row3.Name
                                                    ,
                                           SequenceID = row2.SequanceID
                                                    ,
                                           SrNo = row2.SrNo
                                                    ,
                                           //Observation = row.Observation
                                           //         ,
                                           AuditStartDate = row1.AuditStartDate
                                               ,
                                           AuditEndDate = row1.AuditEndDate
                                       });

                    var AuditDetail = auditDetail.OrderBy(x => x.SequenceID).ToList();
                    grdChecklist.DataSource = AuditDetail;
                    grdChecklist.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdReviewerComplianceDocument_RowCommand(object sender,GridViewCommandEventArgs e)
        {
            GridViewRow gvrow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            Label lblAuditname = (Label)gvrow.FindControl("lblAuditName");
            Label lblAuditID = (Label)gvrow.FindControl("lblAuditID");
            Label AuditStartDatelbl = (Label)gvrow.FindControl("AuditStartDatelbl");
            Label AuditEndDatelbl = (Label)gvrow.FindControl("AuditEndDatelbl");
            string adname = Convert.ToString(lblAuditname.Text);
            var AuditID = Convert.ToInt64(lblAuditID.Text);
            ViewState["AuditID"] = AuditID;
            ViewState["AuditStartDate"] = Convert.ToDateTime(AuditStartDatelbl.Text);
            ViewState["AuditEndDate"] = Convert.ToDateTime(AuditEndDatelbl.Text); ;
            BindAuditCheckList(AuditID);

            upLitigationDetails.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
    
        }

        protected void grdChecklist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("View"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long CheckListID = Convert.ToInt64(commandArgs[0]);
                    long CheckListMappingID = Convert.ToInt64(commandArgs[1]);

                    long AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    ViewState["CheckListMappingID"] = CheckListMappingID;
                    DateTime AuditStartDate = Convert.ToDateTime(ViewState["AuditStartDate"]);
                    DateTime AuditEndDate = Convert.ToDateTime(ViewState["AuditEndDate"]);

                    var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType(AuditID, CheckListMappingID);

                    if (ComplianceType == "S")
                    {
                        var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditID, CheckListMappingID, AuditStartDate, AuditEndDate, "S", AuthenticationHelper.CustomerID,"MGMT").ToList();
                        if (MasterComplianceDocument != null)
                        {
                            grdDocument.DataSource = MasterComplianceDocument;
                            grdDocument.DataBind();

                            foreach (var file in MasterComplianceDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (filePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }

                                break;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated checklist complaince document not available in system')", true);
                        }
                    }
                    else
                    {
                        var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditID, CheckListMappingID, AuditStartDate, AuditEndDate, "I", AuthenticationHelper.CustomerID, "MGMT").ToList();
                        if (MasterComplianceDocument != null)
                        {
                            grdDocument.DataSource = MasterComplianceDocument;
                            grdDocument.DataBind();

                            foreach (var file in MasterComplianceDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (filePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }

                                break;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated checklist complaince document not available')", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static string HideUnhide(object AuditDetailID, object ChecklistMappingID, object FileDataID)
        {
            string Ishide = "Hide";
            int? AuditID = Convert.ToInt32(AuditDetailID);
            int? ChecklistID = Convert.ToInt32(ChecklistMappingID);
            long? FileID = Convert.ToInt64(FileDataID);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.AuditDetailHideDocuments
                            where row.AuditID == AuditID && row.ChecklistID == ChecklistID
                            && row.FileDataID == FileID && row.IsHide == false
                            select row).FirstOrDefault();

                if(data !=null)
                {
                    Ishide = "UnHide";
                }
                else
                {
                    Ishide = "Hide";
                }
                return Ishide;
            }
        }
        public static string getcompliancetype(long checklistid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.AuditChecklistComplianceMappings
                            where row.ChecklistID == checklistid
                            select row.ComplianceType).FirstOrDefault();
                return data;
            }
        }
       

        public static void Update(int? AuditID, int? CheckListID, long FileDataID,Boolean? ishide)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                AuditDetailHideDocument auditupdate = (from row in entities.AuditDetailHideDocuments
                                                       where row.AuditID == AuditID && row.ChecklistID == CheckListID
                                                       && row.FileDataID == FileDataID
                                                       select row).FirstOrDefault();

                auditupdate.IsHide = ishide;
                entities.SaveChanges();
            }
        }

        public static void  Create(AuditDetailHideDocument audit)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.AuditDetailHideDocuments.Add(audit);
                entities.SaveChanges();
            }
        }
   
        public static long GetAuditIDByName(string auditname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AuditId = (from row in entities.AuditDetails
                               where row.AuditName == auditname
                               select row.ID).FirstOrDefault();
                return AuditId;
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = 0;
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public void FillComplianceDocuments()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int userID = -1;
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    String location = tvFilterLocation.SelectedNode.Text;
                    int StatusValue = Convert.ToInt32(ddlStatus.SelectedValue);

                    Session["TotalRows"] = 0;
                    var AuditList = entities.SP_Get_AuditDetail_MGMT(customerID, userID).ToList();

                    if (AuditList.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(location))
                        {
                            if (location != "Entity/Sub-Entity/Location")
                                AuditList = AuditList.Where(entry => entry.LocationName == location).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 0)//for open
                        {
                            AuditList = AuditList.Where(x => x.AuditCloseFlag != true).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 1)//for open
                        {
                            AuditList = AuditList.Where(x => x.AuditCloseFlag == true).ToList();
                        }
                    }

                    grdReviewerComplianceDocument.DataSource = AuditList;
                    grdReviewerComplianceDocument.DataBind();
                    Session["TotalRows"] = AuditList.Count;
                    grdReviewerComplianceDocument.Visible = true;
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                FillComplianceDocuments();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {

        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                int Customerid = -1;
                Customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = -1;
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                String location = tvFilterLocation.SelectedNode.Text;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_Get_AuditDetailWithChecklist_MGMT_Result> MasterManagementCompliancesSummaryQuery = new List<SP_Get_AuditDetailWithChecklist_MGMT_Result>();
                    entities.Database.CommandTimeout = 180;
                    MasterManagementCompliancesSummaryQuery = (entities.SP_Get_AuditDetailWithChecklist_MGMT(Customerid, userID)).ToList();

                    if (MasterManagementCompliancesSummaryQuery.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(location))
                        {
                            if (location != "Entity/Sub-Entity/Location")
                                MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.LocationName == location).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 0)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(x => x.AuditCloseFlag != true).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 1)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(x => x.AuditCloseFlag == true).ToList();
                        }
                    }


                    if (MasterManagementCompliancesSummaryQuery.Count > 0)
                    {

                        DateTime EndDate = DateTime.Today.Date;
                        DateTime FromDate = DateTime.Today.AddMonths(-18);


                        Color colFromInTime = System.Drawing.ColorTranslator.FromHtml("#006500");
                        Color colFromAfterDuedate = System.Drawing.ColorTranslator.FromHtml("#ffcd70");
                        Color colFromOverdue = System.Drawing.ColorTranslator.FromHtml("#FF0000");

                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {

                            #region Audit detail             
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Audit Detail");
                            DataTable ExcelData1 = null;
                            DataView view = new System.Data.DataView((MasterManagementCompliancesSummaryQuery as List<SP_Get_AuditDetailWithChecklist_MGMT_Result>).ToDataTable());
                            ExcelData1 = view.ToTable("Selected", false, "LocationName", "VendorName", "CategoryName", "AuditName", "AuditStartDate", "AuditEndDate", "ChecklistName", "Observation");


                            #region

                           
                            exWorkSheet1.Cells["A3"].LoadFromDataTable(ExcelData1, true);

                            exWorkSheet1.Cells["A3"].Value = "Location Name";
                            exWorkSheet1.Cells["A3"].Merge = true;
                            exWorkSheet1.Cells["A3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.MistyRose);
                            exWorkSheet1.Cells["A3"].Style.WrapText = true;


                            exWorkSheet1.Cells["B3"].Value = "Vendor Name";
                            exWorkSheet1.Cells["B3"].Merge = true;
                            exWorkSheet1.Cells["B3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["B3"].Style.WrapText = true;

                            exWorkSheet1.Cells["C3"].Value = "Category Name";
                            exWorkSheet1.Cells["C3"].Merge = true;
                            exWorkSheet1.Cells["C3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                            exWorkSheet1.Cells["C3"].Style.WrapText = true;

                            exWorkSheet1.Cells["D3"].Value = "Audit Name";
                            exWorkSheet1.Cells["D3"].Merge = true;
                            exWorkSheet1.Cells["D3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D3"].Style.Fill.BackgroundColor.SetColor(Color.LightYellow);
                            exWorkSheet1.Cells["D3"].Style.WrapText = true;

                            exWorkSheet1.Cells["E3"].Value = "Audit Start Date";
                            exWorkSheet1.Cells["E3"].Merge = true;
                            exWorkSheet1.Cells["E3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["E3"].Style.WrapText = true;

                            exWorkSheet1.Cells["F3"].Value = "Audit End Date";
                            exWorkSheet1.Cells["F3"].Merge = true;
                            exWorkSheet1.Cells["F3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["F3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["F3"].Style.WrapText = true;

                            exWorkSheet1.Cells["G3"].Value = "Checklist Name";
                            exWorkSheet1.Cells["G3"].Merge = true;
                            exWorkSheet1.Cells["G3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["G3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["G3"].Style.WrapText = true;

                            exWorkSheet1.Cells["H3"].Value = "Observation";
                            exWorkSheet1.Cells["H3"].Merge = true;
                            exWorkSheet1.Cells["H3"].AutoFitColumns(54);
                            exWorkSheet1.Cells["H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["H3"].Style.WrapText = true;

                            #endregion

                            using (ExcelRange col = exWorkSheet1.Cells[7, 1, 7 + ExcelData1.Rows.Count, 6])
                            {
                                col.Style.WrapText = true;
                                //col.Style
                            }
                            //string ActulValue = string.Empty;
                            //string NewValue = string.Empty;
                            //int j = 5;
                            //int k = 5;
                            //for (int i = 5; i <= 4 + ExcelData1.Rows.Count; i++)
                            //{
                            //    string chke = "A" + i;
                            //    string Checkcell = exWorkSheet1.Cells[chke].Value.ToString();
                            //    if (i > 5)
                            //    {
                            //        if (string.IsNullOrEmpty(Checkcell))
                            //        {
                            //            j++;
                            //        }
                            //        else
                            //        {
                            //            string checknow4 = "A" + k + ":A" + j;
                            //            exWorkSheet1.Cells[checknow4].Merge = true;
                            //            k = i;
                            //            j = i;
                            //        }
                            //        if (j == (2 + ExcelData1.Rows.Count))
                            //        {
                            //            string checknow4 = "A" + k + ":A" + j;
                            //            exWorkSheet1.Cells[checknow4].Merge = true;
                            //        }
                            //    }
                            //}


                            #endregion

                            using (ExcelRange col = exWorkSheet1.Cells[1, 1, 3 + ExcelData1.Rows.Count, 8])
                            {
                                col.Style.WrapText = true;
                                using (ExcelRange col1 = exWorkSheet1.Cells[1, 1, 3 + ExcelData1.Rows.Count, 15])
                                {
                                    col1.Style.Numberformat.Format = "dd-MMM-yyyy";
                                }
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                exWorkSheet1.Cells["A3:H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A3:H3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                //exWorkSheet1.Cells["A4:H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["A4:H4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                // Assign borders
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                Byte[] fileBytes = exportPackge.GetAsByteArray();
                                Response.ClearContent();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=Audit Detail.xlsx");
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.ms-excel";
                                StringWriter sw = new StringWriter();
                                Response.BinaryWrite(fileBytes);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }

                        }
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                // Library.WriteErrorLog("Exception: " + ex.ToString() + " {0}");
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void btnAddNewold_Click(object sender, EventArgs e)
        {
            try
            {
                int Customerid = -1;
                Customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = -1;
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                String location = tvFilterLocation.SelectedNode.Text;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_Get_AuditDetailWithChecklist_MGMT_Result> MasterManagementCompliancesSummaryQuery = new List<SP_Get_AuditDetailWithChecklist_MGMT_Result>();
                    entities.Database.CommandTimeout = 180;
                    MasterManagementCompliancesSummaryQuery = (entities.SP_Get_AuditDetailWithChecklist_MGMT(Customerid, userID)).ToList();

                    if (MasterManagementCompliancesSummaryQuery.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(location))
                        {
                            if (location != "Entity/Sub-Entity/Location")
                                MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.LocationName == location).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 0)//for open
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(x => x.AuditCloseFlag != true).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 1)//for open
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(x => x.AuditCloseFlag == true).ToList();
                        }
                    }


                    if (MasterManagementCompliancesSummaryQuery.Count > 0)
                    {

                        DateTime EndDate = DateTime.Today.Date;
                        DateTime FromDate = DateTime.Today.AddMonths(-18);


                        Color colFromInTime = System.Drawing.ColorTranslator.FromHtml("#006500");
                        Color colFromAfterDuedate = System.Drawing.ColorTranslator.FromHtml("#ffcd70");
                        Color colFromOverdue = System.Drawing.ColorTranslator.FromHtml("#FF0000");

                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {

                            #region Audit detail                
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Audit Detail");
                            DataTable ExcelData1 = null;
                            DataView view = new System.Data.DataView((MasterManagementCompliancesSummaryQuery as List<SP_Get_AuditDetailWithChecklist_MGMT_Result>).ToDataTable());
                            ExcelData1 = view.ToTable("Selected", false, "LocationName", "VendorName", "CategoryName", "AuditName", "AuditStartDate", "AuditEndDate", "ChecklistName", "Observation");


                            #region 

                            //exWorkSheet1.Cells["A1"].Value = "Audit Detail";                            
                            //string EndRow = "A2:A" + b;
                            //exWorkSheet1.Cells["A2"].Value = "";
                            //exWorkSheet1.Cells[EndRow].Merge = true;
                            //exWorkSheet1.Cells["A2"].AutoFitColumns(5);
                            //exWorkSheet1.Cells["B2"].Value = "";
                            //exWorkSheet1.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["B2"].Style.Fill.BackgroundColor.SetColor(Color.Red);

                            exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);
                            exWorkSheet1.Cells["A3"].Value = "Location Name";
                            exWorkSheet1.Cells["A3:A4"].Merge = true;
                            exWorkSheet1.Cells["A3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.MistyRose);
                            exWorkSheet1.Cells["A3"].Style.WrapText = true;


                            exWorkSheet1.Cells["B3"].Value = "Vendor Name";
                            exWorkSheet1.Cells["B3:B4"].Merge = true;
                            exWorkSheet1.Cells["B3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["B3"].Style.WrapText = true;

                            exWorkSheet1.Cells["C3"].Value = "Category Name";
                            exWorkSheet1.Cells["C3:C4"].Merge = true;
                            exWorkSheet1.Cells["C3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                            exWorkSheet1.Cells["C3"].Style.WrapText = true;

                            exWorkSheet1.Cells["D3"].Value = "Audit Name";
                            exWorkSheet1.Cells["D3:D4"].Merge = true;
                            exWorkSheet1.Cells["D3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D3"].Style.Fill.BackgroundColor.SetColor(Color.LightYellow);
                            exWorkSheet1.Cells["D3"].Style.WrapText = true;

                            exWorkSheet1.Cells["E3"].Value = "Audit Start Date";
                            exWorkSheet1.Cells["E3:E4"].Merge = true;
                            exWorkSheet1.Cells["E3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["E3"].Style.WrapText = true;

                            exWorkSheet1.Cells["F3"].Value = "Audit End Date";
                            exWorkSheet1.Cells["F3:F4"].Merge = true;
                            exWorkSheet1.Cells["F3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["F3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["F3"].Style.WrapText = true;

                            exWorkSheet1.Cells["G3"].Value = "Checklist Name";
                            exWorkSheet1.Cells["G3:G4"].Merge = true;
                            exWorkSheet1.Cells["G3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["G3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["G3"].Style.WrapText = true;

                            exWorkSheet1.Cells["H3"].Value = "Observation";
                            exWorkSheet1.Cells["H3:H4"].Merge = true;
                            exWorkSheet1.Cells["H3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["H3"].Style.WrapText = true;

                            #endregion

                            using (ExcelRange col = exWorkSheet1.Cells[7, 1, 7 + ExcelData1.Rows.Count, 6])
                            {
                                col.Style.WrapText = true;
                                //col.Style
                            }
                            //string ActulValue = string.Empty;
                            //string NewValue = string.Empty;
                            //int j = 5;
                            //int k = 5; 
                            //for (int i = 5; i <= 4 + ExcelData1.Rows.Count; i++)
                            //{
                            //    string chke = "A" + i;
                            //    string Checkcell = exWorkSheet1.Cells[chke].Value.ToString();
                            //    if (i > 5)
                            //    {
                            //        if (string.IsNullOrEmpty(Checkcell))
                            //        {
                            //            j++;
                            //        }
                            //        else
                            //        {
                            //            string checknow4 = "A" + k + ":A" + j;
                            //            exWorkSheet1.Cells[checknow4].Merge = true;
                            //            k = i;
                            //            j = i;
                            //        }
                            //        if (j == (2 + ExcelData1.Rows.Count))
                            //        {
                            //            string checknow4 = "A" + k + ":A" + j;
                            //            exWorkSheet1.Cells[checknow4].Merge = true;
                            //        }
                            //    }
                            //}


                            #endregion

                            using (ExcelRange col = exWorkSheet1.Cells[1, 1, 4 + ExcelData1.Rows.Count, 8])
                            {
                                col.Style.WrapText = true;
                                using (ExcelRange col1 = exWorkSheet1.Cells[1, 1, 4 + ExcelData1.Rows.Count, 15])
                                {
                                    col1.Style.Numberformat.Format = "dd-MMM-yyyy";
                                }
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells["A3:H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A3:H3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["A4:H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A4:H4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                // Assign borders
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                Byte[] fileBytes = exportPackge.GetAsByteArray();
                                Response.ClearContent();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=Audit Detail.xlsx");
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.ms-excel";
                                StringWriter sw = new StringWriter();
                                Response.BinaryWrite(fileBytes);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }

                        }
                    }
                    else
                    {
                        //Library.WriteErrorLog("No data found so not create excel");
                    }
                }
            }
            catch (Exception ex)
            {
                // Library.WriteErrorLog("Exception: " + ex.ToString() + " {0}");
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdDocument_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void checkAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdDocument.HeaderRow.FindControl("checkAll");
            foreach (GridViewRow row in grdDocument.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("CheckBox1");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }
        }
        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("View"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int? AuditID = Convert.ToInt32(commandArgs[0]);
                    int? CheckListID = Convert.ToInt32(commandArgs[1]);
                    long FileDataID = Convert.ToInt32(commandArgs[3]);

                    DateTime AuditStartDate =Convert.ToDateTime(ViewState["AuditStartDate"]);
                    DateTime AuditEndDate = Convert.ToDateTime(ViewState["AuditEndDate"]);
                    var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType((long)AuditID, (long)CheckListID);

                    if (ComplianceType == "S")
                    {
                        var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistDocumentPerticular(AuditID, CheckListID, AuditStartDate, AuditEndDate, "S", AuthenticationHelper.CustomerID,(int)FileDataID).ToList();

                        foreach (var file in MasterComplianceDocument)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (filePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    //lblMessageReviewer1.Text = "";
                                    //lblMessageReviewer1.Text = "Zip file can't view please download it";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    CompDocReviewPath = FileName;
                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                            }
                        }
                    }
                    else
                    {
                        var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistDocumentPerticular(AuditID, CheckListID, AuditStartDate, AuditEndDate, "I", AuthenticationHelper.CustomerID, (int)FileDataID).ToList();

                        foreach (var file in MasterComplianceDocument)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (filePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    CompDocReviewPath = FileName;
                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                //lblMessageReviewer1.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                            }
                        }
                    }
                }
                else if (e.CommandName.Equals("HideUnHide"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int? AuditID = Convert.ToInt32(commandArgs[0]);
                    int? CheckListID = Convert.ToInt32(commandArgs[1]);

                    long FileDataID = Convert.ToInt32(commandArgs[3]);
                    string DocType = "";
                    var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType((long)AuditID, (long)CheckListID);

                    if (ComplianceType == "S")
                    {
                        DocType = "S";
                    }
                    else if (ComplianceType == "I")
                    {
                        DocType = "I";
                    }


                    GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    int rowIndex = row.RowIndex;
                    LinkButton lnkhidebutton = (LinkButton)row.FindControl("lnkhidebutton");
                    Boolean hidevalue = false;
                    if (lnkhidebutton.Text == "Hide")
                    {
                        hidevalue = true;
                        lnkhidebutton.Text = "UnHide";
                    }
                    else if (lnkhidebutton.Text == "UnHide")
                    {
                        hidevalue = false;
                        lnkhidebutton.Text = "Hide";
                    }

                    AuditDetailHideDocument audit = new AuditDetailHideDocument()
                    {
                        AuditID = AuditID,
                        ChecklistID = CheckListID,
                        FileDataID = FileDataID,
                        ChecklistType = DocType,
                        IsHide = hidevalue
                    };
                    if (Isexists(AuditID, CheckListID, FileDataID, DocType))
                    {
                        Update(AuditID, CheckListID, FileDataID, audit.IsHide);
                    }
                    else
                    {
                        Create(audit);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool Isexists(int? auditid, int? checklistid, long? filedataid, string ChecklistType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.AuditDetailHideDocuments
                            where row.AuditID == auditid && row.ChecklistID == checklistid
                            && row.ChecklistType == ChecklistType
                            && row.FileDataID == filedataid
                            select row);

                return data.Select(entry => true).FirstOrDefault();
            }
        }
        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    long AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    long CheckListMappingID = Convert.ToInt32(ViewState["CheckListMappingID"]);
                    LinkButton lnkhidebutton = (LinkButton)e.Row.FindControl("lnkhidebutton");
                    Label lblFileID = (Label)e.Row.FindControl("lblFileID");
               
                    long FileID = Convert.ToInt64(lblFileID.Text);

                    var AuditSampleData = Business.VenderAudits.VenderAuditManagment.GetAuditDetailHideDocument(AuditID, CheckListMappingID, FileID);
                    if (AuditSampleData.Count <= 0)
                    {
                        lnkhidebutton.Text = "Hide";
                    }
                    else
                    {
                        lnkhidebutton.Text = "UnHide";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveHideUnHide_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow item in grdDocument.Rows)
                {
                    CheckBox chkAssign = (CheckBox)item.FindControl("CheckBox1");
                    if (chkAssign.Checked)
                    {
                        Label lblChecklistMappingID = (Label)item.FindControl("lblChecklistMappingID");
                        int ChecklistMappingID = Convert.ToInt32(lblChecklistMappingID.Text);

                        int? AuditID = Convert.ToInt32(ViewState["AuditID"]);
                        Label lblFileID = (Label)item.FindControl("lblFileID");
                        int FileID = Convert.ToInt32(lblFileID.Text);

                        var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType((long)AuditID, (long)ChecklistMappingID);
                        string DocType = "";
                        if (ComplianceType == "S")
                        {
                            DocType = "S";
                        }
                        else if (ComplianceType == "I")
                        {
                            DocType = "I";
                        }

                        AuditDetailHideDocument audit = new AuditDetailHideDocument()
                        {
                            AuditID = (int?)AuditID,
                            ChecklistID = ChecklistMappingID,
                            FileDataID = FileID,
                            ChecklistType = DocType,
                            IsHide = true,
                        };
                        if (Isexists(AuditID, ChecklistMappingID, FileID, DocType))
                        {
                            Update(AuditID, ChecklistMappingID, FileID, audit.IsHide);
                        }
                        else
                        {
                            Create(audit);
                        }
                    }
                }

                int? AuditID1 = Convert.ToInt32(ViewState["AuditID"]);
                //Label lblFileID = (Label)item.FindControl("lblFileID");
                //int FileID = Convert.ToInt32(lblFileID.Text);
                int CheckListMappingID = Convert.ToInt32(ViewState["CheckListMappingID"]);
                var ComplianceType1 = Business.VenderAudits.VenderAuditManagment.GetComplianceType((long)AuditID1, (long)CheckListMappingID);

                DateTime AuditStartDate = Convert.ToDateTime(ViewState["AuditStartDate"]);
                DateTime AuditEndDate = Convert.ToDateTime(ViewState["AuditEndDate"]);

                if (ComplianceType1 == "S")
                {
                    var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditID1, CheckListMappingID, AuditStartDate, AuditEndDate, "S", AuthenticationHelper.CustomerID, "MGMT").ToList();
                    if (MasterComplianceDocument != null)
                    {
                        grdDocument.DataSource = MasterComplianceDocument;
                        grdDocument.DataBind();
                    }
                }
                else
                {
                    var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditID1, CheckListMappingID, AuditStartDate, AuditEndDate, "I", AuthenticationHelper.CustomerID, "MGMT").ToList();
                    if (MasterComplianceDocument != null)
                    {
                        grdDocument.DataSource = MasterComplianceDocument;
                        grdDocument.DataBind();
                    }
                }

                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Document hide successfully";

                UpdatePanel3.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}