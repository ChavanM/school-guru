﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages
{
    public partial class AuditSchedule : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ID";
                btnAdd.Visible = true;
                BindLocationFilter();
                AuditDetail();
            }
        }

        private void AuditDetail()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int CustomerBranchID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var auditDetail = (from row in entities.AuditDetails
                                        join row1 in entities.AuditVendors on row.VendorID equals row1.ID
                                        join row2 in entities.CustomerBranches on row.BranchID equals row2.ID
                                        where row.Isactive == false && row.CustomerID == CustomerBranchID
                                   select new
                                        {
                                            row.ID
                                                ,
                                            row.VendorID
                                                ,
                                            VendorName = row1.Name
                                                ,
                                            Branch = row2.Name
                                                ,
                                            row.AuditName
                                                ,
                                            row.AuditStartDate
                                                ,
                                            row.AuditEndDate
                                              
                                        });

                var AuditDetail = auditDetail.ToList();

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        AuditDetail = AuditDetail.OrderBy(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "VendorName")
                    {
                        AuditDetail = AuditDetail.OrderBy(entry => entry.VendorName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "AuditName")
                    {
                        AuditDetail = AuditDetail.OrderBy(entry => entry.AuditName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "AuditStartDate")
                    {
                        AuditDetail = AuditDetail.OrderBy(entry => entry.AuditStartDate).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "AuditEndDate")
                    {
                        AuditDetail = AuditDetail.OrderBy(entry => entry.AuditEndDate).ToList();
                    }
                    
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        AuditDetail = AuditDetail.OrderByDescending(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "VendorName")
                    {
                        AuditDetail = AuditDetail.OrderByDescending(entry => entry.VendorName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "AuditName")
                    {
                        AuditDetail = AuditDetail.OrderByDescending(entry => entry.AuditName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "AuditStartDate")
                    {
                        AuditDetail = AuditDetail.OrderByDescending(entry => entry.AuditStartDate).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "AuditEndDate")
                    {
                        AuditDetail = AuditDetail.OrderByDescending(entry => entry.AuditEndDate).ToList();
                    }

                    direction = SortDirection.Ascending;
                }
                grdAuditDetail.DataSource = AuditDetail;
                grdAuditDetail.DataBind();
                //upCompliancesList.Update();
            }
        }

        protected void grdAuditDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_Audit"))
                {
                    int AuditID = Convert.ToInt32(e.CommandArgument);

                    //Response.Redirect("~/VenderAudit/AuditScheduleAdd.aspx?AuditID=" + AuditID, false);
                    Response.Redirect("~/VenderAudit/aspxpages/AuditScheduleAdd.aspx?Mode=1&AuditID=" + AuditID, false);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? Regex.Replace(tvBranches.SelectedNode.Text, "<.*?>", string.Empty) : "< Select >";
            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);


                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", "initializeConfirmDatePicker1(null);", true);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeChecklist", string.Format("initializeJQueryUI('{0}', 'dvChecklist');", txtChecklist.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideChecklist", "$(\"#dvChecklist\").hide(\"blind\", null, 5, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", "initializeConfirmDatePicker1();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindChecklist(int VendorID)
        {
            try
            {
                rptChecklist.DataSource = Business.VenderAudits.VenderAuditManagment.GetAuditChecklist(VendorID);
                rptChecklist.DataBind();

                foreach (RepeaterItem aItem in rptChecklist.Items)
                {
                    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkChecklist");
                    chkCompliance.Checked = false;
                    CheckBox chkComplianceSelectAll = (CheckBox)rptChecklist.Controls[0].Controls[0].FindControl("ChecklistSelectAll");
                    chkComplianceSelectAll.Checked = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAuditor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                User user = UserManagement.GetByID(Convert.ToInt32(ddlAuditor.SelectedValue));
                int? branch = null;
                if (user.CustomerBranchID != null)
                {
                    branch = user.CustomerBranchID;

                    var customerBranch = CustomerBranchManagement.GetByID(Convert.ToInt64(user.CustomerBranchID));
                    tbxBranch.Text = customerBranch.Name;
                }
                else
                {
                    tbxBranch.Text = "< Select >";
                }
            }
            catch (Exception)
            {

            }

        }


        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

    

        //protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindComplianceInstances();
        //}

        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                int VendorID = Convert.ToInt32(ddlVendor.SelectedValue);

                BindChecklist(VendorID);

                //User user = UserManagement.GetByID(Convert.ToInt32(ddlAuditor.SelectedValue));
                //int? branch=null;
                //if (user.CustomerBranchID != null)
                //{
                //    branch = user.CustomerBranchID;

                //     var customerBranch = CustomerBranchManagement.GetByID(Convert.ToInt64(user.CustomerBranchID));
                //     tbxBranch.Text  = customerBranch.Name;
                //}
                //else
                //{
                //    tbxBranch.Text = "< Select >";
                //}
            }
            catch (Exception)
            {

            }
           
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), CustomerID).ID;
                DateTime startDate;
                DateTime endDate;

                startDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                endDate = DateTime.ParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                com.VirtuosoITech.ComplianceManagement.Business.Data.AuditDetail auditDetail = new com.VirtuosoITech.ComplianceManagement.Business.Data.AuditDetail()
                {
                    AuditName = txtAuditName.Text.Trim(),
                    CustomerID = CustomerID,
                    BranchID = branchID,
                    AuditorID = Convert.ToInt32(ddlAuditor.SelectedValue),
                    VendorID = Convert.ToInt32(ddlVendor.SelectedValue),
                    AuditStartDate = startDate,
                    AuditEndDate = endDate,
                    CreatedBy = AuthenticationHelper.UserID,
                    UpdatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    Isactive = false
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    auditDetail.ID = Convert.ToInt32(ViewState["AuditID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (Business.VenderAudits.VenderAuditManagment.CheckAuditExists(CustomerID,auditDetail.AuditName))
                    {
                        cvDuplicateEntry.ErrorMessage = "Audit name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    Business.VenderAudits.VenderAuditManagment.CreateAuditDetails(auditDetail);

                    //Add Checklist to Audit
                    List<int> ParentEventIds = new List<int>();
                    //Business.ComplianceManagement.UpdateComplianceForEventMappedID(Convert.ToInt32(eventData.ID));
                    foreach (RepeaterItem aItem in rptChecklist.Items)
                    {
                        CheckBox chkChecklist = (CheckBox)aItem.FindControl("chkChecklist");
                        if (chkChecklist.Checked)
                        {
                            ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblChecklistID")).Text.Trim()));
                            AuditCheckListMapping auditCheckListMapping = new AuditCheckListMapping()
                            {
                                ChecklistID = Convert.ToInt32(((Label)aItem.FindControl("lblChecklistID")).Text.Trim()),
                                AuditDetailID = Convert.ToInt32(auditDetail.ID),
                                VendorID = Convert.ToInt32(ddlVendor.SelectedValue),
                                Observation = "",
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                UpdatedOn = DateTime.Now,
                                Isactive = false
                            };
                            Business.VenderAudits.VenderAuditManagment.CreateauditCheckListMapping(auditCheckListMapping);
                        }
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    Business.VenderAudits.VenderAuditManagment.UpdateAuditDetails(auditDetail);
                    //Add Checklist to Audit
                    List<int> ParentEventIds = new List<int>();

                    foreach (RepeaterItem aItem in rptChecklist.Items)
                    {
                        CheckBox chkChecklist = (CheckBox)aItem.FindControl("chkChecklist");
                        if (chkChecklist.Checked)
                        {
                            int ChecklistID = Convert.ToInt32(((Label)aItem.FindControl("lblChecklistID")).Text.Trim());
                            int AuditDetailID = Convert.ToInt32(auditDetail.ID);

                            if (!Business.VenderAudits.VenderAuditManagment.CheckAuditCheckListMappingExists(AuditDetailID, ChecklistID))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblChecklistID")).Text.Trim()));
                                AuditCheckListMapping auditCheckListMapping = new AuditCheckListMapping()
                                {
                                    ChecklistID = ChecklistID,
                                    AuditDetailID = AuditDetailID,
                                    Observation = "",
                                    VendorID = Convert.ToInt32(ddlVendor.SelectedValue),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedOn = DateTime.Now,
                                    Isactive = false
                                };
                                Business.VenderAudits.VenderAuditManagment.CreateauditCheckListMapping(auditCheckListMapping);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version."; 
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                Session["Mode"] = 0;
                Session["AuditID"] = 0;
                Response.Redirect("~/VenderAudit/aspxpages/AuditScheduleAdd.aspx?Mode=0&AuditID=0", false);

                //if (tvBranches.SelectedNode != null)
                //{
                //    tvBranches.SelectedNode.Selected = false;
                //}
                //ViewState["CHECKED_ITEMS"] = null;
                //ddlAuditor.SelectedValue = "-1";
                //ddlVendor.SelectedValue = "-1";
                //ViewState["Mode"] = 0;
                //tbxStartDate.Text ="";
                //tbxEndDate.Text = "";
                //tbxBranch.Enabled = false;
                //txtAuditName.Text = "";
                //grdChecklist.DataSource = null;
                //grdChecklist.DataBind();

                //foreach (RepeaterItem aItem in rptChecklist.Items)
                //{
                //    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkChecklist");
                //    chkCompliance.Checked = false;
                //    CheckBox chkComplianceSelectAll = (CheckBox)rptChecklist.Controls[0].Controls[0].FindControl("ChecklistSelectAll");
                //    chkComplianceSelectAll.Checked = false;
                //}

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignComplianceDialog\").dialog('open');", true);
                //tbxBranch.Text = "< Select >";
                //txtChecklist.Text = "< Select Checklist >";
                //ForceCloseFilterBranchesTreeView();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AuditDetail();
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdAuditDetail_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                AuditDetail();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void btnChecklistRepeater_Click(object sender, EventArgs e)
        {

        }

        protected void grdAuditDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblAuditDetailID = (Label)e.Row.FindControl("lblAuditID");
                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");

                    var data = Business.VenderAudits.VenderAuditManagment.GetAuditDetailByID(Convert.ToInt64(lblAuditDetailID.Text));

                    if (data.AuditCloseFlag == true)
                    {
                        lbtEdit.Visible = false;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}