﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages
{
    public partial class UploadAuditChecklist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindVendorList();                            
            }
        }
        private void bindChecklistGrid()
        {
            try
            {
                var checkdata = GetCheckList(Convert.ToInt64(ddlVendor.SelectedValue));

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    checkdata = checkdata.Where(entry => entry.CheckList.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.AuditCategory.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.VendorName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ClauseNo.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                }
                grdAuditChecklist.DataSource = checkdata;
                grdAuditChecklist.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindVendorList()
        {
            try
            {
                ddlVendor.DataTextField = "Name";
                ddlVendor.DataValueField = "ID";
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                ddlVendor.DataSource = GetVendorList(CustomerID);
                ddlVendor.DataBind();

                ddlVendor.Items.Insert(0, new ListItem("< Select Vendor>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindCategoryList(long VendorID)
        {
            try
            {
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = GetCategoryList(VendorID);
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("< Select Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        private bool RiskTransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("AuditChecklistCreate"))
                    {
                        if (sheet.Name.Trim().Equals("AuditChecklistCreate") || sheet.Name.Trim().Equals("AuditChecklistCreate") || sheet.Name.Trim().Equals("AuditChecklistCreate"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
                //bool flag = false;
                //foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                //{
                //    if (data.Equals("AuditChecklistCreate"))
                //    {
                //        flag = true;
                //        break;
                //    }
                //    else
                //    {
                //        flag = false;
                //        break;
                //    }
                //}
                //return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
           
        }
        public static List<Sp_Get_AuditCheckList_Compliance_Result> GetAllComplianceDetails(long ChecklistID,string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DTLList = (from row in entities.Sp_Get_AuditCheckList_Compliance(ChecklistID, flag)                                 
                                  select row).ToList();

                return DTLList;
            }
        }
        private void BindComplianceDetails(long ChecklistID)
        {
            try
            {
                if (ComplianceType.SelectedValue == "S")
                {
                    GridRemarks.DataSource = GetAllComplianceDetails(ChecklistID,"S");
                    GridRemarks.DataBind();
                }                     
                else
                {
                    GridRemarks.DataSource = GetAllComplianceDetails(ChecklistID, "I");
                    GridRemarks.DataBind();
                }                                 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool flag = RiskTransactionSheetsExitsts(xlWorkbook, "AuditChecklistCreate");
                            if (flag == true)
                            {
                               ProcessAuditStepCreateasd(xlWorkbook, Convert.ToInt64(ddlVendor.SelectedValue));
                                bindChecklistGrid();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Sheet Name does not exist.";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
            }
        }
        public bool checkDuplicateDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, int colNum, string providedText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            TextToCompare = xlWorksheet.Cells[i, colNum].Text.ToString().Trim();
                            if (String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                            {
                                matchSuccess = true;
                                i = lastRow; //exit from for Loop
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }
        private void ProcessAuditStepCreateasd(ExcelPackage xlWorkbook, long vendorId)
        {
            try
            {
                bool saveSuccess = false;
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["AuditChecklistCreate"];
                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    long customerID = Portal.Common.AuthenticationHelper.CustomerID;
                    string ClauseNo = string.Empty;
                    string AuditCategory = string.Empty;
                    long AuditCategoryId = -1;
                    string CheckList = string.Empty;
                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        #region 1 Clasue No
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            ClauseNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            bool matchSuccess = false;
                            matchSuccess = checkDuplicateDataExistExcelSheet(xlWorksheet, i, 1, ClauseNo);
                            if (matchSuccess)
                            {
                                errorMessage.Add("Clasue with Same Clasue Number at Row - " + (count + 1) + " Exists Multiple Times in the Uploaded Excel Document");
                            }
                        }
                        else
                        {
                            errorMessage.Add("Clasue No Not found at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 2 Audit Category
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            AuditCategory = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();                           
                            long GetAuditCategoryID = ExistAuditCategoryName(AuditCategory, vendorId);
                            if (GetAuditCategoryID > 0)
                            {
                                AuditCategoryId = GetAuditCategoryID;
                            }                                                                               
                        }
                        else
                        {
                            errorMessage.Add("Audit Category Not found at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 3 CheckList
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            CheckList = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                        }
                        else
                        {
                            errorMessage.Add("CheckList Not found at row number - " + (count + 1) + "");
                        }
                        #endregion
                                                
                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {                       
                        for (int i = 2; i <= xlrow2; i++)
                        {                                                    
                            ClauseNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            AuditCategory = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            long GetAuditCategoryID = ExistAuditCategoryName(AuditCategory, vendorId);
                            if (GetAuditCategoryID > 0)
                            {
                                AuditCategoryId = GetAuditCategoryID;
                            }                            
                            CheckList = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            ExistAuditCategoryName(AuditCategoryId, CheckList, vendorId, ClauseNo);
                        }
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }                       
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }   
        public static bool ExistAuditCategoryName(long categoryId, string ChecklistName, long AuditVendorId, string clauseno)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool Ans = false;
                long customerID = -1;
                long UserID = -1;
                customerID =Portal.Common.AuthenticationHelper.CustomerID;
                UserID = Portal.Common.AuthenticationHelper.UserID;

                try
                {
                    var query = (from row in entities.AuditChecklists
                                 where row.Name == ChecklistName
                                 && row.CategoryID == categoryId
                                 && row.VendorID == AuditVendorId
                                 select row).FirstOrDefault();
                    if (query != null)
                    {

                    }
                    else
                    {                  
                        AuditChecklist Obj = new AuditChecklist()
                        {
                            CustomerID = Convert.ToInt32(customerID),
                            VendorID = AuditVendorId,
                            Name = ChecklistName,
                            //SequanceID = sequanceid,
                            SrNo = clauseno,
                            CategoryID = categoryId,
                            CreatedBy = UserID,
                            CreatedOn = DateTime.Now,
                            UpdatedBy = UserID,
                            UpdatedOn = DateTime.Now,
                            Isactive = false
                        };
                        entities.AuditChecklists.Add(Obj);
                        entities.SaveChanges();

                    }
                    Ans = true;
                }
                catch (Exception ex)
                {
                    Ans = false;
                }

                return Ans;
            }
        }
        public static long ExistAuditCategoryName(string CategoryName, long AuditVendorId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long customerID = -1;
                long UserID = -1;
                customerID = Portal.Common.AuthenticationHelper.CustomerID;
                UserID = Portal.Common.AuthenticationHelper.UserID;

                long Ans = -1;
                var query = (from row in entities.AuditCategories
                             where row.Name == CategoryName
                             && row.VendorID == AuditVendorId
                             select row).FirstOrDefault();
                if (query != null)
                {
                    Ans = query.ID;
                }
                else
                {
                    AuditCategory Obj = new AuditCategory()
                    {
                        Name = CategoryName,
                        CustomerID = Convert.ToInt32(customerID),
                        VendorID = AuditVendorId,
                        CreatedBy = UserID,
                        CreatedOn = DateTime.Now,
                        UpdatedBy = UserID,
                        UpdatedOn = DateTime.Now,
                        Isactive = false
                    };
                    entities.AuditCategories.Add(Obj);
                    entities.SaveChanges();

                    Ans = Obj.ID;
                }
                return Ans;
            }
        }
        public static List<AuditVendor> GetVendorList(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var VendorDetails = (from row in entities.AuditVendors
                                         where row.Isactive == false
                                         && row.CustomerID == CustomerID
                                     select row).ToList();

                return VendorDetails;
            }
        }
        public static List<AuditCategory> GetCategoryList(long vendorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CategoryDetails = (from row in entities.AuditCategories
                                     where row.Isactive == false && row.VendorID== vendorID
                                       select row).ToList();

                return CategoryDetails;
            }
        }
        public static List<SP_VenderAuditCheckList_Result> GetCheckList(long VID)
        {
            long customerID = -1;           
            customerID = Portal.Common.AuthenticationHelper.CustomerID;         
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var VendorDetails = (from row in entities.SP_VenderAuditCheckList(customerID, VID)                                                                  
                                     select row).ToList();

                return VendorDetails;
            }
        }
        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void upCustomerBranchList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        protected void grdAuditChecklist_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void grdAuditChecklist_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var checkdata = GetCheckList(Convert.ToInt64(ddlVendor.SelectedValue));
                
                if (direction == SortDirection.Ascending)
                {
                    checkdata = checkdata.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    checkdata = checkdata.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdAuditChecklist.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAuditChecklist.Columns.IndexOf(field);
                    }
                }
                grdAuditChecklist.DataSource = checkdata;
                grdAuditChecklist.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void grdAuditChecklist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {                                              
                if (e.CommandName.Equals("EDIT_Checklist"))
                {
                    txtComplianceID.Text = string.Empty;
                    lblClauseNo.Text = string.Empty;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int checklistid = Convert.ToInt32(commandArgs[0]);
                    int categoryId = Convert.ToInt32(commandArgs[1]);

                    ViewState["checklistid"] = null;
                    ViewState["checklistid"] = Convert.ToInt32(checklistid);

                    ViewState["categoryId"] = null;
                    ViewState["categoryId"] = Convert.ToInt32(categoryId);

                    var auditdetails = GetByID(checklistid);
                    if (auditdetails != null)
                    {
                        tbxCheckList.Text = auditdetails.Name.Trim();
                        lblClauseNo.Text= auditdetails.SrNo.Trim();
                        bindCategoryList(auditdetails.VendorID);
                        if (!string.IsNullOrEmpty(Convert.ToString(auditdetails.VendorID)))
                        {
                            ddlCategory.SelectedValue = Convert.ToString(auditdetails.CategoryID);
                        }
                        var checkStatutoryNonStatutory = GetAuditCheklistComplianceMappingByID(checklistid);
                        if (checkStatutoryNonStatutory != null)
                        {
                            if (checkStatutoryNonStatutory.ComplianceType == "S")
                            {
                                ComplianceType.SelectedValue = "S";
                                ComplianceType.Items[1].Enabled = false;
                            }
                            else
                            {
                                ComplianceType.SelectedValue = "I";
                                ComplianceType.Items[0].Enabled = false;
                            }
                        }
                        else
                        {
                            ComplianceType.SelectedValue = "S";
                            ComplianceType.Items[1].Enabled = true;
                            ComplianceType.Items[0].Enabled = true;
                        }
                        
                    }

                    BindDocument(Convert.ToInt32(checklistid));
                    BindComplianceDetails(Convert.ToInt32(checklistid));
                    upCustomerBranches.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open')", true);
                }              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);           
            }
        }
        public static AuditChecklist GetByID(long auditchecklistid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.AuditChecklists
                                  where row.ID == auditchecklistid
                                  && row.Isactive== false
                                  select row).FirstOrDefault();

                return compliance;
            }
        }
        public static AuditChecklistComplianceMapping GetAuditCheklistComplianceMappingByID(long auditchecklistid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.AuditChecklistComplianceMappings
                                  where row.ChecklistID == auditchecklistid
                                  && row.Isactive == false
                                  select row).FirstOrDefault();

                return compliance;
            }
        }        
        protected void grdAuditChecklist_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAuditChecklist.PageIndex = e.NewPageIndex;
                bindChecklistGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditChecklist_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
        }
        public void ErrorMessages(List<string> emsg)
        {          
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void ComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        public static bool InternalExists(List<long> Ilist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliances
                             where row.IsDeleted == false && Ilist.Contains(row.ID)
                             && row.CustomerID==AuthenticationHelper.CustomerID
                             select row);
               
                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool Exists(string filename)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.AuditSampleDocumentDatas
                             where row.IsDeleted == false && row.Name.ToUpper().Trim()== filename.ToUpper().Trim()
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        
        public static bool StatutoryExists(List<long> Ilist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Compliances
                             where row.IsDeleted == false && Ilist.Contains(row.ID)
                             && row.Status ==null                             
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["checklistid"] != null && ViewState["categoryId"] != null)
                {
                    long customerID = -1;
                    long UserID = -1;
                    customerID = Portal.Common.AuthenticationHelper.CustomerID;
                    UserID = Portal.Common.AuthenticationHelper.UserID;
                    List<string> ComplianceTextboxList = new List<string>();
                    List<long> ComplianceList = new List<long>();
                    if (!string.IsNullOrEmpty(txtComplianceID.Text))
                    {

                        ComplianceTextboxList = txtComplianceID.Text.Split(',').ToList();
                        ComplianceList = ComplianceTextboxList.Select(long.Parse).ToList();
                        if (ComplianceType.SelectedValue == "I")
                        {
                            bool checkinternal = InternalExists(ComplianceList);
                            if (!checkinternal)
                            {
                                saveopo.Value = "true";
                                cvDuplicateEntryPopPup.IsValid = false;
                                cvDuplicateEntryPopPup.ErrorMessage = "Enter valid internal complianceid or this ids are not present in the system.";
                                return;
                            }
                        }
                        else
                        {
                            bool checkstatutory = StatutoryExists(ComplianceList);
                            if (!checkstatutory)
                            {
                                saveopo.Value = "true";
                                cvDuplicateEntryPopPup.IsValid = false;
                                cvDuplicateEntryPopPup.ErrorMessage = "Enter valid complianceid or this ids are not present in the system.";
                                return;
                            }
                        }

                        List<AuditChecklistComplianceMapping> ACCMList = new List<AuditChecklistComplianceMapping>();
                        foreach (long clist in ComplianceList)
                        {
                            AuditChecklistComplianceMapping ACCM = new AuditChecklistComplianceMapping();
                            ACCM.CustomerID = Convert.ToInt32(customerID);
                            ACCM.ChecklistID = Convert.ToInt32(ViewState["checklistid"]);
                            ACCM.VendorID = Convert.ToInt32(ddlVendor.SelectedValue);
                            ACCM.ComplianceID = Convert.ToInt32(clist);
                            ACCM.CategoryID = Convert.ToInt32(ViewState["categoryId"]);
                            ACCM.CreatedBy = UserID;
                            ACCM.CreatedOn = DateTime.Now;
                            ACCM.ComplianceType = ComplianceType.SelectedValue.ToString();
                            ACCM.Isactive = false;
                            ACCMList.Add(ACCM);
                        }
                        bool checkFlag = false;
                        checkFlag = BlukClientMapplingCompliance(ACCMList);
                    }                    
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        AuditChecklist ac = new AuditChecklist()
                        {
                            ID = Convert.ToInt32(ViewState["checklistid"]),
                            Name = tbxCheckList.Text.Trim()
                        };
                        Update(ac);
                    }
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divCustomerBranchesDialog\").dialog('close')", true);
                    BindDocument(Convert.ToInt32(ViewState["checklistid"]));
                    BindComplianceDetails(Convert.ToInt32(ViewState["checklistid"]));
                    txtComplianceID.Text = string.Empty;
                    upCustomerBranches.Update();
                    saveopo.Value = "true";

                    saveopo.Value = "true";
                    cvDuplicateEntryPopPup.IsValid = false;
                    cvDuplicateEntryPopPup.ErrorMessage = "Save successfully.";
                    return;
                }//ViewState["checklistid"] & ViewState["categoryId"] End
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDocument(int ChecklistID)
        {
            try
            {
                List<Sp_GetAuditorCheckListDocuments_Result> AuditorCheckListDocument = new List<Sp_GetAuditorCheckListDocuments_Result>();
                AuditorCheckListDocument = GetFileDataGetInternalAuditDocumentsView(ChecklistID).ToList();
                rptComplianceDocumnets.DataSource = AuditorCheckListDocument.OrderBy(x => x.FileID).ToList();
                rptComplianceDocumnets.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Sp_GetAuditorCheckListDocuments_Result> GetFileDataGetInternalAuditDocumentsView(int ChecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.Sp_GetAuditorCheckListDocuments(AuthenticationHelper.CustomerID, ChecklistID)                              
                                select row).ToList();
                return fileData;
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                saveopo.Value = "true";
                if (fuSampleEvidence.HasFile)
                {
                    bool exitstfilename = Exists(fuSampleEvidence.FileName);
                    if (exitstfilename)
                    {
                      
                        cvDuplicateEntryPopPup.IsValid = false;
                        cvDuplicateEntryPopPup.ErrorMessage = "filename already exists please changes name of file.";
                        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open')", true);
                        return;
                    }
                    AuditSampleDocumentData Obj1 = new AuditSampleDocumentData()
                    {
                        ChecklistID = Convert.ToInt32(ViewState["checklistid"]),
                        Name = fuSampleEvidence.FileName,
                        FileData = fuSampleEvidence.FileBytes,
                        FilePath = "~/VenderAudit/SampleDocument/" + fuSampleEvidence.FileName,
                        CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    CreateSampleFile(Obj1);
                    fuSampleEvidence.SaveAs(Server.MapPath("~/VenderAudit/SampleDocument/" + fuSampleEvidence.FileName));
                    upCustomerBranches.Update();
                    BindDocument(Convert.ToInt32(ViewState["checklistid"]));
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open')", true);
                }         
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool BlukClientMapplingCompliance(List<AuditChecklistComplianceMapping> ACCMlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    int Count = 0;
                    ACCMlist.ForEach(EachACCM =>
                    {
                        AuditChecklistComplianceMapping auditcheklistcompliancemapping = (from row in entities.AuditChecklistComplianceMappings
                                                                                  where row.ChecklistID == EachACCM.ChecklistID
                                                                                  &&  row.ComplianceID == EachACCM.ComplianceID
                                                                                  select row).FirstOrDefault();

                        if (auditcheklistcompliancemapping != null)
                        {
                        }
                        else
                        {                           
                            AuditChecklistComplianceMapping newauditcheklistcompliancemapping = new AuditChecklistComplianceMapping();
                            newauditcheklistcompliancemapping.CustomerID = EachACCM.CustomerID;
                            newauditcheklistcompliancemapping.ChecklistID = EachACCM.ChecklistID;
                            newauditcheklistcompliancemapping.VendorID = EachACCM.VendorID;
                            newauditcheklistcompliancemapping.ComplianceID = EachACCM.ComplianceID;
                            newauditcheklistcompliancemapping.CategoryID =EachACCM.CategoryID;
                            newauditcheklistcompliancemapping.CreatedBy = EachACCM.CreatedBy;
                            newauditcheklistcompliancemapping.CreatedOn = EachACCM.CreatedOn;
                            newauditcheklistcompliancemapping.ComplianceType = EachACCM.ComplianceType;
                            newauditcheklistcompliancemapping.Isactive = false;                        
                            entities.AuditChecklistComplianceMappings.Add(newauditcheklistcompliancemapping);                            
                            Count++;
                        }
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static void CreateSampleFile(AuditSampleDocumentData form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                form.CreatedOn = DateTime.Now;                
                entities.AuditSampleDocumentDatas.Add(form);
                entities.SaveChanges();
            }

        }
        public void rptComplianceDocumnets_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void rptComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }                
                else if (e.CommandName.Equals("Delete"))
                {                   
                    Delete(Convert.ToInt32(e.CommandArgument), AuthenticationHelper.UserID);                    
                }                               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnets");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbutton");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lbtLinkDocbutton);
            }
        }
        protected void rptComplianceDocumnets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                rptComplianceDocumnets.PageIndex = e.NewPageIndex;
                BindDocument(Convert.ToInt32(ViewState["checklistid"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static AuditSampleDocumentData GetAuditSampleDocumentDataByID(long fileid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.AuditSampleDocumentDatas
                            where row.ID == fileid
                            select row).FirstOrDefault();

                return form;
            }
        }        
        public static void Delete(int fileID, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                AuditSampleDocumentData ltlmToUpdate = (from row in entities.AuditSampleDocumentDatas
                                                        where row.ID == fileID
                                                        select row).FirstOrDefault();


                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.IsDeleted = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public void DownloadFile(int fileId)
        {
            try
            {
                var file = GetAuditSampleDocumentDataByID(fileId);
                Response.Buffer = true;
                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download

                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

            }
            catch (Exception)
            {
                throw;

            }
        }
        protected void GridRemarks_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                GridRemarks.PageIndex = e.NewPageIndex;
                BindComplianceDetails(Convert.ToInt32(ViewState["checklistid"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void txtComplianceID_TextChanged(object sender, EventArgs e)
        {

        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            bindChecklistGrid();
        }
        public static void Update(AuditChecklist auditchecklist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {             
                AuditChecklist auditToUpdate = (from row in entities.AuditChecklists
                                               where row.ID == auditchecklist.ID
                                               select row).FirstOrDefault();

                auditToUpdate.Name = auditchecklist.Name;                
                entities.SaveChanges();                               
            }
        }
        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindChecklistGrid();
        }

        protected void lnksampleForm_Click(object sender, EventArgs e)
        {
            WebClient req = new WebClient();
            HttpResponse response = HttpContext.Current.Response;
            string filePath = Server.MapPath("~/VenderAudit/SampleFormat/SampleChecklistFormat.xlsx");
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment;filename=SampleChecklistFormat.xlsx");
            byte[] data = req.DownloadData(filePath);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            response.BinaryWrite(data);
            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Flush();
            Response.End();
        }

        //Added by Amita as on 31JAN2019
        protected void GridRemarks_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int complianceID = Convert.ToInt32(e.CommandArgument);
                int checklistID = Convert.ToInt32(ViewState["checklistid"]);
                string complianceType = Convert.ToString(ComplianceType.SelectedValue);
                long vendorID = Convert.ToInt64(ddlVendor.SelectedValue);

                if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    DeleteAuditChecklistComplianceMapping(complianceID);
                    BindComplianceDetails(checklistID);
                    saveopo.Value = "true";
                    cvDuplicateEntryPopPup.IsValid = false;
                    cvDuplicateEntryPopPup.ErrorMessage = "Record Deleted successfully.";

                    //long mappingID = ExistsAuditChecklistComplianceMapping(complianceID, checklistID, complianceType, vendorID);
                    //if (mappingID != 0)
                    //{
                    //    DeleteAuditChecklistComplianceMapping(mappingID);
                    //    BindComplianceDetails(checklistID);
                    //    saveopo.Value = "true";
                    //    cvDuplicateEntryPopPup.IsValid = false;
                    //    cvDuplicateEntryPopPup.ErrorMessage = "Record Deleted successfully.";
                    //}
                    //else
                    //{

                    //    saveopo.Value = "true";
                    //    cvDuplicateEntryPopPup.IsValid = false;
                    //    cvDuplicateEntryPopPup.ErrorMessage = "Mapped Compliance's Checklist is scheduled, mapping can not deleted.";
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryPopPup.IsValid = false;
                cvDuplicateEntryPopPup.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        
        //Added by Amita as on 01FEB2019
        public static long ExistsAuditChecklistComplianceMapping(long complianceID,int checklistID,string complianceType,long vendorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long MappedAuditchklstID, mappingID = 0;

                MappedAuditchklstID = (from row in entities.AuditChecklists                                     
                                      join row1 in entities.AuditCheckListMappings on row.ID equals row1.ChecklistID
                                      join row2 in entities.AuditDetails on row1.AuditDetailID equals row2.ID
                                       where row.Isactive == false && row.VendorID == vendorID  
                                       && row.ID == checklistID                                    
                                       select row.ID).FirstOrDefault();

                if (MappedAuditchklstID == 0)
                {
                    mappingID = (from row in entities.AuditChecklistComplianceMappings
                                 where row.ComplianceID == complianceID
                                 && row.ChecklistID == checklistID
                                 && row.ComplianceType == complianceType                                
                                 select row.ID).FirstOrDefault();
                }

                if (mappingID != 0)
                    return mappingID;
                else
                    return 0;
            }
        }

        public static void DeleteAuditChecklistComplianceMapping(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var idToDelete = (from row in entities.AuditChecklistComplianceMappings
                //                            where row.ID == mappingID
                //                           select row).FirstOrDefault();

                //entities.AuditChecklistComplianceMappings.Remove(idToDelete);
                //entities.SaveChanges();

                var idToDelete = (from row in entities.AuditChecklistComplianceMappings
                                  where row.ComplianceID == complianceID
                                  select row).ToList();
                foreach (var item in idToDelete)
                {
                    entities.AuditChecklistComplianceMappings.RemoveRange(entities.AuditChecklistComplianceMappings.Where(x => x.ID == item.ID));
                    entities.SaveChanges();
                }

            }
        }
    }
}

