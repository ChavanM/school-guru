﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VendorAudit.Master" AutoEventWireup="true" CodeBehind="CheckListDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages.CheckListDashboard" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <meta name="author" content="AVANTIS - Development Team">

     <link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
        <link href="../../newcss/spectrum.css" rel="stylesheet" />
        <script type="text/javascript" src="../../newjs/spectrum.js"></script>

        <style type="text/css">
            #dailyupdates .bx-viewport {
                height: 190px !important;
            }

            .info-box:hover {
                color: #FF7473;
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }

            .function-selector-radio-label {
                font-size: 12px;
            }

            .colorPickerWidget {
                padding: 10px;
                margin: 10px;
                text-align: center;
                width: 360px;
                border-radius: 5px;
                background: #fafafa;
                border: 2px solid #ddd;
            }

            #ContentPlaceHolder1_grdGradingRepportSummary.table tr td {
                border: 1px solid white;
            }

            .TMB {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 16.5% !important;
            }

            .TMB1 {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 19.5% !important;
            }

            .TMBImg {
                padding-left: 0px !important;
                padding-right: 0px !important;
                margin-left: -7%;
                margin-right: -3%;
            }

            .clscircle {
                margin-right: 7px !important;
            }

            .fixwidth {
                width: 20% !important;
            }

            .badge {
                font-size: 10px !important;
                font-weight: 200 !important;
            }

            .responsive-calendar .day {
                width: 13.7% !important;
                height: 45px;
            }

                .responsive-calendar .day.cal-header {
                    border-bottom: none !important;
                    width: 13.9% !important;
                    font-size: 17px;
                    height: 25px;
                }

            #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            .bx-viewport {
                height: 285px !important;
            }

            #dailyupdates .bx-viewport {
                height: 190px !important;
            }

            .graphcmp {
                margin-left: 36%;
                font-size: 16px;
                margin-top: -3%;
                color: #666666;
                font-family: 'Roboto';
            }

            .days > div.day {
                margin: 1px;
                background: #eee;
            }

            .overdue ~ div > a {
                background: red;
            }

            .info-box {
                min-height: 95px !important;
            }

            .Dashboard-white-widget {
                padding: 5px 10px 0px !important;
            }

            .dashboardProgressbar {
                display: none;
            }

            .TMBImg > img {
                width: 47px;
            }

            #reviewersummary {
                height: 150px;
            }

            #performersummary {
                height: 150px;
            }

            #eventownersummary {
                height: 150px;
            }

            #performersummarytask {
                height: 150px;
            }

            #reviewersummarytask {
                height: 150px;
            }

            div.panel {
                margin-bottom: 12px;
            }

            .panel .panel-heading .panel-actions {
                height: 25px !important;
            }

            hr {
                margin-bottom: 8px;
            }

            .panel .panel-heading h2 {
                font-size: 18px;
            }

            td > label {
                padding: 6px;
            }

            .radioboxlist radioboxlistStyle {
                font-size: x-large;
                padding-right: 20px;
            }

            span.input-group-addon {
                padding: 0px;
            }

            td > label {
                padding: 3px 4px 0 4px;
                margin-top: -1%;
            }

            .nav-tabs > li > a {
                color: #333 !important;
            }

            .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                color: #1fd9e1 !important;
            }

        </style>
    <style type="text/css">
        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

         .btuploadss {
            background-image: url(../../Images/icon-updated.png);
            border: 0px;
            width: 30px;
            height: 30px;
            background-color: transparent;
            background-repeat: no-repeat;
            float: left;
        }

        .btnview {
            background-image: url(../Images/view-icon-new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .btnss {
            background-image: url(../../Images/Save-icon.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }


        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }
    </style>

    <script type="text/javascript">

        function gotoBack() {
            window.location.href = '../aspxpages/VendorAuditDashboard.aspx?VendorIDBCK=<% =VendorIDBCK%>&AuditIDBCK=<% =AuditIDBCK%>&ddlAuditTypeBCK=<% =ddlAuditTypeBCK%>&ddlPageSizeBCK=<% =ddlPageSizeBCK%>&location=<% =location%>';
        }

         function fopenSampleFile(file) { 
            $('#ContentPlaceHolder1_docViewerAll').attr('src', "../Common/blank.html");
            $('#SampleFilePopUp').modal('show');
            $('#ContentPlaceHolder1_docViewerAll').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

          function fopenSampleFileRecord(file) { 
              $('#ContentPlaceHolder1_docViewerAllRecord').attr('src', "../Common/blank.html");
            $('#SampleFilePopUpRecord').modal('show');
              $('#ContentPlaceHolder1_docViewerAllRecord').attr('src', "../../docviewer.aspx?docurl=" + file);
          }

        function fopendocfile() {
            $('#SampleFilePopUp').modal('show');
        }

        function openModal() {
            $('#AuditPopUp').modal('show');

            return true;
        }

        function ShowDetails() {
            $('#divViewDocument').modal('show');

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix" style="height: 0px"></div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                                <%--Header Section--%>
                                <div class="clearfix"></div>

                                <div class="panel-body">
                                    <div class="col-md-12 colpadding0">

                                         <div class="col-md-12 colpadding0" style="font-size: 24px;text-align: left; float: left;color:black;">
                                             <asp:Label id="AuditName" runat="server" Text="" style="color:#999;"></asp:Label>
                                           </div>
                                        <div class="clearfix"></div>

                                        <div class="col-md-12 colpadding0" style="text-align: right; float: right;padding-top: 12px;">
                                            
                                            <div class="col-md-10 colpadding0" style="width:69.666667%;">
                                                <div class="col-md-2 colpadding0 entrycount">
                                            <div class="col-md-3 colpadding0">
                                                <p style="color:#999; margin-top:5px"> Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 64px; float: left;margin-left:10px;">
                                                <asp:ListItem Text="5"/>
                                                <asp:ListItem Text="10" Selected="True" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                          
                                        </div>
                                        <div class="col-md-8 colpadding0"> 
                                        <div style="float:left;margin-right: 1%;display:none;">
                                            <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 373px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                            CssClass="txtbox" />                                                       
                                            <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>  
                                        </div>           
                                    </div>
                                                 <div class="col-md-2 colpadding0 entrycount" style="float:left;margin-right: -1%;display:none;">
                                                     <asp:DropDownList runat="server" ID="ddlAuditType" class="form-control m-bot15 search-select" style="width:105px;" 
                                                         AutoPostBack="true">                
                                                         <asp:ListItem Text="All" Value="-1" Selected="True"/>
                                                <asp:ListItem Text="Open" Value="0" />
                                                <asp:ListItem Text="Closed" Value="1" />
                                                    </asp:DropDownList>         
                                                    </div>                                               
                                            </div>    
                                             <div class="col-md-2 colpadding0" style="width: 9.333333%; float:right">
                                                <div class="col-md-6 colpadding0">                                                    
                                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" Style="margin-left:33px !important;display:none;" runat="server" Text="Apply" onclick="btnSearch_Click"/>
                                                     <asp:Button ID="btnBack" CausesValidation="false" class="btn btn-search" Style="margin-left:33px !important;" runat="server" Text="Back" onclientclick="gotoBack()"/>
                                                </div>
                                            </div>              
                                        </div>
                                       
                                         <div class="clearfix"> 
   <div runat="server" id="DivRecordsScrum" style="float: right;" class="AdvanceSearchScrum">
             <p style="padding-right: 0px !Important;">
                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                </p>
             </div>

                                    </div>
                                </div>
                                <div class="tab-content ">
                                   <div runat="server" id="performerdocuments"  class="tab-pane active">

                          <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <asp:ValidationSummary runat="server"
                                        ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                    <asp:Label runat="server" ID="Label2" ForeColor="Red"></asp:Label>
                                </div>
                                <div>
                                      <asp:GridView runat="server" ID="grdChecklist" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                                        AllowSorting="true" OnRowUpdating="grdChecklist_RowUpdating" OnRowCommand="grdChecklist_RowCommand"
                                                        DataKeyNames="AuditDetailID" OnRowDataBound="grdChecklist_RowDataBound" AllowPaging="True" CssClass="table" PageSize="10" Width="100%">
                                                        <Columns>
                                                             <asp:TemplateField>
                                                           <HeaderTemplate>                                                              
                                                                <asp:CheckBox ID="checkAll" runat="server"  AutoPostBack="true" OnCheckedChanged="checkAll_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                  <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Clause">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                         <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("SrNo")%>'></asp:Label>
                                                                         <asp:Label ID="lblAuditDetailID" Visible="false" runat="server" ToolTip='<%# Eval("AuditDetailID") %>' Text='<%# Eval("AuditDetailID")%>'></asp:Label>
                                                                         <asp:Label ID="lblChecklistMappingID" Visible="false" runat="server" ToolTip='<%# Eval("CheckListMappingID") %>' Text='<%# Eval("CheckListMappingID")%>'></asp:Label>
                                                                         <asp:Label ID="lblChecklistID" Visible="false" runat="server" ToolTip='<%# Eval("CheckListID") %>' Text='<%# Eval("CheckListID")%>'></asp:Label>
                                                                         <asp:Label ID="lblAuditStartDate" Visible="false" runat="server" ToolTip='<%# Eval("AuditStartDate") %>' Text='<%# Eval("AuditStartDate")%>'></asp:Label>
                                                                         <asp:Label ID="lblAuditEndDate" Visible="false" runat="server" ToolTip='<%# Eval("AuditEndDate") %>' Text='<%# Eval("AuditEndDate")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Category Name" ItemStyle-Width="10px">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 110px;">
                                                                        <asp:Label ID="lblCategoryName" runat="server" Text='<%# Eval("CategoryName")%>' ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="CheckList Name" ItemStyle-Width="15px">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 170px;">
                                                                        <asp:Label ID="lblCheckListName" runat="server" Text='<%# Eval("CheckListName")%>' ToolTip='<%# Eval("CheckListName") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                             </asp:TemplateField>
                                                             <asp:TemplateField ItemStyle-Width="15px" HeaderText="Document">
                                                             <ItemTemplate>
                                                              <asp:UpdatePanel runat="server" ID="upfile" UpdateMode="Conditional">
                                                                 <ContentTemplate>
                                                                    <asp:FileUpload ID="docFileUpload" Style="width: 185px;" runat="server"/>                                                                                                                
                                                                    <asp:Button ID="UploadDocument" runat="server" 
                                                                       OnClick="UploadDocument_Click" CommandName="DocumentUpload"  CssClass="btuploadss"  data-toggle="tooltip" data-placement="top" ToolTip="Upload"
                                                                         CommandArgument='<%# Eval("CheckListID") %>' CausesValidation="true" /> 
                                                                    
                                                                     <asp:LinkButton ID="lbkDownload" runat="server" ToolTip="Download" CommandName="Download" CommandArgument='<%# Eval("CheckListID") + "," + Eval("CheckListMappingID") %>'><img src="../../Images/download_icon_new.png" alt="Download"/></asp:LinkButton>
                                                                     <asp:LinkButton ID="lbkView" runat="server" ToolTip="View" CommandName="View"  CommandArgument='<%# Eval("CheckListID") + "," + Eval("CheckListMappingID") %>'><img src="../../Images/view-doc.png" alt="View"/></asp:LinkButton>
                                                                                                                                                                                  
                                                                   </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="UploadDocument" />
                                                                              <asp:PostBackTrigger ControlID="lbkDownload" />
                                                                        </Triggers>
                                                                </asp:UpdatePanel>
                                                              </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Record">
                                                             <ItemTemplate>
                                                              <asp:UpdatePanel ID="upFileUploadPanel1" runat="server">
                                                                <ContentTemplate>
                                                                <asp:LinkButton ID="lbkDownloadRecord" runat="server" ToolTip="Download"  CommandName="DownloadRecord" CommandArgument='<%# Eval("AuditDetailID") + "," + Eval("CheckListMappingID") + "," + Eval("AuditStartDate") + "," + Eval("AuditEndDate") + "," + Eval("CheckListName") %>'><img src="../../Images/download_icon_new.png" alt="Download"/></asp:LinkButton>
                                                                 <asp:LinkButton ID="lbkRecordView" runat="server" ToolTip="View" CommandName="ViewRecord"  CommandArgument='<%# Eval("AuditDetailID") + "," + Eval("CheckListMappingID") + "," + Eval("AuditStartDate") + "," + Eval("AuditEndDate") + "," + Eval("CheckListName") %>'><img src="../../Images/view-doc.png" alt="View"/></asp:LinkButton> 
                                                                </ContentTemplate>
                                                                  <Triggers>
                                                                     <asp:PostBackTrigger ControlID="lbkDownloadRecord" />
                                                                  </Triggers>
                                                            </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Observation">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtObservation" runat="server" CssClass="form-control" Style="text-align: left;width:280px;"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 25px;">
                                                                        <asp:Button ID="btn_Update" runat="server" ToolTip="Save" CommandName="Update" CssClass="btnss" />
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Details">
                                                             <ItemTemplate>
                                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 25px;">
                                                               <asp:LinkButton ID="lbkViewDetails" runat="server" CommandArgument='<%# Eval("AuditDetailID") + "," + Eval("CheckListID") + "," + Eval("AuditStartDate") + "," + Eval("AuditEndDate") + "," + Eval("CheckListName") %>' CommandName="ViewDetails" ToolTip="View" ><img src="../../Images/view-doc.png" alt="View"/></asp:LinkButton>
                                                                </div>
                                                             </ItemTemplate>
                                                       </asp:TemplateField> 

                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Right" />
                                        <RowStyle CssClass="clsROWgrid"   />
                                        <HeaderStyle CssClass="clsheadergrid"    />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                         <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                                    </asp:GridView>
                                   
                                </div>
                            </ContentTemplate>
                       
                        </asp:UpdatePanel>
                                        <div style="margin-bottom: 4px">
                                                <table width="100%" style="height: 10px">
                                                    <tr>
                                                        <td>
                                                            <div style="margin-bottom: 4px">
                                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                                    <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                      <div class="clearfix"> </div>                                                   
                                    </div>
                               
                                   <div class="col-md-12 colpadding0">
                                            <div class="col-md-6 colpadding0">
                                                <asp:Button Text="Save as Draft" style="margin-left: 10px;" runat="server" ID="btnSaveDraft" ValidationGroup="ComplianceInstanceValidationGroup1" OnClick="btnSaveDraft_Click" CssClass="btn btn-search" />
                                               <asp:Button Text="Save and Close" style="margin-left: 10px;" runat="server" ID="btnSaveClose" ValidationGroup="ComplianceInstanceValidationGroup1" OnClick="btnSaveClose_Click" OnClientClick="return confirm('Are you sure you want to close this audit?');" CssClass="btn btn-search" />
                                            </div>
                                            <div class="col-md-6 colpadding0">
                                                <div class="table-paging" style="margin-bottom:20px">
                                                    <asp:ImageButton ID="lBPrevious" CssClass ="table-paging-left"  runat="server" ImageUrl ="../../img/paging-left-active.png" onclick="lBPrevious_Click"/>

                                                    <div class="table-paging-text">
                                                        <p><asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label></p> 
                                                    </div>
                                                    <asp:ImageButton ID="lBNext" CssClass ="table-paging-right"  runat="server" ImageUrl ="../../img/paging-right-active.png" onclick="lBNext_Click"/>
                                                </div>
                                            </div>
                                   </div>
                            </div>    
                                                             
                        </section>
                    </div>
                
                
                
                  <div>
                   
                </div>
                </div>
            </div>
            <tr id="trErrorMessage" runat="server" visible="true">
                <td colspan="3" style="background-color: #e9e1e1;">
                    <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </td>
            </tr>

           

        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="SampleFilePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <%-- data-dismiss-modal="modal2"--%>
                <button type="button" class="close" onclick="$('#SampleFilePopUp').modal('hide');" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
               <%-- <div style="width: 100%;">--%>
                    <div style="float: left; width: 10%">
                        <table width="100%" style="text-align: left; margin-left: 5%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptComplianceSampleView" runat="server" OnItemCommand="rptComplianceSampleView_ItemCommand"
                                                    OnItemDataBound="rptComplianceSampleView_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Documents</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ChecklistID") %>' ID="lblSampleView"
                                                                            runat="server" ToolTip='<%# Eval("Name") + "," + Eval("UserName") + "," + Eval("CreatedOn")%>' Text='<%# Eval("Name") %>'>
                                                                             
                                                                        </asp:LinkButton>
                                                                     
                                                                        <%--  <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ChecklistID") %>' ID="LinkButton1"
                                                                            runat="server" onclick="lblSampleView_Click" Text="Hide">
                                                                             
                                                                        </asp:LinkButton>--%>
            
                                                                      
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                            <Triggers>
                                               <%-- <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />--%>
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div style="float: left; width: 90%">
                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                            <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                        </fieldset>
                    </div>
               <%-- </div>--%>
            </div>
        </div>
    </div>
</div>



      <div class="modal fade" id="SampleFilePopUpRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="$('#SampleFilePopUpRecord').modal('hide');" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
                    <div style="float: left; width: 10%">
                        <table width="100%" style="text-align: left; margin-left: 5%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptComplianceSampleViewRecord" runat="server" OnItemCommand="rptComplianceSampleViewRecord_ItemCommand"
                                                    OnItemDataBound="rptComplianceSampleViewRecord_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Documents</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("AuditDetailID")+ ","+ Eval("ScheduledOnID")+ ","+ Eval("ChecklistID")+ ","+ Eval("ChecklistMappingID") %>' ID="lblSampleRecordView"
                                                                            runat="server" ToolTip='<%# Eval("FileName") + "," + Eval("CreatedByText") +"," + Eval("Dated")%>' Text='<%# Eval("FileName").ToString().Substring(0,12) %>'></asp:LinkButton>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="lblSampleRecordView" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div style="float: left; width: 90%">
                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                            <iframe src="about:blank" id="docViewerAllRecord" runat="server" width="100%" height="550px"></iframe>
                        </fieldset>
                    </div>
            </div>
        </div>
    </div>
</div>


         <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 470px;">
                        <div class="col-md-12 colpadding0">
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                        </div>
                     
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-2 colpadding0">
                                <table class="col-md-12">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                 <asp:Panel ID="pnl" ScrollBars="Vertical" Height="400px" runat="server">
                                                <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                       
                                                            <asp:GridView runat="server" ID="grdDetails" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                                                AllowSorting="true" AllowPaging="false" Height="400px" CssClass="table" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ShortDescription" ItemStyle-Width="15px">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription")%>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="LicenseNo" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 125px;">
                                                                                <asp:Label ID="lblLicenseNo" runat="server" Text='<%# Eval("LicenseNo")%>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="LicenseTitle" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 125px;">
                                                                                <asp:Label ID="lblLicenseTitle" runat="server" Text='<%# Eval("LicenseTitle")%>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="ForMonth" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth")%>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Status" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status")%>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                   
                                                                </Columns>

                                                            </asp:GridView>
                                                   
                                                    </ContentTemplate>
                                                  
                                                </asp:UpdatePanel>
                                                          </asp:Panel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
