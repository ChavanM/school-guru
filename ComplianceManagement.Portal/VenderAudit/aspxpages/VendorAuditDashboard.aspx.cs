﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages
{
    public partial class VendorAuditDashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindVendorList();
                bindAuditList();
                BindLocationFilter();
                ddlAuditType.SelectedValue = "-1";
                int ddlStatusValue = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditStatus"]))
                {
                    ddlStatusValue = Convert.ToInt32(Request.QueryString["AuditStatus"]);
                    if (ddlStatusValue == 0)
                    {
                        ddlAuditType.SelectedValue = "0";
                    }
                    else if (ddlStatusValue == 1)
                    {
                        ddlAuditType.SelectedValue = "1";
                    }
                    else
                    {
                        ddlAuditType.SelectedValue = "-1";
                    }
                }

                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["VendorIDBCK"])))
                {
                    ddlVendor.SelectedValue = Convert.ToString(Request.QueryString["VendorIDBCK"]);                     
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AuditIDBCK"])))
                {
                    ddlAudit.SelectedValue = Convert.ToString(Request.QueryString["AuditIDBCK"]);                    
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ddlAuditTypeBCK"])))
                {
                    ddlAuditType.SelectedValue = Convert.ToString(Request.QueryString["ddlAuditTypeBCK"]);                     
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ddlPageSizeBCK"])))
                {
                    ddlPageSize.SelectedValue = Convert.ToString(Request.QueryString["ddlPageSizeBCK"]);                     
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["location"])))
                {
                    string location = Convert.ToString(Request.QueryString["location"]);
                    tbxFilterLocation.Text = location;
                }

              
              
               
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ID";
                long AuditorID = AuthenticationHelper.UserID;
                BindAuditDetail(AuditorID);
                SetAuditCounts(AuditorID);
            
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        public static List<AuditVendor> GetVendorList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var VendorDetails = (from row in entities.AuditVendors
                                     where row.Isactive == false
                                          && row.CustomerID == customerID
                                     select row).ToList();

                return VendorDetails;
            }
        }
        private void bindVendorList()
        {
            try
            {
                ddlVendor.DataTextField = "Name";
                ddlVendor.DataValueField = "ID";

                ddlVendor.DataSource = GetVendorList();
                ddlVendor.DataBind();

                ddlVendor.Items.Insert(0, new ListItem("Select Vendor", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<AuditDetail> GetAuditList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int userID = -1;
                userID = Convert.ToInt32(AuthenticationHelper.UserID);

                var AuditDetails = (from row in entities.AuditDetails
                                    where row.Isactive == false && row.AuditorID == userID
                                         && row.CustomerID == customerID
                                    select row).ToList();

                return AuditDetails;
            }
        }
        private void bindAuditList()
        {
            try
            {
                ddlAudit.DataTextField = "AuditName";
                ddlAudit.DataValueField = "ID";

                ddlAudit.DataSource = GetAuditList();
                ddlAudit.DataBind();

                ddlAudit.Items.Insert(0, new ListItem("Select Audit", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void BindAuditDetail(long AuditorID)
        {
            try
            {
                String location = tvFilterLocation.SelectedNode.Text;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CustomerBranchID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var auditDetail = (from row in entities.AuditDetails
                                       join row1 in entities.AuditVendors on row.VendorID equals row1.ID
                                       join row2 in entities.CustomerBranches on row.BranchID equals row2.ID
                                       where row.Isactive == false && row.CustomerID == CustomerBranchID
                                       && row.AuditorID == AuditorID
                                       select new
                                       {
                                           row.ID
                                                    ,
                                           row.VendorID
                                                    ,
                                           VendorName = row1.Name
                                                    ,
                                           Branch = row2.Name
                                                    ,
                                           row.AuditName
                                                    ,
                                           row.AuditStartDate
                                                    ,
                                           row.AuditEndDate,
                                           row.AuditCloseFlag

                                       });

                    var AuditDetail = auditDetail.ToList();
                    if (AuditDetail.Count > 0)
                    {
                        if (Convert.ToInt64(ddlAudit.SelectedValue) != -1)
                        {
                            AuditDetail = AuditDetail.Where(x => x.ID == Convert.ToInt64(ddlAudit.SelectedValue)).ToList();
                        }
                        if (Convert.ToInt64(ddlVendor.SelectedValue) != -1)
                        {
                            AuditDetail = AuditDetail.Where(x => x.VendorID == Convert.ToInt64(ddlVendor.SelectedValue)).ToList();
                        }

                        if (Convert.ToInt32(ddlAuditType.SelectedValue) == 0)
                        {
                            AuditDetail = AuditDetail.Where(x => x.AuditCloseFlag != true).ToList();
                        }
                        if (Convert.ToInt32(ddlAuditType.SelectedValue) == 1)
                        {
                            AuditDetail = AuditDetail.Where(x => x.AuditCloseFlag == true).ToList();
                        }


                        if (!string.IsNullOrEmpty(location))
                        {
                            if (location != "Entity/Sub-Entity/Location")
                                AuditDetail = AuditDetail.Where(entry => entry.Branch == location).ToList();
                        }

                    }
                    grdAuditDetail.DataSource = AuditDetail;
                    grdAuditDetail.DataBind();
                    Session["TotalRows"] = AuditDetail.Count;
                    //upCompliancesList.Update();
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
               
                var LocationList = CustomerBranchManagement.GetAuditorAssignedLocationList(AuthenticationHelper.UserID, customerID);
               
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

            long AuditorID = AuthenticationHelper.UserID;
            BindAuditDetail(AuditorID);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindAuditCheckList(long AuditDetailID)
        {
            try
            {
                grdChecklist.DataSource = null;
                grdChecklist.DataBind();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CustomerBranchID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var auditDetail = (from row in entities.AuditCheckListMappings
                                       join row1 in entities.AuditDetails on row.AuditDetailID equals row1.ID
                                       join row2 in entities.AuditChecklists on row.ChecklistID equals row2.ID
                                       join row3 in entities.AuditCategories on row2.CategoryID equals row3.ID
                                       where row.Isactive == false && row1.CustomerID == CustomerBranchID
                                       && row1.ID == AuditDetailID
                                       select new
                                       {
                                           AuditDetailID = row1.ID
                                                    ,
                                           CheckListID = row2.VendorID
                                                    ,
                                           CheckListName = row2.Name
                                                    ,
                                           CategoryName = row3.Name
                                                    ,
                                           SequenceID = row2.SequanceID
                                                    ,
                                           SrNo = row2.SrNo
                                                    ,
                                           Observation = row.Observation

                                       });

                    var AuditDetail = auditDetail.ToList();

                    grdChecklist.DataSource = AuditDetail;
                    grdChecklist.DataBind();
                    upChecklistDetails.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void SetAuditCounts(long AuditorID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CustomerBranchID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var CloseAudit = (from row in entities.AuditDetails
                                      where row.Isactive == false && row.CustomerID == CustomerBranchID
                                      && row.AuditCloseFlag == true && row.AuditorID == AuditorID
                                      select row).ToList();

                    var OpenAudit = (from row in entities.AuditDetails
                                     where row.Isactive == false && row.CustomerID == CustomerBranchID
                                     && row.AuditCloseFlag != true && row.AuditorID == AuditorID
                                     select row).ToList();

                    divOpencount.InnerText = Convert.ToString(OpenAudit.Count);
                    divClosedcount.InnerText = Convert.ToString(CloseAudit.Count);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdAuditDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ddlAuditTypeDRP = Convert.ToInt32(ddlAuditType.SelectedValue);
                int ddlPageSizeDRP = Convert.ToInt32(ddlPageSize.SelectedValue);
                string Location = tvFilterLocation.SelectedNode.Text;

                if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long AuditDetailID = Convert.ToInt64(commandArgs[0]);
                    int VenderID = Convert.ToInt32(commandArgs[1]);
                    int ddlVendorDRP = Convert.ToInt32(ddlVendor.SelectedValue);
                    int ddlAuditDRP = Convert.ToInt32(ddlAudit.SelectedValue);

                    Response.Redirect("~/VenderAudit/aspxpages/ChecklistDashboard.aspx?AuditID="
                        + AuditDetailID + " &ddlVendorDRP= " + ddlVendorDRP + "&ddlAuditDRP=" + ddlAuditDRP + "&ddlAuditTypeDRP=" + ddlAuditTypeDRP
                        + "&ddlPageSizeDRP=" + ddlPageSizeDRP + "&Location=" + Location, false);
                    //Response.Redirect("~/VenderAudit/aspxpages/ChecklistDashboard.aspx?AuditID=" + AuditDetailID, false);              
                    BindAuditCheckList(AuditDetailID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            long AuditorID = AuthenticationHelper.UserID;
            BindAuditDetail(AuditorID);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdAuditDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAuditDetail.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                long AuditorID = AuthenticationHelper.UserID;
                BindAuditDetail(AuditorID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdAuditDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAuditDetail.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                long AuditorID = AuthenticationHelper.UserID;
                BindAuditDetail(AuditorID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdAuditDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAuditDetail.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                long AuditorID = AuthenticationHelper.UserID;
                BindAuditDetail(AuditorID);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            long AuditorID = AuthenticationHelper.UserID;
            BindAuditDetail(AuditorID);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        protected void ddlAuditType_SelectedIndexChanged(object sender, EventArgs e)
        {
            long AuditorID = AuthenticationHelper.UserID;
            BindAuditDetail(AuditorID);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        protected void ddlAudit_SelectedIndexChanged(object sender, EventArgs e)
        {
            long AuditorID = AuthenticationHelper.UserID;
            BindAuditDetail(AuditorID);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }
    }
}