﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.CertificateData;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Certificate
{
    public partial class ComplianceOfficerOwnerMapping : System.Web.UI.Page
    {
        public static List<long?> OwnerList = new List<long?>();
        protected int UserID = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
               
                if(AuthenticationHelper.Role=="IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    BindCustomer(UserID);
                }
                else
                {
                    UserID = AuthenticationHelper.UserID;
                    BindCustomer(UserID);
                }
                
                BindGrid();
                if(AuthenticationHelper.Role== "CADMN")
                {
                    divcustomer.Visible = false;
                }
                else
                {
                    divcustomer.Visible = true;
                }
            }

        }

        private void BindGrid()
        {
            int customerid = -1;
            if (AuthenticationHelper.Role=="IMPT")
            {
                 customerid = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
            }
            else
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
           
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
              

                var data1 = (from row in entities.SP_GetComplianceOfficerMappingDetails(customerid)
                            select row).ToList();


                var details = data1.GroupBy(entry=>entry.UserName).Select(x => x.First()).ToList();
              
                grdComplianceOfficerMapping.DataSource = details;
                grdComplianceOfficerMapping.DataBind();

               
            }
        }
        private void BindCustomer(int userid)
        {
          if( AuthenticationHelper.Role!= "IMPT")
            {

                ddlCustomerFilter.DataTextField = "Name";
                ddlCustomerFilter.DataValueField = "ID";

                int customerID = -1;

                 customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                ddlCustomerFilter.DataSource = Assigncustomer.GetAssignCustomerData(customerID);
                ddlCustomerFilter.DataBind();
                ddlCustomerFilter.SelectedValue = Convert.ToString(customerID);


                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                ddlCustomerFilter.DataSource = Assigncustomer.GetAssignCustomerData(customerID);
                ddlCustomerFilter.DataBind();
                ddlCustomerFilter.SelectedValue = Convert.ToString(customerID);

            }
          else
            {
                ddlCustomerFilter.DataTextField = "Name";
                ddlCustomerFilter.DataValueField = "ID";

                ddlCustomerFilter.DataSource = GetCustomerList(userid);
                ddlCustomerFilter.DataBind();

                ddlCustomerFilter.Items.Insert(0, new ListItem("< Select >", "-1"));

                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                ddlCustomer.DataSource = GetCustomerList(userid);
                ddlCustomer.DataBind();

                ddlCustomer.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
          

            


        }
        private void BindOfficer(int customerid)
        {


            ddlComplianceOfficer.DataTextField = "Name";
            ddlComplianceOfficer.DataValueField = "ID";

            ddlComplianceOfficer.DataSource = GetOfficerData(customerid);
            ddlComplianceOfficer.DataBind();

            ddlComplianceOfficer.Items.Insert(0, new ListItem("< Select >", "-1"));
           

        }
        private void BindComplianceOwner(int custid)
        {
            try
            {
                rptComplianceOwner.DataSource = GetAllOwner(custid);
                rptComplianceOwner.DataBind();

                foreach (RepeaterItem aItem in rptComplianceOwner.Items)
                {
                    CheckBox chkCompowner = (CheckBox)aItem.FindControl("chkCompowner");

                    if (!chkCompowner.Checked)
                    {
                        chkCompowner.Checked = true;
                    }
                }
                CheckBox OwnerSelectAll = (CheckBox)rptComplianceOwner.Controls[0].Controls[0].FindControl("OwnerSelectAll");
                OwnerSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static object GetCustomerList(int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.CustomerAssignmentDetails
                            join row1 in entities.Customers
                            on row.CustomerID equals row1.ID
                            where row.IsDeleted == false
                            && row.UserID == userid
                            && row1.IsDeleted == false
                            && row1.ComplianceProductType != 1
                            && row1.Status == 1
                            select row1).ToList();

                return data;
            }
        }

        public static List<object> GetOfficerData(int custid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Users
                            where row.IsActive==true && row.Cer_OfficerRoleID ==1
                            && row.CustomerID==custid
                            select new
                            {
                                row.ID,
                                Name=row.FirstName+" "+row.LastName
                            }).ToList();

                return data.ToList<object>();
            }
        }
        public static List<object>GetAllOwner(int custid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Users
                            where row.IsActive == true && row.Cer_OwnerRoleID == 1
                            && row.CustomerID==custid
                            select new
                            {
                                row.ID,
                                Name=row.FirstName+" "+row.LastName,
                            }).ToList();
                return data.ToList<object>();

            }
        }
        protected void upCom_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeCompanyTypeList", string.Format("initializeJQueryUI('{0}', 'dvOwner');", txtComplianceOwner.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideOwnerList", "$(\"#dvOwner\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddOwner_Click(object sender, EventArgs e)
        {
            ddlComplianceOfficer.SelectedValue = "-1";
            ddlCustomer.SelectedValue = "-1";
            foreach (RepeaterItem aItem in rptComplianceOwner.Items)
            {
                CheckBox chkCompowner = (CheckBox)aItem.FindControl("chkCompowner");
                chkCompowner.Checked = false;
                CheckBox OwnerSelectAll = (CheckBox)rptComplianceOwner.Controls[0].Controls[0].FindControl("OwnerSelectAll");
                OwnerSelectAll.Checked = false;
            }
            ViewState["Mode"] = 0;
            upcom.Update();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divOfficerdetailsDialog\").dialog('open')", true);

        }
        protected void grdComplianceOfficerMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int officermappingID = Convert.ToInt32(e.CommandArgument);
            Certificate_OfficerOwnerMapping mappingowner = GetofficeruserMappedID(officermappingID);


            ViewState["OfficermappingID"] = officermappingID;
            ViewState["OfficerUSerid"] = mappingowner.Cer_OfficerUserID;
            if(e.CommandName== "EDIT_OfficerOwner")
            {
                ViewState["Mode"] = 1;
                BindCustomer(AuthenticationHelper.UserID);
                ddlCustomer.SelectedValue = mappingowner.Customerid.ToString();
                int customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                BindOfficer(customerid);
                BindComplianceOwner(customerid);
                ddlComplianceOfficer.SelectedValue = mappingowner.Cer_OfficerUserID.ToString();
                #region Owner
                txtComplianceOwner.Text = "< Select >";
                var vGetownermappingIDs = GetOwnerMappedID(mappingowner.Cer_OfficerUserID);
                foreach (RepeaterItem aItem in rptComplianceOwner.Items)
                {
                    CheckBox chkCompowner = (CheckBox)aItem.FindControl("chkCompowner");
                    chkCompowner.Checked = false;
                    CheckBox OwnerSelectAll = (CheckBox)rptComplianceOwner.Controls[0].Controls[0].FindControl("OwnerSelectAll");

                    for (int i = 0; i <= vGetownermappingIDs.Count - 1; i++)
                    {
                        if (((Label)aItem.FindControl("lblOwnerID")).Text.Trim() == vGetownermappingIDs[i].ToString())
                            chkCompowner.Checked = true;
                    }
                    if ((rptComplianceOwner.Items.Count) == (vGetownermappingIDs.Count))
                        OwnerSelectAll.Checked = true;
                }
                 upcom.Update();
                 ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divOfficerdetailsDialog\").dialog('open')", true);

                #endregion
            }
            else if(e.CommandName== "DELETE_OfficerOwner")
            {

                DeleteComplianceOwnerMapping(mappingowner.Cer_OfficerUserID);
                BindGrid();
            }
        }
        public static Certificate_OfficerOwnerMapping GetofficeruserMappedID(long officermappingID)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                var IndustryMappedIDs = (from row in entities.Certificate_OfficerOwnerMapping
                                         where row.ID == officermappingID
                                         select row).SingleOrDefault();

                return IndustryMappedIDs;
            }

        }
        public static List<int> GetOwnerMappedID(long officeruserid)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                var IndustryMappedIDs = (from row in entities.Certificate_OfficerOwnerMapping
                                         where row.Cer_OfficerUserID ==officeruserid
                                         select row.Cer_OwnerUserID).ToList();

                return IndustryMappedIDs;
            }

        }
        protected void btnRepeaterOwner_Click(object sender, EventArgs e)
        {
            OwnerList.Clear();
            foreach (RepeaterItem aItem in rptComplianceOwner.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkCompowner");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int OwnerID = Convert.ToInt32(((Label)aItem.FindControl("lblOwnerID")).Text.Trim());
                        OwnerList.Add(OwnerID);
                    }

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Certificate_OfficerOwnerMapping newmapping = new Certificate_OfficerOwnerMapping();
            List<Certificate_OfficerOwnerMapping> mappinglist = new List<Certificate_OfficerOwnerMapping>();
            bool isExists=false;
            if ((int)ViewState["Mode"] == 0)
            {
               
              
                #region add        
               foreach (RepeaterItem aItem in rptComplianceOwner.Items)
                {
                    CheckBox chkCompowner = (CheckBox)aItem.FindControl("chkCompowner");

                    if (chkCompowner.Checked)
                    {
                        Certificate_OfficerOwnerMapping mapping = new Certificate_OfficerOwnerMapping()
                        {
                        Cer_OfficerUserID = Convert.ToInt32(ddlComplianceOfficer.SelectedValue),
                        Cer_OwnerUserID = Convert.ToInt32(((Label)aItem.FindControl("lblOwnerID")).Text.Trim()),
                        CreatedOn = DateTime.Now,
                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                        //Customerid = Convert.ToInt32(AuthenticationHelper.CustomerID),
                       };
                        if (AuthenticationHelper.Role == "CADMN")
                        {
                            mapping.Customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                        else
                        {
                            mapping.Customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        mappinglist.Add(mapping);
                        isExists = Exists(mapping);
                        

                    }
                }

               
                if (isExists)
                {
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Officer Name already exists.";
                    cvDuplicateEntry.IsValid = false;
                }
                else
                {
                    CreateOrUpdateComplianceMapping(mappinglist, 0);
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";
                    cvDuplicateEntry.IsValid = false;
                }
               

                #endregion

            }
            else if((int)ViewState["Mode"]==1)
            {
                newmapping.ID = Convert.ToInt32(ViewState["OfficermappingID"]);
                newmapping.Cer_OfficerUserID = Convert.ToInt32(ViewState["OfficerUSerid"]);
                foreach (RepeaterItem aItem in rptComplianceOwner.Items)
                {
                    CheckBox chkCompowner = (CheckBox)aItem.FindControl("chkCompowner");

                    if (chkCompowner.Checked)
                    {
                        Certificate_OfficerOwnerMapping mapping = new Certificate_OfficerOwnerMapping()
                        {
                            Cer_OfficerUserID = Convert.ToInt32(ddlComplianceOfficer.SelectedValue),
                            Cer_OwnerUserID = Convert.ToInt32(((Label)aItem.FindControl("lblOwnerID")).Text.Trim()),
                            CreatedOn = DateTime.Now,
                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                          
                        };
                        if(AuthenticationHelper.Role=="IMPT")
                        {
                            mapping.Customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        else
                        {
                            mapping.Customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                        mappinglist.Add(mapping);

                    }
                }
                CreateOrUpdateComplianceMapping(mappinglist, newmapping.Cer_OfficerUserID);
                saveopo.Value = "true";
                cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                cvDuplicateEntry.IsValid = false;
                BindGrid();
            }
        }

        public static bool Exists(Certificate_OfficerOwnerMapping mapping)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                var query = (from row in entities.Certificate_OfficerOwnerMapping
                             where row.Cer_OfficerUserID == mapping.Cer_OfficerUserID
                             select row);


                if (mapping.ID > 0)
                {
                    query = query.Where(entry => entry.Cer_OfficerUserID != mapping.Cer_OfficerUserID);
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static void DeleteComplianceOwnerMapping(int officermappingid)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                if (officermappingid > 0)
                {
                    List<Certificate_OfficerOwnerMapping> mappedofficer = (from row in entities.Certificate_OfficerOwnerMapping
                                                                           where row.Cer_OfficerUserID == officermappingid
                                                                           select row).ToList();

                    foreach (Certificate_OfficerOwnerMapping mappingofficerid in mappedofficer)
                    {
                        entities.Certificate_OfficerOwnerMapping.Remove(mappingofficerid);
                        entities.SaveChanges();
                    }


                }

                
            }
        }

        public static void CreateOrUpdateComplianceMapping(List<Certificate_OfficerOwnerMapping> mappingcomList,int officermappingid)
        {
            using (CertificateDataEntities entities = new CertificateDataEntities())
            {
                if (officermappingid > 0)
                {
                    List<Certificate_OfficerOwnerMapping> mappedofficer = (from row in entities.Certificate_OfficerOwnerMapping
                                                                           where row.Cer_OfficerUserID == officermappingid
                                                                           select row).ToList();
                    if(mappedofficer.Count>0)
                    {
                        foreach (Certificate_OfficerOwnerMapping mappingofficerid in mappedofficer)
                        {
                            entities.Certificate_OfficerOwnerMapping.Remove(mappingofficerid);
                            entities.SaveChanges();
                        }
                    }

                   


                }

                foreach (Certificate_OfficerOwnerMapping mappingowner in mappingcomList)
                {
                    entities.Certificate_OfficerOwnerMapping.Add(mappingowner);
                    entities.SaveChanges();
                }
            }
        }

        protected void grdComplianceOfficerMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceOfficerMapping.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCustomerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
            BindOfficer(customerid);
            BindComplianceOwner(customerid);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divOfficerdetailsDialog\").dialog('close')", true);
        }

        protected void ddlComplianceOfficer_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (RepeaterItem aItem in rptComplianceOwner.Items)
            {
                CheckBox chkCompowner = (CheckBox)aItem.FindControl("chkCompowner");
                chkCompowner.Checked = false;
                CheckBox OwnerSelectAll = (CheckBox)rptComplianceOwner.Controls[0].Controls[0].FindControl("OwnerSelectAll");
                OwnerSelectAll.Checked = false;
            }
        }
    }
}