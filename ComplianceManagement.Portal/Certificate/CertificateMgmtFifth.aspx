﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateMgmtFifth.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Certificate.CertificateMgmtFifth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
      <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

    <style type="text/css">
        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        .modal, .modal-backdrop {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .modal-content, .popover {
            background-clip: padding-box;
        }

        .modal-open .navbar-fixed-bottom, .modal-open .navbar-fixed-top, body.modal-open {
            margin-right: 15px;
        }

        .modal {
            display: none;
            overflow: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 1040;
        }

            .modal.fade .modal-dialog {
                -webkit-transform: translate(0,-25%);
                -ms-transform: translate(0,-25%);
                transform: translate(0,-25%);
                -webkit-transition: -webkit-transform .3s ease-out;
                -moz-transition: -moz-transform .3s ease-out;
                -o-transition: -o-transform .3s ease-out;
                transition: transform .3s ease-out;
            }

            .modal.in .modal-dialog {
                -webkit-transform: translate(0,0);
                -ms-transform: translate(0,0);
                transform: translate(0,0);
            }

        .modal-dialog {
            margin-left: auto;
            margin-right: auto;
            width: auto;
            padding: 10px;
            z-index: 1050;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
            box-shadow: 0 3px 9px rgba(0,0,0,.5);
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            z-index: 1030;
            background-color: #000;
        }

            .modal-backdrop.fade {
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .carousel-control, .modal-backdrop.in {
                opacity: .5;
                filter: alpha(opacity=50);
            }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5;
            min-height: 16.43px;
        }

            .modal-header .close {
                margin-top: -2px;
            }

        .modal-title {
            margin: 0;
            line-height: 1.428571429;
        }

        .modal-body {
            position: relative;
            padding: 20px;
        }


        .modal-footer {
            margin-top: 15px;
            padding: 19px 20px 20px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }



            .modal-footer:after, .modal-footer:before {
                content: " ";
                display: table;
            }

            .modal-footer .btn + .btn {
                margin-left: 5px;
                margin-bottom: 0;
            }

            .modal-footer .btn-group .btn + .btn {
                margin-left: -1px;
            }

            .modal-footer .btn-block + .btn-block {
                margin-left: 0;
            }

        @media screen and (min-width:768px) {
            .modal-dialog {
                left: 50%;
                right: auto;
                width: 600px;
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .modal-content {
                -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }
        }

        .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container:after, .dropdown-menu > li > a, .form-horizontal .form-group:after, .modal-footer:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
            clear: both;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: hidden;
        }

        .close, .list-group-item > .badge {
            float: right;
        }

        .close {
            font-size: 21px;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .2;
            filter: alpha(opacity=20);
        }

            .close:focus, .close:hover {
                color: #000;
                text-decoration: none;
                cursor: pointer;
                opacity: .5;
                filter: alpha(opacity=50);
            }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 400px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
            text-align: center !important;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0px 0px 1px 0px;*/
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-grid-header-wrap.k-auto-scrollable {
            width: 100% !important;
        }

        .k-multiselect-wrap .k-input {
            display: inherit !important;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            BindFilters();
            Bindgrid();
            SetLblPerion();
        });

        function BindFilters() {

            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Certificate Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    fCreateStoryBoard('dropdownlistStatus', 'filtersstoryStatus', 'Status')
                    FilterAllMain();
                },
                dataSource: [
                  { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "Pending For Review" },                    
                    { text: "Rejected", value: "Rejected" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },                 
                    { text: "Not Applicable", value: "Not Applicable" },
                    { text: "In Progress", value: "In Progress" }

                ]
            });

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    FilterAllMain();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                           <%-- url: '<% =Path%>Data/GetCertificateLocationList?customerId=<% =CustId%>&userId=<% =UId%>&SID=<% =SchId%>&RId=<% =ReviewerID%>',--%>
                            url: '<% =Path%>Data/GetCertificateLocationListMGMT?Mgmtid=<% =UId%>&OffiUid=<% =OfficerUserID%>&OwnerUid=<% =OwnerID%>&RevUid=<% =ReviewerID%>&cusid=<% =CustId%>&ceID=<% =SchId%>',
                            dataType: "json",
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterAllMain();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindMGMTCertificateActList?Mgmtid=<% =UId%>&OffiUid=<% =OfficerUserID%>&OwnerUid=<% =OwnerID%>&RevUid=<% =ReviewerID%>&cusid=<% =CustId%>&ceID=<% =SchId%>',
                            dataType: "json",
                        }
                    }
                },
                dataBound: function (e) {
                    e.sender.list.width("790");
                }
            });

            <%if (Flag == "Submitted" || Flag == "Auto Submitted"){%>
            $("#dropdownCompliance").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Status",
                change: function (e) {
                    Bindgrid();
                },
                dataSource: [
                    { text: "Certificate Submitted Compliance", value: "Submitted" },
                    { text: "Certificate Not Submitted Compliance", value: "Not Submitted" },
                ]
            });
            $("#dropdownCompliance").data("kendoDropDownList").value("<%=Flag%>");
            <%}%>
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
            }

            if (div == 'filtersstoryStatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;:');
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;border-radius:10px;margin-left:5px;margin-bottom: 4px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function ClearAllFilterMain(e) {
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function BackMain() {
            window.location.href = "../Certificate/CertificateMgmtFourth.aspx?SID=<% =SchId%>&Period=<% =Period%>&OfficerUserID=<% =OfficerUserID%>";
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistStatus', 'filtersstoryStatus', 'Status');
        }

        function FilterAllMain() {

            var locdetail = $("#dropdowntree").data("kendoDropDownTree").value();
            var Status = $("#dropdownlistStatus").data("kendoDropDownTree").value();

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (locdetail.length > 0
                || Status.length > 0
                || ($("#dropdownACT").val() != undefined && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "")) {
                if (Status.length > 0) {
                    var StatusFilter = { logic: "or", filters: [] };

                    $.each(Status, function (i, v) {
                        StatusFilter.filters.push({
                            field: "beforeSubmitStatus", operator: "eq", value: v
                        });
                    });
                    finalSelectedfilter.filters.push(StatusFilter);
                }
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };
                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });
                    finalSelectedfilter.filters.push(ActFilter);
                }

                if (locdetail.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locdetail, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }

            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

       

        function Bindgrid() {

            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '<%=Path%>Data/ComplianceCertificateMgmtFifth?Mgmtid=<% =UId%>&OffiUid=<% =OfficerUserID%>&OwnerUid=<% =OwnerID%>&RevUid=<% =ReviewerID%>&cusid=<% =CustId%>&ceID=<% =SchId%>&IsSubmitted=' + $("#dropdownCompliance").val(),
                     },
                 },
                 pageSize: 10,
                 schema: {
                     model: {
                         fields: {
                             ScheduledOn: { type: "date" },
                         }
                     }
                 },
            });

            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                },
                dataBound: function () {
                    window.parent.forchild($("body").height() + 40);
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        field: "Branch", title: 'Location',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "ActName", title: 'Act',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "ShortDescription", title: 'Short Description',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "PerformerName", title: 'Performer',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "ReviewerName", title: 'Reviewer',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "ForMonth", title: 'Period',
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "beforeSubmitStatus", title: 'Certificate Status',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "Status", title: 'Current Status',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" },
                    
                        ], title: "Action", lock: true, width: "20%;", headerAttributes: {
                            style: "border-right: solid 1px ;text-align: center;"
                        }
                    }
                ]
            });

            $(document).on("click", "#grid tbody tr .ob-overviewMain", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
                return true;
            });

            function OpenOverViewpupMain(scheduledonid, instanceid) {
                
                $('#divOverView1').modal('show');
                $('#OverViews1').attr('width', '1250px');
                $('#OverViews1').attr('height', '600px');
                $('.modal-dialog').css('width', '1306px');

                $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "" && content != "UpdateCancel") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else {
                        e.preventDefault();
                    }
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "View";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Preview";
                }
            });

        }
        function SetLblPerion() {
           <%-- document.getElementById('lblReviewer').innerHTML = "Reviewer - " + '<% =Reviewer%>';--%>
        }
        function exportReport(e) {


            var ReportName = "Compliance Certificate Report";
            var todayDate = moment().format('DD-MMM-YYYY');
            var grid = $("#grid").getKendoGrid();

            var rows = [
                {
                    cells: [
                        { value: "Report Name:", bold: true },
                        { value: ReportName }
                    ]
                },
                {
                    cells: [
                        { value: "Report Generated On:", bold: true },
                        { value: todayDate }
                    ]
                },
                {
                    cells: [
                        { value: "" }
                    ]
                },
                {

                   cells: [
                        { value: "Location", bold: true },
                        { value: "Act Name", bold: true },
                        { value: "Short Description", bold: true },
                        { value: "Performer", bold: true },
                        { value: "Reviewer", bold: true },
                        { value: "Period", bold: true },
                        { value: "Due Date", bold: true },
                        { value: "Certicate Status", bold: true },
                        { value: "Current Status", bold: true },
                    ]
                }
            ];

            var trs = grid.dataSource;
            var filteredDataSource = new kendo.data.DataSource({
                data: trs.data(),
                filter: trs.filter()
            });

            filteredDataSource.read();
            var data = filteredDataSource.view();
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                var rowHeightAct = 0;
                var rowHeightDescr = 0;
                if (dataItem.ActName != null && dataItem.ActName != "") {
                    rowHeightAct = dataItem.ActName.length > 27 ? Math.ceil(dataItem.ActName.length / 27) * 20 : 20;
                }
                if (dataItem.ShortDescription != null && dataItem.ShortDescription != "") {
                    rowHeightDescr = dataItem.ShortDescription.length > 27 ? Math.ceil(dataItem.ShortDescription.length / 27) * 20 : 20;
                }
                var rowHeight = 20;
                if (rowHeightAct > rowHeightDescr) {
                    rowHeight = rowHeightAct;
                }
                else {
                    rowHeight = rowHeightDescr;
                }
                rows.push({
                    cells: [
                        { value: dataItem.Branch },
                        { value: dataItem.ActName },
                        { value: dataItem.ShortDescription },
                        { value: dataItem.PerformerName },
                        { value: dataItem.ReviewerName },
                        { value: dataItem.ForMonth },
                        { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                        { value: dataItem.beforeSubmitStatus },
                        { value: dataItem.Status },
                    ],
                    height: rowHeight
                });
            }
            for (var i = 3; i < rows.length; i++) {
                for (var j = 0; j < 9; j++) {
                    rows[i].cells[j].borderBottom = "#000000";
                    rows[i].cells[j].borderLeft = "#000000";
                    rows[i].cells[j].borderRight = "#000000";
                    rows[i].cells[j].borderTop = "#000000";
                    rows[i].cells[j].hAlign = "left";
                    rows[i].cells[j].vAlign = "top";
                    rows[i].cells[j].wrap = true;
                }
            }
            excelExport(rows, ReportName);
            e.preventDefault();
        }

        function excelExport(rows, ReportName) {


            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        columns: [
                            { width: 180 },
                            { width: 350 },
                            { width: 350 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 250 },
                            { width: 200 },
                        ],
                        title: "Report",
                        rows: rows
                    },
                ]
            });

            var nameOfPage = "ComplianceCertificateReport";
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row" style="padding-bottom: 5px;">
            <div class="col-md-12">
                <label id="lblReviewer" style="font-size: 24px; margin-left: 3px; color: black; font-weight: 300; float: left; width: 95%;">
                    Period - <% =Period%>
                </label>
                <button type="button" id="btnBack" style="width:4%" class="k-button" onclick="BackMain(event)"><span onclick="javascript:return false;"></span>Back</button>
            </div>
        </div>

        <div style="margin: 0.4% 0.3% 0.5%; width: 99%;">
            <input id="dropdowntree" style="width: 20%; margin-right: 4px;" />
            <input id="dropdownlistStatus" style="width: 20%; margin-right: 4px;" />
            <input id="dropdownACT" style="width: 20%; margin-right: 4px;" />
             <%if (Flag == "Submitted" || Flag == "Auto Submitted"){%>
            <input id="dropdownCompliance" style="width: 25%; margin-right: 5px;" />
            <%} %>
            <button type="button" id="ClearfilterMain" class="k-button" style="float: right; margin-right: -0.3%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            <button id="export" onclick="exportReport(event)" class="k-button" style="float: right; margin-right: 5px;"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
        </div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; margin-left: 4px" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; margin-left: 4px" id="filtersstoryStatus">&nbsp;</div>

        <div class="row">
            <div id="grid" style="margin: 4px;"></div>
        </div>

        <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
