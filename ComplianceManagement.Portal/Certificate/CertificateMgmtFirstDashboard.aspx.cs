﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Certificate
{
    public partial class CertificateMgmtFirstDashboard : System.Web.UI.Page
    {
        protected static string Path;
        protected static string FlagIsApp;
        protected static int CustId;
        protected static int UId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            FlagIsApp = Convert.ToString(AuthenticationHelper.Role);
        }
    }
}