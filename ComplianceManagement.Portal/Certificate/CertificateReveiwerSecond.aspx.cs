﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Certificate
{
    public partial class CertificateReveiwerSecond : System.Web.UI.Page
    {
        protected static string Path;
        protected static string FlagIsApp;
        protected static int CustId;
        protected static int UId;
        protected static int SchId;
        protected static string Period;
        protected static string Flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            FlagIsApp = Convert.ToString(AuthenticationHelper.Role);
            if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                SchId = Convert.ToInt32(Request.QueryString["SID"]);

            if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                Period = Convert.ToString(Request.QueryString["Period"]);

            if (!string.IsNullOrEmpty(Request.QueryString["Flag"]))
                Flag = Convert.ToString(Request.QueryString["Flag"]);
        }
    }
}