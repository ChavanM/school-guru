﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateMgmtFourth.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Certificate.CertificateMgmtFourth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 400px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
            text-align: center !important;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0px 0px 1px 0px;*/
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-grid-header-wrap.k-auto-scrollable {
            width: 100% !important;
        }

        .k-multiselect-wrap .k-input {
            display: inherit !important;
        }
    </style>

    <script type="text/javascript">
        var initialLoad = true;
        $(document).ready(function () {
            //BindFilters();
            Bindgrid();

        });
        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: '<%=Path%>Data/ComplianceCertificateMgmtFourth?Mgmtid=<% =UId%>&OffiUid=<% =OfficerUserID%>&OwnerUid=<% =OwnerID%>&cusid=<% =CustId%>&ceID=<% =SchId%>',
                       },
                   },
                   pageSize: 10,
                   schema: {
                       model: {
                           fields: {
                               DueDate: { type: "date" },
                           }
                       }
                   },
               });

        dataSource.read();

        //function BindFilters() {
        //    $("#dropdownlistReviewer").kendoDropDownTree({
        //        placeholder: "Risk",
        //        checkboxes: true,
        //        checkAll: true,
        //        autoClose: true,
        //        checkAllTemplate: "Select All",
        //        autoWidth: true,
        //        dataTextField: "ReviewerName",
        //        dataValueField: "ReviewerUserID",



        //        //placeholder: "Reviewer",
        //        //checkboxes: true,
        //        //checkAll: true,
        //        //autoClose: true,
        //        //checkAllTemplate: "Select All",
        //        //autoWidth: true,
        //        //dataTextField: "ReviewerName",
        //        //dataValueField: "ReviewerUserID",
        //        change: function () {
        //            fCreateStoryBoard('dropdownlistReviewer', 'filtersstoryboard', 'Period')
        //            FilterAllMain();
        //        },
        //        dataSource: dataSource
        //    });
        //}

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Reviewer&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;border-radius:10px;margin-left:5px;margin-bottom: 4px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function ClearAllFilterMain(e) {
            //$("#dropdownlistReviewer").data("kendoDropDownTree").value([]);
            $("#dropdownlistReviewer").data("kendoDropDownList").select(0);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdownlistReviewer', 'filtersstoryboard', 'Period')
        };

        function FilterAllMain() {

            //var Revdetails = $("#dropdownlistReviewer").data("kendoDropDownTree").value();

            //var finalSelectedfilter = { logic: "and", filters: [] };

            //if (Revdetails.length > 0) {

            //    if (Revdetails.length > 0) {
            //        var LocationFilter = { logic: "or", filters: [] };

            //        $.each(Revdetails, function (i, v) {
            //            LocationFilter.filters.push({
            //                field: "ReviewerUserID", operator: "eq", value: v
            //            });
            //        });

            //        finalSelectedfilter.filters.push(LocationFilter);
            //    }

            //    if (finalSelectedfilter.filters.length > 0) {
            //        var dataSource = $("#grid").data("kendoGrid").dataSource;
            //        dataSource.filter(finalSelectedfilter);
            //    }
            //    else {
            //        $("#grid").data("kendoGrid").dataSource.filter({});
            //    }

            //}
            //else {
            //    $("#grid").data("kendoGrid").dataSource.filter({});
            //}
            var finalSelectedfilter = { logic: "and", filters: [] };
            if ($("#dropdownlistReviewer").val() != undefined && $("#dropdownlistReviewer").val() != null && $("#dropdownlistReviewer").val() != "-1" && $("#dropdownlistReviewer").val() != "") {
                var SeqFilter = { logic: "or", filters: [] };
                SeqFilter.filters.push({
                    field: "ReviewerUserID", operator: "eq", value: $("#dropdownlistReviewer").val()
                });
                finalSelectedfilter.filters.push(SeqFilter);

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

<%--        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: '<%=Path%>Data/ComplianceCertificateMgmtFourth?Mgmtid=<% =UId%>&OffiUid=<% =OfficerUserID%>&OwnerUid=<% =OwnerID%>&cusid=<% =CustId%>&ceID=<% =SchId%>',
                },
            },
            pageSize: 10,
            schema: {
                model: {
                    fields: {
                        DueDate: { type: "date" },
                    }
                }
            },
        });--%>

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                },
                dataBound: function () {
                    window.parent.forchild($("body").height() + 40);
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }

                    if (initialLoad) {

                        var dataa = $('#grid').data("kendoGrid").dataSource.data();

                        $("#dropdownlistReviewer").kendoDropDownList({
                            filter: "startswith",
                            autoClose: false,
                            autoWidth: true,
                            dataTextField: "Name",
                            dataTextField: "ReviewerName",
                            dataValueField: "ReviewerUserID",
                            optionLabel: "Select Reviewer",
                            change: function () {
                                fCreateStoryBoard('dropdownlistReviewer', 'filtersstoryboard', 'Period')
                                FilterAllMain();
                            },
                            dataSource: dataa

                        });
                        initialLoad = false;
                    }     
                },
                columns: [
                    {
                        field: "ReviewerName", title: 'Reviewer Name',
                        attributes: {
                            style: 'white-space: nowrap; text-align: center;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    //{
                    //    field: "SubmittedFlag", title: 'Submission Status',
                    //    attributes: {
                    //        style: 'white-space: nowrap; text-align: center;'

                    //    }, filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }, width: "30%"
                    //},
                    {
                        title: "Status as on Certificate Date",
                        attributes: {
                            style: 'white-space: nowrap; text-align: center;'
                        },
                        columns: [
                            {
                                field: "ComplianceStatusClosedTimelyPer", title: 'Completed %', template: '#=ComplianceStatusClosedTimelyPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;text-align: center;'

                                },
                                filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%"
                            }, 
                            {
                                field: "ComplianceStatusOpenPer", title: 'Overdue %', template: '#=ComplianceStatusOpenPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;text-align: center;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%"
                            }]


                    },
                    {
                        title: "Status as on Current Date",
                        attributes: {
                            style: 'white-space: nowrap; text-align: center;'
                        },
                        columns: [
                            {
                                field: "AsOnStatusClosedTimelyPer", title: 'Completed %', template: '#=AsOnStatusClosedTimelyPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%",
                                attributes: {
                                    "class": "table-cell",
                                    style: "text-align: center; font-size: 14px"
                                }
                            },
                            {
                                field: "AsOnStatusOpenPer", title: 'Overdue %', template: '#=AsOnStatusOpenPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%",
                                attributes: {
                                    "class": "table-cell",
                                    style: "text-align: center; font-size: 14px"
                                }
                            }]
                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" }

                        ], title: "Action", lock: true, width: "20%;", headerAttributes: {
                            style: "border-right: solid 1px ;text-align: center;"
                        }
                    }
                ]

            });
            $(document).on("click", "#grid tbody tr .ob-overviewMain", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                window.location.href = "../Certificate/CertificateMgmtFifth.aspx?SID=<% =SchId%>&Period=<% =Period%>&Reviewer=" + item.ReviewerName + "&OfficerUserID=<% =OfficerUserID%>&OwnerID=<% =OwnerID%>&ReviewerID=" + item.ReviewerUserID + "&Flag=" + item.SubmittedFlag;

                return true;
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "View";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Preview";
                }
            });

        }

        function BackMain() {
            window.location.href = "../Certificate/CertificateMgmtThird.aspx?SID=<% =SchId%>&Period=<% =Period%>&Officer=&OfficerUserID=<% =OfficerUserID%>";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row" style="padding-bottom: 5px;">
            <div class="col-md-12">
                <label id="lblPeriod" style="font-size: 24px; margin-left: 3px; color: black; font-weight: 300; float: left; width: 95.5%;">
                    Period - <% =Period%>
                </label>
                <button type="button" id="btnBack" class="k-button" onclick="BackMain(event)"><span onclick="javascript:return false;"></span>Back</button>
            </div>
        </div>

        <div style="margin: 0.4% 0.3% 0.5%; width: 99%;">
            <input id="dropdownlistReviewer" style="width: 20%; margin-right: 4px;" />
            <button type="button" id="ClearfilterMain" class="k-button" style="float: right; margin-right: -0.3%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
        </div>

        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryStatus">&nbsp;</div>

        <div class="row">
            <div id="grid" style="margin: 4px;"></div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-2" style="width: 15%; float: right; display: none;">
                    <button type="button" id="btnSubmit" class="k-button" style="float: right; margin-right: 1%; height: 30px;" onclick="btnsbmitdata(event)"><span onclick="javascript:return false;"></span>Submit</button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
