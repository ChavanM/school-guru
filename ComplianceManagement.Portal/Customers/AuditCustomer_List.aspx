﻿<%@ Page Title="Audit Customer List" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true"
    CodeBehind="AuditCustomer_List.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Customers.AuditCustomer_List" %>

<%@ Register Src="~/Controls/AuditCustomerDetailsControl.ascx" TagName="AuditCustomerDetailsControl"
    TagPrefix="vit" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
       <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <script type="text/javascript">
        function fopenpopup() {
            $('#divNotificationTime').modal('show');
            return true;
        }
        //This is used for Close Popup after save or update data.
        function CloseWin() {
            $('#divNotificationTime').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller() {
            setInterval(CloseWin, 3000);
        };

        function AddCustomerPopup() {
            $('#divCustomersDialog').modal('show');
            return true;
        }

        //This is used for Close Popup after save or update data.
        function CloseWin1() {
            $('#divCustomersDialog').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller1() {
            setInterval(CloseWin1, 3000);
        };
    </script>    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <asp:UpdatePanel ID="upCustomerList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0" style="width:23.666667%">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 colpadding0 entrycount" id="divFilter" runat="server" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0" >
                        <p style="color: #999; margin-top: 5px;"> Filter : </p>
                    </div>
                     <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="Width:250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </div>
                    <div style="text-align:right">
                        <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="AddCustomerPopup()"  ID="btnAddCustomer" OnClick="btnAddCustomer_Click" />
                    </div>                                       
                    <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
                <div style="margin-bottom: 4px">
            <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" AllowSorting="true" 
                 OnSorting="grdCustomer_Sorting" DataKeyNames="ID" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging"
                 PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" OnPreRender="grdCustomer_OnPreRender" >
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                         <ItemTemplate>
                                          <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCustomer" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField> 
                      <asp:TemplateField HeaderText="Name" >
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:80px;">
                            <asp:Label  runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                           </div>
                          </ItemTemplate>
                    </asp:TemplateField> 
                       <asp:TemplateField HeaderText="Buyer Name" >
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:100px;">
                            <asp:Label  runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("BuyerName") %>' ToolTip='<%# Eval("BuyerName") %>'></asp:Label>
                           </div>
                          </ItemTemplate>
                    </asp:TemplateField> 
                    
                       <asp:TemplateField HeaderText="Contact" >
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:50px;">
                            <asp:Label  runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("BuyerContactNumber") %>' ToolTip='<%# Eval("BuyerContactNumber") %>'></asp:Label>
                           </div>
                          </ItemTemplate>
                    </asp:TemplateField>
 
                       <asp:TemplateField HeaderText="Email" >
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:100px;">
                            <asp:Label  runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("BuyerEmail") %>' ToolTip='<%# Eval("BuyerEmail") %>'></asp:Label>
                           </div>
                          </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Status" HeaderText="Status" />
                    <asp:TemplateField HeaderText="Start Date">
                        <ItemTemplate>
                            <%# Eval("StartDate") != null ? ((DateTime)Eval("StartDate")).ToString("dd-MMM-yyyy")  : " "%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <ItemTemplate>
                            <%# Eval("EndDate") != null ? ((DateTime)Eval("EndDate")).ToString("dd-MMM-yyyy") : " "%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DiskSpace" HeaderText="Disk Space"  />
                    <asp:TemplateField HeaderText="Sub Unit">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="VIEW_COMPANIES" CommandArgument='<%# Eval("ID") %>'>sub-entities</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Justify" HeaderText="Action">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CUSTOMER" OnClientClick="AddCustomerPopup()" CommandArgument='<%# Eval("ID") %>' data-toggle="tooltip" data-placement="top" ToolTip="Edit Customer"><img src="../Images/edit_icon_new.png"/></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER" CommandArgument='<%# Eval("ID") %>' data-toggle="tooltip" data-placement="top" ToolTip="Disable Customer"
                                OnClientClick="return confirm('This will also delete all the sub-entities associated with customer. Are you certain you want to delete this customer entry?');"><img src="../Images/delete_icon_new.png"/></asp:LinkButton>
                        </ItemTemplate>                        
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />    
                   <PagerSettings Visible="false" />             
                    <PagerTemplate>
                                     <%--   <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                      </PagerTemplate>
                    </asp:GridView>
                     <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                    </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float: right;">
                                        <p >
                                            Page
                                            <%--<asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px; margin-right: 20px; text-align: right; float: right">
                <asp:Button Text="Notify Software Update" runat="server" ID="btnSendNotification" Width="100%" OnClientClick="fopenpopup()" ToolTip="Click here to send server down notifications." OnClick="btnSendNotification_Click"
                    CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divNotificationTime" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 530px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="dvAuditorDialog">
                        <asp:UpdatePanel ID="upServerDownTimeDetails" runat="server" UpdateMode="Conditional" OnLoad="upServerDownTimeDetails_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 4px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" 
                                            ValidationGroup="ComplianceCategoryValidationGroup1" />
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                            ValidationGroup="ComplianceCategoryValidationGroup1" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Date</label>
                                        <asp:TextBox runat="server" ID="txtDate"  CssClass="form-control" Style="width: 200px;" ReadOnly="true" MaxLength="200" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Date can not be empty." ControlToValidate="txtDate"
                                            runat="server" ValidationGroup="ComplianceCategoryValidationGroup1" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Time
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlStartTime"  CssClass="form-control m-bot15"></asp:DropDownList>
                                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select start time." ControlToValidate="ddlStartTime"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                                            Display="None" />
                                        To
                                        <asp:DropDownList runat="server" ID="ddlEndTime"  CssClass="form-control m-bot15"></asp:DropDownList>
                                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select end time." ControlToValidate="ddlEndTime"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: right; margin-right: 66px; margin-top: 10px;">
                                        <asp:Button Text="Send" runat="server" ID="btnSend" CssClass="btn btn-primary" OnClick="btnSend_click"
                                            ValidationGroup="ComplianceCategoryValidationGroup1" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divCustomersDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" >
                <div class="modal-content" style="width: 650px; height:625px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <vit:AuditCustomerDetailsControl runat="server" ID="udcInputForm" />
                    </div>
                </div>
            </div>
        </div>

    <script type="text/javascript">       
        function initializeDowntimeDatePicker(date) {
            var startDate = new Date();
            $('#<%= txtDate.ClientID %>').datepicker({
                                dateFormat: 'dd-mm-yy',
                                numberOfMonths: 1,
                                minDate: startDate
                            });

                            if (date != null) {
                                $("#<%= txtDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;

            var chkheaderid = headerchk.id.split("_");

            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function Selectchildcheckboxes(header) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length - 1; i++) {
                if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                    count++;
                }
            }

            if (count == gvcheck.rows.length - 2) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }

        $(document).ready(function () {
            setactivemenu('Unit Master');
            fhead('Unit Master');
        });

    </script>
    <%--<vit:CustomerDetailsControl runat="server" ID="udcInputForm" />--%>
</asp:Content>
