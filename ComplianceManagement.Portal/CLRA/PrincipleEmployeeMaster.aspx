﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="PrincipleEmployeeMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CLRA.PrincipleEmployeeMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <style>
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }


                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }
                div.k-confirm
                {
                    width:411px;
                    left:500.5px;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   
    
  <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li id="liPECreation" class="listItem"> <%--class="active"--%>
                       <a href="#" onclick="PECreation();">Principle Employer Creation</a>
                    </li>
                    <li id="liPELocationCreation" class="listItem">
                         <a href="#" onclick="PELocationCreation();">Principle Employer Location Creation</a>
                    </li>
                    <li id="liPEMaster" class="listItem">
                             <a href="#" onclick="PEMaster();">Principle Employee Master</a>                 
                    </li>
                    <li id="liContractorDetails" class="listItem">
                             <a href="#" onclick="ContractorDetails();">Contractor Details</a>                 
                    </li>
                    <li id="liPEReport" class="listItem">
                             <a href="#" onclick="PEReport();">Principle Employee Report</a>    
                             <%--<input type="text" id="txtRohan" value="checkR" />    --%>         
                    </li>
                   
                    <%--<li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                        <input type="text" id="txtRohan" value="check" />
                    </li>--%>
                </ul>
            </header>
        </div>


     <iframe id="iFrame" runat="server" src="PrincipleEmployerCreation.aspx"  style="width: 100%;height:680px;" frameBorder="0" scrolling="no"></iframe>
     <%--height: 475px;--%>
    <%-- <iframe id="iFrame" runat="server" src="test2.aspx"  style="width: 100%;height:530px;" frameBorder="0" scrolling="no"></iframe>--%>

    <script type="text/javascript">

        $('#iFrame').on("load", function () {
            var iframe = $(window.top.document).find("#iFrame");
            iframe.height(iframe[0].ownerDocument.body.scrollHeight + 'px');
        });

        function PECreation()
        {
            
            /*var frame = document.getElementById("ContentPlaceHolder1_iFrame");
            //var msg = frame.contentDocument.getElementById("txtTest").value;
            //alert(msg);
            //var x = frame.contentDocument.getElementById("txtTest");
            //x.style.display = "none";
            var items = frame.contentDocument.getElementsByClassName('tabdata');

            for (var i = 0; i < items.length; i++) {
                items[i].style.display = 'none';
            }
           
            var div = frame.contentDocument.getElementById('divPECreation');
            div.style.display = 'block';

            $(".listItem").removeClass("active");
            $("#liPECreation").addClass("active");*/
            displayTab("PECreation");
        }

        function PELocationCreation()
        {
            displayTab("PECreation");

            var frame = document.getElementById("ContentPlaceHolder1_iFrame");
            var divCheckPE = frame.contentDocument.getElementById("divCheckPE");
            var ddlClient = frame.contentDocument.getElementById("ddlClient").value;
            var ddlPrincipleEmployerState = frame.contentDocument.getElementById("ddlPrincipleEmployerState").value;
            var ddlPrincipleEmployerLocation = frame.contentDocument.getElementById("ddlPrincipleEmployerLocation").value;
            

            if (ddlClient == "-1")
            {
                displayErrors("divCheckPE", "Please select client");
            }
            else
            {
                var frame = document.getElementById("ContentPlaceHolder1_iFrame");
                var div = frame.contentDocument.getElementById("divCheckPE");
                div.style.display = 'none';
                displayTab("PEMaster");

                if (ddlPrincipleEmployerState == null || ddlPrincipleEmployerState == "" || ddlPrincipleEmployerState == "-1")
                {
                    $("#ddlPrincipleEmployerState").val("-1");
                }

                if (ddlPrincipleEmployerLocation == null || ddlPrincipleEmployerLocation == "" || ddlPrincipleEmployerLocation == "-1")
                {
                    $("#ddlPrincipleEmployerLocation").val("-1");
                }
                displayTab("PELocationCreation");
            }


            /*if ($("#ddlClient").val() == "-1")
            {
                $(".tabdata").hide();
                $("#divPECreation").show();
                $("#divCheckPE").html("");
                displayErrors("divCheckPE", "Please Select Client");
            }
            else
            {
                $("#divCheckPE").hide();
                if ($("#ddlPrincipleEmployerState").val() == null || $("#ddlPrincipleEmployerState").val() == "" || $("#ddlPrincipleEmployerState").val() == "-1") {
                    $("#ddlPrincipleEmployerState").val("-1");
                }

                if ($("#ddlPrincipleEmployerLocation").val() == null || $("#ddlPrincipleEmployerLocation").val() == "" || $("#ddlPrincipleEmployerLocation").val() == "-1") {
                    $("#ddlPrincipleEmployerLocation").val("-1");
                }
                $(".tabdata").hide();
                $("#divPELocationCreation").show();
                $("#ddlPrincipleEmployer").select2();
                var items = parent.document.getElementsByClassName('listItem');

                for (var i = 0; i < items.length; i++) {
                    items[i].classList.remove("active");
                }

                var item = parent.document.getElementById("liPELocationCreation");
                item.classList.add("active");

            }*/
           
        }


        function displayErrors(control, msg)
        {
            var frame = document.getElementById("ContentPlaceHolder1_iFrame");
            var x = frame.contentDocument.getElementById(control);
            x.style.display = "block";
            x.innerHTML = "";
            x.append(msg);
            var linebreak = document.createElement("br");
            x.appendChild(linebreak);
        }

        function PEMaster()
        {
          
            displayTab("PELocationCreation");

            var frame = document.getElementById("ContentPlaceHolder1_iFrame");
            var divCheckPELocation = frame.contentDocument.getElementById("divCheckPELocation");
            var ddlPrincipleEmployer = frame.contentDocument.getElementById("ddlPrincipleEmployer").value;
            var ddlPrincipleEmployerState = frame.contentDocument.getElementById("ddlPrincipleEmployerState").value;
            var ddlPrincipleEmployerLocation = frame.contentDocument.getElementById("ddlPrincipleEmployerLocation").value;
            var ddlPrincipleEmployerMaster = frame.contentDocument.getElementById("ddlPrincipleEmployerMaster").value;

            if (ddlPrincipleEmployer == "-1") {
                displayErrors("divCheckPELocation", "Please select principle employer");
            }
            else if (ddlPrincipleEmployerState == "-1") {
                displayErrors("divCheckPELocation", "Please select state");
            }
            else if (ddlPrincipleEmployerLocation == "-1") {
                displayErrors("divCheckPELocation", "Please select location");
            }
            else
            {
                var frame = document.getElementById("ContentPlaceHolder1_iFrame");
                var div = frame.contentDocument.getElementById("divCheckPELocation");
                div.style.display = 'none';
                displayTab("PEMaster");

                if (ddlPrincipleEmployerMaster == null || ddlPrincipleEmployerMaster == "" || ddlPrincipleEmployerMaster == "-1")
                {
                    frame.contentWindow.BindPrincipleEmployerListOnEmployeeMaster();
                }
            }
           
        }

        function ContractorDetails()
        {
            displayTab("PEMaster");

            var frame = document.getElementById("ContentPlaceHolder1_iFrame");
            var divCheckEmployees = frame.contentDocument.getElementById("divCheckEmployees");

            var ddlPrincipleEmployerMaster = frame.contentDocument.getElementById("ddlPrincipleEmployerMaster").value;
            var ddlPrincipleEmployerStateMaster = frame.contentDocument.getElementById("ddlPrincipleEmployerStateMaster").value;
            var ddlPrincipleEmployerLocationMaster = frame.contentDocument.getElementById("ddlPrincipleEmployerLocationMaster").value;
            var ddlPrincipleEmployerBranchMaster = frame.contentDocument.getElementById("ddlPrincipleEmployerBranchMaster").value;
            var ddlPrincipleemployerContractor = frame.contentDocument.getElementById("ddlPrincipleemployerContractor").value;

            if (ddlPrincipleEmployerMaster == "")
            {
                PEMaster();
            }
            else if (ddlPrincipleEmployerMaster == "-1")
            {
                displayErrors("divCheckEmployees", "Please select principle employer");
            }
            else if (ddlPrincipleEmployerStateMaster == "-1") {
                displayErrors("divCheckEmployees", "Please select state");
            }
            else if (ddlPrincipleEmployerLocationMaster == "-1") {
                displayErrors("divCheckEmployees", "Please select location");
            }
            else if (ddlPrincipleEmployerBranchMaster == "-1") {
                displayErrors("divCheckEmployees", "Please select branch");
            }
            else
            {
                var frame = document.getElementById("ContentPlaceHolder1_iFrame");
                var div = frame.contentDocument.getElementById("divCheckEmployees");
                div.style.display = 'none';
                div.innerHTML = "";
                displayTab("ContractorDetails");

                if (ddlPrincipleemployerContractor == null || ddlPrincipleemployerContractor == "" || ddlPrincipleemployerContractor == "-1") {
                    frame.contentWindow.BindPrincipleEmployerListOnEmployeeMaster('Y');
                }
            }

          
        }

        function PEReport()
        {

            displayTab("ContractorDetails");

            var frame = document.getElementById("ContentPlaceHolder1_iFrame");
            var divCheckContractor = frame.contentDocument.getElementById("divCheckContractor");

            var ddlPrincipleemployerContractor = frame.contentDocument.getElementById("ddlPrincipleemployerContractor").value;
            var ddlStateContractor = frame.contentDocument.getElementById("ddlStateContractor").value;
            var ddlLocationContractor = frame.contentDocument.getElementById("ddlLocationContractor").value;
            var ddlBranchContractor = frame.contentDocument.getElementById("ddlBranchContractor").value;
            var ddlClientReport = frame.contentDocument.getElementById("ddlClientReport").value;
            
            if (ddlPrincipleemployerContractor == "-1") {
                displayErrors("divCheckContractor", "Please select principle employer");
            }
            else if (ddlStateContractor == "-1") {
                displayErrors("divCheckContractor", "Please select state");
            }
            else if (ddlLocationContractor == "-1") {
                displayErrors("divCheckContractor", "Please select location");
            }
            else if (ddlBranchContractor == "-1") {
                displayErrors("divCheckContractor", "Please select branch");
            }
            else
            {
                var frame = document.getElementById("ContentPlaceHolder1_iFrame");
                var div = frame.contentDocument.getElementById("divCheckContractor");
                div.style.display = 'none';
                div.innerHTML = "";
                displayTab("PEReport");

                if (ddlClientReport == null || ddlClientReport == "" || ddlClientReport == "-1")
                {
                    frame.contentWindow.BindClientList("R");
                }
               
            }
            /*$(".tabdata").hide();
            if ($("#ddlPrincipleemployerContractor").val() == "-1") 
            {
                $("#divCheckContractor").html("");
                displayErrors("divCheckContractor", "Please select principle employer");

                $("#divContractorDetails").show();

            }
            else if ($("#ddlStateContractor").val() == "-1") 
            {
                $("#divCheckContractor").html("");
                displayErrors("divCheckContractor", "Please select state");

                $("#divContractorDetails").show();
            }
            else if ($("#ddlLocationContractor").val() == "-1") 
            {
                $("#divCheckContractor").html("");
                displayErrors("divCheckContractor", "Please select location");

                $("#divContractorDetails").show();
            }
            else if ($("#ddlBranchContractor").val() == "-1") 
            {
                $("#divCheckContractor").html("");
                displayErrors("divCheckContractor", "Please select branch");

                $("#divContractorDetails").show();
            }
            else 
            {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();

                if ($("#ddlClientReport").val() == null || $("#ddlClientReport").val() == "" || $("#ddlClientReport").val() == "-1") {
                    BindClientList("R");
                }

                $("#divPEReport").show();

                var items = parent.document.getElementsByClassName('listItem');
                for (var i = 0; i < items.length; i++) {
                    items[i].classList.remove("active");
                }

                var item = parent.document.getElementById("liPEReport");
                item.classList.add("active");
            }*/

            

            
        }


        function displayTab(controlId)
        {
            var frame = document.getElementById("ContentPlaceHolder1_iFrame");
            var items = frame.contentDocument.getElementsByClassName('tabdata');

            for (var i = 0; i < items.length; i++) {
                items[i].style.display = 'none';
            }

            var div = frame.contentDocument.getElementById('div'+controlId);
            div.style.display = 'block';
            $(".listItem").removeClass("active");
            $("#li"+controlId).addClass("active");
        }

    </script>

</asp:Content>
