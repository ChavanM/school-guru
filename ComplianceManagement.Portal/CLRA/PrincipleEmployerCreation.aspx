﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrincipleEmployerCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CLRA.PrincipleEmployerCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="../NewCSS/3.3.7/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="../Newjs/3.3.1/jquery.min.js"></script>
    <script src="../Newjs/3.3.7/bootstrap.min.js"></script>
    <script src="../Newjs/bootstrap-datepicker.min.js"></script>
    <script src="../Newjs/jquery.validate.min.js"></script>
    <script src="../Newjs/jquery.validate.unobtrusive.js"></script>

    <link href="style.css" rel="stylesheet" />
    <link href="../assets/select2.min.css" rel="stylesheet" />
    <script src="../assets/select2.min.js"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <link href="../NewCSS/font-awesome.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <script src="../Newjs/fastselect.standalone.js"></script>
    <link href="../NewCSS/fastselect.min.css" rel="stylesheet" />
    <style type="text/css">
        .k-grid-content {
            min-height: auto !important;
        }

        .k-multiselect-wrap .k-input {
            padding-top: 6px;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: -7px;
            padding-left: 0px;
            padding-right: 15px;
        }
        .col-md-12 {
            text-align: center;
        }
        .k-pager-sizes {
            float: left;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
            }

        .k-grid, .k-listview {
            margin-top: 10px;
        }

            .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            }

        .k-tooltip-content {
            width: max-content;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-calendar-container {
            background-color: white;
            width: 217px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top: 0px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }


        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left: 2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height: 34px;
        }

        .k-multiselect-wrap, .k-floatwrap {
            height: 34px;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 1%;
            margin-bottom: 1%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
            color: #515967;
            padding-top: 5px;
        }

        .k-filter-row th, .k-grid-header th[data-index='10'] {
            text-align: center;
        }


        .fstElement {
            font-size: 0.7em;
            height: 34px;
        }

        .fstToggleBtn {
            min-width: 16.5em;
        }

        .submitBtn {
            display: none;
        }

        .fstMultipleMode {
            display: block;
        }

            .fstMultipleMode .fstControls {
                width: 100%;
            }
    </style>
 


          <style type="text/css">
              .k-checkbox-label:hover {
                  color: #1fd9e1;
              }

              .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
                  cursor: pointer;
                  background-color: transparent !important;
                  border-color: transparent;
                  color: #1fd9e1;
              }

              .btn {
                  font-weight: 400;
              }

              .k-dropdown-wrap-hover, .k-state-default-hover, .k-state-hover, .k-state-hover:hover {
                  color: #2e2e2e;
                  background-color: #d8d6d6 !important;
              }

              .k-grid-content {
                  min-height: auto !important;
              }

              .k-multiselect-wrap .k-input {
                  padding-top: 6px;
              }

              .k-grid tbody .k-button {
                  min-width: 19px;
                  min-height: 25px;
                  background-color: transparent;
                  border: none;
                  margin-left: 0px;
                  margin-right: 0px;
                  padding-left: 2px;
                  padding-right: 2px;
                  border-radius: 25px;
              }

              .k-button.k-state-active, .k-button:active {
                  color: black;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li:hover {
                  color: white;
                  background-color: #1fd9e1;
                  border-top-left-radius: 10px;
                  border-top-right-radius: 10px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li {
                  margin-left: 5px !important;
                  margin-right: 5px !important;
              }

              .panel-heading .nav {
                  background-color: #f8f8f8;
                  border: none;
                  font-size: 11px;
                  margin: 0px 0px 0px 0;
                  border-radius: 10px;
              }

                  .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                      color: white;
                      background-color: #1fd9e1;
                      border-top-left-radius: 10px;
                      border-top-right-radius: 10px;
                      margin-left: 0.5em;
                      margin-right: 0.5em;
                  }

              .k-grid, .k-listview {
                  margin-top: 0px;
              }

                  .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                      -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                      box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                  }

              .k-tooltip-content {
                  width: max-content;
              }

              input[type=checkbox], input[type=radio] {
                  margin: 4px -2px 0;
                  margin-top: 1px\9;
                  line-height: normal;
              }

              .k-calendar-container {
                  background-color: white;
                  width: 217px;
              }

              .div.k-grid-footer, div.k-grid-header {
                  border-top-width: 1px;
                  margin-right: 0px;
                  margin-top: 0px;
              }


              .k-grid-footer-wrap, .k-grid-header-wrap {
                  position: relative;
                  width: 100%;
                  overflow: hidden;
                  border-style: solid;
                  border-width: 0 1px 0 0;
                  zoom: 1;
              }


              html {
                  color: #666666;
                  font-size: 15px;
                  font-weight: normal;
                  font-family: 'Roboto',sans-serif;
              }

              .k-checkbox-label, .k-radio-label {
                  display: inline;
              }

              .myKendoCustomClass {
                  z-index: 999 !important;
              }

              .k-header .k-grid-toolbar {
                  background: white;
                  float: left;
                  width: 100%;
              }

              .k-grid td {
                  line-height: 2.0em;
                  border-bottom-width: 1px;
                  background-color: white;
                  border-width: 0 1px 1px 0px;
              }

              .k-i-more-vertical:before {
                  content: "\e006";
              }

              .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                  background-color: #1fd9e1;
                  background-image: none;
                  background-color: white;
              }

              k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
                  color: #000000;
                  border-color: #1fd9e1;
                  background-color: white;
              }

              /*#tblPrincipleEmployer .k-grid-toolbar {
            background: white;
        }*/

              .k-pager-wrap > .k-link > .k-icon {
                  margin-top: 0px;
                  margin-left: 2.7px;
                  color: inherit;
              }

              .toolbar {
                  float: left;
              }

              html .k-grid tr:hover {
                  background: white;
              }

              html .k-grid tr.k-alt:hover {
                  background: white;
              }


              .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
                  margin-right: 0px;
                  margin-right: 0px;
                  margin-left: 0px;
                  margin-left: 0px;
              }

              .k-auto-scrollable {
                  overflow: hidden;
              }

              .k-grid-header {
                  padding-right: 0px !important;
                  margin-right: 2px;
              }

              .k-filter-menu .k-button {
                  width: 27%;
              }

              .k-label input[type="checkbox"] {
                  margin: 0px 5px 0 !important;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  font-size: 15px;
                  background: #f8f8f8;
                  font-family: 'Roboto',sans-serif;
                  color: #2b2b2b;
                  font-weight: 400;
              }

              .k-primary {
                  border-color: #1fd9e1;
                  background-color: #1fd9e1;
              }

              .k-pager-wrap {
                  background-color: white;
                  color: #2b2b2b;
              }

              td.k-command-cell {
                  border-width: 0 0px 1px 0px;
                  text-align: center;
              }

              .k-grid-pager {
                  margin-top: -1px;
              }

              span.k-icon.k-i-calendar {
                  margin-top: 6px;
              }

              .col-md-2 {
                  width: 20%;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  border-width: 0px 0px 1px 0px;
                  background: #E9EAEA;
                  font-weight: bold;
                  margin-right: 18px;
                  font-family: 'Roboto',sans-serif;
                  font-size: 15px;
                  color: rgba(0, 0, 0, 0.5);
                  height: 20px;
                  vertical-align: middle;
              }

              .k-dropdown-wrap.k-state-default {
                  background-color: white;
                  height: 34px;
              }

              .k-multiselect-wrap, .k-floatwrap {
                  height: 34px;
              }

              .k-popup.k-calendar-container, .k-popup.k-list-container {
                  background-color: white;
              }

              .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
                  -webkit-box-shadow: inset 0 0 3px 1px white;
                  box-shadow: inset 0 0 3px 1px white;
              }

              label.k-label {
                  font-family: roboto,sans-serif !important;
                  color: #515967;
                  /* font-stretch: 100%; */
                  font-style: normal;
                  font-weight: 400;
                  min-width: max-content !important;
                  white-space: pre-wrap;
              }

              .k-multicheck-wrap .k-item {
                  line-height: 1.2em;
                  font-size: 14px;
                  margin-bottom: 5px;
              }

              label {
                  display: flex;
                  margin-bottom: 0px;
              }

              .k-state-default > .k-select {
                  border-color: #ceced2;
                  margin-top: 3px;
              }

              .k-grid-norecords {
                  width: 100%;
                  height: 100%;
                  text-align: center;
                  margin-top: 2%;
              }

              .k-grouping-header {
                  font-style: italic;
                  background-color: white;
              }

              .k-grid-toolbar {
                  background: white;
                  border: none;
              }

              .k-grid table {
                  width: 100.5%;
              }

              .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
                  color: #515967;
                  padding-top: 5px;
              }

              .k-filter-row th, .k-grid-header th[data-index='11'] {
                  text-align: center;
              }

              .k-i-filter-clear {
                  margin-top: -3px;
              }

              .k-button {
                  margin-top: 2.5px;
              }

              label.k-label:hover {
                  color: #1fd9e1;
              }

              .k-tooltip {
                  margin-top: 5px;
              }

              .k-column-menu > .k-menu {
                  background-color: white;
              }
          </style>

    <script type="text/javascript">
        var EmployeeMasterListData = [];
        $(document).ready(function () {
            $("#divPrincipleEmployer").hide();
            BindClientList();
            BindPrincipleEmployerTable("");


            $("#btnNewPrincipleEmployer").on("click", function (e) {
                $("#divMsg").hide();

                ClrEmployerCreation();
                // $("#divStatus").hide();

                if ($("#ddlClient").val() != "-1") {
                    $("#ModalPrincipleEmployer").modal("show");
                    $("#divCheckPE").hide();
                }
                else {
                    //alert("Please Select Client");
                    $("#divCheckPE").html("");
                    displayErrors("divCheckPE", "Please Select Client");
                }

            });

            $("#btnNewPELocation").on("click", function (e) {
                $("#divMsgPELocation").hide();
                ClrPELocation();

                $("#ModalPrincipleEmployerLocation").modal("show");
            });

            $("#btnMigrate").on("click", function (e) {
                GetContractDetails();

                $("#divMsgEmployees").hide();
                clrMigrationDetails();

                $("#divContractEnd").hide();
                $("#divMigrateLocation").show();//must uncomment if problem
                $("#ModalEmployeeMigrate").modal("show");
                $("#hdMode").val("A"); //add mode

                $("#ddlStateEmployeeMaster").select2();
                $("#ddlLocationEmployeeMaster").select2();
                $("#ddlBranchEmployeeMaster").select2();

            });
            $("#btnUploadEmployee").on("click", function (e) {
                //$("#UploadEmployeeModal").modal("show");
                OpenUploadPopup();
            });

            $("#btnUploadContractors").on("click", function (e) {
                OpenContractorUploadPopup();
            });

            $("#btnUploadLocation").on("click", function (e) {
                OpenLocationUploadPopup();
            });
            

            $("#btnAddContractor").on("click", function (e) {
                $("#divMsgContractor").hide();
                ClrContractorCreation();
                $("#ModalContractorMaster").modal("show");

            });

            $("#btnNext").on("click", function (e) {
                if ($("#ddlClient").val() == "-1") {
                    //alert("Please select client");
                    //$("#btnNext").removeClass("js-btn-next");
                    //$("#btnPELocationTab").removeClass("multisteps-form__progress-btn");
                    $(".tabdata").hide();
                    $("#divPECreation").show();
                    $("#divCheckPE").html("");
                    displayErrors("divCheckPE", "Please Select Client");
                }
                else {
                    $("#divCheckPE").hide();
                    if ($("#ddlPrincipleEmployerState").val() == null || $("#ddlPrincipleEmployerState").val() == "" || $("#ddlPrincipleEmployerState").val() == "-1") {
                        $("#ddlPrincipleEmployerState").val("-1");
                    }

                    if ($("#ddlPrincipleEmployerLocation").val() == null || $("#ddlPrincipleEmployerLocation").val() == "" || $("#ddlPrincipleEmployerLocation").val() == "-1") {
                        $("#ddlPrincipleEmployerLocation").val("-1");
                    }
                    $(".tabdata").hide();
                    $("#divPELocationCreation").show();
                    $("#ddlPrincipleEmployer").select2();
                    var items = parent.document.getElementsByClassName('listItem');

                    for (var i = 0; i < items.length; i++) {
                        items[i].classList.remove("active");
                    }

                    var item = parent.document.getElementById("liPELocationCreation");
                    item.classList.add("active");

                }
            });




            $("#btnLocationNext").on("click", function (e) {
                $(".tabdata").hide();
                if ($("#ddlPrincipleEmployer").val() == "-1") {
                    //alert("Please select principle employer");

                    $("#divCheckPELocation").html("");
                    displayErrors("divCheckPELocation", "Please select principle employer");

                    $("#divPELocationCreation").show();

                }
                else if ($("#ddlPrincipleEmployerState").val() == "-1") {
                    //alert("Please select state");
                    $("#divCheckPELocation").html("");
                    displayErrors("divCheckPELocation", "Please select state");

                    $("#divPELocationCreation").show();
                }
                else if ($("#ddlPrincipleEmployerLocation").val() == "-1") {
                    //alert("Please select location");
                    $("#divCheckPELocation").html("");
                    displayErrors("divCheckPELocation", "Please select location");

                    $("#divPELocationCreation").show();
                }
                else {
                    $("#divCheckPELocation").hide();

                    if ($("#ddlPrincipleEmployerMaster").val() == null || $("#ddlPrincipleEmployerMaster").val() == "" || $("#ddlPrincipleEmployerMaster").val() == "-1") {
                        BindPrincipleEmployerListOnEmployeeMaster();
                    }

                    // $("#btnLocationNext").addClass("js-btn-next");
                    //$(".tabdata").hide();
                    $("#divPEMaster").show();

                    var items = parent.document.getElementsByClassName('listItem');
                    for (var i = 0; i < items.length; i++) {
                        items[i].classList.remove("active");
                    }

                    var item = parent.document.getElementById("liPEMaster");
                    item.classList.add("active");
                }
            });

            $("#btnLocationPrev").on("click", function (e) {
                TabInactive();
                var item = parent.document.getElementById("liPECreation");
                item.classList.add("active");
                $(".tabdata").hide();
                $("#divPECreation").show();
            });




            function TabInactive() {
                var items = parent.document.getElementsByClassName('listItem');
                for (var i = 0; i < items.length; i++) {
                    items[i].classList.remove("active");
                }
            }






            $("#btnEmpMasterNext").on("click", function (e) {
                $(".tabdata").hide();
                if ($("#ddlPrincipleEmployerMaster").val() == "-1") {
                    //alert("Please select principle employer");
                    $("#divCheckEmployees").html("");
                    displayErrors("divCheckEmployees", "Please select principle employer");

                    $("#divPEMaster").show();
                }
                else if ($("#ddlPrincipleEmployerStateMaster").val() == "-1") {
                    //alert("Please select state");
                    $("#divCheckEmployees").html("");
                    displayErrors("divCheckEmployees", "Please select state");

                    $("#divPEMaster").show();
                }
                else if ($("#ddlPrincipleEmployerLocationMaster").val() == "-1") {
                    //alert("Please select location");
                    $("#divCheckEmployees").html("");
                    displayErrors("divCheckEmployees", "Please select location");

                    $("#divPEMaster").show();
                }
                else if ($("#ddlPrincipleEmployerBranchMaster").val() == "-1") {
                    //alert("Please select branch");
                    $("#divCheckEmployees").html("");
                    displayErrors("divCheckEmployees", "Please select branch");

                    $("#divPEMaster").show();
                }
                else {
                    $("#divCheckEmployees").html("");
                    $("#divCheckEmployees").hide();


                    if ($("#ddlPrincipleemployerContractor").val() == null || $("#ddlPrincipleemployerContractor").val() == "" || $("#ddlPrincipleemployerContractor").val() == "-1") {
                        BindPrincipleEmployerListOnEmployeeMaster('Y');
                    }

                    //BindContractorTable();
                    //$("#btnEmpMasterNext").addClass("js-btn-next");

                    //$(".tabdata").hide();
                    $("#divContractorDetails").show();

                    var items = parent.document.getElementsByClassName('listItem');
                    for (var i = 0; i < items.length; i++) {
                        items[i].classList.remove("active");
                    }

                    var item = parent.document.getElementById("liContractorDetails");
                    item.classList.add("active");
                }


            });

            $("#btnEmpMasterPrev").on("click", function (e) {
                TabInactive();
                var item = parent.document.getElementById("liPELocationCreation");
                item.classList.add("active");
                $(".tabdata").hide();
                $("#divPELocationCreation").show();
            });








            $("#btnContractorNext").on("click", function (e) {
                $(".tabdata").hide();
                if ($("#ddlPrincipleemployerContractor").val() == "-1") {
                    //alert("Please select principle employer");
                    $("#divCheckContractor").html("");
                    displayErrors("divCheckContractor", "Please select principle employer");

                    $("#divContractorDetails").show();

                }
                else if ($("#ddlStateContractor").val() == "-1") {
                    //alert("Please select state");

                    $("#divCheckContractor").html("");
                    displayErrors("divCheckContractor", "Please select state");

                    $("#divContractorDetails").show();
                }
                else if ($("#ddlLocationContractor").val() == "-1") {
                    //alert("Please select location");

                    $("#divCheckContractor").html("");
                    displayErrors("divCheckContractor", "Please select location");

                    $("#divContractorDetails").show();
                }
                else if ($("#ddlBranchContractor").val() == "-1") {
                    //alert("Please select branch");
                    $("#divCheckContractor").html("");
                    displayErrors("divCheckContractor", "Please select branch");

                    $("#divContractorDetails").show();
                }
                else {
                    $("#divCheckContractor").html("");
                    $("#divCheckContractor").hide();

                    if ($("#ddlClientReport").val() == null || $("#ddlClientReport").val() == "" || $("#ddlClientReport").val() == "-1") {
                        BindClientList("R");
                    }

                    $("#divPEReport").show();

                    var items = parent.document.getElementsByClassName('listItem');
                    for (var i = 0; i < items.length; i++) {
                        items[i].classList.remove("active");
                    }

                    var item = parent.document.getElementById("liPEReport");
                    item.classList.add("active");
                }


            });


            $("#btnContractorPrev").on("click", function (e) {
                TabInactive();
                var item = parent.document.getElementById("liPEMaster");
                item.classList.add("active");
                $(".tabdata").hide();
                $("#divPEMaster").show();
            });


            $("#btnPEReportPrev").on("click", function (e) {
                TabInactive();
                var item = parent.document.getElementById("liContractorDetails");
                item.classList.add("active");
                $(".tabdata").hide();
                $("#divContractorDetails").show();
            });




            $("#ddlClient").on("change", function (e) {
                if ($("#ddlClient").val() != "-1") {
                    BindPrincipleEmployerTable($(this).val());
                    BindPrincipleEmployerList();
                    $("#divCheckPE").html("");
                    $("#divCheckPE").hide();
                }


            });

            $("#ddlClientReport").on("change", function (e) {
                if ($("#ddlClientReport").val() != "-1") {
                    $("#ddlClientReport").select2();
                    BindPrincipleEmployerList("R");
                }
            });

            $("#ddlPrincipleEmployerReport").on("change", function (e) {
                if ($("#ddlPrincipleEmployerReport").val() != "-1") {
                    $("#ddlPrincipleEmployerReport").select2();

                    if ($("#tblPrincipleEmployeeReport").hasClass("hidden"))
                        $("#tblPrincipleEmployeeReport").removeClass("hidden");
                    BindPrincipleEmployeeReport();
                }
            });


            $("#ddlPrincipleEmployer").on("change", function (e) {
                $("#divCheckPELocation").html("");
                $("#divCheckPELocation").hide();

                if ($("#ddlPrincipleEmployer").val() == "-1") {
                    $("#divState").addClass("hidden");
                    $("#divLocation").addClass("hidden");
                    $("#divNewLocation").addClass("hidden");
                    $("#tblPrincipleEmployerLocation").addClass("hidden");
                    //$("#divCheckPELocation").html("");
                    //displayErrors("divCheckPELocation", "Please Select Client");
                }
                else {
                    $("#ddlPrincipleEmployer").select2();
                    if ($("#divState").hasClass("hidden"))
                        $("#divState").removeClass("hidden");

                    // BindStateList();
                    BindClientStateList();
                    BindPrincipleEmployerLocationTable();
                    $("#divLocation").addClass("hidden");
                    $("#divNewLocation").addClass("hidden");
                    $("#tblPrincipleEmployerLocation").addClass("hidden");
                }

            });

            $("#ddlPrincipleEmployerMaster").on("change", function (e) {
                $("#divCheckEmployees").html("");
                $("#divCheckEmployees").hide();

                if ($("#ddlPrincipleEmployerMaster").val() == "-1") {
                    $("#divStateMaster").addClass("hidden");
                    $("#divLocationMaster").addClass("hidden");
                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerMaster").select2();

                    if ($("#divStateMaster").hasClass("hidden"))
                        $("#divStateMaster").removeClass("hidden");

                    BindStateListEmployeeMaster();
                    GetContractDetails();

                    $("#divLocationMaster").addClass("hidden");
                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }

            });

            $("#ddlPrincipleemployerContractor").on("change", function (e) {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();

                if ($("#ddlPrincipleemployerContractor").val() == "-1") {
                    $("#divStateContractor").addClass("hidden");
                    $("#divLocationContractor").addClass("hidden");
                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }
                else {
                    BindStateListContractorMaster();//must change

                    if ($("#divStateContractor").hasClass("hidden"))
                        $("#divStateContractor").removeClass("hidden");

                    BindStateListContractorMaster();//must change

                    $("#divLocationContractor").addClass("hidden");
                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }

            });

            $("#ddlPrincipleEmployerState").on("change", function (e) {
                $("#divCheckPELocation").html("");
                $("#divCheckPELocation").hide();

                if ($("#ddlPrincipleEmployerState").val() == "-1") {
                    $("#divLocation").addClass("hidden");
                    $("#divNewLocation").addClass("hidden");
                    $("#tblPrincipleEmployerLocation").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerState").select2();
                    if ($("#divLocation").hasClass("hidden"))
                        $("#divLocation").removeClass("hidden");

                    BindClientLocationList($(this).val());
                    BindPrincipleEmployerLocationTable();
                    $("#divNewLocation").addClass("hidden");
                    $("#tblPrincipleEmployerLocation").addClass("hidden");
                }


            });

            $("#ddlPrincipleEmployerLocation").on("change", function (e) {
                $("#divCheckPELocation").html("");
                $("#divCheckPELocation").hide();

                if ($("#ddlPrincipleEmployerLocation").val() == "-1") {
                    $("#divNewLocation").addClass("hidden");
                    $("#tblPrincipleEmployerLocation").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerLocation").select2();

                    if ($("#divNewLocation").hasClass("hidden"))
                        $("#divNewLocation").removeClass("hidden");

                    if ($("#tblPrincipleEmployerLocation").hasClass("hidden"))
                        $("#tblPrincipleEmployerLocation").removeClass("hidden");


                    BindPrincipleEmployerLocationTable();
                    debugger;
                    //var employerID = $("#ddlPrincipleEmployer").val();
                  

                   <%-- '<%Session["employerId"] = "' + employerID + '"; %>';
                     alert('<%=Session["employerId"] %>');--%>
                    
                   
                }

            });

            $("#ddlPrincipleEmployerStateMaster").on("change", function (e) {
                $("#divCheckEmployees").html("");
                $("#divCheckEmployees").hide();

                if ($("#ddlPrincipleEmployerStateMaster").val() == "-1") {
                    $("#divLocationMaster").addClass("hidden");
                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerStateMaster").select2();

                    if ($("#divLocationMaster").hasClass("hidden"))
                        $("#divLocationMaster").removeClass("hidden");

                    BindLocationListEmployeeMaster($(this).val());
                    BindEmployeeMasterTable();

                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }

            });

            $("#ddlStateContractor").on("change", function (e) {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();

                if ($("#ddlStateContractor").val() == "-1") {
                    $("#divLocationContractor").addClass("hidden");
                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }
                else {
                    $("#ddlStateContractor").select2();

                    if ($("#divLocationContractor").hasClass("hidden"))
                        $("#divLocationContractor").removeClass("hidden");

                    BindLocationListContractorMaster($(this).val());

                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");

                }


            });

            $("#ddlPrincipleEmployerLocationMaster").on("change", function (e) {
                $("#divCheckEmployees").html("");
                $("#divCheckEmployees").hide();

                if ($("#ddlPrincipleEmployerLocationMaster").val() == "-1") {
                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerLocationMaster").select2();
                    if ($("#divBranchMaster").hasClass("hidden"))
                        $("#divBranchMaster").removeClass("hidden");

                    BindBranchListEmployeeMaster();
                    BindEmployeeMasterTable();
                    //$("#tblEmployeeMaster").removeClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }

            });

            $("#ddlLocationContractor").on("change", function (e) {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();

                if ($("#ddlLocationContractor").val() == "-1") {
                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }
                else {
                    $("#ddlLocationContractor").select2();
                    if ($("#divBranchContractor").hasClass("hidden"))
                        $("#divBranchContractor").removeClass("hidden");

                    BindBranchListContractor();
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }

            });

            $("#ddlPrincipleEmployerBranchMaster").on("change", function (e) {

                $("#divCheckEmployees").html("");
                $("#divCheckEmployees").hide();

                if ($("#ddlPrincipleEmployerBranchMaster").val() == "-1") {
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerBranchMaster").select2();

                    if ($("#divMigrateMaster").hasClass("hidden"))
                        $("#divMigrateMaster").removeClass("hidden");

                    BindEmployeeMasterTable();

                    if ($("#tblEmployeeMaster").hasClass("hidden"))
                        $("#tblEmployeeMaster").removeClass("hidden");

                }
            });

            $("#ddlBranchContractor").on("change", function (e) {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();

                if ($("#ddlBranchContractor").val() == "-1") {
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }
                else {
                    $("#ddlBranchContractor").select2();
                    if ($("#divContractor").hasClass("hidden"))
                        $("#divContractor").removeClass("hidden");

                    if ($("#tblContractorMaster").hasClass("hidden"))
                        $("#tblContractorMaster").removeClass("hidden");

                    BindContractorTable();
                }

            });

            $("#ddlStateEmployeeMaster").on("change", function (e) {
                $("#ddlStateEmployeeMaster").select2();
                BindMigrateLocationList($(this).val());
            });

            $("#ddlLocationEmployeeMaster").on("change", function (e) {
                $("#ddlLocationEmployeeMaster").select2();
                BindMigrateBranchListEmployeeMaster();
            });

            $("#ddlBranchEmployeeMaster").on("change", function (e) {
                $("#ddlBranchEmployeeMaster").select2();
            });

            $('#ModalEmployeeMigrate').on('shown.bs.modal', function (e) {
                BindMigrateStateList();
            })


            $("#btnSave").click(function (e) {
                if (ValidatePrincipleEmployer()) {

                    var id = $("#lblPEID").text();
                    //alert("id is "+id);//must comment
                    $("#divErrorsPE").hide();
                    var DetailsObj = {
                        ID: $("#lblPEID").text(),
                        ClientID: $("#ddlClient").val(),
                        PEName: $("#txtPEName").val(),
                        NatureOfBusiness: $("#txtNatureOfBusiness").val(),
                        //ContractFrom: $("#txtContractForm").val(),
                        //ContractTo: $("#txtContractTo").val(),
                        //NoOfEmployees: $("#txtNumberOfEmployees").val(),
                        //Address: $("#txtAddress").val(),
                        //Contractvalue: $("#txtControctalue").val(),
                        //SecurityDeposit: $("#txtSecurityDeposit").val(),
                        //IsCentralAct: ($("#chkCentral").is(":checked") ? 1 : 0),
                        ////,Status: ($("#chkStatus").is(":checked") ? "A" : "I")
                        //PE_PAN: $("#txtPEPAN").val()
                    }

                    //$.ajax({
                    //    type: "POST",
                    //    url: "/CLRA/PrincipleEmployerCreation.aspx/GetPanDetails",
                    //    data: JSON.stringify({ DetailsObj }),
                    //    contentType: "application/json; charset=utf-8",
                    //    dataType: "json",
                    //    success: function (data) {
                    //        var Success = JSON.parse(data.d);
                    //        if (Success) {
                    //            $("#divErrorsPE").show();
                    //            $("#divMsg").hide();
                    //            $("#spanMsg").hide();
                    //            $("#divErrorsPE").html("PAN Already Exist...");
                    //                $("#btnSave").attr("disabled", true);
                               
                    //            setTimeout(function () {

                    //                $("#ModalPrincipleEmployer").modal("hide");
                    //                ClrEmployerCreation();
                    //                $("#btnSave").attr("disabled", false);
                    //            }, 3000);

                    //        }
                    //        else
                    //        {
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerCreation.aspx/SavePrincipleEmployerDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            if (Success) {
                                $("#divErrorsPE").hide();
                                $("#divMsg").show();

                                $("#spanMsg").show();
                                if (id == "0") {
                                    $("#spanMsg").html("Principle Employer Saved Successfully.");
                                    $("#btnSave").attr("disabled", true);
                                }
                                else {
                                    $("#spanMsg").html("Principle Employer Updated Successfully.");
                                    $("#btnSave").attr("disabled", true);
                                }


                                setTimeout(function () {

                                    $("#ModalPrincipleEmployer").modal("hide");
                                    ClrEmployerCreation();
                                    $("#btnSave").attr("disabled", false);
                                }, 3000);

                            }
                            else {
                                alert("Server error occured. Please try again");
                            }

                            BindPrincipleEmployerList();
                            BindPrincipleEmployerTable($("#ddlClient").val());

                        },
                        failure: function (data) {
                            alert(data);
                        },
                        error: function (error) {

                        }
                    });
                    //}
                            
                }
                //});
            });
            //)};



            $("#btnContractorSave").click(function (e) {
                if (ValidatePrincipleEmployerContractor()) {

                    var id = $("#lblContractorId").text();
                    //alert(id);//must comment
                    $("#divErrorsContractor").hide();
                    var DetailsObj = {
                        PECID: $("#lblContractorId").text(),
                        PEC_PEID: $("#ddlPrincipleemployerContractor").val(),
                        ContractorName: $("#txtContractorName").val(),
                        Address: $("#txtContractorAddres").val(),
                        ContractFrom: $("#txtContractorContractForm").val(),
                        ContractTo: $("#txtContractorContractTo").val(),
                        NatureOfWork: $("#txtContractorNatureOfWork").val(),
                        NoOfEmployees: $("#txtContractorNoOfEmployee").val(),
                        Canteen: ($("#chkCanteenProvided").is(":checked") ? "Y" : "N"),
                        Restroom: ($("#chkRestRoomProvided").is(":checked") ? "Y" : "N"),
                        Creches: ($("#chkCrechesProvided").is(":checked") ? "Y" : "N"),
                        DrinkingWater: ($("#chkDrinkingWaterProvided").is(":checked") ? "Y" : "N"),
                        FirstAid: ($("#chkFirstAidProvided").is(":checked") ? "Y" : "N"),
                        State: $("#ddlStateContractor").val(),
                        Location: $("#ddlLocationContractor").val(),
                        Branch: $("#ddlBranchContractor").val()
                    }

                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerCreation.aspx/SaveContractorDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            if (Success) {
                                //if(id=="0")
                                //    alert("Contractor Saved Successfully.");
                                //else
                                //    alert("Contractor Updated Successfully.");


                                $("#divErrorsContractor").hide();
                                $("#divMsgContractor").show();

                                if (id == "0")
                                    $("#spanMsgContractor").html("Contractor Saved Successfully.");
                                else
                                    $("#spanMsgContractor").html("Contractor Updated Successfully.");

                                setTimeout(function () {

                                    $("#ModalContractorMaster").modal("hide");
                                    ClrContractorCreation();

                                }, 3000);

                                //$("#lblContractorId").text("0");
                                //$("#txtContractorName").val("");
                                //$("#txtContractorAddres").val("");
                                //$("#txtContractorContractForm").val("");
                                //$("#txtContractorContractTo").val("");
                                //$("#txtContractorNatureOfWork").val("");
                                //$("#txtContractorNoOfEmployee").val("");
                                //$("#chkCanteenProvided").prop('checked', false);
                                //$("#chkRestRoomProvided").prop('checked', false);
                                //$("#chkCrechesProvided").prop('checked', false);
                                //$("#chkDrinkingWaterProvided").prop('checked', false);
                                //$("#chkFirstAidProvided").prop('checked', false);

                                //$("#ModalContractorMaster").modal("hide");
                            }

                            //BindPrincipleEmployerTable($("#ddlClient").val());
                            BindContractorTable();
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
            });




            $("#btnMigrateSave").click(function (e) {
                if ($("#hdMode").val() == "A") //add mode
                {
                    if (EmployeeMasterListData.length == 0) {
                        alert("Please select CheckList");
                        return false;
                    }
                }


                if (ValidateEmployeeMigration()) {
                    var cl = $("#ddlClient").val();
                    //alert("clientid is " + cl);
                    $("#divErrorsEmployees").hide();
                    var DetailsObj = {
                        MigrateEmployee: EmployeeMasterListData,
                        State: $("#ddlStateEmployeeMaster").val(),
                        Location: $("#ddlLocationEmployeeMaster").val(),
                        Branch: $("#ddlBranchEmployeeMaster").val(),
                        ContractFrom: $("#txtEmployeeContractForm").val(),
                        ContractTo: $("#txtEmployeeContractTo").val(),
                        ContractEndDate: $("#txtContractEndDate").val(),
                        ReasonForContractEnd: $("#txtContractEndReason").val(),
                        ClientID: $("#ddlClient").val(),
                        Mode: $("#hdMode").val(),
                        PELID: $("#lblEMP_PELID").val(),
                        EmployeeID: $("#lblEmployeeID").val(),
                    }

                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerCreation.aspx/SaveMigrateEmployeeDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            //var flag = false;
                            //if (data.d != "false") {
                            //    flag = true;
                            //}
                            //if (flag)
                            //    alert("Employee Migrated Successfully.");
                            //else
                            //    alert("Please select Employee");

                            if (data.d == "true") {
                                //if ($("#hdMode").val() == "A")
                                //    alert("Employee Migrated Successfully.");
                                //else
                                //    alert("Employee Updated Successfully.");

                                $("#divErrorsEmployees").hide();
                                $("#divMsgEmployees").show();

                                if ($("#hdMode").val() == "A")
                                    $("#spanMsgEmployees").html("Employees Migrated Successfully.");
                                else
                                    $("#spanMsgEmployees").html("Employee Updated Successfully.");

                                EmployeeMasterListData = [];
                                BindEmployeeMasterTable();

                                setTimeout(function () {
                                    $("#ModalEmployeeMigrate").modal("hide");
                                    clrMigrationDetails();
                                }, 3000);

                                //$("#ModalEmployeeMigrate").modal("hide");
                            }
                            else
                                alert("Server error occured. Please try again");

                        },
                        failure: function (data) {
                            alert(data);
                        }

                    });
                }


            });

            $("#btnLocationSave").click(function (e) {
                if (ValidatePrincipleEmployerLocation()) {
                    debugger;
                    var id = $("#lblPELID").text();
                    //alert("id is " + id);
                    $("#divErrorsPELocation").hide();
                    var DetailsObj = {
                        PLID: $("#lblPELID").text(),
                        PEID: $("#ddlPrincipleEmployer").val(),
                        State: $("#ddlPrincipleEmployerState").val(),
                        Location: $("#ddlPrincipleEmployerLocation").val(),
                        Branch: $("#txtBranch").val(),
                        NatureOfBusiness: $("#txtbarchNatureOfBusiness").val(),
                        Mines: ($("#chkMineApplicability").is(":checked") ? "1" : "0"),
                        WeekOff: $("#ddlWeekoff").val(),
                        //,Status: ($("#chklocationStatus").is(":checked") ? "A" : "I")

                        PE_LIN: $("#txtPE_LIN").val(),
                        PE_AuthorisedPerson_EmailID: $("#txtPE_AuthorisedPerson_EmailID").val(),
                        PE_Company_PhoneNo: $("#txtPE_Company_PhoneNo").val(),
                        Client_LINNo: $("#txtClient_LINNo").val(),
                        Client_CompanyEmailID: $("#txtClient_CompanyEmailID").val(),
                        Client_Company_Phone_No: $("#txtClient_Company_Phone_No").val(),
                        Contract_Licence_No: $("#txtContract_Licence_No").val(),
                        Licence_Valid_From_date: $("#txtLicenceValidFromdate").val(),
                        Licence_Valid_To_date: $("#txtLicenceValidTodate").val(),
                        Contractor_Person_Incharge_Name: $("#txtContractor_Person_Incharge_Name").val(),
                        Contractor_Person_Incharge_LIN: $("#txtContractor_Person_Incharge_LIN").val(),
                        Contractor_Person_Incharge_PAN: $("#txtContractor_Person_Incharge_PAN").val(),
                        Contractor_Person_Incharge_EmailID: $("#txtContractor_Person_Incharge_EmailID").val(),
                        Contractor_Person_Incharge_MobileNo: $("#txtContractor_Person_Incharge_MobileNo").val(),
                        Client_Nature_of_business: $("#txtClient_Nature_of_business").val(),
                        PE_Address: $("#txtPE_Address").val(),
                        Contractor_Licensing_Officer_Designation: $("#txtContractor_Licensing_Officer_Designation").val(),
                        Licencing_officer_Head_Quarter: $("#txtLicencing_officer_Head_Quarter").val(),
                        Nature_ofwelfare_amenities_provided: $("#txtNature_ofwelfare_amenities_provided").val(),
                        Statutory_statute: $("#txtStatutory_statute").val(),
                        Address: $("#txtBranch_Address").val(),
                        NumberOfEmp: $("#txtNumber_Employee").val(),
                        ContractFrom: $("#txtContractForm").val(),
                        ContractTo: $("#txtContractTo").val()

                    }

                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerCreation.aspx/SavePrincipleEmployerLocationDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            if (Success) {
                                //if(id == "0")
                                //    alert("Principle Employer Location Saved Successfully.");
                                //else
                                //    alert("Principle Employer Location Updated Successfully.");


                                $("#divErrorsPELocation").hide();
                                $("#divMsgPELocation").show();

                                if (id == "0")
                                    $("#spanMsgPELocation").html("Principle Employer Location Saved Successfully.");
                                else
                                    $("#spanMsgPELocation").html("Principle Employer Location Updated Successfully.");

                                setTimeout(function () {

                                    $("#ModalPrincipleEmployerLocation").modal("hide");
                                    ClrPELocation();

                                }, 3000);




                                /*$("#lblPELID").text("0");
                                $("#txtBranch").val("");
                                $("#txtbarchNatureOfBusiness").val("");
                                $("#ddlWeekoff").val("");


                                $('.multipleSelect').val("");
                                $('.fstChoiceRemove').click();
                                $('#ddlWeekoff').fastselect();

                                $("#chkMineApplicability").prop("checked", false);*/

                                //$("#ModalPrincipleEmployerLocation").modal("hide");

                            }
                            else {
                            }
                            BindPrincipleEmployerLocationTable();
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
            });

            $(document).on("click", "#tblPrincipleEmployer tbody tr .Update", function (e) {
                $("#divErrorsPE").hide();
                $("#divMsg").hide();
                ClrEmployerCreation();
                var item = $("#tblPrincipleEmployer").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblID").text(item.ID);
                var DetailsObj = {
                    ID: item.ID
                }
                $.ajax({
                    type: "POST",
                    url: "/CLRA/PrincipleEmployerCreation.aspx/GetPrincipleEmployerDetails",
                    data: JSON.stringify({ DetailsObj }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var EmployerDetails = JSON.parse(data.d);
                        if (EmployerDetails.length > 0) {
                            $("#lblPEID").text(EmployerDetails[0].ID);
                            $("#txtPEName").val(EmployerDetails[0].PEName);
                            $("#txtNatureOfBusiness").val(EmployerDetails[0].NatureOfBusiness);

                            const StartdateTime = EmployerDetails[0].ContractFrom;
                            //const parts = StartdateTime.split(/[- :]/);
                            const parts = StartdateTime.split(/[// :]/);
                            const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                            //$("#txtContractForm").val(FromDate); 

                            $("#txtContractForm").datepicker("setDate", FromDate);

                            const EnddateTime = EmployerDetails[0].ContractTo;
                            //const parts1 = EnddateTime.split(/[- :]/);
                            const parts1 = EnddateTime.split(/[// :]/);
                            const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                            //$("#txtContractTo").val(ToDate);
                            $("#txtContractTo").datepicker("setDate", ToDate);

                            $("#txtNumberOfEmployees").val(EmployerDetails[0].NoOfEmployees);
                            $("#txtAddress").val(EmployerDetails[0].Address);
                            $("#txtControctalue").val(EmployerDetails[0].Contractvalue);
                            $("#txtSecurityDeposit").val(EmployerDetails[0].SecurityDeposit);
                            debugger;
                            if (EmployerDetails[0].IsCentralAct != "False")
                                $("#chkCentral").prop("checked", true);
                            else
                                $("#chkCentral").prop("checked", false);

                            //if (EmployerDetails[0].Status == "A")
                            //    $("#chkStatus").prop("checked", true);
                            //else
                            //    $("#chkStatus").prop("checked", false);

                            //$("#divStatus").show();
                            $("#txtPEPAN").val(EmployerDetails[0].PE_PAN);
                            $("#ModalPrincipleEmployer").modal("show");
                            //OpenEditPrincipleEmployerPopup();
                        }
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });
            });



            $(document).on("click", "#tblContractorMaster tbody tr .Update", function (e) {
                $("#divErrorsContractor").hide();
                $("#divMsgContractor").hide();
                ClrContractorCreation();

                var item = $("#tblContractorMaster").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblContractorId").text(item.PECID);

                $.ajax({
                    type: "POST",
                    url: "/CLRA/PrincipleEmployerCreation.aspx/GetContractorDetails",
                    //data: JSON.stringify({ DetailsObj }),
                    data: '{pecid:' + item.PECID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var ContractorDetails = JSON.parse(data.d);
                        if (ContractorDetails != false) {
                            $("#txtContractorName").val(ContractorDetails.ContractorName);
                            $("#txtContractorAddres").val(ContractorDetails.Address);

                            const StartdateTime = ContractorDetails.ContractFrom;
                            //const parts = StartdateTime.split(/[- :]/);
                            const parts = StartdateTime.split(/[// :]/);
                            const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                            //$("#txtContractorContractForm").val(FromDate);
                            $("#txtContractorContractForm").datepicker("setDate", FromDate);

                            const EnddateTime = ContractorDetails.ContractTo;
                            //const parts1 = EnddateTime.split(/[- :]/);
                            const parts1 = EnddateTime.split(/[// :]/);
                            const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                            //$("#txtContractorContractTo").val(ToDate);
                            $("#txtContractorContractTo").datepicker("setDate", ToDate);

                            $("#txtContractorNatureOfWork").val(ContractorDetails.NatureOfWork);
                            $("#txtContractorNoOfEmployee").val(ContractorDetails.NoOfEmployees);

                            if (ContractorDetails.Canteen == "Y")
                                $("#chkCanteenProvided").prop("checked", true);
                            else
                                $("#chkCanteenProvided").prop("checked", false);

                            if (ContractorDetails.Restroom == "Y")
                                $("#chkRestRoomProvided").prop("checked", true);
                            else
                                $("#chkRestRoomProvided").prop("checked", false);

                            if (ContractorDetails.Creches == "Y")
                                $("#chkCrechesProvided").prop("checked", true);
                            else
                                $("#chkCrechesProvided").prop("checked", false);

                            if (ContractorDetails.DrinkingWater == "Y")
                                $("#chkDrinkingWaterProvided").prop("checked", true);
                            else
                                $("#chkDrinkingWaterProvided").prop("checked", false);

                            if (ContractorDetails.FirstAid == "Y")
                                $("#chkFirstAidProvided").prop("checked", true);
                            else
                                $("#chkFirstAidProvided").prop("checked", false);

                            $("#ModalContractorMaster").modal("show");
                            //OpenEditPrincipleEmployerPopup();
                        }
                        else
                            alert("Server error occured. Please try again");
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });
            });


            $(document).on("click", "#tblContractorMaster tbody tr .Delete", function (e) {
                var item = $("#tblContractorMaster").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblContractorId").text(item.PECID);

                var res = confirm("Are you sure you want to delete this contractor?");
                if (res == true) {
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerCreation.aspx/DeleteContractor",
                        //data: JSON.stringify({ DetailsObj }),
                        data: '{pecid:' + item.PECID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var result = JSON.parse(data.d);

                            if (result == true) {
                                alert("Contractor Deleted Successfully");
                                BindContractorTable();
                            }
                            else
                                alert("Some error occured. Try again");

                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }




            });

            $(document).on("click", "#tblPrincipleEmployer tbody tr .Delete", function (e) {
                var item = $("#tblPrincipleEmployer").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblPEID").text(item.ID);
                //alert("item id is " + item.ID);

                var res = confirm("Are you sure you want to delete this Principle Employer?");
                if (res == true) {
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerCreation.aspx/DeletePrincipleEmployer",
                        //data: JSON.stringify({ DetailsObj }),
                        data: '{peid:' + item.ID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var result = JSON.parse(data.d);

                            if (result == true) {
                                alert("Principle Employer Deleted Successfully");
                                BindPrincipleEmployerList();
                                BindPrincipleEmployerTable($("#ddlClient").val());
                            }
                            else
                                alert("Server error occured. Please try again");

                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }




            });


            $(document).on("click", "#tblPrincipleEmployerLocation tbody tr .Update", function (e) {
                $("#divErrorsPELocation").hide();
                $("#divMsgPELocation").hide();
                //ClrPELocation();

                var item = $("#tblPrincipleEmployerLocation").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblPELID").text(item.PLID);
                var DetailsObj = {
                    PLID: item.PLID
                }
                $.ajax({
                    type: "POST",
                    url: "/CLRA/PrincipleEmployerCreation.aspx/GetPrincipleEmployerLocationDetails",
                    data: JSON.stringify({ DetailsObj }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var LocationDetails = JSON.parse(data.d);
                        debugger;
                        if (LocationDetails.length > 0) {
                            $("#lblPELID").text(LocationDetails[0].PLID);
                            $("#txtBranch").val(LocationDetails[0].Branch);
                            $("#txtbarchNatureOfBusiness").val(LocationDetails[0].NatureOfBusiness);

                            $("#txtPE_LIN").val(LocationDetails[0].PE_LIN);
                            $("#txtPE_AuthorisedPerson_EmailID").val(LocationDetails[0].PE_AuthorisedPerson_EmailID);
                            $("#txtPE_Company_PhoneNo").val(LocationDetails[0].PE_Company_PhoneNo);
                            $("#txtClient_LINNo").val(LocationDetails[0].Client_LINNo);
                            $("#txtClient_CompanyEmailID").val(LocationDetails[0].Client_CompanyEmailID);
                            $("#txtClient_Company_Phone_No").val(LocationDetails[0].Client_Company_Phone_No);
                            $("#txtContract_Licence_No").val(LocationDetails[0].Contract_Licence_No);
                            
                            const StartdateTime = LocationDetails[0].Licence_Valid_From_date;
                            if (StartdateTime===null) {
                            }
                            else if (StartdateTime != "01/01/0001") {
                                const parts = StartdateTime.split(/[// :]/);
                                const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                $("#txtLicenceValidFromdate").datepicker("setDate", FromDate);
                            }
                            const EnddateTime = LocationDetails[0].Licence_Valid_To_date;
                            if (EnddateTime === null) {
                            }
                            else if (EnddateTime != "01/01/0001") {
                                const parts1 = EnddateTime.split(/[// :]/);
                                const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                                $("#txtLicenceValidTodate").datepicker("setDate", ToDate);
                            }

                            $("#txtContractor_Person_Incharge_Name").val(LocationDetails[0].Contractor_Person_Incharge_Name);
                            $("#txtContractor_Person_Incharge_LIN").val(LocationDetails[0].Contractor_Person_Incharge_LIN);
                            $("#txtContractor_Person_Incharge_PAN").val(LocationDetails[0].Contractor_Person_Incharge_PAN);
                            $("#txtContractor_Person_Incharge_EmailID").val(LocationDetails[0].Contractor_Person_Incharge_EmailID);
                            $("#txtContractor_Person_Incharge_MobileNo").val(LocationDetails[0].Contractor_Person_Incharge_MobileNo);
                            $("#txtClient_Nature_of_business").val(LocationDetails[0].Client_Nature_of_business);
                            $("#txtPE_Address").val(LocationDetails[0].PE_Address);
                            $("#txtContractor_Licensing_Officer_Designation").val(LocationDetails[0].Contractor_Licensing_Officer_Designation);
                            $("#txtLicencing_officer_Head_Quarter").val(LocationDetails[0].Licencing_officer_Head_Quarter);
                            $("#txtNature_ofwelfare_amenities_provided").val(LocationDetails[0].Nature_ofwelfare_amenities_provided);
                            $("#txtStatutory_statute").val(LocationDetails[0].Statutory_statute);

                            $("#ddlWeekoff").val("");

                            $('.multipleSelect').val("");
                            $('.fstChoiceRemove').click();
                            $('#ddlWeekoff').fastselect();

                            var array = LocationDetails[0].WeekOff;
                            for (i in array) {
                                if (array[i] != "")
                                    $('.multipleSelect').data('fastselect').setSelectedOption($('.multipleSelect option[value=' + array[i] + ' ]').get(0));
                                $("#ddlWeekoff option[value='" + array[i] + "']").prop("selected", true);
                            }
                            //var values = "Mon,Tue,Wed";
                            //$.each(values.split(","), function (i, e) {
                            //    $("#ddlWeekoff option[value='" + e + "']").prop("selected", true);
                            //});

                            //$('.multipleSelect').val("Mon").change();
                            //$('.multipleSelect').val("Tue").change();
                            //$('.multipleSelect').fastselect();

                            
                            if (LocationDetails[0].Mines != "False")
                                $("#chkMineApplicability").prop("checked", true);
                            else
                                $("#chkMineApplicability").prop("checked", false);
                            debugger;
                            $("#txtBranch_Address").val(LocationDetails[0].Address);
                            $("#txtNumber_Employee").val(LocationDetails[0].NumberOfEmp);
                            $("#txtContractForm").val(LocationDetails[0].ContractFrom);
                            $("#txtContractTo").val(LocationDetails[0].ContractTo);


                            const ContractFromStartdateTime = LocationDetails[0].ContractFrom;
                            if (ContractFromStartdateTime === null) {
                            }
                            else if (ContractFromStartdateTime != "01/01/0001") {
                                const parts = ContractFromStartdateTime.split(/[// :]/);
                                const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                $("#txtContractForm").datepicker("setDate", FromDate);
                            }
                            const ContractToEnddateTime = LocationDetails[0].ContractTo;
                            if (ContractToEnddateTime === null) {
                            }
                            else if (ContractToEnddateTime != "01/01/0001") {
                                const parts1 = ContractToEnddateTime.split(/[// :]/);
                                const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                                $("#txtContractTo").datepicker("setDate", ToDate);
                            }
                            //if (LocationDetails[0].Status == "A")
                            //    $("#chklocationStatus").prop("checked", true);
                            //else
                            //    $("#chklocationStatus").prop("checked", false);

                            $("#ModalPrincipleEmployerLocation").modal("show");
                        }
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });


            });

            $(document).on("click", "#tblPrincipleEmployerLocation tbody tr .Delete", function (e) {
                debugger;

                var item = $("#tblPrincipleEmployerLocation").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblPELID").text(item.PLID);
                //alert("pelid is " + item.PLID);
                var DetailsObj = {
                    PLID: item.PLID
                }
                var res = confirm("Are you sure you want to delete this Principle Employer Location?");
                if (res == true) {
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerCreation.aspx/DeletePrincipleEmployerLocation",
                        //data: JSON.stringify({ DetailsObj }),
                        data: '{plid:' + item.PLID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var result = JSON.parse(data.d);

                            if (result == true) {
                                alert("Principle Employer Location Deleted Successfully");
                                BindPrincipleEmployerLocationTable();
                            }
                            else
                                alert("Server error occured. Please try again");

                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }


            });

            $(document).on("click", "#tblEmployeeMaster tbody tr .Update", function (e) {
                $("#divErrorsEmployees").hide();
                $("#divMsgEmployees").hide();
                clrMigrationDetails();

                var item = $("#tblEmployeeMaster").data("kendoGrid").dataItem($(this).closest("tr"));

                if (item.EmpID != "" && item.PEID != "") {
                    $("#lblPrincEmpId").val(item.PEID);
                    $("#lblEmployeeID").val(item.EmpID);
                    $("#lblEMP_PELID").val(item.EMP_PELID);
                    $("#hdMode").val("E"); //edit mode
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerCreation.aspx/GetPrincipleEmployeeDetails",
                        data: '{"empid":"' + item.EmpID + '","principleEmpId":"' + item.PEID + '","emp_pelid":"' + item.EMP_PELID + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var EmpDetails = JSON.parse(data.d);
                            if (EmpDetails != false) {
                                $("#divMigrateLocation").hide();
                                $("#divContractEnd").show();

                                $("#ModalEmployeeMigrate").modal("show");

                                if (EmpDetails.ContractFrom == "01-01-0001 00:00:00" || EmpDetails.ContractFrom == "01/01/0001 00:00:00" || EmpDetails.ContractFrom == "01/01/0001")
                                    $("#txtEmployeeContractForm").val("");
                                else {
                                    const StartdateTime = EmpDetails.ContractFrom;
                                    //const parts = StartdateTime.split(/[- :]/);
                                    const parts = StartdateTime.split(/[// :]/);
                                    const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                    //$("#txtEmployeeContractForm").val(FromDate);
                                    $("#txtEmployeeContractForm").datepicker("setDate", FromDate);
                                }


                                if (EmpDetails.ContractTo == "01-01-0001 00:00:00" || EmpDetails.ContractTo == "01/01/0001 00:00:00" || EmpDetails.ContractTo == "01/01/0001")
                                    $("#txtEmployeeContractTo").val("");
                                else {
                                    const StartdateTime = EmpDetails.ContractTo;
                                    //const parts = StartdateTime.split(/[- :]/);
                                    const parts = StartdateTime.split(/[// :]/);
                                    const ToDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                    //$("#txtEmployeeContractTo").val(ToDate);
                                    $("#txtEmployeeContractTo").datepicker("setDate", ToDate);
                                }


                                // if (EmpDetails. == "01-01-0001 00:00:00")
                                if (EmpDetails.ContractEndDate == "01-01-0001 00:00:00" || EmpDetails.ContractEndDate == "01/01/0001 00:00:00" || EmpDetails.ContractEndDate == "01/01/0001")
                                    $("#txtContractEndDate").val("");
                                else {
                                    const StartdateTime = EmpDetails.ContractEndDate;
                                    //const parts = StartdateTime.split(/[- :]/);
                                    const parts = StartdateTime.split(/[// :]/);
                                    const EndDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                    $("#txtContractEndDate").val(EndDate);
                                }
                                //$("#txtContractEndDate").val(EmpDetails.ContractEndDate);

                                $("#txtContractEndReason").val(EmpDetails.ReasonForContractEnd);
                            }
                            else
                                alert("Server error occured. Please try again.");
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }

            });


            $(document).on("click", "#tblEmployeeMaster tbody tr .Delete", function (e) {
                var item = $("#tblEmployeeMaster").data("kendoGrid").dataItem($(this).closest("tr"));
                var empid = item.EmpID;
                var peid = item.PEID;

                if (empid != "" && empid != null && peid != "" && peid != null) {
                    var res = confirm("Are you sure you want to delete this employee?");
                    if (res == true) {
                        $.ajax({
                            type: "POST",
                            url: "/CLRA/PrincipleEmployerCreation.aspx/DeleteEmployee",
                            data: '{"EmpID":"' + empid + '","PeID":"' + peid + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var result = JSON.parse(data.d);

                                if (result == true) {
                                    alert("Employee Deleted Successfully");
                                    BindEmployeeMasterTable();
                                }
                                else
                                    alert("Server error occured. Please try again.");

                            },
                            failure: function (data) {
                                alert(data);
                            }
                        });
                    }
                }


            });


            $("#btnSearch").click(function (e) {
                e.preventDefault();

                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#tblPrincipleEmployer").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 10,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "PEName", operator: "contains", value: $x },
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#tblPrincipleEmployer").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    //BindGrid();
                }
                // return false;
            });


        });


        function OpenUploadPopup() {
            var clientId = $("#ddlClient").val();
            $('#divReAssignCompliance').show();

            var myWindowAdv = $("#divReAssignCompliance");

            myWindowAdv.kendoWindow({
                width: "50%",
                height: '50%',
                //maxHeight: '90%',
                //minHeight:'50%',
                title: "Upload Principle Employees",
                visible: false,
                actions: ["Close"]
                //open: onOpen,                    
            });

            $('#iframeReAssign').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeReAssign').attr('src', "/CLRA/PrincipleEmployeeMasterUpload.aspx?clientid=" + clientId);

            var dialog = $("#divReAssignCompliance").data("kendoWindow");
            dialog.title("Upload Principle Employees");

            return false;
        }


        function OpenContractorUploadPopup() {
            var clientId = $("#ddlClient").val();
            $('#divUploadContractors').show();
            var myWindowAdv = $("#divUploadContractors");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '50%',
                title: "Upload Contractors",
                visible: false,
                actions: ["Close"],
                //open: onOpen, 
                close: onClose
            });

            $('#iframeUploadContractors').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeUploadContractors').attr('src', "/CLRA/ContractorMasterUpload.aspx?clientid=" + clientId);
            //$('#iframeUploadContractors').attr({ 'src': "/CLRA/ContractorMasterUpload.aspx?clientid=" + clientId, "scrolling": "no", })

            var dialog = $("#divUploadContractors").data("kendoWindow");
            dialog.title("Upload Contractors");

            return false;
        }

        function OpenLocationUploadPopup() {
            var clientId = $("#ddlClient").val();
            $('#divUploadLocation').show();
            var myWindowAdv = $("#divUploadLocation");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '50%',
                title: "Upload Location",
                visible: false,
                actions: ["Close"],
                //open: onOpen, 
                close: onCloseLocationPopup
            });

            $('#iframeUploadLocation').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeUploadLocation').attr('src', "/CLRA/LocationUpload.aspx?clientid=" + clientId + "&employerId=" + $("#ddlPrincipleEmployer").val() + "&employerState=" + $("#ddlPrincipleEmployerState").val() + "&employerLocation=" + $("#ddlPrincipleEmployerLocation").val());
            //$('#iframeUploadContractors').attr({ 'src': "/CLRA/ContractorMasterUpload.aspx?clientid=" + clientId, "scrolling": "no", })

            var dialog = $("#divUploadLocation").data("kendoWindow");
            dialog.title("Upload Locations");

            return false;
        }


        function onCloseLocationPopup() {
            BindPrincipleEmployerLocationTable();

        }

        function onClose() {
           BindContractorTable();
        }

        function OpenEditPrincipleEmployerPopup() {
            $('#divPrincipleEmployer').show();
            var myWindowAdv = $("#divPrincipleEmployer");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '90%',
                iframe: true,
                title: "Principle Employer Details",
                visible: true,
                actions: ["Close"]
                //close: onClose
            });

            //$('#iframeAdd').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            //$('#iframeAdd').attr('src', "/RLCS/RLCS_CustomerDetails.aspx?CustomerID=" + CustID);
            return false;
        }

        function ClrEmployerCreation() {
            //$("#lblPEID").val("0");
            $("#lblPEID").text("0");
            $("#txtBranch_Address").val("");
            $("#txtNumber_Employee").val("");
            $("#txtPEName").val("");
            $("#txtNatureOfBusiness").val("");
            $("#txtContractForm").val(""); //must uncomment
            $("#txtContractTo").val("");
            $("#txtNumberOfEmployees").val("");
            $("#txtAddress").val("");
            $("#txtControctalue").val("");
            $("#txtSecurityDeposit").val("");
            $("#chkCentral").prop("checked", false);
            $("#divErrorsPE").hide();
            $("#txtPEPAN").val("");
            $('#txtContractForm').datepicker('setDate', null);
            $('#txtContractTo').datepicker('setDate', null);

            //$('#txtContractForm').datepicker('minDate', null);
            //$('#txtContractForm').datepicker('maxDate', null);

            $('#txtContractTo').datepicker('minDate', null);
            $('#txtContractTo').datepicker('maxDate', null);

            //$('#txtContractForm').data("DateTimePicker").clear();
            //$("#txtContractForm").data("DateTimePicker").date(null);
            //$('#txtContractForm').datepicker('setDate', null);
            //$("#chkStatus").prop("checked", false);

            //$('#txtContractForm').datepicker('setDate', new Date());
            //$('#txtContractForm').datepicker('setDate', null);
        }

        function ClrPELocation() {
            $("#lblPELID").text("0");
            $("#txtBranch").val("");
            $("#txtbarchNatureOfBusiness").val("");
            $("#txtBranch_Address").val("");
            $("#txtContractForm").val("");
            $("#txtContractTo").val("");
            $("#txtNumber_Employee").val("");
            $("#txtPE_LIN").val("");
            $("#txtPE_AuthorisedPerson_EmailID").val("");
            $("#txtPE_Company_PhoneNo").val("");
            $("#txtClient_LINNo").val("");
            $("#txtClient_CompanyEmailID").val("");
            $("#txtClient_Company_Phone_No").val("");
            $("#txtContract_Licence_No").val("");
            $("#txtLicenceValidFromdate").val("");
            $("#txtLicenceValidTodate").val("");
            $("#txtContractor_Person_Incharge_Name").val("");
            $("#txtContractor_Person_Incharge_LIN").val("");
            $("#txtContractor_Person_Incharge_PAN").val("");
            $("#txtContractor_Person_Incharge_EmailID").val("");
            $("#txtContractor_Person_Incharge_MobileNo").val("");
            $("#txtClient_Nature_of_business").val("");
            $("#txtPE_Address").val("");
            $("#txtContractor_Licensing_Officer_Designation").val("");
            $("#txtLicencing_officer_Head_Quarter").val("");
            $("#txtNature_ofwelfare_amenities_provided").val("");
            $("#txtStatutory_statute").val("");

            $("#ddlWeekoff").val("");
            $('.multipleSelect').val("");
            $('.fstChoiceRemove').click();
            $('#ddlWeekoff').fastselect();

            $("#chkMineApplicability").prop("checked", false);
            $("#divErrorsPELocation").hide();

        }



        function ClrContractorCreation() {

            $("#lblContractorId").text("0");
            $("#txtContractorName").val("");
            $("#txtContractorNatureOfWork").val("");
            $("#txtContractorContractForm").val("");
            $("#txtContractorContractTo").val("");
            $("#txtContractorNoOfEmployee").val("");
            $("#txtContractorAddres").val("");

            $("#chkCanteenProvided").prop("checked", false);
            $("#chkRestRoomProvided").prop("checked", false);
            $("#chkCrechesProvided").prop("checked", false);
            $("#chkDrinkingWaterProvided").prop("checked", false);
            $("#chkFirstAidProvided").prop("checked", false);
            $("#divErrorsContractor").hide();

            $('#txtContractorContractForm').datepicker('setDate', null);
            $('#txtContractorContractTo').datepicker('setDate', null);

            $('#txtContractorContractForm').datepicker('minDate', null);
            $('#txtContractorContractForm').datepicker('maxDate', null);

            $('#txtContractorContractTo').datepicker('minDate', null);
            $('#txtContractorContractTo').datepicker('maxDate', null);

        }

        function clrMigrationDetails() {
            $("#ddlStateEmployeeMaster").val("-1");
            $("#ddlLocationEmployeeMaster").empty();
            $("#ddlLocationEmployeeMaster").val("");
            $("#ddlLocationEmployeeMaster").append($("<option></option>").val("-1").html("Select Location"));
            $("#ddlLocationEmployeeMaster").select2();

            $("#ddlBranchEmployeeMaster").empty();
            $("#ddlBranchEmployeeMaster").val("");
            $("#ddlBranchEmployeeMaster").append($("<option></option>").val("-1").html("Select Branch"));
            $("#ddlBranchEmployeeMaster").select2();

            $("#lblEmployeeID").val("");
            $("#lblPrincEmpId").val("");
            $("#lblEMP_PELID").val("");
            $("#txtEmployeeContractForm").val("");
            $("#txtEmployeeContractTo").val("");
            $("#txtContractEndDate").val("");
            $("#txtContractEndReason").val("");
            $("#hdMode").val("");
            $("#divErrorsEmployees").hide();

            $('#txtEmployeeContractForm').datepicker('setDate', null);
            $('#txtEmployeeContractTo').datepicker('setDate', null);

            $('#txtEmployeeContractForm').datepicker('minDate', null);
            $('#txtEmployeeContractForm').datepicker('maxDate', null);

            $('#txtEmployeeContractTo').datepicker('minDate', null);
            $('#txtEmployeeContractTo').datepicker('maxDate', null);






        }

        function GetContractDetails() {
            var DetailsObj = {
                ID: $("#ddlPrincipleEmployerMaster").val()
            }

            $.ajax({
                type: "POST",
              //  url: "/CLRA/PrincipleEmployerCreation.aspx/GetPrincipleEmployerDetails",
                url: "/CLRA/PrincipleEmployerCreation.aspx/GetPrincipleEmployerLocDetails",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var EmployerDetails = JSON.parse(data.d);
                    if (EmployerDetails.length > 0) {
                        const StartdateTime = EmployerDetails[0].ContractFrom;
                        //const parts = StartdateTime.split(/[- :]/);
                        const parts = StartdateTime.split(/[// :]/);
                        const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                        $("#txtPEContractFrom").val(FromDate);
                        const EnddateTime = EmployerDetails[0].ContractTo;
                        //const parts1 = EnddateTime.split(/[- :]/);
                        const parts1 = EnddateTime.split(/[// :]/);
                        const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                        $("#txtPEContractTo").val(ToDate);
                        $("#txtContractForm").val(EmployerDetails[0].ContractFrom);
                        $("#txtContractTo").val(EmployerDetails[0].ContractTo);
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }
        function BindClientList(mode) {
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindClientList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //var Customer = JSON.parse(data.d);
                    //if (Customer.length > 0)
                    //{
                    //    var controlId = "ddlClient";
                    //    if (mode == "R")//R stands for Report
                    //        controlId = "ddlClientReport"

                    //    $("#" + controlId).empty();
                    //    $("#" + controlId).append($("<option></option>").val("-1").html("Select Client"));
                    //    $.each(Customer, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.ClientID).html(value.ClientName));
                    //    })
                    //    $("#" + controlId).select2();
                    //}



                    var controlId = "ddlClient";
                    if (mode == "R")//R stands for Report
                        controlId = "ddlClientReport"

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Client"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.ClientID).html(value.ClientName));
                        })

                    }
                    $("#" + controlId).select2();


                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindPrincipleEmployerList(mode) {
            var controlId = "ddlPrincipleEmployer";
            var client = "ddlClient";

            if (mode == "R") {
                controlId = "ddlPrincipleEmployerReport";
                client = "ddlClientReport";
            }

            var ClientObj = {
                ClientID: $("#" + client).val()
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPrincipleEmployerList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //var Customer = JSON.parse(data.d);
                    //if (Customer.length > 0)
                    //{
                    //    $("#" + controlId).empty();
                    //    $("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));
                    //    $.each(Customer, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                    //    })
                    //    $("#" + controlId).select2();
                    //}

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }


        function BindPrincipleEmployerListOnEmployeeMaster(ForContractor) {
            var ClientObj = {
                ClientID: $("#ddlClient").val()
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPrincipleEmployerList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));
                    //$.each(Customer, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                    //})
                    //$("#" + controlId).select2();

                    var controlId = "ddlPrincipleEmployerMaster";
                    if (ForContractor == "Y") {
                        controlId = "ddlPrincipleemployerContractor";
                    }


                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                        })
                    }
                    $("#" + controlId).select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindStateList() {
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindStateList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var State = JSON.parse(data.d);
                    //if (State.length > 0) {
                    //    $("#ddlPrincipleEmployerState").empty();
                    //    $("#ddlPrincipleEmployerState").append($("<option></option>").val("-1").html("Select State"));
                    //    $.each(State, function (data, value) {
                    //        $("#ddlPrincipleEmployerState").append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#ddlPrincipleEmployerState").select2();
                    //}

                    $("#ddlPrincipleEmployerState").empty();
                    $("#ddlPrincipleEmployerState").val("");
                    $("#ddlPrincipleEmployerState").append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#ddlPrincipleEmployerState").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlPrincipleEmployerState").select2();


                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindClientStateList() {
            var ClientObj = {
                ClientID: $("#ddlClient").val()
            }

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindClientStateList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var State = JSON.parse(data.d);
                    //if (State.length > 0) {
                    //    $("#ddlPrincipleEmployerState").empty();
                    //    $("#ddlPrincipleEmployerState").append($("<option></option>").val("-1").html("Select State"));
                    //    $.each(State, function (data, value) {
                    //        $("#ddlPrincipleEmployerState").append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#ddlPrincipleEmployerState").select2();
                    //}

                    $("#ddlPrincipleEmployerState").empty();
                    $("#ddlPrincipleEmployerState").val("");
                    $("#ddlPrincipleEmployerState").append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#ddlPrincipleEmployerState").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlPrincipleEmployerState").select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });

        }


        function BindMigrateStateList() {

            var id = $("#ddlPrincipleEmployerMaster").val();
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPEStateList",
                data: '{peid:' + id + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var State = JSON.parse(data.d);
                    //if (State.length > 0) {
                    //    $("#ddlStateEmployeeMaster").empty();
                    //    $("#ddlStateEmployeeMaster").append($("<option></option>").val("-1").html("Select State"));
                    //    $.each(State, function (data, value) {
                    //        $("#ddlStateEmployeeMaster").append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#ddlStateEmployeeMaster").select2();
                    //}

                    $("#ddlStateEmployeeMaster").empty();
                    $("#ddlStateEmployeeMaster").val("");
                    $("#ddlStateEmployeeMaster").append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#ddlStateEmployeeMaster").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlStateEmployeeMaster").select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindStateListEmployeeMaster() {
            var ClientObj = {
                ClientID: $("#ddlClient").val()
            }

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindClientStateList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var State = JSON.parse(data.d);
                    //if (State.length > 0)
                    //{
                    //    var controlId = "ddlPrincipleEmployerStateMaster";

                    //    $("#" + controlId).empty();
                    //    $("#" + controlId).append($("<option></option>").val("-1").html("Select State"));
                    //    $.each(State, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#" + controlId).select2();
                    //}

                    var controlId = "ddlPrincipleEmployerStateMaster";
                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }


        function BindStateListContractorMaster() {
            var id = $("#ddlPrincipleemployerContractor").val();
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPEStateList",
                data: '{peid:' + id + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var controlId = "ddlStateContractor";
                    //$("#" + controlId).empty();
                    //$("#" + controlId).val("");
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select State"));
                    //$("#" + controlId).select2();

                    //var State = JSON.parse(data.d);
                    //if (State.length > 0)
                    //{
                    //    $.each(State, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#" + controlId).select2();
                    //}

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }


        function BindLocationList(stateid) {
            var StateObj = {
                Code: stateid
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindLocationList",
                data: JSON.stringify({ StateObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    //$("#ddlPrincipleEmployerLocation").empty();
                    //$("#ddlPrincipleEmployerLocation").append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#ddlPrincipleEmployerLocation").append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#ddlPrincipleEmployerLocation").select2();

                    $("#ddlPrincipleEmployerLocation").empty();
                    $("#ddlPrincipleEmployerLocation").val("");
                    $("#ddlPrincipleEmployerLocation").append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#ddlPrincipleEmployerLocation").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlPrincipleEmployerLocation").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }


        function BindClientLocationList(stateid) {
            var id = $("#ddlClient").val();
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindClientLocationList",
                data: '{"clientid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    //$("#ddlPrincipleEmployerLocation").empty();
                    //$("#ddlPrincipleEmployerLocation").append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#ddlPrincipleEmployerLocation").append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#ddlPrincipleEmployerLocation").select2();

                    $("#ddlPrincipleEmployerLocation").empty();
                    $("#ddlPrincipleEmployerLocation").val("");
                    $("#ddlPrincipleEmployerLocation").append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#ddlPrincipleEmployerLocation").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlPrincipleEmployerLocation").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindMigrateLocationList(stateid) {
            var id = $("#ddlPrincipleEmployerMaster").val();
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPELocationList",
                data: '{"peid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    //$("#ddlLocationEmployeeMaster").empty();
                    //$("#ddlLocationEmployeeMaster").append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#ddlLocationEmployeeMaster").append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#ddlLocationEmployeeMaster").select2();

                    $("#ddlLocationEmployeeMaster").empty();
                    $("#ddlLocationEmployeeMaster").val("");
                    $("#ddlLocationEmployeeMaster").append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#ddlLocationEmployeeMaster").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlLocationEmployeeMaster").select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindLocationListEmployeeMaster(stateid) {

            var id = $("#ddlClient").val();
            stateid = $("#ddlPrincipleEmployerStateMaster").val();

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindClientLocationList",
                //data: JSON.stringify({ StateObj }),
                data: '{"clientid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var controlId = "ddlPrincipleEmployerLocationMaster";

                    //var Location = JSON.parse(data.d);
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindLocationListContractorMaster(stateid) {
            var id = $("#ddlPrincipleemployerContractor").val();
            var stateid = $("#ddlStateContractor").val();

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPELocationList",
                data: '{"peid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var controlId = "ddlLocationContractor";
                    //var Location = JSON.parse(data.d);
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();


                },
                failure: function (data) {
                    alert(data);
                }
            });
        }



        function BindMigrateBranchListEmployeeMaster() {
            var BranchObj = {
                Code: $("#ddlStateEmployeeMaster").val(),
                PEID: $("#ddlPrincipleEmployerMaster").val(),
                LocationID: $("#ddlLocationEmployeeMaster").val(),
                ClientID: $("#ddlClient").val()
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindBranchListContractor",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    //$("#ddlBranchEmployeeMaster").empty();
                    //$("#ddlBranchEmployeeMaster").append($("<option></option>").val("-1").html("Select Branch"));
                    //$.each(Location, function (data, value) {
                    //    $("#ddlBranchEmployeeMaster").append($("<option></option>").val(value.ID).html(value.Name));
                    //})
                    //$("#ddlBranchEmployeeMaster").select2();

                    $("#ddlBranchEmployeeMaster").empty();
                    $("#ddlBranchEmployeeMaster").val("");
                    $("#ddlBranchEmployeeMaster").append($("<option></option>").val("-1").html("Select Branch"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#ddlBranchEmployeeMaster").append($("<option></option>").val(value.ID).html(value.Name));
                        })

                    }
                    $("#ddlBranchEmployeeMaster").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindBranchListEmployeeMaster(ForContractor) {
            var BranchObj = {
                Code: $("#ddlPrincipleEmployerStateMaster").val(),
                PEID: $("#ddlPrincipleEmployerMaster").val(),
                LocationID: $("#ddlPrincipleEmployerLocationMaster").val(),
                ClientID: $("#ddlClient").val()
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindBranchList",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    debugger;

                    var controlId = "ddlPrincipleEmployerBranchMaster";
                    if (ForContractor == "Y")
                        controlId = "ddlBranchContractor";


                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }


        function BindBranchListContractor() {
            var BranchObj = {
                Code: $("#ddlStateContractor").val(),
                PEID: $("#ddlPrincipleemployerContractor").val(),
                LocationID: $("#ddlLocationContractor").val(),
                ClientID: $("#ddlClient").val()
            }

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindBranchListContractor",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    debugger;

                    var controlId = "ddlBranchContractor";

                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        /*function BindPrincipleEmployerTable(clntid) {
            var DetailsObj = {
                ClientID: clntid
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPrincipleEmployerTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var AssignCheckList = JSON.parse(data.d);

                    $("#tblPrincipleEmployer").kendoGrid({
                       
                        dataSource: {
                            data: AssignCheckList,
                        serverPaging: false,
                        pageSize: 10,
                        batch: true,
                        pageSize: 10,
                        //schema: {
                        //    data: function (response) {                            
                        //        if (response != null && response != undefined)                                
                        //            return response.Result;
                        //    },
                        //    total: function (response) {                            
                        //        if (response != null && response != undefined)
                        //            if (response.Result != null && response.Result != undefined)
                        //                return response.Result.length;
                        //    }
                        //}
                    },
                        //height: 300,
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        reorderable: true,
                        resizable: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        pageable: {
                            refresh: false,
                            pageSize: 10,
                            pageSizes: true,
                            buttonCount: 3,
                        },
                        dataBinding: function () {
                            record = 0;
                            var total = this.dataSource._pristineTotal;
                            if (this.dataSource.pageSize() == undefined) {
                                this.dataSource.pageSize(total);
                                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                            }
                            else {
                                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                            }
                        },
                        columns: [
                            { hidden: true, field: "ClientID" },
                            { hidden: true, field: "NoOfEmployees" },
                            { hidden: true, field: "Contractvalue" },
                            { hidden: true, field: "SecurityDeposit" },
                            { hidden: true, field: "ID" },
                            { field: "PEName", title: "PE Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "NatureOfBusiness", title: "Nature Of Business", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "ContractFrom", title: "Contract From", width: "20%" },
                            { field: "ContractTo", title: "Contract To", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Address", title: "Address", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "IsCentralAct", title: "Central", width: "15%", attributes: { style: 'white-space: nowrap;' } },
                             {
                                 title: "Action", lock: true, width: "10%;",
                                 command:
                                     [
                                            { name: "UpdatePrincipleEmployer", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                            { name: "DeletePrincipleEmployer", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                            //{ name: "UpdatePrincipleEmployer", text: "Update", className: "Update" },
                                     ]
                             }
                        ],

                        dataBound: function () {
                        var rows = this.items();
                        $(rows).each(function () {
                            var index = $(this).index() + 1
                                + ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.page() - 1));;
                            var rowLabel = $(this).find(".row-number");
                            $(rowLabel).html(index);
                        });
                    }
                    });

                    $("#tblContractor").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }*/
        function BindPrincipleEmployerTable(clntid) {
            var DetailsObj = {
                ClientID: clntid
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPrincipleEmployerTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var AssignCheckList = JSON.parse(data.d);

                    $("#tblPrincipleEmployer").kendoGrid({

                        dataSource: {
                            data: AssignCheckList,
                            serverPaging: false,
                            pageSize: 10,

                            batch: true,
                            pageSize: 10,
                            //schema: {
                            //    data: function (response) {                            
                            //        if (response != null && response != undefined)                                
                            //            return response.Result;
                            //    },
                            //    total: function (response) {                            
                            //        if (response != null && response != undefined)
                            //            if (response.Result != null && response.Result != undefined)
                            //                return response.Result.length;
                            //    }
                            //}
                        },

                        sortable: true,
                        filterable: true,
                        columnMenu: true,
                        pageable: {
                            refresh: false,
                            pageSize: 10,
                            pageSizes: true,
                            buttonCount: 3,
                        },
                        dataBinding: function () {
                            record = 0;
                            var total = this.dataSource._pristineTotal;
                            if (this.dataSource.pageSize() == undefined) {
                                this.dataSource.pageSize(total);
                                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                            }
                            else {
                                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                            }
                        },
                        reorderable: true,
                        resizable: true,
                        multi: true,
                        selectable: true,
                        columns: [

                                { hidden: true, field: "ClientID", menu: false },
                                { hidden: true, field: "NoOfEmployees", menu: false },
                                { hidden: true, field: "Contractvalue", menu: false },
                                { hidden: true, field: "SecurityDeposit", menu: false },
                                { hidden: true, field: "ID", menu: false },

                            {
                                field: "PEName", title: 'PE Name',
                                width: "15%;",
                                attributes: {

                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    extra: false,
                                    multi: true,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                            {
                                field: "NatureOfBusiness", title: 'Nature Of Business',
                                width: "18%",
                                attributes: {

                                    style: 'white-space: nowrap;'
                                },
                                filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                            //{
                            //    field: "ContractFrom", title: 'Contract From',
                            //    width: "15%",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'
                            //    },
                            //    filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                            //{
                            //    field: "ContractTo", title: 'Contract To',
                            //    width: "13%",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'
                            //    },
                            //    filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                            //{
                            //    field: "Address",
                            //    title: 'Address',
                            //    width: "11%",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'
                            //    },
                            //    filterable: {

                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                            //{
                            //    field: "IsCentralAct",
                            //    title: 'Is CentralAct',
                            //    width: "14%",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'

                            //    },
                            //    filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},

                            {
                                command:
                                    [
                                        { name: "UpdatePrincipleEmployer", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                        { name: "DeletePrincipleEmployer", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                    ], title: "Action", lock: true, width: "7%;",
                                attributes: {

                                    style: 'white-space: nowrapx;text-align:center;'
                                },
                            }
                        ],
                        dataBound: function () {
                            var rows = this.items();
                            $(rows).each(function () {
                                var index = $(this).index() + 1
                                    + ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.page() - 1));;
                                var rowLabel = $(this).find(".row-number");
                                $(rowLabel).html(index);
                            });
                        }
                    });
                    /* $("#tblPrincipleEmployer").kendoGrid({
 
                         dataSource: {
                             data: AssignCheckList,
                             serverPaging: false,
 
                             batch: true,
                             pageSize: 10,
 
                         },
                         //height: 300,
                         sortable: true,
                         filterable: true,
                         columnMenu: true,
                         pageable: {
                             refresh: true,
                             buttonCount: 3,
                             change: function (e) {
                             }
                         },
                         reorderable: true,
                         resizable: true,
                         multi: true,
                         selectable: true,
                         noRecords: {
                             template: function (e) {
                                 return "No data available";
                             }
                         },
 
 
                         columns: [
                             { hidden: true, field: "ClientID" },
                             { hidden: true, field: "NoOfEmployees" },
                             { hidden: true, field: "Contractvalue" },
                             { hidden: true, field: "SecurityDeposit" },
                             { hidden: true, field: "ID" },
                             { field: "PEName", title: "PE Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "NatureOfBusiness", title: "Nature Of Business", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "ContractFrom", title: "Contract From", width: "20%" },
                             { field: "ContractTo", title: "Contract To", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "Address", title: "Address", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "IsCentralAct", title: "Central", width: "15%", attributes: { style: 'white-space: nowrap;' } },
                              {
                                  title: "Action", lock: true, width: "10%;",
                                  command:
                                      [
                                             { name: "UpdatePrincipleEmployer", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                             { name: "DeletePrincipleEmployer", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
 
                                      ]
                              }
                         ],
 
                         dataBound: function () {
                             var rows = this.items();
                             $(rows).each(function () {
                                 var index = $(this).index() + 1
                                     + ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.page() - 1));;
                                 var rowLabel = $(this).find(".row-number");
                                 $(rowLabel).html(index);
                             });
 
                             $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                             $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                             $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                         }
 
 
 
                     });*/

                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }


        /*function BindPrincipleEmployeeReport()
        {
            var cid = $("#ddlClientReport").val();
            var pid = $("#ddlPrincipleEmployerReport").val();
            
            if (cid != "" && cid != "-1" && cid != null && pid != "" && pid != "-1" && pid != null)
            {

                $.ajax({
                    type: "POST",
                    url: "/CLRA/PrincipleEmployerCreation.aspx/BindPrincipleEmployeeReport",
                    data: '{"clientid":"' + cid + '","principleEmpId":"' + pid + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var AssignCheckList = JSON.parse(data.d);

                        $("#tblPrincipleEmployeeReport").kendoGrid({

                            dataSource: {
                                data: AssignCheckList,
                                serverPaging: false,
                                pageSize: 10,
                                batch: true,
                                pageSize: 10,

                            },
                            //height: 300,
                            sortable: true,
                            filterable: false,
                            columnMenu: true,
                            reorderable: true,
                            resizable: true,
                            noRecords: {
                                template: function (e) {
                                    return "No data available";
                                }
                            },
                            pageable: {
                                refresh: false,
                                pageSize: 10,
                                pageSizes: true,
                                buttonCount: 3,
                            },
                            dataBinding: function () {
                                record = 0;
                                var total = this.dataSource._pristineTotal;
                                if (this.dataSource.pageSize() == undefined) {
                                    this.dataSource.pageSize(total);
                                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                                }
                                else {
                                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                                }
                            },
                            columns: [
                               
                                { field: "PEID", title: "Principle Employer", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "EmpID", title: "Emp ID", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "EmpName", title: "Emp Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "ClientID", title: "Client ID", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "State", title: "State", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "Location", title: "Location", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "Branch", title: "Branch", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "DateOfJoining", title: "Date of Joining", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "DateOfLeaving", title: "Date of Leaving", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "EndDate", title: "End Date", width: "25%", attributes: { style: 'white-space: nowrap;' } }
                                
                                 
                            ],

                            dataBound: function () {
                                var rows = this.items();
                                $(rows).each(function () {
                                    var index = $(this).index() + 1
                                        + ($("#tblPrincipleEmployeeReport").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployeeReport").data("kendoGrid").dataSource.page() - 1));;
                                    var rowLabel = $(this).find(".row-number");
                                    $(rowLabel).html(index);
                                });
                            }
                        });

                        $("#tblPrincipleEmployeeReport").kendoTooltip({
                            filter: ".k-ScheduleAudit",
                            content: function (e) {
                                return "";
                            }
                        });


                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }*/



        function BindPrincipleEmployeeReport() {
            var cid = $("#ddlClientReport").val();
            var pid = $("#ddlPrincipleEmployerReport").val();

            if (cid != "" && cid != "-1" && cid != null && pid != "" && pid != "-1" && pid != null) {

                $.ajax({
                    type: "POST",
                    url: "/CLRA/PrincipleEmployerCreation.aspx/BindPrincipleEmployeeReport",
                    data: '{"clientid":"' + cid + '","principleEmpId":"' + pid + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var AssignCheckList = JSON.parse(data.d);

                        $("#tblPrincipleEmployeeReport").kendoGrid({

                            dataSource: {
                                data: AssignCheckList,
                                serverPaging: false,
                                batch: true,
                                pageSize: 10,

                            },
                            //height: 300,
                            sortable: true,
                            filterable: false,
                            columnMenu: true,
                            pageable: {
                                refresh: false,
                                buttonCount: 3,
                                change: function (e) {
                                }
                            },
                            reorderable: true,
                            resizable: true,
                            noRecords: {
                                template: function (e) {
                                    return "No data available";
                                }
                            },

                            columns: [

                                { field: "PEID", title: "Principle Employer", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "EmpID", title: "Emp ID", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { hidden: true,field: "EmpName", title: "Emp Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { hidden: true,field: "ClientID", title: "Client ID", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "State", title: "State", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { hidden: true,field: "Location", title: "Location", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "Branch", title: "Branch", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "DateOfJoining", title: "Date of Joining", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "DateOfLeaving", title: "Date of Leaving", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                                { hidden: true,field: "EndDate", title: "End Date", width: "25%", attributes: { style: 'white-space: nowrap;' } }


                            ],

                            dataBound: function () {
                                var rows = this.items();
                                $(rows).each(function () {
                                    var index = $(this).index() + 1
                                        + ($("#tblPrincipleEmployeeReport").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployeeReport").data("kendoGrid").dataSource.page() - 1));;
                                    var rowLabel = $(this).find(".row-number");
                                    $(rowLabel).html(index);
                                });

                                $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                                $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                                $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                            }
                        });

                        $("#tblPrincipleEmployeeReport").kendoTooltip({
                            filter: ".k-ScheduleAudit",
                            content: function (e) {
                                return "";
                            }
                        });


                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

















        /*function BindPrincipleEmployerLocationTable() {
            debugger;
            var DetailsObj = {
                PEID: $("#ddlPrincipleEmployer").val(),
                State: $("#ddlPrincipleEmployerState").val(),
                Location: $("#ddlPrincipleEmployerLocation").val()

            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPrincipleEmployerLocationTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var tblPrincipleEmployerLocationList = JSON.parse(data.d);

                    $("#tblPrincipleEmployerLocation").kendoGrid({
                        dataSource: {
                            data: tblPrincipleEmployerLocationList
                        },
                        height: 300,
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        reorderable: true,
                        resizable: true,
                        pageSize: 10,
                        pageable: {
                            refresh: false,
                            pageSize: 10,
                            pageSizes: true,
                            buttonCount: 3,
                        },
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        
                        columns: [
                            { hidden: true, field: "PLID" },
                            { field: "State", title: "State", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Location", title: "Location", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Branch", title: "Branch", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "NatureOfBusiness", title: "Nature Of Business", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Mines", title: "Mines Applicability", width: "20%" },
                            {
                                title: "Action", lock: true, width: "30%;",
                                command: [
                                    { name: "UpdatePrincipleEmployerLocation", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                    { name: "DeletePrincipleEmployerLocation", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                ]
                            }
                        ]
                    });
                    $("#tblPrincipleEmployerLocation").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }*/

        function BindPrincipleEmployerLocationTable() {
            debugger;
            var DetailsObj = {
                PEID: $("#ddlPrincipleEmployer").val(),
                State: $("#ddlPrincipleEmployerState").val(),
                Location: $("#ddlPrincipleEmployerLocation").val()

            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindPrincipleEmployerLocationTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var tblPrincipleEmployerLocationList = JSON.parse(data.d);

                    $("#tblPrincipleEmployerLocation").kendoGrid({
                        dataSource:
                            {
                                data: tblPrincipleEmployerLocationList,
                                serverPaging: false,

                                batch: true,
                                pageSize: 10,
                            },
                        //height: 300,
                        sortable: true,
                        filterable: true,
                        columnMenu: true,
                        pageable: {
                            refresh: true,
                            buttonCount: 3,
                            change: function (e) {
                            }
                        },
                        reorderable: true,
                        resizable: true,
                        multi: true,
                        selectable: true,

                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },

                        columns: [
                            { hidden: true, field: "PLID" },
                            { field: "State", title: "State", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Location", title: "Location", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Branch", title: "Branch", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            //{ field: "NatureOfBusiness", title: "Nature Of Business", width: "30%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Mines", title: "Mines Applicability", width: "30%" },
                            {
                                title: "Action", lock: true, width: "10%;",
                                command: [
                                    { name: "UpdatePrincipleEmployerLocation", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                    { name: "DeletePrincipleEmployerLocation", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                ]
                            }
                        ],

                        dataBound: function () {
                            var rows = this.items();
                            $(rows).each(function () {
                                var index = $(this).index() + 1
                                    + ($("#tblPrincipleEmployerLocation").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployerLocation").data("kendoGrid").dataSource.page() - 1));;
                                var rowLabel = $(this).find(".row-number");
                                $(rowLabel).html(index);
                            });

                            $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                            $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                            $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                        }

                    });

                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function BindEmployeeMasterTable() {
            debugger;
            var DetailsObj = {
                ClientID: $("#ddlClient").val(),
                PEID: $("#ddlPrincipleEmployerMaster").val(),
                State: $("#ddlPrincipleEmployerStateMaster").val(),
                Location: $("#ddlPrincipleEmployerLocationMaster").val(),
                Branch: $("#ddlPrincipleEmployerBranchMaster").val(),
            }


            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindEmployeeMasterTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var tblPrincipleEmployerLocationList = JSON.parse(data.d);

                    $("#tblEmployeeMaster").kendoGrid({
                        dataSource: {
                            data: tblPrincipleEmployerLocationList,
                            serverPaging: false,
                            batch: true,
                            pageSize: 10,
                        },
                        //height: 250,
                        //scrollable: true,
                        //width: 'auto',
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        pageable: {
                            refresh: true,
                            buttonCount: 3,
                            change: function (e) {
                            }
                        },
                        reorderable: true,
                        resizable: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        change: OnChangeEmployeeMaster,
                        columns: [
                            { hidden: true, field: "PEID" },
                            { hidden: true, field: "EMP_PELID" },
                            { selectable: true, width: "5%"},
                            { field: "EmpID", title: "Emp Id", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "EmpName", title: "Emp Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "State", title: "State", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            //{ field: "Location", title: "Location", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            //{ field: "Branch", title: "Branch", width: "20%" },
                            { field: "TaggedBranch", title: "Tagged Branch", width: "20%" },
                            //{
                            //    field: "ContractFrom", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract From", width: "20%"
                            //},
                            //{ field: "ContractTo", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract To", width: "20%" },                 
                            //{ field: "ContractEndDate", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract End Date", width: "20%" },
                            //{ field: "ReasonForContractEnd", title: "Contract End Reason", width: "20%" },
                            {
                                title: "Action", lock: true, width: "30%;",
                                command:
                                [
                                    { name: "UpdateEmployeeMaster", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                    { name: "DeleteEmployeeMaster", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                ]
                            }
                        ],
                        dataBound: onDataBoundTblEmployeeMaster
                        //dataBound: function ()
                        //{
                        //    onDataBoundTblEmployeeMaster();
                        //    var rows = this.items();
                        //    $(rows).each(function () {
                        //        var index = $(this).index() + 1
                        //            + ($("#tblEmployeeMaster").data("kendoGrid").dataSource.pageSize() * ($("#tblEmployeeMaster").data("kendoGrid").dataSource.page() - 1));;
                        //        var rowLabel = $(this).find(".row-number");
                        //        $(rowLabel).html(index);
                        //    });

                        //    $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                        //    $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                        //    $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                        //}
                    });
                    $("#tblEmployeeMaster").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }



        /*function BindEmployeeMasterTable() {
            debugger;
            var DetailsObj = {
                ClientID: $("#ddlClient").val(),
                PEID: $("#ddlPrincipleEmployerMaster").val(),
                State: $("#ddlPrincipleEmployerStateMaster").val(),
                Location: $("#ddlPrincipleEmployerLocationMaster").val(),
                Branch: $("#ddlPrincipleEmployerBranchMaster").val(),
            }

           
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindEmployeeMasterTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var tblPrincipleEmployerLocationList = JSON.parse(data.d);

                    $("#tblEmployeeMaster").kendoGrid({
                        dataSource: {
                            data: tblPrincipleEmployerLocationList,
                            serverPaging: false,
                            batch: true,
                            pageSize: 4,
                        },
                        //height: 250,
                        //scrollable: true,
                        //width: 'auto',
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        pageable: {
                            refresh: true,
                            buttonCount: 3,
                            change: function (e) {
                            }
                        },
                        reorderable: true,
                        resizable: true,
                        multi: true,
                        selectable: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        change: OnChangeEmployeeMaster,
                        columns: [
                            { hidden: true, field: "PEID" },
                            { hidden: true, field: "EMP_PELID" },
                            { selectable: true, width: "5%" },
                            { field: "EmpID", title: "Emp Id", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "EmpName", title: "Emp Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "State", title: "State", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            //{ field: "Location", title: "Location", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            //{ field: "Branch", title: "Branch", width: "20%" },
                            { field: "TaggedBranch", title: "Tagged Branch", width: "20%" },
                            //{
                            //    field: "ContractFrom", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract From", width: "20%"
                            //},
                            //{ field: "ContractTo", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract To", width: "20%" },                 
                            //{ field: "ContractEndDate", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract End Date", width: "20%" },
                            //{ field: "ReasonForContractEnd", title: "Contract End Reason", width: "20%" },
                            {
                                title: "Action", lock: true, width: "30%;",
                                command:
                                [
                                    { name: "UpdateEmployeeMaster", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                    { name: "DeleteEmployeeMaster", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                ]
                            }
                        ],
                        dataBound: onDataBoundTblEmployeeMaster
                    });

                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }*/

        function OnChangeEmployeeMaster(arg) {
            // alert("hi");
            var rows = arg.sender.select();
            EmployeeMasterListData = [];
            rows.each(function (e) {
                var tblEmployeeRecord = $("#tblEmployeeMaster").data("kendoGrid");
                var dataItem = tblEmployeeRecord.dataItem(this);

                var SelecteddataItem = {
                    EmpID: dataItem.EmpID,

                };
                debugger;
                EmployeeMasterListData.push(SelecteddataItem);
            })
        }

        function onDataBoundTblEmployeeMaster(e) {
            var data = this.dataSource.view();
            for (var i = 0; i < data.length; i++) {
                var uid = data[i].uid;
                var row = this.table.find("tr[data-uid='" + uid + "']");

                if (data[i].TaggedBranch == null || data[i].TaggedBranch == "") {
                    row.find(".Update").hide();
                    row.find(".Delete").hide();
                }
            }

            //var rows = this.items();
            //$(rows).each(function () {
            //    var index = $(this).index() + 1
            //        + ($("#tblEmployeeMaster").data("kendoGrid").dataSource.pageSize() * ($("#tblEmployeeMaster").data("kendoGrid").dataSource.page() - 1));;
            //    var rowLabel = $(this).find(".row-number");
            //    $(rowLabel).html(index);
            //});

            //$(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
            //$(".k-grid-edit").find("span").addClass("k-icon k-edit");
            //$(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
        }

        function BindContractorTable() {
            debugger;
            var DetailsObj = {
                ClientID: $("#ddlClient").val(),
                PEID: $("#ddlPrincipleemployerContractor").val(),
                State: $("#ddlStateContractor").val(),
                Location: $("#ddlLocationContractor").val(),
                Branch: $("#ddlBranchContractor").val(),
            }


            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/BindContractorTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var tblPrincipleEmployerLocationList = JSON.parse(data.d);

                    $("#tblContractorMaster").kendoGrid({
                        dataSource: {
                            data: tblPrincipleEmployerLocationList,
                            serverPaging: false,
                            batch: true,
                            pageSize: 10,
                        },
                        // height: 250,
                        //scrollable: true,
                        //width: 'auto',
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        pageable: {
                            refresh: true,
                            buttonCount: 3,
                            change: function (e) {
                            }
                        },
                        reorderable: true,
                        resizable: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        change: OnChangeContractorTable,
                        columns: [
                            { hidden: true, field: "PECID" },
                            //{ selectable: true, width: "5%" },                            
                            { field: "ContractorName", title: "Contractor Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { hidden: true, field: "Address", title: "Address", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "ContractFrom", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract From", width: "18%" }, //format: "{0:dd-MMM-yyyy}"
                            { field: "ContractTo", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract To", width: "18%" },
                            { field: "NatureOfWork", title: "Nature Of Work", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "NumberOfEmployees", title: "Number Of Employees", width: "26%" },
                            { hidden: true, field: "CanteenProvided", title: "Canteen", width: "20%" },
                            { hidden: true, field: "RestroomProvided", title: "Restroom", width: "20%" },
                            { hidden: true, field: "CrechesProvided", title: "Creches", width: "20%" },
                            { hidden: true, field: "DrinkingWaterProvided", title: "Drinking Water", width: "20%" },
                            { hidden: true, field: "FirstAidProvided", title: "FirstAid", width: "20%" },
                            {
                                title: "Action", lock: true, width: "8%;",
                                command:
                                    [
                                           { name: "UpdateContractor", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                           { name: "DeleteContractor", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                    ]
                            }
                        ],
                        dataBound: function () {
                            var rows = this.items();
                            $(rows).each(function () {
                                var index = $(this).index() + 1
                                    + ($("#tblContractorMaster").data("kendoGrid").dataSource.pageSize() * ($("#tblContractorMaster").data("kendoGrid").dataSource.page() - 1));;
                                var rowLabel = $(this).find(".row-number");
                                $(rowLabel).html(index);
                            });

                            $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                            $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                            $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                        }

                    });
                    $("#tblContractorMaster").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function OnChangeContractorTable(arg) {

            var rows = arg.sender.select();
            EmployeeMasterListData = [];
            rows.each(function (e) {
                var tblEmployeeRecord = $("#tblContractorMaster").data("kendoGrid");
                var dataItem = tblEmployeeRecord.dataItem(this);
                var SelecteddataItem = {
                    EmpID: dataItem.EmpID,
                    State: $("#ddlStateEmployeeMaster").val(),
                    Location: $("#ddlLocationEmployeeMaster").val(),
                    Branch: $("#ddlBranchEmployeeMaster").val(),
                    ContractFrom: $("#txtEmployeeContractForm").val(),
                    ContractTo: $("#txtEmployeeContractTo").val(),
                };
                debugger;
                EmployeeMasterListData.push(SelecteddataItem);
            })


        }


        function ValidatePrincipleEmployer() {
            //var fromDate = $("#txtContractForm").val();
            //fromDate = new Date(fromDate);

            //var toDate = $("#txtContractTo").val();
            //toDate = new Date(toDate);





            var status = true;
            $("#divErrorsPE").html("");
            $("#divErrorsPE").addClass('alert-danger');

            if ($("#txtControctalue").val() == "")
                $("#txtControctalue").val("0");

            if ($("#txtSecurityDeposit").val() == "")
                $("#txtSecurityDeposit").val("0");


            if ($("#txtPEName").val() == "") {
                displayErrors("divErrorsPE", "Please Enter Principle Employer Name");
                status = false;

            }
            if ($("#txtNatureOfBusiness").val() == "") {
                displayErrors("divErrorsPE", "Please Enter Nature Of Business");
                status = false;
            }
            //if ($("#txtContractForm").val() == "") {
            //    displayErrors("divErrorsPE", "Please Select Contract From");
            //    status = false;
            //}
            //if ($("#txtContractTo").val() == "") {
            //    displayErrors("divErrorsPE", "Please Select Contract To");
            //    status = false;
            //}
            //if ($("#txtNumberOfEmployees").val() == "") {
            //    displayErrors("divErrorsPE", "Please Enter Number Of Employee");
            //    status = false;
            //}
            //if ($("#txtPEPAN").val() == "") {
            //    displayErrors("divErrorsPE", "Please Enter PE PAN");
            //    status = false;
            //}
            //if ($("#txtPEPAN").val() != "") {
            //    var val = $("#txtPEPAN").val()
            //    if (/[A-Z]{5}[0-9]{4}[A-Z]{1}$/.test(val)) {
            //    } else {
            //        displayErrors("divErrorsPE", "Please Enter valid PE PAN");
            //        status = false;
            //    }
            //}
            //if ($("#txtAddress").val() == "") {
            //    displayErrors("divErrorsPE", "Please Enter Address");
            //    status = false;
            //}
            return status;
        }


        function displayErrors(control, msg) {
            $("#" + control).append(msg);
            $("#" + control).append("</br>");
            $("#" + control).show();
        }

        function ValidatePrincipleEmployerContractor() {
            var status = true;
            $("#divErrorsContractor").html("");
            $("#divErrorsContractor").addClass('alert-danger');

            var fromDate = $("#txtContractorContractForm").val();
            var toDate = $("#txtContractorContractTo").val();


            if ($("#txtContractorName").val() == "") {
                displayErrors("divErrorsContractor", "Please Enter Contractor Name");
                status = false;
            }
            if ($("#txtContractorAddres").val() == "") {
                displayErrors("divErrorsContractor", "Please Enter Address");
                status = false;
            }
            if ($("#txtContractorContractForm").val() == "") {
                displayErrors("divErrorsContractor", "Please Select Contract From");
                status = false;
            }
            if ($("#txtContractorContractTo").val() == "") {
                displayErrors("divErrorsContractor", "Please Select Contract To");
                status = false;
            }
            if ($("#txtContractorNatureOfWork").val() == "") {
                displayErrors("divErrorsContractor", "Please Enter Nature Of Work");
                status = false;
            }
            //else if (Date.parse(toDate) < Date.parse(fromDate))
            //{
            //    alert("'Contract To Date' Should Be Greater than 'Contract From Date'");
            //    status = false;
            //}

            if ($("#txtContractorNoOfEmployee").val() == "") {
                displayErrors("divErrorsContractor", "Please Enter Number Of Employee");
                status = false;
            }

            return status;
        }

        function ValidatePrincipleEmployerLocation() {
            var status = true;
            $("#divErrorsPELocation").html("");
            $("#divErrorsPELocation").addClass('alert-danger');

            if ($("#txtBranch").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter Branch");
                status = false;
            }
            if ($("#txtBranch_Address").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter Branch Address");
                status = false;
            }
            if ($("#txtNumber_Employee").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter Number of Employees");
                status = false;
            }
            if ($("#txtContractForm").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter Start Contract Date");
                status = false;
            }
            if ($("#txtContractTo").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter End Contract Date");
                status = false;
            }
            if ($("#ddlWeekoff").val() == "") {
                displayErrors("divErrorsPELocation", "Please select weekOff");
                status = false;
            }
            
            
            //if ($("#txtbarchNatureOfBusiness").val() == "") {
            //    displayErrors("divErrorsPELocation", "Please Enter Nature Of Business");
            //    status = false;
            //}

            if ($("#txtPE_AuthorisedPerson_EmailID").val() != "") {
                var val = $("#txtPE_AuthorisedPerson_EmailID").val()
                if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(val)) {
                } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid PE Authorised Person Email ID");
                    status = false;
                }
            }
           
            if ($("#txtPE_Company_PhoneNo").val() != "") {
                var val = $("#txtPE_Company_PhoneNo").val()
                if (/^[789]\d{9}$/.test(val)) {
                } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid PE Company Phone No");
                    status = false;
                }
            }
           
            if ($("#txtClient_CompanyEmailID").val() != "") {
                var val = $("#txtClient_CompanyEmailID").val()
                if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(val)) {
                 } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid Client Company Email ID");
                    status = false;
                }
            }
            

            if ($("#txtClient_Company_Phone_No").val() !="")
            {
             var val = $("#txtClient_Company_Phone_No").val()
             if (/^[789]\d{9}$/.test(val)) {
                 } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid Client Company Phone No");
                    status = false;
                }
            }

          
            if ($("#txtContractor_Person_Incharge_PAN").val() != "") {
                var val = $("#txtContractor_Person_Incharge_PAN").val()
                if (/[A-Z]{5}[0-9]{4}[A-Z]{1}$/.test(val)) {
                } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid Contractor Person Incharge PAN");
                    status = false;
                }
            }
           
            if ($("#txtContractor_Person_Incharge_EmailID").val() != "") {
               var val = $("#txtContractor_Person_Incharge_EmailID").val()
                if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(val)) {
                } else {
                    displayErrors("divErrorsPELocation", "Please Entervalid Contractor Person Incharge Email ID");
                    status = false;
                }
            }

           
            if ($("#txtContractor_Person_Incharge_MobileNo").val() != "") {
                var val = $("#txtContractor_Person_Incharge_MobileNo").val()
                if (/^[789]\d{9}$/.test(val)) {
                 } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid Contractor Person Incharge Mobile No");
                    status = false;
                }
            }
            

            return status;
        }


        function ValidateEmployeeMigration() {
            var status = true;
            $("#divErrorsEmployees").html("");
            $("#divErrorsEmployees").addClass('alert-danger');


            var fromDate = $("#txtEmployeeContractForm").val();
            var processedFromDate = process(fromDate);

            var toDate = $("#txtEmployeeContractTo").val();
            var processedToDate = process(toDate);

            var endDate = $("#txtContractEndDate").val();
            var processedEndDate = process(endDate);

            var PEContractFrom = $("#txtPEContractFrom").val();
            var processedPEContractFrom = process(PEContractFrom);

            var PEContractTo = $("#txtPEContractTo").val();
            var processedPEContractTo = process(PEContractTo);


            if (($("#hdMode").val() == "A") && ($("#ddlStateEmployeeMaster").val() == "" || $("#ddlStateEmployeeMaster").val() == "-1" || $("#ddlStateEmployeeMaster").val() == null)) {
                displayErrors("divErrorsEmployees", "Please Select State");
                status = false;
            }
            if (($("#hdMode").val() == "A") && ($("#ddlLocationEmployeeMaster").val() == "" || $("#ddlLocationEmployeeMaster").val() == "-1" || $("#ddlLocationEmployeeMaster").val() == null)) {
                displayErrors("divErrorsEmployees", "Please Select Location");
                status = false;
            }
            if (($("#hdMode").val() == "A") && ($("#ddlBranchEmployeeMaster").val() == "" || $("#ddlBranchEmployeeMaster").val() == "-1" || $("#ddlBranchEmployeeMaster").val() == null)) {
                displayErrors("divErrorsEmployees", "Please Select Branch");
                status = false;
            }
            if ($("#txtEmployeeContractForm").val() == "") {
                displayErrors("divErrorsEmployees", "Please Select Contract From");
                status = false;
            }
            if ($("#txtEmployeeContractTo").val() == "") {
                displayErrors("divErrorsEmployees", "Please Select Contract To");
                status = false;
            }
            if (processedFromDate < processedPEContractFrom || processedFromDate > processedPEContractTo) {
                displayErrors("divErrorsEmployees", "'Contract From Date' Should be between Contract From Date and Contract To Date Of the selected Principle Employer");
                status = false;
            }
            if (processedToDate < processedPEContractFrom || processedToDate > processedPEContractTo) {
                displayErrors("divErrorsEmployees", "'Contract To Date' Should be between Contract From Date and Contract To Date Of the selected Principle Employer");
                status = false;
            }

            //else if (Date.parse(toDate) < Date.parse(fromDate))
            //{
            //    alert("'Contract To Date' Should Be Greater than 'Contract From Date'");
            //    return false;
            //}
            //else if (Date.parse(fromDate) < Date.parse(PEContractFrom) || Date.parse(fromDate) > Date.parse(PEContractTo))
            //{
            //    alert("'Contract From Date' Should be between Contract From Date and Contract To Date Of the selected Principle Employer");
            //    return false;
            //}
            //else if (Date.parse(toDate) < Date.parse(PEContractFrom) || Date.parse(toDate) > Date.parse(PEContractTo))
            //{
            //    alert("'Contract To Date' Should be between Contract From Date and Contract To Date Of the selected Principle Employer");
            //    return false;
            //}

            if (($("#hdMode").val() == "E") && ($("#txtContractEndDate").val() != "" && (processedEndDate < processedFromDate || processedEndDate > processedToDate))) {
                displayErrors("divErrorsEmployees", "'Contract End Date' Should Be Between Contract From Date and Contract To Date");
                status = false;
            }
            if (($("#hdMode").val() == "E") && ($("#txtContractEndDate").val() != "" && $("#txtContractEndReason").val() == "")) {
                displayErrors("divErrorsEmployees", "Please Enter Contract End Reason");
                status = false;
            }
            return status;
        }


        function process(date) {
            var parts = date.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }

        function downloadSample() {
            //alert("hi");
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerCreation.aspx/DownloadSampleFile",
                //data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    alert("success");
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function chkDecimal(controlId) {

            //var val = $("#txtControctalue").val();
            var val = $("#" + controlId).val();
            if (val != "") {
                //var checkForPercentage = /^\d{1,2}\.\d{1,2}$|^\d{1,3}$/; 
                /*var checkForPercentage = /[^0-9.]/;
                if (!checkForPercentage.test(val)) {
                    alert("Enter numeric values only.");
                    $("#" + controlId).val("");
                }*/

                var rx = /^\d+(?:\.\d{1,2})?$/
                if (!rx.test(val)) {
                    alert("Enter numeric values only.");
                    $("#" + controlId).val("");
                }

            }

        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }


    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#ddlWeekoff').fastselect();
            // fselect();

        });



        function fselect() {
            //alert("in fselect");
            $('.multipleSelect').fastselect();
            var model = "";// "Mon|Wed|Fri";

            var array = model.split("|");

            for (i in array) {
                debugger;
                $('.multipleSelect').data('fastselect').setSelectedOption($('.multipleSelect option[value=' + array[i] + ' ]').get(0));
            }
        }

        function clrWeekOffs() {
            //alert("in clrWeekOffs");
            $('.multipleSelect').fastselect();
            var model = "";// "Mon|Wed|Fri";

            var array = model.split("|");

            for (i in array) {
                debugger;
                $('.multipleSelect').data('fastselect').setSelectedOption($('.multipleSelect option[value=' + array[i] + ' ]').get(0));
            }
        }



    </script>

    <style>
        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }


        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 34px;
            user-select: none;
            -webkit-user-select: none;
        }
    </style>
</head>
<body>

    <!-- partial:index.partial.html -->

    <!--PEN CONTENT     -->
    <div class="contentx">
        <!--content inner-->
        <div class="content__inner">
            <div class="container">
            </div>
            <div class="containerx overflow-hidden">
                <!--multisteps-form-->
                <!--form panels-->
                <div class="row">
                    <div class="col-12 col-lg-12 m-auto">
                        <form class="multisteps-form__form">
                            <%--form--%>
                            <!--single form panel-->
                            <div id="divPECreation" class="tabdata" data-animation="scaleIn">
                                <div class="" style="margin-top: 25px;">
                                    <div class="row alert alert-danger" id="divCheckPE">
                                        <div class="col-md-12">
                                            <div>
                                                <strong></strong><span></span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <label style="color: black;">Client ID</label>
                                            <select class="form-control" id="ddlClient">
                                            </select>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group" style="margin-top: 20px;">
                                            <input class="k-textbox" type="text" id="txtSearch" style="width: 78%; height: 36px" placeholder="Type to search..." autocomplete="off">
                                            <button id="btnSearch" class="btn btn-primary" style="height: 36px; font-weight: 400;">Search</button>
                                        </div>

                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <%--<button class="btn btn-primary" type="button" id="btnNewPrincipleEmployer" title="Add New Principle Employer" style="margin-top: 20px;">Add New Principle Employer</button>--%>
                                            <button class="btn btn-primary" type="button" id="btnNewPrincipleEmployer" style="margin-top: 20px; height: 36px; font-weight: 400;"><span class="k-icon k-i-plus-outline"></span>Add New</button>
                                        </div>
                                    </div>

                                    <%-- <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                <div id="tblPrincipleEmployer" style="height:300px;"></div>
                                            </div>
                                        </div>--%>

                                    <div class="row colpadding0">
                                        <div class="col-md-12">
                                            <div id="tblPrincipleEmployer">
                                                <div class="k-header k-grid-toolbarx">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="button-row d-flex mt-4" style="margin-top: 20px;">
                                        <%-- <button class="btn btn-primary" type="button" title="Next" id="btnNext">-></button>--%>
                                        <button class="btn btn-primary fa fa-chevron-right" type="button" title="Next" id="btnNext" style="width: 5%; font-size: 12px"></button>
                                    </div>
                                </div>
                            </div>
                            <!--single form panel-->
                            <div id="divPELocationCreation" class="tabdata" data-animation="scaleIn">
                                <h3 class=""></h3>

                                <div class="row alert alert-danger" id="divCheckPELocation">
                                    <div class="col-md-12">
                                        <div>
                                            <strong></strong><span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <label style="color: black;">Principle Employer</label>
                                            <select class="form-control" id="ddlPrincipleEmployer">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divState">
                                            <label style="color: black;">State</label>
                                            <select class="form-control" id="ddlPrincipleEmployerState">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divLocation">
                                            <label style="color: black;">Location</label>
                                            <select class="form-control" id="ddlPrincipleEmployerLocation">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divNewLocation">
                                            <%--<button class="btn btn-primary" type="button" id="btnNewPELocation" title="Add New PE Location" style="margin-top: 20px;">Add New PE Location</button>--%>
                                            <button class="btn btn-primary" type="button" id="btnNewPELocation" style="margin-top: 20px; font-weight: 400;"><span class="k-icon k-i-plus-outline"></span>Add New</button>

                                            <button class="btn btn-primary" type="button" id="btnUploadLocation" title="Upload" style="margin-top: 20px"><span class="k-icon k-i-upload"></span></button>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <div id="tblPrincipleEmployerLocation"></div>
                                        </div>
                                    </div>

                                    <div class="button-row d-flex mt-4">
                                        <%--<button class="btn btn-primary" type="button" title="Prev" id="btnLocationPrev">Prev</button>--%>
                                        <button class="btn btn-primary fa fa-chevron-left" type="button" title="Prev" id="btnLocationPrev" style="width: 5%; font-size: 12px"></button>
                                        <%-- <button class="btn btn-primary" type="button" title="Next" id="btnLocationNext">Next</button>--%>
                                        <button class="btn btn-primary fa fa-chevron-right" type="button" title="Next" id="btnLocationNext" style="width: 5%; font-size: 12px"></button>
                                    </div>
                                </div>
                            </div>
                            <!--single form panel-->
                            <div id="divPEMaster" class="tabdata" data-animation="scaleIn">
                                <h3 class=""></h3>
                                <div class="row alert alert-danger" id="divCheckEmployees">
                                    <div class="col-md-12">
                                        <div>
                                            <strong></strong><span></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <label style="color: black;">Principle Employer</label>
                                            <select class="form-control" id="ddlPrincipleEmployerMaster">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divStateMaster">
                                            <label style="color: black;">State</label>
                                            <select class="form-control" id="ddlPrincipleEmployerStateMaster">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divLocationMaster">
                                            <label style="color: black;">Location</label>
                                            <select class="form-control" id="ddlPrincipleEmployerLocationMaster">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divBranchMaster">
                                            <label style="color: black;">Branch</label>
                                            <select class="form-control" id="ddlPrincipleEmployerBranchMaster">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divMigrateMaster">
                                            <button class="btn btn-primary" type="button" id="btnMigrate" title="Migrate">Migrate</button>

                                            <%--<button class="btn btn-primary" type="button" id="btnUploadEmployee" title="Upload">Upload</button>--%>
                                            <button class="btn btn-primary" type="button" id="btnUploadEmployee" title="Upload"><span class="k-icon k-i-upload"></span></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%--<div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                <div id="tblEmployeeMaster" class="table-responsive"></div>
                                            </div>--%>



                                        <div class="col-md-12">
                                            <div id="tblEmployeeMaster">
                                                <div class="k-header k-grid-toolbarx">
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="button-row d-flex mt-4 col-12" style="margin-top: 20px;">
                                        <%--<button class="btn btn-primary" type="button" id="btnEmpMasterPrev" title="Prev">Prev</button>--%>
                                        <button class="btn btn-primary fa fa-chevron-left" type="button" title="Next" id="btnEmpMasterPrev" style="width: 5%; font-size: 12px"></button>
                                        <%--<button class="btn btn-primary" type="button" id="btnEmpMasterNext" title="Next">Next</button>--%>
                                        <button class="btn btn-primary fa fa-chevron-right" type="button" title="Next" id="btnEmpMasterNext" style="width: 5%; font-size: 12px"></button>
                                    </div>

                                </div>
                            </div>
                            <!--single form panel-->
                            <div id="divContractorDetails" class="tabdata" data-animation="scaleIn">
                                <h3 class=""></h3>

                                <div class="row alert alert-danger" id="divCheckContractor">
                                    <div class="col-md-12">
                                        <div>
                                            <strong></strong><span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <label style="color: black;">Principle Employer</label>
                                            <select class="form-control" id="ddlPrincipleemployerContractor">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divStateContractor">
                                            <label style="color: black;">State</label>
                                            <select class="form-control" id="ddlStateContractor">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divLocationContractor">
                                            <label style="color: black;">Location</label>
                                            <select class="form-control" id="ddlLocationContractor">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divBranchContractor">
                                            <label style="color: black;">Branch</label>
                                            <select class="form-control" id="ddlBranchContractor">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-2 form-group hidden" id="divContractor">
                                            <%--<button class="btn btn-primary" type="button" id="btnAddContractor" title="Migrate">Add Contractor</button>--%>
                                            <button class="btn btn-primary" type="button" id="btnAddContractor" style="font-weight: 400;"><span class="k-icon k-i-plus-outline"></span>Add New</button>
                                            <%-- <button class="btn btn-primary" type="button" id="btnUploadContractors" title="Upload">Upload</button>--%>
                                            <button class="btn btn-primary" type="button" id="btnUploadContractors" title="Upload"><span class="k-icon k-i-upload"></span></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <div id="tblContractorMaster"></div>
                                        </div>
                                    </div>
                                    <div class="button-row d-flex mt-4">
                                        <%--<button class="btn btn-primary" type="button" title="Prev" id="btnContractorPrev">Prev</button>--%>
                                        <button class="btn btn-primary fa fa-chevron-left" type="button" title="Next" id="btnContractorPrev" style="width: 5%; font-size: 12px"></button>
                                        <%--<button class="btn btn-primary" type="button" title="Next" id="btnContractorNext">Next</button>--%>
                                        <button class="btn btn-primary fa fa-chevron-right" type="button" title="Next" id="btnContractorNext" style="width: 5%; font-size: 12px"></button>
                                    </div>
                                </div>
                            </div>

                            <!--single form panel-->
                            <div id="divPEReport" class="tabdata" data-animation="scaleIn">
                                <h3 class=""></h3>
                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <label style="color: black;">Client</label>
                                            <select class="form-control" id="ddlClientReport">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <label style="color: black;">Principle Employer</label>
                                            <select class="form-control" id="ddlPrincipleEmployerReport">
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <div id="tblPrincipleEmployeeReport"></div>
                                        </div>
                                    </div>

                                    <div class="button-row d-flex mt-4">
                                        <%--<button class="btn btn-primary" type="button" title="Prev" id="btnPEReportPrev" >Prev</button>--%>
                                        <button class="btn btn-primary fa fa-chevron-left" type="button" id="btnPEReportPrev" style="width: 5%; font-size: 12px"></button>

                                    </div>
                                </div>
                            </div>
                        </form>
                        <%--form ends--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <%-- </div>--%>  <%--13-10-2020--%>
    <!-- partial -->
    <script src="script.js"></script>

    <div class="modal  modal-fullscreen" id="ModalPrincipleEmployer" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" id="divValidation" style="height: 240px; width: 710px; margin-left: -650px; overflow-y: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Principle Employer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" id="divMsg">
                            <div class="col-md-12">
                                <div id="divMsgPanel" class="alert alert-success">
                                    <strong></strong><span id="spanMsg"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row alert" id="divErrorsPE">
                            <div class="col-md-12">
                                <div>
                                    <strong></strong><span id="spanErrorsPE"></span>
                                </div>
                            </div>
                        </div>

                        <%--<div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Name</label>
                                <span style="color: red;">*</span>
                                <input type="text" id="txtPEName" class="form-control" />
                                <label style="color: black;" id="lblPEID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Nature Of Business</label>
                                <span style="color: red;">*</span>
                                <input type="text" id="txtNatureOfBusiness" class="form-control" />
                            </div>
                        </div>--%>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Name <span style="color: red;">*</span></label>

                                <input type="text" id="txtPEName" class="form-control" />
                                <label style="color: black;" id="lblPEID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Nature Of Business <span style="color: red;">*</span></label>

                                <input type="text" id="txtNatureOfBusiness" class="form-control" />
                            </div>
                        </div>
                        <%--
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract From  <span style="color: red;">*</span></label>

                                <%--<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractForm" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>

                                <input type="text" class="form-control" id="txtContractForm" placeholder="From-Date" />


                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract To <span style="color: red;">*</span></label>

                                <%--<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractTo" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>

                                <input type="text" class="form-control" id="txtContractTo" placeholder="To-Date"/>


                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Number Of Employee  <span style="color: red;">*</span></label>

                                <input type="text" id="txtNumberOfEmployees" class="form-control" maxlength="7" onkeypress="return isNumber(event)" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">Address <span style="color: red;">*</span></label>

                                <input type="text" id="txtAddress" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract Value</label>
                                <%--<input type="text" id="txtControctalue" class="form-control" maxlength="10" onblur="chkDecimal('txtControctalue');" /> 
                                <input type="text" id="txtControctalue" class="form-control" maxlength="10" onkeypress="return isNumber(event)" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Security Deposit</label>
                                <%--<input type="text" id="txtSecurityDeposit" class="form-control" maxlength="10" onblur="chkDecimal('txtSecurityDeposit');" />
                                <input type="text" id="txtSecurityDeposit" class="form-control" maxlength="10" onkeypress="return isNumber(event)" />

                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="chkCentral" value="1" />IsCentralAct</label>
                                </div>
                            </div>
                             <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">PE PAN <span style="color: red;">*</span></label>
                                <input type="text" id="txtPEPAN" maxlength="10" class="form-control"  />

                            </div>
                            <%--<div id="divStatus" class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkStatus" value="A" />Active</label>
                                </div>
                            </div>
                        </div>--%>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <%--<div id="divPrincipleEmployer">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Name*</label>
                                <input type="text" id="txtPEName" class="form-control" />
                               
                                <label style="color: black;" id="lblPEID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Nature Of Business*</label>
                                <input type="text" id="txtNatureOfBusiness" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract From*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractForm" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract To*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractTo" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Number Of Employees*</label>
                                <input type="text" id="txtNumberOfEmployees" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">Address*</label>
                                <input type="text" id="txtAddress" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract Value</label>
                                <input type="text" id="txtControctalue" class="form-control" maxlength="10" />

                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Security Deposit</label>
                                <input type="text" id="txtSecurityDeposit" class="form-control" maxlength="10" />

                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="chkCentral" value="1" />IsCentralAct</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkStatus" value="A" />Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>--%>




    <div class="modal  modal-fullscreen" id="ModalPrincipleEmployerLocation" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="overflow: auto; min-height: 500px; max-height: 500px; width: 710px; margin-left: -650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Principle Employer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" id="divMsgPELocation">
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <strong></strong><span id="spanMsgPELocation"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row alert" id="divErrorsPELocation">
                            <div class="col-md-12">
                                <div>
                                    <strong></strong><span></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-6 form-group">
                                <label style="color: black;">Branch Address<span style="color: red;">*</span></label>
                                <input type="text" id="txtBranch_Address" class="form-control" />
                                
                            </div>
                            <%--<div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Nature Of Business <span style="color: red;">*</span></label>
                                <input type="text" id="txtbarchNatureOfBusiness" class="form-control" />
                            </div>--%>
                                
                        </div>

                          <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Branch <span style="color: red;">*</span></label>
                                <input type="text" id="txtBranch" class="form-control" />
                                <label style="color: black;" id="lblPELID" class="hidden">0</label>
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Number of Employees<span style="color: red;">*</span></label>
                                <input type="text" id="txtNumber_Employee" class="form-control" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract From<span style="color: red;">*</span></label>
                                <input type="text" id="txtContractForm" placeholder="From-Date" class="form-control" />
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract To<span style="color: red;">*</span></label>
                                <input type="text" id="txtContractTo" placeholder="To-Date" class="form-control" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">WeekOff<span style="color: red;">*</span></label>
                                <select multiple size="7" id="ddlWeekoff" class="multipleSelect">
                                    <option value="Sun">Sunday</option>
                                    <option value="Mon">Monday</option>
                                    <option value="Tue">Tuesday</option>
                                    <option value="Wed">Wednesday</option>
                                    <option value="Thu">Thursday</option>
                                    <option value="Fri">Friday</option>
                                    <option value="Sat">Saturday</option>
                                </select>

                                <%-- <select id="ddlWeekoff" multiple="multiple">
                                    <option value="cheese">Cheese</option>
                                    <option value="tomatoes">Tomatoes</option>
                                    <option value="mozarella">Mozzarella</option>
                                    <option value="mushrooms">Mushrooms</option>
                                    <option value="pepperoni">Pepperoni</option>
                                    <option value="onions">Onions</option>
                                </select>--%>
                            </div>
                            <%-- <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chklocationStatus" value="1" />Active</label>
                                </div>
                            </div>--%>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkMineApplicability" value="1" />Mines Applicabilty</label>
                                </div>
                            </div>
                           </div>
                              <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE LIN </label>
                                <input type="text" id="txtPE_LIN" class="form-control" />
                                <label style="color: black;" id="lblPE_LIN" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Authorised Person Email ID </label>
                                <input type="text" id="txtPE_AuthorisedPerson_EmailID" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Company Phone No </label>
                                <input type="text" id="txtPE_Company_PhoneNo" maxlength="10" class="form-control" />
                                <label style="color: black;" id="lblPE_Company_PhoneNo" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Client LIN No </label>
                                <input type="text" id="txtClient_LINNo" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Client Company Email ID </label>
                                <input type="text" id="txtClient_CompanyEmailID" class="form-control" />
                                <label style="color: black;" id="lblClient_CompanyEmailID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Client Company Phone No </label>
                                <input type="text" id="txtClient_Company_Phone_No" maxlength="10" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract Licence No </label>
                                <input type="text" id="txtContract_Licence_No" class="form-control" />
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Licence Valid From date </label>
                                <input type="text" id="txtLicenceValidFromdate" placeholder="From-Date" class="form-control"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Licence Valid To date </label>
                                <input type="text" id="txtLicenceValidTodate" placeholder="To-Date" class="form-control"/>
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contractor Person Incharge Name </label>
                                <input type="text" id="txtContractor_Person_Incharge_Name" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contractor Person Incharge LIN </label>
                                <input type="text" id="txtContractor_Person_Incharge_LIN" class="form-control" />
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contractor Person Incharge PAN </label>
                                <input type="text" id="txtContractor_Person_Incharge_PAN" maxlength="10" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contractor Person Incharge Email ID </label>
                                <input type="text" id="txtContractor_Person_Incharge_EmailID" class="form-control" />
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contractor Person Incharge Mobile No </label>
                                <input type="text" id="txtContractor_Person_Incharge_MobileNo" maxlength="10" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Client Nature of business </label>
                                <input type="text" id="txtClient_Nature_of_business" class="form-control" />
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Address </label>
                                <input type="text" id="txtPE_Address" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contractor Licensing Officer Designation </label>
                                <input type="text" id="txtContractor_Licensing_Officer_Designation" class="form-control" />
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Licencing officer Head Quarter </label>
                                <input type="text" id="txtLicencing_officer_Head_Quarter" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Nature of welfare amenities provided </label>
                                <input type="text" id="txtNature_ofwelfare_amenities_provided" class="form-control" />
                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Statutory statute</label>
                                <input type="text" id="txtStatutory_statute" class="form-control" />
                            </div>
                        </div>

                        </div>
                        <div class="row" style="margin-top: 50px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnLocationSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <%-- </div>--%>  <%--13-10-2020--%>

    <div class="modal  modal-fullscreen" id="UploadEmployeeModal" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="height: 400px; margin-left: -650px; width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Excel</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <form runat="server" id="formvalidation">
                            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
                            <asp:UpdatePanel ID="updatepnl" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                                            <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <asp:FileUpload ID="ContractFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <asp:Button runat="server" ID="btnUploadExcel" CssClass="btn btn-primary" Text="Upload" OnClick="btnUploadExcel_Click" ValidationGroup="uploadUtilityValidationGroup" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <%--<a href="SampleFormat/ContractBulkUpload.xlsx"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                            <%--<a id="btnDownloadSample" onclick="downloadSample()"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                            <asp:LinkButton ID="lnkDownload" class="fa fa-file-excel-o" Text="Download Sample Format" CommandArgument='<%# Eval("Value") %>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnUploadExcel" />
                                    <%--<asp:AsyncPostBackTrigger ControlID="btnUploadExcel" EventName = "Click" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal  modal-fullscreen" id="ModalEmployeeMigrate" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="height: 450px; width: 710px; margin-left: -650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Employee Migration to Principle Employer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" id="divMsgEmployees">
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <strong></strong><span id="spanMsgEmployees"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row alert" id="divErrorsEmployees">
                            <div class="col-md-12">
                                <div>
                                    <strong></strong><span></span>
                                </div>
                            </div>
                        </div>


                        <div id="divMigrateLocation" class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">State <span style="color: red;">*</span></label>
                                <select class="form-control" id="ddlStateEmployeeMaster">
                                </select>
                                <label style="color: black;" id="lblEmployeeID" class="hidden">0</label>
                                <label style="color: black;" id="lblPrincEmpId" class="hidden">0</label>
                                <label style="color: black;" id="lblEMP_PELID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Location <span style="color: red;">*</span></label>
                                <select class="form-control" id="ddlLocationEmployeeMaster">
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Branch <span style="color: red;">*</span></label>
                                <select class="form-control" id="ddlBranchEmployeeMaster">
                                </select>
                            </div>
                        </div>



                        <%--<div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract From*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtEmployeeContractForm" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract To*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtEmployeeContractTo" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>--%>



                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract From <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="From-Date" id="txtEmployeeContractForm" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract To <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="To-Date" id="txtEmployeeContractTo"/>
                            </div>
                        </div>

                        <%--<div id="divContractEnd" class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract End Date*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractEndDate" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract End Reason*</label>
                                <input type="text" id="txtContractEndReason" class="form-control" />
                            </div>
                        </div>--%>

                        <div id="divContractEnd" class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract End Date<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="Contract-End-Date" id="txtContractEndDate" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract End Reason<span style="color: red;">*</span></label>
                                <input type="text" id="txtContractEndReason" class="form-control" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnMigrateSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                                <input type="hidden" id="hdMode" name="hdMode" value="" />
                                <input type="hidden" id="txtPEContractFrom" name="txtPEContractFrom" value="" />
                                <input type="hidden" id="txtPEContractTo" name="txtPEContractTo" value="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal  modal-fullscreen" id="ModalContractorMaster" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="height: 530px; width: 710px; margin-left: -650px; overflow-y: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contractor Details</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row" id="divMsgContractor">
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <strong></strong><span id="spanMsgContractor"></span>
                                </div>
                            </div>
                        </div>


                        <div class="row alert" id="divErrorsContractor">
                            <div class="col-md-12">
                                <div>
                                    <strong></strong><span></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contractor Name <span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtContractorName" maxlength="50" />
                                <label style="color: black;" id="lblContractorId" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Address <span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtContractorAddres" maxlength="250" />
                            </div>
                        </div>
                        <div class="row">
                            <%--<div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract From*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractorContractForm" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract To*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractorContractTo" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract From <span style="color: red;">*</span></label>
                                <input  type="text" class="form-control" placeholder="From-Date" id="txtContractorContractForm"/>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract To <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="To-Date" id="txtContractorContractTo" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Nature Of Work <span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtContractorNatureOfWork" maxlength="100" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">No Of Employee <span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtContractorNoOfEmployee" maxlength="7" onkeypress="return isNumber(event)" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkCanteenProvided" value="1" />Canteen Provided</label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkRestRoomProvided" value="1" />Rest Room Provided</label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkCrechesProvided" value="1" />Creches Provided</label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkDrinkingWaterProvided" value="1" />Drinking Water Provided</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkFirstAidProvided" value="1" />First Aid Provided</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnContractorSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="divReAssignCompliance">
        <iframe id="iframeReAssign" style="width: 100%; height: 300px; border: none;"></iframe>
    </div>

    <div id="divUploadContractors">
        <iframe id="iframeUploadContractors" style="width: 100%; height: 300px; border: none;"></iframe>
    </div>

    <div id="divUploadLocation">
        <iframe id="iframeUploadLocation" style="width: 100%; height: 300px; border: none;"></iframe>
    </div>






</body>

</html>
<script type="text/javascript">





    $(document).ready(function () {
        $(".tabdata").hide();
        $("#divPECreation").show();
        $("#divMsg").hide();
        $("#divMsgPELocation").hide();
        $("#divMsgContractor").hide();
        $("#divErrorsPE").hide();
        $("#divErrorsPELocation").hide();
        $("#divErrorsContractor").hide();
        $("#divErrorsEmployees").hide();
        $("#divCheckPE").hide();
        $("#divCheckPELocation").hide();
        $("#divCheckEmployees").hide();
        $("#divCheckContractor").hide();


        var items = parent.document.getElementsByClassName('listItem');
        for (var i = 0; i < items.length; i++) {
            items[i].classList.remove("active");
        }

        var item = parent.document.getElementById("liPECreation");
        item.classList.add("active");

        var sd = new Date(), ed = new Date();

        $('#txtContractForm').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true

        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtContractTo').datepicker('setStartDate', null); //must uncomment
            }
            else {
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('#txtContractTo').datepicker('setStartDate', startDate);
            }

        });;





        $('#txtContractTo').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true,
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtContractForm').datepicker('setEndDate', null); //must uncomment
            }
            else {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('#txtContractForm').datepicker('setEndDate', FromEndDate);
            }
        });

        $('#txtLicenceValidFromdate').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true

        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtLicenceValidTodate').datepicker('setStartDate', null); //must uncomment
            }
            else {
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('#txtLicenceValidTodate').datepicker('setStartDate', startDate);
            }

        });;

         $('#txtLicenceValidTodate').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true,
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtLicenceValidFromdate').datepicker('setEndDate', null); //must uncomment
            }
            else {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('#txtLicenceValidFromdate').datepicker('setEndDate', FromEndDate);
            }
        });


        $('#txtEmployeeContractForm').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true

        }).on('changeDate', function (selected) {

            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtEmployeeContractTo').datepicker('setStartDate', null); //must uncomment
            }
            else {
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('#txtEmployeeContractTo').datepicker('setStartDate', startDate);
                $('#txtContractEndDate').datepicker('setStartDate', startDate);
            }


        });



        $('#txtEmployeeContractTo').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtEmployeeContractForm').datepicker('setEndDate', null); //must uncomment
            }
            else {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('#txtEmployeeContractForm').datepicker('setEndDate', FromEndDate);
            }
        });



        $('#txtContractEndDate').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtEmployeeContractForm').datepicker('setEndDate', FromEndDate);
        });


















        $('#txtContractorContractForm').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                $('#txtContractorContractTo').datepicker('setStartDate', null); //must uncomment
            }
            else {
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('#txtContractorContractTo').datepicker('setStartDate', startDate);
            }

        });;

        $('#txtContractorContractTo').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                $('#txtContractorContractForm').datepicker('setEndDate', null); //must uncomment
            }
            else {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('#txtContractorContractForm').datepicker('setEndDate', FromEndDate);
            }
        });


    });

    

    $("#btnUploadLocation").kendoTooltip({
        content: function (e) {
            return "Upload Bulk";
        }
    });

    $("#btnUploadContractors").kendoTooltip({
        content: function (e) {
            return "Upload Bulk";
        }
    });

    $("#btnAddContractor").kendoTooltip({
        content: function (e) {
            return "Add New Contractor";
        }
    });

    $("#btnUploadEmployee").kendoTooltip({
        content: function (e) {
            return "Upload Bulk";
        }
    });

    $("#btnMigrate").kendoTooltip({
        content: function (e) {
            return "Migrate Employees";
        }
    });

    $("#btnNewPrincipleEmployer").kendoTooltip({
        content: function (e) {
            return "Add New Principle Employer";
        }
    });

    $("#btnNext").kendoTooltip({
        content: function (e) {
            return "Next";
        }
    });


    $("#btnLocationNext").kendoTooltip({
        content: function (e) {
            return "Next";
        }
    });
    $("#btnLocationPrev").kendoTooltip({
        content: function (e) {
            return "Previous";
        }
    });

    $("#btnEmpMasterNext").kendoTooltip({
        content: function (e) {
            return "Next";
        }
    });
    $("#btnEmpMasterPrev").kendoTooltip({
        content: function (e) {
            return "Previous";
        }
    });

    $("#btnContractorNext").kendoTooltip({
        content: function (e) {
            return "Next";
        }
    });
    $("#btnContractorPrev").kendoTooltip({
        content: function (e) {
            return "Previous";
        }
    });

    $("#btnPEReportPrev").kendoTooltip({
        content: function (e) {
            return "Previous";
        }
    });

    $("#btnNewPELocation").kendoTooltip({
        content: function (e) {
            return "Add New Principle Employer Location";
        }
    });

</script>
