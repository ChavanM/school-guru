﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.HomePage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix" style="height: 20px"></div>
    <section class="wrapper">

                <div class="row">
                    <div class="dashboard">
                        <!--Performer summary-->
                       <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Performer Summary</h2>
                                    <div class="panel-actions">

                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>


                            </div>

                            <div id="collapsePerformer" class="panel-collapse collapse in">
                                <div class="row ">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                        <div class="info-box white-bg">
                                            <div class="title">Upcoming</div>
                                            <div class="col-md-6 borderright">
                                                <div class="count">40</div>
                                                <div class="desc">Statutory</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="count">35</div>
                                                <div class="desc">Internal</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                        <div class="info-box white-bg">
                                            <div class="title">Overdue</div>
                                            <div class="col-md-6 borderright">
                                                <div class="count">40</div>
                                                <div class="desc">Statutory</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="count">35</div>
                                                <div class="desc">Internal</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                        <div class="info-box white-bg">
                                            <div class="title">Checklist</div>
                                            <div class="col-md-6 borderright">
                                                <div class="count">40</div>
                                                <div class="desc">Statutory</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="count">35</div>
                                                <div class="desc">Internal</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                        <div class="info-box white-bg">
                                            <div class="title">Rejected</div>
                                            <div class="col-md-6 borderright">
                                                <div class="count">40</div>
                                                <div class="desc">Statutory</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="count">35</div>
                                                <div class="desc">Internal</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div><!--/.row-->
                            </div>
                        </div>


                        <!--Reviewer summary-->
                        <div id="reviewersummary" class="col-lg-12 col-md-12 colpadding0">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Reviewer Summary</h2>
                                    <div class="panel-actions">

                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseReviewer"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('reviewersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>


                            </div>

                            <div id="collapseReviewer" class="panel-collapse collapse in">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                        <div class="info-box white-bg">
                                            <div class="title">Due but not submitted</div>
                                            <div class="col-md-6 borderright">
                                                <div class="count">40</div>
                                                <div class="desc">Statutory</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="count">35</div>
                                                <div class="desc">Internal</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                        <div class="info-box white-bg">
                                            <div class="title">Due but not submitted</div>
                                            <div class="col-md-6 borderright">
                                                <div class="count">40</div>
                                                <div class="desc">Statutory</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="count">35</div>
                                                <div class="desc">Internal</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                        <div class="info-box white-bg">
                                            <div class="title">Due but not submitted</div>
                                            <div class="col-md-6 borderright">
                                                <div class="count">40</div>
                                                <div class="desc">Statutory</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="count">35</div>
                                                <div class="desc">Internal</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0">
                                        <div class="info-box white-bg">
                                            <div class="title">Due but not submitted</div>
                                            <div class="col-md-6 borderright">
                                                <div class="count">40</div>
                                                <div class="desc">Statutory</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="count">35</div>
                                                <div class="desc">Internal</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div><!--/.row-->
                            </div>
                        </div>
                    </div>

                </div>

               <div class="row Dashboard-white-widget" id="performerlocation">
                    <div class="dashboard">
                        <div  class="col-lg-12 col-md-12 ">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Performer Location</h2>
                                    <div class="panel-actions">

                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformerLoc"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('performerlocation')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>


                            </div>

                            <div id="collapsePerformerLoc" class="panel-collapse collapse in">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 colpadding0">
                                        <div class="panel-body-map">
                                            <div id="map" style="height:380px;"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div style="text-align:center;padding-top:40px">
                                            <ul class="bxslider-map" id="PerformerGraph" style="width: 100%; max-width: none;">
                                               <%-- <li style="height:260px">
                                                    <input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="70" data-thickness=".1" data-fgColor="#ff7473">
                                                    <br />
                                                    <h3>Mumbai</h3>
                                                </li>
                                                <li>
                                                    <input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="30" data-thickness=".1" data-fgColor="#ffc952">
                                                    <br />
                                                    <h3>Pune</h3>
                                                </li>
                                                <li>
                                                    <input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="80" data-thickness=".1" data-fgColor="#ff7473">
                                                    <br />
                                                    <h3>Delhi</h3>
                                                </li>
                                                <li>
                                                    <input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="40" data-thickness=".1" data-fgColor="#ffc952">
                                                    <br />
                                                    <h3>Thane</h3>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>





                                </div><!--/.row-->
                                <div class="clearfix" style="height:10px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row Dashboard-white-widget" id="reviewerlocation">
                    <div class="dashboard">
                        <div  class="col-lg-12 col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Reviewer Location</h2>
                                    <div class="panel-actions">

                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePreviewerLoc"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('reviewerlocation')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>


                            </div>

                            <div id="collapsePreviewerLoc" class="panel-collapse collapse in">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 colpadding0">
                                        <div class="panel-body-map">
                                            <div id="map2" style="height:380px;"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div style="text-align:center;padding-top:40px">
                                            <ul class="bxslider-map" id="ReviewerGraph" style="width: 100%; max-width: none;">
                                                <%--<li style="height:260px">
                                                    <input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="70" data-thickness=".1" data-fgColor="#ff7473">
                                                    <br />
                                                    <h3>Mumbai</h3>
                                                </li>
                                                <li>
                                                    <input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="85" data-thickness=".1" data-fgColor="#ffc952">
                                                    <br />
                                                    <h3>Pune</h3>
                                                </li>
                                                <li>
                                                    <input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="45" data-thickness=".1" data-fgColor="#ff7473">
                                                    <br />
                                                    <h3>Delhi</h3>
                                                </li>
                                                <li>
                                                    <input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="30" data-thickness=".1" data-fgColor="#ffc952">
                                                    <br />
                                                    <h3>Thane</h3>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>





                                </div><!--/.row-->
                                <div class="clearfix" style="height:10px"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row Dashboard-white-widget" id="dailyupdates" >
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Daily Updates</h2>
                                    <div class="panel-actions">

                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseDailyUpdates"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('dailyupdates')" class="btn-close" ><i class="fa fa-times"></i></a>
                                    </div>
                                </div>


                            </div>

                            <div id="collapseDailyUpdates" class="panel-collapse collapse in">
                                <div class="row dailyupdates">
                                    <ul id="DailyUpdatesrow" class="bxslider" style="width: 100%; max-width: none;">


                                        
                                    </ul>
                                </div><!--/.row-->
                                <div class="clearfix" style="height:10px"></div>
                            </div>
                            <div class="modal fade" id="NewsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		                            <div class="modal-dialog" style="width:900px">
                                        <div class="modal-content" >
                                            <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h2 id="dailyupdatedate">Daily Updates: November 3, 2016</h2>
                                                    </div>
                                                    <div class="modal-body" id="dailyupdatecontent">
                                                            <h3 id="dailyupdatedateInner">
                                                             Daily Updates: November 3,
                                                            </h3>
                                                        <p id="dailytitle"></p>
                                                                <div id="contents">
                                                                </div>
                                                    </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row Dashboard-white-widget" id="newsletter">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>News Letter</h2>
                                    <div class="panel-actions">

                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseNewsletter"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('newsletter')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>


                            </div>

                            <div id="collapseNewsletter" class="panel-collapse collapse in">
                                <div class="row dailyupdates">
                                    <ul id="Newslettersrow" class="bxslider" style="width: 100%; max-width: none;">
                                 </ul>
                                </div><!--/.row-->
                                 <div class="modal fade" id="Newslettermodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="width:550px">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                               
                                            </div>
                                            <div class="modal-body">
                                                 <img id="newsImg" src="../Images/xyz.png" style="border-bottom-left-radius:10px; border-bottom-right-radius:10px; width:100%" />
                                                 <h2  id="newsTitle"></h2>
                                                 <div class="clearfix" style="height:10px;"></div>
                                                 <div id="newsDesc"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix" style="height:10px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    <script type="text/javascript">
        var dailyupdateJson;
        var NewsLetterJson;
        function bindnewsdata() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',
                url: '/dailyupdateservice.svc/DailyNewsLetter',
                content: 'application/json;charset=utf-8',
                processData: true,
                success: function (result) {
                    NewsLetterJson = result;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {

                        var objDate = new Date(timeconvert(result[step].NewsDate));
                        str += '<li><img src="../Images/' + result[step].FileName + '" /><h3>' + result[step].Title + '</h3><p><a data-toggle="modal" onclick="BindNewsLetterpopup(' + result[step].ID + ');" href="#Newslettermodal1"> Issue ' + getmonths(objDate) + ' ' + objDate.getFullYear() + '</a></p></li>'
                    }
                    $("#Newslettersrow").html(str)
                },
                error: function (e, t) { }
            })
        }
      
        function binddailyupdatedata() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',
                url: '/dailyupdateservice.svc/DailyUpdate',
                content: 'application/json;charset=utf-8',
                processData: true,
                success: function (result) {
                    dailyupdateJson=result;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {
                        str += '<li><h3 > ' + result[step].Title.substring(0, 150) + ' </h3> <p>' + result[step].Description.substring(0, 150) + ' <br /><br />   <a data-toggle="modal" onclick="Binddailyupdatepopup('+result[step].ID+');" href="#NewsModal">Real More</a></p></li>'
                    }
                    $("#DailyUpdatesrow").html(str)
                },
                error: function (e, t) { }
            })
        }

        binddailyupdatedata();
        bindnewsdata();
        function timeconvert(ds) {
            var D, dtime, T, tz, off,
            dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
            T = parseInt(dobj[0]);
            tz = dobj[1];
            off = dobj[2];
            if (off) {
                off = (parseInt(off.substring(0, 2), 10) * 3600000) +
    (parseInt(off.substring(2), 10) * 60000);
                if (tz == '-') off *= -1;
            }
            else off = 0;
            return new Date(T += off).toUTCString();
        }
        function getmonths(mon) {
            try {
                return mon.toString().split(' ')[1];
            } catch (e) { }
        }

        function Binddailyupdatepopup(DID) {
            
             
                for (var i=0; i < dailyupdateJson.length; i++) {
                    if (DID == dailyupdateJson[i].ID) {
                        var objDate = new Date(timeconvert(dailyupdateJson[i].CreatedDate));
                        var mon_I = getmonths(objDate);
                        $("#dailyupdatedate").html('Daily Updates:' + mon_I + ' ' + objDate.getDate() + ', ' + objDate.getFullYear());
                        $("#dailyupdatedateInner").html('Daily Updates:' + mon_I + ' ' + objDate.getDate());
                        $("#contents").html(dailyupdateJson[i].Description);
                        $("#dailytitle").html(dailyupdateJson[i].Title);
                        break;
                    }
                }
            
        }
        function BindNewsLetterpopup(DID) {
            for (var i = 0; i < NewsLetterJson.length; i++) {
                if (DID == NewsLetterJson[i].ID) {
                    
                    $("#newsImg").attr('src', '../Images/' + NewsLetterJson[i].FileName);
                    $("#newsTitle").html(NewsLetterJson[i].Title);
                    $("#newsDesc").html(NewsLetterJson[i].Description);
              
                    break;
                }
            }

        }

    </script>

    <script type="text/javascript">

        var locations = [
          ['Location 1 Name', 'Pune', '70'],
          ['Location 2 Name', 'Mumbai', '80'],
          ['Location 3 Name', 'Bangalore', '75']
        ];
        var locations1 = [
    ['Location 1 Name', 'Pune', '40'],
    ['Location 2 Name', 'Mumbai', '80'],
    ['Location 3 Name', 'Bangalore', '50']
        ];
        //Map code  
        {
            var geocoder, geocoder1;
            var map, map2;
            var bounds = new google.maps.LatLngBounds();
            var bounds1 = new google.maps.LatLngBounds();
            function initialize() {
                map = new google.maps.Map(
                  document.getElementById("map"), {
                      center: new google.maps.LatLng(37.4419, -122.1419),
                      zoom: 13,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                  });

                map2 = new google.maps.Map(
                document.getElementById("map2"), {
                    center: new google.maps.LatLng(37.4419, -122.1419),
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                geocoder = new google.maps.Geocoder();
                for (i = 0; i < locations.length; i++) {
                    geocodeAddress(locations, i);
                }
                geocoder1 = new google.maps.Geocoder();
                for (i = 0; i < locations1.length; i++) {
                    geocodeAddress1(locations1, i);
                }
            }
            google.maps.event.addDomListener(window, "load", initialize);
            function geocodeAddress(locations, i) {
                var title = locations[i][0];
                var address = locations[i][1];
                var url = locations[i][2];
                geocoder.geocode({
                    'address': locations[i][1]
                },
                  function (results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                          var marker = new google.maps.Marker({
                              icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                              map: map,
                              position: results[0].geometry.location,
                              title: title,
                              animation: google.maps.Animation.DROP,
                              address: address,
                              url: url
                          })
                          // infoWindow(marker, map, title, address, url);
                          bounds.extend(marker.getPosition());
                          map.fitBounds(bounds);
                          window.google.maps.event.addListener(marker, 'click', function () {
                              $('#PerformerGraph > li').each(function (index) {

                                  var thechosenone = 'p' + address;
                                  if ($(this).attr("id") == thechosenone) {
                                      $('#PerformerGraph').attr("style", "width: 515%; max-width: none; position: relative; transition-duration: 0s; transform: translate3d(-" + $(this).attr("scroll-val") + ", 0px, 0px); ");

                                  }

                              });
                          });
                      } else {
                          alert("geocode of " + address + " failed:" + status);
                      }
                  });
            }

            function geocodeAddress1(locations1, i) {
                var title1 = locations1[i][0];
                var address1 = locations1[i][1];
                var url1 = locations1[i][2];
                geocoder1.geocode({
                    'address': locations1[i][1]
                },
                  function (results1, status1) {
                      if (status1 == google.maps.GeocoderStatus.OK) {
                          var marker1 = new google.maps.Marker({
                              icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                              map: map2,
                              position: results1[0].geometry.location,
                              title: title1,
                              animation: google.maps.Animation.DROP,
                              address: address1,
                              url: url1
                          })
                          window.google.maps.event.addListener(marker1, 'click', function () {
                              $('#ReviewerGraph > li').each(function (index) {
                                  var thechosenone = 'r' + address1;
                                  if ($(this).attr("id") == thechosenone) {
                                      $('#ReviewerGraph').attr("style", "width: 515%; max-width: none; position: relative; transition-duration: 0s; transform: translate3d(-" + $(this).attr("scroll-val") + ", 0px, 0px); ");
                                  }

                              });
                          });
                          //infoWindow(marker1, map1, title1, address1, url1);
                          bounds1.extend(marker1.getPosition());
                          map2.fitBounds(bounds1);
                      } else {
                          alert("geocode of " + address1 + " failed:" + status1);
                      }
                  });
            }


            function infoWindow(marker, map, title, address, url) {
                google.maps.event.addListener(marker, 'click', function () {
                    var html = "<div><h3>" + title + "</h3><p>" + address + "<br></div><a href='" + url + "'>View location</a></p></div>";
                    iw = new google.maps.InfoWindow({
                        content: html,
                        maxWidth: 350
                    });
                    iw.open(map, marker);
                });
            }
            function createMarker(results) {
                var marker = new google.maps.Marker({
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                    map: map,
                    position: results[0].geometry.location,
                    title: title,
                    animation: google.maps.Animation.DROP,
                    address: address,
                    url: url
                })
                bounds.extend(marker.getPosition());
                map.fitBounds(bounds);
                infoWindow(marker, map, title, address, url);
                return marker;
            }

            function infoWindow1(marker1, map1, title1, address1, url1) {
                google.maps.event.addListener(marker1, 'click', function () {
                    var html1 = "<div><h3>" + title1 + "</h3><p>" + address1 + "<br></div><a href='" + url1 + "'>View location</a></p></div>";
                    iw = new google.maps.InfoWindow({
                        content: html1,
                        maxWidth: 350
                    });
                    iw.open(map1, marker1);
                });
            }
            function createMarker1(results1) {
                var marker1 = new google.maps.Marker({
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                    map: map2,
                    position: results1[0].geometry.location,
                    title: title1,
                    animation: google.maps.Animation.DROP,
                    address: address1,
                    url: url1
                })
                bounds.extend(marker1.getPosition());
                map.fitBounds(bounds1);
                infoWindow(marker1, map2, title1, address1, url1);
                return marker1;
            }
        }

        function fgraph() {
            var graph = "";
            var scroll = 340;

            for (var i = 0; i < locations.length; i++) {

                scroll = scroll + 300;
                graph += '<li style="height:260px" id="p' + locations[i][1] + '" scroll-val="' + scroll + '"><input class="knob" data-width="120" data-min="0" data-displayPrevious=true data-readOnly=true value="' + locations[i][2] + '" data-thickness=".1" data-fgColor="#ff7473"><br /><input class="knob" data-width="120" data-min="0" data-displayPrevious=true data-readOnly=true value="' + locations[i][2] + '" data-thickness=".1" data-fgColor="#ff7473"><br /><h3>' + locations[i][1] + '</h3></li>'
            }
            $("#PerformerGraph").html(graph);
            var graph1 = ''; scroll = 340;
            for (var i = 0; i < locations1.length; i++) {
                scroll = scroll + 300;
                graph1 += '<li style="height:260px" id="r' + locations1[i][1] + '" scroll-val="' + scroll + '"><input class="knob" data-width="200" data-min="0" data-displayPrevious=true data-readOnly=true value="' + locations1[i][2] + '" data-thickness=".1" data-fgColor="#ff7473"><br /><h3>' + locations1[i][1] + '</h3></li>'
            }
            $("#ReviewerGraph").html(graph1);
        }
        fgraph();


    </script>

</asp:Content>
