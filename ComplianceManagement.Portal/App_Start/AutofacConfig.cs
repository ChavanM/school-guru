﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using BM_ManegmentServices.Services.Annotation;
using BM_ManegmentServices.Services.AnnualMeetingCalendar;
using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.Dashboard;
using BM_ManegmentServices.Services.DirectorMeetings;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.EULA;
using BM_ManegmentServices.Services.Forms;
using BM_ManegmentServices.Services.HelpVideo;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.Services.MeetingsHistory;
using BM_ManegmentServices.Services.MyMeetings;
using BM_ManegmentServices.Services.Profile;
using BM_ManegmentServices.Services.Registers;
using BM_ManegmentServices.Services.Reports;
using BM_ManegmentServices.Services.Setting;
using BM_ManegmentServices.Services.UIForm;
using com.VirtuosoITech.ComplianceManagement.Portal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace BM_Manegment.App_Start
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly()); //Register MVC Controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()); //Register WebApi Controllers

            //This will register class & interface
            builder.RegisterType<EntityMaster>().As<IEntityMaster>();
            builder.RegisterType<CapitalMaster>().As<ICapitalMaster>();
            builder.RegisterType<Holiday>().As<IHoliday>();
            builder.RegisterType<ShareholdingMastre>().As<IShareholdingMaster>();
            builder.RegisterType<DirectorMaster>().As<IDirectorMaster>();
            builder.RegisterType<CountryStateCityMaster>().As<ICountryStateCityMaster>();
            builder.RegisterType<CommitteeComp>().As<ICommitteeComp>();
            builder.RegisterType<CommitteeMaster>().As<ICommitteeMaster>();
            builder.RegisterType<DesignationMaster>().As<IDesignationMaster>();
            builder.RegisterType<Board_DirectorMaster>().As<IBoard_DirectorMaster>();
            builder.RegisterType<Note>().As<INote>();
            builder.RegisterType<KMP_Master>().As<IKMP_Master>();

            builder.RegisterType<DirectorRelative>().As<IDirectorRelative>();
            builder.RegisterType<AgendaMaster>().As<IAgendaMaster>();
            
            builder.RegisterType<BM_ManegmentServices.Services.Masters.User>().As<IUser>();

            builder.RegisterType<AuditorMaster>().As<IAuditorMaster>();

            builder.RegisterType<Template_Services>().As<ITemplateservice>();

            builder.RegisterType<Meeting_Service>().As<IMeeting_Service>();
            builder.RegisterType<MainMeeting>().As<IMainMeeting>();
            builder.RegisterType<MeetingInvitee>().As<IMeetingInvitee>();

            builder.RegisterType<AgendaCompliance>().As<IAgendaCompliance>();            
            builder.RegisterType<MeetingComplianceMapping>().As<IMeetingComplianceMapping>();
            builder.RegisterType<MeetingAgendaInterestedParty>().As<IMeetingAgendaInterestedParty>();
            builder.RegisterType<AgendaMasterTemplate>().As<IAgendaMasterTemplate>();


            //builder.RegisterType<Compliance_Service>().As<ICompliance_Service>();
            //builder.RegisterType<Compliance_ServiceSecretarial>().As<ICompliance_ServiceSecretarial>();

            //builder.RegisterType<Meeting_CompliancesSecretarial>().As<IMeeting_Compliances>();
            //builder.RegisterType<Compliance_ServiceSecretarial>().As<ICompliance_Service>();
            //builder.RegisterType<ComplianceTransaction_ServiceComplianceTransaction_ServiceSecretarial>().As<IComplianceTransaction_Service>();
            //builder.RegisterType<FileData_ServiceSecretarial>().As<IFileData_Service>();
            //builder.RegisterType<BM_ManegmentServices.Services.Masters.ComplianceSecretarial>().As<ICompliance>();

            builder.RegisterType<Meeting_Compliances>().As<IMeeting_Compliances>();
            builder.RegisterType<Compliance_Service>().As<ICompliance_Service>();
            builder.RegisterType<ComplianceTransaction_Service>().As<IComplianceTransaction_Service>();
            builder.RegisterType<FileData_Service>().As<IFileData_Service>();
            builder.RegisterType<BM_ManegmentServices.Services.Masters.Compliance>().As<ICompliance>();
            
            builder.RegisterType<Configuration>().As<IConfiguration>();
            builder.RegisterType<EntityUserAssignment>().As<IEntityuserAssignment>();
            builder.RegisterType<Register>().As<IRegister>();
            builder.RegisterType<RegisterDetails>().As<IRegisterDetails>();

            builder.RegisterType<Task>().As<ITask>();

            builder.RegisterType<Home>().As<IHome>();
            builder.RegisterType<PageAuthentication>().As<IPageAuthentication>();
            builder.RegisterType<EmailService>().As<IEmailService>();
            builder.RegisterType<DashboardService>().As<IDashboardService>();

            builder.RegisterType<UIFormService>().As<IUIFormService>();

            builder.RegisterType<FormTemplet>().As<IFormTemplets>();
            builder.RegisterType<Forms_Service>().As<IForms_Service>();

            builder.RegisterType<DirectorCompliances>().As<IDirectorCompliances>();

            builder.RegisterType<MyProfile>().As<IMyProfile>();
            builder.RegisterType<DirctorMeetings>().As<IDirectorMeetings>();
            builder.RegisterType<MyMeetingsService>().As<IMyMeetingsService>();

            builder.RegisterType<ReportPolices>().As<IReportPolices>();
            builder.RegisterType<SettingService>().As<ISettingService>();


            builder.RegisterType<AdditionalComplianceMaster>().As<IAdditionalComplianceMaster>();

            builder.RegisterType<FileStorage_Service>().As<IFileStorage_Service>();
            builder.RegisterType<Historical>().As<IHistorical>();
            builder.RegisterType<NotesMaster_Service>().As<INotesMaster_Service>();

            //builder.RegisterType<BM_ManegmentServices.Services.Masters.DocumentFormat>().As<IDocumentFormat>();
			builder.RegisterType<BM_ManegmentServices.Services.Masters.DocumentFormat>().As<IDocumentFormat>();
            builder.RegisterType<AnnualCalender>().As<IAnnualCalender>();

            builder.RegisterType<VideoMeeting>().As<IVideoMeeting>();
			builder.RegisterType<AgendaMinutesReviewService>().As<IAgendaMinutesReviewService>();
            builder.RegisterType<DocumentSetting_Service>().As<IDocumentSetting_Service>();
            builder.RegisterType<Document_Service>().As<IDocument_Service>();
            builder.RegisterType<EULA_Service>().As<IEULA_Service>();

            builder.RegisterType<EditCustMaster>().As<IEditCustMaster>();
            builder.RegisterType<AnnotationService>().As<IAnnotationService>();
            builder.RegisterType<HelpVideoService>().As<IHelpVideoService>();

            builder.RegisterControllers(typeof(Global).Assembly).PropertiesAutowired();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver((Autofac.IContainer)container); //Set the WebApi DependencyResolver
        }
    }
}
