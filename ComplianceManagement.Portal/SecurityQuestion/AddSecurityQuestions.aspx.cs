﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Security;
using System.Configuration;
using System.Drawing;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.SecurityQuestion
{
    public partial class AddSecurityQuestions : System.Web.UI.Page
    {
        protected string LogoName;
        protected int customerid;
        protected int userid;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (AuthenticationHelper.CustomerID == 0)
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

            }
            else
            {
                customerid = Convert.ToInt32(Session["CustomerID_new"]);

            }
            string CustomerLogo = string.Empty;
            Customer objCust = UserManagement.GetCustomerforLogo(customerid);
            if (objCust != null)
            {
                if (objCust.LogoPath != null)
                {
                    CustomerLogo = objCust.LogoPath;
                    LogoName = CustomerLogo.Replace("~", "..");
                }
            }
            if (!IsPostBack)
            {
                BindQuestionsFirst();
                if (Convert.ToBoolean(Session["QuestionBank"]) == true)
                {
                    divMasterQuestions.Visible = true;
                    QuestionAnswar.Visible = false;
                }
                else
                {
                    QuestionAnswar.Visible = true;
                    divMasterQuestions.Visible = false;
                    SetRandomQuestion();
                }
                if (Session["CustomerID_new"] != null)
                {
                    cid.Value = Session["CustomerID_new"].ToString();
                }

            }
        }

        private void SetRandomQuestion()
        {
            try
            {
                if (!string.IsNullOrEmpty(Session["userID"].ToString()))
                {
                    var randomQuestion = UserManagement.GetRandomQuestions(Convert.ToInt64(Session["userID"]));
                    if (randomQuestion != null)
                    {
                        lblQuestion1ID.Text = Convert.ToString(randomQuestion[0].ID);
                        lblQuestion1.Text = randomQuestion[0].Question;
                        lblQuestion2ID.Text = Convert.ToString(randomQuestion[1].ID);
                        lblQuestion2.Text = randomQuestion[1].Question;
                    }
                }
                else
                {
                    string errormsg = "Session userid is null AddSecurityQuestions";
                    LoggerMessage.InsertLog(null, errormsg.ToString(), MethodBase.GetCurrentMethod().Name);
                }              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlQuestion1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindQuestions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlQuestion2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindQuestions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlQuestion3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindQuestions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindQuestions()
        {
            try
            {
                var Questions = UserManagement.GetSecurityQuestions();

                if (ddlQuestion1.SelectedIndex != -1 && ddlQuestion1.SelectedValue != "-1")
                {
                    Questions = Questions.Where(entry => entry.Question != ddlQuestion1.SelectedItem.Text).ToList();
                }
                if (ddlQuestion2.SelectedIndex != -1 && ddlQuestion2.SelectedValue != "-1")
                {
                    Questions = Questions.Where(entry => entry.Question != ddlQuestion2.SelectedItem.Text).ToList();
                }
                if (ddlQuestion3.SelectedIndex != -1 && ddlQuestion3.SelectedValue != "-1")
                {
                    Questions = Questions.Where(entry => entry.Question != ddlQuestion3.SelectedItem.Text).ToList();
                }
                if (!(ddlQuestion1.SelectedIndex != -1 && ddlQuestion1.SelectedValue != "-1"))
                {
                    ddlQuestion1.DataTextField = "Question";
                    ddlQuestion1.DataValueField = "ID";
                    ddlQuestion1.Items.Clear();
                    ddlQuestion1.DataSource = Questions;
                    ddlQuestion1.DataBind();
                    ddlQuestion1.Items.Insert(0, new ListItem("Select Question 1", "-1"));
                }

                if (!(ddlQuestion2.SelectedIndex != -1 && ddlQuestion2.SelectedValue != "-1"))
                {

                    ddlQuestion2.DataTextField = "Question";
                    ddlQuestion2.DataValueField = "ID";
                    ddlQuestion2.Items.Clear();
                    ddlQuestion2.DataSource = Questions;
                    ddlQuestion2.DataBind();
                    ddlQuestion2.Items.Insert(0, new ListItem("Select Question 2", "-1"));
                }

                if (!(ddlQuestion3.SelectedIndex != -1 && ddlQuestion3.SelectedValue != "-1"))
                {
                    ddlQuestion3.DataTextField = "Question";
                    ddlQuestion3.DataValueField = "ID";
                    ddlQuestion3.Items.Clear();
                    ddlQuestion3.DataSource = Questions;
                    ddlQuestion3.DataBind();
                    ddlQuestion3.Items.Insert(0, new ListItem("Select Question 3", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        

        private void BindQuestionsFirst()
        {
            try
            {
                var Questions = UserManagement.GetSecurityQuestions();

                ddlQuestion1.DataTextField = "Question";
                ddlQuestion1.DataValueField = "ID";
                ddlQuestion1.Items.Clear();
                ddlQuestion1.DataSource = Questions;
                ddlQuestion1.DataBind();
                //ddlQuestion1.Items.Insert(0, new ListItem("< Select Question >", "-1"));
                ddlQuestion1.Items.Insert(0, new ListItem("Select Question 1", "-1"));

                ddlQuestion2.DataTextField = "Question";
                ddlQuestion2.DataValueField = "ID";
                ddlQuestion2.Items.Clear();
                ddlQuestion2.DataSource = Questions;
                ddlQuestion2.DataBind();
                //ddlQuestion2.Items.Insert(0, new ListItem("< Select Question >", "-1"));
                ddlQuestion2.Items.Insert(0, new ListItem("Select Question 2", "-1"));


                ddlQuestion3.DataTextField = "Question";
                ddlQuestion3.DataValueField = "ID";
                ddlQuestion3.Items.Clear();
                ddlQuestion3.DataSource = Questions;
                ddlQuestion3.DataBind();
                ddlQuestion3.Items.Insert(0, new ListItem("Select Question 3", "-1"));
                //ddlQuestion3.Items.Insert(0, new ListItem("< Select Question >", "-1"));


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnValidateQuestions_Click(object sender, EventArgs e)
        {
            try
            {
                txtAnswer1.BorderColor = Color.Empty;
                txtAnswar2.BorderColor = Color.Empty;

                long userID = Convert.ToInt64(Session["userID"]);
                List<SecurityQuestionAnswar> questionList = new List<SecurityQuestionAnswar>();
                SecurityQuestionAnswar question1 = new SecurityQuestionAnswar();
                question1.SQID = Convert.ToInt32(lblQuestion1ID.Text.Trim());
                question1.Answar = txtAnswer1.Text.Trim();
                question1.UserID = userID;
                questionList.Add(question1);

                SecurityQuestionAnswar question2 = new SecurityQuestionAnswar();
                question2.SQID = Convert.ToInt32(lblQuestion2ID.Text);
                question2.Answar = txtAnswar2.Text.Trim();
                question2.UserID = userID;
                questionList.Add(question2);
                #region[add customerwise expire days]
                int? CID = CustomerManagement.GetByCustID(Convert.ToInt32(userID));               
                int expdays=0;
                if (CID == null)
                {
                    expdays = 60;
                }
                else
                {
                    int? Expirydaysgroup = CustomerManagement.GetExpirydays(Convert.ToInt32(CID), Convert.ToInt32(userID));
                    if (Expirydaysgroup == null || Expirydaysgroup == 0)
                    {
                        int? Expirydays = CustomerManagement.GetExpirydays(Convert.ToInt32(CID));
                        if (Expirydays == null || Expirydays == 0)
                        {
                            expdays = 60;
                        }
                        else
                        {
                            expdays = Convert.ToInt32(Expirydays);
                        }
                    }
                    else
                    {
                        expdays = Convert.ToInt32(Expirydaysgroup);
                    }
                }
                #endregion
              
                int flag = UserManagement.ValidateQuestions(questionList);
                if (flag == 0)
                {
                    if (Convert.ToBoolean(Session["RM"]))
                    {
                        if (FormsAuthentication.CookiesSupported)
                        {
                            //LoginCookie
                            HttpCookie loginCookie = new HttpCookie("ALC");
                            //let the cookie expire after 60 days
                            //  loginCookie.Expires = DateTime.Now.AddDays(60);
                            loginCookie.Expires = DateTime.Now.AddDays(expdays);
                            loginCookie.Values["EA"] = Convert.ToString(Session["EA"]);
                            loginCookie.Values["MA"] = Convert.ToString(Session["MA"]);
                            loginCookie.Secure = true;
                            loginCookie.HttpOnly = true;
                            Response.Cookies.Add(loginCookie);
                         
                        }
                    }
                    else
                    {
                        Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                    }                   
                    UserManagement.WrongAttemptCountUpdate(userID);
                    ProcessAuthenticationInformation(UserManagement.GetByID(Convert.ToInt32(userID)));
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SQ", "settracknewSucess();", true);

                    Session["RM"] = null;
                    Session["EA"] = null;
                    Session["MA"] = null;
                    Session["Counter"] = null;
                }
                else
                {
                    //if (flag == 1)
                    //    txtAnswer1.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    //else if (flag == 2)
                    //    txtAnswar2.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    //duEntry.IsValid = false;
                    //duEntry.ErrorMessage = "Please enter correct answers and try again.";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SQ", "settracknewFailed();", true);

                    if (Session["QCounter"] == null)
                    {
                        Session["QCounter"] = 0;
                    }
                    int i = Convert.ToInt32(Session["QCounter"]) + 1;
                    Session["QCounter"] = Convert.ToString(i);
                    if (Convert.ToInt32(Session["QCounter"]) < 6)
                    {
                        if (flag == 1)
                            txtAnswer1.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                        else if (flag == 2)
                            txtAnswar2.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                        duEntry.IsValid = false;
                        duEntry.ErrorMessage = "Please enter correct answers and try again.";
                    }
                    else
                    {
                        duEntry.IsValid = false;
                        duEntry.ErrorMessage = "Your reached failure login attempt, please logout and contact support team.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ProcessAuthenticationInformation(User user)
        {
            try
            {
                mst_User objmstuser = UserManagementRisk.GetByID(Convert.ToInt32(user.ID));
                string mstrole = RoleManagementRisk.GetByID(objmstuser.RoleID).Code;
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));                    
                    var IsInternalComplianceApplicable = c.IComplianceApplicable;
                    if (IsInternalComplianceApplicable != -1)
                    {
                        checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    }
                    
                    var IsTaskApplicable = c.TaskApplicable;
                    if (IsTaskApplicable != -1)
                    {
                        checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    }
                    var IsVerticlApplicable = c.VerticalApplicable;
                    if (IsVerticlApplicable != null)
                    {
                        if (IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }
                    }
                    if (c.IsPayment != null)
                    {
                        IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                    }
                    var IsLabelApplicable = c.IsLabelApplicable;
                    if (IsLabelApplicable != -1)
                    {
                        checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    }
                    if (c.ComplianceProductType != null)
                    {
                        complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                    }
                }                
                User userToUpdate = new User();
                userToUpdate.ID = user.ID;
                userToUpdate.LastLoginTime = DateTime.Now;
                userToUpdate.WrongAttempt = 0;
                UserManagement.Update(userToUpdate);                
                if (role.Equals("SADMN"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Users/UserSummary.aspx", false);
                }
                else if (role.Equals("IMPT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else if (role.Equals("UPDT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/LegalUpdateAdmin.aspx", false);
                }
                else if (role.Equals("RPER"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchPerformerAdmin.aspx", false);
                }
                else if (role.Equals("RREV"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchReviewerAdmin.aspx", false);
                }
                else if (role.Equals("HVADM"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }                
                else
                {
                    ProductMappingStructure _obj = new ProductMappingStructure();
                    var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(user.CustomerID));                   
                    if (ProductMappingDetails.Count == 1)
                    {
                        if (ProductMappingDetails.Contains(1))
                        {
                            _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);                           
                        }
                        else if (ProductMappingDetails.Contains(2)) 
                        {
                            _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(3))
                        {
                            _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(4))
                        {
                            _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(5))
                        {
                            _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(6))
                        {
                            _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(7))
                        {
                            _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(8))
                        {
                            _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                    }                   
                    else if (ProductMappingDetails.Count > 1) 
                    {
                        if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(2) && user.LitigationRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }                            
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(8))
                            {
                                _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(5) && user.ContractRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(8))
                            {
                                _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(6) && user.LicenseRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(8))
                            {
                                _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(7) && user.VendorRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(8))
                            {
                                _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, user.ID, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                            
                            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                            Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                        }
                    }                    
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, user.ID, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                        
                        Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                    }
                }

                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void RedirectToProduct(User user,List<long> ProductMappingDetails)
        {
            try
            {

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long userID = Convert.ToInt64(Session["userID"]);
                bool flag = true;
                bool flag1 = true;
                try
                {                   
                    flag = UserManagement.Delete(userID);
                    flag1 = UserManagementRisk.Delete(userID);
                }
                catch (Exception ex)
                {
                    flag = false;
                    flag1 = false;
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }

                if (flag && flag1)
                {                    
                    List<SecurityQuestionAnswar> questionList = new List<SecurityQuestionAnswar>();
                    SecurityQuestionAnswar question1 = new SecurityQuestionAnswar();
                    question1.SQID = Convert.ToInt32(ddlQuestion1.SelectedValue);
                    question1.Answar = txtQuestionAnswer1.Text.Trim();
                    question1.UserID = userID;
                    questionList.Add(question1);

                    SecurityQuestionAnswar question2 = new SecurityQuestionAnswar();
                    question2.SQID = Convert.ToInt32(ddlQuestion2.SelectedValue);
                    question2.Answar = txtQuestionAnswer2.Text.Trim();
                    question2.UserID = userID;
                    questionList.Add(question2);

                    SecurityQuestionAnswar question3 = new SecurityQuestionAnswar();
                    question3.SQID = Convert.ToInt32(ddlQuestion3.SelectedValue);
                    question3.Answar = txtQuestionAnswer3.Text.Trim();
                    question3.UserID = userID;
                    questionList.Add(question3);

                    List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk> questionListrisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk>();
                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question1risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
                    question1risk.SQID = Convert.ToInt32(ddlQuestion1.SelectedValue);
                    question1risk.Answar = txtQuestionAnswer1.Text.Trim();
                    question1risk.UserID = userID;
                    questionListrisk.Add(question1risk);

                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question2risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
                    question2risk.SQID = Convert.ToInt32(ddlQuestion2.SelectedValue);
                    question2risk.Answar = txtQuestionAnswer2.Text.Trim();
                    question2risk.UserID = userID;
                    questionListrisk.Add(question2risk);

                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question3risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
                    question3risk.SQID = Convert.ToInt32(ddlQuestion3.SelectedValue);
                    question3risk.Answar = txtQuestionAnswer3.Text.Trim();
                    question3risk.UserID = userID;
                    questionListrisk.Add(question3risk);

                    bool flagc = UserManagement.Create(questionList);
                    bool flagc1 = UserManagementRisk.Create(questionListrisk);
                    if (flagc && flagc1)
                    {
                        if (UserManagement.HasUserSecurityQuestion(userID))
                        {
                            bool signoutUser = true;
                            User loggedInUser = UserManagement.GetByID(Convert.ToInt32(userID));

                            if (loggedInUser != null)
                            {
                                if (Convert.ToBoolean(loggedInUser.SSOAccess))
                                {
                                    string ipaddress = string.Empty;

                                    signoutUser = false;
                                                                        
                                    Session["RM"] = false;
                                    Session["EA"] = Util.CalculateAESHash(loggedInUser.Email.Trim());
                                    Session["MA"] = Util.CalculateAESHash(ipaddress);
                                    Session["QuestionBank"] = false;
                                    Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                }
                            }
                            
                            if(signoutUser)
                            {
                                Session["userID"] = null;
                                FormsAuthentication.SignOut();
                                Session.Abandon();
                                FormsAuthentication.RedirectToLoginPage();
                            }
                        }
                        else
                        {
                            Session["QuestionBank"] = true;
                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                        }

                        //Session["userID"] = null;
                        //FormsAuthentication.SignOut();
                        //Session.Abandon();
                        //FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ddlQuestion1.SelectedIndex = -1;
                txtQuestionAnswer1.Text = string.Empty;
                ddlQuestion2.SelectedIndex = -1;
                txtQuestionAnswer2.Text = string.Empty;
                ddlQuestion3.SelectedIndex = -1;
                txtQuestionAnswer3.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnValidateClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtAnswer1.Text = string.Empty;
                txtAnswar2.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        protected void resetQuestions_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/SecurityQuestion/ResetSecurityQuestions.aspx", false);             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}