﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Security;
using System.Configuration;
using System.Drawing;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.SecurityQuestion
{
    public partial class UnlockAccount : System.Web.UI.Page
    {
        static int WrongOTPCount=0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                WrongOTPCount = 0;
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            try
            {
                User user = null;
                bool Success = true;

                if (UserManagement.IsValidUser(txtUnlockAccountUserID.Text.Trim(), out user))
                {
                    if ((UserManagement.WrongAttemptCount(txtUnlockAccountUserID.Text.Trim()) >= 3))
                    {
                        if (user.IsActive)
                        {
                            Session["userID"] = user.ID;

                            List<String> AdminEmailList = new List<String>();

                            String message = String.Empty;
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                            //Generate Random Number 6 Digit For OTP.
                            Random random = new Random();
                            int value = random.Next(1000000);
                            long Contact;
                            //VerifyOTP OTPData = new VerifyOTP()
                            //{
                            //    UserId = Convert.ToInt32(user.ID),
                            //    EmailId = txtUnlockAccountUserID.Text,
                            //    MobileNo = Convert.ToInt64(user.ContactNumber),
                            //    OTP = Convert.ToInt32(value),
                            //    CreatedOn = DateTime.Now,
                            //    IsVerified = false
                            //};

                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = txtUnlockAccountUserID.Text,
                                OTP = Convert.ToInt32(value),
                                CreatedOn = DateTime.Now,
                                IsVerified = false
                            };
                            bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                            if (OTPresult)
                            {
                                OTPData.MobileNo = Contact;
                            }
                            else
                            {
                                OTPData.MobileNo = 0;
                            }
                            VerifyOTPManagement.Create(OTPData);

                            try
                            {
                                //Send Email on User Mail Id.
                                if (user.CustomerID != 5)
                                {
                                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password to Unlock Account is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis.");

                                }
                                string[] spl = user.Email.Split('@');
                                int lenthusername = (spl[0].Length / 2);
                                int mod = spl[0].Length % 2;
                                if (mod > 0)
                                {
                                    lenthusername = lenthusername - mod;
                                }
                                string beforemanupilatedemail = "";
                                if (lenthusername == 1)
                                {
                                    beforemanupilatedemail = spl[0].Substring(lenthusername);
                                }
                                else
                                {
                                    beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
                                }
                                string appendstar = "";
                                for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
                                {
                                    appendstar += "*";
                                }
                                string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

                                string[] spl1 = spl[1].Split('.');
                                int lenthatname = (spl1[0].Length / 2);
                                int modat = spl1[0].Length % 2;
                                if (modat > 0)
                                {
                                    lenthatname = lenthatname - modat;
                                }
                                string beforatemail = "";
                                if (lenthatname == 1)
                                {
                                    beforatemail = spl1[0].Substring(lenthatname);
                                }
                                else
                                {
                                    beforatemail = spl1[0].Substring(lenthatname + 1);
                                }
                                string appendstar1 = "";
                                for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
                                {
                                    appendstar1 += "*";
                                }
                                string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
                                string emailid = manupilatedemail;

                                DateTime NewTime = DateTime.Now.AddMinutes(30);
                                Time.Text = NewTime.Hour + ":" + NewTime.Minute + NewTime.ToString(" tt");
                                email.Text = user.Email.Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);
                            }
                            catch (Exception ex)
                            {                              
                                Success = false;
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            if (OTPresult && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                            {
                                try
                                {
                                    var data = VerifyOTPManagement.GetSMSConfiguration("AvacomUnlock");
                                    if (data != null)
                                    {                                        
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password to Unlock Account is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                    }
                                    else
                                    {
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password to Unlock Account is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161891649452292");
                                    }
                                    
                                    //For Mobile Number Logic Set.
                                    string s = Convert.ToString(user.ContactNumber);
                                    int l = s.Length;
                                    s = s.Substring(l - 4);
                                    string r = new string('*', l - 4);
                                    //r = r + s;
                                    mobileno.Text = r + s;

                                    Success = true;
                                }
                                catch (Exception ex)
                                {                                  
                                    Success = false;
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                            }

                            if (!Success)
                            {
                                var AdminList = UserManagement.GetCompanyAdminByCustID(user.CustomerID);
                                message = SendNotificationEmail(user.Email, Convert.ToInt32(user.CustomerID));
                                EmailManager.SendMail(SenderEmailAddress, AdminList, new List<String>(new String[] { user.Email }), null, "Unlock Account Request", message);

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "You don't seem to have a registered email/mobile number with us." +
                                    " Your unlock account request forwarded to administrator." + " Please update your mobile number to use this feature in future."; /*Hence this authentication is not available to you right now.*/
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                            }
                            else
                            {
                                tblResetPasswordQuestion.Visible = false;
                                divResetPasswordSubmit.Visible = false;
                                divOTPVerify.Visible = true;
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForSuccess();", true);
                            }
                        }
                    }
                    else
                    {
                        
                        string scriptText = "alert('Your Account is Active !!!! Please Login with your login credentials.'); window.location='" + ConfigurationManager.AppSettings["LoginURL"].ToString() + "/Login.aspx'";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage", scriptText, true);                       
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Incorrect Email/ Email is not registered with us.";
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnOTP_Click(object sender, EventArgs e)
        {
            try
            {
                String message = String.Empty;
                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                if (txtOTP.Text != "")
                {
                    int i;
                    if (int.TryParse(txtOTP.Text, out i))
                    {
                        if (!string.IsNullOrEmpty(Session["userID"].ToString()))
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

                            if (VerifyOTPManagement.Exists(Convert.ToInt32(txtOTP.Text), Convert.ToInt32(Session["userID"].ToString())))
                            {
                                int UserId;
                                UserId = int.Parse(Session["userID"].ToString());

                                VerifyOTP OTPData = VerifyOTPManagement.GetByID(UserId);

                                //int a = Convert.ToInt32(DateTime.Now.ToString("hhmm"));
                                //int b = Convert.ToInt32(OTPData.CreatedOn.Value.ToString("hhmm"));

                                //int c = a - b;
                                //if (c <= 30)
                                //{

                                TimeSpan span = DateTime.Now.Subtract(OTPData.CreatedOn.Value);
                                if (0 <= span.Minutes && span.Minutes <= 30)
                                {
                                    VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(Session["userID"].ToString()), Convert.ToInt32(txtOTP.Text));

                                    //SetResetPasswordRandomQuestion();
                                    UserManagement.WrongAttemptCountUpdate(Convert.ToInt32(Session["userID"].ToString()));
                                    UserManagementRisk.WrongAttemptCountUpdate(Convert.ToInt32(Session["userID"].ToString()));
                                    string scriptText = "alert('Account Unlocked Successfully!!!!'); window.location='" + ConfigurationManager.AppSettings["LoginURL"].ToString() + "/Login.aspx'";

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage", "settracknewForSuccess();", true);
                                    Session["userID"] = null;
                                    FormsAuthentication.SignOut();
                                    Session.Abandon();
                                    tblResetPasswordQuestion.Visible = false;
                                    divResetPasswordSubmit.Visible = false;
                                    divOTPVerify.Visible = false;
                                    //Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                }
                                else
                                {
                                    var AdminList = UserManagement.GetCompanyAdminByCustID(user.CustomerID);
                                    message = SendNotificationEmail(user.Email, Convert.ToInt32(user.CustomerID));
                                    EmailManager.SendMail(SenderEmailAddress, AdminList, new List<String>(new String[] { user.Email }), null, "Unlock Account Request", message);

                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Contact Administrator to Unlock the Account.";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage", "settracknewForFailed();", true);

                                    return;
                                }
                            }
                            else
                            {
                                if (WrongOTPCount < 3)
                                {
                                    cvDuplicateEntry.ErrorMessage = "Invalid OTP, Please Enter Again.";
                                    cvDuplicateEntry.IsValid = false;
                                    WrongOTPCount++;
                                    return;
                                }
                                else
                                {
                                    var AdminList = UserManagement.GetCompanyAdminByCustID(user.CustomerID);
                                    message = SendNotificationEmail(user.Email, Convert.ToInt32(user.CustomerID));
                                    EmailManager.SendMail(SenderEmailAddress, AdminList, new List<String>(new String[] { user.Email }), null, "Unlock Account Request", message);

                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Contact Administrator to Unlock the Account.";
                                    return;
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "OTP has numbers only. Try Again.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "Required OTP.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        //protected void btnOTP_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        String message = String.Empty;
        //        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

        //        if (txtOTP.Text != "")
        //        {
        //            if (!string.IsNullOrEmpty(Session["userID"].ToString()))
        //            {
        //                User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

        //                if (VerifyOTPManagement.Exists(Convert.ToInt32(txtOTP.Text), Convert.ToInt32(Session["userID"].ToString())))
        //                {
        //                    int UserId;
        //                    UserId = int.Parse(Session["userID"].ToString());

        //                    VerifyOTP OTPData = VerifyOTPManagement.GetByID(UserId);

        //                    int a = Convert.ToInt32(DateTime.Now.ToString("hhmm"));
        //                    int b = Convert.ToInt32(OTPData.CreatedOn.Value.ToString("hhmm"));

        //                    int c = a - b;
        //                    if (c <= 30)
        //                    {
        //                        VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(Session["userID"].ToString()), Convert.ToInt32(txtOTP.Text));

        //                        SetResetPasswordRandomQuestion();

        //                        tblResetPasswordQuestion.Visible = true;
        //                        divResetPasswordSubmit.Visible = false;
        //                        divOTPVerify.Visible = false;
        //                        //Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
        //                    }
        //                    else
        //                    {
        //                        var AdminList = UserManagement.GetCompanyAdminByCustID(user.CustomerID);
        //                        message = SendNotificationEmail(user.Email);
        //                        EmailManager.SendMail(SenderEmailAddress, AdminList, new List<String>(new String[] { user.Email }), null, "Unlock Account Request", message);

        //                        cvDuplicateEntry.IsValid = false;
        //                        cvDuplicateEntry.ErrorMessage = "Please Contact Administrator to Unlock the Account.";
        //                        return;
        //                    }
        //                }
        //                else
        //                {
        //                    if (WrongOTPCount < 3)
        //                    {
        //                        cvDuplicateEntry.ErrorMessage = "Invalid OTP, Please Enter Again.";
        //                        cvDuplicateEntry.IsValid = false;
        //                        WrongOTPCount++;
        //                        return;
        //                    }
        //                    else
        //                    {
        //                        var AdminList = UserManagement.GetCompanyAdminByCustID(user.CustomerID);
        //                        message = SendNotificationEmail(user.Email);
        //                        EmailManager.SendMail(SenderEmailAddress, AdminList, new List<String>(new String[] { user.Email }), null, "Unlock Account Request", message);

        //                        cvDuplicateEntry.IsValid = false;
        //                        cvDuplicateEntry.ErrorMessage = "Please Contact Administrator to Unlock the Account.";
        //                        return;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again.";
        //                cvDuplicateEntry.IsValid = false;
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            cvDuplicateEntry.ErrorMessage = "Required OTP.";
        //            cvDuplicateEntry.IsValid = false;
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again.";
        //    }
        //}

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                txtRPQustion1.BorderColor = Color.Empty;
                txtRPQustion2.BorderColor = Color.Empty;

                if (Convert.ToString(ViewState["userID"]) != null)
                {
                    if (Convert.ToString(ViewState["userID"]) != "")
                    {
                        long userID = Convert.ToInt64(ViewState["userID"]);
                        List<SecurityQuestionAnswar> questionList = new List<SecurityQuestionAnswar>();
                        SecurityQuestionAnswar question1 = new SecurityQuestionAnswar();
                        question1.SQID = Convert.ToInt32(lblRPQustion1ID.Text.Trim());
                        question1.Answar = txtRPQustion1.Text.Trim();
                        question1.UserID = userID;
                        questionList.Add(question1);

                        SecurityQuestionAnswar question2 = new SecurityQuestionAnswar();
                        question2.SQID = Convert.ToInt32(lblRPQustion2ID.Text);
                        question2.Answar = txtRPQustion2.Text.Trim();
                        question2.UserID = userID;
                        questionList.Add(question2);

                        int flag = UserManagement.ValidateQuestions(questionList);

                        if (flag == 0)
                        {
                            UserManagement.WrongAttemptCountUpdate(userID);
                            UserManagementRisk.WrongAttemptCountUpdate(userID);
                            string scriptText = "alert('Account Unlocked Successfully!!!!'); window.location='" + ConfigurationManager.AppSettings["LoginURL"].ToString() + "/Login.aspx'";                            
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage", scriptText, true);
                            Session["userID"] = null;
                            FormsAuthentication.SignOut();
                            Session.Abandon();
                        }

                        else
                        {
                            if (flag == 1)
                                txtRPQustion1.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                            else if (flag == 2)
                                txtRPQustion2.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                            else
                                txtRPQustion2.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please enter valid answers.";
                        }
                    }
                }              

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnResetPasswordClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtRPQustion1.Text = string.Empty;
                txtRPQustion2.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnklogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/login.aspx", false);
        }

        private void SetResetPasswordRandomQuestion()
        {
            try
            {
                User user = UserManagement.GetByUsername(txtUnlockAccountUserID.Text.Trim());

                if (user != null)
                {
                    ViewState["userID"] = null;
                    ViewState["userID"] = user.ID;

                    var randomQuestion = UserManagement.GetRandomQuestions(user.ID);

                    if (randomQuestion.Count > 0)
                    {                       
                        lblRPQustion1ID.Text = Convert.ToString(randomQuestion[0].ID);
                        lblRPQustion1.Text = randomQuestion[0].Question;
                        lblRPQustion2ID.Text = Convert.ToString(randomQuestion[1].ID);
                        lblRPQustion2.Text = randomQuestion[1].Question;
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "You are the guest user or you did not set your security questions. Please contact your company admin to unlock the account or reset password.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Enter valid emailID.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        private string SendNotificationEmail(String Email,int CustID)
        {
            try
            {              
                string ReplyEmailAddressName = "Avantis";
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(CustID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UnlockAccountRequest
                                        .Replace("@Username", Email)
                                        .Replace("@User", "Admin")
                                        .Replace("@PortalURL", Convert.ToString(portalURL))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(portalURL));

                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UnlockAccountRequest
                //                        .Replace("@Username", Email)
                //                        .Replace("@User", "Admin")
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))                                        
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                return message;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }
    }
}