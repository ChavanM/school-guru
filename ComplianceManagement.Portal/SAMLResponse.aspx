﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SAMLResponse.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.SAMLResponse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />

    <title>Login :: AVANTIS - Products that simplify</title>  
    <!-- Bootstrap CSS -->
     <link href="NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="NewCSS/bootstrap-theme.css" rel="stylesheet" />             
    <!--external css-->
    <!-- font icon -->
    <link href="NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="NewCSS/style.css" rel="stylesheet" />
    <link href="NewCSS/style-responsive.css" rel="stylesheet" />
    <style type="text/css">
        .otpdiv > span > label {
            border: 0px;
            color: black;
            /* margin-bottom: 5px; */
            margin-left: -17px;
            margin-top: 2px;
            position: absolute;
            width: 111px;

        }
      .otpdiv > span  {
            width: 233px;
            height: 26px;
            margin: auto;
            position: relative;
            /* background: #fff; */
            /* border: 1px solid #2e2e2e; */
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
        }
        .otpdiv > span > input {
            width: 80px;
            height: 26px;
            margin: auto;
            position: relative;           
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            margin-left: -9px;
        }
    </style>
    
    
</head>
<body>
    <div class="container">
        <%--<form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">--%>
            <form runat="server" class="login-form" name="login">
            

                <asp:ScriptManager ID="ScriptManager2" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">  <a href="https://tlconnect.teamlease.com">
                                <img src="Images/TeamLease.png" /></a>
                            </p>
                        </div>

                        <div class="login-wrap">
                            <div id="divLogin" class="row" runat="server">
                                <h1></h1>

                                <div class="clearfix"></div>

                                <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" />
                                <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" />
                                
                            </div>
                            <div class="clearfix" style="height: 10px"></div>    
                             </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <script type="text/javascript" src="Newjs/jquery.js"></script>
    <script type='text/javascript' src="Newjs/bootstrap.min.js"></script>   
</body>
</html>