﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCS_VendorAuditMyCalenderData : System.Web.UI.Page
    {
        public static string date = "";
        public static string Overviewflag = "";
        public List<string> AuditPeriod = new List<string>();
        public static int CustID;
        public static int UserID;
        public static int Type;
        public static string Overdue;
        public static List<long> locationList1 = new List<long>();
        Boolean flag = true;
        protected string UsrRole;
        protected static List<RLCS_VendorAuditScheduleOn> RLCS_VendorAuditScheduleOn1 = null;
        protected static List<RLCS_VendorAuditAssignment> RLCS_VendorAuditAssignment1 = null;
        //protected static User userdetail1 = null;
        //protected static List<Role> role1 = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["date"] != "undefined" && Request.QueryString["date"] != null)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["date"])))
                {
                    if (!Convert.ToString(Request.QueryString["date"]).Equals("null"))
                    {
                        date = Convert.ToDateTime(Request.QueryString["date"]).ToString("yyyy-dd-MM");
                        DateTime dte = Business.DateTimeExtensions.GetDateCalender(date);
                    }
                }

            }
            if (Request.QueryString["AuditType"] != "undefined" && Request.QueryString["AuditType"] != null)
            {
                if (!Convert.ToString(Request.QueryString["AuditType"]).Equals("null"))
                    Type = Convert.ToInt32(Request.QueryString["AuditType"]);
            }
            if (Request.QueryString["overdue"] != "undefined" && Request.QueryString["overdue"] != null)
            {
                if (!Convert.ToString(Request.QueryString["overdue"]).Equals("null"))
                {
                    Overdue = Convert.ToString(Request.QueryString["overdue"]);
                }
            }

            if (!IsPostBack)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    RLCS_VendorAuditScheduleOn1 = (from row in entities.RLCS_VendorAuditScheduleOn
                                                   select row).ToList();

                    RLCS_VendorAuditAssignment1 = (from row in entities.RLCS_VendorAuditAssignment
                                                   where row.UserID == AuthenticationHelper.UserID
                                                   select row).ToList();

                    //userdetail1 = UserManagement.GetByID(AuthenticationHelper.UserID);

                    //role1 = (from row in entities.Roles
                    //         select row).ToList();

                }
                tvFilterLocation.Nodes.Clear();
                locationList1.Clear();
                if (flag == true)
                {
                    Overviewflag = "Managment";
                    performerCalender.Attributes.Remove("class");
                    performerCalender.Attributes.Add("class", "tab-pane active");
                }
                UserID = AuthenticationHelper.UserID;
                CustID = (int)AuthenticationHelper.CustomerID;
                //if (userdetail1 != null)
                //{
                //    UsrRole = role1.Where(x => x.ID == (int)userdetail1.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                //    //UsrRole = RoleManagement.GetByID((int)userdetail1.VendorRoleID).Code;
                //}
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    UsrRole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                }
                flag = false;
                BindCustomerFilter();

                BindAuditPeriodFilter(CustID);
                if (CustID > 0)
                    BindDataPerformer();

                if (string.IsNullOrEmpty(Session["TotalComplianceRows"] as string))
                {
                    if (Session["TotalComplianceRows"] != null && Session["TotalComplianceRows"] as string != "")
                    {
                        TotalRows.Value = Session["TotalComplianceRows"].ToString();
                        GetPageDisplaySummary();
                    }
                }
               
            }
        }           
            
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {            
            string user_Roles = AuthenticationHelper.Role;
            var role = string.Empty;
            long customerID = -1;            
            if (user_Roles.Contains("HVADM"))
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlCustomer.SelectedValue);
                    CustID = (int)customerID;                    
                }
                role = user_Roles.Trim();
            }
            else
            {
                //if (userdetail1 != null)
                //{
                //    role = role1.Where(x => x.ID == (int)userdetail1.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                //    //role = RoleManagement.GetByID((int)userdetail1.VendorRoleID).Code;
                //}

                var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                role = RoleManagement.GetByID((int)user.VendorRoleID).Code;

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlCustomer.SelectedValue);
                    CustID = (int)customerID;                    
                }
            }
            if (customerID != 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetVendorAuditAssignedLocationBranches((int)customerID, AuthenticationHelper.UserID, role)).ToList();
                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            List<int> assignedbranchIDs = new List<int>();
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                             BindLocationFilter(assignedbranchIDs, (int)customerID);

                        }
                    }
                }
            }
         
            BindAuditPeriodFilter(Convert.ToInt32(CustID));        
            ForceCloseFilterBranchesTreeView();
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ForceCloseFilterBranchesTreeView();
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();
                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalComplianceRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / 10;
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % 10;
                if (pageItemRemain > 0)
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
       
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                performerCalender.Attributes.Remove("class");
                performerCalender.Attributes.Add("class", "tab-pane active");               
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }
                grdCompliancePerformer.PageSize = 10;
                grdCompliancePerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindDataPerformer();
                ForceCloseFilterBranchesTreeView();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "BindControls();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                performerCalender.Attributes.Remove("class");
                performerCalender.Attributes.Add("class", "tab-pane active");             
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }
                grdCompliancePerformer.PageSize = 10;
                grdCompliancePerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindDataPerformer();
                ForceCloseFilterBranchesTreeView();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "BindControls();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void grdCompliancePerformer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliancePerformer.PageIndex = e.NewPageIndex;
                BindDataPerformer();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindDataPerformer()
        {
            try
            {
                Session["TotalComplianceRows"] = 0;
                int customerid = 0;
                string user_Roles = AuthenticationHelper.Role;
                var role = string.Empty;

                int userid = AuthenticationHelper.UserID;
                if (user_Roles.Contains("HVADM"))
                {
                    role = user_Roles.Trim();
                }
                else
                {
                    //if (userdetail1 != null)
                    //{
                    //    role = role1.Where(x => x.ID == (int)userdetail1.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                    //    //role = RoleManagement.GetByID((int)userdetail1.VendorRoleID).Code;
                    //}
                    var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                }
                List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result> AuditDetails = new List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>();


                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    AuditDetails = fetchDataManagement(Convert.ToInt32(ddlCustomer.SelectedValue), userid, role, ddlPeriod.SelectedItem.Text);

                }
                else
                {
                    customerid = (int)AuthenticationHelper.CustomerID;
                    AuditDetails = fetchDataManagement(Convert.ToInt32(customerid), userid, role, ddlPeriod.SelectedItem.Text);

                }
                if (!(string.IsNullOrEmpty(ddlPeriod.SelectedValue)))
                {
                    AuditDetails = AuditDetails.Where(x => x.ForMonth.Trim().ToUpper() == ddlPeriod.SelectedValue.Trim().ToUpper()).ToList();
                }
                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                if (locationList1 != null && locationList1.Count > 0)
                {
                    AuditDetails = AuditDetails.Where(x => locationList1.Contains((int)x.BranchID)).Distinct().ToList();
                }
                if (Type > 0)
                {
                    if (Type == 1)
                        AuditDetails = AuditDetails.Where(x => (x.AuditStatusID == 1 || x.AuditStatusID == 2)).Distinct().ToList();
                    else
                        AuditDetails = AuditDetails.Where(x => x.AuditStatusID == Type).Distinct().ToList();
                }

                if (!string.IsNullOrEmpty(date))
                {

                    DateTime dte = Business.DateTimeExtensions.GetDateCalender(date);

                    //DateTime now = DateTime.Now;
                    //var startDate = new DateTime(now.Year, now.Month, 1);
                    //if (dte != startDate)
                    //{
                    var Assigned = (from dts in AuditDetails
                                    where dts.StartDate == dte
                                    select dts).Distinct().ToList();


                    grdCompliancePerformer.DataSource = Assigned.OrderByDescending(x => x.AuditScheduleOnID).ToList();                   
                    Session["TotalComplianceRows"] = Assigned.Count;
                    Session["grdCalendardetails"] = null;
                    Session["grdCalendardetails"] = (grdCompliancePerformer.DataSource as List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>).ToList();
                    grdCompliancePerformer.DataBind();
                    date = null;
                    Type = 0;
                    Overdue = null;
                    //}
                    //else
                    //{
                    //grdCompliancePerformer.DataSource = null;
                    //    grdCompliancePerformer.DataSource = AuditDetails;
                    //    Session["TotalComplianceRows"] = AuditDetails.Count;
                    //    grdCompliancePerformer.DataBind();
                    //    date = null;
                    //}
                }
                else
                {
                    if (Type > 0)
                    {
                        if (!String.IsNullOrEmpty(Overdue))
                        {
                            AuditDetails = AuditDetails.Where(x => (x.AuditStatusID == 1 || x.AuditStatusID == 2) && x.StartDate < DateTime.Now).ToList();
                            grdCompliancePerformer.DataSource = AuditDetails.OrderByDescending(x=>x.AuditScheduleOnID).ToList();
                            Session["TotalComplianceRows"] = AuditDetails.Count;
                            Session["grdCalendardetails"] = null;
                            Session["grdCalendardetails"] = (grdCompliancePerformer.DataSource as List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>).ToList();
                            grdCompliancePerformer.DataBind();
                            date = null;
                            Overdue = null;
                            Type = 0;
                        }
                        else
                        {
                            if (Type == 1)
                            {
                                DateTime EndMonthDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                                           DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

                                //var test = AuditDetails.Where(x => x.AuditScheduleOnID == null);

                                
                                AuditDetails = AuditDetails.Where(x => (x.AuditStatusID == 1 || x.AuditStatusID == 2) && x.StartDate <= EndMonthDate).ToList();
                                grdCompliancePerformer.DataSource = AuditDetails.OrderByDescending(x=>x.AuditScheduleOnID).ToList();
                                Session["TotalComplianceRows"] = AuditDetails.Count;
                                Session["grdCalendardetails"] = null;
                                Session["grdCalendardetails"] = (grdCompliancePerformer.DataSource as List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>).ToList();
                                grdCompliancePerformer.DataBind();
                                date = null;
                                Type = 0;
                                Overdue = null;
                            }
                            else
                            {
                                AuditDetails = AuditDetails.Where(x => x.AuditStatusID == Type).ToList();
                                grdCompliancePerformer.DataSource = AuditDetails.OrderByDescending(x=>x.AuditScheduleOnID).ToList();
                                Session["TotalComplianceRows"] = AuditDetails.Count;
                                Session["grdCalendardetails"] = null;
                                Session["grdCalendardetails"] = (grdCompliancePerformer.DataSource as List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>).ToList();
                                //Session["grdCalendardetails"] = AuditDetails.OrderByDescending(x => x.AuditScheduleOnID);
                                grdCompliancePerformer.DataBind();
                                date = null;
                                Overdue = null;
                                Type = 0;
                            }
                        }

                    }
                    else
                    {
                       
                        grdCompliancePerformer.DataSource = AuditDetails.OrderByDescending(x => x.AuditScheduleOnID).ToList();
                        Session["TotalComplianceRows"] = AuditDetails.Count;
                        Session["grdCalendardetails"] = null;
                        Session["grdCalendardetails"] = (grdCompliancePerformer.DataSource as List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>).ToList();
                        grdCompliancePerformer.DataBind();
                        date = null;
                        Overdue = null;
                        Type = 0;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        public List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result> fetchDataManagement(int customerid, int userid, string role, string Period = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty(date))
                {

                    DateTime dte = Business.DateTimeExtensions.GetDateCalender(date);
                }

                string Type = Request.QueryString["Type"].ToString();
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }

                #region Statutory
                entities.Database.CommandTimeout = 180;
                //var mgmtQueryStatutory = (from row in entities.SP_RLCS_ComplianceInstanceTransactionViewCustomerWiseManagement(customerid, userid,"7613")
                //                          select row).ToList();
                string vrole = Convert.ToString(ViewState["vrole"]);

                var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(customerid, userid, role)
                                          select row).ToList();

                if (role == "HVEND")
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.Where(x => (x.AuditStatusID == 1 || x.AuditStatusID == 4 || x.AuditStatusID == 2 || x.AuditStatusID == 5) && x.IsOffline == false).ToList();
                }
                else
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.Where(x => x.AuditStatusID == 1 || x.AuditStatusID == 4 || x.AuditStatusID == 2 || x.AuditStatusID == 5).ToList();                //var result =  from m in mgmtQueryStatutory join CB in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                }
                

                //if (mgmtQueryStatutory.Count > 0)
                //{
                //    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.StartDate).Select(entity => entity.FirstOrDefault()).ToList();

                //}

                #endregion


                if (mgmtQueryStatutory.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Period))
                        mgmtQueryStatutory = mgmtQueryStatutory.Where(x => x.ForMonth.Trim().ToUpper() == Period.Trim().ToUpper()).ToList();

                }



                return mgmtQueryStatutory;
            }
        }
        public DataTable fetchDataReviewer(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = Business.DateTimeExtensions.GetDateCalender(date);
                string Type = Request.QueryString["Type"].ToString();
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    //QueryStatutory= QueryStatutory.Where(entry=>entry.RoleID == 4 && entry.ScheduledOn == dt).ToList();
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 4
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 4 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();

                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }

                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
                return table;
            }
        }
        protected void grdCompliancePerformer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Label lblAuditID = (Label)e.Row.FindControl("lblAuditID");
                        Label lblAuditStatusID = (Label)e.Row.FindControl("lblAuditStatusID");
                        Label lblAuditScheduleOnID = (Label)e.Row.FindControl("lblAuditScheduleOnID");

                        Label lblInvoice = (Label)e.Row.FindControl("lblInvoice");
                        Label lblPO = (Label)e.Row.FindControl("lblPO");

                        if (lblAuditID != null && lblAuditScheduleOnID != null)
                        {
                            var auditid = Convert.ToInt64(lblAuditID.Text);
                            var ScheduleOnID = Convert.ToInt64(lblAuditScheduleOnID.Text);

                            var roleID = (from row in RLCS_VendorAuditAssignment1
                                          where row.UserID == AuthenticationHelper.UserID
                                                        && row.AuditId == auditid
                                          select row.RoleID).FirstOrDefault();

                            //var roleID = (from row in entities.RLCS_VendorAuditAssignment
                            //              where row.UserID == AuthenticationHelper.UserID
                            //              && row.AuditId == auditid
                            //              select row.RoleID).FirstOrDefault();

                            LinkButton btnsave = (LinkButton)e.Row.FindControl("btnChangeStatus");
                            LinkButton btnReassign = (LinkButton)e.Row.FindControl("lnkReassignAudit");
                            LinkButton lnksave = (LinkButton)e.Row.FindControl("lnkSave");
                            TextBox txtEndDate = (TextBox)e.Row.FindControl("txtEndDate");
                            LinkButton lnkCancel = (LinkButton)e.Row.FindControl("lnkCancel");

                            var checkExist = (from row in RLCS_VendorAuditScheduleOn1
                                              where row.AuditID == auditid
                                              && row.ID == ScheduleOnID
                                             // && row.IsActive == true
                                              select row).FirstOrDefault();
                            if (checkExist != null && !string.IsNullOrEmpty(checkExist.InVoiceNo))
                            {
                                lblInvoice.Text = checkExist.InVoiceNo;
                                lblInvoice.ToolTip = lblInvoice.Text;
                            }

                            if (checkExist != null && !string.IsNullOrEmpty(checkExist.PONO))
                            {
                                lblPO.Text = checkExist.PONO;
                                lblPO.ToolTip = lblPO.Text;
                            }

                            UserID = AuthenticationHelper.UserID;
                            User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                            if (LoggedUser != null)
                            {
                                UsrRole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                            }

                            //if (userdetail1 != null)
                            //{
                            //    UsrRole = role1.Where(x => x.ID == (int)userdetail1.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                            //    //UsrRole = RoleManagement.GetByID((int)userdetail1.VendorRoleID).Code;
                            //}
                            if (UsrRole== "HVAUD" && ( Convert.ToInt32(lblAuditStatusID.Text.Trim())==1 || Convert.ToInt32(lblAuditStatusID.Text.Trim()) == 2)) //HVEND
                            {
                                btnReassign.Visible = true;
                            }
                            else
                            {
                                btnReassign.Visible = false;
                            }

                            if (roleID == 3)
                            {
                                btnsave.Visible = true;
                                txtEndDate.Enabled = false;
                                lnksave.Visible = false;
                                lnkCancel.Visible = false;
                            }
                            else
                            {
                                //var checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                //                  where row.AuditID == auditid
                                //                  && row.ID == ScheduleOnID
                                //                  && row.IsActive == true
                                //                  select row).FirstOrDefault();
                                if (checkExist != null)
                                {
                                    if (checkExist.AuditStatusID == 4)
                                    {
                                        lnksave.Visible = false;
                                        lnkCancel.Visible = false;
                                    }
                                }

                                if (!string.IsNullOrEmpty(txtEndDate.Text))
                                {
                                    btnsave.Visible = true;
                                }
                                else
                                {
                                    btnsave.Visible = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
            
        }
      
        protected bool CanChangeStatus(long userID, int RoleID, int statusID, string Type)
        {
            try
            {
                bool result = false;
                if (RoleID == 3 && (Type == "Statutory" || Type == "Event Based"))
                {
                    result = CanChangePerformerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 4 && (Type == "Statutory" || Type == "Event Based"))
                {
                    result = CanChangeReviewerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Statutory CheckList")
                {
                    result = CanChangePerformerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Internal")
                {
                    result = CanChangeInternalPerformerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 4 && Type == "Internal")
                {
                    result = CanChangeInternalReviewerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Internal Checklist")
                {
                    result = CanChangeInternalPerformerStatus(userID, RoleID, statusID);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        protected bool CanChangeReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11 || statusID == 12;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        protected bool CanChangeInternalPerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 10;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        protected bool CanChangeInternalReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        protected bool CanChangePerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 12;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        private void BindCustomerFilter()
        {
            string user_Roles = AuthenticationHelper.Role;
            var role = string.Empty;
            if (user_Roles.Contains("HVADM"))
            {
                role = user_Roles.Trim();
            }
            else
            {
                //if (userdetail1 != null)
                //{
                //    role = role1.Where(x => x.ID == (int)userdetail1.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                //    //role = RoleManagement.GetByID((int)userdetail1.VendorRoleID).Code;
                //}
                var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
            }
            //var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);

            string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
            if (CheckContractCustomer == "True")
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAllClientWise(AuthenticationHelper.UserID, role, AuthenticationHelper.CustomerID);

                ddlCustomer.DataSource = lstCustomers;
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataBind();
            }
            else
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);

                ddlCustomer.DataSource = lstCustomers;
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataBind();
            }
            //ddlCustomer.DataTextField = "Name";
            //ddlCustomer.DataValueField = "ID";
            //ddlCustomer.DataSource = lstCustomers;
            //ddlCustomer.DataBind();
        }
        private void BindAuditPeriodFilter(int custID)
        {
            string user_Roles = AuthenticationHelper.Role;
            var role = string.Empty;
            if (user_Roles.Contains("HVADM"))
            {
                role = user_Roles.Trim();
            }
            else
            {
                //if (userdetail1 != null)
                //{
                //    role = role1.Where(x => x.ID == (int)userdetail1.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                //    //role = RoleManagement.GetByID((int)userdetail1.VendorRoleID).Code;
                //}
                var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
            }
            var lstCustomers = GetAuditPeriod(CustID, UserID, role);           
            ddlPeriod.DataSource = lstCustomers.Distinct().ToList();
            ddlPeriod.DataBind();
        }
        public List<string> GetAuditPeriod(int custID, int UserID, string role)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(Convert.ToInt32(ddlCustomer.SelectedValue), UserID, role)
                                              select row).ToList();
                    if (mgmtQueryStatutory.Count > 0)
                    {
                        //mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.StartDate).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                    AuditPeriod = mgmtQueryStatutory.Select(x => x.ForMonth).Distinct().ToList();
                    //AuditPeriod = mgmtQueryStatutory.Select(x => x.ForMonth).ToList();

                }
                else
                {
                    var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(Convert.ToInt32(custID), UserID, role)
                                              select row).ToList();
                    //var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(Convert.ToInt32(311), UserID, role)
                    //                          select row).ToList();
                    if (mgmtQueryStatutory.Count > 0)
                    {
                        //mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.StartDate).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                    AuditPeriod = mgmtQueryStatutory.Select(x => x.ForMonth).Distinct().ToList();
                    //AuditPeriod = mgmtQueryStatutory.Select(x => x.ForMonth).ToList();
                }
            }

            return AuditPeriod;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindDataPerformer();
            GetPageDisplaySummary();
            ForceCloseFilterBranchesTreeView();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "BindControls();", true);
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
           
        }

        protected DataTable BindDatatable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("VendorName", typeof(string));
            dt.Columns.Add("Location", typeof(string));
            dt.Columns.Add("ForMonth", typeof(string));
            dt.Columns.Add("StartDate", typeof(DateTime));
            dt.Rows.Add("EndDate", typeof(DateTime));
          
            return dt;
        }

        

        DataTable GetDataTable(GridView dtg)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("VendorName", typeof(string));
            dt.Columns.Add("Location", typeof(string));
            dt.Columns.Add("ForMonth", typeof(string));
            dt.Columns.Add("StartDate", typeof(DateTime));
            dt.Columns.Add("EndDate", typeof(DateTime));
            dt.Columns.Add("ComplianceStatus", typeof(string));
            dt.Columns.Add("ScheduledID", typeof(string));
            dt.Columns.Add("Remark", typeof(string));
            foreach (TableCell cell in grdCompliancePerformer.HeaderRow.Cells)
            {
                cell.BackColor = grdCompliancePerformer.HeaderStyle.BackColor;
            }
            string vname = "", Location = "", Formonth = "", Customer = "", Status = "", ScheduledID = "", Remark=""; 
                
            DateTime? startdt = null, Enddt = null;

            var listAllgrdData = (List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>)Session["grdCalendardetails"];
            for (int i = 0; i < listAllgrdData.Count; i++)
            {
                Location = listAllgrdData[i].Location;
                vname = listAllgrdData[i].VendorName;
                startdt = listAllgrdData[i].StartDate;
                Enddt = listAllgrdData[i].EndDate;
                Formonth = listAllgrdData[i].ForMonth;
                Customer = listAllgrdData[i].Customer;
                Status = listAllgrdData[i].ComplianceStatus; 
                ScheduledID = Convert.ToString(listAllgrdData[i].AuditScheduleOnID);
                Remark = "";
                dt.Rows.Add(Customer, vname, Location, Formonth, startdt, Enddt, Status, ScheduledID, Remark);
            }

            //for (var i = 0; i < grdCompliancePerformer.Rows.Count; i++)
            //{
            //    foreach (TableCell cell in grdCompliancePerformer.Rows[i].Cells)
            //    {
            //        Location = ((Label)cell.FindControl("lbllocation")).Text;
            //        vname = ((Label)cell.FindControl("LabeVendorName")).Text;
            //        startdt = Convert.ToDateTime(((Label)cell.FindControl("lblStartDate")).Text);
            //        if (!string.IsNullOrEmpty(((TextBox)cell.FindControl("txtEndDate")).Text))
            //        {
            //            Enddt = Convert.ToDateTime(((TextBox)cell.FindControl("txtEndDate")).Text);
            //        }
            //        else
            //        {
            //            //Enddt = ;
            //        }
            //        Formonth = ((Label)cell.FindControl("lblFormMonth")).Text; 
            //        Customer = ((Label)cell.FindControl("lblCustomer")).Text;
            //        Status = ((Label)cell.FindControl("lblStatus")).Text;
            //        cell.CssClass = "textmode";
            //    }

            //    dt.Rows.Add(Customer,vname, Location, Formonth, startdt, Enddt,Status);
            //}
            return dt;
        }

        protected void lnkDownloadHearing_Click(object sender, EventArgs e)
        {
            try
            {
              
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;
                    var data = GetDataTable(grdCompliancePerformer);
                    DataView view = new System.Data.DataView(data);
                    ExcelData = view.ToTable("Selected", false, "Customer", "VendorName", "Location", "ForMonth", "StartDate", "EndDate", "ComplianceStatus", "ScheduledID","Remark");

                    //DataView view = new System.Data.DataView((DataTable)Session["grdCalendardetails"]);
                    //ExcelData = view.ToTable("Selected", false, "Customer", "VendorName", "Location", "ForMonth", "StartDate", "EndDate", "ComplianceStatus");

                    #region Lawyer Performance Report

                    exWorkSheet.Cells["A1"].LoadFromDataTable(data, true);
                    exWorkSheet.Cells["A1"].Value = "Customer Name";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                   exWorkSheet.Cells["A1"].AutoFitColumns(24);
                    exWorkSheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                  
                    exWorkSheet.Cells["B1"].Value = "VendorName";
                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].AutoFitColumns(28);
                    exWorkSheet.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["C1"].Value = "Location";
                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                   exWorkSheet.Cells["C1"].AutoFitColumns(45);
                    exWorkSheet.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["D1"].Value = "ForMonth";
                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].AutoFitColumns(22);
                    exWorkSheet.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663



                    exWorkSheet.Cells["E1"].Value = "StartDate";
                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].AutoFitColumns(22);
                    exWorkSheet.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["F1"].Value = "EndDate";
                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].AutoFitColumns(24);
                    exWorkSheet.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["G1"].Value = "Status";
                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].AutoFitColumns(24);
                    exWorkSheet.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["H1"].Value = "Scheduled ID";
                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].AutoFitColumns(24);
                    exWorkSheet.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["I1"].Value = "Remark";
                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].AutoFitColumns(24);
                    exWorkSheet.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                    #endregion

                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, count, 9])
                    {
                        col.Style.WrapText = true;
                        col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.Font.Size = 12;
                    }
                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    string locatioName = "VendorAudit-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename=VendorAudit-" + locatioName);//locatioName                
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void grdCompliancePerformer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               
                if (e.CommandName.Equals("CHANGE_EDIT"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long AuditID = Convert.ToInt64(commandArgs[0]);
                    int AuditScheduleOnID = Convert.ToInt32(commandArgs[1]);
                    string VendorName = Convert.ToString(commandArgs[2]);
                    bool IsOffline = Convert.ToBoolean(commandArgs[3]);
                    long VendorID = Convert.ToInt64(commandArgs[4]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", string.Format("Editpopup('{0}',{1},'{2}','{3}','{4}'); ", AuditID, AuditScheduleOnID, VendorName,IsOffline, VendorID), true);                    
                }
                else if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    int StatusID = RLCSVendorMastersManagement.getAuditStatus(Convert.ToInt64(commandArgs[1]), Convert.ToInt64(commandArgs[0]));
                    if (StatusID > 0)
                    {
                        if (StatusID == 1)
                        {
                            HiddenFieldAuditId.Value = Convert.ToString(commandArgs[0]);
                            HiddenFieldScheduleOnId.Value = Convert.ToString(commandArgs[1]);
                            lblLocation.Text = Convert.ToString(commandArgs[2]);
                            lblPeriod.Text = Convert.ToString(commandArgs[3]);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", string.Format("fopenpopupUpdateStatus('{0}','{1}','{2}','{3}'); ", lblLocation.Text, lblPeriod.Text, HiddenFieldAuditId.Value, HiddenFieldScheduleOnId.Value), true);                           
                        }
                        else if (StatusID == 5)
                        {
                            if (!RLCSVendorMastersManagement.getAuditRescheduleAllow(Convert.ToInt64(commandArgs[0]), Convert.ToInt64(commandArgs[1]), Convert.ToString(commandArgs[3])))
                            {
                                HiddenFieldAuditId1.Value = Convert.ToString(commandArgs[0]);
                                HiddenFieldScheduleOnId1.Value = Convert.ToString(commandArgs[1]);
                                lblLocation1.Text = Convert.ToString(commandArgs[2]);
                                lblPeriod1.Text = Convert.ToString(commandArgs[3]);

                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", string.Format("fopenpopupReschedule('{0}','{1}','{2}','{3}'); ", lblLocation1.Text, lblPeriod1.Text, HiddenFieldAuditId1.Value, HiddenFieldScheduleOnId1.Value), true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Audit Already Rescheduled for selected period.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenmsgCancelschedule();", true);
                        }
                    }
                    ForceCloseFilterBranchesTreeView();
                }
                else if (e.CommandName.Equals("CHANGE_SAVE"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long AuditID = Convert.ToInt64(commandArgs[0]);
                    int AuditScheduleOnID = Convert.ToInt32(commandArgs[1]);

                    if (AuditID > 0 && AuditScheduleOnID > 0)
                    {
                        GridViewRow gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                        TextBox txtenddate = (TextBox)gvRow.FindControl("txtEndDate");
                        Label lblstartdate = (Label)gvRow.FindControl("lblStartDate");
                        if (!string.IsNullOrEmpty(txtenddate.Text))
                        {
                            DateTime startdate = Convert.ToDateTime(lblstartdate.Text);
                            DateTime Enddate = Convert.ToDateTime(txtenddate.Text);
                            int compareValue;
                            try
                            {
                                compareValue = Enddate.CompareTo(startdate);
                            }
                            catch (ArgumentException)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 5 + ");", true);
                                return;
                            }
                            if (compareValue == -1)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 4 + ");", true);
                                return;
                            }
                            else if (compareValue == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 3 + ");", true);
                                return;
                            }
                            else
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    RLCS_VendorAuditScheduleOn updateEnddate = (from row in entities.RLCS_VendorAuditScheduleOn
                                                                                where row.ID == AuditScheduleOnID
                                                                                   && row.AuditID == AuditID
                                                                                select row).FirstOrDefault();
                                    if (updateEnddate != null)
                                    {
                                        updateEnddate.ScheduleOnEndDate = Convert.ToDateTime(txtenddate.Text);
                                        entities.SaveChanges();
                                    }
                                    long AuditorID = AuthenticationHelper.UserID;
                                    BindDataPerformer();
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 1 + ");", true);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 0 + ");", true);
                        }
                    }
                    ForceCloseFilterBranchesTreeView();
                }
                else if(e.CommandName.Equals("ReassignAudit"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long AuditID = Convert.ToInt64(commandArgs[0]);
                    string CustomerID = Convert.ToString(commandArgs[1]);
                    int AuditScheduleOnID = Convert.ToInt32(commandArgs[2]);
                    string Period = Convert.ToString(commandArgs[3]);
                    string Role = "HVAUD";
                   
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", string.Format("ReassignPopup('{0}','{1}','{2}','{3}','{4}','{5}')", AuditID, CustomerID, UserID, Role, Period, AuditScheduleOnID), true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        protected void grdCompliancePerformer_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
           
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
              
            }
        }

        private void BindLocationFilter(List<int> assignedBranchList, int custid)
        {
            try
            {
                locationList1.Clear();
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(custid);
                tvFilterLocation.Nodes.Clear();

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode();
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
           
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
             
            }
        }
        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != AuthenticationHelper.CustomerID)
                    {
                        if (!locationList1.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList1.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != AuthenticationHelper.CustomerID)
                            {
                                if (!locationList1.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList1.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        
        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
            ForceCloseFilterBranchesTreeView();
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            try
            {
                long AuditId = -1;
                if (!string.IsNullOrEmpty(HiddenFieldAuditId.Value))
                    AuditId = Convert.ToInt64(HiddenFieldAuditId.Value);

                long ScheduleOnId = -1;
                if (!string.IsNullOrEmpty(HiddenFieldScheduleOnId.Value))
                    ScheduleOnId = Convert.ToInt64(HiddenFieldScheduleOnId.Value);

                if (AuditId > 0 && ScheduleOnId > 0)
                {
                    if (RLCSVendorMastersManagement.UpdateAuditScheduleOnStatus(ScheduleOnId, AuditId, txtRemark.Text))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "FRemarkUpdate();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Audit Cancelation Save not Successfully.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                locationList1.Clear();
                upComplianceTypeList.Update();
                ForceCloseFilterBranchesTreeView();              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnClearFilters_Click(object sender, EventArgs e)
        {
            tvFilterLocation.Nodes.Clear();
            locationList1.Clear();

            ddlCustomer.SelectedIndex = -1;
            ddlPeriod.SelectedIndex = -1;
            tbxFilterLocation.Text = string.Empty;           
            btnClear1_Click(sender, e);
            ForceCloseFilterBranchesTreeView();

            BindDataPerformer();
            GetPageDisplaySummary();
        }
        public string YesNo(bool active)
        {
            return active ? "Yes" : "No";
        }
    }
}