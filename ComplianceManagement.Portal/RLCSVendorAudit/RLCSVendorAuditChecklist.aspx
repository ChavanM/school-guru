﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSVendorAuditChecklist.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSVendorAuditChecklist" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%--<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=[VersionNumber], Culture=neutral, PublicKeyToken=[TokenNumber]" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <meta name="author" content="AVANTIS - Development Team">

    <link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../newcss/spectrum.css" rel="stylesheet" />
    <script type="text/javascript" src="../../newjs/spectrum.js"></script>

    <style type="text/css">
        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .info-box:hover {
            color: #FF7473;
            font-weight: 500;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            z-index: 5;
        }
        .function-selector-radio-label {
            font-size: 12px;
        }

        .colorPickerWidget {
            padding: 10px;
            margin: 10px;
            text-align: center;
            width: 360px;
            border-radius: 5px;
            background: #fafafa;
            border: 2px solid #ddd;
        }

        #ContentPlaceHolder1_grdGradingRepportSummary.table tr td {
            border: 1px solid white;
        }


        .TMB {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 16.5% !important;
        }

        .TMB1 {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 19.5% !important;
        }

        .TMBImg {
            padding-left: 0px !important;
            padding-right: 0px !important;
            margin-left: -7%;
            margin-right: -3%;
        }

        .clscircle {
            margin-right: 7px !important;
        }

        .fixwidth {
            width: 20% !important;
        }

        .badge {
            font-size: 10px !important;
            font-weight: 200 !important;
        }

        .responsive-calendar .day {
            width: 13.7% !important;
            height: 45px;
        }

            .responsive-calendar .day.cal-header {
                border-bottom: none !important;
                width: 13.9% !important;
                font-size: 17px;
                height: 25px;
            }

        #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        .bx-viewport {
            height: 285px !important;
        }

        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .graphcmp {
            margin-left: 36%;
            font-size: 16px;
            margin-top: -3%;
            color: #666666;
            font-family: 'Roboto';
        }

        .days > div.day {
            margin: 1px;
            background: #eee;
        }

        .overdue ~ div > a {
            background: red;
        }

        .info-box {
            min-height: 95px !important;
        }

        .Dashboard-white-widget {
            padding: 5px 10px 0px !important;
        }

        .dashboardProgressbar {
            display: none;
        }

        .TMBImg > img {
            width: 47px;
        }

        #reviewersummary {
            height: 150px;
        }

        #performersummary {
            height: 150px;
        }

        #eventownersummary {
            height: 150px;
        }

        #performersummarytask {
            height: 150px;
        }

        #reviewersummarytask {
            height: 150px;
        }

        div.panel {
            margin-bottom: 12px;
        }

        .panel .panel-heading .panel-actions {
            height: 25px !important;
        }

        hr {
            margin-bottom: 8px;
        }

        .panel .panel-heading h2 {
            font-size: 18px;
        }

        td > label {
            padding: 6px;
        }

        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 20px;
        }

        span.input-group-addon {
            padding: 0px;
        }

        td > label {
            padding: 3px 4px 0 4px;
            margin-top: -1%;
        }

        .nav-tabs > li > a {
            color: #333 !important;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #1fd9e1 !important;
        }

        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .btuploadss {
            background-image: url(../../Images/icon-updated.png);
            border: 0px;
            width: 30px;
            height: 30px;
            background-color: transparent;
            background-repeat: no-repeat;
            float: left;
        }

        .btnview {
            background-image: url(../Images/view-icon-new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .btnss {
            background-image: url(../../Images/Save-icon.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        .circle {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

        .dd_chk_select {
            height: 81px !important;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
            height: 30px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        div.dd_chk_drop {
            background-color: white;
            border: 1px solid #CCCCCC;
            text-align: left;
            z-index: 1000;
            left: -1px;
            top: 28px !important;
            min-width: 100%;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function ()
        {
            debugger
            $("button[data-dismiss-modal=modal1]").click(function () {
                $('#DocumentReviewPopUp1').modal('hide');
            });
        });


        function fopendocfileReview(file)
        {
            $('#DocumentReviewPopUp1').modal('show');
            $('#ContentPlaceHolder1_docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }
        $(document).ready(function () {
             var query = window.location.search.substring(1);
             var vars = query.split("&");
             for (var i = 0; i < vars.length; i++) {
                 var pair = vars[i].split("=");
                 if (pair[0] == "Flag")
                 {
                     if (pair[1] == "byOtp") {
                         $('#main-content').css('margin-left', '10px');
                         $('.header').css('display', 'none');
                         $('#sidebar').css('display', 'none');
                         $('#timedateProgress').css('display', 'none');
                         $('#pagetype').parent('div').parent('.wrapper').parent('div.row').hide();
                         document.getElementById('AuditChecklist').style.display = 'inherit';
                     }                    
                 }
             }

             $("input[type=radio][name='SampleFormat']").change(function () {                 
                 if (<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %> == 9503) {                   
                     if (this.value == 'D') {
                         $("#lnksampledownload").attr("href", "SampleFormat/Subcontractor_Assessment.docx");
                     }
                     else if (this.value == 'I') {
                         $("#lnksampledownload").attr("href", "SampleFormat/Declaration_Indemnity.docx");
                     }
                 }
                 else
                 {
                     if (this.value == 'D') {
                         $("#lnksampledownload").attr("href", "SampleFormat/DeclarationonStatutoryLabourComplaince.docx");
                     }
                     else if (this.value == 'I') {
                         $("#lnksampledownload").attr("href", "SampleFormat/Input_Format.xlsx");
                     }
                 }
             });
             $("input[type=radio][name='SampleFormat']").trigger("change");
      });

        function showUploadDocumentModal() {
            $("#divUploadDocument").modal("show");
        }

        function gotoBack() {
            window.location.href = "../RLCSVendorAudit/RLCSVendorAuditDashboard.aspx";
        }
        $('#divShowDialog').on("show", function () {
            $(this).find(".modal-body").css("max-height", height);
        });

        $('#divShowDialog').on('show.bs.modal', function () {
            $('#divShowDialog').find('.modal-body').css({
                width: 'auto', //probably not needed
                height: 'auto', //probably not needed 
                'max-height': '100%'
            });
        });

        function openperformerpopup(CHID, AID, SOID, SID, CBID,VendorID) {
            
            var modalHeight = screen.height - 200;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "../RLCSVendorAudit/RLCS_PerofrmerTransaction.aspx?CHID=" + CHID + "&AID=" + AID + "&SOID=" + SOID + "&SID=" + SID + "&CBID=" + CBID + "&VendorID=" + VendorID);
        }

        function openreviwerpopup(CHID, AID, SOID, SID, CBID,VendorID) {
            var modalHeight = screen.height - 200;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "../RLCSVendorAudit/RLCS_ReviewerTransaction.aspx?CHID=" + CHID + "&AID=" + AID + "&SOID=" + SOID + "&SID=" + SID + "&CBID=" + CBID+ "&VendorID=" + VendorID);
        }

        function openreviwerperformerpopup(CHID, AID, SOID, SID, CBID,VendorID) {
            debugger
            var modalHeight = screen.height - 200;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "../RLCSVendorAudit/RLCS_ReviewerPerformerTransaction.aspx?CHID=" + CHID + "&AID=" + AID + "&SOID=" + SOID + "&SID=" + SID + "&CBID=" + CBID + "&VendorID=" + VendorID);
        }

         function openClosedpopup(CHID, AID, SOID, SID, CBID) {
            var modalHeight = screen.height - 200;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "../RLCSVendorAudit/RLCS_ClosedTransaction.aspx?CHID=" + CHID + "&AID=" + AID + "&SOID=" + SOID + "&SID=" + SID + "&CBID=" + CBID);
         }
        
        // function divexpandcollapse(divname) {
        //     var div = document.getElementById(divname);
        //     var img = document.getElementById('img' + divname);             
        //     if (div.style.display == "none") {
        //         div.style.display = "inline";
        //         img.src = "../Images/expand.png";
        //     } else {
        //         div.style.display = "none";
        //         img.src = "../Images/collapse.png";
        //     }
        //}

         function divexpandcollapse(divname) {  
             
             var img = "img" + divname;
             if ($("#" + img).attr("src") == "/Images/expand.png")
             {
                 $("#" + img).closest("tr").after("<tr><td></td><td colspan = '100%'>" + $("#" + divname)
                     .html() + "</td></tr>");
                 $("#" + img).attr("src", "/Images/collapse.png");                 
                 $("input", $(this).closest("tr").next()).each(function () {                     
                     this.value = this.value.substring(',', '');
                    
                 });
                
                 
             } else {
                 $("#" + img).closest("tr").next().remove();
                 $("#" + img).attr("src", "/Images/expand.png");
             }
         }
        function fclosepopup() {
            $('#divShowDialog').modal('hide');
        }
        function closeModal() {
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }
        function fopenpopup() {
            $('#divShowAddNewStepDialog').modal('show');
            return true;
        }
        function CloseWin() {
            $('#divShowAddNewStepDialog').modal('hide');
        };
        //This is used for Close Popup after button click.
        function caller() {
            setInterval(CloseWin, 30000);
        };
        function ddCountyChange(sender)
        {
            document.getElementById('ContentPlaceHolder1_setddlstatusvalue').value = sender.value;
            return false;
        }
        function ddlcheckliststatusChange(sender) {
        
            document.getElementById('ContentPlaceHolder1_setchecklistddlstatusvalue').value = sender.value;
            return false;
        }
        function refreshpage() {           
            window.location.href = window.location.pathname + window.location.search + window.location.hash;         
        }
        function Opengridplus(divname1,index) 
        {
            var divname="div" + divname1;
            var img = "img" + divname;
            if ($("#" + img).attr("src") == "/Images/expand.png")
            {
                $("#" + img).closest("tr").after("<tr><td></td><td colspan = '100%'>" + $("#" + divname)
                    .html() + "</td></tr>");
                $("#" + img).attr("src", "/Images/collapse.png");
               
                $("input", $(this).closest("tr").next()).each(function () {
               
                    this.value = this.value.substring(',', '');
                });
            } else {
                $("#" + img).closest("tr").next().remove();
                $("#" + img).attr("src", "/Images/expand.png");
            }   
          
            var remark="input#ContentPlaceHolder1_grdChecklist_txtRemark1_" + index;
            $(remark).val('');
            var Recommendation = "input#ContentPlaceHolder1_grdChecklist_txtRecommendation_" + index;
            $(Recommendation).val('');
            
            document.getElementById('ContentPlaceHolder1_setddlstatusvalue').value='';
            document.getElementById('ContentPlaceHolder1_setchecklistddlstatusvalue').value='';
            //$("input#ContentPlaceHolder1_grdChecklist_txtRemark1_0").val('');
        }
       
        var querystring = location.search;
        var IsOfflineStatus = querystring.split('=')[4];

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix" style="height: 0px"></div>
    <asp:UpdatePanel ID="Upvendorchecklist" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                            <div class="clearfix"></div>
                            <div class="panel-body">
                                <div class="col-md-12 colpadding0">
                                    <asp:Label ID="AuditChecklist" Style="color: black; font-size: 25px; font-weight: 450; display: none;">Audit Checklist</asp:Label>
                                </div>
                                 <div class="col-md-12 colpadding0">
                                    <asp:Label ID="lblAuditName" runat="server" Style="color: black; font-size: 25px; font-weight: 450;"></asp:Label>
                                </div>
                                <div class="col-md-12 colpadding0">
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0" style="padding-top: 12px;">
                                        <div class="col-md-4 colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="ddlAct"
                                                DataPlaceHolder="Select Act"
                                                Height="30px" Width="100%" class="form-control m-bot15"
                                                OnSelectedIndexChanged="ddlAct_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="col-md-4">
                                            <asp:DropDownListChosen runat="server" ID="ddlChecklistStatus"
                                                DataPlaceHolder="Select Status"
                                                Height="30px" Width="100%" class="form-control m-bot15"
                                                OnSelectedIndexChanged="ddlChecklistStatus_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="col-md-4 colpadding0" style="float: right;">
                                             <button type="button" id="btnDocumentUpload" class="btn btn-primary pull-left"   onclick="showUploadDocumentModal();">Document</button>
                                            <asp:Button ID="btnBack" CausesValidation="false" class="btn btn-search pull-right" Style="margin-left: 14px !important; float: right;" runat="server" Text="Back" OnClientClick="gotoBack()" />

                                            <asp:Button ID="btnAddNewStep" CausesValidation="false" Visible="false" class="btn btn-search" Style="margin-left: 33px !important; float: right;display:none;" runat="server" Text="Add New Step" OnClientClick="fopenpopup()" />

                                        </div>
                                    </div>

                                    <div class="clearfix">
                                    </div>
                                </div>
                                <div class="tab-content ">
                                    <div runat="server" id="performerdocuments" class="tab-pane active">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div>
                                                    <asp:ValidationSummary runat="server"
                                                        ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                        ValidationGroup="ComplianceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                                    <asp:Label runat="server" ID="Label2" ForeColor="Red"></asp:Label>
                                                </div>
                                                <div>
                                                 
                                                <asp:TextBox ID="setddlstatusvalue" runat="server" Text="" style="display:none;" ></asp:TextBox>                                                                                               
                                                <asp:TextBox ID="setchecklistddlstatusvalue" runat="server" Text="" style="display:none;" ></asp:TextBox>                                                                                               
                                                    <asp:GridView runat="server" ID="grdChecklist" AutoGenerateColumns="false" AllowSorting="true"
                                                        GridLines="None" OnRowUpdated="grdChecklist_RowUpdated" OnRowDataBound="grdChecklist_RowDataBound" CssClass="table" OnRowCommand="grdChecklist_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                <ItemTemplate>
                                                                <div runat="server"  Visible='<%# CanChangeStatus((string)Eval("Status"), (int)Eval("AuditStatusID")) %>'>

                                                                    <a href="JavaScript:divexpandcollapse
                                                                        ('div<%# Eval("Id") %>');">
                                                                        <img alt="Details" id="imgdiv<%# Eval
                                                                        ("Id") %>"
                                                                            src="/Images/expand.png" />
                                                                    </a>
                                                                    </div>
                                                                    <!-- Adding the Div container -->
                                                                    <div id="div<%# Eval("Id") %>" style="display: none;">
                                                                        <!-- Adding Child GridView -->
                                                              

                                                                  <div style="border: 1px solid #cac6c6;border-radius: 11px;padding: 10px;">
                                                                        <asp:GridView runat="server" ID="grdDocument1" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none"
                                                                            OnRowCommand="grdDocument_RowCommand1"  Width="100%" OnRowDataBound="grdDocument1_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%" HeaderStyle-Font-Bold="true">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex + 1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Document Name"  ItemStyle-Width="10%" HeaderStyle-Font-Bold="true">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 433px;">                                                                                           
                                                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Wrap="false" HeaderText="Expired On" ItemStyle-Width="2%" HeaderStyle-Font-Bold="true">
                                                                                    <ItemTemplate>
                                                                                            <asp:Label ID="txtExpire" data-toggle="tooltip" runat="server" ForeColor="Red"  visible='<%#  Convert.ToDateTime(Eval("ValidTill")) > DateTime.Now ||  Eval("ValidTill") == null?false:true%>' ToolTip='<%#  Eval("ValidTill")!= null?((DateTime)Eval("ValidTill")).ToString("dd-MMM-yyyy"):""%>' Text='<%#  Eval("ValidTill")!= null?((DateTime)Eval("ValidTill")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Document Uploaded Date" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CreatedOn")!= null?((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("CreatedOn")!= null?((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-Font-Bold="true" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="UpdatePanel23" runat="server" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton ID="lnkDownloadDocument1" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                                   visible='<%#  Convert.ToDateTime(Eval("ValidTill")) > DateTime.Now || Eval("ValidTill") == null?true:false%>'  CommandArgument='<%# Eval("Id") + "," + ((GridViewRow) Container).RowIndex %>'>
                                                                                            <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>

                                                                                                <asp:LinkButton ID="lnkViewDocument1" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                                    visible='<%#  Convert.ToDateTime(Eval("ValidTill")) > DateTime.Now || Eval("ValidTill") == null?true:false%>' CommandArgument='<%# Eval("Id") + "," + (((GridViewRow)(((Control)(sender)).Parent.BindingContainer))).RowIndex %>'>
                                                                                            <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>

                                                                                                <asp:LinkButton ID="lnkDeleteDocument1" runat="server" CommandName="Delete Document"
                                                                                                visible='<%# Eval("ValidTill")!= null?false:true%>' ToolTip="Delete Document" data-toggle="tooltip"
                                                                                                    CommandArgument='<%# Eval("Id") + "," + ((GridViewRow) Container).RowIndex %>'>
                                                                                            <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkDownloadDocument1" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <PagerSettings Visible="false" />
                                                                            <PagerTemplate>
                                                                            </PagerTemplate>
                                                                            <EmptyDataTemplate>
                                                                                No Record Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                  
                                                                        <table>
                                                                        <div runat="server" visible='<%# DisplayOnlyVendor() %>'>
                                                                            <tr class="spaceUnder">
                                                                                <td style="width: 25%;">
                                                                                    <label style="font-weight: bold; vertical-align: text-top;">Upload  Document(s)</label>
                                                                                </td>
                                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                                <td style="width: 38%;">
                                                                                    <asp:FileUpload ID="docFileUpload" multiple="multiple" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                     <asp:UpdatePanel ID="UpdatePanelamol" runat="server" UpdateMode="Conditional">
                                                                                        <ContentTemplate>
                                                                                            <asp:Button ID="UploadDocumentdetail" 
                                                                                                CommandArgument='<%# Eval("Id") + "," + ((GridViewRow) Container).RowIndex %>'
                                                                                                runat="server" CommandName="Upload_Document" Text="Upload Document" class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Ddocument"
                                                                                                CausesValidation="true" />
                                                                                            </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="UploadDocumentdetail" />                                                                                              
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                             </div>

                                                                             <tr>
                                                                               <td >
                                                                                    <label style="font-weight: bold;display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                    <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                                                </td>
                                                                                <td  font-weight: bold;">: </td>
                                                                               <td>
                                                                                   <asp:UpdatePanel ID="UpdatePanel41"  runat="server" UpdateMode="Always">
                                                                                       <ContentTemplate>
                                                                                           <asp:DropDownList runat="server" ID="ddlStatus1" onchange="ddCountyChange(this)" DataPlaceHolder="Select Status"
                                                                                               Height="30px" Width="30%" class="form-control m-bot15">
                                                                                           </asp:DropDownList>

                                                                                           <label style="font-weight: bold;margin: -33px 190px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                    <label style="font-weight: bold; margin: -33px 201px;vertical-align: text-top;">Remark:</label>
                                                                                       </ContentTemplate>
                                                                                       <Triggers>
                                                                                           <asp:AsyncPostBackTrigger ControlID="ddlstatus1" />
                                                                                       </Triggers>
                                                                                   </asp:UpdatePanel>                                                                              
                                                                               </td>
                                                                                <%--<td> 
                                                                                    <label style="display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                    <label style="font-weight: bold; vertical-align: text-top;">Remark:</label>
                                                                                </td>--%>
                                                                                
                                                                               <td>                                                                               
                                                                                   <asp:TextBox  runat="server" Height="30px" ID="txtRemark1" class="form-control" style="height:30px;float: left;"/>
                                                                               </td>
                                                                                 <td></td>
                                                                            </tr>
                                                                              <div runat="server" visible='<%# DisplayOnlyAuditor(IsOfflineStatus) %>'>
                                                                                <tr>
                                                                                    <td style="width: 25%;">
                                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                        <label style="font-weight: bold; vertical-align: text-top;">Checklist Status</label>
                                                                                    </td>
                                                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                                                    <td style="width: 38%;">
                                                                                        <asp:UpdatePanel ID="UpdatePanel3s" runat="server" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:DropDownList runat="server" ID="ddlCheckliststatus1" onchange="ddlcheckliststatusChange(this)" class="form-control m-bot15"  Height="30px" Width="100%">
                                                                                                    <asp:ListItem Text="Select Checklist status" Selected="True" Value="-1" />
                                                                                                    <asp:ListItem Text="Team Review" Value="3" />
                                                                                                    <asp:ListItem Text="Closed" Value="4" />
                                                                                                </asp:DropDownList>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:AsyncPostBackTrigger ControlID="ddlCheckliststatus1" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </td>
                                                                                    
                                                                                </tr>   
                                                                                </div> 
                                                                            <div>
                                                                                <td>
                                                                                        <%--<label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>--%>
                                                                                        <label style="font-weight: bold; vertical-align: text-top;"><span style="color: red;">*</span>Recommendation</label>
                                                                                    </td>
                                                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                                                    <td style="width: 50%;">
                                                                                        <asp:TextBox name="txtRecommendation" value="" Text="" runat="server" Height="30px" Width="100%" ID="txtRecommendation" class="form-control" />
                                                                                    </td>
                                                                            </div>                                                                      
                                                                           <tr class="spaceUnder">

                                                                             <td colspan="3">
                                                                                 <div runat="server" visible='<%# DisplayOnlyVendor() %>'>
                                                                                     <label style="font-weight: bold; vertical-align: text-top;">Please select check box If documents are not available?</label>
                                                                                     <asp:CheckBox ID="chkDocument" runat="server" />
                                                                                 </div>
                                                                                </td>
                                                                            </tr>
                                                                              <div runat="server" visible='<%# DisplayOnlyAuditor() %>'>
                                                                            <tr style="visibility:collapse">
                                                                                <td style="width: 25%;">
                                                                                    <label style="margin-left: 12px;font-weight: bold; vertical-align: text-top;">Status</label>
                                                                                </td>
                                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                                <td style="width: 38%;">
                                                                                    <asp:Label ID="ddlstatusname" runat="server" Text=""></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            </div>
                                                                              </table>
                                                                      <div class="col-lg-12 col-md-12">                                                                       
                                                                          <asp:UpdatePanel ID="UpdatePanel411" runat="server" UpdateMode="Conditional">
                                                                              <ContentTemplate>
                                                                                  <asp:Button ID="btnSave1"
                                                                                      CommandName="Save_Detail"                                                                                     
                                                                                      CommandArgument='<%# Eval("Id") + "," + ((GridViewRow) Container).RowIndex + "," + Eval("CustomerBranchID") %>'
                                                                                      class="btn btn-search"
                                                                                      runat="server" Text="Save" style="float: none;margin-left: 45%;" />
                                                                              </ContentTemplate>
                                                                              <Triggers>
                                                                                  <asp:PostBackTrigger ControlID="btnSave1" />
                                                                              </Triggers>
                                                                          </asp:UpdatePanel>
                                                                      </div>
                                                                      
                                                                        </div>
                                                               
                                                              
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr" HeaderStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <%#Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="State Name" HeaderStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                        <asp:Label ID="lblStateID" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("StateId") %>' ToolTip='<%# Eval("StateId") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Act Name" HeaderStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                        <asp:Label ID="lblAM_ActName" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("AM_ActName") %>' ToolTip='<%# Eval("AM_ActName") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Nature Of Compliance" HeaderStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblNatureOfCompliance" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("NatureOfCompliance") %>' ToolTip='<%# Eval("NatureOfCompliance") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type Of Compliance" HeaderStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblTypeOfCompliance" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("TypeOfCompliance") %>' ToolTip='<%# Eval("TypeOfCompliance") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                               <asp:TemplateField HeaderText="Risk" HeaderStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("Risk") %>' ToolTip='<%# Eval("Risk") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status" HeaderStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                        <asp:Label ID="lblCustomerbranch" runat="server" visible="false" 
                                                                            Text='<%# Eval("CustomerBranchID") %>' ToolTip='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                                         <asp:Label ID="LblId" runat="server" visible="false" 
                                                                            Text='<%# Eval("Id") %>' ToolTip='<%# Eval("Id") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Action" HeaderStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton ID="lnkEditChecklist" runat="server" CommandName="EditChecklist" ToolTip="Edit Checklist"
                                                                                data-toggle="tooltip"
                                                                                Visible='<%# CanChangeStatus((string)Eval("Status"),(int) Eval("AuditStatusID")) %>'
                                                                                CommandArgument='<%# Eval("Id") + "," + Eval("Status") +","+Eval("CustomerBranchID") %>'>
                                                                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit Checklist" /></asp:LinkButton>
                                                                               
                                                                            <asp:LinkButton ID="lnkViewChecklist" runat="server" CommandName="ClosedChecklist" ToolTip="View Checklist"
                                                                                data-toggle="tooltip"
                                                                                Visible='<%# CanChangeStatusClosed((string)Eval("Status")) %>'
                                                                                CommandArgument='<%# Eval("Id") + "," + Eval("Status") +","+Eval("CustomerBranchID") %>'>
                                                                                    <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="View Checklist" /></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="lnkEditChecklist" />
                                                                            <asp:PostBackTrigger ControlID="lnkViewChecklist" />
                                                                     <%--       <asp:PostBackTrigger ControlID="docFileUpload" />  --%>                                                                          
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Right" />
                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <PagerTemplate>
                                                            <table style="display: none">
                                                                <tr>
                                                                    <td>
                                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div style="margin-bottom: 4px; float: left;">
                                            <table width="100%" style="height: 10px">
                                                <tr>
                                                    <td colspan="2">
                                                         <asp:TextBox ID="txtObservation" runat="server" CssClass="form-control" Visible="false" TextMode="MultiLine" Style="width:250%;"  placeholder="Observation"></asp:TextBox>
                                                   
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td>&nbsp</td>
                                                      <td>&nbsp</td>
                                                </tr>
                                                <tr>
                                                     <td colspan="2">
                                                            <asp:TextBox ID="txtRecomandation" runat="server" CssClass="form-control" Visible="false" TextMode="MultiLine" Style="width:250%;" placeholder="Recommendation"></asp:TextBox>
                                                    </td>
                                                   
                                                </tr>
                                                <tr>
                                                    <td>&nbsp</td>
                                                      <td>&nbsp</td>
                                                </tr>
                                                <tr>
                                                     <td>
                                                        <asp:Button ID="btnSave" CausesValidation="false" class="btn btn-search"
                                                             runat="server" Visible="true"
                                                            Text="Save" OnClick="btnSave_Click"  />

                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSaveClose" CausesValidation="false" class="btn btn-search"
                                                             runat="server" Visible="false"
                                                            Text="Save & Close" OnClick="btnSaveClose_Click" />

                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                        </section>
                    </div>
                </div>
            </div>
            <tr id="trErrorMessage" runat="server" visible="true">
                <td colspan="3" style="background-color: #e9e1e1;">
                    <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </td>
            </tr>
            <div style="display: none;">

                <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                </asp:LinkButton>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 35px; text-align: left;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7; border-radius: 10px;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
                  <button type="button" data-dismiss="modal" hidden="hidden" id="close"></button>
            </div>
        </div>
    </div>


    <div class="modal fade" id="divShowAddNewStepDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 55%">
            <div class="modal-content" style="width: 100%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAddNewStepDialog">
                        <asp:UpdatePanel ID="upNewStep" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>                            
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" Display="none"
                                             class="alert alert-block alert-danger fade in"
                                             ValidationGroup="NewStepValidationGroup" />

                                        <asp:CustomValidator ID="cvpopup" runat="server" EnableClientScript="False"
                                            ValidationGroup="NewStepValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 90px; display: block; float: left; font-size: 13px; color: #333;">
                                            CheckList</label>
                                        <asp:DropDownCheckBoxes ID="ddlNewStepCheck" runat="server" Width="200px"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            OnSelectedIndexChanged="ddlNewStepCheck_SelcetedIndexChanged">
                                            <Style SelectBoxWidth="500" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                            <Texts SelectBoxCaption="Select Checklist" />
                                        </asp:DropDownCheckBoxes>

                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnPopupSave" OnClick="btnPopupSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="NewStepValidationGroup" />
                                        <%--<asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />--%>
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <%--<p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>--%>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divUploadDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:95%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divUpload_Document">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>                            
                                <div class="row" runat="server" id="divUploadDoc">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <input type="radio" id="sampeformart1" name="SampleFormat" value="D" checked>
                                            <label for="sampeformart1" style="color:black;">Declaration on Statutory Labour Compliance</label><br>
                                            <input type="radio" id="sampeformart2" name="SampleFormat" value="I">
                                            <label for="sampeformart2" style="color:black;">Input Form</label><br>
                                         
                                      </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                      <a id="lnksampledownload" href="">Download Sample Format </a>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <asp:FileUpload ID="MusterRollFileUpload" multiple="multiple"  runat="server" />
                                    </div>
                                     <div class="col-lg-1 col-md-1 col-sm-1">
                                          <asp:Button ID="btnMusterRoll" runat="server" Text="Upload" 
                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload"
                                                    CausesValidation="true" OnClick="btnMusterRoll_Click" />
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                        PageSize="10" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%" HeaderStyle-Font-Bold="true">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Document Uploaded Date" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CreatedOn")!= null?((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("CreatedOn")!= null?((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                <ItemTemplate>   
                                                     <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>                                        
                                                     <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>  
                                                        </ContentTemplate>
                                                        <Triggers>  
                                                            <asp:PostBackTrigger ControlID="lnkDownloadDocument" />  
                                                        </Triggers>  
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <PagerTemplate>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>  
                                <asp:PostBackTrigger ControlID="btnMusterRoll" />  
                            </Triggers>  
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal1" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">                                               
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                                </fieldset>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>  
</asp:Content>

