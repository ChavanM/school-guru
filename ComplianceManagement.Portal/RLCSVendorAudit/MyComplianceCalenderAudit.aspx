﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyComplianceCalenderAudit.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.MyComplianceCalenderAudit" %>
<link href="../NewCSS/responsive-calendar.css" rel="stylesheet" />
<script src="../Newjs/responsive-calendar.js"></script>
 
<!-- Responsive calendar - START -->
<div class="responsive-calendar" style="width: 105%" id="CalenderData">
    <div class="controls">
        <a class="pull-left" data-go="prev">
            <div class="btn btn-primary" style="line-height:19px"><</div>
        </a>
        <h4><span data-head-month></span>&nbsp;<span data-head-year></span></h4>
        <a class="pull-right" data-go="next">
            <div class="btn btn-primary" style="line-height:19px">></div>
        </a>
    </div>
    <hr  style="margin-top: 10px;margin-bottom: 0px;"/>
    <div class="day-headers">
        <div class="day cal-header">Mon</div>
        <div class="day cal-header">Tue</div>
        <div class="day cal-header">Wed</div>
        <div class="day cal-header">Thu</div>
        <div class="day cal-header">Fri</div>
        <div class="day cal-header">Sat</div>
        <div class="day cal-header">Sun</div>
    </div>
    <div class="days" data-group="days">
    </div>
    <div style="margin-bottom: 5px; margin-top: 10px;">

        <div style="background-color: #006500; margin-left: 30%; width: 30px; height: 10px; float: left" data-toggle="tooltip" data-placement="bottom" title="All compliances are completed"></div>
        <div style="background-color: #FF0000; width: 30px; margin-left: 10px; height: 10px; float: left;" data-toggle="tooltip" data-placement="bottom" title="One or more compliances are overdue"></div>
        <div style="background-color: #1d86c8; width: 30px; margin-left: 10px; height: 10px; float: left;" data-toggle="tooltip" data-placement="bottom" title="Open"></div>
        <div style="background-color: #ffcd70; width: 30px; margin-left: 10px; height: 10px; float: left;" data-toggle="tooltip" data-placement="bottom" title="One or more compliances are cancelled"></div>
    </div>
</div>

<script>
$(document).ready(function () {
           
            $(".responsive-calendar").responsiveCalendar({
                time: '<%=CalendarDate%>',
                events: {
                    <%=CalenderDateString%>
                }
            });
               setInterval(setcolor, 1000);
});
     <%--fcal('<%=CalendarTodayOrNextDate%>')--%>
 <%--   fcal('<%=CalendarDate%>')--%>
    </script>
<!-- Responsive calendar - END -->