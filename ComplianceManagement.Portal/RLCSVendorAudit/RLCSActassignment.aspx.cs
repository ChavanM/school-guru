﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSActassignment : System.Web.UI.Page
    {
        protected static string user_Roles;
        protected bool flag;
        protected static string ContractCustomer;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ContractCustomer = "False";
                    ContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                    user_Roles = AuthenticationHelper.Role;

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        [WebMethod]
        public static string BindCustomerList()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var role = string.Empty;
                    if (user_Roles.Contains("HVADM"))
                    {
                        role = user_Roles.Trim();
                    }
                    else
                    {
                        var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                        role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                    }
                    string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                    if (CheckContractCustomer == "True")
                    {
                        var lstCustomers = RLCSVendorMastersManagement.GetAllClientWise(AuthenticationHelper.UserID, role, AuthenticationHelper.CustomerID);
                        return serializer.Serialize(lstCustomers);
                    }
                    else {
                        var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);
                        return serializer.Serialize(lstCustomers);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        public class getclientdetail
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        [WebMethod]
        public static string BindEntityList(ActAssignmentDetails AssignmentObj)
        {
            try
            {
                long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                List<object> Entities = new List<object>();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var role = string.Empty;
                if (user_Roles.Contains("HVADM"))
                {
                    role = user_Roles.Trim();
                }
                else
                {
                    var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                }
                if (CustomerID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetVendorAuditAssignedLocationBranches((int)CustomerID, AuthenticationHelper.UserID, role)).ToList();
                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                Entities = RLCSVendorMastersManagement.GetEntitiesCustomerWise((int)CustomerID);
                            }
                        }
                    }
                }
                return serializer.Serialize(Entities);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindEntityStateList(ActAssignmentDetails AssignmentObj)
        {
            try
            {
                long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                List<object> EntityState = new List<object>();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var role = string.Empty;
                if (user_Roles.Contains("HVADM"))
                {
                    role = user_Roles.Trim();
                }
                else
                {
                    var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                }
                if (CustomerID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        EntityState = RLCSVendorMastersManagement.GetStateEntityWise((int)CustomerID);
                    }
                }
                return serializer.Serialize(EntityState);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindEntityLocationList(ActAssignmentDetails AssignmentObj)
        {
            try
            {
                //long StateID = Convert.ToInt64(AssignmentObj.StateID);
                var StateID = AssignmentObj.StateID;
                long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                List<object> EntityLocation = new List<object>();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var role = string.Empty;
                if (user_Roles.Contains("HVADM"))
                {
                    role = user_Roles.Trim();
                }
                else
                {
                    var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //EntityLocation = RLCSVendorMastersManagement.GetLocationStateWise((int)StateID, (int)CustomerID);
                    EntityLocation = RLCSVendorMastersManagement.GetLocationStateWise((dynamic)StateID, (int)CustomerID);
                }
                
                return serializer.Serialize(EntityLocation);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindEntityBranchList(ActAssignmentDetails AssignmentObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (AssignmentObj.Location != null)
                {
                    long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                    bool flag = false;
                    string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                    if (CheckContractCustomer == "False")
                    {
                        flag = true;
                        string checkISTeamlease = RLCSVendorMastersManagement.IsServiceProviderCheck(CustomerID);
                        if (checkISTeamlease == "True")
                        {
                            flag = false;
                        }
                    }

                    if (flag) //if (CheckContractCustomer == "False")
                    {
                        List<string> LocationList = new List<string>();
                        AssignmentObj.Location = AssignmentObj.Location.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        LocationList = AssignmentObj.Location;
                        //long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                        List<BranchesLocationWise> EntityBranch = new List<BranchesLocationWise>();
                        var role = string.Empty;
                        if (user_Roles.Contains("HVADM"))
                        {
                            role = user_Roles.Trim();
                        }
                        else
                        {
                            var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                            role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                        }
                        foreach (var LocationID in LocationList)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var branches = RLCSVendorMastersManagement.GetBranchLocationWise((int)CustomerID, LocationID);
                                foreach (var item in branches)
                                {
                                    BranchesLocationWise branchobj = new BranchesLocationWise();
                                    branchobj.ID = item.ID;
                                    branchobj.Name = item.Name;
                                    EntityBranch.Add(branchobj);
                                }
                            }
                        }
                        return serializer.Serialize(EntityBranch);
                    }
                    else
                    {
                        //long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                        List<BranchesLocationWise> EntityBranch = new List<BranchesLocationWise>();
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var branches = RLCSVendorMastersManagement.GetBranchClientWise((int)CustomerID);
                            foreach (var item in branches)
                            {
                                BranchesLocationWise branchobj = new BranchesLocationWise();
                                branchobj.ID = item.ID;
                                branchobj.Name = item.Name;
                                EntityBranch.Add(branchobj);
                            }
                        }
                        return serializer.Serialize(EntityBranch);
                    }

                }
                return serializer.Serialize(0);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
      
        [WebMethod]
        public static string BindVendorList(ActAssignmentDetails AssignmentObj)
        {
            try
            {

                int branchID = -1;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (AssignmentObj.Branches != null && AssignmentObj.CustomerID != null)
                {
                    //  branchID = Convert.ToInt32(AssignmentObj.Location);
                    List<int> BranchList = new List<int>();
                    AssignmentObj.Branches = AssignmentObj.Branches.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                    BranchList = AssignmentObj.Branches.ConvertAll(int.Parse);
                    List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                    Dictionary<string, object> childRow;

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<CustomerBranch> Obj = new List<CustomerBranch>();
                        DataTable VendorTable = new DataTable("Vendor");
                        DataColumn dtColumn;
                        DataRow myDataRow;
                        // Create id column  
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(Int32);
                        dtColumn.ColumnName = "ID";
                        dtColumn.Caption = "Vendor ID";
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        // Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);

                        // Create Name column.    
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(String);
                        dtColumn.ColumnName = "Name";
                        dtColumn.Caption = "Vendor Name";
                        dtColumn.AutoIncrement = false;
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        /// Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);

                        // Create Name column.    
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(String);
                        dtColumn.ColumnName = "ContractorName";
                        dtColumn.Caption = "Contractor Name";
                        dtColumn.AutoIncrement = false;
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        /// Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);
                        // Create Name column.    
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(int);
                        dtColumn.ColumnName = "StartYear";
                        dtColumn.Caption = "Contractor StartYear";
                        dtColumn.AutoIncrement = false;
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        /// Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);
                        // Create Name column.    
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(int);
                        dtColumn.ColumnName = "EndYear";
                        dtColumn.Caption = "Contractor EndYear";
                        dtColumn.AutoIncrement = false;
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        /// Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);
                        foreach (var BranchId in BranchList)
                        {
                            branchID = Convert.ToInt32(BranchId);
                            int branchid = branchID;
                            int Customerid = Convert.ToInt32(AssignmentObj.CustomerID);
                            var data = entities.SP_RLCS_Vendor_GetContractorsList(branchid, Customerid).ToList();
                            foreach (var row in data)
                            {
                                myDataRow = VendorTable.NewRow();
                                myDataRow["ID"] = row.AuditID;
                                myDataRow["Name"] = row.Name;
                                myDataRow["ContractorName"] = row.CC_ContractorName;
                                myDataRow["StartYear"] = row.ContractStartYear;
                                myDataRow["EndYear"] = row.ContractEndYear;
                                VendorTable.Rows.Add(myDataRow);
                            }
                        }
                        foreach (DataRow row in VendorTable.Rows)
                        {
                            childRow = new Dictionary<string, object>();
                            foreach (DataColumn col in VendorTable.Columns)
                            {
                                childRow.Add(col.ColumnName, row[col]);
                            }
                            parentRow.Add(childRow);
                        }
                        if (VendorTable.Rows.Count > 0)
                        {
                            return serializer.Serialize(parentRow);
                        }
                        else
                        {
                            return serializer.Serialize("");
                        }
                    }
                }
                else
                {
                    return serializer.Serialize("");
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindStateList(ActAssignmentDetails AssignmentObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = RLCSVendorMastersManagement.getRLCSddlStateData();
                    data = data.Where(x => !x.SM_Code.Equals("CENTRAL")).ToList();
                    return serializer.Serialize(data);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindActList(ActAssignmentDetails AssignmentObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string stateorcentral = string.Empty;
                    string StateID = string.Empty;
                    stateorcentral = AssignmentObj.Type;
                    StateID = AssignmentObj.StateID;

                    var data = RLCSVendorMastersManagement.getRLCSActlistData(stateorcentral);
                    if (data.Count > 0)
                        if (!string.IsNullOrEmpty(StateID))
                            data = data.Where(x => (x.AM_StateID == StateID || x.AM_StateID.ToUpper() == "CENTRAL")).ToList();

                    return serializer.Serialize(data);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindAssignCheckList(ActAssignmentDetails AssignmentObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (AssignmentObj.VendorIDs != null && AssignmentObj.Frequency != null && AssignmentObj.Frequency == "C"
                    && AssignmentObj.StartDate != null && AssignmentObj.EndDate != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<long> AuditIDList = new List<long>();
                        AssignmentObj.VendorIDs = AssignmentObj.VendorIDs.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        AuditIDList = AssignmentObj.VendorIDs.ConvertAll(long.Parse);
                        List<int> VendorList = new List<int>();
                        foreach (var AuditID in AuditIDList)
                        {
                            long Auditid = Convert.ToInt64(AuditID);
                            var AuditObj = (from aduit in entities.RLCS_VendorAuditInstance
                                            where aduit.ID == Auditid
                                            select aduit).FirstOrDefault();
                            int VendorID = Convert.ToInt32(AuditObj.AVACOM_VendorID);
                            VendorList.Add(VendorID);
                        }


                        //AssignmentObj.Years = AssignmentObj.Years.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        //List<int> YearList = AssignmentObj.Years.ConvertAll(int.Parse);

                        //AssignmentObj.Periods = AssignmentObj.Periods.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        //List<string> PeriodList = AssignmentObj.Periods;

                        List<SP_RLCS_VendorACT_Mapping_Result> lstVendorActDetails = new List<SP_RLCS_VendorACT_Mapping_Result>();
                        List<VendorAssignCheckListDetails> AssignlstActDetailsList = new List<VendorAssignCheckListDetails>();
                        if (VendorList.Count > 0)
                        {
                            string SchedulePeriod = AssignmentObj.StartDate + " - " + AssignmentObj.EndDate;

                            //var YearPeriodList = new List<string>();
                            //foreach (var Year in YearList)
                            //{
                            //    foreach (var Period in PeriodList)
                            //    {
                            //        string FirstMonth = Period.Substring(0, 3);
                            //        string SecondMonth = Period.Substring(Period.Length - 3);
                            //        string SchedulePeriod = FirstMonth + " " + Year + " " + "-" + " " + SecondMonth + " " + Year;
                            //        YearPeriodList.Add(SchedulePeriod);
                            //    }
                            //}


                            string StateID = string.Empty;
                            if (!string.IsNullOrEmpty(AssignmentObj.StateID))
                                if (AssignmentObj.StateID != "-1")
                                    StateID = Convert.ToString(AssignmentObj.StateID);

                            string ActID = string.Empty;
                            if (!string.IsNullOrEmpty(AssignmentObj.ActID))
                                if (AssignmentObj.ActID != "-1")
                                    ActID = Convert.ToString(AssignmentObj.ActID);

                            lstVendorActDetails = entities.SP_RLCS_VendorACT_Mapping().ToList();

                            if (lstVendorActDetails.Count > 0)
                            {
                                string stateorcentral = string.Empty;
                                if (!string.IsNullOrEmpty(AssignmentObj.Type))
                                {
                                    stateorcentral = Convert.ToString(AssignmentObj.Type);
                                    if (stateorcentral.ToUpper().Trim() == "Central".ToUpper().Trim())
                                    {
                                        lstVendorActDetails = lstVendorActDetails.Where(x => x.AM_StateID.ToUpper().Trim() == stateorcentral.ToUpper().Trim()).ToList();
                                    }
                                    else
                                    {
                                        lstVendorActDetails = lstVendorActDetails.Where(x => x.AM_StateID.ToUpper().Trim() != "Central".ToUpper().Trim()).ToList();
                                    }
                                }

                                if (AuditIDList.Count > 0)
                                    lstVendorActDetails = lstVendorActDetails.Where(x => AuditIDList.Contains((long)x.AuditID)).ToList();

                                //if (VendorList.Count > 0)
                                //    lstVendorActDetails = lstVendorActDetails.Where(x => VendorList.Contains(x.VendorID)).ToList();

                                if (!string.IsNullOrEmpty(StateID))
                                    lstVendorActDetails = lstVendorActDetails.Where(x => x.AM_StateID == StateID).ToList();

                                if (!string.IsNullOrEmpty(ActID))
                                    lstVendorActDetails = lstVendorActDetails.Where(x => x.ActID == ActID).ToList();

                                if (!string.IsNullOrEmpty(SchedulePeriod))
                                    lstVendorActDetails = lstVendorActDetails.Where(x => x.SchedulePeriod == SchedulePeriod).ToList();

                                //if (YearPeriodList.Count > 0)
                                //    lstVendorActDetails = lstVendorActDetails.Where(x => YearPeriodList.Contains(x.SchedulePeriod)).ToList();
                                lstVendorActDetails = lstVendorActDetails.GroupBy(p => p.ActID)
                                      .Select(g => g.First())
                                      .ToList();

                                foreach (var item in lstVendorActDetails)
                                {
                                    long Audid = Convert.ToInt64(item.AuditID);
                                    var AuditDetailsObj = (from aduit in entities.RLCS_VendorAuditInstance
                                                           where aduit.ID == Audid
                                                           select aduit).FirstOrDefault();
                                    string VendorName = AuditDetailsObj.CC_ContractorName;
                                    int BranchID = Convert.ToInt32(AuditDetailsObj.AVACOM_BranchID);

                                    var BranchDetailsObj = (from brch in entities.CustomerBranches
                                                            where brch.ID == BranchID
                                                            select brch).FirstOrDefault();
                                    string BranchName = BranchDetailsObj.Name;

                                    VendorAssignCheckListDetails AssignlstActDetails = new VendorAssignCheckListDetails();
                                    AssignlstActDetails.Id = item.Id;
                                    AssignlstActDetails.ActID = item.ActID;
                                    AssignlstActDetails.AM_ActName = item.AM_ActName;
                                    AssignlstActDetails.VendorID = item.VendorID;
                                    AssignlstActDetails.NatureOfCompliance = item.NatureOfCompliance;
                                    AssignlstActDetails.StateName = item.StateName;
                                    AssignlstActDetails.AM_ActGroup = item.AM_ActName;
                                    AssignlstActDetails.Risk = item.Risk;
                                    AssignlstActDetails.SchedulePeriod = item.SchedulePeriod;
                                    AssignlstActDetails.AuditID = Audid;
                                    AssignlstActDetails.BranchName = BranchName;
                                    AssignlstActDetails.VendorName = VendorName;
                                    AssignlstActDetails.CheckListMasterId = Convert.ToString(item.CheckListMasterId);
                                    AssignlstActDetailsList.Add(AssignlstActDetails);
                                }
                            }

                        }
                        return serializer.Serialize(AssignlstActDetailsList);
                    }
                }
                else if (AssignmentObj.VendorIDs != null && AssignmentObj.Years != null && AssignmentObj.Periods != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<long> AuditIDList = new List<long>();
                        AssignmentObj.VendorIDs = AssignmentObj.VendorIDs.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        AuditIDList = AssignmentObj.VendorIDs.ConvertAll(long.Parse);
                        List<int> VendorList = new List<int>();
                        foreach (var AuditID in AuditIDList)
                        {
                            long Auditid = Convert.ToInt64(AuditID);
                            var AuditObj = (from aduit in entities.RLCS_VendorAuditInstance
                                            where aduit.ID == Auditid
                                            select aduit).FirstOrDefault();
                            int VendorID = Convert.ToInt32(AuditObj.AVACOM_VendorID);
                            VendorList.Add(VendorID);
                        }


                        AssignmentObj.Years = AssignmentObj.Years.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        List<int> YearList = AssignmentObj.Years.ConvertAll(int.Parse);

                        AssignmentObj.Periods = AssignmentObj.Periods.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        List<string> PeriodList = AssignmentObj.Periods;

                        List<SP_RLCS_VendorACT_Mapping_Result> lstVendorActDetails = new List<SP_RLCS_VendorACT_Mapping_Result>();
                        List<VendorAssignCheckListDetails> AssignlstActDetailsList = new List<VendorAssignCheckListDetails>();
                        if (VendorList.Count > 0)
                        {

                            var YearPeriodList = new List<string>();
                            foreach (var Year in YearList)
                            {
                                foreach (var Period in PeriodList)
                                {
                                    string FirstMonth = Period.Substring(0, 3);
                                    string SecondMonth = Period.Substring(Period.Length - 3);
                                    string SchedulePeriod = FirstMonth + " " + Year + " " + "-" + " " + SecondMonth + " " + Year;
                                    YearPeriodList.Add(SchedulePeriod);
                                }
                            }


                            string StateID = string.Empty;
                            if (!string.IsNullOrEmpty(AssignmentObj.StateID))
                                if (AssignmentObj.StateID != "-1")
                                    StateID = Convert.ToString(AssignmentObj.StateID);

                            string ActID = string.Empty;
                            if (!string.IsNullOrEmpty(AssignmentObj.ActID))
                                if (AssignmentObj.ActID != "-1")
                                    ActID = Convert.ToString(AssignmentObj.ActID);

                            lstVendorActDetails = entities.SP_RLCS_VendorACT_Mapping().ToList();

                            if (lstVendorActDetails.Count > 0)
                            {
                                string stateorcentral = string.Empty;
                                if (!string.IsNullOrEmpty(AssignmentObj.Type))
                                {
                                    stateorcentral = Convert.ToString(AssignmentObj.Type);
                                    if (stateorcentral.ToUpper().Trim() == "Central".ToUpper().Trim())
                                    {
                                        lstVendorActDetails = lstVendorActDetails.Where(x => x.AM_StateID.ToUpper().Trim() == stateorcentral.ToUpper().Trim()).ToList();
                                    }
                                    else
                                    {
                                        lstVendorActDetails = lstVendorActDetails.Where(x => x.AM_StateID.ToUpper().Trim() != "Central".ToUpper().Trim()).ToList();
                                    }
                                }

                                if (AuditIDList.Count > 0)
                                    lstVendorActDetails = lstVendorActDetails.Where(x => AuditIDList.Contains((long)x.AuditID)).ToList();

                                //if (VendorList.Count > 0)
                                //    lstVendorActDetails = lstVendorActDetails.Where(x => VendorList.Contains(x.VendorID)).ToList();

                                if (!string.IsNullOrEmpty(StateID))
                                    lstVendorActDetails = lstVendorActDetails.Where(x => x.AM_StateID == StateID).ToList();

                                if (!string.IsNullOrEmpty(ActID))
                                    lstVendorActDetails = lstVendorActDetails.Where(x => x.ActID == ActID).ToList();

                                if (YearPeriodList.Count > 0)
                                    lstVendorActDetails = lstVendorActDetails.Where(x => YearPeriodList.Contains(x.SchedulePeriod)).ToList();


                                lstVendorActDetails = lstVendorActDetails.GroupBy(p => p.ActID)
                                      .Select(g => g.First())
                                      .ToList();

                                foreach (var item in lstVendorActDetails)
                                {
                                    long Audid = Convert.ToInt64(item.AuditID);
                                    var AuditDetailsObj = (from aduit in entities.RLCS_VendorAuditInstance
                                                           where aduit.ID == Audid
                                                           select aduit).FirstOrDefault();
                                    string VendorName = AuditDetailsObj.CC_ContractorName;
                                    int BranchID = Convert.ToInt32(AuditDetailsObj.AVACOM_BranchID);

                                    var BranchDetailsObj = (from brch in entities.CustomerBranches
                                                            where brch.ID == BranchID
                                                            select brch).FirstOrDefault();
                                    string BranchName = BranchDetailsObj.Name;

                                    VendorAssignCheckListDetails AssignlstActDetails = new VendorAssignCheckListDetails();
                                    AssignlstActDetails.Id = item.Id;
                                    AssignlstActDetails.ActID = item.ActID;
                                    AssignlstActDetails.AM_ActName = item.AM_ActName;
                                    AssignlstActDetails.VendorID = item.VendorID;
                                    AssignlstActDetails.NatureOfCompliance = item.NatureOfCompliance;
                                    AssignlstActDetails.StateName = item.StateName;
                                    AssignlstActDetails.AM_ActGroup = item.AM_ActName;
                                    AssignlstActDetails.Risk = item.Risk;
                                    AssignlstActDetails.SchedulePeriod = item.SchedulePeriod;
                                    AssignlstActDetails.AuditID = Audid;
                                    AssignlstActDetails.BranchName = BranchName;
                                    AssignlstActDetails.VendorName = VendorName;
                                    AssignlstActDetails.CheckListMasterId = Convert.ToString(item.CheckListMasterId);
                                    AssignlstActDetailsList.Add(AssignlstActDetails);
                                }
                            }

                        }
                        return serializer.Serialize(AssignlstActDetailsList);
                    }
                }
                else
                {
                    return serializer.Serialize("");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindUnAssignCheckList(ActAssignmentDetails AssignmentObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                int CustomerID = Convert.ToInt32(AssignmentObj.CustomerID);
                if (AssignmentObj.VendorIDs != null && AssignmentObj.Frequency != null && AssignmentObj.Frequency == "C"
                   && AssignmentObj.StartDate != null && AssignmentObj.EndDate != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<int> VendorList = new List<int>();
                        AssignmentObj.VendorIDs = AssignmentObj.VendorIDs.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        VendorList = AssignmentObj.VendorIDs.ConvertAll(int.Parse);


                        List<int> AuditIDList = new List<int>();
                        AssignmentObj.VendorIDs = AssignmentObj.VendorIDs.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        AuditIDList = AssignmentObj.VendorIDs.ConvertAll(int.Parse);

                        //AssignmentObj.Years = AssignmentObj.Years.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        //List<int> YearList = AssignmentObj.Years.ConvertAll(int.Parse);

                        //AssignmentObj.Periods = AssignmentObj.Periods.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        //List<string> PeriodList = AssignmentObj.Periods;

                        List<int> BranchesList = new List<int>();
                        AssignmentObj.Branches = AssignmentObj.Branches.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        BranchesList = AssignmentObj.Branches.ConvertAll(int.Parse);


                        List<SP_RLCS_Vendor_CheckListDetail_Result> lstActDetails = new List<SP_RLCS_Vendor_CheckListDetail_Result>();

                        if (AuditIDList.Count > 0)
                        {
                            string StateID = string.Empty;
                            if (!string.IsNullOrEmpty(AssignmentObj.StateID))
                                if (AssignmentObj.StateID != "-1")
                                    StateID = Convert.ToString(AssignmentObj.StateID);

                            string ActID = string.Empty;
                            if (!string.IsNullOrEmpty(AssignmentObj.ActID))
                                if (AssignmentObj.ActID != "-1")
                                    ActID = Convert.ToString(AssignmentObj.ActID);

                            lstActDetails = entities.SP_RLCS_Vendor_CheckListDetail().ToList();


                            if (lstActDetails.Count > 0)
                            {
                                string stateorcentral = string.Empty;
                                if (!string.IsNullOrEmpty(AssignmentObj.Type))
                                {
                                    stateorcentral = Convert.ToString(AssignmentObj.Type);
                                    if (stateorcentral.ToUpper().Trim() == "Central".ToUpper().Trim())
                                    {
                                        lstActDetails = lstActDetails.Where(x => x.StateID.ToUpper().Trim() == stateorcentral.ToUpper().Trim()).ToList();
                                    }
                                    else
                                    {
                                        lstActDetails = lstActDetails.Where(x => x.StateID.ToUpper().Trim() != "Central".ToUpper().Trim()).ToList();
                                    }
                                }



                                if (!string.IsNullOrEmpty(StateID))
                                    lstActDetails = lstActDetails.Where(x => (x.StateID == StateID || x.StateID.ToUpper() == "CENTRAL")).ToList();

                                if (!string.IsNullOrEmpty(ActID))
                                    lstActDetails = lstActDetails.Where(x => x.ActID == ActID).ToList();

                                if (CustomerID != 0)
                                    lstActDetails = lstActDetails.Where(x => x.CustomerID == CustomerID || x.CustomerID == null).ToList();

                                string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                                if (CheckContractCustomer == "True")
                                {
                                    if (CustomerID != 0)
                                        lstActDetails = lstActDetails.Where(x => x.CustomerID == CustomerID).ToList();
                                }
                                else
                                {
                                    string checkISTeamlease = RLCSVendorMastersManagement.IsServiceProviderCheck(CustomerID);
                                    if (checkISTeamlease == "True")
                                    {
                                        if (CustomerID != 0)
                                            lstActDetails = lstActDetails.Where(x => x.CustomerID == CustomerID).ToList();
                                    }
                                }

                                if (AssignmentObj.isclientbasedchecklist)
                                {
                                    lstActDetails = lstActDetails.Where(x => x.CustomerID == CustomerID).ToList(); ;
                                }


                                List<VendorUnassignCheckListDetails> UnassignlstActDetailsList = new List<VendorUnassignCheckListDetails>();
                                foreach (var auditid in AuditIDList)
                                {
                                    DateTime ActiveStartDate = new DateTime();
                                    DateTime ActiveEndDate = new DateTime();
                                    int VendorID = 0;
                                    int BranchID = 0;
                                    string VendorName = "";
                                    var AuditObj = (from aduit in entities.RLCS_VendorAuditInstance
                                                    where aduit.ID == auditid
                                                    select aduit).FirstOrDefault();

                                    if (AuditObj != null)
                                    {
                                        ActiveStartDate = Convert.ToDateTime(AuditObj.CC_ContractFrom);
                                        ActiveEndDate = Convert.ToDateTime(AuditObj.CC_ContractTo);
                                        VendorID = Convert.ToInt32(AuditObj.AVACOM_VendorID);
                                        VendorName = AuditObj.CC_ContractorName;
                                        BranchID = Convert.ToInt32(AuditObj.AVACOM_BranchID);
                                    }
                                    
                                            //string FirstMonth = Period.Substring(0, 3);
                                            //string SecondMonth = Period.Substring(Period.Length - 3);
                                            //string SchedulePeriod = FirstMonth + " " + Year + " " + "-" + " " + SecondMonth + " " + Year;
                                            var BranchObj = (from branch in entities.CustomerBranches
                                                             where branch.ID == BranchID
                                                             select branch).FirstOrDefault();

                                    string BranchName = BranchObj.Name;
                                    string SchedulePeriod = AssignmentObj.StartDate + " - " + AssignmentObj.EndDate;

                                    foreach (var row in lstActDetails)
                                    {

                                        var checklist = (from checklst in entities.RLCS_VendorActChecklistMapping
                                                         where checklst.VendorID == VendorID && checklst.CheckListMasterId == row.Id && checklst.SchedulePeriod == SchedulePeriod && checklst.IsDeleted == false && checklst.AuditID == auditid
                                                         select checklst).FirstOrDefault();

                                        if (checklist == null)
                                        {
                                            
                                            VendorUnassignCheckListDetails UnassignlstActDetails = new VendorUnassignCheckListDetails();
                                            UnassignlstActDetails.Id = row.Id;
                                            UnassignlstActDetails.ChecklistID = row.ChecklistID;
                                            UnassignlstActDetails.ActID = row.ActID;
                                            UnassignlstActDetails.NatureOfCompliance = row.NatureOfCompliance;
                                            UnassignlstActDetails.AM_ActName = row.AM_ActName;
                                            UnassignlstActDetails.AM_ActGroup = row.AM_ActGroup;
                                            UnassignlstActDetails.StateID = row.StateID;
                                            UnassignlstActDetails.StateName = row.StateName;
                                            UnassignlstActDetails.CreatedOn = Convert.ToDateTime(row.CreatedOn);
                                            UnassignlstActDetails.isDeleted = row.isDeleted;
                                            UnassignlstActDetails.SM_Status = row.SM_Status;
                                            UnassignlstActDetails.Risk = row.Risk;
                                            UnassignlstActDetails.VendorName = VendorName;
                                            UnassignlstActDetails.Period = SchedulePeriod;
                                            UnassignlstActDetails.VendorID = VendorID;
                                            UnassignlstActDetails.AuditID = auditid;
                                            UnassignlstActDetails.Branch = BranchName;
                                            UnassignlstActDetailsList.Add(UnassignlstActDetails);
                                        }
                                    }
                                }
                                return serializer.Serialize(UnassignlstActDetailsList);

                            }
                            else
                            {
                                return serializer.Serialize("");
                            }
                        }
                        else
                        {
                            return serializer.Serialize("");
                        }
                    }
                }
                else if (AssignmentObj.VendorIDs != null && AssignmentObj.Years != null && AssignmentObj.Periods != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<int> VendorList = new List<int>();
                        AssignmentObj.VendorIDs = AssignmentObj.VendorIDs.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        VendorList = AssignmentObj.VendorIDs.ConvertAll(int.Parse);


                        List<int> AuditIDList = new List<int>();
                        AssignmentObj.VendorIDs = AssignmentObj.VendorIDs.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        AuditIDList = AssignmentObj.VendorIDs.ConvertAll(int.Parse);

                        AssignmentObj.Years = AssignmentObj.Years.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        List<int> YearList = AssignmentObj.Years.ConvertAll(int.Parse);

                        AssignmentObj.Periods = AssignmentObj.Periods.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        List<string> PeriodList = AssignmentObj.Periods;

                        List<int> BranchesList = new List<int>();
                        AssignmentObj.Branches = AssignmentObj.Branches.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        BranchesList = AssignmentObj.Branches.ConvertAll(int.Parse);


                        List<SP_RLCS_Vendor_CheckListDetail_Result> lstActDetails = new List<SP_RLCS_Vendor_CheckListDetail_Result>();

                        if (AuditIDList.Count > 0)
                        {
                            string StateID = string.Empty;
                            if (!string.IsNullOrEmpty(AssignmentObj.StateID))
                                if (AssignmentObj.StateID != "-1")
                                    StateID = Convert.ToString(AssignmentObj.StateID);

                            string ActID = string.Empty;
                            if (!string.IsNullOrEmpty(AssignmentObj.ActID))
                                if (AssignmentObj.ActID != "-1")
                                    ActID = Convert.ToString(AssignmentObj.ActID);

                            lstActDetails = entities.SP_RLCS_Vendor_CheckListDetail().ToList();


                            if (lstActDetails.Count > 0)
                            {
                                string stateorcentral = string.Empty;
                                if (!string.IsNullOrEmpty(AssignmentObj.Type))
                                {
                                    stateorcentral = Convert.ToString(AssignmentObj.Type);
                                    if (stateorcentral.ToUpper().Trim() == "Central".ToUpper().Trim())
                                    {
                                        lstActDetails = lstActDetails.Where(x => x.StateID.ToUpper().Trim() == stateorcentral.ToUpper().Trim()).ToList();
                                    }
                                    else
                                    {
                                        lstActDetails = lstActDetails.Where(x => x.StateID.ToUpper().Trim() != "Central".ToUpper().Trim()).ToList();
                                    }
                                }
                                
                                if (!string.IsNullOrEmpty(StateID))
                                    lstActDetails = lstActDetails.Where(x => (x.StateID == StateID || x.StateID.ToUpper() == "CENTRAL")).ToList();

                                if (!string.IsNullOrEmpty(ActID))
                                    lstActDetails = lstActDetails.Where(x => x.ActID == ActID).ToList();
                                
                                if (CustomerID != 0)
                                    lstActDetails = lstActDetails.Where(x => x.CustomerID == CustomerID || x.CustomerID == null).ToList();

                                string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                                if (CheckContractCustomer == "True")
                                {
                                    if (CustomerID != 0)
                                        lstActDetails = lstActDetails.Where(x => x.CustomerID == CustomerID).ToList();
                                }
                                else
                                {
                                    string checkISTeamlease = RLCSVendorMastersManagement.IsServiceProviderCheck(CustomerID);
                                    if (checkISTeamlease == "True")
                                    {
                                        if (CustomerID != 0)
                                            lstActDetails = lstActDetails.Where(x => x.CustomerID == CustomerID).ToList();
                                    }
                                }

                                if (AssignmentObj.isclientbasedchecklist)
                                {
                                    lstActDetails = lstActDetails.Where(x => x.CustomerID == CustomerID).ToList(); ;
                                }

                                List<VendorUnassignCheckListDetails> UnassignlstActDetailsList = new List<VendorUnassignCheckListDetails>();
                                foreach (var auditid in AuditIDList)
                                {
                                    DateTime ActiveStartDate = new DateTime();
                                    DateTime ActiveEndDate = new DateTime();
                                    int VendorID = 0;
                                    int BranchID = 0;
                                    string VendorName = "";
                                    var AuditObj = (from aduit in entities.RLCS_VendorAuditInstance
                                                    where aduit.ID == auditid
                                                    select aduit).FirstOrDefault();

                                    if (AuditObj != null)
                                    {
                                        ActiveStartDate = Convert.ToDateTime(AuditObj.CC_ContractFrom);
                                        ActiveEndDate = Convert.ToDateTime(AuditObj.CC_ContractTo);
                                        VendorID = Convert.ToInt32(AuditObj.AVACOM_VendorID);
                                        VendorName = AuditObj.CC_ContractorName;
                                        BranchID = Convert.ToInt32(AuditObj.AVACOM_BranchID);
                                    }

                                    foreach (var Year in YearList)
                                    {
                                        foreach (var Period in PeriodList)
                                        {
                                            string FirstMonth = Period.Substring(0, 3);
                                            string SecondMonth = Period.Substring(Period.Length - 3);
                                            string SchedulePeriod = FirstMonth + " " + Year + " " + "-" + " " + SecondMonth + " " + Year;
                                            var BranchObj = (from branch in entities.CustomerBranches
                                                             where branch.ID == BranchID
                                                             select branch).FirstOrDefault();

                                            string BranchName = BranchObj.Name;
                                            foreach (var row in lstActDetails)
                                            {

                                                var checklist = (from checklst in entities.RLCS_VendorActChecklistMapping
                                                                 where checklst.VendorID == VendorID && checklst.CheckListMasterId == row.Id && checklst.SchedulePeriod == SchedulePeriod && checklst.IsDeleted == false && checklst.AuditID == auditid
                                                                 select checklst).FirstOrDefault();

                                                if (checklist == null)
                                                {
                                                    VendorUnassignCheckListDetails UnassignlstActDetails = new VendorUnassignCheckListDetails();
                                                    UnassignlstActDetails.Id = row.Id;
                                                    UnassignlstActDetails.ChecklistID = row.ChecklistID;
                                                    UnassignlstActDetails.ActID = row.ActID;
                                                    UnassignlstActDetails.NatureOfCompliance = row.NatureOfCompliance;
                                                    UnassignlstActDetails.AM_ActName = row.AM_ActName;
                                                    UnassignlstActDetails.AM_ActGroup = row.AM_ActGroup;
                                                    UnassignlstActDetails.StateID = row.StateID;
                                                    UnassignlstActDetails.StateName = row.StateName;
                                                    UnassignlstActDetails.CreatedOn = Convert.ToDateTime(row.CreatedOn);
                                                    UnassignlstActDetails.isDeleted = row.isDeleted;
                                                    UnassignlstActDetails.SM_Status = row.SM_Status;
                                                    UnassignlstActDetails.Risk = row.Risk;
                                                    UnassignlstActDetails.VendorName = VendorName;
                                                    UnassignlstActDetails.Period = SchedulePeriod;
                                                    UnassignlstActDetails.VendorID = VendorID;
                                                    UnassignlstActDetails.AuditID = auditid;
                                                    UnassignlstActDetails.Branch = BranchName;
                                                    UnassignlstActDetailsList.Add(UnassignlstActDetails);
                                                }

                                            }
                                        }
                                    }

                                }
                                return serializer.Serialize(UnassignlstActDetailsList);

                            }
                            else
                            {
                                return serializer.Serialize("");
                            }
                        }
                        else
                        {
                            return serializer.Serialize("");
                        }
                    }
                }
                else
                {
                    return serializer.Serialize("");
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string AssignCheckListPeriodWise(ActAssignmentDetails AssignmentObj)
        {
            bool Success = false;

            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    foreach (var item in AssignmentObj.AssignCheckList)
                    {
                        if (item.AuditID != null)
                        {
                            long AuditID = Convert.ToInt64(item.AuditID);
                            string Period = item.SchedulePeriod;
                            DateTime ContractStartdate = new DateTime();
                            DateTime ContractEnddate = new DateTime();
                            RLCS_VendorAuditInstance obj = (from row in entities.RLCS_VendorAuditInstance
                                                            where row.ID == AuditID
                                                            select row).FirstOrDefault();

                            int vendorid = 0;
                            long auditid = 0;
                            int branchId = -1;
                            int frequencyid = -1;

                            if (obj != null)
                            {
                                ContractStartdate = Convert.ToDateTime(obj.CC_ContractFrom);
                                ContractEnddate = Convert.ToDateTime(obj.CC_ContractTo);
                                auditid = obj.ID;
                                branchId = Convert.ToInt32(obj.AVACOM_BranchID);
                                frequencyid = Convert.ToInt32(item.FrequencyID);
                                vendorid = Convert.ToInt32(obj.AVACOM_VendorID);
                            }
                            int StartYear = ContractStartdate.Year;
                            int EndYear = ContractEnddate.Year;

                            DateTime GetStartMonthYear = Convert.ToDateTime(ContractStartdate.Month + "/" + ContractStartdate.Year);
                            DateTime GetSEndMonthYear = Convert.ToDateTime(ContractEnddate.Month + "/" + ContractEnddate.Year);

                            int Startmonth = ContractStartdate.Month;
                            int Endmonth = ContractEnddate.Month;


                            //string FirstMonth = Period.Substring(0, 3);
                            //string SecondMonth = Period.Substring(Period.Length - 6, 3);
                            //string Year = Period.Substring(Period.Length - 2);
                            string SchedulePeriod = Period;

                            //DateTime StartMonthPeriod = Convert.ToDateTime(FirstMonth + "/" + Year);
                            //DateTime EndMonthPeriod = Convert.ToDateTime(SecondMonth + "/" + Year);

                            //if (((StartMonthPeriod <= GetStartMonthYear) && (EndMonthPeriod <= GetSEndMonthYear) && (EndMonthPeriod >= GetStartMonthYear)) || ((GetStartMonthYear <= StartMonthPeriod) && (EndMonthPeriod <= GetSEndMonthYear)) || ((StartMonthPeriod <= GetSEndMonthYear) && (GetSEndMonthYear <= EndMonthPeriod)))
                            //{
                                RLCS_VendorActChecklistMapping objAdd = new RLCS_VendorActChecklistMapping
                                {
                                    ActID = item.ActID,
                                    CheckListId = item.ChecklistID,
                                    CheckListMasterId = item.ChecklistMasterID,
                                    VendorID = Convert.ToInt32(item.VendorID),
                                    IsDeleted = false,
                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                    CreatedOn = DateTime.Now,
                                    SchedulePeriod = SchedulePeriod,
                                    AuditID = AuditID
                                };
                                RLCSVendorMastersManagement.saveActMappingDetail(objAdd);
                                Success = true;
                          //  }
                            RLCS_VendorAuditScheduleOn objAcList = (from row in entities.RLCS_VendorAuditScheduleOn
                                                                    where row.ForMonth == SchedulePeriod && row.AuditID == AuditID && row.CustomerBranchID == branchId
                                                                    select row).FirstOrDefault();
                            if (objAcList == null && Success)
                            {
                                UpdateComplianceUpdatedOn(AuditID);
                                RLCS_VendorAuditScheduleOn complianceScheduleon = new RLCS_VendorAuditScheduleOn();
                                complianceScheduleon.AuditID = AuditID;
                                complianceScheduleon.ScheduleOn = null;
                                complianceScheduleon.ForMonth = SchedulePeriod;
                                complianceScheduleon.ForPeriod = frequencyid;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.CustomerBranchID = branchId;
                                complianceScheduleon.AuditStatusID = 1;
                                entities.RLCS_VendorAuditScheduleOn.Add(complianceScheduleon);
                                entities.SaveChanges();

                                RLCS_VendorAuditTransaction transaction = new RLCS_VendorAuditTransaction()
                                {
                                    AuditID = AuditID,
                                    VendorAuditScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    CustomerBranchId = branchId,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                CreateTransaction(transaction);
                                var lstChecklistDetails = entities.SP_RLCS_GetChecklistDetail(vendorid, branchId).ToList();
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.SchedulePeriod == SchedulePeriod).ToList();
                                
                                //By Amol
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.ChecklistID == item.ChecklistID).ToList();
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.AuditID == AuditID).ToList();
                                //End

                                var checklistIdlist = lstChecklistDetails.Select(entry => entry.ID).ToList();

                                SaveAuditStepsDetails(checklistIdlist.ToList(), AuditID, complianceScheduleon.ID);

                            }
                            else
                            {
                                var lstChecklistDetails = entities.SP_RLCS_GetChecklistDetail(vendorid, branchId).ToList();
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.SchedulePeriod == SchedulePeriod).ToList();
                                //By Amol
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.ChecklistID == item.ChecklistID).ToList();
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.AuditID == AuditID).ToList();
                                //End
                                if (lstChecklistDetails.Count > 0)
                                {
                                    var checklistIdlist = lstChecklistDetails.Select(entry => entry.ID).ToList();
                                    SaveAuditStepsDetails(checklistIdlist.ToList(), AuditID, objAcList.ID);
                                }

                            }


                        }
                    }
                    return serializer.Serialize(Success);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        public static void UpdateComplianceUpdatedOn(long auditid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_VendorAuditInstance compliance = (from row in entities.RLCS_VendorAuditInstance
                                                       where row.ID == auditid
                                                       select row).FirstOrDefault();

                compliance.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }

        public static bool CreateTransaction(RLCS_VendorAuditTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.RLCS_VendorAuditTransaction.Add(transaction);
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }

        public static bool SaveAuditStepsDetails(List<long> ActiveAuditStepIDList, long auditID, long scheduleonid)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (ActiveAuditStepIDList.Count > 0)
                    {
                        //Save Each Step in AuditStepMapping
                        if (ActiveAuditStepIDList.Count > 0)
                        {
                            if (auditID != 0)
                            {
                                List<RLCS_VendorAuditStepMapping> AuditStepMappingList = new List<RLCS_VendorAuditStepMapping>();


                                ActiveAuditStepIDList.ForEach(EachAuditStep =>
                                {
                                    RLCS_VendorAuditStepMapping obj = (from row in entities.RLCS_VendorAuditStepMapping
                                                                       where row.AuditID == auditID && row.VendorAuditScheduleOnID == scheduleonid && row.CheckListID == EachAuditStep && row.IsActive == false
                                                                       select row).FirstOrDefault();
                                    if (obj != null)
                                    {
                                        obj.IsActive = true;
                                        entities.SaveChanges();
                                    }

                                    RLCS_VendorAuditStepMapping newASM = new RLCS_VendorAuditStepMapping()
                                    {
                                        AuditID = auditID,
                                        VendorAuditScheduleOnID = scheduleonid,
                                        CheckListID = EachAuditStep,
                                        IsActive = true,
                                    };

                                    if (!AuditStepMappingExists(newASM))
                                        AuditStepMappingList.Add(newASM);
                                });

                                if (AuditStepMappingList.Count > 0)
                                {
                                    saveSuccess = CreateAuditStepMapping(AuditStepMappingList);
                                }
                            } //AuditID is ZERO
                            else
                                saveSuccess = false;
                        } //No Audit Step
                        else
                            saveSuccess = false;
                    } //No Audit Step
                    else
                        saveSuccess = false;

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool CreateAuditStepMapping(List<RLCS_VendorAuditStepMapping> AuditStepMappingList)
        {
            try
            {
                int saveCount = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    AuditStepMappingList.ForEach(entry =>
                    {
                        entities.RLCS_VendorAuditStepMapping.Add(entry);
                        saveCount++;
                        if (saveCount > 100)
                        {
                            entities.SaveChanges();
                            saveCount = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool AuditStepMappingExists(RLCS_VendorAuditStepMapping ASM)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.RLCS_VendorAuditStepMapping
                             where row.AuditID == ASM.AuditID
                             && row.CheckListID == ASM.CheckListID
                             && row.VendorAuditScheduleOnID == ASM.VendorAuditScheduleOnID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();
                });
            }
        }

        public static List<BranchesLocationWise> GetBranchLocationWise(int VendorID, int LocationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from RVAS in entities.RLCS_VendorAuditInstance
                                     join CB in entities.CustomerBranches on RVAS.AVACOM_BranchID equals CB.ID
                                     where RVAS.AVACOM_VendorID == VendorID && CB.ParentID == LocationID
                                     select new BranchesLocationWise
                                     {
                                         Name = CB.Name,
                                         ID = CB.ID
                                     }).Distinct().ToList();

                return objchechklist;
            }
        }

        [WebMethod]
        public static string DeleteCheckList(ActAssignmentDetails AssignmentObj)
        {
            string Success = "";
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long AuditID = Convert.ToInt64(AssignmentObj.AuditID);
                    string ForMonth = AssignmentObj.SchedulePeriod;
                    long ScheduleOnID = -1;
                    long CheckListID = Convert.ToInt64(AssignmentObj.CheckListID);
                    RLCS_VendorAuditScheduleOn obj = (from row in entities.RLCS_VendorAuditScheduleOn
                                                      where row.AuditID == AuditID && row.ForMonth == ForMonth
                                                      select row).FirstOrDefault();

                    if (obj != null)
                    {
                        ScheduleOnID = obj.ID;
                    }


                    if (CheckAuditOpenOrCloseOrInprogress(AuditID, ScheduleOnID, CheckListID))
                    {
                        int ID = Convert.ToInt32(AssignmentObj.ID);
                        if (ID > 0)
                        {
                            if (RLCSVendorMastersManagement.DeleteAssignCheklistMappingDetail(AuditID, ForMonth, CheckListID))
                            {

                                if (DeleteStepMappingDetail(AuditID, ScheduleOnID, CheckListID))
                                    Success = "Unassign checklist to Vendor.";
                                else
                                    Success = "Something wrong";
                            }
                            else
                            {
                                Success = "Something wrong";
                            }
                        }
                    }
                    else
                    {
                        Success = "Audit has Scheduled";
                    }

                    return serializer.Serialize(Success);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }

        }

        [WebMethod]
        public static string GetCustomerFrequency(ActAssignmentDetails AssignmentObj)
        {

            string Frequency = "";
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int ID = Convert.ToInt32(AssignmentObj.CustomerID);

                    if (ID > 0)
                    {
                        var query = (from row in entities.RLCS_VendorAuditInstance
                                     where row.AVACOM_CustomerID == ID
                                     orderby row.ID descending
                                     select row).FirstOrDefault();

                        Frequency = query.CC_Frequency;
                    }

                    return serializer.Serialize(Frequency);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }

        }

        public static bool DeleteStepMappingDetail(long AuditID, long ScheduleOnID, long CheckListID)
        {
            bool flag = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_VendorAuditStepMapping obj = (from row in entities.RLCS_VendorAuditStepMapping
                                                   where row.AuditID == AuditID && row.VendorAuditScheduleOnID == ScheduleOnID && row.CheckListID == CheckListID
                                                   select row).FirstOrDefault();
                if (obj != null)
                {

                    obj.IsActive = false;
                    entities.SaveChanges();
                    flag = true;
                }
                else
                {
                    flag = false;
                }
                return flag;
            }
        }

        public static bool CheckAuditOpenOrCloseOrInprogress(long AuditID, long ScheduleOnID, long CheckListID)
        {
            bool flag = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_VendorAuditTransaction obj = (from row in entities.RLCS_VendorAuditTransaction
                                                   where row.AuditID == AuditID && row.VendorAuditScheduleOnID == ScheduleOnID && row.CheckListId == CheckListID
                                                   select row).FirstOrDefault();
                if (obj != null)
                {

                    if (obj.StatusId != 1)
                        flag = false;
                    else
                        flag = true;
                }
                else
                {
                    flag = true;
                }
                return flag;
            }
        }



        public class ActAssignmentDetails
        {
            public string AuditID { get; set; }
            public string ScheduledOnID { get; set; }
            public string CheckListID { get; set; }

            public string DeleteVendorID { get; set; }
            public string SchedulePeriod { get; set; }

            public string ID { get; set; }
            public string CustomerID { get; set; }
            public string EntityID { get; set; }
            public string StateID { get; set; }
            public List<string> Location { get; set; }
            public List<string> Branches { get; set; }
            public string Type { get; set; }
            public string ActID { get; set; }
            public List<string> VendorIDs { get; set; }
            public List<string> Years { get; set; }
            public List<string> Periods { get; set; }
            public List<GetActsDetails> AssignCheckList { get; set; }
            public bool isclientbasedchecklist { get; set; }
            public string Frequency { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
        }
        public class GetActsDetails
        {
            public string ActID { get; set; }
            public string AuditID { get; set; }
            public int ChecklistMasterID { get; set; }
            public string ChecklistID { get; set; }
            public string SchedulePeriod { get; set; }
            public string VendorID { get; set; }
            public List<string> VendorIDs { get; set; }
            public List<string> Years { get; set; }
            public List<string> Periods { get; set; }
            public string FrequencyID { get; set; }
            public List<string> LocationID { get; set; }
            //public string Frequency { get; set; }
            //public string StartDate { get; set; }
            //public string EndDate { get; set; }


        }

        public class BranchesLocationWise
        {
            public int? ID { get; set; }
            public string Name { get; set; }
        }

        public class VendorUnassignCheckListDetails
        {
            public long Id { get; set; }
            public string ChecklistID { get; set; }
            public string ActID { get; set; }
            public string NatureOfCompliance { get; set; }
            public string AM_ActName { get; set; }
            public string AM_ActGroup { get; set; }
            public string StateID { get; set; }
            public string StateName { get; set; }
            public DateTime CreatedOn { get; set; }
            public bool isDeleted { get; set; }
            public string SM_Status { get; set; }
            public string Risk { get; set; }
            public string VendorName { get; set; }
            public int VendorID { get; set; }
            public int AuditID { get; set; }
            public string Period { get; set; }
            public string Branch { get; set; }

        }

        public class VendorAssignCheckListDetails
        {
            public long Id { get; set; }
            public string ChecklistID { get; set; }
            public string ActID { get; set; }
            public string NatureOfCompliance { get; set; }
            public string AM_ActName { get; set; }
            public string AM_ActGroup { get; set; }
            public string StateID { get; set; }
            public string StateName { get; set; }
            public DateTime CreatedOn { get; set; }
            public bool isDeleted { get; set; }
            public string SM_Status { get; set; }
            public string Risk { get; set; }
            public string VendorName { get; set; }
            public string CheckListMasterId { get; set; }
            public int VendorID { get; set; }
            public long AuditID { get; set; }
            public string BranchName { get; set; }
            public string SchedulePeriod { get; set; }

        }

    }
}