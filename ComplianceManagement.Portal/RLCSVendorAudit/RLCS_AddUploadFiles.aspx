﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_AddUploadFiles.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_AddUploadFiles" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <link href="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript">

         
    </script>
  

    <script>


        $(document).ready(function () {
            $(function () {
                $('input[id*=txtDocumentValidity]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                        //,
                        //minDate: 0
                    });
            });

            $("button[data-dismiss-modal=modal1]").click(function () {
                $('#DocumentReviewPopUp1').modal('hide');
            });

            $("#docUpload").click(function () {
                $('[id*=ShowFile]').text("");
            });

        });
        function fopendocfileReview(file) {
            debugger
            
            $('#DocumentReviewPopUp1').modal('show');
            $('#docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }

    </script>
    <style>
        .form-control {
            margin-bottom: 8px;
            height: 31px;
            font-family: sans-serif;
        }

        .button, input, select, textarea {
            border: none;
        }

        .m-10 {
            padding-left: 24px;
        }

        html, body {
            overflow-x: hidden;
        }
       #divControl
        {
            width:100%;
        }
       .inner
        {
            display: inline-block;
        }
    </style>


</head>

<body style="background-color: white;">

    <form id="form1" runat="server">

        <div style="margin-bottom: 2px">
            <asp:ValidationSummary ID="vdsummary" Style="padding-left: 5%" runat="server" Display="none"
                class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup" />
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in"
                EnableClientScript="true" ValidationGroup="ComplianceValidationGroup" Style="display: none;" />
            <asp:CustomValidator ID="validDateNull" runat="server" class="alert alert-block alert-danger fade in"
                EnableClientScript="true" ValidationGroup="ComplianceValidationGroup" Style="display: none;" />
            <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
            <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div class="col-md-12">
            
                <%--<asp:TextBox ID="txtActName" runat="server" ReadOnly="true"></asp:TextBox>--%>
             

            <div id="divControl">
            
            <div class="inner"><asp:Label id="ShowFile" ForeColor="Blue" runat="server"></asp:Label><asp:FileUpload ID="docUpload"  CssClass="form-control" runat="server" style="height:40px;width: 356px;margin: 0px 20px 0px 0px;"/></div>
            
            <div class="inner"><asp:Label Visible="true" ForeColor="GrayText" Text="Please select validity" runat="server"></asp:Label><asp:TextBox ID="txtDocumentValidity" CssClass="form-control" runat="server" style="height:40px;width: 136px;margin: 0px 20px 0px 0px;"></asp:TextBox></div>
            
                <div class="inner"><asp:Button ID="btnSubmitDocument" Text="Save" class="btn btn-primary" runat="server" OnClick="btnSubmitDocument_Click" /></div>
            <div class="inner"><asp:Button ID="btnCancel" Text="Cancel" class="btn btn-primary" runat="server" OnClick="btnCancel_Click" /></div>
            
            </div>
        </div>
        <div class="col-md-12">
            <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>--%>

                    <div class="form-group clearfix"></div>

                    <asp:GridView Width="100%" Height="70%" runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                        PageSize="10" AllowPaging="true"  OnRowCommand="grdDocument_RowCommand" AutoPostBack="true" CssClass="table" OnRowEditing="grdDocument_RowEditing" OnPageIndexChanging="grdDocument_PageIndexChanging" >
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr. No." ItemStyle-Width="1%">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Valid Till" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ValidTill","{0:dd/MM/yyyy}") %>' ToolTip='<%# Eval("ValidTill","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-left" ItemStyle-CssClass="text-left" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="4%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEditDocument" runat="server" CommandName="Edit Document" ToolTip="Edit Document" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit Document" /></asp:LinkButton>

                                    <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>

                                    <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>

                                    <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Upload File">
                            <ItemTemplate>
                                <asp:FileUpload ID="checkListFileUpload"  CssClass="form-control" AutoPostBack="true" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="130px" />
                            <ItemStyle HorizontalAlign="Center" Width="130px" />
                        </asp:TemplateField>--%>
                            <%--<asp:TemplateField HeaderText="Valid Till">
                            <ItemTemplate>
                                <asp:TextBox ID="txtValidity" CssClass="formcontrolnew" AutoPostBack="true" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="130px" />
                            <ItemStyle HorizontalAlign="Center" Width="130px" />

                            <FooterTemplate>
                                <asp:ImageButton ID="btnAdd" class="btn btn-primary" ValidationGroup="valid1" runat="server" ImageUrl="~/img/Add.png" BackColor="White" ToolTip="Add New" OnClick="btnAddNew_Click" Style="width:40px;" />
                            </FooterTemplate>
                        </asp:TemplateField>--%>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <PagerTemplate>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                        
                    </asp:GridView>
                    </div>
               <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
        
        <%--<div id="divRLCSHeaderUploadDialogBulk">
            <iframe id="iframeUploadBulk" style="width: 878px; height: 500px; border: none"></iframe>
        </div>--%>
         <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 1%;">
            <div class="modal-dialog" style="width: 100%;height:100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal1" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" >
                        <div style="width: 100%;">                                               
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; ">
                                    <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="1000px"></iframe>
                                </fieldset>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </form>
    
    
        
    
</body>

</html>


