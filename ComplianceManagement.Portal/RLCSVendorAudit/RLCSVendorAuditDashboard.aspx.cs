﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSVendorAuditDashboard : System.Web.UI.Page
    {
        protected static int CustID;
        protected static int UserID;
        protected static string profileID;
        protected static string avacomRLCSAPIURL;
        protected static string[] BranchesChecked;
        protected string user_Roles;
        public List<string> AuditPeriod = new List<string>();

        protected static List<long> locationList = new List<long>();
        protected static List<RLCS_VendorAuditScheduleOn> RLCS_VendorAuditScheduleOn1 = null;
        protected static List<RLCS_VendorAuditAssignment> RLCS_VendorAuditAssignment1 = null;
        //protected static User LoggedUser = null;
        //protected static List<Role> role1 = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    RLCS_VendorAuditScheduleOn1 = (from row in entities.RLCS_VendorAuditScheduleOn
                                                   select row).ToList();

                    RLCS_VendorAuditAssignment1 = (from row in entities.RLCS_VendorAuditAssignment
                                                   //where row.UserID == AuthenticationHelper.UserID
                                                   select row).ToList();

                    //LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                    //role1 = (from row in entities.Roles
                    //         select row).ToList();

                }
                BindCustomerFilter();              
                ddlAuditType.SelectedValue = "-1";
                int ddlStatusValue = -1;
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                    ViewState["vrole"] = Convert.ToString(vrole);
                    user_Roles = vrole;
                    
                }

                avacomRLCSAPIURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];              
                UserID = AuthenticationHelper.UserID;
                string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];

                profileID = AuthenticationHelper.ProfileID;
                if (!string.IsNullOrEmpty(TLConnectKey))
                {
                    string userProfileID = string.Empty;
                    userProfileID = AuthenticationHelper.ProfileID;
                    if (!string.IsNullOrEmpty(userProfileID))
                    {
                        profileID = CryptographyHandler.encrypt(userProfileID.Trim(), TLConnectKey);
                    }                    
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditStatus"]))
                {
                    ddlStatusValue = Convert.ToInt32(Request.QueryString["AuditStatus"]);
                    if (ddlStatusValue == 1)
                    {
                        ddlAuditType.SelectedValue = "1";
                    }
                    else if (ddlStatusValue == 4)
                    {
                        ddlAuditType.SelectedValue = "4";
                    }
                    else if (ddlStatusValue == 5)
                    {
                        ddlAuditType.SelectedValue = "5";
                    }
                    else
                    {
                        ddlAuditType.SelectedValue = "-1";
                    }
                }
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ID";
                long AuditorID = AuthenticationHelper.UserID;
                BindAuditDetail(AuditorID);
                BindgrdSubmittedDocumentDetails(UserID);


            }
            
          
            user_Roles = ViewState["vrole"].ToString();

        }
        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
        private void BindCustomerFilter()
        {
            string user_Roles = AuthenticationHelper.Role;
            var role = string.Empty;
            if (user_Roles.Contains("HVADM"))
            {
                role = user_Roles.Trim();
            }
            else
            {
                var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                //role = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
            }
            string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
            if (CheckContractCustomer == "True")
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAllClientWise(AuthenticationHelper.UserID, role, AuthenticationHelper.CustomerID);

                ddlFilterCustomer.DataSource = lstCustomers;
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataBind();
            }
            else
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);

                ddlFilterCustomer.DataSource = lstCustomers;
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataBind();
            }
            //var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);
            //ddlFilterCustomer.DataTextField = "Name";
            //ddlFilterCustomer.DataValueField = "ID";
            //ddlFilterCustomer.DataSource = lstCustomers;
            //ddlFilterCustomer.DataBind();
        }
        //protected void rdbcalender_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            HdnTreeEvent.Value = "Event";
            string user_Roles = AuthenticationHelper.Role;
            var role = string.Empty;
            long customerID = -1;            
            if (user_Roles.Contains("HVADM"))
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
                    CustID = (int)customerID;
                    HdnCustVal.Value = Convert.ToString(CustID);

                }
                role = user_Roles.Trim();
            }
            else
            {
                var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                //role = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();

                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
                    CustID = (int)customerID;
                    HdnCustVal.Value = Convert.ToString(CustID);
                }
            }
            if (customerID != 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    BindStateFilter(CustID);
                  
                }                
                BindAuditPeriodFilter((int)customerID);
            }
            ForceCloseFilterBranchesTreeView();
        }
        private void BindStateFilter(int CustID)
        {
            string user_Roles = AuthenticationHelper.Role;
            var role = string.Empty;

            var lstCustomers = RLCSVendorMastersManagement.GetAllStates(CustID);
            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "ID";
            ddlState.DataSource = lstCustomers;
            ddlState.DataBind();
        }
        public static List<AuditDetail> GetAuditList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int userID = -1;
                userID = Convert.ToInt32(AuthenticationHelper.UserID);

                var AuditDetails = (from row in entities.AuditDetails
                                    where row.Isactive == false && row.AuditorID == userID
                                         && row.CustomerID == customerID
                                    select row).ToList();

                return AuditDetails;
            }
        }

        private void BindAuditDetail(long AuditorID)
        {
            try
            {
                String location = string.Empty;
               
                DateTime dt = DateTime.Now.AddDays(30);

                List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result> Checklist = new List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>();
                List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result> AuditDetails = new List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    DateTime EndMonthDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                                             DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                    int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        string vrole = Convert.ToString(ViewState["vrole"]);
                        //var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        var auditDetail = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(CustomerID, AuthenticationHelper.UserID, vrole)
                                           select row).ToList();
                        if(vrole== "HVEND")
                            auditDetail = auditDetail.Where(x => x.IsOffline == false).ToList();
                        auditDetail = auditDetail.Where(x => x.AuditStatusID == 1 || x.AuditStatusID == 4 || x.AuditStatusID == 2 || x.AuditStatusID == 5).ToList();
                        if (auditDetail != null && !(string.IsNullOrEmpty(ddlAuditPeriod.SelectedValue)))
                        {
                            auditDetail = auditDetail.Where(x => x.ForMonth.Trim().ToUpper() == ddlAuditPeriod.SelectedValue.Trim().ToUpper()).ToList();
                        }                     
                        //if (locationList != null)
                        //{
                        //    foreach (var branch in locationList)
                        //    {
                        //        if (branch >0)
                        //        {
                        //            auditDetail = auditDetail.Where(entry => entry.BranchID == branch).ToList();                                    
                        //            Checklist.AddRange(auditDetail);
                        //        }
                        //    }
                        //    Checklist = Checklist.Distinct().ToList();
                        //}
                       
                        Session["TotalRows"] = null;
                        if (Checklist.Count > 0)
                        {
                            AuditDetails = Checklist;
                        }
                        else
                        {
                            AuditDetails = auditDetail;
                        }

                        //if (auditDetail.Count > 0)
                        //{
                        divOpencount.InnerText = "0";
                        divClosedcount.InnerText = "0";
                        divCancelledcount.InnerText = "0";
                        divOverduecount.InnerText = "0";
                        if (Convert.ToInt64(ddlAuditType.SelectedValue) != -1)
                        {
                            #region Open Audit
                            //List<int> openstatus = new List<int>();
                            //openstatus.Add(1);
                            //openstatus.Add(2);
                            //openstatus.Add(3);

                            auditDetail.Where(e => e.StartDate <= EndMonthDate);
                            var openAudits = auditDetail.Where(e => e.StartDate <= EndMonthDate && e.AuditStatusID == 1).ToList();
                            //var OpenAudits = AuditDetails.Where(x => x.AuditStatusID == 1).ToList(); comment by rahul on 11 DEC 2019
                            divOpencount.InnerText = Convert.ToString(openAudits.Count);
                            #endregion
                            #region Closed Audit
                            var ClosedAudit = AuditDetails.Where(x => x.AuditStatusID == 4).ToList();
                            divClosedcount.InnerText = Convert.ToString(ClosedAudit.Count);
                            #endregion
                            #region Cancelled Audit
                            var CancelledAudit = AuditDetails.Where(x => x.AuditStatusID == 5).ToList();
                            divCancelledcount.InnerText = Convert.ToString(CancelledAudit.Count);
                            #endregion
                            #region Overdue Audit
                            DateTime now = DateTime.Now.Date;
                            var OverdueAudit = auditDetail.Where(e => e.AuditStatusID == 1 && e.StartDate < now).ToList();
                            //var OverdueAudit = AuditDetails.Where(x => x.AuditStatusID == 1 && x.EndDate != null).ToList();
                            //OverdueAudit = OverdueAudit.Where(y => y.StartDate < now).ToList();
                            divOverduecount.InnerText = Convert.ToString(OverdueAudit.Count);
                            #endregion


                            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                            {
                                RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                            }
                            if (locationList != null && locationList.Count > 0)
                            {
                                AuditDetails = AuditDetails.Where(x => locationList.Contains((int)x.BranchID)).Distinct().ToList();
                            }
                            AuditDetails = AuditDetails.Where(e => e.StartDate > DateTime.Now && e.StartDate <= EndMonthDate && (e.AuditStatusID == 1 || e.AuditStatusID == 2)).ToList();
                            grdAuditDetail.DataSource = AuditDetails;
                            grdAuditDetail.DataBind();
                            Session["TotalRows"] = AuditDetails.Count;
                            GetPageDisplaySummary();
                            //}
                        }
                        else
                        {
                            #region Open Audit
                            var openAudits = auditDetail.Where(e => e.StartDate <= EndMonthDate && (e.AuditStatusID == 1 || e.AuditStatusID == 2)).ToList();
                            divOpencount.InnerText = Convert.ToString(openAudits.Count);
                            #endregion

                            #region Closed Audit
                            var ClosedAudit = AuditDetails.Where(x => x.AuditStatusID == 4).ToList();
                            divClosedcount.InnerText = Convert.ToString(ClosedAudit.Count);
                            #endregion
                            #region Cancelled Audit
                            var CancelledAudit = AuditDetails.Where(x => x.AuditStatusID == 5).ToList();
                            divCancelledcount.InnerText = Convert.ToString(CancelledAudit.Count);
                            #endregion

                            DateTime now = DateTime.Now.Date;
                            var OverdueAudit = auditDetail.Where(e => (e.AuditStatusID == 1 || e.AuditStatusID == 2) && e.StartDate < now).ToList();
                            //var OverdueAudit = AuditDetails.Where(x => x.AuditStatusID == 1 && x.EndDate == null).ToList();
                            ////OverdueAudit = OverdueAudit.Where(y => y.EndDate < now).ToList();
                            //OverdueAudit = OverdueAudit.Where(y => y.StartDate < now).ToList();
                            divOverduecount.InnerText = Convert.ToString(OverdueAudit.Count);


                            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                            {
                                RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                            }
                            if (locationList != null && locationList.Count > 0)
                            {
                                AuditDetails = AuditDetails.Where(x => locationList.Contains((int)x.BranchID)).Distinct().ToList();
                            }
                            AuditDetails = AuditDetails.Where(e => e.StartDate > DateTime.Now && e.StartDate <= EndMonthDate && (e.AuditStatusID == 1 || e.AuditStatusID == 2)).ToList();

                            

                            grdAuditDetail.DataSource = AuditDetails;

                            grdAuditDetail.DataBind();
                            Session["TotalRows"] = AuditDetails.Count;
                            GetPageDisplaySummary();
                        }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //private void BindLocationFilter(List<int> assignedBranchList, int Customerid)
        //{
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            //var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Customerid);
        //            //tvFilterLocation.Nodes.Clear();
        //            //TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
        //            //node.Selected = true;
        //            //tvFilterLocation.Nodes.Add(node);

        //            //foreach (var item in bracnhes)
        //            //{
        //            //    node = new TreeNode(item.Name, item.ID.ToString());
        //            //    node.SelectAction = TreeNodeSelectAction.Expand;
        //            //    CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
        //            //    tvFilterLocation.Nodes.Add(node);
        //            //}
        //            //tvFilterLocation.CollapseAll();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        //private void BindLocationFilter()
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        tvFilterLocation.Nodes.Clear();
        //        var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);

        //        var LocationList = CustomerBranchManagement.GetAuditorAssignedLocationList(AuthenticationHelper.UserID, customerID);

        //        TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
        //        node.Selected = true;
        //        tvFilterLocation.Nodes.Add(node);

        //        foreach (var item in bracnhes)
        //        {
        //            node = new TreeNode(item.Name, item.ID.ToString());
        //            node.SelectAction = TreeNodeSelectAction.Expand;
        //            CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
        //            tvFilterLocation.Nodes.Add(node);
        //        }

        //        tvFilterLocation.CollapseAll();
        //        tvFilterLocation_SelectedNodeChanged(null, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            // var branches = HdnDropDown.Value;
            // BranchesChecked = HdnDropDown.Value.Split(',');
            BranchesChecked = BranchesChecked.Distinct().ToArray();
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdAuditDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                txtRemark.Text = "";
                txtStartDate.Text = "";
                txtEndDate.Text = "";
                txtRemarkReschedule.Text = "";
                if (e.CommandName.Equals("CHANGE_EDIT"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long AuditID = Convert.ToInt64(commandArgs[0]);
                    int AuditScheduleOnID = Convert.ToInt32(commandArgs[1]);

                    Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditChecklist.aspx?AID=" + AuditID + "&SOID=" + AuditScheduleOnID, false);
                }
                else if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    int StatusID = RLCSVendorMastersManagement.getAuditStatus(Convert.ToInt64(commandArgs[1]), Convert.ToInt64(commandArgs[0]));
                    if (StatusID > 0)
                    {
                        if (StatusID == 1)
                        {
                            HiddenFieldAuditId.Value = Convert.ToString(commandArgs[0]);
                            HiddenFieldScheduleOnId.Value = Convert.ToString(commandArgs[1]);
                            lblLocation.Text = Convert.ToString(commandArgs[2]);
                            lblPeriod.Text = Convert.ToString(commandArgs[3]);

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopupUpdateStatus();", true);
                        }
                        else if (StatusID == 5)
                        {
                            if (!RLCSVendorMastersManagement.getAuditRescheduleAllow(Convert.ToInt64(commandArgs[0]), Convert.ToInt64(commandArgs[1]), Convert.ToString(commandArgs[3])))
                            {
                                HiddenFieldAuditId1.Value = Convert.ToString(commandArgs[0]);
                                HiddenFieldScheduleOnId1.Value = Convert.ToString(commandArgs[1]);
                                lblLocation1.Text = Convert.ToString(commandArgs[2]);
                                lblPeriod1.Text = Convert.ToString(commandArgs[3]);

                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopupReschedule();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Audit Already Rescheduled for selected period.')", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenmsgCancelschedule();", true);
                        }
                    }
                    ForceCloseFilterBranchesTreeView();
                }
                else if (e.CommandName.Equals("CHANGE_SAVE"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long AuditID = Convert.ToInt64(commandArgs[0]);
                    int AuditScheduleOnID = Convert.ToInt32(commandArgs[1]);

                    if (AuditID > 0 && AuditScheduleOnID > 0)
                    {
                        GridViewRow gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                        TextBox txtenddate = (TextBox)gvRow.FindControl("txtEndDate");
                        Label lblstartdate = (Label)gvRow.FindControl("lblStartDate");
                        if (!string.IsNullOrEmpty(txtenddate.Text))
                        {
                            DateTime startdate = Convert.ToDateTime(lblstartdate.Text);
                            DateTime Enddate = Convert.ToDateTime(txtenddate.Text);
                            int compareValue;
                            try
                            {
                                compareValue = Enddate.CompareTo(startdate);
                            }
                            catch (ArgumentException)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 5 + ");", true);
                                return;
                            }
                            if (compareValue == -1)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 4 + ");", true);
                                return;
                            }
                            else if (compareValue == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 3 + ");", true);
                                return;
                            }
                            else
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    RLCS_VendorAuditScheduleOn updateEnddate = (from row in entities.RLCS_VendorAuditScheduleOn
                                                                                where row.ID == AuditScheduleOnID
                                                                                   && row.AuditID == AuditID
                                                                                select row).FirstOrDefault();
                                    if (updateEnddate != null)
                                    {
                                        updateEnddate.ScheduleOnEndDate = Convert.ToDateTime(txtenddate.Text);
                                        entities.SaveChanges();
                                    }
                                    long AuditorID = AuthenticationHelper.UserID;
                                    BindAuditDetail(AuditorID);
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 1 + ");", true);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup(" + 0 + ");", true);
                        }
                    }
                    ForceCloseFilterBranchesTreeView();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            long AuditorID = AuthenticationHelper.UserID;
            BindAuditDetail(AuditorID);
            GetPageDisplaySummary();
            ForceCloseFilterBranchesTreeView();
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdAuditDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAuditDetail.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                long AuditorID = AuthenticationHelper.UserID;
                BindAuditDetail(AuditorID);
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdAuditDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAuditDetail.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                long AuditorID = AuthenticationHelper.UserID;
                BindAuditDetail(AuditorID);
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdAuditDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAuditDetail.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                long AuditorID = AuthenticationHelper.UserID;
                BindAuditDetail(AuditorID);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected void grdAuditDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Label lblAuditID = (Label)e.Row.FindControl("lblAuditID");
                        Label lblAuditScheduleOnID = (Label)e.Row.FindControl("lblAuditScheduleOnID");
                        if (lblAuditID != null && lblAuditScheduleOnID != null)
                        {
                            var auditid = Convert.ToInt64(lblAuditID.Text);
                            var ScheduleOnID = Convert.ToInt64(lblAuditScheduleOnID.Text);
                            //var roleID = (from row in entities.RLCS_VendorAuditAssignment
                            //              where row.UserID == AuthenticationHelper.UserID
                            //              && row.AuditId == auditid
                            //              select row.RoleID).FirstOrDefault();
                            var roleID = (from row in RLCS_VendorAuditAssignment1
                                          where row.UserID == AuthenticationHelper.UserID
                                          && row.AuditId == auditid
                                          select row.RoleID).FirstOrDefault();

                            LinkButton btnsave = (LinkButton)e.Row.FindControl("btnChangeStatus");
                            LinkButton lnksave = (LinkButton)e.Row.FindControl("lnkSave");
                            TextBox txtEndDate = (TextBox)e.Row.FindControl("txtEndDate");
                            LinkButton lnkCancel = (LinkButton)e.Row.FindControl("lnkCancel");

                            if (roleID == 3)
                            {
                                btnsave.Visible = true;
                                txtEndDate.Enabled = false;
                                lnksave.Visible = false;
                                lnkCancel.Visible = false;
                            }
                            else
                            {
                                var checkExist = (from row in RLCS_VendorAuditScheduleOn1
                                                  where row.AuditID == auditid
                                                  && row.ID == ScheduleOnID
                                                  && row.IsActive == true
                                                  select row).FirstOrDefault();

                                //var checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                //                  where row.AuditID == auditid
                                //                  && row.ID == ScheduleOnID
                                //                  && row.IsActive == true
                                //                  select row).FirstOrDefault();
                                if (checkExist != null)
                                {
                                    if (checkExist.AuditStatusID == 4)
                                    {
                                        lnksave.Visible = false;
                                        lnkCancel.Visible = false;
                                    }
                                }

                                if (!string.IsNullOrEmpty(txtEndDate.Text))
                                {
                                    btnsave.Visible = true;
                                }
                                else
                                {
                                    btnsave.Visible = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            try
            {
                long AuditId = -1;
                if (!string.IsNullOrEmpty(HiddenFieldAuditId.Value))
                    AuditId = Convert.ToInt64(HiddenFieldAuditId.Value);

                long ScheduleOnId = -1;
                if (!string.IsNullOrEmpty(HiddenFieldScheduleOnId.Value))
                    ScheduleOnId = Convert.ToInt64(HiddenFieldScheduleOnId.Value);

                if (AuditId > 0 && ScheduleOnId > 0)
                {
                    if (RLCSVendorMastersManagement.UpdateAuditScheduleOnStatus(ScheduleOnId, AuditId, txtRemark.Text))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "FRemarkUpdate();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Audit Cancelation Save not Successfully.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnReschedule_Click(object sender, EventArgs e)
        {
            try
            {
                long AuditId = -1;
                if (!string.IsNullOrEmpty(HiddenFieldAuditId1.Value))
                    AuditId = Convert.ToInt64(HiddenFieldAuditId1.Value);

                long ScheduleOnId = -1;
                if (!string.IsNullOrEmpty(HiddenFieldScheduleOnId1.Value))
                    ScheduleOnId = Convert.ToInt64(HiddenFieldScheduleOnId1.Value);

                if (AuditId > 0 && ScheduleOnId > 0)
                {
                    if (Convert.ToDateTime(txtStartDate.Text) < Convert.ToDateTime(txtEndDate.Text))
                    {
                        if (!RLCSVendorMastersManagement.getAuditRescheduleAllow(AuditId, ScheduleOnId, lblPeriod1.Text))
                        {
                            if (RLCSVendorMastersManagement.UpdateAuditReschedule(ScheduleOnId, AuditId, Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text), txtRemarkReschedule.Text, AuthenticationHelper.UserID))
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "FRescheduleUpdate();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "script", "FRescheduleValidation();", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "script", "FAlreadyValidation();", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "script", "FRescheduledateValidation();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindAuditPeriodFilter(int custID)
        {
            string user_Roles = AuthenticationHelper.Role;
            var role = string.Empty;
            if (user_Roles.Contains("HVADM"))
            {
                role = user_Roles.Trim();
            }
            else
            {
                var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                //role = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
            }

            if (custID > 0 && UserID > 0)
            {
                var lstCustomers = GetAuditPeriod(custID, UserID, role);
                ddlAuditPeriod.DataSource = lstCustomers.Distinct().ToList();
                ddlAuditPeriod.DataBind();
            }

        }
        public List<string> GetAuditPeriod(int custID, int UserID, string role)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(Convert.ToInt32(ddlFilterCustomer.SelectedValue), UserID, role)
                                              select row).ToList();
                    if (mgmtQueryStatutory.Count > 0)
                    {
                        mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.StartDate).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                    AuditPeriod = mgmtQueryStatutory.Select(x => x.ForMonth).ToList();

                }
                else
                {
                    var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(Convert.ToInt32(311), UserID, role)
                                              select row).ToList();
                    if (mgmtQueryStatutory.Count > 0)
                    {
                        mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.StartDate).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                    AuditPeriod = mgmtQueryStatutory.Select(x => x.ForMonth).ToList();
                }
            }

            return AuditPeriod;
        }

        protected void ddlFilterPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ForceCloseFilterBranchesTreeView();
        }


        protected void btnlocation_Click(object sender, EventArgs e)
        {
            locationList.Clear();
            BindAuditPeriodFilter(CustID);
            ForceCloseFilterBranchesTreeView();
        }
        private void BindLocationFilter(int custid)
        {
            try
            {
                locationList.Clear();
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                tvFilterLocation.Nodes.Clear();


                var bracnhes = RLCSVendorMastersManagement.GetBranchesbyStates(custid,ddlState.SelectedValue);
                tvFilterLocation.Nodes.Clear();

               
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode();
                    node = new TreeNode(item.Name, Convert.ToString(item.ID));
                    node.SelectAction = TreeNodeSelectAction.Expand;                 
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
            }
        }
        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != AuthenticationHelper.CustomerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != AuthenticationHelper.CustomerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
            ForceCloseFilterBranchesTreeView();
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }


        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter(CustID);
            ForceCloseFilterBranchesTreeView();
        }

        protected void btnClearFilters_Click(object sender, EventArgs e)
        {
            tvFilterLocation.Nodes.Clear();
            locationList.Clear();
            ddlFilterCustomer.SelectedIndex = -1;
            ddlState.SelectedIndex = -1;
            tbxFilterLocation.Text = string.Empty;
            btnClear1_Click(sender, e);
            ForceCloseFilterBranchesTreeView();
            long AuditorID = AuthenticationHelper.UserID;
            BindAuditDetail(AuditorID);
        }

        private void BindgrdSubmittedDocumentDetails(int UserID)
        {
            try
            {
                List<SP_RLCS_VendorAudit_GetSubmittedAndPendingDocumentCount_Result> lstSubmittedDocumentDetails = new List<SP_RLCS_VendorAudit_GetSubmittedAndPendingDocumentCount_Result>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstSubmittedDocumentDetails = entities.SP_RLCS_VendorAudit_GetSubmittedAndPendingDocumentCount(UserID).ToList();
                    grdSubmittedDocumentDetails.DataSource = lstSubmittedDocumentDetails;
                    Session["grdSubmittedDocumentDetails"] = null;
                    Session["grdSubmittedDocumentDetails"] = (grdSubmittedDocumentDetails.DataSource as List<SP_RLCS_VendorAudit_GetSubmittedAndPendingDocumentCount_Result>).ToList();
                    grdSubmittedDocumentDetails.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdSubmittedDocumentDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("CHANGE_EDIT"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long AuditID = Convert.ToInt64(commandArgs[0]);
                    int AuditScheduleOnID = Convert.ToInt32(commandArgs[1]);
                    string VendorName = Convert.ToString(commandArgs[2]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", string.Format("Editpopup('{0}',{1},'{2}'); ", AuditID, AuditScheduleOnID, VendorName), true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnExcelUpload_Click(object sender, EventArgs e)
        {
            if (ExcelFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(ExcelFileUpload.FileName);
                    ExcelFileUpload.SaveAs(Server.MapPath("~/RLCSVendorAudit/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/RLCSVendorAudit/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            ProcessChecklistData(xlWorkbook);
                          
                        }
                    }
                   
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                   
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "script", "UploadExcelSheetValidation();", true);
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedVendorCount = 0;

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Report"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();

                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    long ScheduledID = -1;
                    DateTime StartDate = new DateTime();
                    DateTime EndDate = new DateTime();
                    string Status = string.Empty;
                    string Remark = string.Empty;
                    int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                    if (errorMessage.Count <= 0)
                    {
                        #region Save                        
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            ScheduledID = Convert.ToInt64(xlWorksheet.Cells[i, 8].Text);
                            Status = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                            StartDate = Convert.ToDateTime(xlWorksheet.Cells[i, 5].Text);
                            EndDate = Convert.ToDateTime(xlWorksheet.Cells[i, 6].Text);
                            Remark = Convert.ToString(xlWorksheet.Cells[i, 9].Text); ;
                            int statusID = 0;
                            bool Active = true;
                            if(Status== "Open")
                            {
                                statusID = 5;
                                Active = false;
                            }
                            else if(Status == "Closed-Delayed")
                            {
                                statusID = 1;
                                Active = true;
                            }
                            if (statusID==1 || statusID == 5) {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    RLCS_VendorAuditScheduleOn objUpdateScheduleOn = (from row in entities.RLCS_VendorAuditScheduleOn
                                                                                      where row.ID == ScheduledID
                                                                                      select row).FirstOrDefault();
                                    if (objUpdateScheduleOn != null)
                                    {

                                        objUpdateScheduleOn.ScheduleOnEndDate = EndDate;
                                        objUpdateScheduleOn.ScheduleOn = StartDate;
                                        objUpdateScheduleOn.AuditStatusID = statusID;
                                        objUpdateScheduleOn.Remark = Remark;
                                        objUpdateScheduleOn.IsActive = Active;
                                        entities.SaveChanges();
                                        saveSuccess = true;

                                    }
                                }
                            }
                        
                        }
                        #endregion
                    }
                  

                    if (saveSuccess)
                    {
                        ViewState["SortOrder"] = "Asc";
                        ViewState["SortExpression"] = "ID";
                        long AuditorID = AuthenticationHelper.UserID;
                        BindAuditDetail(AuditorID);
                        BindgrdSubmittedDocumentDetails(UserID);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Uploaded Successfully')", true);
                    }
                }
            }
            catch (Exception ex)
            {
               
            }
        }

        protected void lnkDownloadSubmittedCount_Click(object sender, EventArgs e)
        {
            try
            {

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("SubmittedCountReport");
                    DataTable ExcelData = null;
                    var data = GetDataTable(grdSubmittedDocumentDetails);
                    DataView view = new System.Data.DataView(data);
                    ExcelData = view.ToTable("Selected", false, "Location", "VendorName", "Period", "Open", "Submitted", "Closed", "TeamReview", "Total");

                    //DataView view = new System.Data.DataView((DataTable)Session["grdCalendardetails"]);
                    //ExcelData = view.ToTable("Selected", false, "Customer", "VendorName", "Location", "ForMonth", "StartDate", "EndDate", "ComplianceStatus");

                    #region Lawyer Performance Report

                    exWorkSheet.Cells["A1"].LoadFromDataTable(data, true);
                    exWorkSheet.Cells["A1"].Value = "Location";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].AutoFitColumns(24);
                    exWorkSheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                    exWorkSheet.Cells["B1"].Value = "Vendor Name";
                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].AutoFitColumns(28);
                    exWorkSheet.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["C1"].Value = "Period";
                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].AutoFitColumns(45);
                    exWorkSheet.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["D1"].Value = "Open";
                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].AutoFitColumns(22);
                    exWorkSheet.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663



                    exWorkSheet.Cells["E1"].Value = "Submitted";
                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].AutoFitColumns(22);
                    exWorkSheet.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["F1"].Value = "Closed";
                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].AutoFitColumns(24);
                    exWorkSheet.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["G1"].Value = "Team Review";
                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].AutoFitColumns(24);
                    exWorkSheet.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["H1"].Value = "Total";
                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].AutoFitColumns(24);
                    exWorkSheet.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                   
                    #endregion

                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, count, 8])
                    {
                        col.Style.WrapText = true;
                        col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.Font.Size = 12;
                    }
                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    string locatioName = "VendorAuditSubmittedCount-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename="+ locatioName);//locatioName                
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        DataTable GetDataTable(GridView dtg)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Location", typeof(string));
            dt.Columns.Add("VendorName", typeof(string));
            dt.Columns.Add("Period", typeof(string));
            dt.Columns.Add("Open", typeof(string));
            dt.Columns.Add("Submitted", typeof(string));
            dt.Columns.Add("Closed", typeof(string));
            dt.Columns.Add("TeamReview", typeof(string));
            dt.Columns.Add("Total", typeof(string));
            foreach (TableCell cell in grdSubmittedDocumentDetails.HeaderRow.Cells)
            {
                cell.BackColor = grdSubmittedDocumentDetails.HeaderStyle.BackColor;
            }
            string Location = "", VendorName = "", Period = "", Open = "", Submitted = "", Closed = "", TeamReview = "",Total="";


            var listAllgrdData = (List<SP_RLCS_VendorAudit_GetSubmittedAndPendingDocumentCount_Result>)Session["grdSubmittedDocumentDetails"];
            for (int i = 0; i < listAllgrdData.Count; i++)
            {
                Location = listAllgrdData[i].Branch;
                VendorName = listAllgrdData[i].VendorName;
                Period = listAllgrdData[i].ForMonth;
                Open = listAllgrdData[i].Open.ToString();
                Submitted = listAllgrdData[i].Submitted.ToString();
                Closed = listAllgrdData[i].Closed.ToString();
                TeamReview = listAllgrdData[i].Team_Review.ToString();
                Total= listAllgrdData[i].Total.ToString();
                dt.Rows.Add(Location, VendorName, Period, Open, Submitted, Closed, TeamReview,Total);
            }

            
            return dt;
        }
    }
}