﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCS_CreateAuditContract : System.Web.UI.Page
    {
        protected static string user_Roles;
        protected static string ContractCustomer;        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ContractCustomer = "False";
                    ContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                    user_Roles = AuthenticationHelper.Role;

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        [WebMethod]
        public static string BindClientList()
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ClientDetails> ClientDetailsList = new List<ClientDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var ClientObj = (from Client in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                     where Client.BranchType == "E"
                                     select Client).Distinct().ToList();
                    foreach (var row in ClientObj)
                    {
                        ClientDetails Details = new ClientDetails();
                        Details.ClientID = row.CM_ClientID;
                        Details.ClientName = row.CM_ClientName;
                        Details.CustomerID = row.AVACOM_CustomerID.ToString();
                        ClientDetailsList.Add(Details);
                    }

                    return serializer.Serialize(ClientDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindStateList(string ClientID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstStateList = (from STATE in entities.RLCS_State_Mapping
                                        join RCBM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping on STATE.SM_Code equals RCBM.CM_State
                                        where RCBM.BranchType == "B" && RCBM.CM_ClientID== ClientID
                                        select new StateDetails
                                        {
                                            ID = STATE.AVACOM_StateID,
                                            Name = STATE.SM_Name,
                                            Code = STATE.SM_Code
                                        }).Distinct().ToList();
                    return serializer.Serialize(lstStateList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindLocationList(StateDetails StateObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstStateList = (from Location in entities.RLCS_Location_City_Mapping
                                        join RCBM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping on Location.LM_Code equals RCBM.CM_City
                                        where Location.SM_Code == StateObj.Code && RCBM.CM_ClientID== StateObj.ClientID && RCBM.BranchType == "B"
                                        select new LocationDetails
                                        {
                                            Name = Location.LM_Name,
                                            Code = Location.LM_Code
                                        }).Distinct().ToList();
                    return serializer.Serialize(lstStateList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindBranchList(BranchDetails BranchObj)
        {
            try
            {
                bool flag = false;

                //string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                //bool flag = false;
                string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                if (CheckContractCustomer == "False")
                {
                    flag = true;
                    string checkISTeamlease = RLCSVendorMastersManagement.IsServiceProviderCheck(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (checkISTeamlease == "True")
                    {
                        flag = false;
                    }
                }
                else
                {
                    flag = true;
                }
                if (flag)//CheckContractCustomer == "True"
                {
                    string Branchtype = "B";
                    //long BranchId = Convert.ToInt64(BranchObj.ID);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstbranchList = (from branch in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                             where branch.BranchType == Branchtype 
                                             && branch.CM_ClientID == BranchObj.ClientID 
                                             select new BranchDetails
                                             {
                                                 Name = branch.AVACOM_BranchName,
                                                 ID = branch.AVACOM_BranchID
                                             }).Distinct().ToList();
                        return serializer.Serialize(lstbranchList);
                    }
                }
                else
                {
                    string Branchtype = "B";
                    long BranchId = Convert.ToInt64(BranchObj.ID);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstbranchList = (from branch in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                             where branch.BranchType == Branchtype && branch.CM_State == BranchObj.Code && branch.CM_ClientID == BranchObj.ClientID && branch.CM_City == BranchObj.LocationCode
                                             select new BranchDetails
                                             {
                                                 Name = branch.AVACOM_BranchName,
                                                 ID = branch.AVACOM_BranchID
                                             }).Distinct().ToList();
                        return serializer.Serialize(lstbranchList);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindAuditorList(string ClientID)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var CustomerObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                       where SPOC.BranchType == "E" && SPOC.CM_ClientID == ClientID
                                       select SPOC).FirstOrDefault();
                    //int CustomerID = Convert.ToInt32(CustomerObj.AVACOM_CustomerID);
                    int CustomerID = Convert.ToInt32(CustomerObj.ID);
                    var lstAuditorList = (from RVAS in entities.Users
                                          where RVAS.VendorRoleID == 22
                                          && RVAS.CustomerID == CustomerID
                                          select new AuditDetails
                                          {
                                              Name = RVAS.FirstName,
                                              ID = RVAS.ID
                                          }).Distinct().ToList();
                    return serializer.Serialize(lstAuditorList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string SaveContractDetails(AuditContractDetails DetailsObj)
        {
            try
            {
                bool Success = false;
                DateTime StartDate = GetDate(DetailsObj.CC_ContractFrom);
                DateTime EndDate = GetDate(DetailsObj.CC_ContractTo);
                long AuditID = DetailsObj.ID;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    RLCS_VendorAuditInstance updateDetails = (from row in entities.RLCS_VendorAuditInstance
                                                              where row.ID == AuditID
                                                              select row).FirstOrDefault();
                    int ContMasterID = Convert.ToInt32(DetailsObj.ContractorMasterID);
                    var SPOCDetails = (from SPOC in entities.RLCS_VendorContractorMaster
                                       where SPOC.ID == ContMasterID
                                       select SPOC).FirstOrDefault();
                    string EmailID = SPOCDetails.SPOCEmailID;

                    var VendorObj = (from user in entities.Users
                                     where user.Email == EmailID
                                     select user).FirstOrDefault();
                    long VendorID = VendorObj.ID;
                    var CustomerObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                       where SPOC.BranchType == "E" && SPOC.CM_ClientID == DetailsObj.CC_ClientID
                                       select SPOC).FirstOrDefault();
                    int CustomerID = Convert.ToInt32(CustomerObj.AVACOM_CustomerID);

                    if (updateDetails != null)
                    {
                        updateDetails.CC_Month = DateTime.Now.Month;
                        updateDetails.CC_Year = DateTime.Now.Year;
                        updateDetails.CC_ClientID = DetailsObj.CC_ClientID;
                        updateDetails.CC_ContracterID = DetailsObj.CC_ContracterID;
                        updateDetails.CC_ContractorName = DetailsObj.CC_ContractorName;
                        updateDetails.CC_ContractorAddress = DetailsObj.CC_ContractorAddress;
                        updateDetails.CC_StateID = DetailsObj.CC_StateID;
                        updateDetails.CC_BranchID = DetailsObj.CC_BranchID;
                        updateDetails.CC_ContractFrom = StartDate;
                        updateDetails.CC_ContractTo = EndDate;
                        //updateDetails.CC_NoOfJoinees = DetailsObj.CC_NoOfJoinees;
                        updateDetails.CC_NoOfJoinees = DetailsObj.CC_MaleHeadCount + DetailsObj.CC_FeMaleHeadCount;
                        updateDetails.CC_NatureOfBusiness = DetailsObj.CC_NatureOfBusiness;
                        updateDetails.CC_EffectiveFromMonth = StartDate.Month;
                        updateDetails.CC_EffectiveFromYear = StartDate.Year;
                        updateDetails.CC_Status = DetailsObj.CC_Status;
                        updateDetails.CC_EffectiveToMonth = EndDate.Month;
                        updateDetails.CC_EffectiveToYear = EndDate.Year;
                        updateDetails.CC_CreatedBy = DetailsObj.CC_CreatedBy;
                        updateDetails.CC_CreatedDate = DetailsObj.CC_CreatedDate;
                        updateDetails.CC_ModifiedBy = DetailsObj.CC_ModifiedBy;
                        updateDetails.CC_ModifiedDate = DetailsObj.CC_ModifiedDate;
                        updateDetails.CC_Version = DetailsObj.CC_Version;
                        updateDetails.CC_MaleHeadCount = DetailsObj.CC_MaleHeadCount;
                        updateDetails.CC_FeMaleHeadCount = DetailsObj.CC_FeMaleHeadCount;
                        updateDetails.CC_ESIC_Number = DetailsObj.CC_ESIC_Number;
                        updateDetails.CC_PT_State = DetailsObj.CC_PT_State;
                        updateDetails.CC_PF_Code = DetailsObj.CC_PF_Code;
                        updateDetails.CC_SPOC_Name = SPOCDetails.SPOCName;
                        updateDetails.CC_SPOC_Email = SPOCDetails.SPOCEmailID;
                        updateDetails.CC_SPOC_Mobile = SPOCDetails.SPOCMobileNo;
                        updateDetails.CC_Auditor = "T" + DetailsObj.CC_Auditor;
                        updateDetails.AVACOM_CustomerID = CustomerID;
                        updateDetails.AVACOM_BranchID = DetailsObj.AVACOM_BranchID;
                        updateDetails.AVACOM_AuditorID = Convert.ToInt32(DetailsObj.CC_Auditor);
                        updateDetails.AVACOM_VendorID = Convert.ToInt32(VendorID);
                        updateDetails.CC_Frequency = DetailsObj.CC_Frequency;
                        updateDetails.CC_Location = DetailsObj.CC_Location;
                        updateDetails.DueDate = DetailsObj.DueDate;
                        updateDetails.DepartmentType = DetailsObj.DepartmentType;
                        updateDetails.ContractMasterID = ContMasterID;
                        updateDetails.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                        Success = true;


                    }
                    else
                    {
                        string StateID = string.Empty;
                        string LocationID = string.Empty;

                        bool flag = false;
                        string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                        if (CheckContractCustomer == "False")
                        {
                            flag = true;
                            string checkISTeamlease = RLCSVendorMastersManagement.IsServiceProviderCheck(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (checkISTeamlease == "True")
                            {
                                flag = false;
                            }
                        }
                        else
                        {
                            flag = true;
                        }
                        //string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                        if (flag)//CheckContractCustomer == "True"
                        {
                            int branchID = Convert.ToInt32(DetailsObj.AVACOM_BranchID);

                            var CustomerbranchObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                     where SPOC.BranchType == "B"
                                                     && SPOC.CM_ClientID == DetailsObj.CC_ClientID
                                                     && SPOC.AVACOM_BranchID == branchID
                                                     && SPOC.CM_Status.Equals("A")
                                                     select SPOC).FirstOrDefault();
                            if (CustomerbranchObj != null)
                            {
                                StateID = CustomerbranchObj.CM_State;
                                LocationID = CustomerbranchObj.CM_City;
                            }
                        }
                        else
                        {
                            StateID = DetailsObj.CC_StateID;
                            LocationID = DetailsObj.CC_Location;
                        }
                        RLCS_VendorAuditInstance vendorMaster = new RLCS_VendorAuditInstance()
                        {
                            CC_Month = DateTime.Now.Month,
                            CC_Year = DateTime.Now.Year,
                            CC_ClientID = DetailsObj.CC_ClientID,
                            CC_ContracterID = DetailsObj.CC_ContracterID,
                            CC_ContractorName = DetailsObj.CC_ContractorName,
                            CC_ContractorAddress = DetailsObj.CC_ContractorAddress,
                            CC_StateID = StateID,
                            CC_BranchID = DetailsObj.CC_BranchID,
                            CC_ContractFrom = StartDate,
                            CC_ContractTo = EndDate,
                            CC_NoOfJoinees = DetailsObj.CC_MaleHeadCount + DetailsObj.CC_FeMaleHeadCount,
                            CC_NatureOfBusiness = DetailsObj.CC_NatureOfBusiness,
                            CC_EffectiveFromMonth = StartDate.Month,
                            CC_EffectiveFromYear = StartDate.Year,
                            CC_Status = DetailsObj.CC_Status,
                            CC_EffectiveToMonth = EndDate.Month,
                            CC_EffectiveToYear = EndDate.Year,
                            CC_CreatedBy = DetailsObj.CC_CreatedBy,
                            CC_CreatedDate = DetailsObj.CC_CreatedDate,
                            CC_Version = DetailsObj.CC_Version,
                            CC_MaleHeadCount = DetailsObj.CC_MaleHeadCount,
                            CC_FeMaleHeadCount = DetailsObj.CC_FeMaleHeadCount,
                            CC_ESIC_Number = DetailsObj.CC_ESIC_Number,
                            CC_PT_State = DetailsObj.CC_PT_State,
                            CC_PF_Code = DetailsObj.CC_PF_Code,
                            CC_SPOC_Name = SPOCDetails.SPOCName,
                            CC_SPOC_Email = SPOCDetails.SPOCEmailID,
                            CC_SPOC_Mobile = SPOCDetails.SPOCMobileNo,
                            CC_Auditor = "T" + (DetailsObj.CC_Auditor),
                            CC_Frequency = DetailsObj.CC_Frequency,
                            CC_Location = LocationID,
                            AVACOM_BranchID = DetailsObj.AVACOM_BranchID,
                            AVACOM_CustomerID = CustomerID,
                            AVACOM_AuditorID = Convert.ToInt32(DetailsObj.CC_Auditor),
                            AVACOM_VendorID = Convert.ToInt32(VendorID),
                            DueDate = Convert.ToInt32(DetailsObj.DueDate),
                            DepartmentType = DetailsObj.DepartmentType,
                            ContractMasterID = ContMasterID,
                            CreatedOn = DateTime.Now
                        };


                        entities.RLCS_VendorAuditInstance.Add(vendorMaster);
                        entities.SaveChanges();
                        Success = true;


                    }
                    return serializer.Serialize(Success);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindClientWiseContractor(AuditContractDetails DetailsObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ClientWiseContractDetails> ContractorDetailsList = new List<ClientWiseContractDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var ContractorObj = (from Contractor in entities.RLCS_VendorAuditInstance
                                         join state in entities.RLCS_State_Mapping on Contractor.CC_StateID equals state.SM_Code
                                         where Contractor.CC_ClientID == DetailsObj.CC_ClientID && Contractor.ContractMasterID == DetailsObj.ContractorMasterID
                                         select new { Contractor.ID, Contractor.CC_BranchID, state.SM_Name, Contractor.CC_NatureOfBusiness, Contractor.AVACOM_AuditorID, Contractor.CC_Status, Contractor.CC_Location }).ToList();

                    foreach (var row in ContractorObj)
                    {
                        string AuditorName = "";
                        string LocationName = "";
                        if (row.AVACOM_AuditorID != null)
                        {
                            int id = Convert.ToInt32(row.AVACOM_AuditorID);
                            var AuditorNameobj = (from auditor in entities.Users
                                                  where auditor.ID == id
                                                  select auditor).FirstOrDefault();

                            if (AuditorNameobj != null)
                                AuditorName = AuditorNameobj.FirstName +" "+ AuditorNameobj.LastName;
                        }

                        if (row.CC_Location != null)
                        {
                           // int id = Convert.ToInt32(row.CC_Location);
                            var LocationNameobj = (from location in entities.RLCS_Location_City_Mapping
                                                   where location.LM_Code == row.CC_Location
                                                   select location).FirstOrDefault();

                            if (LocationNameobj != null)
                                LocationName = LocationNameobj.LM_Name;
                        }

                        ClientWiseContractDetails Details = new ClientWiseContractDetails();
                        Details.ID = row.ID;
                        Details.StateName = row.SM_Name;
                        Details.LocationName = LocationName;
                        Details.BranchName = row.CC_BranchID;
                        Details.AuditorName = AuditorName;
                        Details.NatureOfService = row.CC_NatureOfBusiness;
                        if (row.CC_Status == "A")
                        {
                            Details.Status = "Active";

                        }
                        else
                        {
                            Details.Status = "Inactive";
                        }

                        ContractorDetailsList.Add(Details);
                    }

                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string GetContractorAuditDetails(AuditContractDetails DetailsObj)
        {
            try
            {
                bool Success = false;

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var ContractorObj = (from Contractor in entities.RLCS_VendorAuditInstance
                                         where Contractor.ID == DetailsObj.ID
                                         select Contractor).FirstOrDefault();
                    var CustomerNameObj = (from Customer in entities.Customers
                                           where Customer.ID == ContractorObj.AVACOM_CustomerID
                                           select Customer).FirstOrDefault();

                    string CustomerName = "";
                    if (CustomerNameObj != null)
                    {
                        CustomerName = CustomerNameObj.Name;
                    }


                    AuditContractDetails Details = new AuditContractDetails();
                    Details.ID = ContractorObj.ID;
                    Details.CC_Frequency = ContractorObj.CC_Frequency;
                    Details.DueDate = ContractorObj.DueDate;
                    Details.CC_ContractorName = ContractorObj.CC_ContractorName;
                    Details.CustomerName = CustomerName;
                    Details.CC_StateID = ContractorObj.CC_StateID;
                    Details.CC_Location = ContractorObj.CC_Location;
                    Details.AVACOM_BranchID = ContractorObj.AVACOM_BranchID;
                    Details.CC_ContractorAddress = ContractorObj.CC_ContractorAddress;
                    Details.CC_ContractFrom = ContractorObj.CC_ContractFrom.ToString();
                    Details.CC_ContractTo = ContractorObj.CC_ContractTo.ToString();
                    Details.CC_Auditor = ContractorObj.AVACOM_AuditorID.ToString();
                    Details.CC_NatureOfBusiness = ContractorObj.CC_NatureOfBusiness;
                    Details.CC_NatureOfBusiness = ContractorObj.CC_NatureOfBusiness;
                    Details.CC_MaleHeadCount = ContractorObj.CC_MaleHeadCount;
                    Details.CC_FeMaleHeadCount = ContractorObj.CC_FeMaleHeadCount;
                    Details.CC_Status = ContractorObj.CC_Status;
                    Details.CC_ESIC_Number = ContractorObj.CC_ESIC_Number;
                    Details.CC_PF_Code = ContractorObj.CC_PF_Code;
                    Details.CC_PT_State = ContractorObj.CC_PT_State;
                    Details.DepartmentType = ContractorObj.DepartmentType;

                    return serializer.Serialize(Details);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        //private void ProcessChecklistData(ExcelPackage xlWorkbook)
        //{
        //    try
        //    {
        //        bool saveSuccess = false;

        //        int uploadedVendorCount = 0;

        //        ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RLCSVendorCheckList"];

        //        if (xlWorksheet != null)
        //        {
        //            List<string> errorMessage = new List<string>();

        //            int xlrow2 = xlWorksheet.Dimension.End.Row;

        //            string ClientID = string.Empty;
        //            string Vendor = string.Empty;
        //            string StateId = string.Empty;
        //            string NatureOfCompliance = string.Empty;
        //            string Risk = string.Empty;
        //            string Description = string.Empty;

        //            var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, "HVADM");
        //            #region Validations

        //            for (int i = 2; i <= xlrow2; i++)
        //            {
        //                #region 1 ActId,ChecklistId,StateID and Nature Of Compliance

        //                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim())
        //                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim())
        //                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim())
        //                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim())
        //                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim())
        //                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
        //                {
        //                    ActId = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
        //                    ChecklistId = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
        //                    StateId = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
        //                    NatureOfCompliance = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
        //                    Description = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();

        //                    bool actExist = RLCSVendorMastersManagement.ExistsActData(ActId);
        //                    bool stateExist = false;
        //                    if (StateId.Trim().ToUpper().Equals("CENTRAL"))
        //                    {
        //                        stateExist = true;
        //                    }
        //                    else
        //                    {
        //                        stateExist = RLCSVendorMastersManagement.ExistsStateData(StateId);
        //                    }

        //                    if (actExist && stateExist)
        //                    {
        //                        bool matchSuccess = true;
        //                     //   matchSuccess = checkDuplicateMultipleDataExistExcelSheet(xlWorksheet, i, ActId, StateId, NatureOfCompliance);
        //                        if (matchSuccess)
        //                        {
        //                            errorMessage.Add("Nature of Compliance at Row - " + i + " Exists Multiple Times For Nature of Compliance in the Uploaded Excel Document");
        //                        }
        //                        bool existVendor = RLCSVendorMastersManagement.ExistsChecklistData(ActId, StateId, NatureOfCompliance);
        //                        if (existVendor)
        //                        {
        //                            errorMessage.Add("Nature of Compliance already exists at row number - " + i + "");
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (!actExist)
        //                        {
        //                            errorMessage.Add("ActID-" + ActId + " not exists at row number - " + i + "");
        //                        }
        //                        if (!stateExist)
        //                        {
        //                            errorMessage.Add("State Name not exists at row number - " + i + "");
        //                        }
        //                    }
        //                }
        //                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
        //                {
        //                    errorMessage.Add("Required ActID at row number" + i + ".");
        //                }
        //                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
        //                {
        //                    errorMessage.Add("Required CheckListID at row number" + i + ".");
        //                }
        //                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
        //                {
        //                    errorMessage.Add("Required StateID at row number" + i + ".");
        //                }
        //                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
        //                {
        //                    errorMessage.Add("Required Nature of Compliance at row number" + i + ".");
        //                }
        //                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
        //                {
        //                    errorMessage.Add("Required Description at row number" + i + ".");
        //                }
        //                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
        //                {
        //                    errorMessage.Add("Required Risk at row number" + i + ".");
        //                }
        //                else
        //                {
        //                    if (xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "HIGH"
        //                        || xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "MEDIUM"
        //                        || xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "LOW")
        //                    {

        //                    }
        //                    else
        //                    {
        //                        errorMessage.Add("Required Risk at row number" + i + ".");
        //                    }
        //                }
        //                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 10].Text).Trim()))
        //                {
        //                    errorMessage.Add("Required Types of Compliance at row number" + i + ".");
        //                }
        //                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 14].Text).Trim()))
        //                {
        //                    lstCustomers.Select(e => e.Name == (xlWorksheet.Cells[i, 14].Text).Trim()).FirstOrDefault();
        //                    if (lstCustomers == null)
        //                        errorMessage.Add("Customer should be blank or not matching with value at " + i + ".");
        //                }
        //                #endregion
        //            }

        //            #endregion

        //            string ContractTypeNameFlag = string.Empty;

        //            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);

        //            if (errorMessage.Count <= 0)
        //            {
        //                #region Save                        
        //                for (int i = 2; i <= xlrow2; i++)
        //                {
        //                    ActId = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
        //                    ChecklistId = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
        //                    StateId = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
        //                    NatureOfCompliance = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
        //                    //Risk = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
        //                    int? customerid = null;
        //                    if (xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "HIGH")
        //                    {
        //                        Risk = "High";
        //                    }
        //                    else if (xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "MEDIUM")
        //                    {
        //                        Risk = "Medium";
        //                    }
        //                    else if (xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "LOW")
        //                    {
        //                        Risk = "Low";
        //                    }
        //                    try
        //                    {
        //                        customerid = lstCustomers.Where(e => e.Name == (xlWorksheet.Cells[i, 14].Text).Trim()).FirstOrDefault().ID;
        //                    }
        //                    catch (Exception e) { }

        //                    RLCS_tbl_CheckListMaster objchklst = new RLCS_tbl_CheckListMaster()
        //                    {
        //                        ActID = ActId,
        //                        ChecklistID = ChecklistId,
        //                        StateID = StateId,
        //                        NatureOfCompliance = NatureOfCompliance,
        //                        Section = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim(),
        //                        Checklist_Rule = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim(),
        //                        Form = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim(),
        //                        TypeOfCompliance = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim(),
        //                        Frequency = Convert.ToString(xlWorksheet.Cells[i, 11].Text).Trim(),
        //                        Type = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim(),
        //                        CreatedOn = DateTime.Now,
        //                        CreatedBy = UserID,
        //                        IsDeleted = false,
        //                        Risk = Risk,
        //                        Description = Description,
        //                        Consequences = Convert.ToString(xlWorksheet.Cells[i, 13].Text).Trim(),
        //                        customerid = customerid,

        //                    };
        //                    RLCSVendorMastersManagement.saveCheckListDetail(objchklst);
        //                    uploadedVendorCount++;
        //                    saveSuccess = true;
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                if (errorMessage.Count > 0)
        //                {
        //                    ErrorMessages(errorMessage);
        //                }
        //            }

        //            if (saveSuccess)
        //            {
        //                if (uploadedVendorCount > 0)
        //                {

        //                    cvUploadUtilityPage.IsValid = false;
        //                    cvUploadUtilityPage.ErrorMessage = uploadedVendorCount + " Checklist Details Uploaded Successfully";
        //                    cvUploadUtilityPage.CssClass = "alert alert-success";
        //                }
        //                else
        //                {
        //                    cvUploadUtilityPage.IsValid = false;
        //                    cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvUploadUtilityPage.IsValid = false;
        //        cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
        //    }
        //}

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        public static DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public class AuditDetails
        {
            public long ID { get; set; }
            public string Name { get; set; }
        }

        public class StateDetails
        {
            public long ID { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public string ClientID { get; set; }

        }
        public class LocationDetails
        {
            public long? ID { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }

        }

        public class BranchDetails
        {
            public int? ID { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string ClientID { get; set; }
            public string LocationCode { get; set; }
        }

        public class AuditContractDetails
        {
            public long ID { get; set; }
            public int ContractorMasterID { get; set; }
            public int CC_Month { get; set; }
            public int CC_Year { get; set; }
            public string CC_ClientID { get; set; }
            public string CustomerName { get; set; }
            public string CC_ContracterID { get; set; }
            public string CC_ContractorName { get; set; }
            public string CC_ContractorAddress { get; set; }
            public string CC_StateID { get; set; }
            public string CC_BranchID { get; set; }
            public string CC_ContractFrom { get; set; }
            public string CC_ContractTo { get; set; }
            public int CC_NoOfJoinees { get; set; }
            public string CC_NatureOfBusiness { get; set; }
            public int CC_EffectiveFromMonth { get; set; }
            public int CC_EffectiveFromYear { get; set; }
            public string CC_Status { get; set; }
            public int CC_EffectiveToMonth { get; set; }
            public int CC_EffectiveToYear { get; set; }
            public string CC_CreatedBy { get; set; }
            public DateTime CC_CreatedDate { get; set; }
            public string CC_ModifiedBy { get; set; }
            public DateTime CC_ModifiedDate { get; set; }
            public int CC_Version { get; set; }
            public int? CC_MaleHeadCount { get; set; }
            public int? CC_FeMaleHeadCount { get; set; }
            public string CC_ESIC_Number { get; set; }
            public string CC_PT_State { get; set; }
            public string CC_PF_Code { get; set; }
            public string CC_SPOC_Name { get; set; }
            public string CC_SPOC_Email { get; set; }
            public string CC_SPOC_Mobile { get; set; }
            public string CC_Auditor { get; set; }
            public string CC_Frequency { get; set; }
            public int? AVACOM_BranchID { get; set; }
            public string CC_Location { get; set; }
            public string DepartmentType { get; set; }
            public int? AVACOM_CustomerID { get; set; }
            public int DueDate { get; set; }
            public DateTime CreatedOn { get; set; }

        }

        public class ClientWiseContractDetails
        {
            public int ID { get; set; }
            public string StateName { get; set; }
            public string LocationName { get; set; }
            public string BranchName { get; set; }
            public string AuditorName { get; set; }
            public string Department { get; set; }
            public string NatureOfService { get; set; }
            public string Status { get; set; }

        }

        public class ClientDetails
        {
            public string ClientID { get; set; }
            public string CustomerID { get; set; }
            public string ClientName { get; set; }

        }

        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            if (ContractFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(ContractFileUpload.FileName);
                    ContractFileUpload.SaveAs(Server.MapPath("~/RLCSVendorAudit/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/RLCSVendorAudit/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "ContractBulkUpload");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ContractBulkUpload'.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }

                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool flagusermapping = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int ContMasterID = Convert.ToInt32(Request.QueryString["ContractorID"]);

                    int CID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var SPOCDetails = (from SPOC in entities.RLCS_VendorContractorMaster
                                       where SPOC.ID == ContMasterID
                                       select SPOC).FirstOrDefault();
                    if (SPOCDetails != null)
                    {
                        string EmailID = SPOCDetails.SPOCEmailID;

                        var VendorObj = (from user in entities.Users
                                         where user.Email == EmailID
                                         && user.CustomerID == CID
                                         && user.IsActive == true
                                         && user.IsDeleted == false
                                         select user).FirstOrDefault();
                        if (VendorObj != null)
                        {
                            flagusermapping = true;
                        }
                    }
                }
                if (flagusermapping)
                {
                    bool saveSuccess = false;

                    int uploadedVendorCount = 0;

                    ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ContractBulkUpload"];

                    if (xlWorksheet != null)
                    {
                        List<string> errorMessage = new List<string>();

                        int xlrow2 = xlWorksheet.Dimension.End.Row;

                        string Location = string.Empty;
                        string LocationCode = string.Empty;
                        string Branch = string.Empty;
                        string AuditorName = string.Empty;
                        string FromDate = string.Empty;
                        string ToDate = string.Empty;


                        string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                        if (CheckContractCustomer == "False")
                        {
                            var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, "HVADM");
                            #region Validations

                            for (int i = 2; i <= xlrow2; i++)
                            {

                                #region 1 DueDate,StateID,Location , Branch,Address,ContractFrom,ContractTo,Auditor

                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim())
                                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim())
                                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim())
                                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim())
                                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim())
                                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim())
                                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim())
                                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim())
                                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                {
                                    Location = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                                    Branch = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                                    FromDate = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                    ToDate = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();

                                    if (true)
                                    {
                                        bool matchSuccess = false;
                                        matchSuccess = checkDuplicateMultipleDataExistExcelSheet(xlWorksheet, i, Location, Branch, FromDate, ToDate);
                                        if (matchSuccess)
                                        {
                                            errorMessage.Add("Location, branch at Row - " + i + " Exists Multiple Times For From Date and To Date in the Uploaded Excel Document");
                                        }

                                    }

                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                                {
                                    errorMessage.Add("Required Due Date at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                {
                                    errorMessage.Add("Required State ID at row number" + i + ".");
                                }
                                else
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        string stateCode = (xlWorksheet.Cells[i, 2].Text).Trim();
                                        var LocationDetails = (from state in entities.RLCS_State_Mapping
                                                               where state.SM_Code == stateCode
                                                                 && state.SM_Status == "A"
                                                               select state).FirstOrDefault();
                                        if (LocationDetails == null)
                                        {
                                            errorMessage.Add("State ID Does not Exists at row number" + i + ".");
                                        }

                                    }
                                }

                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                {
                                    errorMessage.Add("Required Location at row number" + i + ".");
                                }
                                else
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        string locationanme = (xlWorksheet.Cells[i, 3].Text).Trim();
                                        string stateCode = (xlWorksheet.Cells[i, 2].Text).Trim();
                                        var LocationDetails = (from Lctn in entities.RLCS_Location_City_Mapping
                                                               where Lctn.LM_Name == locationanme
                                                               && Lctn.SM_Code == stateCode
                                                               && Lctn.LM_Status == "A"
                                                               select Lctn).FirstOrDefault();
                                        if (LocationDetails == null)
                                        {
                                            errorMessage.Add("Location Name Does not Exists at row number" + i + ".");
                                        }
                                        else
                                        {
                                            LocationCode = LocationDetails.LM_Code;
                                        }
                                    }
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                                {
                                    errorMessage.Add("Required Branch at row number" + i + ".");
                                }
                                else
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {


                                        string BranchName = (xlWorksheet.Cells[i, 4].Text).Trim();
                                        string ClientID = Request.QueryString["ClientID"].ToString();
                                        string stateCode = (xlWorksheet.Cells[i, 2].Text).Trim();

                                        var CustomerObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                           where SPOC.BranchType == "E" && SPOC.CM_ClientID == ClientID
                                                           select SPOC).FirstOrDefault();
                                        int CustomerID = Convert.ToInt32(CustomerObj.AVACOM_CustomerID);

                                        var BranchDetails = (from branch in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                             where branch.AVACOM_BranchName == BranchName && branch.CM_ClientID == ClientID && branch.BranchType == "B" && branch.CM_State == stateCode && branch.CM_City == LocationCode
                                                             && branch.AVACOM_CustomerID == CustomerID
                                                             select branch).FirstOrDefault();
                                        if (BranchDetails == null)
                                        {
                                            errorMessage.Add("Branch Name Does not Exists against state and location at row number" + i + ".");
                                        }
                                    }
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                                {
                                    errorMessage.Add("Required Address at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                                {
                                    errorMessage.Add("Required Contract From Date at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                                {
                                    errorMessage.Add("Required Contract To Date at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 8].Text).Trim()))
                                {
                                    errorMessage.Add("Required Auditor at row number" + i + ".");
                                }
                                else
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        string ClientID = Request.QueryString["ClientID"].ToString();
                                        var CustomerObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                           where SPOC.BranchType == "E" && SPOC.CM_ClientID == ClientID
                                                           select SPOC).FirstOrDefault();
                                        int CustomerID = Convert.ToInt32(CustomerObj.AVACOM_CustomerID);

                                        string AudiotrName = (xlWorksheet.Cells[i, 8].Text).Trim();
                                        var AuditDetails = (from auditor in entities.Users
                                                            where auditor.Email == AudiotrName
                                                            && auditor.VendorRoleID == 22
                                                              && auditor.CustomerID == CustomerID
                                                         && auditor.IsActive == true
                                                         && auditor.IsDeleted == false
                                                            select auditor).FirstOrDefault();
                                        if (AuditDetails == null)
                                        {
                                            errorMessage.Add("Auditor Name Does not Exists at row number" + i + ".");
                                        }
                                    }
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 9].Text).Trim()))
                                {
                                    errorMessage.Add("Required Nature of Business at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 10].Text).Trim()))
                                {
                                    errorMessage.Add("Required Male Count  at row number" + i + ".");

                                }
                                else
                                {
                                    try
                                    {
                                        int MaleCount = Convert.ToInt32((xlWorksheet.Cells[i, 10].Text).Trim());
                                    }
                                    catch { errorMessage.Add("Male Count Should be number" + i + "."); }

                                }

                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 11].Text).Trim()))
                                {
                                    errorMessage.Add("Required Female Count  at row number" + i + ".");
                                }
                                else
                                {
                                    try
                                    {
                                        int FemaleCount = Convert.ToInt32((xlWorksheet.Cells[i, 11].Text).Trim());
                                    }
                                    catch { errorMessage.Add("Female Count Should be number" + i + "."); }

                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 15].Text).Trim()))
                                {

                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        string DepartmentName = (xlWorksheet.Cells[i, 15].Text).Trim();
                                        var DeptDetails = (from dept in entities.Vendor_Department
                                                           where dept.Name == DepartmentName
                                                           && dept.IsDeleted == false
                                                           select dept).FirstOrDefault();
                                        if (DeptDetails == null)
                                        {
                                            errorMessage.Add("Deparmtent Name not valid at row number" + i + ".");
                                        }
                                    }

                                }

                                #endregion
                            }
                            #endregion

                            string ContractTypeNameFlag = string.Empty;

                            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);

                            if (errorMessage.Count <= 0)
                            {
                                #region Save                        
                                for (int i = 2; i <= xlrow2; i++)
                                {
                                    Location = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                                    Branch = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                                    FromDate = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                    ToDate = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                    AuditorName = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                                    DateTime StartDate = GetDate(FromDate);
                                    DateTime EndDate = GetDate(ToDate);


                                    string DepartmentID = null;
                                    if (xlWorksheet.Cells[i, 15].Text.Trim() == "Admin")
                                    {
                                        DepartmentID = "1";
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.Trim() == "HR")
                                    {
                                        DepartmentID = "2";
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.Trim() == "Support")
                                    {
                                        DepartmentID = "3";
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.Trim() == "Finance")
                                    {
                                        DepartmentID = "4";
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.Trim() == "IT")
                                    {
                                        DepartmentID = "5";
                                    }
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        int ContMasterID = Convert.ToInt32(Request.QueryString["ContractorID"]);
                                        string ClientID = Request.QueryString["ClientID"].ToString();

                                        var CustomerObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                           where SPOC.BranchType == "E" && SPOC.CM_ClientID == ClientID
                                                           select SPOC).FirstOrDefault();
                                        int CustomerID = Convert.ToInt32(CustomerObj.AVACOM_CustomerID);

                                        string LocationId = null;
                                        var LocationDetails = (from Lctn in entities.RLCS_Location_City_Mapping
                                                               where Lctn.LM_Name == Location && Lctn.LM_Status == "A"
                                                               select Lctn).FirstOrDefault();
                                        if (LocationDetails != null)
                                            LocationId = LocationDetails.LM_Code.ToString();

                                        int BranchId = 0;
                                        var BranchDetails = (from branch in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                             where branch.AVACOM_BranchName == Branch
                                                             && branch.AVACOM_CustomerID == CustomerID
                                                             && branch.CM_ClientID == ClientID
                                                             select branch).FirstOrDefault();

                                        if (BranchDetails != null)
                                            BranchId = Convert.ToInt32(BranchDetails.AVACOM_BranchID);

                                        int AuditId = 0;
                                        var AuditDetails = (from auditor in entities.Users
                                                            where auditor.Email == AuditorName
                                                            && auditor.VendorRoleID == 22
                                                              && auditor.CustomerID == CustomerID
                                                         && auditor.IsActive == true
                                                         && auditor.IsDeleted == false
                                                            select auditor).FirstOrDefault();
                                        if (AuditDetails != null)
                                            AuditId = Convert.ToInt32(AuditDetails.ID);


                                        var SPOCDetails = (from SPOC in entities.RLCS_VendorContractorMaster
                                                           where SPOC.ID == ContMasterID
                                                           select SPOC).FirstOrDefault();
                                        string EmailID = SPOCDetails.SPOCEmailID;

                                        var VendorObj = (from user in entities.Users
                                                         where user.Email == EmailID
                                                         && user.CustomerID == CustomerID
                                                         && user.IsActive == true
                                                         && user.IsDeleted == false
                                                         select user).FirstOrDefault();
                                        long VendorID = VendorObj.ID;

                                        RLCS_VendorAuditInstance ContractMaster = new RLCS_VendorAuditInstance()
                                        {
                                            CC_Month = DateTime.Now.Month,
                                            CC_Year = DateTime.Now.Year,
                                            CC_ClientID = ClientID,
                                            CC_ContracterID = "",
                                            CC_ContractorName = SPOCDetails.ContractorName,
                                            CC_ContractorAddress = xlWorksheet.Cells[i, 5].Text.Trim(),
                                            CC_StateID = xlWorksheet.Cells[i, 2].Text.Trim(),
                                            CC_BranchID = xlWorksheet.Cells[i, 4].Text.Trim(),
                                            CC_ContractFrom = StartDate,
                                            CC_ContractTo = EndDate,
                                            CC_NoOfJoinees = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text.Trim()) + Convert.ToInt32(xlWorksheet.Cells[i, 11].Text.Trim()),
                                            CC_NatureOfBusiness = xlWorksheet.Cells[i, 9].Text.Trim(),
                                            CC_EffectiveFromMonth = StartDate.Month,
                                            CC_EffectiveFromYear = StartDate.Year,
                                            CC_Status = "A",
                                            CC_EffectiveToMonth = EndDate.Month,
                                            CC_EffectiveToYear = EndDate.Year,
                                            CC_CreatedBy = "Admin",
                                            CC_CreatedDate = DateTime.Now,
                                            CC_Version = 0,
                                            CC_MaleHeadCount = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text.Trim()),
                                            CC_FeMaleHeadCount = Convert.ToInt32(xlWorksheet.Cells[i, 11].Text.Trim()),
                                            CC_ESIC_Number = xlWorksheet.Cells[i, 12].Text.Trim(),
                                            CC_PT_State = xlWorksheet.Cells[i, 14].Text.Trim(),
                                            CC_PF_Code = xlWorksheet.Cells[i, 13].Text.Trim(),
                                            CC_SPOC_Name = SPOCDetails.SPOCName,
                                            CC_SPOC_Email = SPOCDetails.SPOCEmailID,
                                            CC_SPOC_Mobile = SPOCDetails.SPOCMobileNo,
                                            CC_Auditor = "T" + (AuditId),
                                            CC_Frequency = "A",
                                            CC_Location = LocationId,
                                            AVACOM_BranchID = BranchId,
                                            AVACOM_CustomerID = CustomerID,
                                            AVACOM_AuditorID = AuditId,
                                            AVACOM_VendorID = Convert.ToInt32(VendorID),
                                            DueDate = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim()),
                                            DepartmentType = DepartmentID,
                                            ContractMasterID = ContMasterID,
                                            CreatedOn = DateTime.Now
                                        };


                                        entities.RLCS_VendorAuditInstance.Add(ContractMaster);
                                        entities.SaveChanges();

                                        uploadedVendorCount++;
                                        saveSuccess = true;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                if (errorMessage.Count > 0)
                                {
                                    ErrorMessages(errorMessage);
                                }
                            }
                        }
                        else
                        {
                            var lstCustomers = RLCSVendorMastersManagement.GetAllClientWise(AuthenticationHelper.UserID, "HVADM", AuthenticationHelper.CustomerID);
                            //var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, "HVADM");
                            #region Validations

                            for (int i = 2; i <= xlrow2; i++)
                            {

                                #region 1 DueDate,StateID,Location , Branch,Address,ContractFrom,ContractTo,Auditor

                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim())
                                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim())
                                    //&& !String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim())
                                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim())
                                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim())
                                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim())
                                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim())
                                     && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim())
                                    && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                {
                                    //Location = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                                    Branch = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                                    FromDate = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                    ToDate = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();

                                    if (true)
                                    {
                                        bool matchSuccess = false;
                                        matchSuccess = checkDuplicateMultipleDataExistExcelSheet(xlWorksheet, i, Location, Branch, FromDate, ToDate);
                                        if (matchSuccess)
                                        {
                                            errorMessage.Add("Location, branch at Row - " + i + " Exists Multiple Times For From Date and To Date in the Uploaded Excel Document");
                                        }

                                    }

                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                                {
                                    errorMessage.Add("Required Due Date at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                {
                                    errorMessage.Add("Required State ID at row number" + i + ".");
                                }
                                else
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        string stateCode = (xlWorksheet.Cells[i, 2].Text).Trim();
                                        var LocationDetails = (from state in entities.RLCS_State_Mapping
                                                               where state.SM_Code == stateCode
                                                                 && state.SM_Status == "A"
                                                               select state).FirstOrDefault();
                                        if (LocationDetails == null)
                                        {
                                            errorMessage.Add("State ID Does not Exists at row number" + i + ".");
                                        }

                                    }
                                }

                                //if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                //{
                                //    errorMessage.Add("Required Location at row number" + i + ".");
                                //}
                                //else
                                //{
                                //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                //    {
                                //        string locationanme = (xlWorksheet.Cells[i, 3].Text).Trim();
                                //        string stateCode = (xlWorksheet.Cells[i, 2].Text).Trim();
                                //        var LocationDetails = (from Lctn in entities.RLCS_Location_City_Mapping
                                //                               where Lctn.LM_Name == locationanme
                                //                               && Lctn.SM_Code == stateCode
                                //                               && Lctn.LM_Status == "A"
                                //                               select Lctn).FirstOrDefault();
                                //        if (LocationDetails == null)
                                //        {
                                //            errorMessage.Add("Location Name Does not Exists at row number" + i + ".");
                                //        }
                                //        else
                                //        {
                                //            LocationCode = LocationDetails.LM_Code;
                                //        }
                                //    }
                                //}
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                                {
                                    errorMessage.Add("Required Branch at row number" + i + ".");
                                }
                                else
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {


                                        string BranchName = (xlWorksheet.Cells[i, 4].Text).Trim();
                                        string ClientID = Request.QueryString["ClientID"].ToString();
                                        string stateCode = (xlWorksheet.Cells[i, 2].Text).Trim();

                                        var CustomerObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                           where SPOC.BranchType == "E" && SPOC.CM_ClientID == ClientID
                                                           select SPOC).FirstOrDefault();
                                        int CustomerID = Convert.ToInt32(CustomerObj.AVACOM_CustomerID);

                                        var BranchDetails = (from branch in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                             where branch.AVACOM_BranchName == BranchName
                                                             && branch.CM_ClientID == ClientID
                                                             && branch.BranchType == "B"
                                                             && branch.CM_State == stateCode
                                                             && branch.AVACOM_CustomerID == CustomerID
                                                             select branch).FirstOrDefault();
                                        if (BranchDetails == null)
                                        {
                                            errorMessage.Add("Branch Name Does not Exists against state and location at row number" + i + ".");
                                        }
                                    }
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                                {
                                    errorMessage.Add("Required Address at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                                {
                                    errorMessage.Add("Required Contract From Date at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                                {
                                    errorMessage.Add("Required Contract To Date at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 8].Text).Trim()))
                                {
                                    errorMessage.Add("Required Auditor at row number" + i + ".");
                                }
                                else
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        string ClientID = Request.QueryString["ClientID"].ToString();
                                        var CustomerObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                           where SPOC.BranchType == "E" && SPOC.CM_ClientID == ClientID
                                                           select SPOC).FirstOrDefault();
                                        int CustomerID = Convert.ToInt32(CustomerObj.AVACOM_CustomerID);

                                        string AudiotrName = (xlWorksheet.Cells[i, 8].Text).Trim();
                                        var AuditDetails = (from auditor in entities.Users
                                                            where auditor.Email == AudiotrName
                                                            && auditor.VendorRoleID == 22
                                                              && auditor.CustomerID == CustomerID
                                                         && auditor.IsActive == true
                                                         && auditor.IsDeleted == false
                                                            select auditor).FirstOrDefault();
                                        if (AuditDetails == null)
                                        {
                                            errorMessage.Add("Auditor Name Does not Exists at row number" + i + ".");
                                        }
                                    }
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 9].Text).Trim()))
                                {
                                    errorMessage.Add("Required Nature of Business at row number" + i + ".");
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 10].Text).Trim()))
                                {
                                    errorMessage.Add("Required Male Count  at row number" + i + ".");

                                }
                                else
                                {
                                    try
                                    {
                                        int MaleCount = Convert.ToInt32((xlWorksheet.Cells[i, 10].Text).Trim());
                                    }
                                    catch { errorMessage.Add("Male Count Should be number" + i + "."); }

                                }

                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 11].Text).Trim()))
                                {
                                    errorMessage.Add("Required Female Count  at row number" + i + ".");
                                }
                                else
                                {
                                    try
                                    {
                                        int FemaleCount = Convert.ToInt32((xlWorksheet.Cells[i, 11].Text).Trim());
                                    }
                                    catch { errorMessage.Add("Female Count Should be number" + i + "."); }

                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 15].Text).Trim()))
                                {

                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        string DepartmentName = (xlWorksheet.Cells[i, 15].Text).Trim();
                                        var DeptDetails = (from dept in entities.Vendor_Department
                                                           where dept.Name == DepartmentName
                                                           && dept.IsDeleted == false
                                                           select dept).FirstOrDefault();
                                        if (DeptDetails == null)
                                        {
                                            errorMessage.Add("Deparmtent Name not valid at row number" + i + ".");
                                        }
                                    }

                                }

                                #endregion
                            }
                            #endregion

                            string ContractTypeNameFlag = string.Empty;

                            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);

                            if (errorMessage.Count <= 0)
                            {
                                #region Save                        
                                for (int i = 2; i <= xlrow2; i++)
                                {
                                    //Location = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                                    Branch = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                                    FromDate = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                    ToDate = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                    AuditorName = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                                    DateTime StartDate = GetDate(FromDate);
                                    DateTime EndDate = GetDate(ToDate);


                                    string DepartmentID = null;
                                    if (xlWorksheet.Cells[i, 15].Text.Trim() == "Admin")
                                    {
                                        DepartmentID = "1";
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.Trim() == "HR")
                                    {
                                        DepartmentID = "2";
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.Trim() == "Support")
                                    {
                                        DepartmentID = "3";
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.Trim() == "Finance")
                                    {
                                        DepartmentID = "4";
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.Trim() == "IT")
                                    {
                                        DepartmentID = "5";
                                    }
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        int ContMasterID = Convert.ToInt32(Request.QueryString["ContractorID"]);
                                        string ClientID = Request.QueryString["ClientID"].ToString();

                                        var CustomerObj = (from SPOC in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                           where SPOC.BranchType == "E" && SPOC.CM_ClientID == ClientID
                                                           select SPOC).FirstOrDefault();
                                        int CustomerID = Convert.ToInt32(CustomerObj.AVACOM_CustomerID);

                                        //string LocationId = null;
                                        //var LocationDetails = (from Lctn in entities.RLCS_Location_City_Mapping
                                        //                       where Lctn.LM_Name == Location && Lctn.LM_Status == "A"
                                        //                       select Lctn).FirstOrDefault();
                                        //if (LocationDetails != null)
                                        //    LocationId = LocationDetails.LM_Code.ToString();

                                        int BranchId = 0;
                                        var BranchDetails = (from branch in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                             where branch.AVACOM_BranchName == Branch
                                                             && branch.AVACOM_CustomerID == CustomerID
                                                             && branch.CM_ClientID == ClientID
                                                             select branch).FirstOrDefault();

                                        if (BranchDetails != null)
                                            BranchId = Convert.ToInt32(BranchDetails.AVACOM_BranchID);

                                        int AuditId = 0;
                                        var AuditDetails = (from auditor in entities.Users
                                                            where auditor.Email == AuditorName
                                                            && auditor.VendorRoleID == 22
                                                              && auditor.CustomerID == CustomerID
                                                         && auditor.IsActive == true
                                                         && auditor.IsDeleted == false
                                                            select auditor).FirstOrDefault();
                                        if (AuditDetails != null)
                                            AuditId = Convert.ToInt32(AuditDetails.ID);

                                        var SPOCDetails = (from SPOC in entities.RLCS_VendorContractorMaster
                                                           where SPOC.ID == ContMasterID
                                                           select SPOC).FirstOrDefault();
                                        string EmailID = SPOCDetails.SPOCEmailID;

                                        var VendorObj = (from user in entities.Users
                                                         where user.Email == EmailID
                                                         && user.CustomerID == CustomerID
                                                         && user.IsActive == true
                                                         && user.IsDeleted == false
                                                         select user).FirstOrDefault();
                                        if (VendorObj != null)
                                        {
                                            long VendorID = VendorObj.ID;

                                            RLCS_VendorAuditInstance ContractMaster = new RLCS_VendorAuditInstance()
                                            {
                                                CC_Month = DateTime.Now.Month,
                                                CC_Year = DateTime.Now.Year,
                                                CC_ClientID = ClientID,
                                                CC_ContracterID = "",
                                                CC_ContractorName = SPOCDetails.ContractorName,
                                                CC_ContractorAddress = xlWorksheet.Cells[i, 5].Text.Trim(),
                                                CC_StateID = xlWorksheet.Cells[i, 2].Text.Trim(),
                                                CC_BranchID = xlWorksheet.Cells[i, 4].Text.Trim(),
                                                CC_ContractFrom = StartDate,
                                                CC_ContractTo = EndDate,
                                                CC_NoOfJoinees = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text.Trim()) + Convert.ToInt32(xlWorksheet.Cells[i, 11].Text.Trim()),
                                                CC_NatureOfBusiness = xlWorksheet.Cells[i, 9].Text.Trim(),
                                                CC_EffectiveFromMonth = StartDate.Month,
                                                CC_EffectiveFromYear = StartDate.Year,
                                                CC_Status = "A",
                                                CC_EffectiveToMonth = EndDate.Month,
                                                CC_EffectiveToYear = EndDate.Year,
                                                CC_CreatedBy = "Admin",
                                                CC_CreatedDate = DateTime.Now,
                                                CC_Version = 0,
                                                CC_MaleHeadCount = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text.Trim()),
                                                CC_FeMaleHeadCount = Convert.ToInt32(xlWorksheet.Cells[i, 11].Text.Trim()),
                                                CC_ESIC_Number = xlWorksheet.Cells[i, 12].Text.Trim(),
                                                CC_PT_State = xlWorksheet.Cells[i, 14].Text.Trim(),
                                                CC_PF_Code = xlWorksheet.Cells[i, 13].Text.Trim(),
                                                CC_SPOC_Name = SPOCDetails.SPOCName,
                                                CC_SPOC_Email = SPOCDetails.SPOCEmailID,
                                                CC_SPOC_Mobile = SPOCDetails.SPOCMobileNo,
                                                CC_Auditor = "T" + (AuditId),
                                                CC_Frequency = "A",
                                                //CC_Location = LocationId,
                                                AVACOM_BranchID = BranchId,
                                                AVACOM_CustomerID = CustomerID,
                                                AVACOM_AuditorID = AuditId,
                                                AVACOM_VendorID = Convert.ToInt32(VendorID),
                                                DueDate = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim()),
                                                DepartmentType = DepartmentID,
                                                ContractMasterID = ContMasterID,
                                                CreatedOn = DateTime.Now
                                            };

                                            entities.RLCS_VendorAuditInstance.Add(ContractMaster);
                                            entities.SaveChanges();

                                            uploadedVendorCount++;
                                            saveSuccess = true;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                if (errorMessage.Count > 0)
                                {
                                    ErrorMessages(errorMessage);
                                }
                            }
                        }
                        if (saveSuccess)
                        {
                            if (uploadedVendorCount > 0)
                            {
                                //BindGrid();
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = uploadedVendorCount + " Contract Details Uploaded Successfully";
                                cvUploadUtilityPage.CssClass = "alert alert-success";
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                            }
                        }
                    }
                }
                else
                {
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Vendor contractor Master user not mapping with this customer.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public bool checkDuplicateMultipleDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, string LocationText, string BranchText, string FromDateText, string ToDateText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            string TextLocation = xlWorksheet.Cells[i, 3].Text.ToString().Trim();
                            if ((TextLocation.Trim()).Equals(LocationText.Trim()))
                            {
                                string TextBranch = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
                                if ((TextBranch.Trim()).Equals(BranchText.Trim()))
                                {
                                    string TextFromDate = xlWorksheet.Cells[i, 6].Text.ToString().Trim();
                                    if ((TextFromDate.Trim()).Equals(FromDateText.Trim()))
                                    {
                                        string TextToDate = xlWorksheet.Cells[i, 7].Text.ToString().Trim();
                                        if ((TextToDate.Trim()).Equals(ToDateText.Trim()))
                                        {
                                            matchSuccess = true;
                                            i = lastRow; //exit from for Loop
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }

        [WebMethod]
        public static string GetScopeOfFrequency(string ClientID )
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string lstscopeList = (from scope in entities.RLCS_ScopeOfWork_Master
                                         where scope.ClientID == ClientID && (scope.ScopeID=="SOW10" ||scope.ScopeName== "VendorAudit") && scope.Frequency!=null 
                                         select scope.Frequency).Distinct().FirstOrDefault();                   
                    return serializer.Serialize(lstscopeList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
    }
}