﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSActassignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSActassignment" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../assets/fSelect.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../Newjs/bootstrap-multiselect.js"></script>
    <script src="../assets/fSelect.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />


    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <%--<script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>--%>
    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

    <script type="text/javascript">

        var AssignCheckListData = [];
        var StartYear = [];
        var EndYear = [];
        var FrequencyId = '';
        function BindControls() {

            $(function () {
                $('input[id*=txtStartDate]').datepicker(
                    {
                        dateFormat: 'd M yy',
                        onSelect: function (selected) {
                            $("#ContentPlaceHolder1_txtEndDate").datepicker("option", "minDate", selected);
                        }
                    });
            });
            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'd M yy'
                    });
            });
        }
         function Bindview()
        {
            <%--if ('<%= ContractCustomer %>' == 'False') 
            {--%>
                ddlstatediv.style.display = "block";
                divLocation.style.display = "block";
            //}
            //else
            //{
            //    ddlstatediv.style.display = "none";
            //    divLocation.style.display = "none";
            //}
        }
        $(document).ready(function () {
            setactivemenu('leftnoticesmenu');
            fhead('CheckList Assignment');
            BindControls();
            BindCustomerList();
            BindEntityStateList(0);
            BindEntityLocationList([0]);
            BindEntityBranchList([0]);
            MultiSelectDropdown();
            BindStateList();
            BindActList();
            Bindview();

            $("#ddlCustomer").on("change", function (e) {

                $("#ddlLocation").empty();
                $("#ddlVendor").empty();
                $("#ddlVendor").fSelect('reload');
                $("#ddlLocation").fSelect('reload');
                BindEntityStateList($(this).val());
                BindEntityBranchList([0]);
                GetCustomerFrequency($(this).val());
                document.getElementById("CustomerID").value = $(this).val();


            });

            $("#ddlEntity").on("change", function (e) {
                $("#ddlEntityState").empty();
                $("#ddlLocation").empty();
                $("#ddlBranch").empty();
                BindEntityStateList($(this).val());
                document.getElementById("EntityID").value = $(this).val();
            });

            $("#ddlEntityState").on("change", function (e) {
                $("#ddlLocation").empty();
                $("#ddlLocation").fSelect('reload');
                BindEntityLocationList($(this).val());
            });

            $("#ddlLocation").on("change", function (e) {
               
                BindEntityBranchList($(this).val());
                document.getElementById("LocationID").value = $(this).val();
            });

            $("#ddlBranch").on("change", function (e) {
                BindVendorList($(this).val());
                document.getElementById("BranchID").value = $(this).val();
            });

            $("#ddlVendor").on("change", function (e) {
                document.getElementById("VendorID").value = $(this).val();

            });

            $("#ddlYear").on("change", function (e) {
                document.getElementById("YearID").value = $(this).val();
            });

            $("#ddlPeriod").on("change", function (e) {
                document.getElementById("Period").value = $(this).val();
            });
            $("#ddlFrequency").on("change", function (e) {
                
                if ($(this).val() != "C")
                {
                    $("#divPeriod").removeClass("hidden");
                    $("#divYear").removeClass("hidden");
                    $("#divStartDate").addClass("hidden");
                    $("#divEndDate").addClass("hidden");
                    BindPeriods($(this).val());
                }
                else
                {
                    FrequencyId = "5";
                    $("#divPeriod").addClass("hidden");
                    $("#divYear").addClass("hidden");
                    $("#divStartDate").removeClass("hidden");
                    $("#divEndDate").removeClass("hidden");
                  
                }
            });

            $("#ddlType").on("change", function (e) {
             
                var Year = $("#ddlYear").val();
                var Period = $("#ddlPeriod").val();
                if ($(this).val() == "Central")
                    $("#ddlState").attr("disabled", "disabled");
                else
                    $("#ddlState").removeAttr("disabled");


                BindStateList();
                BindActList();
                document.getElementById("Type").value = $(this).val();
                
                if ($("#ddlFrequency").val() == "C")
                {
                    BindAssignCheckList();
                    BindUnAssignCheckList();
                }
                else {
                    if (Year == null) {
                        alert("Please select at least One Year");
                    }
                    else if (Period == null) {
                        alert("Please select at least One Period");
                    }
                    else {
                        BindAssignCheckList();
                        BindUnAssignCheckList();
                    }
                }
            });

            $("#ddlState").on("change", function (e) {
                document.getElementById("State").value = $(this).val();
                BindActList();
                BindAssignCheckList();
                BindUnAssignCheckList();
            });

            $("#ddlAct").on("change", function (e) {
                document.getElementById("Act").value = $(this).val();
            });
            $('#customerSpecific').change(function () {

                BindUnAssignCheckList();
            });

            $("#btnSave").click(function (e) {
              
                if (AssignCheckListData.length > 0) {
                    var AssignmentObj = {
                        AssignCheckList: AssignCheckListData

                    }

                    $.ajax({
                        type: "POST",
                        url: "./RLCSActassignment.aspx/AssignCheckListPeriodWise",
                        data: JSON.stringify({ AssignmentObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var flag = false;
                            if (data.d != "false") {
                                flag = true;
                            }
                            if (flag)
                                alert("Selected Checklist assigned to Contractor Successfully.");
                            else
                                alert("Please Check Contract start Date and End Date or Checklist Already assigned");

                            AssignCheckListData = [];
                            BindAssignCheckList();
                            BindUnAssignCheckList();
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
                else {
                    alert("Please select CheckList");
                }
            });

            $("#btnClear").on("click", function (e) {
                $("#ddlType").val("Central");
                $("#txtFequency").val("");
                BindCustomerList();
                BindEntityStateList(0);
                BindEntityLocationList(0);
                BindStateList();
                BindActList();
                $("#ddlVendor").empty();
                $("#ddlVendor").fSelect('reload');
                $("#ddlLocation").empty();
                $("#ddlLocation").fSelect('reload');
                $("#ddlYear").empty();
                $("#ddlYear").fSelect('reload');
                $("#ddlPeriod").empty();
                $("#ddlPeriod").fSelect('reload');
                $("#ddlBranch").empty();
                $("#ddlBranch").fSelect('reload');

            });

            $("#btnApply").on("click", function (e) {
                var VendorList = $("#ddlVendor").val();
                var Customer = $("#ddlCustomer").val();
                var Year = $("#ddlYear").val();
                var Period = $("#ddlPeriod").val();
                if (Customer == "-1") {
                    alert("Please select Customer");
                }
                else if (VendorList == null) {
                    alert("Please select at least One Contractor");
                }
                else
                {
                    if ($("#ddlFrequency").val() == "C")
                    {
                        BindAssignCheckList();
                        BindUnAssignCheckList();
                    }
                    else if (Year == null) {
                        alert("Please select at least One Year");
                    }
                    else if (Period == null) {
                        alert("Please select at least One Period");
                    }
                    else {
                        BindAssignCheckList();
                        BindUnAssignCheckList();
                    }
                }                
            });

            $(document).on("click", "#tblAssignRecord tbody tr .Delete", function (e) {
                var item = $("#tblAssignRecord").data("kendoGrid").dataItem($(this).closest("tr"));
                DeleteChecklist(item.Id, item.AuditID, item.CheckListMasterId, item.VendorID, item.SchedulePeriod);
            });


            $("#ContentPlaceHolder1_lnkAssign").on("click", function (e) {
                $("#ContentPlaceHolder1_liAssign").addClass("active");
                $("#ContentPlaceHolder1_liUnAssign").removeClass("active");
                resizeGrid();
            });
            $("#ContentPlaceHolder1_lnkUnAssign").on("click", function (e) {
                $("#ContentPlaceHolder1_liUnAssign").addClass("active");
                $("#ContentPlaceHolder1_liAssign").removeClass("active");
                BindUnAssignCheckList();
            });

        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 500, function () { });
        }

        function BindCustomerList() {
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindCustomerList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Customer = JSON.parse(data.d);
                    if (Customer.length > 0) {
                        $("#ddlCustomer").empty();
                        $("#ddlCustomer").append($("<option></option>").val("-1").html("Select Principal Employer"));
                        $.each(Customer, function (data, value) {
                            $("#ddlCustomer").append($("<option></option>").val(value.ID).html(value.Name));
                        })
                        $("#ddlCustomer").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindEntityList(id) {
            var AssignmentObj = {
                CustomerID: id
            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindEntityList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Entity = JSON.parse(data.d);
                    if (Entity.length > 0) {
                        $('#ddlEntity').empty();
                        $("#ddlEntity").append($("<option></option>").val("-1").html("Select Entity"));
                        $.each(Entity, function (data, value) {
                            $("#ddlEntity").append($("<option></option>").val(value.ID).html(value.Name));
                        })
                        $("#ddlEntity").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindEntityStateList(id) {
            var AssignmentObj = {
                CustomerID: $("#ddlCustomer").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindEntityStateList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Entity = JSON.parse(data.d);
                
                    //if (Entity.length > 0) {

                    $('#ddlEntityState').empty();
                    $("#ddlEntityState").append($("<option></option>").val("-1").html("Select State"));
                    $.each(Entity, function (data, value) {
                        $("#ddlEntityState").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    $("#ddlEntityState").select2();

                    //}

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindEntityLocationList(id) {
            var AssignmentObj = {
                StateID: id,
                CustomerID: $("#ddlCustomer").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindEntityLocationList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    debugger;
                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $('#ddlLocation').empty();
                        $.each(Location, function (data, value) {
                            $("#ddlLocation").append($("<option></option>").val(value.ID).html(value.Name));
                        })
                        $("#ddlLocation").fSelect('reload');
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindEntityBranchList(id) {

            var AssignmentObj = {
                Location: id,
                CustomerID: $("#ddlCustomer").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindEntityBranchList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Location = JSON.parse(data.d);
                    $('#ddlBranch').empty();
                    $.each(Location, function (data, value) {
                        $("#ddlBranch").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    $("#ddlBranch").fSelect('reload');
                },
                failure: function (data) {
                    alert(data);
                }
            });

        }

        function BindVendorList(id) {

            var AssignmentObj = {
                Branches: id,
                CustomerID: $("#ddlCustomer").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindVendorList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Vendors = JSON.parse(data.d);
                    if (Vendors.length > 0) {
                        if (Vendors == "") {
                            $("#ddlVendor").empty();
                            $("#ddlVendor").fSelect('reload');
                        }
                        else {
                            $("#ddlVendor").empty();
                            StartYear = [];

                            $.each(Vendors, function (data, value) {
                               
                                StartYear.push(value.StartYear);
                                StartYear.push(value.EndYear);

                                $("#ddlVendor").append($("<option></option>").val(value.ID).html(value.ContractorName));
                            })
                        
                            $("#ddlVendor").fSelect('reload');
                            var Year = [];
                            var MinYear = Math.max.apply(Math, StartYear); // 3

                            var MaxYear = Math.min.apply(Math, StartYear); // 1

                            Year.push(MinYear);
                            Year.push(MaxYear);
                            BindYear(Year);
                        }
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });

        }

        function BindStateList() {
            var AssignmentObj = {
                Type: ""
            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindStateList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                 
                    var Entity = JSON.parse(data.d);
                    if (Entity.length > 0) {
                        $('#ddlState').empty();
                        $("#ddlState").append($("<option></option>").val("-1").html("Select State"));
                        $.each(Entity, function (data, value) {
                            $("#ddlState").append($("<option></option>").val(value.SM_Code).html(value.SM_Name));
                        })
                        $("#ddlState").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindActList() {
            var AssignmentObj = {
                Type: $('#ddlType').val(),
                StateID: $('#ddlState').val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindActList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Entity = JSON.parse(data.d);
                    if (Entity.length > 0) {
                        $('#ddlAct').empty();
                        $("#ddlAct").append($("<option></option>").val("-1").html("Select Act"));
                        $.each(Entity, function (data, value) {
                            $("#ddlAct").append($("<option></option>").val(value.AM_ActID).html(value.AM_ActName));
                        })
                        $("#ddlAct").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function onDataBound_AssignCheckList() {
            kendo.ui.progress($("#AssignLoader"), false);
        }
        function onDataBound_UnAssignCheckList() {
            kendo.ui.progress($("#UnAssignLoader"), false);
        }
        function BindAssignCheckList()
        {
          
            var AssignmentObj = {
                Type: $("#ddlType").val(),
                StateID: $("#ddlState").val(),
                ActID: $("#ddlAct").val(),
                VendorIDs: $("#ddlVendor").val(),
                Periods: $("#ddlPeriod").val(),
                Years: $("#ddlYear").val(),
                isclientbasedchecklist: $("#customerSpecific").is(':checked'),
                Frequency: $("#ddlFrequency").val(),
                StartDate: $("#ContentPlaceHolder1_txtStartDate").val(),
                EndDate: $("#ContentPlaceHolder1_txtEndDate").val()
            }
            kendo.ui.progress($("#AssignLoader"), true);
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindAssignCheckList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var AssignCheckList = JSON.parse(data.d);

                    //if (AssignCheckList.length > 0) {

                    $("#tblAssignRecord").kendoGrid({
                        dataSource: {
                            data: AssignCheckList
                        },
                        dataBound: onDataBound_AssignCheckList,
                        height: 300,
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        //pageable: {
                        //    pageSize: 10,
                        //    refresh: true,
                        //    buttonCount: 3
                        //},

                        reorderable: true,
                        resizable: true,
                        columns: [
                            { hidden: true, field: "Id" },
                            { hidden: true, field: "ActID" },
                            { hidden: true, field: "AuditID" },
                            { hidden: true, field: "CheckListMasterId" },
                            { hidden: true, field: "VendorID" },
                            { field: "StateName", title: "State Name", width: "30%" },
                            { field: "AM_ActName", title: "Act Name", width: "30%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "NatureOfCompliance", title: "Nature Of Compliance", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Risk", title: "Risk", width: "20%" },
                            { field: "VendorName", title: "Contractor Name", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "BranchName", title: "Branch Name", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "SchedulePeriod", title: "Period", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             {
                                 title: "Action", lock: true, width: "15%;",
                                 attributes: {
                                     style: 'text-align: left;'
                                 },
                                 command: [{ name: "DeleteScheduleModal", text: "", iconClass: ".k-icon .k-i-delete k-i-trash k-icon k-edit", className: "Delete" }],
                             }
                        ]
                    });
                    $("#tblAssignRecord").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });
                    $("#tblAssignRecord").kendoTooltip({
                        filter: "td",
                        position: "down",
                        content: function (e) {
                          
                            var content = e.target.context.textContent;
                            if (content != "") {
                                return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                            }
                            else {
                                content = "UnAssign"
                                return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                            }
                            //else
                            //    e.preventDefault();
                        }
                    }).data("kendoTooltip");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });

         
        }

        function BindUnAssignCheckList() {
            var AssignmentObj = {
                Type: $("#ddlType").val(),
                StateID: $("#ddlState").val(),
                ActID: $("#ddlAct").val(),
                VendorIDs: $("#ddlVendor").val(),
                Periods: $("#ddlPeriod").val(),
                Years: $("#ddlYear").val(),
                Branches: $("#ddlBranch").val(),
                CustomerID: $("#ddlCustomer").val(),
                isclientbasedchecklist: $("#customerSpecific").is(':checked'),
                Frequency: $("#ddlFrequency").val(),
                StartDate: $("#ContentPlaceHolder1_txtStartDate").val(),
                EndDate: $("#ContentPlaceHolder1_txtEndDate").val()
            }
            kendo.ui.progress($("#UnAssignLoader"), true);
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/BindUnAssignCheckList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                pageSize: 10,
                success: function (data) {
                    var UnAssignCheckList = JSON.parse(data.d);
                 
                 //   if (UnAssignCheckList.length > 0) {
                        $("#tblUnAssignRecord").kendoGrid({
                            dataSource: {
                                data: UnAssignCheckList
                            },
                            dataBound: onDataBound_UnAssignCheckList,
                            height: 300,
                            sortable: true,
                            filterable: false,
                            columnMenu: true,
                            reorderable: true,
                            resizable: true,
                            noRecords: {
                                template: function(e){
                                    return "No data available";
                                }
                            },
                            change: onChangeUnAssign,
                            columns: [
                                { selectable: true, width: "5%" },
                                { hidden: true, field: "Id" },
                                { hidden: true, field: "ChecklistID" },
                                { hidden: true, field: "VendorID" },
                                { hidden: true, field: "AuditID" },
                                { field: "StateName", title: "State Name", width: "30%" },
                                { hidden: true, field: "ActID" },
                                { field: "AM_ActName", title: "Act Name", width: "30%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "NatureOfCompliance", title: "Nature Of Compliance", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "Risk", title: "Risk", width: "20%" },
                                { field: "AM_ActGroup", title: "Act Group", width: "20%" },
                                { field: "VendorName", title: "Contractor Name", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "Branch", title: "Branch Name", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                                { field: "Period", title: "Period", width: "20%", attributes: { style: 'white-space: nowrap;' } }
                            ]
                        });
                        //$("#tblUnAssignRecord").kendoTooltip({
                        //    filter: ".k-ScheduleAudit",
                        //    content: function (e) {
                        //        return "";
                        //    }
                        //});
                    $("#tblUnAssignRecord").kendoTooltip({
                            filter: "td",
                            position: "down",
                            content: function (e) {                             
                                var content = e.target.context.textContent;
                                if (content != "") {
                                    if (content.length > 1) {
                                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                                    }
                                    else {
                                        e.preventDefault();
                                    }
                                }
                                else
                                    e.preventDefault();
                            }
                    }).data("kendoTooltip");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function onChangeUnAssign(arg) {
            var Year = $("#ddlYear").val();
            var Period = $("#ddlPeriod").val();

            if ($("#ddlFrequency").val() == "C")
            {
                var rows = arg.sender.select();
                AssignCheckListData = [];
                rows.each(function (e) {
                    var tblUnAssignRecord = $("#tblUnAssignRecord").data("kendoGrid");
                    var dataItem = tblUnAssignRecord.dataItem(this);
                    var SelecteddataItem = {
                        ActID: dataItem.ActID,
                        ChecklistMasterID: dataItem.Id,
                        ChecklistID: dataItem.ChecklistID,
                        VendorID: dataItem.VendorID,
                        SchedulePeriod: dataItem.Period,
                        Years: $("#ddlYear").val(),
                        Periods: $("#ddlPeriod").val(),
                        FrequencyID: FrequencyId,
                        LocationID: $("#ddlLocation").val(),
                        AuditID: dataItem.AuditID
                    };
              
                    AssignCheckListData.push(SelecteddataItem);
                })
            }
            else {
                if (Year == null) {
                    alert("Please select at least One Year");
                }
                else if (Period == null) {
                    alert("Please select at least One Period");
                }
                else {
                    var rows = arg.sender.select();
                    AssignCheckListData = [];
                    rows.each(function (e) {
                        var tblUnAssignRecord = $("#tblUnAssignRecord").data("kendoGrid");
                        var dataItem = tblUnAssignRecord.dataItem(this);
                        var SelecteddataItem = {
                            ActID: dataItem.ActID,
                            ChecklistMasterID: dataItem.Id,
                            ChecklistID: dataItem.ChecklistID,
                            VendorID: dataItem.VendorID,
                            SchedulePeriod: dataItem.Period,
                            Years: $("#ddlYear").val(),
                            Periods: $("#ddlPeriod").val(),
                            FrequencyID: FrequencyId,
                            LocationID: $("#ddlLocation").val(),
                            AuditID: dataItem.AuditID
                        };
                    
                        AssignCheckListData.push(SelecteddataItem);
                    })
                }
            }
        }

        function DeleteChecklist(id, Auditid, checklistid, VendorID, SchedulePeriod) {
            var AssignmentObj = {
                ID: id,
                AuditID: Auditid,
                ScheduledOnID: 0,
                CheckListID: checklistid,
                DeleteVendorID: VendorID,
                SchedulePeriod: SchedulePeriod

            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/DeleteCheckList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var data = JSON.parse(data.d);
                    alert(data);
                    BindAssignCheckList();
                    BindUnAssignCheckList();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function MultiSelectDropdown() {
            $('#ddlVendor').attr("multiple", "multiple");
            $('#ddlVendor').fSelect({ placeholder: "Select Contractor" });
            $('#ddlLocation').attr("multiple", "multiple");
            $('#ddlLocation').fSelect({ placeholder: "Select Location" });
            $('#ddlYear').attr("multiple", "multiple");
            $('#ddlYear').fSelect({ placeholder: "Select Year" });
            $('#ddlPeriod').attr("multiple", "multiple");
            $('#ddlPeriod').fSelect({ placeholder: "Select Period" });
            $('#ddlBranch').attr("multiple", "multiple");
            $('#ddlBranch').fSelect({ placeholder: "Select Branch" });
        }

        function GetCustomerFrequency(id) {
            var AssignmentObj = {
                CustomerID: id
            }
            $.ajax({
                type: "POST",
                url: "./RLCSActassignment.aspx/GetCustomerFrequency",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Freq = JSON.parse(data.d);
                    $("#divStartDate").addClass("hidden");
                    $("#divEndDate").addClass("hidden");
                    if (Freq == "A") {
                        $("#txtFequency").val("Annually");
                        $("#divFrequency").addClass("hidden");
                        BindPeriods(Freq);
                    }
                    if (Freq == "H") {
                        $("#txtFequency").val("Half Yearly");
                        $("#divFrequency").addClass("hidden");
                        BindPeriods(Freq);
                    }
                    if (Freq == "Q") {
                        $("#txtFequency").val("Quarterly");
                        $("#divFrequency").addClass("hidden");
                        BindPeriods(Freq);
                    }
                    if (Freq == "M") {
                        $("#txtFequency").val("Monthly");
                        $("#divFrequency").addClass("hidden");
                        BindPeriods(Freq);
                    }
                    if (Freq == "O") {
                        $("#txtFequency").val("OnOccurence");
                        $("#divFrequency").removeClass("hidden");
                        $("#ddlFrequency").val("-1");
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindPeriods(Frq) {

            $('#ddlPeriod').empty();
            if (Frq == "C") {
                FrequencyId = "5";
            }
            if (Frq == "Q") {
                $("#ddlPeriod").append($("<option></option>").val("Jan - Mar").html("Jan - Mar"));
                $("#ddlPeriod").append($("<option></option>").val("Apr - Jun").html("Apr - Jun"));
                $("#ddlPeriod").append($("<option></option>").val("Jul - Sep").html("Jul - Sep"));
                $("#ddlPeriod").append($("<option></option>").val("Oct - Dec").html("Oct - Dec"));
                FrequencyId = "4";
            }
            if (Frq == "A" || Frq == "Y") {
                $("#ddlPeriod").append($("<option></option>").val("Jan - Dec").html("Jan - Dec"));
                FrequencyId = "3";
            }
            if (Frq == "H") {
                $("#ddlPeriod").append($("<option></option>").val("Jan - Jun").html("Jan - Jun"));
                $("#ddlPeriod").append($("<option></option>").val("Jul - Dec").html("Jul - Dec"));
                FrequencyId = "2";
            }
            if (Frq == "M") {
                $("#ddlPeriod").append($("<option></option>").val("Jan").html("Jan"));
                $("#ddlPeriod").append($("<option></option>").val("Feb").html("Feb"));
                $("#ddlPeriod").append($("<option></option>").val("Mar").html("Mar"));
                $("#ddlPeriod").append($("<option></option>").val("Apr").html("Apr"));
                $("#ddlPeriod").append($("<option></option>").val("May").html("May"));
                $("#ddlPeriod").append($("<option></option>").val("Jun").html("Jun"));
                $("#ddlPeriod").append($("<option></option>").val("Jul").html("Jul"));
                $("#ddlPeriod").append($("<option></option>").val("Aug").html("Aug"));
                $("#ddlPeriod").append($("<option></option>").val("Sep").html("Sep"));
                $("#ddlPeriod").append($("<option></option>").val("Oct").html("Oct"));
                $("#ddlPeriod").append($("<option></option>").val("Nov").html("Nov"));
                $("#ddlPeriod").append($("<option></option>").val("Dec").html("Dec"));
                FrequencyId = "1";
            }

            $("#ddlPeriod").fSelect('reload');
        }

        function BindYear(Year) {
            var YearOption = [];
            Year.sort();
            for (var i = Year[0]; i <= Year[1]; i++) {
                YearOption.push(i);
            }
            $("#ddlYear").empty();
            $.each(YearOption, function (data, value) {
                var lastDigit = value % 100;
                $("#ddlYear").append($("<option></option>").val(lastDigit).html(value));

            })
            $("#ddlYear").fSelect('reload');
        }


    </script>

    <style type="text/css">
        .k-grid-content {
            max-height: 250px;
        }

        .Mar-Top15 {
            margin-top: 15px;
        }

        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }

        .LoadingHide {
            display: none;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .form-control {
            width: 100%;
            height: 28px;
            padding: 2px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #8e8e93;
            background-color: #fff;
            border: 1px solid #c7c7cc;
            border-radius: 4px;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
            height: 150px;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
            border: 1px solid #c7c7cc;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
            padding: 0px 0px 0px 2px !important;
            font-size: 13px;
            font-weight: 400;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }

        .AddNewspan1 {
            padding: 5px;
            border: 2px solid #f4f0f0;
            border-radius: 51px;
            display: inline-block;
            height: 26px;
            width: 26px;
        }

        .MarRight {
            margin-right: 20px;
        }

        #DivVenoder > div.fs-wrap {
            width: 245px;
        }

        #divLocation > div.fs-wrap {
            width: 237px;
        }

        #divBranch > div.fs-wrap {
            width: 237px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <select id="ddlCustomer" class="form-control">
                    </select>
                    <input type="hidden" id="CustomerID" name="CustomerID" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 hidden" >
                    <select class="form-control" id="ddlEntity">
                    </select>
                    <input type="hidden" id="EntityID" name="EntityID" />
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" id="ddlstatediv">
                    <select class="form-control" id="ddlEntityState">
                    </select>
                    <input type="hidden" id="EntityStateID" name="EntityStateID" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3" id="divLocation">
                    <select class="form-control" id="ddlLocation">
                    </select>
                    <input type="hidden" id="LocationID" name="LocationID" />
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2" id="divBranch">
                    <select class="form-control" id="ddlBranch">
                    </select>
                    <input type="hidden" id="BranchID" name="BranchID" />
                </div>
            </div>

            <div class="row" style="margin-top: 15px;">
                <div class="col-lg-3 col-md-3 col-sm-3" id="DivVenoder">
                    <select id="ddlVendor" class="form-control">
                    </select>
                    <input type="hidden" id="VendorID" name="VendorID" />
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <input type="text" id="txtFequency" class="form-control" readonly="true" />

                    <input type="hidden" id="Frequency" name="Frequency" />
                </div>

               
                 <div class="col-lg-2 col-md-2 col-sm-2 hidden" id="divFrequency">
                    <select id="ddlFrequency" class="form-control">
                        <option value="-1">Select Frequency</option>
                        <option value="A">Annually</option>
                        <option value="H">Half Yearly</option>
                        <option value="Q">Quarterly</option>
                        <option value="M">Monthly</option>
                        <option value="C">Custom</option>
                    </select>
                </div>
                 <div class="col-lg-2 col-md-2 col-sm-2" id="divYear">
                    <select id="ddlYear" class="form-control">
                    </select>
                    <input type="hidden" id="YearID" name="YearID" />
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" id="divPeriod">
                    <select id="ddlPeriod" class="form-control">
                    </select>
                    <input type="hidden" id="Period" name="Period" />
                </div>
                 <div class="col-lg-2 col-md-2 col-sm-2 hidden" id="divStartDate">
                     <asp:TextBox ID="txtStartDate" class="form-control endate" PlaceHolder="Start Date" runat="server">
                    </asp:TextBox>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 hidden" id="divEndDate">
                     <asp:TextBox ID="txtEndDate" class="form-control endate" PlaceHolder="End Date" runat="server">
                    </asp:TextBox>
                </div>
               <%--  <div class="col-lg-2 col-md-2 col-sm-2" id="divPeriod">
                    <select id="ddlPeriod" class="form-control">
                    </select>
                    <input type="text" id="Period" name="Period" />
                </div>--%>
            </div>

            <div class="row" style="margin-top: 15px;">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <select id="ddlType" class="form-control">
                        <option value="Central">Central</option>
                        <option value="State">State</option>
                    </select>
                    <input type="hidden" id="Type" name="Type" value="Central" />
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <select id="ddlState" class="form-control" disabled>
                    </select>
                    <input type="hidden" id="State" name="State" />
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 hidden">
                    <select id="ddlAct" class="form-control">
                    </select>
                    <input type="hidden" id="Act" name="Act" />
                </div>
                <div class="col-lg-4 col-md-3 col-sm-3" style="color: black;">
                    <input type="checkbox" id="customerSpecific" />
                    Show Client Specific Checklist
                   
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="pull-right">
                        <button id="btnApply" type="button" class="btn btn-primary pull-left MarRight">Apply</button>
                        <button type="button" id="btnClear" class="btn btn-primary pull-left">Clear</button>
                    </div>
                </div>
            </div>

            <div class="row Mar-Top15">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#Assign">Assigned</a></li>
                        <li><a data-toggle="tab" href="#UNAssign">Unassigned</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="Assign" class="tab-pane fade in active">
                            <div class="row Mar-Top15">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div id="tblAssignRecord"></div>
                                    <div class="chart-loading" id="AssignLoader"></div>
                                </div>
                            </div>
                        </div>
                        <div id="UNAssign" class="tab-pane fade">
                            <div class="row Mar-Top15">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <button id="btnSave" type="button" class="btn btn-primary pull-right">Assign CheckList</button>
                                </div>
                            </div>
                            <div class="row Mar-Top15">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div id="tblUnAssignRecord"></div>
                                    <div class="chart-loading" id="UnAssignLoader"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
    </div>
</asp:Content>
