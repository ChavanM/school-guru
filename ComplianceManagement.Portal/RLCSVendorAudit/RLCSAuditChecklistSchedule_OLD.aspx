﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSAuditChecklistSchedule_OLD.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSAuditChecklistSchedule_OLD" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 100%;
        }
        .AddNewspan1 {
            padding: 5px;
            border: 2px solid #f4f0f0;
            border-radius: 51px;
            display: inline-block;
            height: 26px;
            width: 26px;
        }
    </style>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { };
        $(document).ready(function () {
            setactivemenu('leftnoticesmenu');
            fhead('Assign Schedule');
        });      
    
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            if ($("#<%=tbxFilterLocation.ClientID %>") != null) {
                $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

                $("#<%=tbxFilterLocation.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { }
        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 0, function () { });
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12" style="padding-left: 1px; padding-right: 1px;">
                <section class="panel">
                    <div class="clearfix"></div>
                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="DepartmentPageValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="NoticePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div class="row">
                      

                            <div class="form-group required col-md-4" id="CustomerList1" runat="server">
                                <asp:DropDownListChosen runat="server" ID="ddlFilterCustomer"
                                    AllowSingleDeselect="false" DisableSearchThreshold="5"
                                    DataPlaceHolder="Select Customer"
                                    class="form-control" Width="300px" Enabled="true"
                                    OnSelectedIndexChanged="ddlFilterCustomer_SelectedIndexChanged"
                                    AutoPostBack="true" />
                                <asp:RequiredFieldValidator ID="rfvCustomer" ErrorMessage="Please Select Customer"
                                    ControlToValidate="ddlFilterCustomer" runat="server" ValidationGroup="LicenseListPageValidationGroup" Display="None" />
                            </div>
                            <div class="col-md-4 colpadding0">
                                <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Select Entity/State/Location/Branch" autocomplete="off"
                                    CssClass="clsDropDownTextBox" />
                                <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divBranches">
                                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                        Style="margin-top: -5%; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                            </div>
                              <div class="col-md-2 colpadding0">
                          

                                  <asp:DropDownListChosen runat="server" ID="ddlVendorListBase"
                                    AllowSingleDeselect="false" DisableSearchThreshold="5"
                                    DataPlaceHolder="Select Vendor"
                                    class="form-control" Width="150px" Enabled="true"
                                    OnSelectedIndexChanged="ddlVendorListBase_SelectedIndexChanged"
                                    AutoPostBack="true" />
                        </div>
                            <div class="col-md-2 colpadding0" style="display:none;">
                                <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" 
                                    ID="lnkBtnApplyFilter" OnClick="lnkBtnApplyFilter_Click"
                                    Style="float: right;" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 colpadding0">
                                <asp:GridView runat="server" ID="grdChecklistDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>                                               
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblAVACOMchecklistid" Text='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblRLCSchecklistid" Text='<%# Eval("ChecklistID") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblPID" Text='<%# Eval("Avacom_VendorID") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblRID" Text='<%# Eval("AVACOM_AuditorID") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblAuditID" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblAVACOMlocation" Text='<%# Eval("AVACOM_BranchID") %>'></asp:Label>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CheckList" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NatureOFCompliance") %>' ToolTip='<%# Eval("NatureOFCompliance") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActName") %>' ToolTip='<%# Eval("AM_ActName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Section" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Section") %>' ToolTip='<%# Eval("Section") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Checklist Rule" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Checklist_Rule") %>' ToolTip='<%# Eval("Checklist_Rule") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type Of Compliance" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TypeOfCompliance") %>' ToolTip='<%# Eval("TypeOfCompliance") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("SM_Name") %>' ToolTip='<%# Eval("SM_Name") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-8 colpadding0">
                                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                </div>

                                <div class="col-md-2 colpadding0">
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" Selected="True" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>

                                <div class="col-md-2 colpadding0" style="float: right;">
                                    <div style="float: left; width: 60%">
                                        <p class="clsPageNo">Page</p>
                                    </div>
                                    <div style="float: left; width: 40%">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                         <div class="row">
                             <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                         </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</asp:Content>
