﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_PerofrmerTransaction.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCS_PerofrmerTransaction" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <style>
        tr.spaceUnder > td {
            padding-bottom: 1em;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        .circle {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

        body {
            color: #f1f1f1;
            background: #fff !important;
            font-family: 'Roboto',sans-serif;
            padding: 0 !important;
            margin: 0 !important;
            font-size: 14px !important;
        }
    </style>

    <title></title>

    <script type="text/javascript">

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function fopendocfileReview(file) {

            $('#DocumentReviewPopUp1').modal('show');
            $('#docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        //function closeViewdocument() {
        //      $('#DocumentReviewPopUp1').modal('show');
        //}

        function fCloseBack() {            
            var modal1 = $('#close', window.parent.document);
            modal1.click();
            window.parent.closeModal();
        }

         $(document).ready(function () {
            $("button[data-dismiss-modal=modal1]").click(function () {
                $('#DocumentReviewPopUp1').modal('hide');
            });

        });

    </script>
</head>
<body>
    <form id="form1" runat="server">

        <div style="margin-bottom: 2px">
            <asp:ValidationSummary ID="vdsummary" Style="padding-left: 5%" runat="server" Display="none"
                class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup" />
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in"
                EnableClientScript="true" ValidationGroup="ComplianceValidationGroup" Style="display: none;" />            
            <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
            <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
        </div>

        <div id="ActDetails" class="row Dashboard-white-widget" style="margin-bottom: 0px; padding: 8px 10px 2px;">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                    <h2>Checklist Details</h2>
                                </a>
                                <div class="panel-actions">
                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                </div>
                            </div>
                        </div>

                        <div id="collapseActDetails" class="collapse">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                    <table style="width: 100%;">
                                        <tr class="spaceUnder">
                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Checklist Name</td>
                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                            <td style="width: 73%;">
                                                <asp:Label ID="lblChecklistName" Style="width: 88%; font-size: 13px; color: #333;"
                                                    autosize="true" runat="server" />
                                            </td>
                                        </tr>
                                         <tr class="spaceUnder">
                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Description</td>
                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                            <td style="width: 73%;">
                                                <asp:Label ID="lblDescription" Style="width: 88%; font-size: 13px; color: #333;"
                                                    autosize="true" runat="server" />
                                            </td>
                                        </tr>
                                        <tr class="spaceUnder">
                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">State </td>
                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                            <td style="width: 73%;">
                                                <asp:Label ID="LblState" Style="width: 88%; font-size: 13px; color: #333;"
                                                    autosize="true" runat="server" />
                                            </td>
                                        </tr>
                                        <tr class="spaceUnder">
                                            <td style="width: 25%; font-weight: bold;">Section</td>
                                            <td style="width: 2%; font-weight: bold;">: </td>
                                            <td style="width: 73%;">
                                                <asp:Label ID="LblSection" Style="width: 300px; font-size: 13px; color: #333;"
                                                    maximunsize="300px" autosize="true" runat="server" />
                                            </td>
                                        </tr>
                                        <tr class="spaceUnder">
                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Checklist Rule</td>
                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                            <td style="width: 73%;">
                                                <asp:Label ID="LblChecklistRule" Style="width: 300px; font-size: 13px; color: #333;"
                                                    maximunsize="300px" autosize="true" runat="server" />
                                            </td>
                                        </tr>
                                        <tr class="spaceUnder">
                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Type of Compliance</td>
                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                            <td style="width: 73%;">
                                                <asp:Label ID="LbltypeofCompliance" Style="width: 300px; font-size: 13px; color: #333;" maximunsize="300px" autosize="true" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix" style="height: 10px; background-color: #f7f7f7;"></div>
        <div id="UpdateComplianceStatus" class="row Dashboard-white-widget" style="margin-bottom: 0px; padding: 8px 10px 2px;">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-default" style="margin-bottom: 1px;">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus">
                                    <%--<h2>Upload  Document</h2>--%>
                                      <h2><asp:Label ID="LblNatureofCompliance" Style="color: #333;"
                                                    autosize="true" runat="server" /></h2>
                                </a>
                                <div class="panel-actions">
                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                </div>
                            </div>
                        </div>
                        <div id="collapseUpdateComplianceStatus" class="panel-collapse collapse in">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">

                                <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                                    <table style="width: 100%">
                                        
                                        <tr class="spaceUnder">
                                            <td style="width: 25%;">
                                                <label style="font-weight: bold; vertical-align: text-top;">Upload  Document(s)</label>
                                            </td>
                                            <td style="width: 2%; font-weight: bold;">: </td>
                                            <td style="width: 38%;">
                                                <asp:FileUpload ID="docFileUpload" multiple="multiple"  runat="server" />
                                            </td>
                                            <td style="width: 35%; float: right;">
                                                <asp:Button ID="UploadDocument" runat="server" Text="Upload Document" OnClick="UploadDocument_Click"
                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Ddocument"
                                                    CausesValidation="true" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <hr style="border-bottom: 0px solid #dddddd;" />
                                <div style="margin-bottom: 7px">
                                    <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                        PageSize="10" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField ItemStyle-Wrap="false" HeaderText="Expired On" ItemStyle-Width="2%" HeaderStyle-Font-Bold="true">
                                                                                    <ItemTemplate>
                                                                                            <asp:Label ID="txtExpire" data-toggle="tooltip" runat="server" ForeColor="Red"  visible='<%#  Convert.ToDateTime(Eval("ValidTill")) > DateTime.Now ||  Eval("ValidTill") == null?false:true%>' ToolTip='<%#  Eval("ValidTill")!= null?((DateTime)Eval("ValidTill")).ToString("dd-MMM-yyyy"):""%>' Text='<%#  Eval("ValidTill")!= null?((DateTime)Eval("ValidTill")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Document Uploaded Date" ItemStyle-Width="10%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CreatedOn")!= null?((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("CreatedOn")!= null?((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                <ItemTemplate>                                          
                                                     <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                        visible='<%#  Convert.ToDateTime(Eval("ValidTill")) > DateTime.Now || Eval("ValidTill") == null?true:false%>' CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>

                                                    <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                        visible='<%#  Convert.ToDateTime(Eval("ValidTill")) > DateTime.Now || Eval("ValidTill") == null?true:false%>' CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                    
                                                    <asp:LinkButton ID="lnkDeleteDocument" Visible='<%# Eval("ValidTill")!= null?false:true%>' runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>                                                                                                        
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <PagerTemplate>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                                <div style="margin-bottom: 7px">
                                    <table style="width: 100%">
                                        <tr class="spaceUnder">
                                            <td style="width: 25%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                            </td>
                                            <td style="width: 2%; font-weight: bold;">: </td>
                                            <td colspan="2" style="width: 73%;">
                                                <asp:TextBox runat="server" ID="txtRemark" TextMode="MultiLine" class="form-control" Rows="2" />
                                                 <asp:RequiredFieldValidator ID="RFVtxtRemark" ErrorMessage="Required Remark."
                                        ControlToValidate="txtRemark" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                            </td>
                                        </tr>
                                        <tr  class="spaceUnder" id="trtimeline" runat="server" visible="false">
                                            <td style="width: 25%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                <label style="font-weight: bold; vertical-align: text-top;">TimeLine</label>
                                            </td>
                                            <td style="width: 2%; font-weight: bold;">: </td>
                                            <td  colspan="1">
                                                <asp:TextBox runat="server" ID="txtTimeLine" Style="width: 25%;" ReadOnly="true" class="form-control" />
                                            </td>
                                        </tr>
                                        <tr class="spaceUnder">
                                            <td style="width: 25%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                            </td>
                                            <td style="width: 2%; font-weight: bold;">: </td>
                                            <td colspan="1">
                                                <asp:DropDownListChosen runat="server" ID="ddlStatus" DataPlaceHolder="Select Status"
                                                    AllowSingleDeselect="false" 
                                                    Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="false" />
                                                <asp:RequiredFieldValidator ID="rfvddlStatus" ErrorMessage="Please Select Status."
                                                    ControlToValidate="ddlStatus" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                            </td>
                                        </tr>
                                        <tr class="spaceUnder">
                                            <td style="width: 25%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                <label style="font-weight: bold; vertical-align: text-top;">Please select check box If documents are not available? </label>
                                            </td>
                                            <td style="width: 2%; font-weight: bold;">: </td>
                                            <td colspan="1">
                                                  <asp:CheckBox ID="chkDocument" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%;"></td>
                                            <td style="width: 2%;"></td>
                                             <td style="width: 100%; float: right;">
                                                <asp:Button ID="btnSave" CausesValidation="true" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" OnClick="btnSave_Click"
                                                    Width="90px" Style="margin-top: 10px; margin-left: 26%;" runat="server" Text="Save" />
                                               <asp:Button ID="btnClose" CausesValidation="true" class="btn btn-search"  OnClientClick="fCloseBack()"
                                                    Width="90px" Style="margin-top: 10px; margin-left: 26%;" runat="server" Text="Back" />
                                             </td>
                                            <td style="width: 38%;">

                                  

                                            </td>
                                           
                                        </tr>
                                    </table>
                                </div>

                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix" style="height: 10px; background-color: #f7f7f7;"></div>
        <div id="AuditLog" class="row Dashboard-white-widget" style="margin-bottom: 0px; padding: 8px 10px 2px;">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-default" style="margin-bottom: 1px;">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog">
                                    <h2>Audit Log</h2>
                                </a>
                                <div class="panel-actions">
                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog"><i class="fa fa-chevron-up"></i></a>
                                </div>
                            </div>
                        </div>


                        <div id="collapseAuditLog" class="collapse">
                            <div runat="server" id="log" style="text-align: left;">
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                        <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                            AllowPaging="true" PageSize="20" CssClass="table" GridLines="Horizontal"
                                            BorderWidth="0px" DataKeyNames="AuditTransactionID">
                                            <Columns>
                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                        </asp:GridView>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


          <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal1" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">                                               
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                                </fieldset>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>                 
    </form>
</body>
</html>

