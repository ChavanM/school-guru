﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCS_ReviewerPerformerTransaction: System.Web.UI.Page
    {

        public static bool masterDocument = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                    && !string.IsNullOrEmpty(Request.QueryString["AID"])
                       && !string.IsNullOrEmpty(Request.QueryString["SOID"])
                         && !string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    BindStatusList();
                    long CheckListID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long auditid = Convert.ToInt64(Request.QueryString["AID"].ToString());
                    long scheduleonid = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    int CheckListStatus = Convert.ToInt32(Request.QueryString["SID"]);
                    if (CheckListID > 0)
                    {
                        GetAllValues(CheckListID);
                        BindTransactions();
                        BindGrid();
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var getChecklist = (from row in entities.RLCS_Vendor_RecentCheckListStatus(auditid, CheckListID, scheduleonid)
                                                select row).FirstOrDefault();
                            if (getChecklist != null)
                            {
                                if (getChecklist.IsDocument)
                                    chkDocument.Checked = true;

                                if (!string.IsNullOrEmpty(Convert.ToString(getChecklist.ResultID)))
                                {
                                    ddlStatus.SelectedValue = Convert.ToString(getChecklist.ResultID);
                                }
                                //txtTimeLine.Text = getChecklist.TimeLine != null ? getChecklist.TimeLine.Value.ToString("dd-MMM-yyyy") : null;
                                //if (!string.IsNullOrEmpty(txtTimeLine.Text))
                                //{
                                //    trtimeline.Visible = true;

                                //    DateTime startdate = DateTime.Today.Date;
                                //    DateTime Enddate = Convert.ToDateTime(txtTimeLine.Text);
                                //    int compareValue;
                                //    try
                                //    {                                       
                                //        compareValue = Enddate.CompareTo(startdate);
                                //        if (compareValue == -1)
                                //        {
                                //            //End date cannot be less than start date                                            
                                //            UploadDocument.Enabled = false;
                                //            btnSave.Enabled = false;
                                //            cvDuplicateEntry.IsValid = false;
                                //            cvDuplicateEntry.ErrorMessage = "Timeline  for the checklist has passed/expired. So, You can not be able to change anything";
                                //        }                                       
                                //        else
                                //        {
                                //            //End date greater than start date
                                //            UploadDocument.Enabled = true;
                                //            btnSave.Enabled = true;
                                //        }
                                //    }
                                //    catch (ArgumentException)
                                //    {
                                //        UploadDocument.Enabled = false;
                                //        btnSave.Enabled = false;
                                //    }
                                //}
                                //else
                                //{
                                //    trtimeline.Visible = false;
                                //}
                            }
                        }
                    }
                }
            }
        }

        private void BindStatusList()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;

                        if (!string.IsNullOrEmpty(vrole))
                        {
                            var data = RLCSVendorMastersManagement.getRLCSResultStatus(vrole);
                            ddlStatus.DataTextField = "Name";
                            ddlStatus.DataValueField = "ID";
                            ddlStatus.DataSource = data;
                            ddlStatus.DataBind();
                            ddlStatus.Items.Insert(0, new ListItem("Select Status ", "-1"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetAllValues(long CheckListID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getChecklist = (from row in entities.RLCS_tbl_CheckListMaster
                                    where row.IsDeleted == false && row.Id == CheckListID
                                    select row).FirstOrDefault();
                if (getChecklist != null)
                {
                    LblNatureofCompliance.Text = getChecklist.NatureOfCompliance.ToString();
                    lblChecklistName.Text = getChecklist.NatureOfCompliance.ToString();
                    lblDescription.Text = string.Empty;
                    if (getChecklist.Description != null)
                    {
                        lblDescription.Text = getChecklist.Description.ToString();
                    }
                    LblState.Text = getChecklist.StateID.ToString();
                    LblSection.Text = getChecklist.Section.ToString();
                    LblChecklistRule.Text = getChecklist.Checklist_Rule.ToString();
                    LbltypeofCompliance.Text = getChecklist.TypeOfCompliance.ToString();
                }
            }
        }
        private void BindTransactions()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                   && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    int CheckListID = Convert.ToInt32(Request.QueryString["CHID"].ToString());
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                    grdTransactionHistory.DataSource = RLCSVendorMastersManagement.GetAllTransactionLog(ScheduledOnID, CheckListID);
                    grdTransactionHistory.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        public void BindGrid()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])                   
                      && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    long ChecklistID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long ScheduleOnID = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstChecklistDetails = RLCSVendorMastersManagement.getDocumentData(ChecklistID, ScheduleOnID, VendorID);
                        if (lstChecklistDetails.Count > 0)
                        {
                            ViewState["DocumentListCount"] = lstChecklistDetails.Count;
                            
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                            chkDocument.Enabled = false;
                        }
                        else
                        {
                            ViewState["DocumentListCount"] = 0;
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                            chkDocument.Enabled = true;
                        }
                        lstChecklistDetails.Clear();
                        lstChecklistDetails = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                       && !string.IsNullOrEmpty(Request.QueryString["AID"])
                          && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    int UserID = AuthenticationHelper.UserID;
                    int ChecklistID = Convert.ToInt32(Request.QueryString["CHID"].ToString());
                    int AuditID = Convert.ToInt32(Request.QueryString["AID"].ToString());
                    int ScheduleOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                    if (docFileUpload != null)
                    {
                        if (docFileUpload.HasFile)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<RLCSVendorFileData> files = new List<RLCSVendorFileData>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            HttpFileCollection fileCollection = Request.Files;
                            bool blankfileCount = true;
                            if (fileCollection.Count > 0)
                            {
                                string directoryPath = null;
                                string version = null;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\RLCSVendorDocuments\\" + UserID + "\\" + AuditID.ToString() + "\\" + ChecklistID.ToString() + "\\" + ScheduleOnID + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/RLCSVendorDocuments/" + UserID + "/" + AuditID.ToString() + "/" + ChecklistID.ToString() + "/" + ScheduleOnID + "/" + version);
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = fileCollection[i];

                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    String fileName = "";

                                    fileName = uploadfile.FileName;
                                    list.Add(new KeyValuePair<string, int>(fileName, 1));

                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                    if (uploadfile.ContentLength > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }
                                        RLCSVendorFileData file = new RLCSVendorFileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            Version = version,
                                            CreatedOn = DateTime.Now,
                                            FileSize = uploadfile.ContentLength.ToString()
                                        };
                                        files.Add(file);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            bool flag = false;
                            if (blankfileCount)
                            {
                                flag = RLCSVendorMastersManagement.FileUpload(Convert.ToInt64(ChecklistID), Convert.ToInt64(ScheduleOnID), files, list, Filelist);

                                if (flag)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "File Upload Successfully.";
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please attached at least one file.";
                        }
                    }
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }              
        }

        public class dataEmail
        {
            public string ActName { get; set; }
            public string Stateid { get; set; }
            public string NatureOfCompliance { get; set; }
            public string TypeOfCompliance { get; set; }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool FlagIsDocument = false;
                int DocumnetCount = Convert.ToInt32(ViewState["DocumentListCount"]);
                if (DocumnetCount > 0)
                {
                    FlagIsDocument = true;
                }
                else
                {
                    if (chkDocument.Checked)
                    {
                        FlagIsDocument = true;
                    }
                }

                if (FlagIsDocument)
                {
                    if (!string.IsNullOrEmpty(txtRemark.Text) && !string.IsNullOrEmpty(ddlStatus.SelectedValue))
                    {
                        if (ddlStatus.SelectedValue != "-1")
                        {

                            if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                                 && !string.IsNullOrEmpty(Request.QueryString["AID"])
                                    && !string.IsNullOrEmpty(Request.QueryString["SOID"])
                                    && !string.IsNullOrEmpty(Request.QueryString["CBID"]))
                            {
                                long CheckListID = Convert.ToInt32(Request.QueryString["CHID"].ToString());
                                long AuditId = Convert.ToInt32(Request.QueryString["AID"].ToString());
                                long ScheduleOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                                long branchID = Convert.ToInt32(Request.QueryString["CBID"].ToString());
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    RLCS_VendorAuditScheduleOn checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                                                             where row.ID == ScheduleOnID
                                                                             && row.IsActive == true
                                                                             select row).FirstOrDefault();
                                    if (checkExist != null)
                                    {
                                        if (checkExist.AuditStatusID == 1)
                                        {
                                            checkExist.AuditStatusID = 2;
                                            entities.SaveChanges();
                                        }
                                        RLCS_VendorAuditTransaction transaction = new RLCS_VendorAuditTransaction()
                                        {
                                            AuditID = AuditId,
                                            VendorAuditScheduleOnID = ScheduleOnID,
                                            StatusId = 4,
                                            Remarks = txtRemark.Text,
                                            Recommendation = txtRecommendation.Text,
                                            Dated = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                            CreatedByText = AuthenticationHelper.User,
                                            CustomerBranchId = Convert.ToInt64(branchID),
                                            CheckListId = CheckListID,
                                            StatusChangedOn = DateTime.Now,
                                            ResultId = Convert.ToInt64(ddlStatus.SelectedValue),
                                            IsDocument = chkDocument.Checked
                                        };
                                        bool saveflag = RLCS_VendorScheduler.CreateTransaction(transaction);
                                        if (saveflag)
                                        {
                                            int CheckListStatus = Convert.ToInt32(Request.QueryString["SID"]);
                                            if (CheckListStatus == 3)
                                            {
                                                try
                                                {
                                                    var userdetil = entities.sp_GetScheduleDetail(Convert.ToInt32(ScheduleOnID)).FirstOrDefault();

                                                    if (userdetil != null)
                                                    {
                                                        string ForMonth = string.Empty;
                                                        string ActName = string.Empty;
                                                        string StateName = string.Empty;
                                                        string NatureOfCompliance = string.Empty;
                                                        string TypeOfCompliance = string.Empty;

                                                        long ChklistID = Convert.ToInt32(Request.QueryString["CHID"].ToString());

                                                        var getChecklist = (from row in entities.RLCS_tbl_CheckListMaster
                                                                            join row1 in entities.RLCS_Act_Mapping
                                                                            on row.ActID equals row1.AM_ActID
                                                                            where row.IsDeleted == false && row.Id == ChklistID
                                                                            select new dataEmail
                                                                            {
                                                                                ActName = row1.AM_ActName,
                                                                                Stateid = row.StateID,
                                                                                NatureOfCompliance = row.NatureOfCompliance,
                                                                                TypeOfCompliance = row.TypeOfCompliance
                                                                            }).FirstOrDefault();

                                                        if (getChecklist != null)
                                                        {
                                                            ForMonth = userdetil.ForMonth;
                                                            NatureOfCompliance = getChecklist.NatureOfCompliance;
                                                            TypeOfCompliance = getChecklist.TypeOfCompliance;
                                                            ActName = getChecklist.ActName;
                                                            StateName = getChecklist.Stateid;
                                                        }

                                                        DateTime SDstartDate = userdetil.ScheduleOn;
                                                        DateTime today = DateTime.Now;

                                                        string ClientName = userdetil.ClientName;
                                                        string Subject = ClientName + " Documents reuploaded for the period(" + userdetil.ForMonth + ")";
                                                        string formattedDate = SDstartDate.AddDays(10).ToString("dd-MM-yyyy");

                                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_RLCSReUploadDocument
                                                                        .Replace("@User", userdetil.CC_SPOC_Name)
                                                                        .Replace("@Date", formattedDate)
                                                                        .Replace("@ForMonth", ForMonth)
                                                                        .Replace("@StName", StateName)
                                                                        .Replace("@ActName", ActName)
                                                                        .Replace("@NatureOfCompliance", NatureOfCompliance)
                                                                        .Replace("@TypeofCompliance", TypeOfCompliance)
                                                                        .Replace("@Status", "Submitted");

                                                        List<string> aa = new List<string>();
                                                        aa.Add(userdetil.AuditorEmail);
                                                        //aa.Add("amol@avantis.info");

                                                        RLCS_Vendor_AuditScheduleMail_Log obj = new RLCS_Vendor_AuditScheduleMail_Log();
                                                        obj.AuditID = Convert.ToInt32(AuditId);
                                                        obj.Avacom_UserID = Convert.ToInt32(userdetil.VendorID);
                                                        obj.ScheduleOnID = Convert.ToInt32(ScheduleOnID);
                                                        obj.CreatedOn = DateTime.Now;
                                                        obj.IsSend = true;
                                                        obj.Flag = "Document Uploaded from Vendor in Team Review";
                                                        entities.RLCS_Vendor_AuditScheduleMail_Log.Add(obj);
                                                        entities.SaveChanges();

                                                        SendGridEmailManager.SendGridMail(ConfigurationManager.AppSettings["SenderEmailAddress"], "info@avantis.co.in", aa, null, null, Subject, message);

                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                }
                                            }

                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Record Save Successfully.";
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Status.";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(txtRemark.Text))
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Remark.";
                        }
                        else if (string.IsNullOrEmpty(ddlStatus.SelectedValue) || ddlStatus.SelectedValue == "-1")
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Status.";
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select check box because documents are not available";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }            
        }
        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (RLCSVendorMastersManagement.DeleteRLCSVendorFile(FileID, Convert.ToInt32(AuthenticationHelper.UserID)))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);
                            BindGrid();
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=VendorAudit.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }                            
                        }
                        BindGrid();
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {

                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);                                  
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    string CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                    //lblMessage.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);                             
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }                        
        }

    
    }

}