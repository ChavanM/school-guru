﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class VerifyOtpNew : System.Web.UI.Page
    {
        public static string key = "A!9HHhi%XjjYY4YP2@Nob009X";
        static int WrongOTPCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    divVerifyotp.Visible = false;
                    spangenarteotp.Visible = false;
                    Labelmsg.Text = "will be send";
                    //string EncryptUsercfo = Encrypt("cfo@bitaconsulting.co.in");
                    //string EncryptUser = Encrypt("rishi@avantis.co.in");
                    //string Encryptdate = Encrypt("2018-12-10 06:01:59.000");                    
                    int userId = -1;
                    int customerId = -1;
                    int RoleId = -1;
                    bool Success = false;
                    string DecryptedEmail = string.Empty;

                    if (!string.IsNullOrEmpty(Request.QueryString["x"]) && !string.IsNullOrEmpty(Request.QueryString["y"]))
                    {

                        DecryptedEmail = Decrypt(Request.QueryString["x"].Replace(" ", "+"));
                        CreateDate.Text = Decrypt(Request.QueryString["y"].Replace(" ", "+"));
                        if (!string.IsNullOrEmpty(DecryptedEmail))
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                User user = (from row in entities.Users
                                             where (row.Email == DecryptedEmail)
                                               && row.IsActive == true
                                               && row.IsDeleted == false
                                             select row).FirstOrDefault();

                                if (user != null)
                                {
                                    //string ipaddress = string.Empty;
                                    //try
                                    //{                                 
                                    //    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                    //    if (ipaddress == "" || ipaddress == null)
                                    //        ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    //}

                                    userId = Convert.ToInt32(user.ID);
                                    customerId = Convert.ToInt32(user.CustomerID);
                                    RoleId = Convert.ToInt32(user.RoleID);
                                    UserEmail.Text = DecryptedEmail;
                                    FirstNameUser.Text = user.FirstName.ToString();
                                    UserId.Text = userId.ToString();
                                    Label1.Text = DecryptedEmail.Substring(0, 1).ToUpper();

                                    #region Email,Time,contact number display
                                    // For Email Id Logic Set.
                                    string[] spl = Convert.ToString(DecryptedEmail).Split('@');
                                    int lenthusername = (spl[0].Length / 2);
                                    int mod = spl[0].Length % 2;
                                    if (mod > 0)
                                    {
                                        lenthusername = lenthusername - mod;
                                    }
                                    string beforemanupilatedemail = "";
                                    if (lenthusername == 1)
                                    {
                                        beforemanupilatedemail = spl[0].Substring(lenthusername);
                                    }
                                    else
                                    {
                                        beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
                                    }

                                    string appendstar = "";
                                    for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
                                    {
                                        appendstar += "*";
                                    }

                                    string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

                                    string[] spl1 = spl[1].Split('.');
                                    int lenthatname = (spl1[0].Length / 2);
                                    int modat = spl1[0].Length % 2;
                                    if (modat > 0)
                                    {
                                        lenthatname = lenthatname - modat;
                                    }

                                    string beforatemail = "";
                                    if (lenthatname == 1)
                                    {
                                        beforatemail = spl1[0].Substring(lenthatname);
                                    }
                                    else
                                    {
                                        beforatemail = spl1[0].Substring(lenthatname + 1);
                                    }
                                    string appendstar1 = "";
                                    for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
                                    {
                                        appendstar1 += "*";
                                    }

                                    string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
                                    string emailid = manupilatedemail;

                                    DateTime NewTime = DateTime.Now.AddMinutes(30);
                                    Time.Text = NewTime.Hour + ":" + NewTime.Minute + NewTime.ToString(" tt");
                                    email.Text = Convert.ToString(DecryptedEmail).Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);

                                    if (!string.IsNullOrEmpty(Convert.ToString(user.ContactNumber)))
                                    {
                                        // For Mobile Number Logic Set.
                                        if (Convert.ToString(user.ContactNumber).Length == 10 && (Convert.ToString(user.ContactNumber).StartsWith("9") || Convert.ToString(user.ContactNumber).StartsWith("8") || Convert.ToString(user.ContactNumber).StartsWith("7")))
                                        {
                                            string s = Convert.ToString(user.ContactNumber);
                                            int l = s.Length;
                                            s = s.Substring(l - 4);
                                            string r = new string('*', l - 4);
                                            //r = r + s;
                                            mobileno.Text = r + s;
                                        }
                                        else
                                        {
                                            mobileno.Text = "";
                                            //cvLogin.IsValid = false;
                                            //cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                                        }
                                    }
                                    else
                                    {
                                        mobileno.Text = "";
                                        //cvLogin.IsValid = false;
                                        //cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                                    }
                                    #endregion
                                    //if (!(user.WrongAttempt >= 3))
                                    //if (true) /*UserManagement.WrongAttemptCount(txtemail.Text.Trim())*/
                                    //{
                                    //if (user.IsActive && user.LastLoginTime != null)
                                    //if (user.IsActive)
                                    //{
                                    //    Random random = new Random();
                                    //    int value = random.Next(1000000);
                                    //    long Contact;

                                    //    VerifyOTP OTPData = new VerifyOTP()
                                    //    {
                                    //        UserId = Convert.ToInt32(user.ID),
                                    //        EmailId = DecryptedEmail.Trim(),
                                    //        OTP = Convert.ToInt32(value),
                                    //        CreatedOn = DateTime.Now,
                                    //        IsVerified = false,
                                    //        IPAddress = ipaddress
                                    //    };
                                    //    bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                    //    if (OTPresult)
                                    //    {
                                    //        OTPData.MobileNo = Contact;
                                    //    }
                                    //    else
                                    //    {
                                    //        OTPData.MobileNo = 0;
                                    //    }
                                    //    VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                                    //    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                    //    try
                                    //    {
                                    //        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        Success = false;
                                    //    }
                                    //    if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                                    //    {
                                    //        try
                                    //        {
                                    //            SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.");
                                    //        }
                                    //        catch (Exception ex)
                                    //        {
                                    //            Success = false;
                                    //        }
                                    //    }
                                    //    if (Success)
                                    //    {

                                    //    }
                                    //    else
                                    //    {

                                    //    }
                                    //}
                                    //}
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                Response.Redirect("Blank.aspx", false);

            }
        }



        public static string Encrypt(string text)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }

        public static string Decrypt(string cipher)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateDecryptor())
                    {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }

        protected void btnOTP_Click(object sender, EventArgs e)
        {

            try
            {
                int userID = -1;
                if (!string.IsNullOrEmpty(UserId.Text))
                {
                    userID = Convert.ToInt32(UserId.Text);
                }

                if (userID != null)
                {
                    if (VerifyOTPManagement.verifyOTP(Convert.ToInt32(txtOTP.Text), userID))
                    {
                        User user = UserManagement.GetByID(userID);

                        VerifyOTP OTPData = VerifyOTPManagement.GetByID(Convert.ToInt32(userID));
                        TimeSpan span = DateTime.Now.Subtract(OTPData.CreatedOn.Value);
                        if (0 <= span.Minutes && span.Minutes <= 30)
                        {
                            VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(userID), Convert.ToInt32(txtOTP.Text));

                            //string EcnrypetedUserId = Encrypt(userID.ToString());
                            //window.location = "ReportFile.aspx?UserId=" + $("#<%=UserId.ClientID%>")[0].value + "&CreateDate=" + $("#<%=CreateDate.ClientID%>")[0].value;
                            Response.Redirect("ReportFile.aspx?UserId=" + userID + "&CreateDate=" + CreateDate.Text.ToString(), false);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage3", "settracknew('Login', 'VerifyOTP', 'Success', '')", true);
                        }
                        else
                        {
                            cvLogin.ErrorMessage = "OTP Expired.";
                            cvLogin.IsValid = false;
                            return;
                        }
                    }
                    else
                    {
                        if (WrongOTPCount < 3)
                        {
                            cvLogin.ErrorMessage = "Invalid OTP, Please Enter Again.";
                            cvLogin.IsValid = false;
                            WrongOTPCount++;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage6", "settracknew('Login', 'VerifyOTP', 'Failed', '')", true);
                            return;
                        }
                        else
                        {
                            cvLogin.ErrorMessage = "Invalid OTP, Please Enter Again.";
                            cvLogin.IsValid = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage4", "settracknew('Login', 'VerifyOTP', 'Failed', '')", true);
                            return;
                        }
                    }
                }
                else
                {
                    cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                    cvLogin.IsValid = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage5", "settracknew('Login', 'VerifyOTP', 'Failed', '')", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkResendOTP_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage", "settracknew('Login','VerifyOTP','ResendOTP','')", true);
            bool Success = false;
            int userID = -1;
            if (!string.IsNullOrEmpty(UserId.Text))
            {
                userID = Convert.ToInt32(UserId.Text);
            }
            if (userID != null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User user = (from row in entities.Users
                                 where (row.ID == userID)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {
                        if (user.IsActive)
                        {
                            string ipaddress = string.Empty;
                            try
                            {
                                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                if (ipaddress == "" || ipaddress == null)
                                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            Random random = new Random();
                            int value = random.Next(1000000);
                            long Contact;

                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = user.Email.ToString(),
                                OTP = Convert.ToInt32(value),
                                CreatedOn = DateTime.Now,
                                IsVerified = false,
                                IPAddress = ipaddress
                            };
                            bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                            if (OTPresult)
                            {
                                OTPData.MobileNo = Contact;
                            }
                            else
                            {
                                OTPData.MobileNo = 0;
                            }
                            VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            try
                            {
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                Success = true;
                            }
                            catch (Exception ex)
                            {
                                Success = false;
                            }
                            if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                            {
                                try
                                {                                    
                                    var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                    if (data != null)
                                    {
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                    }
                                    else
                                    {
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                    }

                                    Success = true;
                                }
                                catch (Exception ex)
                                {
                                    Success = false;
                                }
                            }
                            if (Success)
                            {
                                cvLogin.ErrorMessage = "OTP Sent Successfully.";
                                cvLogin.IsValid = false;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage2", "settracknew('Login', 'VerifyOTP', 'Do not have access to OTP', '')", true);
                                //cvLogin.ErrorMessage = "OTP Sent Successfully.";
                                //cvLogin.IsValid = false;
                            }
                        }                     
                    }
                }
            }
        }

        protected void BtnGenerate_Click(object sender, EventArgs e)
        {
            divgenarteotp.Visible = false;
            divVerifyotp.Visible = true;
            spangenarteotp.Visible = true;
            Labelmsg.Text = "has been sent";
            
            bool Success = false;
            int userID = -1;
            if (!string.IsNullOrEmpty(UserId.Text))
            {
                userID = Convert.ToInt32(UserId.Text);
            }
            if (userID != null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User user = (from row in entities.Users
                                 where (row.ID == userID)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {
                        if (user.IsActive)
                        {
                            string ipaddress = string.Empty;
                            try
                            {
                                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                if (ipaddress == "" || ipaddress == null)
                                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            Random random = new Random();
                            int value = random.Next(1000000);
                            long Contact;

                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = user.Email.ToString(),
                                OTP = Convert.ToInt32(value),
                                CreatedOn = DateTime.Now,
                                IsVerified = false,
                                IPAddress = ipaddress
                            };
                            bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                            if (OTPresult)
                            {
                                OTPData.MobileNo = Contact;
                            }
                            else
                            {
                                OTPData.MobileNo = 0;
                            }
                            VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            try
                            {
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                Success = true;
                            }
                            catch (Exception ex)
                            {
                                Success = false;
                            }
                            if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                            {
                                try
                                {
                                    var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                    if (data != null)
                                    {
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                    }
                                    else
                                    {
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                    }
                                    
                                    Success = true;
                                }
                                catch (Exception ex)
                                {
                                    Success = false;
                                }
                            }
                            if (Success)
                            {
                                cvLogin.ErrorMessage = "OTP Sent Successfully.";
                                cvLogin.IsValid = false;
                            }
                            else
                            {
                                //cvLogin.ErrorMessage = "OTP Sent Successfully.";
                                //cvLogin.IsValid = false;
                            }
                        }
                    }
                }
            }
        }
    }
}