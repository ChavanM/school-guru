﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="InsiderAuditorMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InsiderAuditorMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">
.k-i-arrow-60-down{
         margin-top:5px !important;
    }
    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
    <style>
        body{
        color:#666;
        font-weight:300;
    }
        .form-group.required .control-label:after { 
   content:"*";
   color:red;
}
        html .k-grid tr:hover {
    background: #E4F7FB;
}
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="col-sm-12">
                     <div class="col-sm-12" style="padding:0">
                <div class="toolbar"> 
                    <label class="control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Select Firm: &nbsp; </label>
                <input id="dropdownlistFirmType" data-placeholder="Type" style="width:172px;">
                 <button  style="float:right;"  id="button" type="button" class="btn btn-primary"  onclick="OpenAdvanceSearch(event)" >Add New</button> 
                </div>
                 
                        
            </div>
        <div class="col-sm-12" style="margin-bottom:1%;" >
            
        </div>

    <div class="col-sm-12" id="grid"></div>
  </div>    
        <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999;display:none;">

                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;display:none;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>                      
                    </div>
                </div>
                <div  class="col-sm-12" style="padding:0">
                       <form  class="form-inline" method="POST" id="fcontact"  >
                  
            <div class="col-sm-12" style="padding:0;padding-top:10px;">
            <div class="col-sm-6  required " style="padding:0;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Firm Name:</label>
                <input id="dropdownFirmName" data-placeholder="Type" style="width:40%">
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>

                <div class="col-sm-6  required " style="padding:0;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Name:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="fname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            
            <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Designation:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="designation" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Designation" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">PAN:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="pan" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="PAN" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Email:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="email" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Email" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Mobile:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="mobile" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Mobile" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Status:</label>
                <input id="dropdownStatus" data-placeholder="Type" style="width:40%">
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>

            
                
            </div>
            
            
          

                 
                
                   
           
            
            </div>
            <button id="saveBtn" style="margin-top:40px;" type="submit" onclick="saveDetails(event)" class="btn btn-primary">Save</button>
                                        
                       </form>
                      </div>
           </div>
    <input type="hidden" id="editFlag" value="0" />
        <script>

            $(document).ready(function () {
                $("#pagetype").text("Auditor Master");
                $("#grid").kendoGrid({

                    dataSource: {
                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getauditortable/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    AuditorName: { type: "string" },
                                    FirmName: { type: "string" },
                                    Designation: { type: "string" },
                                    EmployeeID: { type: "string" },

                                }
                            }
                        },
                        pageSize: 10
                    },
                    sortable: {
                        mode: "single",
                        allowUnsort: false
                    },
                    //height: 550,
                    //toolbar: ["search"],
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 5
                    },
                    columns: [{

                        field: "FirmName",
                        title: "Firm Name",
                        width: 150

                    },{

                        field: "AuditorName",
                        title: "Auditor Name",
                        width: 150

                    }, {

                            field: "Designation",
                        title: "Designation",
                        width: 150

                    }, {

                            field: "EmployeeID",
                        title: "PAN",
                        width: 130

                        }, {

                            field: "Email",
                            title: "Email",
                            width: 150

                        }, {

                            field: "Mobile",
                            title: "Mobile",
                            width: 120

                        }, {

                            field: "Status",
                            title: "Status",
                            width: 100

                        },
                        {
                        template: "<div class='customer-photo'" +
                                "style='background-image: url(../../Images/edit_icon.png);'" + " onClick='OpenAdvanceSearch(#= JSON.stringify(data) #)' >",
                        field: "",
                        title: "Action",
                        width: 100
                    }]
                });
            });
    </script>
<script>
    $("#dropdownStatus").kendoDropDownList({
        placeholder: "",
        dataTextField: "text",
        dataValueField: "value",
        checkboxes: true,
        checkAll: true,
        autoClose: true,
        dataSource: [
            { text: "Active", value: "1" },
            { text: "In Active", value: "0" },
        ],
        index: 0,
        change: function (e) {


        }

    });
    function OpenAdvanceSearch(dd) {
        if (dd.AuditorName != undefined) {
            $('#editFlag').val(dd.ID);
        } else {
            $('#editFlag').val(0);
        }
        $('#fname').val(dd.AuditorName);
        //alert(dd.FirmName)
        $("#dropdownFirmName").data("kendoDropDownList").value(dd.FirmName);
       // $("#dropdownStatus").data("kendoDropDownList").value(dd.Status);
        if (dd.Status == 'Active') {
            $("#dropdownStatus").data("kendoDropDownList").value(1);
        } else {
            $("#dropdownStatus").data("kendoDropDownList").value(0);
        }
       // $('#dropdownFirmName').val();
        $('#designation').val(dd.Designation);
        $('#pan').val(dd.EmployeeID);
        $('#email').val(dd.Email);
        $('#mobile').val(dd.Mobile);
       // $("#dropdownFirmName").data("kendoDropDownList").value(dd.FirmName);
        var myWindowAdv = $("#divAdvanceSearchModel");

        function onClose() {

        }

        myWindowAdv.kendoWindow({
            width: "65%",
            height: "35%",
            title: "Add New Auditor",
            visible: false,
            actions: [
                //"Pin",
                //"Minimize",
                "Maximize",
                "Close"
            ],
            close: onClose
        });
        $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

        myWindowAdv.data("kendoWindow").center().open();
        //e.preventDefault();
        return false;
    }

    function OpenAdvanceSearchFilter(e) {
        $('#divAdvanceSearchFilterModel').modal('show');
        e.preventDefault();
        return false;
    }
        </script>
    
    <script>
        $("#dropdownFirmName").kendoDropDownList({
            dataTextField: "FirmName",
            dataValueField: "FirmName",
            dataSource: {
                transport: {
                    read: {
                        url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmp/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                    }
                }
            }
        });
    </script>
    <script>
        function saveDetails(e) {
            e.preventDefault();
            //confirm("Are you sure want to add details?");
            var editFlag = $("#editFlag").val();

            var user_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
                var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
                var firmname = $("#dropdownFirmName").val();
                var is_active = $('#dropdownStatus').val();
                //alert(is_active)
                var fname = $('#fname').val();
                var designation = $('#designation').val();
                var pan = $('#pan').val();
                var email = $('#email').val();
                var mobile = $('#mobile').val();
                var created_by = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>";
            //var pdata = '{ "user_id":' + user_id + ' ,"customer_id":' + customer_id + ' ,"firmname":' + firmname + ' ,"designation":' + designation + ' ,"employeeID":' + pan + ' ,"auditorname":' + fname + ' ,"created_by":' + created_by + '   }';

                //alert(pdata);
                //pdata = JSON.parse(pdata);
                $.ajax({
                    type: 'post',
                    url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>createauditor/',
                    data: { "user_id": user_id, "customer_id": customer_id, "firmname": firmname, "designation": designation, "employeeID": pan, "auditorname": fname, "email": email, "mobile": mobile, "created_by": created_by, "editflag": editFlag, "is_active": is_active },
                    success: function (result) {
                        // alert(result.data);
                        if (editFlag == 0) {
                            alert("Auditor Successfully Created");

                        } else {
                            alert("Auditor Successfully Edited");

                        }
                        location.reload();

                    },
                    error: function (e, t) { alert('something went wrong'); }
                });

            return false;
        }
        </script>
    <script>
        $("#dropdownlistFirmType").kendoDropDownList({
            dataTextField: "FirmName",
            dataValueField: "ID",
            dataSource: {
                transport: {
                    read: {
                        url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmpall/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                    }
                }
            },
            change: function (e) {
                //alert(e.sender.value());
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getauditortablespecific/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + this.value()
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    Name: { type: "string" },
                                    Relation: { type: "string" },
                                    RelationCompany: { type: "string" },
                                    Address: { type: "string" },
                                    CIN: { type: "string" },

                                }
                            }
                        }


                    });
                var grid = $("#grid").data("kendoGrid");
                grid.setDataSource(dataSource);
                // Use the value of the widget
            }
        });
    </script>
</asp:Content>
