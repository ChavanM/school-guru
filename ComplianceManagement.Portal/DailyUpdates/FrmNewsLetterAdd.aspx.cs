﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.Drawing;
using System.Web.UI.HtmlControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates
{
    public partial class FrmNewsLetterAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Mode"] = Session["Mode"];
                ViewState["newsLetterID"] = Session["newsLetterID"];

                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                lblSampleForm.Text = "< Not selected >";
                lblSampleDoc.Text = "< Not selected >";
                txtTitle.Text = "";
                txtDescription.Text = "";
                if ((int)ViewState["Mode"] == 1)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.NewsLetter eventData = DailyUpdateManagment.GetByNewsLetterID(Convert.ToInt32(ViewState["newsLetterID"]));

                    txtTitle.Text = eventData.Title;
                    txtDescription.Text = eventData.Description;
                    lblSampleForm.Text = eventData.FileName;
                    lblSampleDoc.Text = eventData.DocFileName;
                    string StartDate = Convert.ToDateTime(eventData.NewsDate).ToString("dd-MM-yyyy");

                    tbxStartDate.Text = StartDate; 
                }
                else
                {
                    RequiredFieldValidator2.Enabled = true;
                    RequiredFieldValidator4.Enabled = true;
                }
            }
        }
        //protected void upEvent_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DateTime date = DateTime.MinValue;
        //        if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
        //        {
        //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string filePath="";
                string docfileName = "";
                string docfilePath = "";
                if ((int)ViewState["Mode"] == 1)
                {
                    var NewsLetter = DailyUpdateManagment.GetNewsletter(Convert.ToInt32(ViewState["newsLetterID"]));

                    fileName = NewsLetter.FileName;
                    filePath = NewsLetter.FilePath;
                    docfileName = NewsLetter.DocFileName;
                    docfilePath = NewsLetter.DocFilePath;
                }

                if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                {
                    fuSampleFile.SaveAs(Server.MapPath("../NewsLetterImages/" + fuSampleFile.FileName));
                    fileName = fuSampleFile.FileName;
                    filePath = "../NewsLetterImages/" + fuSampleFile.FileName;
                }

                if (FileUpload1.FileBytes != null && FileUpload1.FileBytes.LongLength > 0)
                {
                    FileUpload1.SaveAs(Server.MapPath("~/NewsLetterImages/" + FileUpload1.FileName));
                    docfileName = FileUpload1.FileName;
                    docfilePath = "~/NewsLetterImages/" + FileUpload1.FileName;
                }

                com.VirtuosoITech.ComplianceManagement.Business.Data.NewsLetter eventData = new com.VirtuosoITech.ComplianceManagement.Business.Data.NewsLetter()
                {
                    Title = txtTitle.Text,
                    Description = txtDescription.Text,
                    FileName = fileName,
                    FilePath = filePath,
                    DocFileName = docfileName,
                    DocFilePath = docfilePath,
                    NewsDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedDate = DateTime.Now.Date,
                    UpdatedDate = DateTime.Now.Date,
                    IsDeleted = false,
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    eventData.ID = Convert.ToInt32(ViewState["newsLetterID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (EventManagement.ExistsNewsLetter(eventData.Title))
                    {
                        cvDuplicateEntry.ErrorMessage = "News title already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    DailyUpdateManagment.CreateNewsLetter(eventData);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    DailyUpdateManagment.UpdateNewsLetter(eventData);
                }
                
                Response.Redirect("~/DailyUpdates/FrmNewsLetter.aspx", false);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DailyUpdates/FrmNewsLetter.aspx", false);
        }
      
    }
}