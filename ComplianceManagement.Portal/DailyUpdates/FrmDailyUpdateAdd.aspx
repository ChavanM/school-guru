﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="FrmDailyUpdateAdd.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates.FrmDailyUpdateAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>

    <script type="text/javascript" language="javascript">

        function Confirm() {

            if (document.getElementById("BodyContent_fudocument").files.length == 0) {

                if (confirm("Dailyupdate without document, to continue saving click OK!!")) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        tinyMCE.init({
            // General options
            mode: "textareas",
            //theme: "advanced",
            plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups",
            toolbar: 'fontselect',
            font_formats: 'Arial=arial'
        });
        function initializeCombobox() {
            $("#<%= ddlScheme.ClientID %>").combobox();
        }

         function initializeDatePicker(todayDate) {

             $('.monthYearPicker').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'M yy'
            }).focus(function () {
                var thisCalendar = $(this);
                $('.ui-datepicker-calendar').detach();
                $('.ui-datepicker-close').click(function () {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    thisCalendar.datepicker('setDate', new Date(year, month, 1));
                });
            });
        }
    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }



    .ui-datepicker-div {
    position: absolute;
    top: 946px;
    left: 165px;
    z-index: 1;
    display: block;
}
    .cssShortdes:hover{
        cursor:pointer;
        text-decoration:underline
    }

    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upEventList" runat="server" UpdateMode="Conditional" OnLoad="upEventList_Load">
        <ContentTemplate>
            <div style="margin: 5px;border:1px solid;width:50%;float:left">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                        ValidationGroup="EventValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="EventValidationGroup" Display="None" />
                </div>
                <asp:UpdatePanel ID="upEvent" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Is Miscellaneous?</label>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="IsMiscellaneous_CheckedChanged" ID="IsMiscellaneous" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="IsMiscellaneous" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Act
                            </label>
                            <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; width: 490px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvActList">
                                <asp:Repeater ID="rptActList" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable" style="width: 513px;">
                                            <tr>
                                                <td style="width: 52px;">
                                                    <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                <td style="width: 282px;">
                                                    <asp:UpdatePanel ID="upact" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Button runat="server" ID="btnRefresh" Text="Ok" Style="                                                                    float: left
                                                            " OnClick="btnRefresh_Click" /></td>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnRefresh" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                            <td style="width: 400px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                      

                        <div id="divScheme" runat="server" visible="false" style="margin-bottom: 7px; position: relative; display: inline-block;">
                            <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                <ContentTemplate>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                        *</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Scheme Name</label>
                                    <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlScheme" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 490px;"
                                        CssClass="txtbox" />
                                    <asp:CompareValidator ID="cmpScheme" ErrorMessage="Please select Scheme Name."
                                        ControlToValidate="ddlScheme" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                        ValidationGroup="EventValidationGroup" Display="None" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="ddlScheme" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>

                        
                        <div style="margin-bottom: 14px;display:none;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Filter ComplianceID Comma Seprated </label>
                            <asp:TextBox runat="server" ID="txtComplianceIDList" AutoPostBack="true" OnTextChanged="txtComplianceIDList_TextChanged" MaxLength="500" Width="490px" ToolTip="Name" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Title can not be empty." ControlToValidate="txtTitle"
                                runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                        </div>


                            <div style="margin-bottom: 7px;display:none;" id="divcompliance" runat="server" >
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>

                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Compliance
                            </label>
 
                            
                            <asp:TextBox runat="server" ID="txtcompliance" AutoPostBack="true" OnTextChanged="txtcompliance_TextChanged"  Style="padding: 0px; margin: 0px; height: 22px; width: 490px;"
                                CssClass="txtbox" />
                                  <asp:Button ID="Button1" runat="server" Text="Clear" OnClick="Button1_Click" />
                            <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;display:block !important;" id="dvComplianceList">
                                
                            </div>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Category
                            </label>
                            <asp:TextBox runat="server" ID="txtCategory" Style="padding: 0px; margin: 0px; height: 22px; width: 490px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvCategory">
                                <asp:Repeater ID="rptCategory" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                            <tr>
                                                <%--<td style="width: 100px;">
                                                    <asp:CheckBox ID="CategorySelectAll" Text="Select All" runat="server" onclick="checkAllCategory(this)" /></td>
                                                <td style="width: 282px;">
                                                    <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>--%>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkCategory" runat="server" onclick="UncheckHeaderCategory();" /></td>
                                            <td style="width: 200px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblCategoryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="lblCategoryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <div style="margin-bottom: 7px">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        DailyUpdate Type</label>
                                    <asp:DropDownList runat="server" ID="ddlDailyUpdateType" OnSelectedIndexChanged="ddlDailyUpdateType_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" AutoPostBack="true">
                                        <asp:ListItem Text="< Select >" Value="0" />
                                        <asp:ListItem Text="Central" Value="1" />
                                        <asp:ListItem Text="State" Value="2" />
                                    </asp:DropDownList>
                                    <asp:CompareValidator ErrorMessage="Please select DailyUpdate Type." ControlToValidate="ddlDailyUpdateType"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventValidationGroup"
                                        Display="None" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="ddlDailyUpdateType" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>

                        <div runat="server" id="divState" style="margin-bottom: 7px">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        State
                                    </label>
                                    <asp:TextBox runat="server" ID="txtState" Style="padding: 0px; margin: 0px; height: 22px; width: 490px;"
                                        CssClass="txtbox" />
                                    <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvState">
                                        <asp:Repeater ID="rptState" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                    <%--  <tr>
                                                        <td style="width: 100px;">
                                                            <asp:CheckBox ID="StateSelectAll" Text="Select All" runat="server" onclick="checkAllState(this)" /></td>
                                                        <td style="width: 282px;">
                                                            <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                                    </tr>--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkState" runat="server" onclick="UncheckHeaderState();" /></td>
                                                    <td style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                            <asp:Label ID="lblStateID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblStateName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="ddlDailyUpdateType" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Industry
                            </label>
                            <asp:TextBox runat="server" ID="txtIndustry" Style="padding: 0px; margin: 0px; height: 22px; width: 490px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 100px;" id="dvIndustry">
                                <asp:Repeater ID="rptIndustry" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                            <tr>
                                                <asp:UpdatePanel ID="upRepeater" runat="server">
                                                    <ContentTemplate>
                                                        <td style="width: 100px;">
                                                            <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="IndustrycheckAll(this)" /></td>
                                                        <td style="width: 282px;">
                                                            <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" /></td>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnRepeater" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckIndustryHeader();" /></td>
                                            <td style="width: 200px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Ministry</label>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList runat="server" ID="ddlMinistry" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" AutoPostBack="true" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Regulator</label>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList runat="server" ID="ddlRegulator" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" AutoPostBack="true" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Department</label>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" AutoPostBack="true" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Title</label>
                            <asp:TextBox runat="server" ID="txtTitle" MaxLength="500" Width="490px" ToolTip="Name" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Title can not be empty." ControlToValidate="txtTitle"
                                runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Sub Title</label>
                            <asp:TextBox runat="server" ID="txtSubTitle" MaxLength="500" Width="490px" ToolTip="Sub Title" />
                            <asp:RequiredFieldValidator ID="rfvSubTitle" ErrorMessage="Sub Title can not be empty." ControlToValidate="txtSubTitle"
                                runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Description</label>
                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="327px" Width="490px"></asp:TextBox>
                        </div>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Is Main/Sub Dailyupdate</label>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList runat="server" ID="ddlMainSub" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" AutoPostBack="true">
                                        <asp:ListItem Text="< Select >" Value="0" />
                                        <asp:ListItem Text="Main" Value="1" />
                                        <asp:ListItem Text="Sub" Value="2" />
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Is Translate</label>
                            <asp:UpdatePanel ID="upact" runat="server">
                                <ContentTemplate>
                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="chkIsTranslate" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Meta Keyword</label>
                            <asp:TextBox runat="server" ID="txtMetaKeyword" Width="490px" ToolTip="Meta Keyword" />
                        </div>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Meta Description</label>
                            <asp:TextBox runat="server" ID="txtMetaDesc" Width="490px" ToolTip="Meta Description" />
                        </div>
                         <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Document Keyword</label>
                            <asp:TextBox runat="server" ID="txtDocKeyword" Width="490px" ToolTip="Document Keyword" />
                        </div>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Link</label>
                            <asp:TextBox runat="server" ID="txtLink" MaxLength="500" Width="490px" ToolTip="Name" />
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Act Document Type</label>
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList runat="server" ID="ddlActDocType" OnSelectedIndexChanged="ddlActDocType_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" AutoPostBack="true" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="ddlActDocType" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <asp:CompareValidator ErrorMessage="Please select Act Document Type." ControlToValidate="ddlActDocType"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventValidationGroup"
                                Display="None" />
                        </div>

                        <div style="margin-bottom: 7px" id="divDate" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                *</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Month-Year
                            </label>
                            <asp:TextBox runat="server" ID="txtActDocVerstionDate" CssClass='monthYearPicker' Style="height: 30px; width: 150px;" />
                            <asp:RequiredFieldValidator ID="rfvDate" ErrorMessage="Please Select Month and Year"
                                ControlToValidate="txtActDocVerstionDate" runat="server" ValidationGroup="EventValidationGroup"
                                Display="None" />
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                            <label style="width: 300px; display: block; float: left; font-size: 13px; color: #333;">
                            </label> 
                            <asp:CheckBox Style="margin-left: 0px; font-weight: bold;" ID="chkAll" Text="Do you want to send this update to all users?" runat="server" />
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 138px; display: block; float: left; font-size: 13px; color: #333;">
                                Is Covid Update</label>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="chkIsCovid" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 138px; display: block; float: left; font-size: 13px; color: #333;">
                                Upload Document</label>
                            <asp:Label runat="server" ID="lbldocument" CssClass="txtbox" />
                            <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:FileUpload runat="server" ID="fudocument" />
                                    <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 10px">
                                        <asp:Button Text="Download" runat="server" Style="margin-left: 5px;" ID="btndocumentDownload" Visible="false" OnClick="btndocumentDownload_Click" />
                                        <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 10px">
                                            <asp:Button Text="Save as Draft" runat="server" ID="btnSave" OnClick="btnSave_Click" OnClientClick="return Confirm();" CssClass="button"
                                                ValidationGroup="EventValidationGroup" />
                                            <asp:Button Text="Save & Publish" runat="server" ID="btnSavePublish" OnClientClick="return Confirm();" OnClick="btnSavePublish_Click" CssClass="button"
                                                ValidationGroup="EventValidationGroup" />
                                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClick="btnCancel_Click" />
                                        </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                            <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="txtTitle" />
                        <asp:PostBackTrigger ControlID="txtDescription" />
                        <asp:PostBackTrigger ControlID="btnSave" />
                         <asp:PostBackTrigger ControlID="btnSavePublish" />
                    </Triggers>
                </asp:UpdatePanel>
           </div> </div>
            <div style="margin:1px;padding-left:2px;border:1px solid ;width:48%;float:right">
                 <br /> 
                <input id="txtbox" type="text"  style="width:400px;"  onkeyup="myFunction()" /> <a href="javascript:fclean();">Clear</a>
                <br /> <br />
                <asp:Repeater ID="rptComplianceList" runat="server" >
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable1">
                                            <tr>
                                                <td style="width: 52px;">
                                                    <asp:CheckBox ID="complianceSelectAll" Text="Select All" Visible="false" runat="server" onclick="checkAllcompliance(this)" />
                                                </td>
                                                    
                                                <td style="width: 282px;">
                                                    <asp:UpdatePanel ID="upcompliance" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Button runat="server" Visible="false" ID="btnRefreshcompliance" Text="Ok" Style="float: left"  /></td>
                                                <td></td>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnRefreshcompliance" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                            </tr>
					                 <tr class="ui-widget-header">
                                           <th scope="col" style="width: 20px">
                                                
                                            </th>
                                            <th scope="col" style="text-align: left;width:13%">
                                                Compliance Id
                                            </th>
                                            <th scope="col" style="text-align: left;">
                                                Name
                                            </th>
                                            <th scope="col" style="text-align: left;">
                                                Section
                                            </th>
                                        </tr>
                                           
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                             <td style="width: 20px;">
                                                <asp:CheckBox ID="chkCompliance" runat="server" onclick="UncheckComplianceHeader();" /></td>  
                                            <td >  <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label></td> 
                                            <td >
                                                  <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width:300px; ">
                                                 
                                                    <asp:Label ID="lblComplianceDesc" CssClass="cssShortdes" runat="server" Text='<%# Eval("Name") %>' data-attr='<%# Eval("Name") %>' onclick="fopendialog(this);"></asp:Label>
                                                </div>
                                            </td>
                                            <td  >
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width:150px; ">
                                                <asp:Label ID="Label1"  CssClass="csssection" runat="server" Text='<%# Eval("Name") %>'  ></asp:Label></td>
                                        </div>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
   
<%--</div>--%>
  <div id="dialog" title="Basic dialog" style="display:none;width:500px;">
  <p><b>ComplianceId:</b> <span id="cid"></span> </p><br />
  <p><b>Shortdescription:</b> <span id="cname"></span> </p><br />
  <p><b>Section:</b> <span id="csection"></span> </p><br />
      
  <label style="text-decoration:underline;color:blue"> Description </label><br />
   <p1><b>Description:</b> <span id="cdesc"></span> </p1>
</div>
   
    <script type="text/javascript">
        $(document).ready(function () {
            $(".cssShortdes").each(function () {

                var arr = $(this).text().split(" | ");
                $(this).text(arr[1]);
                $(this).attr('title', (arr[1]));

            });
            $(".csssection").each(function () {
                var arr = $(this).text().split(" | ");
                $(this).text(arr[2]);
                $(this).attr('title', (arr[2]));
            });

           $("label").click(function () {
               $("p1").toggle();
           });

        });



        function fclean() {
            $("#txtbox").val('');
            myFunction();
        }
        function fopendialog(obj) {
            debugger;
            var tse = $(obj).attr('data-attr');
            var arr = tse.split(" | ");
            $('#cid').html(arr[0]);
            $('#cname').html(arr[1]);
            $('#csection').html(arr[2]);
            $('#cdesc').html(arr[3]);
            $("#dialog").dialog();
        }

        function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, td1, td2, i, txtValue;
            input = document.getElementById("txtbox");
            filter = input.value.toUpperCase();
            table = document.getElementById("RepeaterTable1");
            tr = table.getElementsByTagName("tr");
           
            // Loop through all table rows, and hide those who don't match the search query
            for (i = 2; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                td1 = tr[i].getElementsByTagName("td")[2];
                td2 = tr[i].getElementsByTagName("td")[3];
                if (td && td1 && td2) {
                    txtValue = td.textContent || td.innerText;
                    txtValue1 = td1.textContent || td1.innerText;
                    txtValue2 = td2.textContent || td2.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1 || txtValue1.toUpperCase().indexOf(filter) > -1 || txtValue2.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }


        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function checkAllcompliance(cb) {
            var ctrls = documnet.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkCompliance") > -1) {
                    cbox.checked = cb.checked;
                }
            }

        }

        function IndustrycheckAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }



        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {
                rowCheckBoxHeader[0].checked = false;
            }
        }

        function UncheckComplianceHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkCompliance']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCompliance']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='complianceSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {
                rowCheckBoxHeader[0].checked = false;
            }
        }
        function UncheckIndustryHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {
                rowCheckBoxHeader[0].checked = false;
            }
        }

        //function UncheckHeaderState() {
        //    var rowCheckBox = $("#RepeaterTable input[id*='chkState']");
        //    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkState']:checked");
        //    var rowCheckBoxHeader = $("#RepeaterTable input[id*='StateSelectAll']");
        //    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        //        rowCheckBoxHeader[0].checked = true;
        //    } else {
        //        rowCheckBoxHeader[0].checked = false;
        //    }
        //}

        //function checkAllCategory(cb) {
        //    var ctrls = document.getElementsByTagName('input');
        //    for (var i = 0; i < ctrls.length; i++) {
        //        var cbox = ctrls[i];
        //        if (cbox.type == "checkbox" && cbox.id.indexOf("chkCategory") > -1) {
        //            cbox.checked = cb.checked;
        //        }
        //    }
        //}

        //function UncheckHeaderCategory() {
        //    var rowCheckBox = $("#RepeaterTable input[id*='chkCategory']");
        //    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCategory']:checked");
        //    var rowCheckBoxHeader = $("#RepeaterTable input[id*='CategorySelectAll']");
        //    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        //        rowCheckBoxHeader[0].checked = true;
        //    } else {

        //        rowCheckBoxHeader[0].checked = false;
        //    }
        //}
    </script>
</asp:Content>

