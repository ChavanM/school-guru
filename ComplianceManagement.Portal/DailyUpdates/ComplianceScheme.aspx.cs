﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates
{
    public partial class ComplianceScheme : System.Web.UI.Page
    {
        protected string CheckStatus = string.Empty;
        public static List<int> ActList = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindScheme();
                BindAct();
                ViewState["Mode"] = "Add";
            }
        }

        private void BindScheme()
        {
            try
            {
                List<Scheme> AllData= ComplianceTypeManagement.ShowAllScheme();
                AllData = AllData.Where(entry => entry.Title.Contains((tbxFilter.Text).Trim())).ToList();
                grdAllScheme.DataSource = AllData;
                grdAllScheme.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindAct()
        {
            try
            {

                rptAct.DataSource = ComplianceTypeManagement.GetAllACTS();
                rptAct.DataBind();
                txtAct.Text = "< Select >";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdAllScheme.PageIndex = 0;
                BindScheme();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAllScheme_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdAllScheme_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdAllScheme_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_Scheme"))
                {
                    long ID = Convert.ToInt64(e.CommandArgument);
                    lblID.InnerText = Convert.ToString(e.CommandArgument);
                    List<SP_SchemeGetByID_Result> SchemeData = ComplianceTypeManagement.GetSchemebyID(ID);
                    if (SchemeData.Count > 0)
                    {

                        foreach (RepeaterItem aItem in rptAct.Items)
                        {
                            CheckBox chkAct = (CheckBox) aItem.FindControl("chkAct");
                            chkAct.Checked = false;
                            CheckBox ActSelectAll = (CheckBox) rptAct.Controls[0].Controls[0].FindControl("ActSelectAll");

                            for (int i = 0; i <= SchemeData.Count - 1; i++)
                            {
                                if (((Label) aItem.FindControl("lblActID")).Text.Trim() == SchemeData[i].ActID.ToString())
                                {
                                    chkAct.Checked = true;
                                }
                                tbxTitle.Text = SchemeData[i].Title;
                            }
                            if ((rptAct.Items.Count) == (SchemeData.Count))
                            {
                                ActSelectAll.Checked = true;

                            }
                            else
                            {
                                ActSelectAll.Checked = false;
                            }
                        }
                        txtAct.Text = "< Select >";
                        ViewState["Mode"] = "Edit";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenModel", "fopenpopup();", true);
                        UpdateSchemes.Update();
                    }
                }
                else if (e.CommandName.Equals("DELETE_Scheme"))
                {
                    long SchemeID = Convert.ToInt64(e.CommandArgument);
                    ComplianceTypeManagement.DeleteSchemeByID(SchemeID);
                    ComplianceTypeManagement.DeActiveExistingSchemeMapping(SchemeID);
                    BindScheme();
                    upIComplianceTypeList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdAllScheme_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAllScheme.PageIndex = e.NewPageIndex;
                BindScheme();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<Int64> ActList = new List<long>();
                bool SaveSuccess = false;
                long SchemeID = 0;
                ActList.Clear();
                bool Validation = false;
                
                foreach (RepeaterItem aItem in rptAct.Items)
                {
                    CheckBox chkAct = (CheckBox) aItem.FindControl("chkAct");
                    if (chkAct.Checked)
                    {
                        ActList.Add(Convert.ToInt64(((Label) aItem.FindControl("lblActID")).Text.Trim()));
                        Validation = true;
                    }
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Mode"])))
                {
                    CheckStatus = Convert.ToString(ViewState["Mode"]);
                }
                
                if (!string.IsNullOrEmpty(tbxTitle.Text))
                {
                    if (ActList.Count() > 0)
                    {
                        Validation = true;
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Act should not be empty";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Title should not be empty";
                }
                if (Validation)
                {
                    Scheme ObjScheme = new Scheme();
                    SchemeMapping ObjSchemeMapping = new SchemeMapping();

                    if (CheckStatus == "Add")
                    {
                        ObjScheme.Title = tbxTitle.Text;
                        ObjScheme.CreatedBy = AuthenticationHelper.UserID;
                        ObjScheme.CreatedOn = DateTime.Now;
                        ObjScheme.IsActive = true;
                        if (ComplianceTypeManagement.CheckComplianceSchemeExist(ObjScheme))
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Scheme with same title already exists.";
                            return;
                        }
                        else
                        {
                            SchemeID = ComplianceTypeManagement.CreateScheme(ObjScheme);

                            if (ActList.Count > 0)
                            {
                                ObjSchemeMapping.CreatedBy = AuthenticationHelper.UserID;
                                ObjSchemeMapping.CreatedOn = DateTime.Now;
                                ObjSchemeMapping.IsActive = true;
                                ObjSchemeMapping.SchemeID = SchemeID;
                                foreach (var item in ActList)
                                {
                                    ObjSchemeMapping.ActID = item;
                                    SaveSuccess = ComplianceTypeManagement.CreateSchemeMapping(ObjSchemeMapping);
                                }
                            }
                            ViewState["Mode"] = "Add";
                            if (SaveSuccess)
                            {
                                tbxTitle.Text = string.Empty;
                                foreach (RepeaterItem aItem in rptAct.Items)
                                {
                                    CheckBox chkIndustry = (CheckBox) aItem.FindControl("chkAct");
                                    chkIndustry.Checked = false;
                                    CheckBox IndustrySelectAll = (CheckBox) rptAct.Controls[0].Controls[0].FindControl("ActSelectAll");
                                    IndustrySelectAll.Checked = false;
                                }
                                BindScheme();
                                upIComplianceTypeList.Update();
                                UpdateSchemes.Update();
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Scheme Saved Successfully.";
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lblID.InnerText))
                        {
                            SchemeID = Convert.ToInt64(lblID.InnerText);
                        }

                        ObjScheme.ID = SchemeID;
                        ObjScheme.Title = tbxTitle.Text;
                        ComplianceTypeManagement.UpdateScheme(ObjScheme);

                        ComplianceTypeManagement.DeActiveExistingSchemeMapping(SchemeID);
                        if (ActList.Count > 0)
                        {
                            ObjSchemeMapping.UpdatedBy = AuthenticationHelper.UserID;
                            ObjSchemeMapping.UpdatedOn = DateTime.Now;
                            ObjSchemeMapping.IsActive = true;
                            ObjSchemeMapping.SchemeID = SchemeID;
                            foreach (var item in ActList)
                            {
                                ObjSchemeMapping.ActID = item;
                                SaveSuccess = ComplianceTypeManagement.UpdateSchemaMapping(ObjSchemeMapping);
                            }
                        }
                        if (SaveSuccess)
                        {
                            tbxTitle.Text = string.Empty;
                            foreach (RepeaterItem aItem in rptAct.Items)
                            {
                                CheckBox chkIndustry = (CheckBox) aItem.FindControl("chkAct");
                                chkIndustry.Checked = false;
                                CheckBox IndustrySelectAll = (CheckBox) rptAct.Controls[0].Controls[0].FindControl("ActSelectAll");
                                IndustrySelectAll.Checked = false;
                            }
                            BindScheme();
                            upIComplianceTypeList.Update();
                            UpdateSchemes.Update();
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Scheme Updated Successfully.";
                        }
                        ViewState["Mode"] = "Add";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnRepeater_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvAct');", txtAct.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvAct\").hide(\"blind\", null, 5, function () { });", true);
        }

        protected void UpdateSchemes_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvAct');", txtAct.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvAct\").hide(\"blind\", null, 5, function () { });", true);
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
        }

        protected void btnAddScheme_Click(object sender, EventArgs e)
        {
            try
            {
                tbxTitle.Text = string.Empty;
                foreach (RepeaterItem aItem in rptAct.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkAct");
                    chkIndustry.Checked = false;
                    CheckBox IndustrySelectAll = (CheckBox)rptAct.Controls[0].Controls[0].FindControl("ActSelectAll");
                    IndustrySelectAll.Checked = false;
                }
                UpdateSchemes.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}