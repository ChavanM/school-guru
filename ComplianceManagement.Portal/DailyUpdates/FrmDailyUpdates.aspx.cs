﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates
{
    public partial class FrmDailyUpdates : System.Web.UI.Page
    {
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindEvents();
                if(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("UPDT"))
                {
                    btnAddEvent.Visible = true;
                }

                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;

                if (objlocal == "Local")
                {
                    cashTimeval = "dailyupdate_CHE" + AuthenticationHelper.UserID;
                }
                else
                {
                    cashTimeval = "dailyupdateCHE" + AuthenticationHelper.UserID;
                }

                if (CacheHelper.Exists(cashTimeval))
                {
                    DateTime ss;
                    CacheHelper.Get<DateTime>(cashTimeval, out ss);
                    TimeSpan span = DateTime.Now - ss;
                    if (span.Hours == 0)
                    {
                        lnkCacheRefresh.Text = "Last updated within the last hour";
                    }
                    else
                    {
                        lnkCacheRefresh.Text = "Last updated " + span.Hours + " hour ago";
                    }
                }
                else
                {
                    lnkCacheRefresh.Text = "Last updated within the last hour";
                }
            }
        }

        private void BindEvents()
        {
            try
            {
                List<Business.Data.DailyUpdate> EventList = null;
               
                EventList = DailyUpdateManagment.GetAllDailyUpdate();

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        EventList = EventList.OrderBy(entry => entry.Title).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Description")
                    {
                        EventList = EventList.OrderBy(entry => entry.Description).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "SubTitle")
                    {
                        EventList = EventList.OrderBy(entry => entry.SubTitle).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        EventList = EventList.OrderByDescending(entry => entry.Title).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Description")
                    {
                        EventList = EventList.OrderByDescending(entry => entry.Description).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "SubTitle")
                    {
                        EventList = EventList.OrderBy(entry => entry.SubTitle).ToList();
                    }
                    direction = SortDirection.Ascending;
                }

                //Added by Amita as on 6FEB2019
                string filter = string.Empty;
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                { 
                    filter = Convert.ToString(tbxFilter.Text).Trim().ToUpper();
                    EventList = EventList.Where(entry => entry.Title.Trim().ToUpper().Contains(filter) || entry.Title.Trim().ToUpper().Contains(filter)).ToList();
                }

                grdEventList.DataSource = EventList;
                grdEventList.DataBind();
                upEventList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int eventID = Convert.ToInt32(e.CommandArgument);
                 //com.VirtuosoITech.ComplianceManagement.Business.Data.DailyUpdate eventData = DailyUpdateManagment.GetByID(eventID);

                if (e.CommandName.Equals("EDIT_ACT"))
                {
                    Session["Mode"] =1;
                   // ViewState["Mode"] = 1;
                    Session["dailyUpdateID"] = eventID;

                    Response.Redirect("~/DailyUpdates/FrmDailyUpdateAdd.aspx", false);

                    //txtTitle.Text = eventData.Title;
                    //txtDescription.Text = eventData.Description;
                    //upEvent.Update();
             
                   // ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divEventDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_ACT"))
                {
                    DailyUpdateManagment.DeleteDailyUpdate(eventID);
                    BindEvents();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEventList.PageIndex = e.NewPageIndex;
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var actList = DailyUpdateManagment.GetAllDailyUpdate();
                if (direction == SortDirection.Ascending)
                {
                    actList = actList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    actList = actList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdEventList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventList.Columns.IndexOf(field);
                    }
                }

                grdEventList.DataSource = actList;
                grdEventList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdEventList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
                    LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
                    Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");
                    Label lblMainFlag = (Label)e.Row.FindControl("lblMainFlag");


                    if (lblIsActive.Text == "False")
                    {
                        lblIsActive.Text = "Draft";
                    }
                    else
                    {
                        lblIsActive.Text = "Publish";
                    }

                    if (lblMainFlag.Text == "1")
                    {
                        lblMainFlag.Text = "Main";
                    }
                    else if (lblMainFlag.Text == "2")
                    {
                        lblMainFlag.Text = "Sub";
                    }
                    else 
                    {
                        lblMainFlag.Text = "";
                    }

                    lbtEdit.Visible = false;
                    lbtDelete.Visible = false;
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("UPDT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void upEvent_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideCompliance", "$(\"#dvCompliance\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddEvent_Click(object sender, EventArgs e)
        {
            try
            {
                Session["Mode"] = 0;
                //ViewState["Mode"] = 0;
                //txtTitle.Text = string.Empty;
                //txtDescription.Text = string.Empty;
                //upEvent.Update();
                Session["dailyUpdateID"] = 0;
                Response.Redirect("~/DailyUpdates/FrmDailyUpdateAdd.aspx", false);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdEventList.PageIndex = 0;
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkCacheRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetAllCompliance_Result> compliance = new List<SP_GetAllCompliance_Result>();
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashTimeval = string.Empty;
                    string cashEventval = string.Empty;
                    string cashEventmasterval = string.Empty;
                    string cashActval = string.Empty;
                    if (objlocal == "Local")
                    {
                        cashTimeval = "dailyupdate_CHE" + AuthenticationHelper.UserID;
                        cashActval = "dailyupdate_CHEAct" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "dailyupdateCHE" + AuthenticationHelper.UserID;
                        cashActval = "dailyupdateAct" + AuthenticationHelper.UserID;
                    }

                    CacheHelper.Remove(cashActval);

                    compliance = (from row in entities.SP_GetAllCompliance()
                                  select row).ToList();

                    CacheHelper.Set<List<SP_GetAllCompliance_Result>>(cashActval, compliance);

                    if (CacheHelper.Exists(cashTimeval))
                    {
                        CacheHelper.Remove(cashTimeval);
                        CacheHelper.Set(cashTimeval, DateTime.Now);
                    }
                    else
                    {
                        CacheHelper.Set(cashTimeval, DateTime.Now);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}