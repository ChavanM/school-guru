﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Data;
using System.Reflection;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.ComponentModel;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class CannedReportPerformer : System.Web.UI.UserControl
    {
        // To Register grid view click eventsddlpageSize_SelectedIndexChanged        
        public event EventHandler pagingClickArgs;
        static int CustomerID;
        static int UserRoleID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    UserRoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            BindCategoriesInternalAll();
                            BindTypesInternalAll();
                            BindActListAllInternal();
                        }
                        else
                        {
                            BindCategoriesAll();
                            BindTypesAll();
                            BindActListAll();
                        }
                        BindComplianceSubTypeListAll();
                    }
                    else
                    {
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            BindTypesInternal();
                            BindCategoriesInternal();
                            BindActListInternal();
                        }
                        else
                        {
                            BindTypes();
                            BindCategories();
                            BindActList();
                        }
                        BindComplianceSubTypeList();

                    }
                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        ddlComplianceType.SelectedValue = "0";
                        ddlComplianceType.Items.Remove(ddlComplianceType.Items.FindByValue("-1"));
                        ddlComplianceType.Items.Remove(ddlComplianceType.Items.FindByValue("1"));
                        ddlComplianceType.Items.Remove(ddlComplianceType.Items.FindByValue("2"));
                        ddlComplianceType.Items.Remove(ddlComplianceType.Items.FindByValue("3"));

                        Session["ComplianceType"] = "Internal";
                        grdComplianceTransactionsInt.Visible = true;
                    }

                    if (AuthenticationHelper.Role=="AUDT")
                    {
                        txtStartDate.Text = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                        txtEndDate.Text = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");
                    }
                    BindEvents();
                    BindStatus();
                    BindLocationFilter();
                    Ffilldata();                
                    GetPageDisplaySummary();
                    if (SelectedPageNo.Text == "")
                    {
                        SelectedPageNo.Text = "1";
                    }
                    if (tvFilterLocationPerformer.SelectedValue != "-1")
                        Session["LocationName"] = tvFilterLocationPerformer.SelectedNode.Text;
                    else
                        Session["LocationName"] = tvFilterLocationPerformer.Nodes[1].Text;
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            pagingClickArgs += new EventHandler(Paging_Click);          
        }

        private void BindComplianceSubTypeListAll()
        {
            try
            {
                ddlComplianceSubType.Items.Clear();

                List<SP_GetComplianceSubTypeIDAll_Result> ComplianceSubTypeList = ActManagement.GetSubTypeByUserIDAll(CustomerID);
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Insert(0, new ListItem("Compliance Subtype", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindComplianceSubTypeList()
        {
            try
            {
                ddlComplianceSubType.Items.Clear();

                List<SP_GetComplianceSubTypeID_Result> ComplianceSubTypeList = ActManagement.GetSubTypeByUserID(AuthenticationHelper.UserID);
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Insert(0, new ListItem("Compliance Subtype", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEventNature();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEvents()
        {
            try
            {
                ddlEvent.DataSource = null;
                ddlEvent.DataBind();
                ddlEvent.DataTextField = "EventName";
                ddlEvent.DataValueField = "eventid";
                var TypeList = ActManagement.GetActiveEvents(10, CustomerID, AuthenticationHelper.UserID, UserRoleID);
                ddlEvent.DataSource = TypeList;
                ddlEvent.DataBind();

                ddlEvent.Items.Insert(0, new ListItem("Event Name", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEventNature()
        {
            try
            {
                ddlEventNature.DataSource = null;
                ddlEventNature.DataBind();
                ddlEventNature.DataTextField = "EventNature";
                ddlEventNature.DataValueField = "EventScheduleOnid";
                var TypeList = ActManagement.GetEventNature(10, CustomerID, AuthenticationHelper.UserID, Convert.ToInt32(ddlEvent.SelectedValue), UserRoleID);
                ddlEventNature.DataSource = TypeList;
                ddlEventNature.DataBind();

                ddlEventNature.Items.Insert(0, new ListItem("Event Nature", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterPerformer", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPerformer", "$(\"#divFilterLocationPerformer\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }   
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                tvFilterLocationPerformer.Nodes.Clear();
                string isstatutoryinternal = "";
                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocationPerformer.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocationPerformer.Nodes.Add(node);
                }

                tvFilterLocationPerformer.CollapseAll();
                divFilterLocationPerformer.Style.Add("display", "none");
                tvFilterLocationPerformer_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActList()
        {
            try
            {
                ddlAct.Items.Clear();
                int complianceTypeID = Convert.ToInt32(ddlType.SelectedValue);
                int complianceCategoryID = Convert.ToInt32(ddlCategory.SelectedValue);

                List<SP_GetActsByUserID_Result> ActList = ActManagement.GetActsByUserID(AuthenticationHelper.UserID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActListInternal()
        {
            try
            {
                ddlAct.Items.Clear();

                List<SP_GetInternalActsByUserID_Result> ActList = ActManagement.GetInternalActsByUserID(AuthenticationHelper.UserID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActListAll()
        {
            try
            {
                ddlAct.Items.Clear();
                var ActList = ActManagement.GetAllAssignedActs(CustomerID,-1);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActListAllInternal()
        {
            try
            {
                ddlAct.Items.Clear();
                var ActList = ActManagement.GetAllAssignedInternalActs(CustomerID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategoriesAll()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                var CatList = ActManagement.GetAllAssignedCategoryAll(CustomerID);
                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategories()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                var CatList = ActManagement.GetAllAssignedCategory(CustomerID, AuthenticationHelper.UserID);
                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategoriesInternal()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                var CatList = ActManagement.GetAllAssignedInternalCategory(CustomerID, AuthenticationHelper.UserID);

                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategoriesInternalAll()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                var CatList = ActManagement.GetAllAssignedInternalCategoryAll(CustomerID);

                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypesAll()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var TypeList = ActManagement.GetAllAssignedTypeAll(CustomerID);
                ddlType.DataSource = TypeList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                var TypeList = ActManagement.GetAllAssignedType(CustomerID, AuthenticationHelper.UserID);
                ddlType.DataSource = TypeList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypesInternal()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var TypleList = ActManagement.GetAllAssignedInternalType(CustomerID, AuthenticationHelper.UserID);
                ddlType.DataSource = TypleList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypesInternalAll()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                var TypleList = ActManagement.GetAllAssignedInternalTypeAll(CustomerID);
                ddlType.DataSource = TypleList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<CannedReportFilterNewStatus>();
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindStatus()
        {
            try
            {              
                foreach (CannedReportFilterNewStatus r in Enum.GetValues(typeof(CannedReportFilterNewStatus)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(CannedReportFilterNewStatus), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static bool IsVisible(int RoleID)
        {
            if (RoleID!=0)
            {
                if (RoleID != 8)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                CannedReportFilterNewStatus status = (CannedReportFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int type = Convert.ToInt32(ddlType.SelectedValue);
                int category = Convert.ToInt32(ddlCategory.SelectedValue);
                int ActID = Convert.ToInt32(ddlAct.SelectedValue);
                int SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                string StringType = "";
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                var UserDetails = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                bool DeptHead = false;
                if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
                {
                    DeptHead = (bool)UserDetails.IsHead;
                }

                int EventID = Convert.ToInt32(ddlEvent.SelectedValue);

                int EventScheduleID;
                if (ddlEventNature.SelectedValue.ToString() != "")
                {
                    EventScheduleID = Convert.ToInt32(ddlEventNature.SelectedValue);
                }
                else
                {
                    EventScheduleID = -1;
                }
                int complianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);

                if (txtStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (txtEndDate.Text == "")
                {
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    if (DeptHead)
                    {
                        var GridData = DepartmentHeadManagement.GetCannedReportDataForPerformer_DeptHead(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);

                        if (GridData.Count > 0)
                        {
                            var maxDate = GridData.Max(r => r.ScheduledOn);
                            var minDate = GridData.Min(r => r.ScheduledOn);
                        }

                        grdComplianceTransactions.DataSource = null;
                        grdComplianceTransactions.DataBind();

                        grdComplianceTransactions.DataSource = GridData;
                        grdComplianceTransactions.DataBind();
                    }
                    else
                    {
                        var GridData = CannedReportManagement.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);
                        grdComplianceTransactions.DataSource = null;
                        grdComplianceTransactions.DataBind();
                        grdComplianceTransactions.DataSource = GridData;
                        grdComplianceTransactions.DataBind();
                    }
                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    var GridData = CannedReportManagement.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID,StringType);
                    grdComplianceTransactions.DataSource = GridData;
                    grdComplianceTransactions.DataBind();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                {
                    var GridData = CannedReportManagement.GetChecklistCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);
                    grdComplianceTransactions.DataSource = GridData;
                    grdComplianceTransactions.DataBind();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based CheckList")
                {
                    var GridData = CannedReportManagement.GetEventChecklistCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);
                    grdComplianceTransactions.DataSource = GridData;
                    grdComplianceTransactions.DataBind();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                   
                        //var GridData = InternalCanned_ReportManagement.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, dtfrom, dtTo, StringType);
                        //grdComplianceTransactionsInt.DataSource = GridData;
                        //grdComplianceTransactionsInt.DataBind();
                    
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal CheckList")
                {
                    var GridData = InternalCanned_ReportManagement.GetChecklistCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category,ActID, dtfrom, dtTo, StringType);
                    grdComplianceTransactionsInt.DataSource = GridData;
                    grdComplianceTransactionsInt.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DataTable GetGrid()
        {
            var UserDetails = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
            bool DeptHead = false;
            if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
            {
                DeptHead = (bool)UserDetails.IsHead;
            }
            dlFilters_SelectedIndexChanged(null, null);

            if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
            {
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    if (DeptHead)
                    {
                        return (grdComplianceTransactions.DataSource as List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>).ToDataTable();
                    }
                    else
                    {
                        return (grdComplianceTransactions.DataSource as List<SP_GetCannedReportCompliancesSummary_Result>).ToDataTable();
                    }
                }
                else
                {
                    return (grdComplianceTransactions.DataSource as List<SP_GetCannedReportCompliancesSummary_Result>).ToDataTable();
                }
            }
            else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList" || ddlComplianceType.SelectedItem.Text == "Event Based CheckList")
            {
                return (grdComplianceTransactions.DataSource as List<SP_GetCheckListCannedReportCompliancesSummary_Result>).ToDataTable();
            }
            else if (ddlComplianceType.SelectedItem.Text == "Internal")
            {
                return (grdComplianceTransactionsInt.DataSource as List<InternalComplianceInstanceTransactionView>).ToDataTable();
            }
            else //Internal CheckList
            {
                return (grdComplianceTransactionsInt.DataSource as List<InternalComplianceInstanceCheckListTransactionView>).ToDataTable();
            }
        }

     
        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CannedReportFilterNewStatus>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                //if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
                //{
                result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                //}
                //else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                //{
                //    result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                //}
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
                return "";
            }
        }

        protected string GetReviewerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 4);
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal CheckList") 
                {
                    result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 4);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
                return "";
            }
        }

        protected string GetRisk(byte RiskID)
        {
            try
            {
                string result = "";

                if (RiskID == 0)
                    result = "High";
                else if (RiskID == 1)
                    result = "Medium";
                else if (RiskID == 2)
                    result = "Low";
                
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
                return "";
            }

        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(e.CommandArgument.ToString()))
                {
                    int ScheduledOnID = Convert.ToInt32(e.CommandArgument);
                    if (e.CommandName.Equals("View_History"))
                    {
                        BindTransactions(ScheduledOnID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DateTime CurrentDate = DateTime.Today.Date;

                int filtervalue = Convert.ToInt32(dlFilters.SelectedIndex);

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    String GridStatus = e.Row.Cells[6].Text;

                    LinkButton lnkbtnHistory = (LinkButton)e.Row.FindControl("lnkbtnHistory");
                    Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");

                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");                    

                    //BindTransactions(Convert.ToInt32(lblScheduledOnID.Text));
                    if (filtervalue == 4)
                    {
                        lnkbtnHistory.Visible = true;
                        //BindTransactions(Convert.ToInt32(lblScheduledOnID.Text));
                    }
                    else
                    {
                        lnkbtnHistory.Visible = false;
                    }

                    if (lblScheduledOn != null)
                    {
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            e.Row.Cells[6].Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            e.Row.Cells[6].Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        protected void grdComplianceTransactionsInt_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DateTime CurrentDate = DateTime.Today.Date;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    String GridStatus = e.Row.Cells[6].Text;
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblInternalScheduledOn");

                    if (lblScheduledOn != null)
                    {
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            e.Row.Cells[6].Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            e.Row.Cells[6].Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }
        private void BindTransactions(int ScheduledOnID)
        {
            try
            {                
                if (ScheduledOnID != 0)
                {
                    grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                    grdTransactionHistory.DataBind();                    
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divActDialog111\").dialog('open')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);  
            }
        }            
        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {           
        }
        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {           
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvFilterLocationPerformer.SelectedValue != "-1")
                    Session["LocationName"] = tvFilterLocationPerformer.SelectedNode.Text;
                else
                    Session["LocationName"] = tvFilterLocationPerformer.Nodes[1].Text;

                SelectedPageNo.Text = "1";

                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList") 
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                else
                    grdComplianceTransactionsInt.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                Ffilldata();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void Ffilldata()
        {
            try
            {
                var UserDetails = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                bool DeptHead = false;
                if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
                {
                    DeptHead = (bool)UserDetails.IsHead;
                }
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                CannedReportFilterNewStatus status = (CannedReportFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int type = Convert.ToInt32(ddlType.SelectedValue);
                int category = Convert.ToInt32(ddlCategory.SelectedValue);
                int ActID = Convert.ToInt32(ddlAct.SelectedValue);
                int SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                string StringType = "";
               

                int EventID = Convert.ToInt32(ddlEvent.SelectedValue);

                int EventScheduleID;
                if (ddlEventNature.SelectedValue.ToString() != "")
                {
                    EventScheduleID = Convert.ToInt32(ddlEventNature.SelectedValue);
                }
                else
                {
                    EventScheduleID = -1;
                }
                int complianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);

                if (txtStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (txtEndDate.Text == "")
                {
                    //dtTo = DateTime.Today.Date;
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                StringType = txtSearchType.Text;

                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    if (DeptHead)
                    {
                        var GridData = DepartmentHeadManagement.GetCannedReportDataForPerformer_DeptHead(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);

                        if (GridData.Count > 0)
                        {
                            var maxDate = GridData.Max(r => r.ScheduledOn);
                            var minDate = GridData.Min(r => r.ScheduledOn);
                        }

                        grdComplianceTransactions.DataSource = null;
                        grdComplianceTransactions.DataBind();

                        grdComplianceTransactions.DataSource = GridData;
                        Session["TotalRowsPerformer"] = GridData.Count;
                        grdComplianceTransactions.DataBind();
                    }
                    else
                    {
                        var GridData = CannedReportManagement.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);

                        if (GridData.Count > 0)
                        {
                            var maxDate = GridData.Max(r => r.ScheduledOn);
                            var minDate = GridData.Min(r => r.ScheduledOn);
                        }

                        grdComplianceTransactions.DataSource = null;
                        grdComplianceTransactions.DataBind();

                        grdComplianceTransactions.DataSource = GridData;
                        Session["TotalRowsPerformer"] = GridData.Count;
                        grdComplianceTransactions.DataBind();
                    }
                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    var GridData = CannedReportManagement.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID,StringType);

                    if (GridData.Count > 0)
                    {
                        var maxDate = GridData.Max(r => r.ScheduledOn);
                        var minDate = GridData.Min(r => r.ScheduledOn);
                    }

                    grdComplianceTransactions.DataSource = null;
                    grdComplianceTransactions.DataBind();

                    grdComplianceTransactions.DataSource = GridData;
                    Session["TotalRowsPerformer"] = GridData.Count;
                    grdComplianceTransactions.DataBind();

                }
                else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                {

                    var GridData = CannedReportManagement.GetChecklistCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);

                    if (GridData.Count > 0)
                    {
                        var maxDate = GridData.Max(r => r.ScheduledOn);
                        var minDate = GridData.Min(r => r.ScheduledOn);
                    }

                    grdComplianceTransactions.DataSource = null;
                    grdComplianceTransactions.DataBind();

                    grdComplianceTransactions.DataSource = GridData;
                    Session["TotalRowsPerformer"] = GridData.Count;
                    grdComplianceTransactions.DataBind();

                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based CheckList")
                {
                    var GridData = CannedReportManagement.GetEventChecklistCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);

                    if (GridData.Count > 0)
                    {
                        var maxDate = GridData.Max(r => r.ScheduledOn);
                        var minDate = GridData.Min(r => r.ScheduledOn);
                    }

                    grdComplianceTransactions.DataSource = null;
                    grdComplianceTransactions.DataBind();

                    grdComplianceTransactions.DataSource = GridData;
                    Session["TotalRowsPerformer"] = GridData.Count;
                    grdComplianceTransactions.DataBind();

                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                 
                        //var GridData = InternalCanned_ReportManagement.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, dtfrom, dtTo, StringType);

                        //grdComplianceTransactionsInt.DataSource = null;
                        //grdComplianceTransactionsInt.DataBind();

                        //grdComplianceTransactionsInt.DataSource = GridData;
                        //Session["TotalRowsPerformer"] = GridData.Count;
                        //grdComplianceTransactionsInt.DataBind();
                    
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal CheckList")
                {
                    var GridData = InternalCanned_ReportManagement.GetChecklistCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, StringType);

                    grdComplianceTransactionsInt.DataSource = null;
                    grdComplianceTransactionsInt.DataBind();

                    grdComplianceTransactionsInt.DataSource = GridData;
                    Session["TotalRowsPerformer"] = GridData.Count;
                    grdComplianceTransactionsInt.DataBind();
                }
                GetPageDisplaySummary();

                upCannedReportPerformer.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        static public void EnumToListBox(Type EnumType, ListControl TheListBox)
        {
            Array Values = System.Enum.GetValues(EnumType);

            foreach (long Value in Values)
            {
                string Display = Enum.GetName(EnumType, Value);
                ListItem Item = new ListItem(Display, Value.ToString());
                TheListBox.Items.Add(Item);
            }
        }

        protected void ddlpageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                 
                if (!IsValid()) { return; };
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList" || ddlComplianceType.SelectedItem.Text == "Event Based CheckList")
                {
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                    grdComplianceTransactionsInt.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactionsInt.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                }
                //Reload the Grid
                Ffilldata();  
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }         
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }             
                if (!(StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRowsPerformer"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRowsPerformer"]))
                    EndRecord = Convert.ToInt32(Session["TotalRowsPerformer"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                pagingClickArgs(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Paging_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList" || ddlComplianceType.SelectedItem.Text == "Event Based CheckList")
                {
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                    grdComplianceTransactionsInt.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactionsInt.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                Ffilldata();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlpageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRowsPerformer"]))
                    EndRecord = Convert.ToInt32(Session["TotalRowsPerformer"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                pagingClickArgs(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRowsPerformer"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRowsPerformer"].ToString())))
                            lblEndRecord.Text = ddlpageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRowsPerformer"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
               
                TotalRows.Value = Session["TotalRowsPerformer"].ToString();


                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlpageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlpageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                CannedReportFilterNewStatus status = (CannedReportFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                ddlEventNature.SelectedValue = "-1";
                ddlEvent.SelectedValue = "-1";
                divEvent.Visible = false;
                if (ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    divEvent.Visible = true;
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    DivComplianceSubTypeList.Attributes.Remove("disabled");

                    grdComplianceTransactionsInt.Visible = false;
                    grdComplianceTransactions.Visible = true;

                    SelectedPageNo.Text = "1";

                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    Ffilldata();
                    GetPageDisplaySummary();

                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        BindTypesAll();
                        BindCategoriesAll();
                    }
                    else
                    {
                        BindTypes();
                        BindCategories();
                    }

                    Session["ComplianceType"] = "EventBased";
                }
                else if(ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    DivComplianceSubTypeList.Attributes.Remove("disabled");

                    grdComplianceTransactionsInt.Visible = false;
                    grdComplianceTransactions.Visible = true;

                    SelectedPageNo.Text = "1";

                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    Ffilldata();
                    GetPageDisplaySummary();

                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        BindTypesAll();
                        BindCategoriesAll();
                    }
                    else
                    {
                        BindTypes();
                        BindCategories();
                    }

                    Session["ComplianceType"] = "Statutory";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                {
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    DivComplianceSubTypeList.Attributes.Add("disabled", "true");

                    grdComplianceTransactionsInt.Visible = false;
                    grdComplianceTransactions.Visible = true;

                    SelectedPageNo.Text = "1";

                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    Ffilldata();
                    GetPageDisplaySummary();

                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        BindTypesAll();
                        BindCategoriesAll();
                    }
                    else
                    {
                        BindTypes();
                        BindCategories();
                    }

                    Session["ComplianceType"] = "Statutory CheckList";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based CheckList")
                {
                    divEvent.Visible = true;

                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    DivComplianceSubTypeList.Attributes.Add("disabled", "true");

                    grdComplianceTransactionsInt.Visible = false;
                    grdComplianceTransactions.Visible = true;

                    SelectedPageNo.Text = "1";

                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    Ffilldata();
                    GetPageDisplaySummary();

                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        BindTypesAll();
                        BindCategoriesAll();
                    }
                    else
                    {
                        BindTypes();
                        BindCategories();
                    }

                    Session["ComplianceType"] = "EventBased CheckList";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    ddlAct.ClearSelection();
                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        PanelAct.Enabled = true;
                    }
                    else
                    {
                        PanelAct.Enabled = false;
                    }
                    DivAct.Attributes.Add("disabled", "true");
                    Panelsubtype.Enabled = false;
                    DivComplianceSubTypeList.Attributes.Add("disabled", "true");

                    grdComplianceTransactions.Visible = false;
                    grdComplianceTransactionsInt.Visible = true;

                    SelectedPageNo.Text = "1";

                    grdComplianceTransactionsInt.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactionsInt.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        BindCategoriesInternalAll();
                        BindTypesInternalAll();
                    }
                    else
                    {
                        BindCategoriesInternal();
                        BindTypesInternal();
                    }

                    Ffilldata();

                    Session["ComplianceType"] = "Internal";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal CheckList")
                {
                    ddlAct.ClearSelection();
                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        PanelAct.Enabled = true;
                    }
                    else
                    {
                        PanelAct.Enabled = false;
                    }
                    DivAct.Attributes.Add("disabled", "true");
                    Panelsubtype.Enabled = false;
                    DivComplianceSubTypeList.Attributes.Add("disabled", "true");

                    grdComplianceTransactions.Visible = false;
                    grdComplianceTransactionsInt.Visible = true;

                    SelectedPageNo.Text = "1";

                    grdComplianceTransactionsInt.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactionsInt.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        BindCategoriesInternalAll();
                        BindTypesInternalAll();
                    }
                    else
                    {
                        BindCategoriesInternal();
                        BindTypesInternal();
                    }

                    Ffilldata();

                    Session["ComplianceType"] = "Internal CheckList";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ddlComplianceSubType.SelectedValue = "-1";
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtStartDate.Text = "";
                txtEndDate.Text = "";
                txtSearchType.Text = string.Empty;
                divAdvSearch.Visible = false;

                SelectedPageNo.Text = "1";

                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                else
                    grdComplianceTransactionsInt.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                Ffilldata();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                else
                    grdComplianceTransactionsInt.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;

                int type = 0;
                int Category = 0;
                int Act = 0;
                int subCategory = 0;

                if (txtStartDate.Text != "" && txtEndDate.Text == "" || txtStartDate.Text == "" && txtEndDate.Text != "")
                {
                    if (txtStartDate.Text == "")
                        txtStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtEndDate.Text == "")
                        txtEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }

                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }

                if (ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }


                if (ddlComplianceSubType.SelectedValue != "-1")
                {
                    subCategory = ddlComplianceSubType.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (subCategory >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                    }
                    else
                    {
                        if (subCategory >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                    }
                }

                if (txtStartDate.Text != "" && txtEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtEndDate.Text;
                    }
                }

                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>Type to Search: </b>" + txtSearchType.Text;
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    Ffilldata();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Session["Status"] = ddlStatus.SelectedItem.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void tvFilterLocationPerformer_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationPerformer.Text = tvFilterLocationPerformer.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }          
        }        
    }
}