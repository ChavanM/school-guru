﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternalCanned_ReportReviewer.ascx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.InternalCanned_ReportReviewer" %>
<asp:UpdatePanel ID="upCannedReportReviewer" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div style="float: right; margin-right: 10px; margin-top: -35px;">
            <asp:DataList runat="server" ID="dlFilters" RepeatDirection="Horizontal" OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"
                DataKeyField="ID">
                <SeparatorTemplate>
                    <span style="margin: 0 5px 0 5px">|</span>
                </SeparatorTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select" Style="text-decoration: none;
                        color: Black">  
                                    <%# DataBinder.Eval(Container.DataItem, "Name") %>
                    </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Font-Names="Tahoma" Font-Size="13px" VerticalAlign="Middle" />
                <SelectedItemStyle Font-Names="Tahoma" Font-Size="13px" Font-Bold="True" VerticalAlign="Middle"
                    Font-Underline="true" />
            </asp:DataList>
        </div>
        <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false"
            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdComplianceTransactions_RowCreated"
            BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnSorting="grdComplianceTransactions_Sorting"
            Width="100%" Font-Size="12px" DataKeyNames="InternalComplianceInstanceID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Location" ItemStyle-Width="20%" SortExpression="Branch">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                            <asp:Label  runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description" ItemStyle-Width="30%" SortExpression="Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                            <asp:Label  runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Role" HeaderText="Role" ItemStyle-Width="10%" SortExpression="Role" />
            <%--    <asp:BoundField DataField="User" HeaderText="User" ItemStyle-Width="10%" />--%>
                <asp:TemplateField HeaderText="Scheduled On" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                    <ItemTemplate>
                        <%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ForMonth" HeaderText="For Month" ItemStyle-Height="20px" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="20px" SortExpression="Status"
                    ItemStyle-Width="15%" />
                 <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="Status"
                    ItemStyle-Width="15%" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
            <pagersettings position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
            <EmptyDataTemplate>
                No Records Found.
            </EmptyDataTemplate>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
