﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Configuration;
using Ionic.Zip;
using System.Net;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class InternalComplianceStatusperformer : System.Web.UI.Page
    {

        static string sampleFormPath4 = "";
        public static string subTaskDocViewPath = "";
        public static List<long> Tasklist = new List<long>();
        public static List<SP_TaskInstanceTransactionInternalView_Result> MastersTasklistinternal = new List<SP_TaskInstanceTransactionInternalView_Result>();
        public static List<TaskDocumentsView> MastersTaskDocumentslistinternal = new List<TaskDocumentsView>();
        protected static bool IsDocumentCompulsary = false;
        protected static bool IsRemarkCompulsary1 = false;
        protected string UploadDocumentLink;
        bool IsNotCompiled = false;
        bool IsCompliedButDocumentPending = false;
        protected static int CustId;
        protected static bool IsComplianceupdatedCustomer = false;
        public string Status;
        public string ComplainceType;
        public static bool LockUnlockCustomerEnable;
        public string ISmail;
        public static bool IsRemark;
        protected void Page_Load(object sender, EventArgs e)
        {
            LockUnlockCustomerEnable = false;
            IsRemark = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "RemarkList");
            string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
            string[] listCust = new string[] { };
            if (!string.IsNullOrEmpty(listCustomer))
                listCust = listCustomer.Split(',');

            if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                UploadDocumentLink = "True";
            else
                UploadDocumentLink = "False";
            Status = Convert.ToString(Request.QueryString["Status"]);
            ComplainceType = Convert.ToString(Request.QueryString["ComplainceType"]);

            ISmail = "0";
            try
            {
                ISmail = Convert.ToString(Request.QueryString["ISM"]);
                if (!string.IsNullOrEmpty(ISmail) && ISmail == "1")
                {
                    lnkgotoportal.Visible = true;
                }
                else
                {
                    lnkgotoportal.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "ISmail", MethodBase.GetCurrentMethod().Name);
                ISmail = "0";
            }


            if (!IsPostBack)
            {
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                IsComplianceupdatedCustomer = false;
                var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (details != null)
                    IsComplianceupdatedCustomer = true;

                int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                int instanceId = Convert.ToInt32(Request.QueryString["InId"].ToString());
                OpenTransactionPage(scheduleOnId, instanceId);
                Labellockingmsg.Visible = false;
                string listLockingCustomer = ConfigurationManager.AppSettings["LockingDaysCustomerlist"];
                string[] listLockCust = new string[] { };
                if (!string.IsNullOrEmpty(listLockingCustomer))
                    listLockCust = listLockingCustomer.Split(',');
                if (listLockCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                {
                    if (Business.ComplianceManagement.GetCurrentStatusByScheduleONID(scheduleOnId, "I", Convert.ToInt32(AuthenticationHelper.CustomerID)))
                    {
                        Labellockingmsg.Visible = true;
                        btnSave2.Enabled = false;
                    }
                    else
                    {
                        Labellockingmsg.Visible = false;
                        btnSave2.Enabled = true;
                    }
                }
                LockUnlockCustomerEnable = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "LockUnlock");
                if (LockUnlockCustomerEnable)
                {
                    if (Status == "Overdue")
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var query = (from row in entities.SP_DetailLockingDayReviewer(ComplainceType, CustId)
                                         where row.ComplianceScheduleOnID == scheduleOnId
                                         select row).FirstOrDefault();

                            var UpdateComplianceUnlockStatus = (from row in entities.ComplianceUnlockStatus
                                                                where row.ComplianceScheduleOnID == scheduleOnId
                                                                select row).ToList();

                            if (query != null)
                            {
                                if (query.Unlock == false)
                                {
                                    Labellockingmsg.Visible = true;
                                    btnSave2.Enabled = false;
                                }

                                else if (query.Unlock == true && DateTime.Today.Date > query.Dated)
                                {
                                    Labellockingmsg.Visible = true;
                                    btnSave2.Enabled = false;
                             
                                    if (UpdateComplianceUnlockStatus.Count > 0)
                                    {
                                        foreach (var item in UpdateComplianceUnlockStatus)
                                        {
                                            item.Unlock = false;
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    Labellockingmsg.Visible = false;
                                    btnSave2.Enabled = true;
                                }
                            }
                            else
                            {
                                Labellockingmsg.Visible = true;
                                btnSave2.Enabled = false;
                            }
                        }
                    }
                }
                var customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "TaxReportFields");

                if (customizedid == AuthenticationHelper.CustomerID)
                {
                    lbldate.InnerText = "Date of Compliance";
                }
                else
                {
                    lbldate.InnerText = "Date";
                }
            }
        }

        protected void lnkgotoportal_Click(object sender, EventArgs e)
        {
            try
            {
                ProductMappingStructure _obj = new ProductMappingStructure();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _obj.ReAuthenticate_UserNew();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            var ICSOID = Convert.ToInt64(ScheduledOnID);
            var RCT = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID((int)ICSOID);
            if (RCT.ComplianceStatusID == 2 || RCT.ComplianceStatusID == 3 || RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5 || RCT.ComplianceStatusID == 18)
            {
                btnSave2.Visible = false;
                cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "This compliance already performed.";
                cvDuplicateEntry.ErrorMessage = "This compliance is already submitted for review or already closed.";
            }
            else
            {
                btnSave2.Visible = true;
                try
                {
                    lblRisk2.Text = string.Empty;
                    var complianceInfo = Business.InternalComplianceManagement.GetInternalComplianceByInstanceID(ScheduledOnID);
                    var complianceForm = Business.InternalComplianceManagement.GetInternalComplianceFormByID(complianceInfo.ID);
                    //var RecentComplianceTransaction = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID(ScheduledOnID);
                    lblComplianceID2.Text = Convert.ToString(complianceInfo.ID);
                    if (complianceInfo.IShortDescription != null)
                        lblComplianceDiscription2.Text = complianceInfo.IShortDescription.Replace("\n", "<br>");

                    if (complianceInfo.IDetailedDescription != null)
                        lbldetaileddesc.Text = complianceInfo.IDetailedDescription.Replace("\n", "<br>");

                    lblShortForm.Text = complianceInfo.IShortForm ;


                    var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (details != null)
                    {
                        int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                        int instanceId = Convert.ToInt32(Request.QueryString["InId"].ToString());

                        bool checkisrequired = InternalComplianceManagement.GetLogDetails(Convert.ToInt32(complianceInfo.ID), instanceId, scheduleOnId);
                        if (checkisrequired)
                        {
                            HDNIsDocumentCompulsary.Value = "T";
                            IsDocumentCompulsary = true;                            
                        }
                        else
                        {
                            HDNIsDocumentCompulsary.Value = "F";
                            IsDocumentCompulsary = false;
                        }
                    }
                    else
                    {
                        if (complianceInfo.IsDocumentRequired != true)
                        {
                            HDNIsDocumentCompulsary.Value = "F";
                            IsDocumentCompulsary = false;
                        }
                        else
                        {
                            HDNIsDocumentCompulsary.Value = "T";
                            IsDocumentCompulsary = true;
                        }
                    }

                    lblFrequency2.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency != null ? (int)complianceInfo.IFrequency : -1));
                    grdInternalDocument.DataSource = null;
                    grdInternalDocument.DataBind();
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int customerbranchID = -1;
                    var AllComData = Business.ComplianceManagement.GetPeriodLocationPerformerInternal(ScheduledOnID, complianceInstanceID);
                    var complinaceinstance = Business.ComplianceManagement.GetBranchLocationInternal(complianceInstanceID);
                    if (AllComData != null)
                    {
                        customerbranchID = AllComData.CustomerBranchID;
                        lblLocation.Text = AllComData.Branch;
                        lblDueDate.Text = Convert.ToDateTime(AllComData.InternalScheduledOn).ToString("dd-MMM-yyyy");
                        hiddenDueDateInternal.Value = Convert.ToDateTime(AllComData.InternalScheduledOn).ToString("dd-MM-yyyy");
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            lblPeriod1.Text = Business.ComplianceManagement.PeriodReplace(AllComData.ForMonth);
                            lblPeriod.Text = AllComData.ForMonth;
                        }
                        else
                        {
                            lblPeriod1.Text = AllComData.ForMonth;
                            lblPeriod.Text = AllComData.ForMonth;
                        }
                    }

                    BindTempDocumentData(ScheduledOnID);
                    var IsAfter = TaskManagment.IsTaskAfterInternal(ScheduledOnID, 3, lblPeriod.Text, customerbranchID, customerID);
                    lbltaskinternal.Text = "";
                    var showHideButton = false;
                    if (IsAfter == false)
                    {
                        showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                        if (showHideButton == false)
                        {
                            lbltaskinternal.Text = "Compliance releted task is not yet completed.";
                        }
                        btnSave2.Enabled = showHideButton;
                    }
                    else
                    {
                        showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                        lbltaskinternal.Text = "";
                        btnSave2.Enabled = true;
                    }

                    ViewState["IsAfter"] = IsAfter;
                    ViewState["Period"] = lblPeriod.Text;
                    ViewState["CustomerBrachID"] = customerbranchID;
                    string risk = Business.InternalComplianceManagement.GetRiskType(complianceInfo);
                    lblRiskType2.Text = Business.InternalComplianceManagement.GetRisk(complianceInfo);
                    lblRisk2.Text = risk;

                    if (risk == "HIGH")
                    {
                        divRiskType2.Attributes["style"] = "background-color:red;";
                    }
                    else if (risk == "MEDIUM")
                    {
                        divRiskType2.Attributes["style"] = "background-color:yellow;";
                    }
                    else if (risk == "LOW")
                    {
                        divRiskType2.Attributes["style"] = "background-color:green;";
                    }
                    else if (risk == "CRITICAL")
                    {
                        divRiskType2.Attributes["style"] = "background-color:#CC0900;";
                    }
                    var AuditChecklistName = Business.ComplianceManagement.GetAuditChecklistNameInternal(complianceInfo.ID, AuthenticationHelper.CustomerID);

                    if (!string.IsNullOrEmpty(AuditChecklistName))
                    {
                        lblAuditChecklist3.Text = AuditChecklistName;
                        trAuditChecklist3.Visible = true;
                    }
                    else
                    {
                        trAuditChecklist3.Visible = false;
                    }

                    if (RCT.ComplianceStatusID == 10)
                    {
                        //rfvFile.Visible = false;
                        BindDocument(ScheduledOnID);
                    }
                    else
                    {
                        //rfvFile.Visible = true;
                        divDeleteDocument.Visible = false;
                        rptComplianceDocumnets.DataSource = null;
                        rptComplianceDocumnets.DataBind();
                        rptWorkingFiles.DataSource = null;
                        rptWorkingFiles.DataBind();
                    }

                    // commented by narendra on 8 June 2018  if (complianceInfo.IUploadDocument == true && complianceForm != null)
                    if (complianceForm != null)
                    {
                        lnkViewSampleForm4.Visible = true;
                        lblSlash1.Visible = true;
                        lbDownloadSample4.Text = "<u style=color:blue>Click here</u> to download sample form.";
                        lbDownloadSample4.CommandArgument = complianceForm.IComplianceID.ToString();
                        lbDownloadSample4.Text = "Download";
                        lblNote.Visible = true;

                        sampleFormPath4 = complianceForm.Name;
                        lnkViewSampleForm4.Visible = true;
                        lblSlash1.Visible = true;

                        #region save samle form
                        string Filename = complianceForm.Name;
                        string filePath = sampleFormPath4;
                        if (filePath != null)
                        {
                            try
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                //string[] path = filePath.Split('/');
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }
                                //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                string FileName = DateFolder + "/" + User + "" + extension;
                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                bw.Write(complianceForm.FileData);
                                bw.Close();
                                lblpathsample4.Text = FileName;
                            }
                            catch (Exception ex)
                            {

                            }

                            var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceForm(Convert.ToInt32(lbDownloadSample4.CommandArgument));

                            rptComplianceSampleView.DataSource = complianceform;
                            rptComplianceSampleView.DataBind();
                        }
                        #endregion

                    }
                    else
                    {
                        lblNote.Visible = false;
                        lblSlash1.Visible = false;
                        lnkViewSampleForm4.Visible = false;
                        sampleFormPath4 = "";
                    }

                    btnSave2.Attributes.Remove("disabled");
                    if (RCT.ComplianceStatusID == 1 || RCT.ComplianceStatusID == 6 || RCT.ComplianceStatusID == 10 || RCT.ComplianceStatusID == 19)
                    {
                        divUploadDocument.Visible = true;
                        divWorkingfiles.Visible = true;
                    }
                    else if (RCT.ComplianceStatusID != 1)
                    {
                        divUploadDocument.Visible = false;
                        divWorkingfiles.Visible = false;
                        if ((complianceInfo.IUploadDocument ?? false))
                        {
                            btnSave2.Attributes.Add("disabled", "disabled");
                        }
                    }

                    BindStatusList(Convert.ToInt32(RCT.ComplianceStatusID));
                    BindTransactions(ScheduledOnID);
                    tbxRemarks2.Text = string.Empty;
                    hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                    hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();
                    upComplianceDetails.Update();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        public void BindDocument(int ScheduledOnID)
        {
            try
            {
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileDataInternal(ScheduledOnID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = ComplianceDocument.Where(entry => entry.FileType == 2).ToList();
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory2_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.InternalComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["InternalScheduledOnID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory2.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory2.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory2.DataSource = complianceTransactionHistory;
                grdTransactionHistory2.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory2_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory2.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["InternalScheduledOnID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                grdTransactionHistory2.DataSource = Business.InternalComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory2.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {
                ddlStatus2.DataSource = null;
                ddlStatus2.DataBind();
                ddlStatus2.ClearSelection();

                ddlStatus2.DataTextField = "Name";
                ddlStatus2.DataValueField = "ID";
                
                var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (details != null)
                {
                    int ComplianceinstanceID = Convert.ToInt32(Request.QueryString["InId"].ToString());
                    int CompID = Convert.ToInt32(Business.InternalComplianceManagement.GetComplianceID(ComplianceinstanceID));
                    var Compliancedetails = Business.InternalComplianceManagement.GetDetailsCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(CompID));
                    if (Compliancedetails != null)
                    {
                        int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                        
                        var Compliancesceduledetails = Business.InternalComplianceManagement.GetDetailsComplianceScheduled(Convert.ToInt32(ComplianceinstanceID), Convert.ToInt32(scheduleOnId));

                        if (Compliancesceduledetails != null)
                        {
                            if (Compliancesceduledetails.RoleID == 3)
                            {
                                var statusList = ComplianceStatusManagement.GetStatusListByCustomer(false, "3");
                                ddlStatus2.DataSource = statusList;
                                ddlStatus2.DataBind();
                                ddlStatus2.Items.Insert(0, new ListItem("< Select >", "-1"));
                            }
                            else
                            {
                                var statusList = ComplianceStatusManagement.GetStatusListByCustomer(true, "3");
                                ddlStatus2.DataSource = statusList;
                                ddlStatus2.DataBind();
                                ddlStatus2.Items.Insert(0, new ListItem("< Select >", "-1"));
                            }
                        }
                        else
                        {
                            var statusList = ComplianceStatusManagement.GetStatusListByCustomer(Compliancedetails.IsApproverCompulsory, "3");
                            ddlStatus2.DataSource = statusList;
                            ddlStatus2.DataBind();
                            ddlStatus2.Items.Insert(0, new ListItem("< Select >", "-1"));
                        }
                    }
                }
                else
                {
                    // STT Change- Add Status
                    long CustomerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                    string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {
                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == CustomerID.ToString())
                            {
                                IsNotCompiled = true;
                                break;
                            }
                        }
                    }

                    // Zomato Change- Add Status -CompliedButDocumentPending
                    string customerList = ConfigurationManager.AppSettings["CompliedButDocumentPendingCustID"].ToString();
                    List<string> CompliedButDocumentPendingCustIDCustomerList = customerList.Split(',').ToList();
                    if (CompliedButDocumentPendingCustIDCustomerList.Count > 0)
                    {
                        if (CustomerID == 0)
                        {
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                        foreach (string PList in CompliedButDocumentPendingCustIDCustomerList)
                        {
                            if (PList == CustomerID.ToString())
                            {
                                IsCompliedButDocumentPending = true;
                                break;
                            }
                        }
                    }

                    if (IsNotCompiled == true)
                    {
                        var statusList = ComplianceStatusManagement.GetNotCompliedStatusList();
                        List<ComplianceStatu> allowedStatusList = null;
                        List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();
                        ddlStatus2.DataSource = allowedStatusList;
                        ddlStatus2.DataBind();
                        ddlStatus2.Items.Insert(0, new ListItem("< Select >", "-1"));
                    }
                    else if (IsCompliedButDocumentPending == true)
                    {
                        var statusList = ComplianceStatusManagement.GetCompliedButDocumentPendingList();
                        List<ComplianceStatu> allowedStatusList = null;
                        List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();
                        ddlStatus2.DataSource = allowedStatusList;
                        ddlStatus2.DataBind();
                        ddlStatus2.Items.Insert(0, new ListItem("< Select >", "-1"));
                    }
                    else
                    {
                        var statusList = ComplianceStatusManagement.GetStatusList();
                        List<ComplianceStatu> allowedStatusList = null;
                        List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();
                        ddlStatus2.DataSource = allowedStatusList;
                        ddlStatus2.DataBind();
                        ddlStatus2.Items.Insert(0, new ListItem("< Select >", "-1"));

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int RemarkTypeID = -1;
            var ICSOID = Convert.ToInt64(hdnComplianceScheduledOnId.Value);
            var RCT = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID((int)ICSOID);
            if (RCT.ComplianceStatusID == 2 || RCT.ComplianceStatusID == 3 || RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5 || RCT.ComplianceStatusID == 18)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This compliance is already submitted for review or already closed.";
            }
            else
            {
                #region Save Code
                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                try
                {
                    //if (IsRemark == true)
                    //{
                    //    if (ddlRemark.SelectedValue != null || ddlRemark.SelectedValue != "")
                    //    {
                    //        RemarkTypeID = Convert.ToInt32(ddlRemark.SelectedValue);
                    //    }
                    //}
                    var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (details != null)
                    {
                        var complianceInfo = Business.InternalComplianceManagement.GetInternalCompliance(Convert.ToInt64(lblComplianceID2.Text));

                        int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                        int instanceId = Convert.ToInt32(Request.QueryString["InId"].ToString());

                        bool checkisrequired = InternalComplianceManagement.GetLogDetails(Convert.ToInt32(complianceInfo.ID), instanceId, scheduleOnId);
                        if (checkisrequired)
                        {
                            HDNIsDocumentCompulsary.Value = "T";
                            IsDocumentCompulsary = true;
                        }
                        else
                        {
                            HDNIsDocumentCompulsary.Value = "F";
                            IsDocumentCompulsary = false;
                        }
                    }
                    else
                    {
                        if (ddlStatus2.SelectedValue != "18") //STT Added status 'Not Complied' Document not mandatory
                        {
                            var complianceInfo = Business.InternalComplianceManagement.GetInternalCompliance(Convert.ToInt64(lblComplianceID2.Text));
                            if (complianceInfo.IsDocumentRequired != true)
                            {
                                HDNIsDocumentCompulsary.Value = "F";
                                IsDocumentCompulsary = false;
                            }
                            else
                            {
                                HDNIsDocumentCompulsary.Value = "T";
                                IsDocumentCompulsary = true;
                            }
                        }
                        else
                        {
                            HDNIsDocumentCompulsary.Value = "F";
                            IsDocumentCompulsary = false;
                        }
                    }
                    Label1.Text = "";
                    Label1.Style.Add("display", "none");
                    Label1.Style.Remove("class");

                    #region insert
                    //InternalComplianceTransaction
                    string tbxDate = Request.Form[tbxDate2.UniqueID];

                    Boolean isDocument = false;
                    if (ddlStatus2.SelectedValue != "16" || ddlStatus2.SelectedValue != "18" || ddlStatus2.SelectedValue != "19") //STT Added status 'Not Complied' Document not mandatory
                    {
                        if (IsDocumentCompulsary == true)
                        {
                            // Document mandatory
                            if (grdInternalDocument.Rows.Count > 0)
                            {
                                for (int i = 0; i < grdInternalDocument.Rows.Count; i++)
                                {
                                    Label lblDocType = (Label)grdInternalDocument.Rows[i].FindControl("lblInternalDocType");
                                    if (lblDocType.Text == "Compliance Document")
                                    {
                                        isDocument = true;
                                    }
                                }
                            }
                            else
                            {
                                isDocument = false;
                            }
                        }
                        else
                        {
                            // Document not mandatory
                            isDocument = true;
                        }
                    }
                    else
                    {
                        // Document not mandatory
                        isDocument = true;
                    }

                    if (isDocument == true)
                    {
                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                        {
                            InternalComplianceScheduledOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                            InternalComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value),
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                            StatusId = Convert.ToInt32(ddlStatus2.SelectedValue),
                            StatusChangedOn = DateTime.ParseExact(tbxDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            Remarks = tbxRemarks2.Text,
                            RemarkTypeID = RemarkTypeID
                        };
                        if (IsRemark == true)
                        {
                            if (ddlStatus2.SelectedValue == "18" || ddlStatus2.SelectedValue == "3")
                            { 
                                if (ddlRemark.SelectedItem.Text != "Others")
                                {
                                    transaction.Remarks = ddlRemark.SelectedItem.Text;
                                }
                          }                          
                        }

                        var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "P");
                        if (leavedetails != null)
                        {
                            transaction.OUserID = leavedetails.OldPerformerID;
                        }
                        List<InternalFileData> files = new List<InternalFileData>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                        //HttpFileCollection fileCollection1 = Request.Files;

                        var httpRequest = System.Web.HttpContext.Current.Request;
                        HttpFileCollection fileCollection1 = httpRequest.Files;
                        string directoryPath = null;
                        bool blankfileCount = true;
                        var TempDocData = Business.ComplianceManagement.GetTempInternalComplianceDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));

                        if (TempDocData.Count > 0)
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (true)// if (fileCollection1.Count > 0)
                                {
                                    int? customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                    var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                                    string version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                    directoryPath = "InternalAvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;

                                    foreach (var item in TempDocData)
                                    {
                                        if (item.ISLink)
                                        {
                                            String fileName = "";
                                            if (item.DocType == "IS")
                                            {
                                                fileName = "InternalComplianceDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            else
                                            {
                                                fileName = "InternalWorkingFiles_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                            }

                                            InternalFileData file = new InternalFileData()
                                            {
                                                Name = fileName,
                                                Version = version,
                                                FilePath = item.DocPath,
                                                VersionDate = DateTime.UtcNow,
                                                ISLink = true
                                            };

                                            files.Add(file);
                                        }
                                        else
                                        {
                                            #region fileupload
                                            //HttpPostedFile uploadfile = fileCollection1[i];
                                            //string[] keys = fileCollection1.Keys[i].Split('$');
                                            String fileName = "";
                                            if (item.DocType == "IS")
                                            {
                                                fileName = "InternalComplianceDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            else
                                            {
                                                fileName = "InternalWorkingFiles_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                            }

                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                            if (item.DocData.Length > 0)
                                            {
                                                string filepathvalue = string.Empty;
                                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                filepathvalue = vale.Replace(@"\", "/");

                                                InternalFileData file = new InternalFileData()
                                                {
                                                    Name = fileName,
                                                    FilePath = filepathvalue, // directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                    FileKey = fileKey.ToString(),
                                                    Version = version,
                                                    VersionDate = DateTime.UtcNow,
                                                    FileSize = item.FileSize,
                                                };

                                                files.Add(file);

                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(item.DocName))
                                                    blankfileCount = false;
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (true)// if (fileCollection1.Count > 0)
                                {
                                    int? customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                    var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                                    string version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(hdnComplianceScheduledOnId.Value));

                                    //string version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));


                                    //Change by SACHIN on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version;
                                    }
                                    else
                                    {
                                        directoryPath = Server.MapPath("~/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                                    }

                                    DocumentManagement.CreateDirectory(directoryPath);

                                    foreach (var item in TempDocData)
                                    {
                                        if (item.ISLink)
                                        {
                                            String fileName = "";
                                            if (item.DocType == "IS")
                                            {
                                                fileName = "InternalComplianceDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            else
                                            {
                                                fileName = "InternalWorkingFiles_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                            }

                                            InternalFileData file = new InternalFileData()
                                            {
                                                Name = fileName,
                                                Version = version,
                                                FilePath = item.DocPath,
                                                VersionDate = DateTime.UtcNow,
                                                ISLink = true
                                            };

                                            files.Add(file);
                                        }
                                        else
                                        {
                                            #region fileupload
                                            //HttpPostedFile uploadfile = fileCollection1[i];
                                            //string[] keys = fileCollection1.Keys[i].Split('$');
                                            String fileName = "";
                                            if (item.DocType == "IS")
                                            {
                                                fileName = "InternalComplianceDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            else
                                            {
                                                fileName = "InternalWorkingFiles_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                            }

                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));

                                            //Stream fs = uploadfile.InputStream;
                                            //BinaryReader br = new BinaryReader(fs);
                                            //Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            //Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                            if (item.DocData.Length > 0)
                                            {
                                                string filepathvalue = string.Empty;
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {
                                                    string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                    filepathvalue = vale;
                                                }
                                                else
                                                {
                                                    filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                }
                                                InternalFileData file = new InternalFileData()
                                                {
                                                    Name = fileName,
                                                    FilePath = filepathvalue, // directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                    FileKey = fileKey.ToString(),
                                                    Version = version,
                                                    VersionDate = DateTime.UtcNow,
                                                    FileSize = item.FileSize,
                                                };

                                                files.Add(file);

                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(item.DocName))
                                                    blankfileCount = false;
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                        if (files.Count == 0)
                        {
                            if (Directory.Exists(directoryPath))
                                Directory.Delete(directoryPath);
                        }

                        bool flag = false;
                        bool flag1 = false;

                        if (blankfileCount)
                        {
                            flag = Business.InternalComplianceManagement.CreateTransaction(transaction, files, list, Filelist, directoryPath, Convert.ToInt64(hdnComplianceScheduledOnId.Value), CustId);

                            //try
                            //{
                            //    foreach (var item in TempDocData)
                            //    {
                            //        if (item.ISLink == false)
                            //        {
                            //            string path = Server.MapPath(item.DocPath);
                            //            FileInfo file = new FileInfo(path);
                            //            if (file.Exists) //check file exsit or not
                            //            {
                            //                file.Delete();
                            //            }
                            //        }
                            //    }
                            //}
                            //catch (Exception)
                            //{
                            //}

                            flag1 = Business.ComplianceManagement.DeleteTempInternalDocumentFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload blank files.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload blank files.')", true);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                        }
                        #endregion

                        try
                        {
                            Business.InternalComplianceManagement.BindInternalDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                        }
                        catch { }
                        //bool flag = true;
                        if (flag != true)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('save successfully')", true);

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select documents for upload.";
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                }
                #endregion
            }
        }
        protected void grdTransactionHistory2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.Data); // create the file
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload2_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample2_Click(object sender, EventArgs e)
        {
            try
            {
                //var file = Business.InternalComplianceManagement.GetInternalComplianceFormByID(Convert.ToInt32(lbDownloadSample4.CommandArgument));

                //Response.Buffer = true;
                //Response.Clear();
                //Response.ContentType = "application/octet-stream";
                //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.FileData); // create the file
                //Response.Flush(); // send it to the client to download

                var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceFormByID(Convert.ToInt32(lbDownloadSample4.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample4.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var file in complianceform)
                    {
                        if (file.FileData != null)
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            ComplianceZip.AddEntry(str, file.FileData);
                            i++;
                        }
                    }

                    string fileName = "ComplianceID_" + complianceID1 + "_InternalSampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails2_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformerInternal", string.Format("initializeDatePickerforPerformerInternal(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformerInternal", "initializeDatePickerforPerformerInternal(null);", true);
                }

                if (lblDueDate.Text != "")
                {
                    if (ddlStatus2.SelectedValue == "3")
                    {
                        DateTime Date = DateTime.ParseExact(hiddenDueDateInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Date = Date.AddDays(1);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDueInternal", string.Format("initializeDatePickerOverDueInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                    }
                    if (ddlStatus2.SelectedValue == "2")
                    {
                        DateTime Date = DateTime.ParseExact(hiddenDueDateInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTimeInternal", string.Format("initializeDatePickerInTimeInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory2_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {

                tbxRemarks2.Text = tbxDate2.Text = string.Empty;
                InternalVD = null;
                //InternalVD.DataBind();
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                upComplianceDetails.Update();

                ViewState["InternalScheduledOnID"] = ScheduledOnID;
                ViewState["complianceInstanceID"] = complianceInstanceID;

                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        protected void btnUpload_Click(object sender, EventArgs e)
        {


            HttpFileCollection fileCollection = Request.Files;
            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile uploadfile = fileCollection[i];
                string fileName = Path.GetFileName(uploadfile.FileName);
                if (uploadfile.ContentLength > 0)
                {
                    //uploadfile.SaveAs(Server.MapPath("~/UploadFiles/") + fileName);
                    //lblMessage.Text += fileName + "Saved Successfully<br>";
                }
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["InternalScheduledOnID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("Download"))
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            else if (e.CommandName.Equals("Delete"))
            {
                DeleteFile(Convert.ToInt32(e.CommandArgument));
                BindDocument(Convert.ToInt32(ViewState["InternalScheduledOnID"]));
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by SACHIN on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                        // filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }

                    //string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file != null)
                {
                    string path = string.Empty;
                    //Change by SACHIN on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        //path = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);

                        string pathvalue = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                        path = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                    }
                    else
                    {
                        path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    }
                    DocumentManagement.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkDocbutton = (LinkButton)e.Item.FindControl("lbtLinkDocbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);

            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkbutton = (LinkButton)e.Item.FindControl("lbtLinkbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkbutton);
            }
        }


        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        //comment by rahul on 20 JAN 2017
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionViewInternal.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionViewInternal.DataBind();
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName; 
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionViewInternal.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionViewInternal.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, string period, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_TaskInstanceTransactionInternalView_Result> masterdocumentData = new List<SP_TaskInstanceTransactionInternalView_Result>();

                    masterdocumentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
                                          where row.ForMonth == period
                                          && row.RoleID == roleID
                                          && row.CustomerBranchID == CustomerBranchid
                                          && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                          select row).ToList();

                    var documentData = (from row in masterdocumentData
                                        where row.ParentID == null
                                        && row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    //var documentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
                    //                    where row.ParentID == null
                    //                    && row.ForMonth == period
                    //                    && row.RoleID == roleID
                    //                    && row.CustomerBranchID == CustomerBranchid
                    //                    && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                    //                    select row).ToList();

                    var TaskdocumentData = (from row in entities.TaskDocumentsViews
                                            where row.CustomerID == customerID && row.TaskType == 2
                                            select row).ToList();
                    if (TaskdocumentData.Count > 0)
                    {
                        TaskdocumentData = TaskdocumentData.Where(entry => entry.TaskStatusID == 4 || entry.TaskStatusID == 5).ToList();
                        MastersTaskDocumentslistinternal = TaskdocumentData;
                    }


                    if (documentData.Count != 0)
                    {
                        MastersTasklistinternal = masterdocumentData;
                        divTask.Visible = true;
                        gridSubTaskInternal.DataSource = documentData;
                        gridSubTaskInternal.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }


        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }

        protected void gridSubTaskInternal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage 
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str,  DocumentManagement.ReadDocFiles(filePath));
                                            
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage 
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage 
                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionViewInternal.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionViewInternal.DataBind();
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternalPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternalPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage 
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionViewInternal.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionViewInternal.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternalPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternalPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }

                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionViewInternal_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    rptComplianceVersionViewInternal.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionViewInternal.DataBind();

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        { 
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName; 
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                //UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternal();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionViewInternal.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionViewInternal.DataBind();

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                //UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternal();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTaskInternal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    CheckBox chkTask = (CheckBox)e.Row.FindControl("chkTask");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");
                    Label lblsubtasks = (Label)e.Row.FindControl("lblsubtasks");
                    Label lblTaskID = (Label)e.Row.FindControl("lblTaskID");
                    Label lblCbranchId = (Label)e.Row.FindControl("lblCbranchId");
                    Label lblForMonth = (Label)e.Row.FindControl("lblForMonth");
                    Label lblsubtaskDocuments = (Label)e.Row.FindControl("lblsubtaskDocuments");
                    Image chkDocument = (Image)e.Row.FindControl("chkDocument");

                    int taskId = Convert.ToInt32(lblTaskID.Text);
                    int CbranchId = Convert.ToInt32(lblCbranchId.Text);

                    if (MastersTasklistinternal.Count > 0)
                    {
                        var documentData = (from row in MastersTasklistinternal
                                            where row.ParentID == (long?)taskId &&
                                             row.ForMonth == lblForMonth.Text
                                             && row.CustomerBranchID == (long?)CbranchId
                                             && row.RoleID == 3
                                            select row).ToList();

                        string strDocumentSubtasks = "<div style='float:left;border-bottom: 2px solid #dddddd;' class='topdivlive subhead'> <div style='float:left;' class='topdivlivetext subhead'>Task/Compliance</div> <div style='float:left;' class='topdivliveperformer subhead'>Performer</div> <div style='float:left;' class='topdivliveimage subhead'>Action</div></div><div style='clear:both;height:5px;'></div>";
                        string strSubtasks = "<div style='float:left;border-bottom: 2px solid #dddddd;' class='topdivlive subhead'> <div style='float:left;' class='topdivlivetext subhead'>Task/Compliance</div> <div style='float:left;' class='topdivliveperformer subhead'>Performer</div> <div style='float:left;' class='topdivliveimage subhead'>Action</div></div><div style='clear:both;height:5px;'></div>";
                        int DocCounter = 0;
                        int counter = 0;
                        foreach (var item in documentData)
                        {
                            counter += 1;
                            string disp = "none";
                            if (item.UserID == AuthenticationHelper.UserID && (item.TaskStatusID == 1 || item.TaskStatusID == 8))
                            {
                                disp = "block";
                            }
                            #region Document
                            if (MastersTaskDocumentslistinternal.Count > 0)
                            {
                                var taskdocumentData = (from row in MastersTaskDocumentslistinternal
                                                        where row.ForMonth == item.ForMonth &&
                                                         row.CustomerBranchID == item.CustomerBranchID
                                                         && row.TaskScheduleOnID == item.TaskScheduledOnID
                                                        select row).ToList();
                                if (taskdocumentData.Count > 0)
                                {
                                    DocCounter = 1;
                                    strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item.TaskStatusID + "' data-Formonth='" + item.ForMonth + "' data-InId='" + item.TaskInstanceID + "' data-scId='" + item.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                    strDocumentSubtasks += "</div>";
                                }
                            }
                            #endregion

                            if (1 == 1)
                            {
                                string topline = "";
                                if (counter > 1)
                                {
                                    topline = "topline";
                                }
                                strSubtasks += "<div style='float:left;' class='topdivlive " + topline + "'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='openInternalTaskSummary(this)' data-statusId='" + item.TaskStatusID + "' data-Formonth='" + item.ForMonth + "' data-InId='" + item.TaskInstanceID + "' data-scId='" + item.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";

                                var documentData1 = (from row in MastersTasklistinternal
                                                     where
                                                      row.ForMonth == lblForMonth.Text
                                                      && row.CustomerBranchID == (long?)CbranchId
                                                      && row.RoleID == 3
                                                       && row.ParentID == item.TaskID
                                                     select row).ToList();
                                foreach (var item1 in documentData1)
                                {
                                    disp = "none";
                                    if (item1.UserID == AuthenticationHelper.UserID && (item1.TaskStatusID == 1 || item1.TaskStatusID == 8))
                                    {
                                        disp = "block";
                                    }
                                    //style='display:"+ disp + "'
                                    #region Document1
                                    if (MastersTaskDocumentslistinternal.Count > 0)
                                    {
                                        var taskdocumentData1 = (from row in MastersTaskDocumentslistinternal
                                                                 where row.ForMonth == item1.ForMonth &&
                                                                  row.CustomerBranchID == item1.CustomerBranchID
                                                                  && row.TaskScheduleOnID == item1.TaskScheduledOnID
                                                                 select row).ToList();
                                        if (taskdocumentData1.Count > 0)
                                        {
                                            DocCounter = 1;
                                            strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item1.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                            strDocumentSubtasks += "</div>";
                                        }
                                    }
                                    #endregion

                                    strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item1.TaskTitle + "</div><div style='float:left;margin-left: -5px;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "' onclick='openInternalTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                    var documentData2 = (from row in MastersTasklistinternal
                                                         where
                                                           row.ForMonth == lblForMonth.Text
                                                           && row.CustomerBranchID == (long?)CbranchId
                                                           && row.RoleID == 3
                                                           && row.ParentID == item1.TaskID
                                                         select row).ToList();
                                    foreach (var item2 in documentData2)
                                    {
                                        disp = "none";
                                        if (item2.UserID == AuthenticationHelper.UserID && (item2.TaskStatusID == 1 || item2.TaskStatusID == 8))
                                        {
                                            disp = "block";
                                        }
                                        //style='display:"+ disp + "'
                                        #region Document2
                                        if (MastersTaskDocumentslistinternal.Count > 0)
                                        {
                                            var taskdocumentData2 = (from row in MastersTaskDocumentslistinternal
                                                                     where row.ForMonth == item2.ForMonth &&
                                                                      row.CustomerBranchID == item2.CustomerBranchID
                                                                      && row.TaskScheduleOnID == item2.TaskScheduledOnID
                                                                     select row).ToList();
                                            if (taskdocumentData2.Count > 0)
                                            {
                                                strDocumentSubtasks += "<div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input style='display:" + disp + "' type='button' onclick='openInternalTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                strDocumentSubtasks += "</div>";
                                            }
                                        }
                                        #endregion


                                        strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input style='display:" + disp + "' type='button' onclick='openInternalTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                        var documentData3 = (from row in MastersTasklistinternal
                                                             where
                                                               row.ForMonth == lblForMonth.Text
                                                               && row.CustomerBranchID == (long?)CbranchId
                                                               && row.RoleID == 3
                                                               && row.ParentID == item2.TaskID
                                                             select row).ToList();
                                        foreach (var item3 in documentData3)
                                        {
                                            disp = "none";
                                            if (item3.UserID == AuthenticationHelper.UserID && (item3.TaskStatusID == 1 || item3.TaskStatusID == 8))
                                            {
                                                disp = "block";
                                            }
                                            //style='display:"+ disp + "'
                                            #region Document3
                                            if (MastersTaskDocumentslistinternal.Count > 0)
                                            {
                                                var taskdocumentData3 = (from row in MastersTaskDocumentslistinternal
                                                                         where row.ForMonth == item3.ForMonth &&
                                                                          row.CustomerBranchID == item3.CustomerBranchID
                                                                          && row.TaskScheduleOnID == item3.TaskScheduledOnID
                                                                         select row).ToList();
                                                if (taskdocumentData3.Count > 0)
                                                {
                                                    DocCounter = 1;
                                                    strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item3.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button'  onclick ='downloadTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                    strDocumentSubtasks += "</div>";
                                                }


                                            }
                                            #endregion

                                            strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item3.TaskTitle + "</div><div style='float:left;margin-left: -15px;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' style='display:" + disp + "' class='topdivliveimage'><input type='button'  onclick='openInternalTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                            strSubtasks += "</div>";
                                        }
                                        strSubtasks += "</div>";


                                    }
                                    strSubtasks += "</div>";

                                }
                                strSubtasks += "</div>";

                            }
                        }
                        lblsubtasks.Text = strSubtasks;
                        if (DocCounter == 0)
                        {
                            chkDocument.Visible = false;
                        }
                        lblsubtaskDocuments.Text = strDocumentSubtasks;
                    }
                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    chkTask.Enabled = false;
                                    chkTask.Checked = true;
                                    btnSubTaskDocDownload.Visible = false;
                                    lblSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void LoadSubEntitiesTask(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            var query = (from row in entities.Tasks
                         where row.ParentID != null
                         && row.ParentID == nvp.ID
                         select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                Tasklist.Add(item.ID);
                LoadSubEntitiesTask(item, false, entities);
            }
        }

        public static List<NameValueHierarchy> GetAllHierarchyTask(int TaskID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Tasks
                             where row.ID == TaskID
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    Tasklist.Add(item.ID);
                    LoadSubEntitiesTask(item, true, entities);
                }
            }
            return hierarchy;
        }

        protected void lblNo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvrow in gridSubTaskInternal.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("chkTask");
                    if (chk != null & chk.Checked)
                    {
                        int TaskID = Convert.ToInt32(gridSubTaskInternal.DataKeys[gvrow.RowIndex].Value.ToString());
                        string str = gridSubTaskInternal.DataKeys[gvrow.RowIndex].Value.ToString();
                        Label lblTaskScheduledOnID = (Label)gvrow.FindControl("lblTaskScheduledOnID");
                        int TaskScheduledOnID = Convert.ToInt32(lblTaskScheduledOnID.Text);
                        Label lblComplianceScheduleOnID = (Label)gvrow.FindControl("lblComplianceScheduleOnID");
                        int ComplianceScheduleOnID = Convert.ToInt32(lblComplianceScheduleOnID.Text);
                        Label lbllblTaskInstanceID = (Label)gvrow.FindControl("lblTaskInstanceID");
                        int TaskInstanceID = Convert.ToInt32(lbllblTaskInstanceID.Text);
                        Label lblMainTaskID = (Label)gvrow.FindControl("lblMainTaskID");
                        int MainTaskID = Convert.ToInt32(lblMainTaskID.Text);
                        Label lblForMonth = (Label)gvrow.FindControl("lblForMonth");
                        string ForMonth = Convert.ToString(lblForMonth.Text);


                        if (TaskID != -1)
                        {
                            long? parentID = TaskID;
                            List<NameValueHierarchy> hierarchy = new List<NameValueHierarchy>();
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Tasklist.Clear();
                                GetAllHierarchyTask(TaskID);
                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                var documentData = (from row in entities.TaskInstanceTransactionViews
                                                    where Tasklist.Contains(row.TaskID)
                                                    && row.ForMonth == ForMonth
                                                    && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                    && row.RoleID == 4
                                                    && row.CustomerID == customerID
                                                    select row).ToList();

                                for (int i = 0; i < documentData.Count; i++)
                                {
                                    var IsTaskTransactionPresent = TaskManagment.CheckTaskTransacion(Convert.ToInt64(documentData[i].TaskInstanceID), Convert.ToInt64(documentData[i].TaskScheduledOnID), Convert.ToInt64(documentData[i].ComplianceScheduleOnID));
                                    if (IsTaskTransactionPresent == false)
                                    {
                                        int StatusID = 4;
                                        TaskTransaction transaction = new TaskTransaction()
                                        {
                                            ComplianceScheduleOnID = Convert.ToInt64(documentData[i].ComplianceScheduleOnID),
                                            TaskScheduleOnID = Convert.ToInt64(documentData[i].TaskScheduledOnID),
                                            TaskInstanceId = Convert.ToInt64(documentData[i].TaskInstanceID),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedByText = AuthenticationHelper.User,
                                            StatusId = StatusID,
                                            StatusChangedOn = DateTime.ParseExact(DateTime.Now.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            Remarks = "Not applicable",
                                            IsTaskClose = true,
                                        };
                                        bool sucess = TaskManagment.CreateTaskTransaction(transaction, null, null, null);
                                    }
                                }
                            }
                        }
                    }
                }
                var IsAfter = "";
                var Period = "";
                int CustomerBrachID = -1;
                int ScheduledOnID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["IsAfter"])))
                {
                    IsAfter = Convert.ToString(ViewState["IsAfter"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Period"])))
                {
                    Period = Convert.ToString(ViewState["Period"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBrachID"])))
                {
                    CustomerBrachID = Convert.ToInt32(ViewState["CustomerBrachID"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["InternalScheduledOnID"])))
                {
                    ScheduledOnID = Convert.ToInt32(ViewState["InternalScheduledOnID"]);
                }

                var showHideButton = false;
                if (IsAfter == "false")
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave2.Enabled = showHideButton;
                }
                else
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave2.Enabled = true;
                }

                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceSampleView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    InternalComplianceForm complianceForm = Business.InternalComplianceManagement.GetSelectedInternalComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

                    string Filename = complianceForm.Name;
                    string filePath = Filename;
                    if (filePath != null)
                    {
                        try
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            //string[] path = filePath.Split('/');
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }
                            //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + FileDate;
                            string FileName = DateFolder + "/" + User + "" + extension;
                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            bw.Write(complianceForm.FileData);
                            bw.Close();
                            lblpathsample4.Text = FileName;

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfile4('" + lblpathsample4.Text + "');", true);
                            }
                        }
                        catch (Exception ex)
                        {

                        }

                        //var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceForm(Convert.ToInt32(lbDownloadSample4.CommandArgument));

                        //rptComplianceSampleView.DataSource = complianceform;
                        //rptComplianceSampleView.DataBind();

                    }

                    //if (CMPDocuments != null)
                    //{
                    //    string fullfilePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath));
                    //    string filePath = CMPDocuments.FilePath;
                    //    string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                    //    if (CMPDocuments.FilePath != null && File.Exists(fullfilePath))
                    //    {
                    //        string extension = System.IO.Path.GetExtension(CompDocPath);

                    //        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                    //        {
                    //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                    //        }
                    //        else
                    //        {
                    //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocPath + "');", true);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView);
            }
        }

        protected void ddlStatus2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (details != null)
                {
                    //int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                    //int instanceId = Convert.ToInt32(Request.QueryString["InId"].ToString());
                    //var complianceInfo = Business.InternalComplianceManagement.GetInternalCompliance(Convert.ToInt64(lblComplianceID2.Text));
                    //bool checkisrequired = InternalComplianceManagement.GetLogDetails(Convert.ToInt32(complianceInfo.ID), instanceId, scheduleOnId);
                    //if (checkisrequired)
                    //{
                    //    HDNIsDocumentCompulsary.Value = "T";
                    //    IsDocumentCompulsary = true;
                    //}
                    //else
                    //{
                    //    HDNIsDocumentCompulsary.Value = "F";
                    //    IsDocumentCompulsary = false;
                    //}
                }
                else
                {
                    if (ddlStatus2.SelectedValue != "19")
                    {
                        var complianceInfo = Business.InternalComplianceManagement.GetInternalCompliance(Convert.ToInt64(lblComplianceID2.Text));
                        if (complianceInfo.IsDocumentRequired != true)
                        {
                            HDNIsDocumentCompulsary.Value = "F";
                            IsDocumentCompulsary = false;
                        }
                        else
                        {
                            HDNIsDocumentCompulsary.Value = "T";
                            IsDocumentCompulsary = true;
                        }
                    }
                    else
                    {
                        HDNIsDocumentCompulsary.Value = "T";
                        IsDocumentCompulsary = true;
                    }
                }
                if (ddlStatus2.SelectedValue == "3" || ddlStatus2.SelectedValue == "18" )
                {
                    IsRemarkCompulsary1 = false;
                }
                else
                {
                    IsRemarkCompulsary1 = true;
                }

                if (lblDueDate.Text != "")
                {
                    DateTime Date = DateTime.ParseExact(hiddenDueDateInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    DateTime dtcurrent = DateTime.Now;
                    if (Date > dtcurrent)  //Upcoming
                    {
                        if (ddlStatus2.SelectedValue == "3")
                        {
                            Date = Date.AddDays(1);
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerUpcomingInternal", string.Format("initializeDatePickerUpcomingInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        }
                        if (ddlStatus2.SelectedValue == "2")
                        {
                            //DateTime Date = DateTime.ParseExact(hiddenDueDateInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTimeInternal", string.Format("initializeDatePickerInTimeInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        }
                    }
                    else // Overdue
                    {
                        if (ddlStatus2.SelectedValue == "3")
                        {
                            Date = Date.AddDays(1);
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDueInternal", string.Format("initializeDatePickerOverDueInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        }
                        if (ddlStatus2.SelectedValue == "2")
                        {
                            //DateTime Date = DateTime.ParseExact(hiddenDueDateInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTimeInternal", string.Format("initializeDatePickerInTimeInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        }
                    }
                }
                if(ddlStatus2.SelectedValue=="18")
                {
                    tbxDate2.Text = DateTime.Today.Date.ToString("dd-MM-yyyy");
                }
                if (ddlStatus2.SelectedValue == "16" || ddlStatus2.SelectedValue == "18" || ddlStatus2.SelectedValue == "19")
                {
                    btnSave2.Enabled = true;
                    IsRemarkCompulsary1 = false;
                    HDNIsDocumentCompulsary.Value = "F";
                    IsDocumentCompulsary = false;
                }

                if (ddlStatus2.SelectedValue == "18")
                {
                    IsRemarkCompulsary1 = true;
                }

                if (details != null)
                {
                    if (ddlStatus2.SelectedValue == "18" || ddlStatus2.SelectedValue == "16")
                    {
                        btnSave2.Enabled = true;
                        IsRemarkCompulsary1 = true;
                        HDNIsDocumentCompulsary.Value = "F";
                        IsDocumentCompulsary = false;
                    }
                }
                if(IsRemark == true)
                {
                    if (ddlStatus2.SelectedValue == "18" || ddlStatus2.SelectedValue == "3")
                    {

                        BindRemark(Convert.ToInt32(ddlStatus2.SelectedValue));
                        ddlRemark.Visible = true;
                        tbxRemarks2.Visible = false;
                    }
                    else
                    {
                        tbxRemarks2.Visible = true;
                        ddlRemark.Visible = false;
                    }
                }
                
            
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTempDocumentData(long ScheduledOnID)
        {
            try
            {
                grdInternalDocument.DataSource = null;
                grdInternalDocument.DataBind();
                var DocData = DocumentManagement.GetTempInternalComplianceDocumentData(ScheduledOnID);
                if (DocData.Count > 0)
                {
                    grdInternalDocument.Visible = true;
                    grdInternalDocument.DataSource = DocData;
                    grdInternalDocument.DataBind();
                }
                else
                {
                    grdInternalDocument.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                            BindTempDocumentData(Convert.ToInt64(ViewState["InternalScheduledOnID"]));
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                            if (file != null)
                            {
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                Response.TransmitFile(Server.MapPath(file.DocPath));
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        BindTempDocumentData(Convert.ToInt64(ViewState["InternalScheduledOnID"]));
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.DocPath));
                            if (file.DocName != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    string CompDocReviewPath = file.DocPath;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ;
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenInternalDocumentPriview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }
        protected void grdInternalDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDocumentName = (Label)e.Row.FindControl("lblDocumentName");
                    Label lblIsLinkTrue = (Label)e.Row.FindControl("lblIsLinkTrue");
                    Label lblInternalDocType = (Label)e.Row.FindControl("lblInternalDocType");

                    if (lblInternalDocType.Text.Trim() == "IS")
                    {
                        lblInternalDocType.Text = "Compliance Document";
                    }
                    else
                    {
                        lblInternalDocType.Text = "Working Files";
                    }


                    LinkButton lnkRedirectDocument = (LinkButton)e.Row.FindControl("lnkRedirectDocument");
                    LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadInternalDocument");
                    LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewInternalDocument");

                    if (lblIsLinkTrue.Text == "True")
                    {
                        lnkDownloadDocument.Visible = false;
                        lnkViewDocument.Visible = false;
                        lnkRedirectDocument.Visible = true;
                        lblDocumentName.Visible = false;
                        //lnkDeleteDocument.Visible = false;
                    }
                    else
                    {
                        lnkDownloadDocument.Visible = true;
                        lnkViewDocument.Visible = true;
                        lnkRedirectDocument.Visible = false;
                        lblDocumentName.Visible = true;
                        //lnkDeleteDocument.Visible = true;
                    }
                }
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lblInternalDocType = (Label)e.Row.FindControl("lblInternalDocType");

                //    if (lblInternalDocType.Text.Trim() == "IS")
                //    {
                //        lblInternalDocType.Text = "Compliance Document";
                //    }
                //    else
                //    {
                //        lblInternalDocType.Text = "Working Files";
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void UploadInternalDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdInternalDocument.DataSource = null;
                grdInternalDocument.DataBind();
                long ScheduledOnID = Convert.ToInt64(ViewState["InternalScheduledOnID"]);
                long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

                TempComplianceDocument tempComplianceDocument = null;
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    #region file upload
                    bool isBlankFile = false;
                    string[] validFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        int filelength = uploadfile.ContentLength;
                        string fileName = Path.GetFileName(uploadfile.FileName);
                        string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            if (filelength == 0)
                            {
                                isBlankFile = true;
                                break;
                            }
                            else if (ext == "")
                            {
                                isBlankFile = true;
                                break;
                            }
                            else
                            {
                                if (ext != "")
                                {
                                    for (int j = 0; j < validFileTypes.Length; j++)
                                    {
                                        if (ext == "." + validFileTypes[j])
                                        {
                                            isBlankFile = true;
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }
                    if (isBlankFile == false)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string directoryPath = null;

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string[] keys = fileCollection.Keys[i].Split('$');
                                if (keys[keys.Count() - 1].Equals("fuSampleFile2"))
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/Internal/");
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/InternalWorking/");
                                }

                                DocumentManagement.CreateDirectory(directoryPath);
                                string finalPath = Path.Combine(directoryPath, fileName);
                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                tempComplianceDocument = new TempComplianceDocument()
                                {
                                    ScheduleOnID = ScheduledOnID,
                                    ComplianceInstanceID = complianceInstanceID,
                                    DocPath = finalPath,
                                    DocData = bytes,
                                    DocName = fileCollection[i].FileName,
                                    FileSize = uploadfile.ContentLength,
                                };

                                if (keys[keys.Count() - 1].Equals("fuSampleFile2"))
                                {
                                    tempComplianceDocument.DocType = "IS";
                                }
                                else
                                {
                                    tempComplianceDocument.DocType = "IW";
                                }

                                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                                if (_objTempDocumentID > 0)
                                {
                                    BindTempDocumentData(ScheduledOnID);
                                }
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please do not upload blank files.";
                    }
                    #endregion
                }
                BindTempDocumentData(ScheduledOnID);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void UploadlinkWorkingfile_Click(object sender, EventArgs e)
        {
            grdInternalDocument.DataSource = null;
            grdInternalDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["InternalScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = Txtworkingdocumentlnk.Text;
            string fileName = DocumentManagement.getFileName(url);

            if (!string.IsNullOrEmpty(Txtworkingdocumentlnk.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };  
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "IW",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
            }
            Txtworkingdocumentlnk.Text = "";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
        }

        protected void UploadlinkCompliancefile_Click(object sender, EventArgs e)
        {
            grdInternalDocument.DataSource = null;
            grdInternalDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["InternalScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = TxtCompliancedocumentlnk.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(TxtCompliancedocumentlnk.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };  
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "IS",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
            }
            TxtCompliancedocumentlnk.Text = "";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
        }

        protected void btnCancel2_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRemark(int RemarkID)
        {
            try
            {
                // var statusList = "";
                ddlRemark.DataSource = null;
                ddlRemark.DataBind();
                ddlRemark.ClearSelection();
                ddlRemark.DataTextField = "Remark";
                ddlRemark.DataValueField = "ID";
                int ComplianceinstanceID = Convert.ToInt32(Request.QueryString["InId"].ToString());
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
              
                if (RemarkID == 18)
                {
                    bool ISDept = ComplianceStatusManagement.GetInternalComplinceDeptID(ComplianceinstanceID, CustomerID);
                    if (ISDept == true)
                    {
                        var statusList = ComplianceStatusManagement.GetRemarkListByDepartment(RemarkID);
                        ddlRemark.DataSource = statusList;                        
                    }
                    else
                    {
                        var statusList = ComplianceStatusManagement.GetRemarkListByStatus(RemarkID);
                        ddlRemark.DataSource = statusList;                        
                    }
                }
                else
                {
                    var statusList = ComplianceStatusManagement.GetRemarkListByStatus(RemarkID);
                    ddlRemark.DataSource = statusList;                    
                }
                ddlRemark.DataBind();
                ddlRemark.Items.Insert(0, new ListItem("< Select Remark>", "-1"));


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlRemark_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRemark.SelectedValue == "14" || ddlRemark.SelectedValue == "21")
            {
                tbxRemarks2.Visible = true;
                ddlRemark.Visible = false;
            }
            else
            {
                tbxRemarks2.Visible = false;
                ddlRemark.Visible = true;
            }
        }
    }
}