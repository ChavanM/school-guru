﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class AuditCustomerDetailsControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerStatus();
                BindLocationType();
            }
        }

        public void BindLocationType()
        {
            try
            {
                ddlLocationType.DataTextField = "Name";
                ddlLocationType.DataValueField = "ID";
                ddlLocationType.Items.Clear();
                ddlLocationType.DataSource = ProcessManagement.FillLocationType();
                ddlLocationType.DataBind();
                ddlLocationType.Items.Insert(0, new ListItem(" All ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void EditCustomerInformation(int customerID)
        {
            try
            {
                lblErrorMassage.Text = "";
                ViewState["Mode"] = 1;
                ViewState["CustomerID"] = customerID;

                Customer customer = CustomerManagement.GetByID(customerID);
                tbxName.Text = customer.Name;
                tbxAddress.Text = customer.Address;
                tbxBuyerName.Text = customer.BuyerName;
                tbxBuyerContactNo.Text = customer.BuyerContactNumber;
                tbxBuyerEmail.Text = customer.BuyerEmail;
                txtStartDate.Text = customer.StartDate != null ? customer.StartDate.Value.ToString("dd-MM-yyyy") : " ";
                txtEndDate.Text = customer.EndDate != null ? customer.EndDate.Value.ToString("dd-MM-yyyy") : " ";
                txtDiskSpace.Text = customer.DiskSpace;
                if (customer.LocationType != null)
                {
                    ddlLocationType.SelectedValue = Convert.ToString(customer.LocationType);
                }
                if (customer.IComplianceApplicable != null)
                {
                    ddlComplianceApplicable.SelectedValue = Convert.ToString(customer.IComplianceApplicable);
                }
                if (customer.TaskApplicable != null)
                {
                    ddlTaskApplicable.SelectedValue = Convert.ToString(customer.TaskApplicable);
                }
                if (customer.Status != null)
                {
                    ddlCustomerStatus.SelectedValue = Convert.ToString(customer.Status);
                }
                upCustomers.Update();
                //ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "OpenDialog", "$(\"#divCustomersDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void AddCustomer()
        {
            try
            {
                lblErrorMassage.Text = string.Empty;
                ViewState["Mode"] = 0;

                tbxName.Text = tbxAddress.Text = tbxBuyerName.Text = tbxBuyerContactNo.Text = tbxBuyerEmail.Text = txtStartDate.Text = txtEndDate.Text = txtDiskSpace.Text = string.Empty;
                ddlCustomerStatus.SelectedIndex = -1;
                upCustomers.Update();
                //ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "OpenDialog", "$(\"#divCustomersDialog\").dialog('open');initializeDatePicker(null); ", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlComplianceApplicable_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Customer customer = new Customer();
                customer.Name = tbxName.Text;
                customer.Address = tbxAddress.Text;                
                customer.BuyerName = tbxBuyerName.Text;
                customer.BuyerContactNumber = tbxBuyerContactNo.Text;
                customer.BuyerEmail = tbxBuyerEmail.Text;

                string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                string enddate = Request[txtEndDate.UniqueID].ToString().Trim();
                if (strtdate != "" && enddate != "")
                {
                    customer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    customer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                customer.DiskSpace = txtDiskSpace.Text;
                if ((int)ViewState["Mode"] == 1)
                {
                    customer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                }
                if (CustomerManagement.Exists(customer))
                {
                    cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                customer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                customer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                customer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                customer.VerticalApplicable = Convert.ToInt32(ddlVerticalsApplicable.SelectedValue);
                customer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer mstCustomer = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer();
                mstCustomer.Name = tbxName.Text;
                mstCustomer.Address = tbxAddress.Text;
                mstCustomer.BuyerName = tbxBuyerName.Text;
                mstCustomer.BuyerContactNumber = tbxBuyerContactNo.Text;
                mstCustomer.BuyerEmail = tbxBuyerEmail.Text;

                if (strtdate != "" && enddate != "")
                {
                    mstCustomer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    mstCustomer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                mstCustomer.DiskSpace = txtDiskSpace.Text;
                if ((int)ViewState["Mode"] == 1)
                {
                    mstCustomer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                }
                if (CustomerManagementRisk.Exists(mstCustomer))
                {
                    cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
                mstCustomer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                mstCustomer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                mstCustomer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                mstCustomer.VerticalApplicable = Convert.ToInt32(ddlVerticalsApplicable.SelectedValue);
                mstCustomer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);


                if ((int)ViewState["Mode"] == 0)
                {                    
                    CustomerManagement.Create(customer);
                    CustomerManagementRisk.Create(mstCustomer);//added by rahul on 18 April 2016
                    string accessURL = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (Urloutput != null)
                    {
                        accessURL = Urloutput.URL;
                    }
                    else
                    {
                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    // Added by SACHIN 28 April 2016
                    string ReplyEmailAddressName = "Avantis";
                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                                      .Replace("@NewCustomer", customer.Name)
                                      .Replace("@LoginUser", AuthenticationHelper.User)
                                      .Replace("@PortalURL", Convert.ToString(accessURL))
                                      .Replace("@From", ReplyEmailAddressName)
                                      .Replace("@URL", Convert.ToString(accessURL));
                                  
                    //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                    //                    .Replace("@NewCustomer", customer.Name)
                    //                    .Replace("@LoginUser", AuthenticationHelper.User)
                    //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                    //                    .Replace("@From", ReplyEmailAddressName)
                    //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                    //                ;

                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();
                    //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer account created.", message);

                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    CustomerManagement.Update(customer);
                    CustomerManagementRisk.Update(mstCustomer);//added by rahul on 18 April 2016
                }
                //ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "CloseDialog", "$(\"#divCustomersDialog\").dialog('close')", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller1()", true);
                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCustomers_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// This method for binding customer status.
        /// </summary>
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public event EventHandler OnSaved;
    }
}