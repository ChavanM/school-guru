﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class CommonDropdown : System.Web.UI.UserControl
    {
        //public string location
        //{
        //    get { return ddlFilterLocation.SelectedValue; }
        //    set { ddlFilterLocation.SelectedValue = value; }

        //}
        public  event EventHandler location;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                BindCustomer();
                BindLocationType();
                BindProcess("P");
                //BindSubProcess();
            }
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (location != null)
                {
                    location(this, e);
                }
                
                BindProcess("P");              
            }
            else
            {              
                BindProcess("N");             
            }
        }
        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {                
                BindProcess("P");             
            }
            else
            {             
                BindProcess("N");              
            }
        }
        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (ddlFilterProcess.SelectedValue != "All" || ddlFilterProcess.SelectedValue != "" || ddlFilterProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                }               
            }
            else
            {
                if (ddlFilterProcess.SelectedValue != "All" || ddlFilterProcess.SelectedValue != "" || ddlFilterProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                }               
            }
        }
        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                //BindData("P", "S");
            }
            else
            {
                //BindData("N", "S");
            }
        }
        protected void ddlFilterLocationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                //this.BindData("P", "L");
            }
            else
            {
                //this.BindData("N", "L");
            }
        }
        public void BindCustomer()
        {
            long customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("All", "-1"));
            ddlFilterProcess.Items.Insert(0, new ListItem("All", "-1"));
            ddlFilterSubProcess.Items.Insert(0, new ListItem("All", "-1"));
        }
        public void BindLocationType()
        {
            ddlFilterLocationType.DataTextField = "Name";
            ddlFilterLocationType.DataValueField = "ID";
            ddlFilterLocationType.Items.Clear();
            ddlFilterLocationType.DataSource = ProcessManagement.FillLocationType();
            ddlFilterLocationType.DataBind();
            ddlFilterLocationType.Items.Insert(0, new ListItem("All", "-1"));
        }
        private void BindProcess(string flag)
        {
            try
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    if (flag == "P")
                    {
                        var process = ProcessManagement.FillProcess("P", Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        ddlFilterSubProcess.Items.Clear();
                        ddlFilterProcess.Items.Clear();
                        ddlFilterProcess.DataTextField = "Name";
                        ddlFilterProcess.DataValueField = "Id";
                        ddlFilterProcess.DataSource = process;
                        ddlFilterProcess.DataBind();
                        ddlFilterProcess.Items.Insert(0, new ListItem("All", "-1"));
                        if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                        {
                            BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                        }
                    }
                    else
                    {
                        var process = ProcessManagement.FillProcess("N", Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        ddlFilterSubProcess.Items.Clear();
                        ddlFilterProcess.Items.Clear();
                        ddlFilterProcess.DataTextField = "Name";
                        ddlFilterProcess.DataValueField = "Id";
                        ddlFilterProcess.DataSource = process;
                        ddlFilterProcess.DataBind();
                        ddlFilterProcess.Items.Insert(0, new ListItem("All", "-1"));
                        if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                        {
                            BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("All", "-1"));
                }
                else
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("All", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}