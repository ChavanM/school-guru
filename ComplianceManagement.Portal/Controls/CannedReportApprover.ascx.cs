﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Data;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class CannedReportApprover : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFilters();
                dlFilters.SelectedIndex = 0;
                dlFilters_SelectedIndexChanged(null, null);
            }
        }

        private void BindFilters()
        {
            dlFilters.DataSource = Enumerations.GetAll<CannedReportFilterForApprover>();
            dlFilters.DataBind();
        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CannedReportFilterForApprover filter = (CannedReportFilterForApprover)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);

                if (filter == CannedReportFilterForApprover.EntityByCategory || filter == CannedReportFilterForApprover.CategoryByEntity || filter == CannedReportFilterForApprover.RiskByEntity)
                {
                    BindMatrixData(filter);
                }

                switch (filter)
                {
                    case CannedReportFilterForApprover.All:
                        BindListData(filter);
                        break;
                    case CannedReportFilterForApprover.EntityByCategory:
                        BindMatrixData(filter);
                        break;
                    case CannedReportFilterForApprover.CategoryByEntity:
                        BindMatrixData(filter);
                        break;
                    case CannedReportFilterForApprover.RiskByEntity:
                        BindMatrixData(filter);
                        break;
                    case CannedReportFilterForApprover.ForFinalApproval:
                        BindListData(filter);
                        break;
                    case CannedReportFilterForApprover.ConsecutiveDefaulters:
                        BindDefaulters(filter);
                        break;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindDefaulters(CannedReportFilterForApprover filter)
        {
            try
            {
                grdComplianceTransactionsMatrix.Visible = true;
                grdComplianceTransactions.Visible = false;
                grdComplianceTransactionsMatrix.DataSource = CannedReportManagement.GetDefaultersForApprover(AuthenticationHelper.UserID, filter);
                grdComplianceTransactionsMatrix.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindListData(CannedReportFilterForApprover filter)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                grdComplianceTransactionsMatrix.Visible = false;
                grdComplianceTransactions.Visible = true;
                grdComplianceTransactions.DataSource = CannedReportManagement.GetCannedReportDataForApprover(customerid,AuthenticationHelper.UserID, filter);
                grdComplianceTransactions.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindMatrixData(CannedReportFilterForApprover filter)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                grdComplianceTransactionsMatrix.Visible = true;
                grdComplianceTransactions.Visible = false;
                var result = CannedReportManagement.GetNonComplianceReportsForApprover(customerid,AuthenticationHelper.UserID, filter);

                DataTable dataSource = new DataTable();

                var columns = result.Select(entry => entry.Column).Distinct().ToList();
                var rows = result.Select(entry => entry.Row).Distinct().ToList();

                string firstColumnName = string.Empty;

                switch (filter)
                {
                    case CannedReportFilterForApprover.EntityByCategory:
                        firstColumnName = "Location";
                        break;
                    case CannedReportFilterForApprover.CategoryByEntity:
                        firstColumnName = "Category";
                        break;
                    case CannedReportFilterForApprover.RiskByEntity:
                        firstColumnName = "Risk";
                        break;
                }
                dataSource.Columns.Add(new DataColumn(firstColumnName));

                foreach (var column in columns)
                {
                    dataSource.Columns.Add(new DataColumn(column));
                }

                foreach (var rowValue in rows)
                {
                    DataRow row = dataSource.NewRow();

                    row[0] = rowValue;

                    foreach (var columnValue in columns)
                    {
                        row[columnValue] = result.Where(entry => entry.Row == rowValue && entry.Column == columnValue).Select(entry => entry.Count).FirstOrDefault();
                    }

                    dataSource.Rows.Add(row);
                }
                grdComplianceTransactionsMatrix.DataSource = dataSource;
                grdComplianceTransactionsMatrix.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactionsMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactionsMatrix.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DataTable GetGrid()
        {
            try
            {
                dlFilters_SelectedIndexChanged(null, null);
                return (grdComplianceTransactions.DataSource as List<ComplianceInstanceTransactionView>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return null;
                
        }

        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CannedReportFilterForApprover>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        public DataTable GetGridMatrix()
        {
            dlFilters_SelectedIndexChanged(null, null);
            return grdComplianceTransactionsMatrix.DataSource as DataTable;
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                CannedReportFilterForApprover filter = (CannedReportFilterForApprover)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                var cannedReportApproverList = CannedReportManagement.GetCannedReportDataForApprover(customerid,AuthenticationHelper.UserID, filter);
                if (direction == SortDirection.Ascending)
                {
                    cannedReportApproverList = cannedReportApproverList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    cannedReportApproverList = cannedReportApproverList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = cannedReportApproverList;
                grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }


    }
}