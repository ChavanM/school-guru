﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class calendardataAPI : System.Web.UI.Page
    {
        protected  string UploadDocumentLink;
        protected  string date;
        protected  string RoleId;
        protected  string Path;
        protected  string isallorsi;
        protected  int UId;
        protected  int CustId;
        protected  List<Int32> roles;
        protected  int PerformerFlagID;
        protected  int ReviewerFlagID;
        protected  int IsApprover;
        protected  string dhead;
        protected static string Authorization;

        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }

            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            Path = ConfigurationManager.AppSettings["KendoPathApp"];

            if (Request.QueryString["date"] != "undefined")
            {
                date = Convert.ToDateTime(Request.QueryString["date"]).ToString("yyyy-dd-MM");
                DateTime dt = Business.DateTimeExtensions.GetDateCalender(date);
                roles = CustomerBranchManagement.GetAssignedCalenderRoleId(AuthenticationHelper.UserID, dt);
            }
            PerformerFlagID = 0;
            ReviewerFlagID = 0;
            roledisplay.Visible = true;
            if (!string.IsNullOrEmpty(Request.QueryString["m"].ToString()))
            {
                RoleId = Request.QueryString["m"].ToString();                
            }
            dhead = "0";
            if (!string.IsNullOrEmpty(Request.QueryString["dhead"]))
            {
                dhead = Request.QueryString["dhead"].ToString();
            }            
            if (Request.QueryString["m"].ToString() == "8" && (roles.Contains(3) || roles.Contains(4) || roles.Contains(6)))
            {
                PerformerFlagID = 1;
                roledisplay.Visible = false;
            }
            else
            {
                if (roles.Contains(3) && roles.Contains(4))
                {
                    PerformerFlagID = 1;
                    ReviewerFlagID = 1;
                }
                else if (roles.Contains(3))
                {
                    PerformerFlagID = 1;
                }
                else if (roles.Contains(4))
                {
                    ReviewerFlagID = 1;
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["m"]) && Request.QueryString["m"].ToString() == "8")
                {
                    PerformerFlagID = 1;
                    roledisplay.Visible = false;
                }
            }

            string Type = Request.QueryString["Type"].ToString();
            isallorsi = string.Empty;
            if (Type == "0")
            {
                isallorsi = "SI";
            }
            else
            {
                isallorsi = "All";
            }

            IsApprover = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                if (GetApprover.Count > 0)
                {
                    IsApprover = 1;
                }
            }

            if (Convert.ToString(AuthenticationHelper.Role).Equals("MGMT") || Convert.ToString(AuthenticationHelper.Role).Equals("APPR"))
                UploadDocumentLink = "1";
            else
            {
                if (IsApprover == 1)
                    UploadDocumentLink = "1";
                else
                    UploadDocumentLink = "0";
            }
        }
    }
}