﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calendardata.aspx.cs" EnableEventValidation="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.calendardata" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>



<head runat="server">
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>


    <script type="text/javascript">

        function CalenderPopPup(obj) {
            parent.OpenPerrevpopup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'), $(obj).attr('Interimdays'), $(obj).attr('CType'), $(obj).attr('RoleID'), $(obj).attr('data-date'));
        }

        function fComplianceOverview(obj) {
            parent.OpenOverViewpup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'), $(obj).attr('CType'));
        }
        $(document).ready(function () {

            $(".nav a").on("click", function () {
                $(".nav").find(".active").removeClass("active");
                $(this).parent().addClass("active");
            });
            $('#loaderdiv').hide();
            window.parent.hideloader();
        });
        function fredv(type) {
            if (type == "P") {
                $('#liPerformer1').addClass('active');
                $('#liReviewer1').removeClass('active');
                $('#reviewerCalender').removeClass('active');
                $('#performerCalender').addClass('active');
            } else if (type == "R") {
                $('#liPerformer1').removeClass('active');
                $('#liReviewer1').addClass('active');
                $('#reviewerCalender').addClass('active');
                $('#performerCalender').removeClass('active');
            }
        }
    </script>

    <style>
        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        body {
            background: none !important;
        }

        .table {
            width: 100%;
            text-align: left;
        }

            /*.clsheadergrid {
        display: none !important;
    }

    .clsROWgrid {
        border: none !important;
    }*/

            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > tbody > tr > th {
                vertical-align: bottom;
                border-bottom: 1px solid #dddddd;
            }

            .table > thead > tr > th > a {
                vertical-align: bottom;
                border-bottom: 1px solid #dddddd;
            }

            .table tr th {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 1px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .table, pre.prettyprint {
            margin-bottom: 3px !important;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .today > a {
            background-color: #ccc;
            font-weight: 700;
        }

        /*.pill-content > .pill-pane, .tab-content > .tab-pane {
     display: block; 
}*/
    </style>
</head>

<body>
    <form runat="server" id="f1">


        <%--<div class="panel panel-default" style="width: 500px; padding: 10px; margin: 10px">--%>
        <div class="col-lg-12 col-md-12 " style="padding-left: 1px;">
            <%--<div id="Tabs" role="tabpanel">--%>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs calender-li" role="tablist">
                <%if (roles != null)%>
                <%{%>
                   <% if (mroles != null && !mroles.Contains("MGMT"))%>
                   <%{%>
                        <% if (roles.Contains(3))%>
                        <%{%>
                            <li id="liPerformer1" style="cursor: pointer;" runat="server" onclick="fredv('P');"><a id="liPerformer" runat="server" aria-controls="performerCalender" role="tab" data-toggle="tab">Performer</a></li>
                        <%}%>
                        <% if (roles.Contains(4))%>
                        <%{%>
                            <li id="liReviewer1" runat="server" style="cursor: pointer;" onclick="fredv('R');"><a id="liReviewer" runat="server" aria-controls="reviewerCalender" role="tab" data-toggle="tab">Reviewer</a></li>
                        <%}%>
                    <%}%>
                <%}%>
            </ul>

            <div class="tab-content" style="padding-top: 2px">
                <div role="tabpanel" class="tab-pane" runat="server" id="performerCalender">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <div>
                                <asp:GridView runat="server" ID="grdCompliancePerformer" AutoGenerateColumns="false"
                                    GridLines="None" PageSize="5" OnPageIndexChanging="grdCompliancePerformer_PageIndexChanging"
                                    CssClass="table" AllowPaging="True" OnRowDataBound="grdCompliancePerformer_RowDataBound">                                    
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblShortdesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblComplianceStatusID" runat="server" Text='<%# Eval("ComplianceStatusID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Eval("ScheduledOn") %>' Visible="false"></asp:Label>
                                                <asp:Image runat="server" ID="imtemplat" data-toggle="tooltip" data-placement="bottom" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type of Compliance">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CType") %>' ToolTip='<%# Eval("CType") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:Button runat="server" ID="btnChangeStatus" CssClass="btnss" Visible='<%# CanChangeStatus((int)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID"),(string)Eval("CType")) %>'
                                                    instanceId='<%#Eval("ComplianceInstanceID")  %>' data-date='<%#Request.QueryString["date"] %>' ScheduledOnID='<%# Eval("ScheduledOnID")%>'
                                                    Interimdays='<%# Eval("Interimdays")%>' CType='<%# Eval("CType")%>' RoleID='<%# Eval("RoleID")%>'
                                                    OnClientClick='CalenderPopPup(this)' />                                                
                                                <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "MGMT" || roles.Contains(6))
                                                    {%>
                                                <asp:ImageButton ID="lblOverView" Visible="false" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID")  %>' CType='<%# Eval("CType")%>'
                                                    OnClientClick='fComplianceOverview(this)'  Style="cursor:pointer" ToolTip="Click to OverView"></asp:ImageButton>
                                                <%}%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                            </div>
                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px; width: 110px;">
                                    <asp:ImageButton ID="lBPrevious" Style="width: 28px;" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />
                                    <div class="table-paging-text" style="width: 54px; margin-top: -2px;">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" Style="width: 28px;" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div role="tabpanel" runat="server" id="reviewerCalender">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <div>
                                
                                <asp:GridView runat="server" ID="grdComplianceReviewer" AutoGenerateColumns="false"
                                    GridLines="None" PageSize="5" OnPageIndexChanging="grdComplianceReviewer_PageIndexChanging" 
                                     OnRowDataBound="grdComplianceReviewer_RowDataBound"
                                    CssClass="table" AllowPaging="True">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Status">
                                            <ItemTemplate>                                                
                                                <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblComplianceStatusID" runat="server" Text='<%# Eval("ComplianceStatusID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Eval("ScheduledOn") %>' Visible="false"></asp:Label>
                                                <asp:Image runat="server" ID="imtemplat" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type of Compliance">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblType" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CType") %>' ToolTip='<%# Eval("CType") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>

                                                <asp:Button runat="server" ID="btnChangeStatus1" CssClass="btnss" Visible='<%# CanChangeStatus((int)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID"),(string)Eval("CType")) %>'
                                                    instanceId='<%#Eval("ComplianceInstanceID")  %>' data-date='<%#Request.QueryString["date"] %>' ScheduledOnID='<%# Eval("ScheduledOnID")%>'
                                                    Interimdays='<%# Eval("Interimdays")%>' CType='<%# Eval("CType")%>' RoleID='<%# Eval("RoleID")%>'
                                                    OnClientClick='CalenderPopPup(this)' />                                                
                                                <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "MGMT")
                                                    {%>
                                                <asp:ImageButton ID="lblOverView" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID")  %>' CType='<%# Eval("CType")%>'
                                                    OnClientClick='fComplianceOverview(this)' Style="cursor:pointer" ToolTip="Click to OverView"></asp:ImageButton>

                                                <%}%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                </asp:GridView>

                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                            </div>
                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px; max-width: 110px;">
                                    <asp:ImageButton ID="lBPreviousReviewer" Style="width: 28px;" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="PreviousReviewer_Click" />
                                    <div class="table-paging-text" style="width: 54px; margin-top: -2px;">
                                        <p>
                                            <asp:Label ID="SelectedPageNoReviewer" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCountReviewer" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNextReviewer" Style="width: 28px;" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="NextReviewer_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            <asp:HiddenField ID="TotalRowsReviewer" runat="server" Value="0" />
        </div>
        <script>

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        </script>
    </form>
</body>
