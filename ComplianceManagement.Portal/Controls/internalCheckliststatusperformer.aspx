﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="internalCheckliststatusperformer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.internalCheckliststatusperformer" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <title>DASHBOARD :: AVANTIS - Products that simplify</title>
</head>
<body style="overflow-y: scroll; max-height: 500px;">
    <form runat="server" style="background: #f7f7f7;">
        <asp:ScriptManager ID="Isdf" runat="server" LoadScriptsBeforeUI="true" EnablePageMethods="true"></asp:ScriptManager>
        <link href="/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
        <link href="/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <link href="/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
        <link href="/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- full calendar css-->
        <link href="/newcss/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- owl carousel -->
        <link rel="stylesheet" href="/newcss/owl.carousel.css" type="text/css" />
        <!-- Custom styles -->
        <link rel="stylesheet" href="/newcss/fullcalendar.css" type="text/css" />
        <link href="/newcss/stylenew.css" rel="stylesheet" type="text/css" />
        <link href="/newcss/jquery-ui-1.10.7.min.css" rel="stylesheet" type="text/css" />
        <link href="/newcss/jquery.bxslider.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="/newjs/jquery.js"></script>
        <script type="text/javascript" src="/newjs/jquery-ui-1.9.2.custom.min.js"></script>
        <link href="../tree/jquerysctipttop.css" rel="stylesheet" />
        <script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
        <link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />
        <script type="text/javascript">
            function openInNewTab(url) {
                var win = window.open(url, '_blank');
                win.focus();
            }
            function CloseAndBindData() {
                try {
                    window.parent.CloseModalInternalPerformer();
                } catch (e) {

                }
            }

            function CloseCalenderPERPop() {
                try {
                    window.parent.fcloseandcallcal();
                } catch (e) {

                }
            }
            function fFilesubmit() {
                fileUpload = document.getElementById('fuSampleFile2');
                if (fileUpload.value != '') {
                    document.getElementById("<%=UploadDocument.ClientID %>").click();
            }
        }
        function fopenDocumentPriview(file) {
            $('#DocumentPriview').modal('show');
            $('#docPriview').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        function fopendocfile() {
            $('#SampleFilePopUp').modal('show');
            $('#docViewerAll').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample.ClientID %>").text());
    }

    function fopendocfile4() {
        debugger;
        $('#docViewerAll').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample.ClientID %>").text());
    }
    function fopendocInternalChecklisttaskfileReview(file) {
        $('#modalDocumentInternalChecklistViewer').modal('show');
        $('#docInternalChecklistViewer').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    $(document).ready(function () {
        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#DocumentPopUp').modal('hide');
            $('#modalDocumentInternalChecklistViewer').modal('hide');
        });

    });
    // var validFilesTypes = ["exe", "bat", "zip", "rar", "dll"];
    var validFilesTypes = ["exe", "bat", "dll"];
    function ValidateFile() {

        var label = document.getElementById("<%=Label1.ClientID%>");
        var fuSampleFile = $("#<%=fuSampleFile2.ClientID%>").get(0).files;
          <%--  var FileUpload1 = $("#<%=FileUpload12.ClientID%>").get(0).files;--%>
        var isValidFile = true;

        for (var i = 0; i < fuSampleFile.length; i++) {
            var fileExtension = fuSampleFile[i].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                isValidFile = false;
                break;
            }
        }
        if (!isValidFile) {
            label.style.color = "red";
            label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
        }
        return isValidFile;
    }

        </script>

        <style>
            tr.spaceUnder > td {
                padding-bottom: 1em;
            }
        </style>

         <style type="text/css">
        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }
    </style>
        <div id="divComplianceDetailsDialog11">
            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
                OnLoad="upComplianceDetails2_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="true"
                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                            <asp:Label ID="Label1" Visible="false" class="alert alert-block alert-danger fade in" runat="server"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                        </div>
                        <asp:Label ID="Labellockingmsg" style="border-color: #ffd0e1;color: #ff2d55;" text="Compliance is locked,contact to management user" runat="server"></asp:Label>
                        <div class="clearfix" style="margin-bottom: 10px"></div>
                        <div>
                            <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                            <div id="divRiskType2" runat="server" class="circle"></div>
                            <asp:Label ID="lblRiskType2" Style="width: 300px; margin-left: -17px; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                        </div>
                        <div id="ComplianceDetails" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails2">
                                                    <h2>Compliance Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails2"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseComplianceDetails2" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <table style="width: 100%;">
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold;">Compliance ID</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblComplianceID2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold;">Short Description</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblComplianceDiscription2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                     <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold;">Short Form</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblshortForm" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblFrequency2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="OthersDetails2" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails2">
                                                    <h2>Additional Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails2"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseOthersDetails2" class="collapse">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Risk Type</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblRisk2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Sample Form</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblFormNumber2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                            maximunsize="300px" autosize="true" runat="server" />
                                                                        <asp:LinkButton ID="lbDownloadSample2" Style="width: 300px; font-size: 13px; color: blue;"
                                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample2_Click1" />
                                                                        <asp:Label ID="lblSlash" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton ID="lnkViewSampleForm" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocfile();" />
                                                                        <asp:Label ID="lblpathsample" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Location</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Period</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" id="trAuditChecklist" runat="server">
                                                            <td style="width: 25%; font-weight: bold;">Audit Checklist</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblAuditChecklist" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="divTask" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                                <h2>Main Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseTaskSubTask" class="collapse in">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="gridSubTaskInternal" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    AllowPaging="true" PageSize="5" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                    OnRowCommand="gridSubTaskInternal_RowCommand" OnRowDataBound="gridSubTask_RowDataBound" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Task">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>

                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Due Date">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                        </asp:LinkButton>
                                                                        <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                        <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="divDeleteDocument" visible="false" style="text-align: left;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                        OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Compliance Related Documents</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                        ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                    </asp:LinkButton></td>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkDocbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                        OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblWorkingFiles">
                                                                <thead>
                                                                    <th>Compliance Working Files</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="width: 50%">
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                        ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                    </asp:LinkButton></td>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>

                            </fieldset>
                        </div>
                        <div id="UpdateComplianceStatus2" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus2">
                                                    <h2>Update Compliance Status</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus2"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseUpdateComplianceStatus2" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                                                   <table style="width: 100%">
                                                 <% if (UploadDocumentLink == "True")
                                                {%>
                                                <tr>
                                                    <td style="width: 25%;">
                                                         <% if (IsDocumentCompulsary)
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <%}
                                                                else
                                                                {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <%}%>

                                                        <label style="font-weight: bold; vertical-align: text-top;">Compliance Document(s)</label>
                                                    </td>

                                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:TextBox runat="server" ID="TxtChecklistDocument"  class="form-control" /> <%--onchange="Workingdocumentlnktest()"--%>
                                                         </td>
                                                    <td>
                                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Button ID="Uploadlingchecklistfile" runat="server" Text="Add Link" Style="" OnClick="Uploadlingchecklistfile_Click"
                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Fetch Document" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="Uploadlingchecklistfile" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                  </td>
                                                </tr>
                                                 <%}%>
                                                 <% if (UploadDocumentLink != "True")
                                                {%>
                                                <tr>
                                                    <td style="width: 25%;">
                                                       
                                                         <% if (IsDocumentCompulsary)
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <%}
                                                                else
                                                                {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <%}%>

                                                        <label style="font-weight: bold; vertical-align: text-top;">Upload Compliance Document(s)</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 60%;">
                                                        <asp:FileUpload ID="fuSampleFile2" Multiple="Multiple" onchange="fFilesubmit()" runat="server" Style="color: black" />
                                                        <%--  <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuSampleFile2"
                                                            runat="server" ID="rfvFile" ValidationGroup="ComplianceValidationGroup" Display="None" />--%>
                                                    </td>
                                                    <td style="width: 13%;">
                                                        <asp:Button ID="UploadDocument" runat="server" Text="Upload Document" Style="display: none;" OnClick="UploadDocument_Click"
                                                            class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document"
                                                            CausesValidation="true" />
                                                    </td>
                                                </tr>
                                                  <%}%>
                                            </table>
                                                </div>
                                                <div style="margin-bottom: 7px" runat="server" id="divgrdFiles">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%;">
                                                       
                                                                 <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                            PageSize="100" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" OnRowDataBound="grdDocument_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                            <asp:Label ID="lblRedirectDocument" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DocName") %>' ToolTip='<%# Eval("DocName") %>'></asp:Label>
                                                                             <asp:LinkButton ID="lnkRedierctDocument" Style="color: blue;text-decoration: underline;" runat="server" Text='<%# Eval("DocName") %>' OnClientClick=<%# "openInNewTab('" + Eval("DocPath") + "')" %>>                                                                                                                                                                             
                                                                              </asp:LinkButton>                                                                           
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                            <asp:Label ID="lblIsLinkTrue" Visible="false" runat="server" data-placement="bottom" Text='<%# Eval("ISLink") %>' ToolTip='<%# Eval("ISLink") %>'></asp:Label>
                                                                            <asp:Label ID="lblDocType" runat="server" data-placement="bottom" Text='<%# Eval("DocType") %>' ToolTip='<%# Eval("DocType") %>'></asp:Label>
                                                                            <asp:Label ID="lblScheduleOnID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduleOnID") %>'></asp:Label>
                                                                            <asp:Label ID="lblComplianceInstanceID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="lnkDownloadDocument" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerSettings Visible="false" />
                                                            <PagerTemplate>
                                                            </PagerTemplate>
                                                            <EmptyDataTemplate>
                                                                No Record Found
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" class="form-control" Rows="2" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px;">
                                                    <asp:Button Text="Submit" runat="server" ID="btnSave2" OnClick="btnSave_Click" ValidationGroup="ComplianceValidationGroup" CssClass="btn btn-search" />
                                                     <asp:Button Text="Not Complied" runat="server" ID="btnNotComplied" Visible="false" OnClick="btnNotComplied_Click"  Style="margin-left: 15px;" ValidationGroup="ComplianceValidationGroup" CssClass="btn btn-search" />
                                                    <asp:Button Text="Not Applicable" runat="server" ID="btnNotApplicable" OnClick="btnNotApplicable_Click"  Style="margin-left: 15px;" ValidationGroup="ComplianceValidationGroup" CssClass="btn btn-search" />
                                                    <asp:Button Text="Close" runat="server" ID="btnCancel2" OnClick="btnCancel_Click" Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" />
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="AuditLog2" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog2">
                                                    <h2>Audit Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog2"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseAuditLog2" class="collapse">
                                            <div runat="server" id="log" style="text-align: left;">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionHistory2" AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory2_OnPageIndexChanging"
                                                            OnRowCreated="grdTransactionHistory2_RowCreated" BorderWidth="0px" OnSorting="grdTransactionHistory2_Sorting"
                                                            DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory2_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString("dd-MM-yyyy") : ""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </fieldset>
                                                <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                                                <div></div>
                                                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                                <asp:Button ID="btnDownload2" runat="server" Style="display: none" OnClick="btnDownload2_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload2" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample2" />
                    <asp:PostBackTrigger ControlID="btnSave2" />
                    <asp:PostBackTrigger ControlID="UploadDocument" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div>
            <div class="modal fade" id="modalDocumentInternalChecklistViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptInternalChecklistVersionView" runat="server" OnItemCommand="rptInternalChecklistVersionView_ItemCommand">
                                                                <HeaderTemplate>
                                                                    <table id="tblComplianceDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblInternalChecklistDocumentVersionView"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblInternalChecklistDocumentVersionView" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptInternalChecklistVersionView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                    <asp:Label runat="server" ID="lblMessageInternalChecklist" Style="color: red;"></asp:Label>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docInternalChecklistViewer" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="SampleFilePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <%-- data-dismiss-modal="modal2"--%>
                        <button type="button" class="close" onclick="$('#SampleFilePopUp').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <%-- <div style="width: 100%;">--%>
                        <div style="float: left; width: 10%">
                            <table width="100%" style="text-align: left; margin-left: 5%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdatleMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptComplianceSampleView" runat="server" OnItemCommand="rptComplianceSampleView_ItemCommand"
                                                        OnItemDataBound="rptComplianceSampleView_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Sample Forms</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView"
                                                                                runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="float: left; width: 90%">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <div class="modal fade" id="DocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: -3%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="$('#DocumentPriview').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docPriview" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="/newjs/jquery.bxslider.js"></script>
        <script type="text/javascript" src="/newjs/bootstrap.min.js"></script>
        <!-- charts scripts -->
        <script type="text/javascript" src="../assets/jquery-knob/js/jquery.knob.js"></script>
        <script type="text/javascript" src="/newjs/owl.carousel.js"></script>
        <!-- jQuery full calendar -->
        <script type="text/javascript" src="/newjs/fullcalendar.min.js"></script>
        <!--script for this page only-->
        <script type="text/javascript" src="/newjs/calendar-custom.js"></script>
        <script type="text/javascript" src="/newjs/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script type="text/javascript" src="/newjs/jquery.customSelect.min.js"></script>
        <!--custome script for all page-->
        <%-- <script type="text/javascript" src="/newjs/scripts.js"></script>--%>
    </form>
</body>
</html>

