﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class checkliststatusperformer : System.Web.UI.Page
    {

        static string sampleFormPath = "";
        public static string subTaskDocViewPath = "";
        public static List<long> Tasklist = new List<long>();
        protected static string ActDocString;
        protected static bool IsRemarkCompulsary = false;
        public static string ChecklistTaskDocViewPath = "";
        protected static bool IsDocumentCompulsary = false;
        protected string UploadDocumentLink;
        bool IsNotCompiled = false;
        protected static string KendoPath;
        protected static int CustId;
        protected static int UId;
        protected static int compInstanceID;
        protected static string Authorization;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
                string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
                Authorization = (string)HttpContext.Current.Cache[CacheName];
                if (Authorization == null)
                {
                    Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                    HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }
                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";
                else
                    UploadDocumentLink = "False";


                // STT Change- Add Status
                string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                long customerID = AuthenticationHelper.CustomerID;
                List<string> NotCompliedCustIDList = customer.Split(',').ToList();
                if (NotCompliedCustIDList.Count > 0)
                {
                    foreach (string PList in NotCompliedCustIDList)
                    {
                        if (PList == customerID.ToString())
                        {
                            IsNotCompiled = true;
                            break;
                        }
                    }
                }
                if (IsNotCompiled == true)
                {
                    btnNotComplied.Visible = true;
                }
                KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                compInstanceID = Convert.ToInt32(Request.QueryString["InId"].ToString());
                int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                int instanceId = Convert.ToInt32(Request.QueryString["InId"].ToString());
                OpenTransactionPage(scheduleOnId, instanceId);
                Labellockingmsg.Visible = false;
                string listLockingCustomer = ConfigurationManager.AppSettings["LockingDaysCustomerlist"];
                string[] listLockCust = new string[] { };
                if (!string.IsNullOrEmpty(listLockingCustomer))
                    listLockCust = listLockingCustomer.Split(',');

                if (listLockCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                {
                    if (Business.ComplianceManagement.GetCurrentStatusByScheduleONID(Convert.ToInt32(Request.QueryString["sId"].ToString()), "S", Convert.ToInt32(AuthenticationHelper.CustomerID)))
                    {
                        Labellockingmsg.Visible = true;
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        Labellockingmsg.Visible = false;
                        btnSave.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                lblPenalty.Text = string.Empty;
                lblRisk.Text = string.Empty;
                lbDownloadSample.Text = string.Empty;

                //   var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var AllComData = (from row in entities.ComplianceScheduleOns
                                      join row1 in entities.ComplianceInstances
                                        on row.ComplianceInstanceID equals row1.ID

                                      join CB in entities.CustomerBranches
                                      on row1.CustomerBranchID equals CB.ID
                                      where row.ID == ScheduledOnID
                                      select new
                                      {
                                          row.ForPeriod,
                                          row.ForMonth,
                                          CB.Name,
                                          row.ScheduleOn,
                                          row.ActualScheduleon,
                                      }).FirstOrDefault();
                    if (AllComData != null)
                    {
                        lblLocation.Text = AllComData.Name;
                        lblDueDate.Text = Convert.ToDateTime(AllComData.ScheduleOn).ToString("dd-MMM-yyyy");
                        lblPeriod.Text = AllComData.ForMonth;
                        if (AllComData.ActualScheduleon !=null)
                        {
                            TRactualduedate.Visible = true;
                            lblActualDueDate.Text = Convert.ToDateTime(AllComData.ActualScheduleon).ToString("dd-MMM-yyyy");
                        }                       
                    }
                }

                grdDocument.DataSource = null;
                grdDocument.DataBind();
                BindTempDocumentData(ScheduledOnID);
                ViewState["ScheduledOnID"] = ScheduledOnID;
                var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
                var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                var complinaceinstance = Business.ComplianceManagement.GetBranchLocation(complianceInstanceID);
                var customerbranchID = complinaceinstance.CustomerBranchID;
                var showHideButton = BindSubTasks(ScheduledOnID, 3, customerbranchID);
                //btnSave.Enabled = showHideButton;

                long CustomerID = 0;
                CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (CustomerID == 691) // Customer All Time Plastic
                {
                    var IsData = Business.ComplianceManagement.GetStatutorycomplianceByType(complianceInfo.ID, CustomerID);

                    if (IsData == true)
                    {
                        IsDocumentCompulsary = true;
                    }
                    else
                    {
                        IsDocumentCompulsary = false;
                    }
                }
                else
                {
                    IsDocumentCompulsary = false;
                }

                ViewState["complianceInstanceID"] = complinaceinstance.ID;
                if (complianceInfo != null)
                {

                    lblComplianceID.Text = Convert.ToString(complianceInfo.ID);
                    lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
                    lblComplianceDiscription.Text = complianceInfo.ShortDescription;
                    lblDetailedDiscription.Text = complianceInfo.Description;
                    lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
                    //lblRefrenceText.Text = complianceInfo.ReferenceMaterialText;
                    lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
                    //string Penalty = Business.ComplianceManagement.GetPanalty(complianceInfo);
                    lblPenalty.Text = complianceInfo.PenaltyDescription;
                    string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceInstanceID);
                    lblRiskType.Text = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);
                    lblRisk.Text = risk;
                    var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
                    if (ActInfo != null)
                    {
                        lblActName.Text = ActInfo.Name;
                    }

                    if (risk == "HIGH")
                    {
                        divRiskType.Attributes["style"] = "background-color:red;";
                    }
                    else if (risk == "MEDIUM")
                    {
                        divRiskType.Attributes["style"] = "background-color:yellow;";
                    }
                    else if (risk == "LOW")
                    {
                        divRiskType.Attributes["style"] = "background-color:green;";
                    }
                    else if (risk == "CRITICAL")
                    {
                        divRiskType.Attributes["style"] = "background-color:#CC0900;";
                    }
                    lblRule.Text = complianceInfo.Sections;
                    lblFormNumber.Text = complianceInfo.RequiredForms;
                }

                var AuditChecklistName = Business.ComplianceManagement.GetAuditChecklistName(complianceInfo.ID,Convert.ToInt32(AuthenticationHelper.CustomerID));

                if (!string.IsNullOrEmpty(AuditChecklistName))
                {
                    lblAuditChecklist.Text = AuditChecklistName;
                    trAuditChecklist.Visible = true;
                }
                else
                {
                    trAuditChecklist.Visible = false;
                }
                if (RecentComplianceTransaction.ComplianceStatusID == 10)
                {
                    //rfvFile.Visible = false;
                    BindDocument(ScheduledOnID);
                }
                else
                {
                    //rfvFile.Visible = true;
                    divDeleteDocument.Visible = false;
                    rptComplianceDocumnets.DataSource = null;
                    rptComplianceDocumnets.DataBind();

                    rptWorkingFiles.DataSource = null;
                    rptWorkingFiles.DataBind();
                }

                if (complianceInfo.UploadDocument == true && complianceForm != null)
                {
                    // lbDownloadSample.Text = "<u style=color:blue>Click here</u> to download sample form.";
                    lbDownloadSample.Text = "Download";
                    lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                    sampleFormPath = complianceForm.FilePath;
                    sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2);

                    lblNote.Visible = true;
                    lnkViewSampleForm.Visible = true;
                    lblSlash.Visible = true;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        lblpathsample.Text = sampleFormPath;
                    }
                    else
                    {
                        lblpathsample.Text = sampleFormPath;
                    }

                    var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample.CommandArgument));

                    rptComplianceSampleView.DataSource = complianceform;
                    rptComplianceSampleView.DataBind();
                }
                else
                {
                    lblNote.Visible = false;
                    lblSlash.Visible = false;
                    lnkViewSampleForm.Visible = false;
                    sampleFormPath = "";
                }

                btnSave.Attributes.Remove("disabled");

                if (RecentComplianceTransaction.ComplianceStatusID == 1 || RecentComplianceTransaction.ComplianceStatusID == 6 || RecentComplianceTransaction.ComplianceStatusID == 10)
                {
                    if (complianceInfo.EventFlag == true)
                    {
                        if (complianceInfo.UpDocs == true)
                        {
                            divUploadDocument.Visible = true;
                            //divWorkingfiles.Visible = true;
                            //btnSave.Visible = true;
                            //btnSaveDOCNotCompulsory.Visible = false;
                            lblDocComplasary.Visible = true;
                        }
                        else
                        {
                            //rfvFile.Visible = false;
                            //btnSaveDOCNotCompulsory.Visible = true;
                            //btnSave.Visible = false;
                            divUploadDocument.Visible = true;
                            //divWorkingfiles.Visible = true;
                            lblDocComplasary.Visible = false;
                        }
                    }
                    else
                    {
                        divUploadDocument.Visible = true;
                        //divWorkingfiles.Visible = true;
                        // btnSave.Visible = true;
                        // btnSaveDOCNotCompulsory.Visible = false;
                        lblDocComplasary.Visible = true;
                    }
                    //rfvFile.Enabled = (complianceInfo.UploadDocument ?? false);
                }
                else if (RecentComplianceTransaction.ComplianceStatusID != 1)
                {
                    divUploadDocument.Visible = false;
                    //divWorkingfiles.Visible = false;
                    if ((complianceInfo.UploadDocument ?? false))
                    {
                        btnSave.Attributes.Add("disabled", "disabled");
                    }
                }

                //BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));
                BindTransactions(ScheduledOnID);
                tbxRemarks.Text = string.Empty;
                hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                #region Act_Document
                var actChkPfDocVersionData = ActManagement.getFileNamebyID(complianceInfo.ActID);
                rptActChkPfDocVersion.DataSource = actChkPfDocVersionData;
                rptActChkPfDocVersion.DataBind();
                #endregion

                upComplianceDetails.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindDocument(int ScheduledOnID)
        {
            try
            {
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                divDeleteDocument.Visible = true;
                ComplianceDocument = DocumentManagement.GetFileData1(ScheduledOnID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = ComplianceDocument.Where(entry => entry.FileType == 2).ToList();
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["ScheduledOnID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private void BindStatusList(int statusID)
        //{
        //    try
        //    {
        //        ddlStatus.DataSource = null;
        //        ddlStatus.DataBind();
        //        ddlStatus.ClearSelection();

        //        ddlStatus.DataTextField = "Name";
        //        ddlStatus.DataValueField = "ID";

        //        var statusList = ComplianceStatusManagement.GetStatusList();

        //        List<ComplianceStatu> allowedStatusList = null;

        //        List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
        //        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

        //        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry=>entry.Name).ToList();

        //        ddlStatus.DataSource = allowedStatusList;
        //        ddlStatus.DataBind();

        //        ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
            try
            {
                Boolean isDocument = false;
                if (IsDocumentCompulsary == true)
                {
                    // Document mandatory
                    if (grdDocument.Rows.Count > 0)
                    {
                        for (int i = 0; i < grdDocument.Rows.Count; i++)
                        {
                            Label lblDocType = (Label)grdDocument.Rows[i].FindControl("lblDocType");
                            if (lblDocType.Text == "Compliance Document")
                            {
                                isDocument = true;
                            }
                        }
                    }
                    else
                    {
                        isDocument = false;
                    }
                }
                else
                {
                    // Document not mandatory
                    isDocument = true;
                }

                if (isDocument == true)
                {
                    long? StatusID = ComplianceManagement.Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID;

                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = 4, // Convert.ToInt32(ddlStatus.SelectedValue),
                        StatusChangedOn = DateTime.Now, // DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks.Text
                        //Remarks = "Closed Timely"
                    };


                    List<FileData> files = new List<FileData>();
                    List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                    HttpFileCollection fileCollection = Request.Files;
                    bool blankfileCount = true;

                    var TempDocData = Business.DocumentManagement.GetTempCheckListComplianceDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                    string directoryPath = null;
                    if (TempDocData.Count > 0)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                            var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                           
                            string version = null;
                            if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                            {
                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                directoryPath = "avacomdocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;

                            }
                            else
                            {
                                version = "1.0";
                                directoryPath = "avacomdocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                            }

                            foreach (var item in TempDocData)
                            {
                                if (item.ISLink)
                                {
                                    String fileName = "";
                                    if (item.DocType == "C")
                                    {
                                        fileName = "ComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    FileData file = new FileData()
                                    {
                                        Name = fileName,
                                        FilePath = item.DocPath,
                                        Version = version,
                                        VersionDate = DateTime.Now,
                                        ISLink = true
                                    };

                                    files.Add(file);
                                }
                                else
                                {
                                    String fileName = "";
                                    if (item.DocType == "C")
                                    {
                                        fileName = "ComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }


                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));


                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                    if (item.DocData.Length > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                        filepathvalue = vale.Replace(@"\", "/");

                                        FileData file = new FileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            //Version = "1.0",
                                            Version = version,
                                            VersionDate = DateTime.UtcNow,
                                        };

                                        files.Add(file);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(item.DocName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                            var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                            string version = null;
                            if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                            {
                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + version);
                                }
                            }
                            else
                            {
                                version = "1.0";
                                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                                }
                            }

                            DocumentManagement.CreateDirectory(directoryPath);

                            foreach (var item in TempDocData)
                            {
                                if (item.ISLink)
                                {
                                    String fileName = "";
                                    if (item.DocType == "C")
                                    {
                                        fileName = "ComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    FileData file = new FileData()
                                    {
                                        Name = fileName,
                                        FilePath = item.DocPath,
                                        Version = version,
                                        VersionDate = DateTime.Now,
                                        ISLink = true
                                    };

                                    files.Add(file);
                                }
                                else
                                {
                                    String fileName = "";
                                    if (item.DocType == "C")
                                    {
                                        fileName = "ComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }


                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));


                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                    if (item.DocData.Length > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }
                                        FileData file = new FileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            //Version = "1.0",
                                            Version = version,
                                            VersionDate = DateTime.UtcNow,
                                        };

                                        files.Add(file);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(item.DocName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            #endregion
                        }
                    }


                    bool flag = false;
                    bool flag1 = false;
                    if (blankfileCount)
                    {
                        flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist, directoryPath, Convert.ToInt64(hdnComplianceScheduledOnId.Value), CustId);

                        try
                        {
                            foreach (var item in TempDocData)
                            {
                                if (item.ISLink == false)
                                {
                                    string path = Server.MapPath(item.DocPath);
                                    FileInfo file = new FileInfo(path);
                                    if (file.Exists) //check file exsit or not
                                    {
                                        file.Delete();
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                        
                        flag1 = Business.ComplianceManagement.DeleteTempDocumentChecklistFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please do not upload blank files.";
                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload blank files.')", true);
                    }

                    //bool flag = true;
                    if (flag != true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    try
                    {
                        Business.ComplianceManagement.BindStatutoryCheckListDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                    }
                    catch { }
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('save successfully')", true);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select documents for upload.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.Data); // create the file
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {

                var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var filex in complianceform)
                    {
                        if (filex.FilePath != null)
                        {
                            string[] filename = filex.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            ComplianceZip.AddEntry(str, Business.DocumentManagement.ReadDocFiles(Server.MapPath(filex.FilePath)));
                            i++;
                        }
                    }

                    string fileName = "ComplianceID_" + complianceID1 + "_SampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }

                var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));

                //Response.Buffer = true;
                //Response.Clear();
                //Response.ContentType = "application/octet-stream";
                //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.FileData); // create the file
                //Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //protected void lbDownloadSample_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));

        //        Response.Buffer = true;
        //        Response.Clear();
        //        Response.ContentType = "application/octet-stream";
        //        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
        //        Response.BinaryWrite(file.FileData); // create the file
        //        Response.Flush(); // send it to the client to download
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {

                //DateTime date = DateTime.MinValue;
                //if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", string.Format("initializeDatePickerforPerformer1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", "initializeDatePickerforPerformer1(null);", true);
                //}

                // ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {

                //tbxRemarks.Text = tbxDate.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            try
            {
                HttpFileCollection fileCollection = Request.Files;
                for (int i = 0; i < fileCollection.Count; i++)
                {
                    HttpPostedFile uploadfile = fileCollection[i];
                    string fileName = Path.GetFileName(uploadfile.FileName);
                    if (uploadfile.ContentLength > 0)
                    {
                        //uploadfile.SaveAs(Server.MapPath("~/UploadFiles/") + fileName);
                        //lblMessage.Text += fileName + "Saved Successfully<br>";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {

                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file != null)
                {
                    string path = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                        path = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                        // path = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                    }
                    else
                    {
                        path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    }
                    DocumentManagement.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkDocbutton = (LinkButton)e.Item.FindControl("lbtLinkDocbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);

            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkbutton = (LinkButton)e.Item.FindControl("lbtLinkbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkbutton);
            }
        }

        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm.Text != "")
                {
                    string url = lnkSampleForm.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkViewSampleForm_Click(object sender, EventArgs e)
        {

        }

        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            if (taskDocument.Count > 0)
                            {
                                string fileName = string.Empty;

                                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                Label lblTaskTitle = null;

                                if (row != null)
                                {
                                    lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                    if (lblTaskTitle != null)
                                        fileName = lblTaskTitle.Text + "-Documents";

                                    if (fileName.Length > 250)
                                        fileName = "TaskDocuments";
                                }

                                ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                int i = 0;
                                foreach (var eachFile in taskDocument)
                                {
                                    //comment by rahul on 20 JAN 2017
                                    string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                    //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                    if (eachFile.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = eachFile.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in taskDocumenttoView)
                                {
                                    rptChecklistVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptChecklistVersionView.DataBind();

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        bw.Close();

                                        ChecklistTaskDocViewPath = FileName;
                                        ChecklistTaskDocViewPath = ChecklistTaskDocViewPath.Substring(2, ChecklistTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + ChecklistTaskDocViewPath + "');", true);
                                        lblMessageChecklist.Text = "";
                                        UpdatePanel4.Update();
                                    }
                                    else
                                    {
                                        lblMessageChecklist.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptChecklistVersionView.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                rptChecklistVersionView.DataBind();

                                foreach (var file in taskDocumenttoView)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        bw.Close();

                                        ChecklistTaskDocViewPath = FileName;
                                        ChecklistTaskDocViewPath = ChecklistTaskDocViewPath.Substring(2, ChecklistTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + ChecklistTaskDocViewPath + "');", true);
                                        lblMessageChecklist.Text = "";
                                        //UpdatePanel4.Update();
                                    }
                                    else
                                    {
                                        lblMessageChecklist.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                btnSubTaskDocDownload.Visible = true;
                                lblSlashReview.Visible = true;
                                btnSubTaskDocView.Visible = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                        where row.ParentID == null
                                        //&& row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        divTask.Visible = true;
                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }

        protected void rptComplianceSampleView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    ComplianceForm CMPDocuments = Business.ComplianceManagement.GetSelectedComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

                    if (CMPDocuments != null)
                    {
                        string fullfilePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath));
                        string filePath = CMPDocuments.FilePath;
                        string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                        if (CMPDocuments.FilePath != null && File.Exists(fullfilePath))
                        {
                            string extension = System.IO.Path.GetExtension(CompDocPath);

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fram();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fram('" + CompDocPath + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fram();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView);
            }
        }


        protected void rptActChkPfDocVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                string version = Convert.ToString(commandArgs[1]);
                int actID = Convert.ToInt32(commandArgs[0]);
                lblMessageChkPfAct.Text = string.Empty;
                List<Sp_Act_Document_Result> ActDocumentlst = new List<Sp_Act_Document_Result>();
                ActDocumentlst = ActManagement.getFileNamebyID(Convert.ToInt32(actID)).ToList();

                if (e.CommandName.Equals("Download"))
                {
                    #region Download                   
                    var files = ActDocumentlst.Where(entry => entry.Version == version).ToList();
                    if (files.Count > 0)
                    {
                        foreach (var file in files)
                        {
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename= " + file.FileName);
                            Response.TransmitFile(Server.MapPath(file.FilePath));
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                    }
                    #endregion
                }
                else if (e.CommandName.Equals("View"))
                {
                    #region View
                    var ActDocument = ActDocumentlst.Where(entry => entry.Version == version).ToList();
                    if (ActDocument.Count > 0)
                    {
                        foreach (var file in ActDocument)
                        {
                            string filePath = Server.MapPath(file.FilePath);

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                rptActChkPfDocVersionView.DataSource = ActDocumentlst.OrderBy(entry => entry.Version);
                                rptActChkPfDocVersionView.DataBind();
                                UpdatePanel6.Update();

                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z" || extension.ToUpper() == ".RAR")
                                {
                                    lblMessageChkPfAct.Text = extension.ToUpper().Trim() + " file can't view please download it";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileChkPfActPopUp();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    bw.Write(DocumentManagement.ReadDocFiles(filePath));
                                    bw.Close();

                                    FileName = FileName.Substring(2, FileName.Length - 2);
                                    lblMessageChkPfAct.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileChkPfAct('" + FileName + "');", true);

                                }
                            }
                            else
                            {
                                lblMessageChkPfAct.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileChkPfActPopUp();", true);
                                break;
                            }
                        }
                    }
                    #endregion
                }
                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void rptActChkPfDocVersion_ItemDataBound(object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton btnActChkPfVersionDoc = (LinkButton)e.Item.FindControl("btnActChkPfVersionDoc");
                scriptManager.RegisterPostBackControl(btnActChkPfVersionDoc);
            }
        }


        protected void rptActChkPfDocVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                string version = Convert.ToString(commandArgs[1]);
                int actID = Convert.ToInt32(commandArgs[0]);
                lblMessageChkPfAct.Text = string.Empty;
                List<Sp_Act_Document_Result> ActDocumentlst = new List<Sp_Act_Document_Result>();
                ActDocumentlst = ActManagement.getFileNamebyID(Convert.ToInt32(actID)).ToList();

                if (e.CommandName.Equals("View"))
                {
                    #region View
                    var ActDocument = ActDocumentlst.Where(entry => entry.Version == version).ToList();
                    if (ActDocument.Count > 0)
                    {
                        foreach (var file in ActDocument)
                        {
                            string filePath = Server.MapPath(file.FilePath);

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z" || extension.ToUpper() == ".RAR")
                                {
                                    lblMessageChkPfAct.Text = extension.ToUpper().Trim() + " file can't view please download it";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileChkPfActPopUp();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    bw.Write(DocumentManagement.ReadDocFiles(filePath));
                                    bw.Close();

                                    FileName = FileName.Substring(2, FileName.Length - 2);
                                    lblMessageChkPfAct.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileChkPfAct('" + FileName + "');", true);

                                }
                            }
                            else
                            {
                                lblMessageChkPfAct.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileChkPfActPopUp();", true);
                                break;
                            }
                        }
                    }
                    #endregion
                }
                UpdatePanel6.Update();
                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptActChkPfDocVersionView_ItemDataBound(object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblActChkPfDocumentVersionView = (LinkButton)e.Item.FindControl("lblActChkPfDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblActChkPfDocumentVersionView);
            }
        }

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
                long complianceInstanceID = Convert.ToInt64(ViewState["ScheduledOnID"]);

                TempComplianceDocument tempComplianceDocument = null;
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    #region file upload
                    bool isBlankFile = false;
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        int filelength = uploadfile.ContentLength;
                        string fileName = Path.GetFileName(uploadfile.FileName);
                        if (filelength == 0 && fileName != "")
                        {
                            isBlankFile = true;
                        }
                    }
                    if (isBlankFile == false)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string directoryPath = null;

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string[] keys = fileCollection.Keys[i].Split('$');
                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/StatutoryCheckList/");
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                string finalPath = Path.Combine(directoryPath, fileName);
                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                tempComplianceDocument = new TempComplianceDocument()
                                {
                                    ScheduleOnID = ScheduledOnID,
                                    ComplianceInstanceID = complianceInstanceID,
                                    DocPath = finalPath,
                                    DocData = bytes,
                                    DocName = fileCollection[i].FileName,
                                };

                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    tempComplianceDocument.DocType = "C";
                                }

                                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                                if (_objTempDocumentID > 0)
                                {
                                    BindTempDocumentData(ScheduledOnID);
                                }

                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please do not upload blank files.";
                    }
                    #endregion
                }

                BindTempDocumentData(ScheduledOnID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTempDocumentData(long ScheduledOnID)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                var DocData = DocumentManagement.GetTempCheckListComplianceDocumentData(ScheduledOnID);
                if (DocData.Count > 0)
                {
                    grdDocument.Visible = true;
                    grdDocument.DataSource = DocData;
                    grdDocument.DataBind();
                }
                else
                {
                    grdDocument.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                            BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                            if (file != null)
                            {
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                Response.TransmitFile(Server.MapPath(file.DocPath));
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.DocPath));
                            if (file.DocName != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    string CompDocReviewPath = file.DocPath;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fCalltreeCollapsed();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }
        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblRedirectDocumentTrue = (Label)e.Row.FindControl("lblRedirectDocument");
                    Label lblIsLinkTrue = (Label)e.Row.FindControl("lblIsLinkTrue");

                    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

                    if (lblDocType.Text.Trim() == "C")
                    {
                        lblDocType.Text = "Compliance Document";
                    }

                    LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadDocument");
                    LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewDocument");
                    LinkButton lnkRedirectDocument = (LinkButton)e.Row.FindControl("lnkRedierctDocument");
                    //LinkButton lnkDeleteDocument = (LinkButton)e.Row.FindControl("lnkDeleteDocument");

                    if (lblIsLinkTrue.Text == "True")
                    {
                        lnkDownloadDocument.Visible = false;
                        lnkViewDocument.Visible = false;
                        lnkRedirectDocument.Visible = true;
                        lblRedirectDocumentTrue.Visible = false;
                    }
                    else
                    {
                        lnkDownloadDocument.Visible = true;
                        lnkViewDocument.Visible = true;
                        lnkRedirectDocument.Visible = false;
                        lblRedirectDocumentTrue.Visible = true;
                    }
                }
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

                //    if (lblDocType.Text.Trim() == "C")
                //    {
                //        lblDocType.Text = "Compliance Document";
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnNotApplicable_Click(object sender, EventArgs e)
        { 
            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
            try
            {
                long? StatusID = ComplianceManagement.Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID;
                ComplianceTransaction transaction = new ComplianceTransaction()
                {
                    ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                    ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = 15,
                    StatusChangedOn = DateTime.Now,
                    Remarks = "Not Applicable"
                };

                List<FileData> files = new List<FileData>();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                HttpFileCollection fileCollection = Request.Files;
                bool blankfileCount = true;

                var TempDocData = Business.DocumentManagement.GetTempCheckListComplianceDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));

                if (TempDocData.Count > 0)
                {
                    int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                    var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                    string directoryPath = null;
                    string version = null;
                    if (StatusID == 6)
                    {
                        version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + version;
                        }
                        else
                        {
                            directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + version);
                        }
                    }
                    else
                    {
                        version = "1.0";
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                        }
                        else
                        {
                            directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                        }
                    }

                    DocumentManagement.CreateDirectory(directoryPath);

                    foreach (var item in TempDocData)
                    {
                        String fileName = "";
                        if (item.DocType == "C")
                        {
                            fileName = "ComplianceDoc_" + item.DocName;
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                        }


                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));


                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                        if (item.DocData.Length > 0)
                        {
                            string filepathvalue = string.Empty;
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {
                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                filepathvalue = vale.Replace(@"\", "/");
                            }
                            else
                            {
                                filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            }
                            FileData file = new FileData()
                            {
                                Name = fileName,
                                FilePath = filepathvalue,
                                FileKey = fileKey.ToString(),
                                Version = version,
                                VersionDate = DateTime.UtcNow,
                            };

                            files.Add(file);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item.DocName))
                                blankfileCount = false;
                        }
                    }
                }

                bool flag = false;
                bool flag1 = false;
                if (blankfileCount)
                {
                    flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                    flag1 = Business.ComplianceManagement.DeleteTempDocumentChecklistFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please do not upload blank files.";
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload blank files.')", true);
                }

                if (flag != true)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                try
                {
                    Business.ComplianceManagement.BindStatutoryCheckListDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                }
                catch { }
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Not applicable save successfully')", true);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void Uploadlingchecklistfile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["ScheduledOnID"]);

            string url = TxtChecklistDocument.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(TxtChecklistDocument.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };   
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "C",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
            TxtChecklistDocument.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);

        }

        protected void btnNotComplied_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
            try
            {
                long? StatusID = ComplianceManagement.Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID;
                ComplianceTransaction transaction = new ComplianceTransaction()
                {
                    ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                    ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = 17,
                    StatusChangedOn = DateTime.Now,
                    Remarks = "Not Complied"
                };

                List<FileData> files = new List<FileData>();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                HttpFileCollection fileCollection = Request.Files;
                bool blankfileCount = true;

                var TempDocData = Business.DocumentManagement.GetTempCheckListComplianceDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));

                if (TempDocData.Count > 0)
                {
                    int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                    var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                    string directoryPath = null;
                    string version = null;
                    if (StatusID == 6)
                    {
                        version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + version;
                        }
                        else
                        {
                            directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + version);
                        }
                    }
                    else
                    {
                        version = "1.0";
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                        }
                        else
                        {
                            directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                        }
                    }

                    DocumentManagement.CreateDirectory(directoryPath);

                    foreach (var item in TempDocData)
                    {
                        String fileName = "";
                        if (item.DocType == "C")
                        {
                            fileName = "ComplianceDoc_" + item.DocName;
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                        }


                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));


                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                        if (item.DocData.Length > 0)
                        {
                            string filepathvalue = string.Empty;
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {
                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                filepathvalue = vale.Replace(@"\", "/");
                            }
                            else
                            {
                                filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            }
                            FileData file = new FileData()
                            {
                                Name = fileName,
                                FilePath = filepathvalue,
                                FileKey = fileKey.ToString(),
                                Version = version,
                                VersionDate = DateTime.UtcNow,
                            };

                            files.Add(file);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item.DocName))
                                blankfileCount = false;
                        }
                    }
                }

                bool flag = false;
                bool flag1 = false;
                if (blankfileCount)
                {
                    flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                    flag1 = Business.ComplianceManagement.DeleteTempDocumentChecklistFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                }
                try
                {
                    Business.ComplianceManagement.BindStatutoryCheckListDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                }
                catch { }
                if (flag != true)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}