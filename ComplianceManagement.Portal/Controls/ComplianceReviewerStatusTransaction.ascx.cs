﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Dynamic;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using System.Configuration;
using System.Net;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class ComplianceReviewerStatusTransaction : System.Web.UI.UserControl
    {
        static string sampleFormPath1 = "";
        public static string CompDocReviewPath = "";
        public static string TaskDocViewPath = "";

        protected bool IsPenaltyVisible = true;
        protected bool IsRemarkCompulsary2 = false;
        protected static string ActReviewDocString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) { }
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                lblPenalty1.Text = string.Empty;
                lblRisk1.Text = string.Empty;
                lbDownloadSample1.Text = string.Empty;
                hdnfieldPanaltyDisplay.Value = "true";
                ViewState["complianceInstanceID"] = ScheduledOnID;
                int customerID = -1;
                int customerbranchID = -1;

                var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);
                var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
                var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                if (AllComData != null)
                {
                    customerbranchID = AllComData.CustomerBranchID;
                    lblLocation.Text = AllComData.Branch;
                    lblDueDate.Text = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MMM-yyyy");
                    hiddenReviewDueDate.Value = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MM-yyyy");
                    lblPeriod.Text = AllComData.ForMonth;
                }
                var showHideButton = BindSubTasks(ScheduledOnID, 4, customerbranchID);

                if (complianceInfo != null)
                {
                    lblComplianceID1.Text = Convert.ToString(complianceInfo.ID);
                    lnkSampleForm1.Text = Convert.ToString(complianceInfo.SampleFormLink);
                    lblFrequency1.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
                    // lblRefrenceText1.Text = complianceInfo.ReferenceMaterialText;
                    lblRefrenceText1.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
                    lblComplianceDiscription1.Text = complianceInfo.ShortDescription;
                    lblDetailedDiscription1.Text = complianceInfo.Description;
                    // string Penalty = Business.ComplianceManagement.GetPanalty(complianceInfo);
                    lblPenalty1.Text = complianceInfo.PenaltyDescription;
                    string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceInstanceID);
                    lblRisk1.Text = risk;
                    lblRiskType1.Text = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);

                    if (risk == "HIGH")
                    {
                        divRiskType1.Attributes["style"] = "background-color:red;";
                    }
                    else if (risk == "MEDIUM")
                    {
                        divRiskType1.Attributes["style"] = "background-color:yellow;";
                    }
                    else if (risk == "LOW")
                    {
                        divRiskType1.Attributes["style"] = "background-color:green;";
                    }

                    var AuditChecklistName = Business.ComplianceManagement.GetAuditChecklistName(complianceInfo.ID,Convert.ToInt32(AuthenticationHelper.CustomerID));

                    if (!string.IsNullOrEmpty(AuditChecklistName))
                    {
                        lblAuditChecklist1.Text = AuditChecklistName;
                        trAuditChecklist1.Visible = true;
                    }
                    else
                    {
                        trAuditChecklist1.Visible = false;
                    }

                    var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
                    if (ActInfo != null)
                    {
                        lblActName1.Text = ActInfo.Name;
                    }
                    if(RecentComplianceTransaction.IsPenaltySave ==false && RecentComplianceTransaction.Status == "Complied Delayed but pending review")
                    {

                        //Intrest and penalty save
                        // fieldsetpenalty.Visible = true;
                        //fieldsetpenalty.Attributes.Add("style", "display:block");
                        chkPenaltySaveReview.Checked = (bool) RecentComplianceTransaction.IsPenaltySave;
                        txtInterestReview.Text = Convert.ToString(RecentComplianceTransaction.Interest);
                        txtPenaltyReview.Text =Convert.ToString(RecentComplianceTransaction.Penalty);
                    }
                    else
                    {
                        if (RecentComplianceTransaction.IsPenaltySave == null)
                        {
                            //Earlier compliance performed 
                            chkPenaltySaveReview.Checked = false;
                            //fieldsetpenalty.Visible = true;
                           // fieldsetpenalty.Attributes.Add("style", "display:block");
                            txtInterestReview.Text = "";
                            txtPenaltyReview.Text = "";
                        }
                        else
                        {
                            if (RecentComplianceTransaction.Status == "Complied but pending review")
                            {
                                //performed in Time
                                chkPenaltySaveReview.Checked = false;
                                //fieldsetpenalty.Visible = false;
                                // fieldsetpenalty.Attributes.Add("style", "display:none");
                                txtInterestReview.Text = "0";
                                txtPenaltyReview.Text = "0";
                            }
                            else if (RecentComplianceTransaction.Status == "Submitted For Interim Review")
                            {
                                //performed Interim
                                chkPenaltySaveReview.Checked = false;
                                //fieldsetpenalty.Visible = false;
                                //fieldsetpenalty.Attributes.Add("style", "display:none");
                                txtInterestReview.Text = "0";
                                txtPenaltyReview.Text = "0";
                            }
                            else
                            {
                                //Penalty and intrest not known this moment
                                chkPenaltySaveReview.Checked = (bool) RecentComplianceTransaction.IsPenaltySave;
                                //fieldsetpenalty.Visible = true;
                                //fieldsetpenalty.Attributes.Add("style", "display:block");
                                txtInterestReview.Text = "0";
                                txtPenaltyReview.Text = "0";
                                //txtInterestReview.ReadOnly = true;
                                //txtPenaltyReview.ReadOnly = true;
                                txtInterestReview.Attributes.Add("disabled", "disabled");
                                txtPenaltyReview.Attributes.Add("disabled", "disabled");
                            }
                        }
                    }
                    var data = Business.ComplianceManagement.CheckMonetary(Convert.ToInt32(lblComplianceID1.Text));
                    if (data == null)
                    {
                        fieldsetpenalty.Visible = false;
                    }
                    else
                    {
                        if (data.NonComplianceType == 1)
                        {
                            fieldsetpenalty.Visible = false;
                        }
                        else
                        {
                            // Penalty visible for customer
                            string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                            List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                            if (PenaltyNotDisplayCustomerList.Count > 0)
                            {                                
                                foreach (string PList in PenaltyNotDisplayCustomerList)
                                {
                                    if (PList == customerID.ToString())
                                    {
                                        IsPenaltyVisible = false;
                                        hdnfieldPanaltyDisplay.Value = "false";
                                        break;
                                    }
                                }
                            }
                            if (IsPenaltyVisible == true)
                            {
                                if (RecentComplianceTransaction.Status == "Complied Delayed but pending review")
                                {
                                    // fieldsetpenalty.Visible = true;
                                    fieldsetpenalty.Attributes.Add("style", "display:block");
                                }
                                else
                                {
                                    fieldsetpenalty.Attributes.Add("style", "display:none");
                                }
                            }
                            else
                            {
                                //fieldsetpenalty.Visible = false;
                                fieldsetpenalty.Attributes.Add("style", "display:none");
                            }
                            
                        }
                    }
                    lblNatureofcompliance.Text = "";
                    txtValueAsPerSystem.Text = "";
                    txtValueAsPerReturn.Text = "";
                    txtLiabilityPaid.Text = "";
                    txtDiffAB.Text = "";
                    txtDiffBC.Text = "";

                    if (complianceInfo.NatureOfCompliance != null)
                    {
                        lblNatureofcomplianceID.Text = Convert.ToString(complianceInfo.NatureOfCompliance);

                        string Nature = Business.ComplianceManagement.GetNatureOfComplianceFromID(Convert.ToInt32(complianceInfo.NatureOfCompliance));
                        lblNatureofcompliance.Text = Nature.ToString();

                        txtValueAsPerSystem.Text = Convert.ToString(RecentComplianceTransaction.ValuesAsPerSystem);
                        txtValueAsPerReturn.Text = Convert.ToString(RecentComplianceTransaction.ValuesAsPerReturn);
                        txtLiabilityPaid.Text = Convert.ToString(RecentComplianceTransaction.LiabilityPaid);

                        if (complianceInfo.NatureOfCompliance == 1)
                        {
                            fieldsetTDS.Visible = true;
                            //lblLiabilityPaid.Visible = true;
                            //txtLiabilityPaid.Visible = true;
                            //lbldiffBC.Visible = true;
                            //txtLiabilityPaid.Visible = true;
                            //txtDiffBC.Visible = true;
                            //tdLiabilityPaidSC.Visible = true;
                            //tddiffBCSC.Visible = true;
                            lblValueAsPerSystem.InnerText = "Liability as per system (A)";
                            lblValueAsPerReturn.InnerText = "Liability as per return (B)";
                            trreturn.Visible = true;

                            try
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Diff1", "Diff1();", true);
                            }
                            catch (Exception ex)
                            {
                            }
                         
                        }
                        else if (complianceInfo.NatureOfCompliance == 0)
                        {
                            fieldsetTDS.Visible = true;
                            lblValueAsPerSystem.InnerText = "Values as per system (A)";
                            lblValueAsPerReturn.InnerText = "Values as per return (B)";
                            trreturn.Visible = false;
                            //lblLiabilityPaid.Visible = false;
                            //txtLiabilityPaid.Visible = false;
                            //lbldiffBC.Visible = false;
                            //txtLiabilityPaid.Visible = false;
                            //tdLiabilityPaidSC.Visible = false;
                            //txtDiffBC.Visible = false;
                            //tddiffBCSC.Visible = false;

                            try
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Diff1", "Diff1();", true);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            fieldsetTDS.Visible = false;
                        }
                    }
                    else
                    {
                        fieldsetTDS.Visible = false;
                    }

                    lblRule1.Text = complianceInfo.Sections;
                    lblFormNumber1.Text = complianceInfo.RequiredForms;
                }

             

                if (complianceInfo.UploadDocument == true && complianceForm != null)
                {
                    //lbDownloadSample1.Text = "<u style=color:blue>Click here</u> to download sample form.";
                    lbDownloadSample1.Text = "Download";
                    lbDownloadSample1.CommandArgument = complianceForm.ComplianceID.ToString();

                    sampleFormPath1 = complianceForm.FilePath;
                    sampleFormPath1 = sampleFormPath1.Substring(2, sampleFormPath1.Length - 2);
                    lblpathsample1.Text = sampleFormPath1;
                    lnkViewSampleForm1.Visible = true;
                    lblSlash1.Visible = true;
                    lblNote.Visible = true;

                    var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample1.CommandArgument));

                    rptComplianceSampleView1.DataSource = complianceform;
                    rptComplianceSampleView1.DataBind();
                }
                else
                {
                    lblNote.Visible = false;
                    lblSlash1.Visible = false;
                    lnkViewSampleForm1.Visible = false;
                    sampleFormPath1 = "";
                }

                /* added by sudarshan for types of document to show reviewer */
                //List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(RecentComplianceTransaction.ComplianceScheduleOnID), RecentComplianceTransaction.ComplianceTransactionID);
                //if (CMPDocuments != null)
                //{
                var documentVersionData = DocumentManagement.GetFileData1(Convert.ToInt32(RecentComplianceTransaction.ComplianceScheduleOnID)).Select(x => new
                {
                    ID = x.ID,
                    ScheduledOnID = x.ScheduledOnID,
                    FileID  = x.FileID,
                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                    VersionDate = x.VersionDate,
                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                //List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry=>entry.Version!=null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                //if (entitiesData.Count <= 0)
                //{
                //    GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                //    entityData.Version = "1.0";
                //    entityData.ScheduledOnID = RecentComplianceTransaction.ComplianceScheduleOnID;
                //    entitiesData.Add(entityData);
                //}

                rptComplianceVersion.DataSource = documentVersionData;
                rptComplianceVersion.DataBind();

                rptComplianceDocumnets.DataSource = null;
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = null;
                rptWorkingFiles.DataBind();

                //}

                if (Session["InterimdaysReview"].ToString() != "")
                {
                    //int EscDays = Convert.ToInt32(Session["InterimdaysReview"]);
                    lblReviewDoc.Text = "Review Working File";
                    BindEscalationRevStatusList();
                    fieldsetTDS.Visible = false;
                    btnReject1.Visible = false;
                    btnSave1.Text = "Submit";
                }
                else
                {
                    lblReviewDoc.Text = "Review Compliance Document";
                    BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));
                    //fieldsetTDS.Visible = true;
                    btnReject1.Visible = true;
                    btnSave1.Text = "Approve";
                }

            


                BindTransactions(ScheduledOnID);
                tbxRemarks1.Text = string.Empty;
                tbxDate1.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                if (complianceInfo.EventFlag == true)
                {
                    if (complianceInfo.UpDocs == true)
                    {
                        rdbtnStatus1.Attributes.Add("disabled", "disabled");
                        tbxRemarks1.Attributes.Add("disabled", "disabled");
                        tbxDate1.Attributes.Add("disabled", "disabled");
                        btnSave1.Attributes.Add("disabled", "disabled");
                        btnReject1.Attributes.Add("disabled", "disabled");
                        fieldsetdownloadreview.Visible = true;
                    }
                    else
                    {
                        rdbtnStatus1.Attributes.Remove("disabled");
                        tbxRemarks1.Attributes.Remove("disabled");
                        tbxDate1.Attributes.Remove("disabled");
                        btnSave1.Attributes.Remove("disabled");
                        btnReject1.Attributes.Remove("disabled");


                        tbxRemarks1.Attributes.Add("enabled", "enabled");
                        tbxRemarks1.Attributes.Add("enabled", "enabled");
                        tbxDate1.Attributes.Add("enabled", "enabled");
                        btnSave1.Attributes.Add("enabled", "enabled");
                        btnReject1.Attributes.Add("enabled", "enabled");

                        if (documentVersionData.Count > 0)
                        {
                            fieldsetdownloadreview.Visible = true;
                        }
                        else
                        {
                            fieldsetdownloadreview.Visible = false;
                        }
                    }
                }
                else
                {
                    rdbtnStatus1.Attributes.Add("disabled", "disabled");
                    tbxRemarks1.Attributes.Add("disabled", "disabled");
                    tbxDate1.Attributes.Add("disabled", "disabled");
                    btnSave1.Attributes.Add("disabled", "disabled");
                    btnReject1.Attributes.Add("disabled", "disabled");
                }

                #region Act_Document

                var actDocVersionData = ActManagement.getFileNamebyID(complianceInfo.ActID);
                actDocVersionData = actDocVersionData.GroupBy(x => x.DocumentTypeID).Select(x => x.FirstOrDefault()).ToList();

                PopulateTreeView(actDocVersionData, complianceInfo.ActID);
                
                #endregion

                upComplianceDetails1.Update();
                // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divReviewerComplianceDetailsDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void PopulateTreeView(List<Sp_Act_Document_Result> lstActType, int ActID)
        {

            System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();

            stringbuilder.Append(@"<table id='basic' width='100%'><tr>");

            foreach (var item in lstActType)
            {
                stringbuilder.Append("<tr data-node-id=" + Convert.ToString(item.DocumentTypeID) + ">" +
                            " <td class='locationheadLocationbg'>" + Convert.ToString(item.DocumentType) + "</td>");
                stringbuilder.Append("</tr>");
                var actDocVersionData = ActManagement.getActDocumentTypeWise(ActID, item.DocumentTypeID);
                int i = 0;
                foreach (var item1 in actDocVersionData)
                {
                    i += 1;
                    string id = Convert.ToString(item.DocumentTypeID) + "." + i;
                    string FileName = "";
                    string[] filename = item1.FileName.Split('.');
                    string str = filename[0] + i + "." + filename[1];
                    if (filename[0].Length > 90)
                    {
                        FileName = filename[0].Substring(1, 90) + "." + filename[1];
                    }
                    else
                    {
                        FileName = item1.FileName;
                    }

                    if (item.Act_TypeVersionDate != null)
                    {
                        FileName = FileName + " on " + Convert.ToDateTime(item1.Act_TypeVersionDate).ToString("mmm-yy");
                    }

                    stringbuilder.Append("<tr data-node-id=" + Convert.ToString(id) + " data-node-pid=" + Convert.ToString(item.DocumentTypeID) + " >" +
                         " <td class='locationheadLocationbg'>" + Convert.ToString(FileName) + "</td>");

                    stringbuilder.Append("<td class='downloadcss' onclick ='OpenDocumentDowmloadOverviewpup(" + ActID + "," + item1.ID + ")'> Download </td>");
                    stringbuilder.Append("<td class='Viewcss' onclick='OpenDocumentOverviewpup(" + ActID + "," + item1.ID + ")'> View </td>");

                    stringbuilder.Append("</tr>");
                }
            }
            stringbuilder.Append("</table>");
            ActReviewDocString = stringbuilder.ToString().Trim();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fr();", true);
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {
                        rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        rptComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                        rptWorkingFiles.DataBind();

                        var documentVersionData = ComplianceDocument.Select(x => new
                        {
                            ID = x.ID,
                            ScheduledOnID = x.ScheduledOnID,
                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                            VersionDate = x.VersionDate,
                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                        rptComplianceVersion.DataSource = documentVersionData;
                        rptComplianceVersion.DataBind();

                        upComplianceDetails1.Update();


                    }
                }
                else if (e.CommandName.Equals("Download"))
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {

                                //comment by rahul on 20 JAN 2017
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));



                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string ext = Path.GetExtension(file.FileName);
                                    string[] filename = file.FileName.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];
                                    string str = filename[0] + i + "." + ext;
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();

                        Response.Buffer = true;

                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
                else if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                    //Business.ComplianceManagement.GetDocumnetList(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    //Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                rptComplianceVersionView.DataSource = entitiesData; 
                                rptComplianceVersionView.DataBind();
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageReviewer1.Text = "";
                                        lblMessageReviewer1.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;

                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                        lblMessageReviewer1.Text = "";

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    lblMessageReviewer1.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool SaveData(string FileName, byte[] Data)
        {
            BinaryWriter Writer = null;
            //string Name = @"C:\temp\yourfile.name";

            try
            {
                // Create a new stream to write to the file
                Writer = new BinaryWriter(File.OpenWrite(FileName));

                // Writer raw data                
                Writer.Write(Data);
                Writer.Flush();
                Writer.Close();
            }
            catch
            {
                //...
                return false;
            }

            return true;
        }
        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["complianceInstanceID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEscalationRevStatusList()
        {
            try
            {
                rdbtnStatus1.Items.Clear();
                var statusList = ComplianceStatusManagement.GetEscalationRevStatusList();

                foreach (ComplianceStatu st in statusList)
                {
                        rdbtnStatus1.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {

                rdbtnStatus1.Items.Clear();
                var statusList = ComplianceStatusManagement.GetStatusList();

                List<ComplianceStatu> allowedStatusList = null;

                List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                foreach (ComplianceStatu st in allowedStatusList)
                {
                    if (!(st.ID == 6))
                    {
                        rdbtnStatus1.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                    }
                }

                lblStatus1.Visible = allowedStatusList.Count > 0 ? true : false;
                divDated1.Visible = allowedStatusList.Count > 0 ? true : false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave1_Click(object sender, EventArgs e)
        {
            try
            {
                   if (txtInterestReview.Text == "")
                        txtInterestReview.Text = "0";
                    if (txtPenaltyReview.Text == "")
                        txtPenaltyReview.Text = "0";
                    if (txtValueAsPerSystem.Text == "")
                    txtValueAsPerSystem.Text = "0";
                    if (txtValueAsPerReturn.Text == "")
                    txtValueAsPerReturn.Text = "0";
                    if (txtLiabilityPaid.Text == "")
                    txtLiabilityPaid.Text = "0";

                    int StatusID = 0;
                    if (lblStatus1.Visible)
                        StatusID = Convert.ToInt32(rdbtnStatus1.SelectedValue);
                    else
                        StatusID = Convert.ToInt32(ComplianceManagement.Business.ComplianceManagement.GetClosedTransaction(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID);

                #region Performer Penalty Update

                var PenaltySubmit = "";
                if (rdbtnStatus1.SelectedValue == "5")
                {
                    if (chkPenaltySaveReview.Checked == false)
                    {
                        //Performer Penalty Submitted
                        PenaltySubmit = "S";
                    }
                    else
                    {
                        //Performer penalty Pending
                        PenaltySubmit = "P";
                    }
                }
                else
                {
                    //Performer penalty Rejected
                    PenaltySubmit = "R";
                }

                if (StatusID == 2 || StatusID == 3 || StatusID == 5 || StatusID == 4)
                {
                    ComplianceTransaction transactionUpdatePerformer = new ComplianceTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        IsPenaltySave = chkPenaltySaveReview.Checked,
                        Penalty = 0,
                        Interest=0,
                        PenaltySubmit = PenaltySubmit,
                    };
                    Business.ComplianceManagement.UpdatePenaltyTransactionByReviewer(transactionUpdatePerformer);
                }
                #endregion

                ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = StatusID,
                        StatusChangedOn = DateTime.ParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks1.Text,
                        Interest = Convert.ToDecimal(txtInterestReview.Text),
                        Penalty = Convert.ToDecimal(txtPenaltyReview.Text),
                        ComplianceSubTypeID = Convert.ToInt32(lblNatureofcomplianceID.Text),
                        ValuesAsPerSystem = Convert.ToDecimal(txtValueAsPerSystem.Text),
                        ValuesAsPerReturn = Convert.ToDecimal(txtValueAsPerReturn.Text),
                        LiabilityPaid = Convert.ToDecimal(txtLiabilityPaid.Text),
                        IsPenaltySave = chkPenaltySaveReview.Checked,
                        PenaltySubmit = PenaltySubmit,
                    };

                    Business.ComplianceManagement.CreateTransaction(transaction);

                    if (OnSaved != null)
                    {
                        OnSaved(this, null);
                    }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fcloseandcallcal();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnReject1_Click(object sender, EventArgs e)
        {
            try
            {
                     ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = 6,
                        StatusChangedOn = DateTime.ParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks1.Text
                    };

                    Business.ComplianceManagement.CreateTransaction(transaction);

                    if (OnSaved != null)
                    {
                        OnSaved(this, null);
                    }

                    var Compliance = ComplianceManagement.Business.ComplianceManagement.GetInstanceTransactionCompliance(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                    if (Compliance != null)
                    {
                        int customerID = -1;
                        customerID = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID)).CustomerID ?? 0;
                        string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                        var user = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID));
                    string accessURL = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (Urloutput != null)
                    {
                        accessURL = Urloutput.URL;
                    }
                    else
                    {
                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    string message = Settings.Default.EMailTemplate_RejectedCompliance
                                          .Replace("@User", Compliance.User)
                                          .Replace("@ComplianceDescription", Compliance.ShortDescription)
                                          .Replace("@Role", Compliance.Role)
                                          .Replace("@ScheduledOn", Compliance.ScheduledOn.ToString("dd-MMM-yyyy"))
                                          .Replace("@From", ReplyEmailAddressName)
                                          .Replace("@URL", Convert.ToString(accessURL));
                    //string message = Settings.Default.EMailTemplate_RejectedCompliance
                    //                    .Replace("@User", Compliance.User)
                    //                    .Replace("@ComplianceDescription", Compliance.ShortDescription)
                    //                    .Replace("@Role", Compliance.Role)
                    //                    .Replace("@ScheduledOn", Compliance.ScheduledOn.ToString("dd-MMM-yyyy"))
                    //                    .Replace("@From", ReplyEmailAddressName)
                    //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Notification :: Compliance has been rejected.", message);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fcloseandcallcal();", true);

                    }
               
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divReviewerComplianceDetailsDialog\").dialog('close');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);

                    if (file.FilePath == null)
                    {
                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                            //using (FileStream fs = File.OpenRead(Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name))))
                            using (FileStream fs = File.OpenRead(pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"])))
                            {
                                int length = (int)fs.Length;
                                byte[] buffer;

                                using (BinaryReader br = new BinaryReader(fs))
                                {
                                    buffer = br.ReadBytes(length);
                                }

                                Response.Buffer = true;
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                                }
                                Response.Flush(); // send it to the client to download
                            }
                        }
                        else
                        {
                            using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
                            {
                                int length = (int)fs.Length;
                                byte[] buffer;

                                using (BinaryReader br = new BinaryReader(fs))
                                {
                                    buffer = br.ReadBytes(length);
                                }

                                Response.Buffer = true;
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                                }
                                Response.Flush(); // send it to the client to download
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }


        protected void lbDownloadSample1_Click(object sender, EventArgs e)
        {
            try
            {

                var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample1.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample1.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var file in complianceform)
                    {
                        if (file.FilePath != null)
                        {
                            string ext = Path.GetExtension(file.Name);
                            string[] filename = file.Name.Split('.');
                            //string str = filename[0] + i + "." + filename[1];
                            string str = filename[0] + i + "." + ext;
                            ComplianceZip.AddEntry(str, Business.DocumentManagement.ReadDocFiles(Server.MapPath(file.FilePath)));
                            i++;
                        }
                    }

                    string fileName = "ComplianceID_" + complianceID1 + "_SampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }

                //var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample1.CommandArgument));

                //Response.Buffer = true;
                //Response.Clear();
                //Response.ContentType = "application/octet-stream";

                //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.FileData); // create the file
                //Response.Flush(); // send it to the client to download

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails1_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }

                if (lblDueDate.Text != "")
                {
                    if (rdbtnStatus1.SelectedValue == "5") //Closed-Delayed
                    {
                        DateTime Date = DateTime.ParseExact(hiddenReviewDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Date = Date.AddDays(1);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDueReview", string.Format("initializeDatePickerOverDueReview(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                    }
                    else if (rdbtnStatus1.SelectedValue == "4") //Closed-Timely 
                    {
                        DateTime Date = DateTime.ParseExact(hiddenReviewDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTimeReview", string.Format("initializeDatePickerInTimeReview(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                    }
                }
                // ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {

                tbxRemarks1.Text = tbxDate1.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        //filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));

                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }


            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void ViewDocument(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    //if (filePath != null && File.Exists(filePath))
                    //{
                    //    Response.Buffer = true;
                    //    Response.Clear();
                    //    Response.ClearContent();
                    //    Response.ContentType = "application/octet-stream";
                    //    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                    //    Response.Flush(); // send it to the client to download
                    //    Response.End();

                    //}
                    CompDocReviewPath = filePath;
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                HttpFileCollection fileCollection = Request.Files;
                for (int i = 0; i < fileCollection.Count; i++)
                {
                    HttpPostedFile uploadfile = fileCollection[i];
                    string fileName = Path.GetFileName(uploadfile.FileName);
                    if (uploadfile.ContentLength > 0)
                    {                       
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm1.Text != "")
                {
                    string url = lnkSampleForm1.Text;
                  
                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm1.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void txtInterestReview_TextChanged(object sender, EventArgs e)
        {
            int val =Convert.ToInt32(txtInterestReview.Text);

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divComplianceDetailsDialog\").dialog('open');", true);

        }
        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton) e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                                
                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);

                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageReviewer1.Text = "";
                                        lblMessageReviewer1.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }

                                        bw.Close();
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
                                        lblMessageReviewer1.Text = "";

                                    }
                                }
                                else
                                {
                                    lblMessageReviewer1.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                        where row.ParentID == null
                                        //&& row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        divTask.Visible = true;
                        gridSubTaskReviewer.DataSource = documentData;
                        gridSubTaskReviewer.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }

        protected void gridSubTaskReviewer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            if (taskDocument.Count > 0)
                            {
                                string fileName = string.Empty;

                                GridViewRow row = (GridViewRow) (((Control) e.CommandSource).NamingContainer);

                                Label lblTaskTitle = null;

                                if (row != null)
                                {
                                    lblTaskTitle = (Label) row.FindControl("lblTaskTitle");
                                    if (lblTaskTitle != null)
                                        fileName = lblTaskTitle.Text + "-Documents";

                                    if (fileName.Length > 250)
                                        fileName = "TaskDocuments";
                                }

                                ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                int i = 0;
                                foreach (var eachFile in taskDocument)
                                {
                                    //comment by rahul on 20 JAN 2017
                                    string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                    //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                    if (eachFile.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(eachFile.FileName);
                                        string[] filename = eachFile.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (eachFile.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in taskDocumenttoView)
                                {
                                    rptTaskVersionView.DataSource = entitiesData;
                                    rptTaskVersionView.DataBind();

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessagetask.Text = "";
                                            lblMessagetask.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            TaskDocViewPath = FileName;
                                            TaskDocViewPath = TaskDocViewPath.Substring(2, TaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview1('" + TaskDocViewPath + "');", true);
                                            lblMessagetask.Text = "";
                                            UpdatePanel2.Update();
                                        }
                                    }
                                    else
                                    {
                                        lblMessagetask.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptTaskVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptTaskVersionView.DataSource = taskFileData;
                                rptTaskVersionView.DataBind();

                                foreach (var file in taskDocumenttoView)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessagetask.Text = "";
                                            lblMessagetask.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                        }
                                        else
                                        {
                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            TaskDocViewPath = FileName;
                                            TaskDocViewPath = TaskDocViewPath.Substring(2, TaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview1('" + TaskDocViewPath + "');", true);
                                            lblMessagetask.Text = "";
                                            UpdatePanel2.Update();
                                        }
                                    }
                                    else
                                    {
                                        lblMessagetask.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTaskReviewer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblTaskSlashReview = (Label)e.Row.FindControl("lblTaskSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblTaskSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblTaskSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    btnSubTaskDocDownload.Visible = false;
                                    lblTaskSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblTaskSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceSampleView1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    ComplianceForm CMPDocuments = Business.ComplianceManagement.GetSelectedComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

                    if (CMPDocuments != null)
                    {
                        string fullfilePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath));
                        string filePath = CMPDocuments.FilePath;
                        string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                        if (CMPDocuments.FilePath != null && File.Exists(fullfilePath))
                        {
                            string extension = System.IO.Path.GetExtension(CompDocPath);

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileReviewer();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFileReviewer('" + CompDocPath + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileReviewer();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView);
            }
        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample1.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample1.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var file in complianceform)
                    {
                        if (file.FilePath != null)
                        {
                            string ext = Path.GetExtension(file.Name);
                            string[] filename = file.Name.Split('.');
                            //string str = filename[0] + i + "." + filename[1];
                            string str = filename[0] + i + "." + ext;
                            ComplianceZip.AddEntry(str, Business.DocumentManagement.ReadDocFiles(Server.MapPath(file.FilePath)));
                            i++;
                        }
                    }

                    string fileName = "ComplianceID_" + complianceID1 + "_SampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdbtnStatus1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdbtnStatus1.SelectedValue == "4") //Closed-Timely
                {
                    //rfvRemark1.Enabled = false;
                    IsRemarkCompulsary2 = false;
                    DateTime Date = DateTime.ParseExact(hiddenReviewDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTimeReview", string.Format("initializeDatePickerInTimeReview(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                }
                else if (rdbtnStatus1.SelectedValue == "5") //Closed-Delayed
                {
                    //rfvRemark1.Enabled = true;
                    IsRemarkCompulsary2 = true;
                    DateTime Date = DateTime.ParseExact(hiddenReviewDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    Date = Date.AddDays(1);
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDueReview", string.Format("initializeDatePickerOverDueReview(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                }
                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "enableControls1()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}