﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class AddChecklistRemark : System.Web.UI.Page
    {
        protected static int remarkId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                remarkId = Convert.ToInt32(Request.QueryString["ID"]);
                int rkid = Convert.ToInt32(remarkId);
                if(remarkId != 0)
                {
                    var existingremark = GetOldData(rkid);
                    tbxRemarks.Text = existingremark.Remark;
                }
                
            }
        }
        public static tbl_ChecklistRemark GetOldData(int remarkId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var details = (from row in entities.tbl_ChecklistRemark
                               where row.ID == remarkId
                               select row).SingleOrDefault();

                return details;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                tbl_ChecklistRemark chkremark = new tbl_ChecklistRemark()
                {

                    Remark = tbxRemarks.Text,
                    CreateBy = AuthenticationHelper.UserID,
                    CreateOn = System.DateTime.Now,
                    CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID),
                    IsActive = true,
                };
                if (remarkId==0)
                {

                  

                    if (Exists(chkremark))
                    {
                        cvDuplicateEntry.ErrorMessage = "Remark already exist.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else
                    {
                        entities.tbl_ChecklistRemark.Add(chkremark);
                        entities.SaveChanges();

                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Remark Saved Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                    }
                    
                }
                else
                {
                    if (Exists(chkremark))
                    {
                        cvDuplicateEntry.ErrorMessage = "Remark already exist.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else
                    {
                        UpdateRemark(remarkId);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Remark Updated Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                    }
                        
                  
                }

              

            }
        }

        public static bool Exists(tbl_ChecklistRemark tblchk)
        {
            Boolean flag;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_ChecklistRemark
                            where row.Remark.ToUpper().Trim()== tblchk.Remark.ToUpper().Trim()
                            && row.IsActive == true
                            select row).SingleOrDefault();

                if (data != null)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
                return flag;

            }
        }
        public void UpdateRemark(int remarkId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_ChecklistRemark
                            where row.ID == remarkId
                            select row).FirstOrDefault();

                if(data!=null)
                {

                    data.Remark = tbxRemarks.Text;
                    data.UpdatedBy = AuthenticationHelper.UserID;
                    data.UpdatedOn = System.DateTime.Now;
                    entities.SaveChanges();
                    tbxRemarks.Text = string.Empty;
                  
                }
               
               
               

            }
        }
    }
}