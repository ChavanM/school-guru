﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Threading;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System.IO;
using System.Globalization;
using System.Text;                                                                           
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class UserDetailsControl : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rblAuditRole.Enabled = true;
                BindCustomers();
                BindRoles();
                BindHRRoles();
                BindRolesRisk();

                BindRoles_Secretarial();

                txtDepartment.Attributes.Add("readonly", "readonly");
                txtDepartment.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                rptUserGroup.ClearSelection();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUserGroup", "initializeJQueryUserGroup();", true);
            }
        }
        protected void Upload(object sender, EventArgs e)
        {
            if (UserImageUpload.HasFile)
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

                string ext = System.IO.Path.GetExtension(UserImageUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                }
            }
        }
        protected void upUsers_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUserGroup", "initializeJQueryUserGroup();", true);


                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvDept\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeDatePickerFunctionEndDate", string.Format("initializeDatePickerFunctionEndDate(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeDatePickerFunctionEndDate1", string.Format("initializeDatePickerFunctionEndDate1(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeDatePickerFunctionEndDate2", string.Format("initializeDatePickerFunctionEndDate2(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeDatePickerFunctionEndDate3", string.Format("initializeDatePickerFunctionEndDate3(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rblAuditRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRole.SelectedIndex = -1;
            ddlRiskRole.SelectedIndex = -1;

            if (rblAuditRole.SelectedItem.Text == "Is Audit Head" || rblAuditRole.SelectedItem.Text == "Is Audit Manager")
            {
                //ddlRole.Enabled = false;
                ddlRole.Items.FindByText("Non - Admin").Selected = true;

                ddlRiskRole.Enabled = false;
                ddlRiskRole.Items.FindByText("Non - Admin").Selected = true;
            }
            else
            {
                ddlRole.Enabled = true;
                ddlRiskRole.Enabled = true;
            }
        }
        private void BindDepartment(int CustomerID)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    customerID = CustomerID;
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                    }
                }
                if (customerID != -1)
                {
                    rptDepartment.DataSource = CompDeptManagement.FillDepartment(customerID);
                    rptDepartment.DataBind();


                    foreach (RepeaterItem aItem in rptDepartment.Items)
                    {
                        CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");

                        if (!chkDepartment.Checked)
                        {
                            chkDepartment.Checked = true;
                        }
                    }
                    CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                    DepartmentSelectAll.Checked = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upDepeList_Load(object sender, EventArgs e)
        {
        }
        private void BindGroupName(int CustomerID)
        {
            try
            {
                rptUserGroup.DataTextField = "Name";
                rptUserGroup.DataValueField = "ID";
                rptUserGroup.DataSource = IPAddBlock.GetAllGroupName(CustomerID);                
                rptUserGroup.DataBind();
                rptUserGroup.Items.Insert(0, new ListItem("< Select Security Group >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void chkDesktopUserRestricted_Checkedchanged(object sender, EventArgs e)
        {
            if (chkDesktopUserRestricted.Checked == true)
            {
                UserGroupname.Visible = true;
            }
            else
            {
                UserGroupname.Visible = false;
            }
        }
        private void BindRoles()
        {
            try
            {
                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "ID";

                var roles = RoleManagement.GetAll(false);
                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }

                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }
                ddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindHRRoles()
        {
            try
            {
                ddlHRRole.DataTextField = "Name";
                ddlHRRole.DataValueField = "ID";

                List<Role> roles = new List<Role>();

                //var roles = RoleManagement.GetAll(false);
                //roles = RoleManagement.GetAll_Compliance(false);
                roles = RoleManagement.GetAll_HRCompliance_Roles();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }

                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }

                bool showDADMNRole = false;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        var customerDetails = CustomerManagement.GetByID(Convert.ToInt32(ddlCustomer.SelectedValue));

                        if (customerDetails != null)
                            if (customerDetails.IsDistributor != null)
                                if (Convert.ToBoolean(customerDetails.IsDistributor))
                                    showDADMNRole = true;
                    }
                }

                if (!showDADMNRole)
                    roles = roles.Where(entry => !entry.Code.Equals("DADMN")).ToList();

                ddlHRRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlHRRole.DataBind();

                ddlHRRole.Items.Insert(0, new ListItem("< Select HR Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindRoles_Secretarial()
        {
            try
            {
                ddlSecRole.DataTextField = "Name";
                ddlSecRole.DataValueField = "ID";

                List<Role> roles = new List<Role>();
              
                roles = RoleManagement.GetAll_Secretarial_Roles();

                ddlSecRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlSecRole.DataBind();

                ddlSecRole.Items.Insert(0, new ListItem("< Select Secretarial Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        private void BindRolesRisk()
        {
            try
            {
                ddlRiskRole.DataTextField = "Name";
                ddlRiskRole.DataValueField = "ID";

                var roles = RoleManagement.GetLimitedRole(false);
                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                ddlRiskRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRiskRole.DataBind();

                ddlRiskRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlRiskRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRiskRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                if (!divCustomerBranch.Visible)
                {
                    divCustomerBranch.Visible = roleCode.Equals("EXCT") || roleCode.Equals("OTHER");
                    if (roleCode.Equals("VAUDT"))
                    {
                        divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                    }
                }
               
                if (Convert.ToInt32(ddlRiskRole.SelectedValue) == 2 || Convert.ToInt32(ddlRiskRole.SelectedValue) == 1 || Convert.ToInt32(ddlRiskRole.SelectedValue) == 8 || Convert.ToInt32(ddlRiskRole.SelectedValue) == 12)
                {
                    rblAuditRole.Enabled = false;
                    rblAuditRole.ClearSelection();
                }
                else
                {
                    rblAuditRole.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewRoleChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
                reqAudit1.Visible = false;
                reqAudit2.Visible = false;
                reqAudit3.Visible = false;
                reqAudit4.Visible = false;
                if (roleCode.Equals("AUDT"))
                {
                    Auditor1.Visible = true;
                    Auditor2.Visible = true;

                    reqAudit1.Visible = true;
                    reqAudit2.Visible = true;
                    reqAudit3.Visible = true;
                    reqAudit4.Visible = true;
                }
                else
                {
                    Auditor1.Visible = false;
                    Auditor2.Visible = false;
                    reqAudit1.Visible = true;
                    reqAudit2.Visible = true;
                    reqAudit3.Visible = true;
                    reqAudit4.Visible = true;
                }

                divCertificate.Visible = false;
                if (roleCode.Equals("MGMT"))
                {
                    divCertificate.Visible = true;
                }
                if (divCustomerBranch.Visible)
                {
                    ddlCustomer_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewRoleChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                int userid = -1;
                userid = Convert.ToInt32(AuthenticationHelper.UserID);
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                if (AuthenticationHelper.Role == "IMPT")
                {
                    ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(userid);
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlCustomer.DataSource = Assigncustomer.GetAllCustomerData(customerID);
                    ddlCustomer.SelectedValue = Convert.ToString(customerID);
                    ddlCustomer.DataBind();
                }
                //ddlCustomer.DataSource =Assigncustomer.GetAllCustomer(userid);
                //ddlCustomer.DataBind();
                //ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                BindReportingTo();
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewCustomerChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptUserGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var result= IPAddBlock.GetAllGroupNameNew(Convert.ToInt32(rptUserGroup.SelectedValue));

                if (result.Count >= 0)
                {
                    if (result[0].mobileaccess == true)
                    {
                        chkMobileAccess.Visible = true;
                        chkMobileAccess.Checked = true;
                        chkIMEI.Visible = true;
                       
                    }
                    else
                    {
                        chkMobileAccess.Visible = false;
                        chkMobileAccess.Checked = false;
                        chkIMEI.Visible=false;
                    }
                    if(result[0].IMEI==true)
                    {
                        chkIMEI.Checked = true;
                        chkIMEI.Visible = true;
                        IMEINumber.Visible = true;
                    }
                    else
                    {
                        chkIMEI.Checked = false;
                        IMEINumber.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindReportingTo()
        {
            try
            {
                ddlReportingTo.DataTextField = "Name";
                ddlReportingTo.DataValueField = "ID";

                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(Convert.ToInt32(ddlCustomer.SelectedValue), roleCode);
                ddlReportingTo.DataBind();

                ddlReportingTo.Items.Insert(0, new ListItem("< Select Reporting to person >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Branch >";
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (customerID != -1)
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;
                    var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    if (branchs.Count > 0)
                    {
                        branch = branchs[0];
                    }
                    tbxBranch.Text = "< Select Location >";
                    List<TreeNode> nodes = new List<TreeNode>();
                    BindBranchesHierarchy(null, branch, nodes);
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }
                    tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        {
            try
            {
                if (userParameterValues == null)
                {
                    userParameterValues = new List<UserParameterValueInfo>();
                    var userParameters = UserManagement.GetAllUserParameters();
                    userParameters.ForEach(entry =>
                    {
                        userParameterValues.Add(new UserParameterValueInfo()
                        {
                            UserID = -1,
                            ParameterID = entry.ID,
                            ValueID = -1,
                            Name = entry.Name,
                            DataType = (DataType)entry.DataType,
                            Length = (int)entry.Length,
                            Value = string.Empty
                        });
                    });
                }

                repParameters.DataSource = userParameterValues;
                repParameters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void EnableDisableRole(int CustomerID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ab = false;
                int customerID = -1;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    customerID = CustomerID;
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                bool ac = UserManagement.CheckProductmapping(customerID, 1);
                if (ac == true)
                {
                    divComplianceRole.Visible = true;
                    ddlRole.Enabled = true;
                }
                else
                {
                    divComplianceRole.Visible = false;
                    ddlRole.Enabled = false;
                }
                bool a = UserManagementRisk.CheckProductmapping(customerID, 3);
                if (a == true)
                {
                    divRiskRole.Visible = true;
                    ddlRiskRole.Enabled = true;
                }
                else
                {
                    ab = UserManagementRisk.CheckProductmapping(customerID, 4);
                    if (ab == true)
                    {
                        divRiskRole.Visible = true;
                        ddlRiskRole.Enabled = true;
                    }
                    else
                    {
                        divRiskRole.Visible = false;
                        ddlRiskRole.Enabled = false;
                    }
                }
                if (ac == false && a == false && ab == false)
                {
                    divComplianceRole.Visible = true;
                    ddlRole.Enabled = true;
                }
            }
        }
        public void AddNewUser(int CustomerID)
        {

            try
            {
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                UserGroupname.Visible = true;
                tbxEmail.Enabled = true;
                ddlCustomer.Enabled = true;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlRiskRole.ClearSelection();
                ddlRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);

                ddlSecRole.ClearSelection();

                chkDesktopUserRestricted.Checked = false;
                chkHead.Checked = false;
                txtEndDate.Text = "";
                txtStartDate.Text = "";
                txtperiodStartDate.Text = "";
                txtperiodEndDate.Text = "";
                BindParameters();
                chkDesktopUserRestricted_Checkedchanged(null, null);
                chkIMEI_Checkedchanged(null, null);
                chkMobileAccess_Checkedchanged(null, null);
                EnableDisableRole(CustomerID);
                BindDepartment(CustomerID);
                BindGroupName(CustomerID);
                chkDesktopUserRestricted.Checked = true;
                UserGroupname.Visible = true;
                chkIMEI.Visible = false;
                IMEINumber.Visible = false;
                chkMobileAccess.Visible = false;
                rptUserGroup.ClearSelection();

                chkSSOAccess.Checked = true;

                try
                {
                    if (rptDepartment.Items.Count > 0)
                    {
                        foreach (RepeaterItem aItem in rptDepartment.Items)
                        {
                            CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                            chkDepartment.Checked = false;
                            CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                            DepartmentSelectAll.Checked = false;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        public void AddNewUser()
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                chkIMEI.Checked = false;
                tbxEmail.Enabled = true;
                ddlCustomer.Enabled = true;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlRiskRole.ClearSelection();
                ddlRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);

                ddlSecRole.ClearSelection();

                rptUserGroup.ClearSelection();
                rptUserGroup.SelectedValue = "-1";
                BindParameters();
                chkDesktopUserRestricted_Checkedchanged(null, null);
                chkDesktopUserRestricted.Checked = true;
                UserGroupname.Visible = true;
                rptUserGroup.ClearSelection();
                // chkMobileAccess.Visible = false;

                chkSSOAccess.Checked = true;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    bool ab = false;
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    BindGroupName(customerID);
                    bool ac = UserManagement.CheckProductmapping(customerID, 1);
                    if (ac == true)
                    {
                        divComplianceRole.Visible = true;
                    }
                    else
                    {
                        divComplianceRole.Visible = false;
                    }

                    bool a = UserManagementRisk.CheckProductmapping(customerID, 3);
                    if (a == true)
                    {
                        divRiskRole.Visible = true;
                    }
                    else
                    {
                        ab = UserManagementRisk.CheckProductmapping(customerID, 4);
                        if (ab == true)
                        {
                            divRiskRole.Visible = true;
                        }
                        else
                        {
                            divRiskRole.Visible = false;
                        }
                    }
                    if (ac == false && a == false && ab == false)
                    {
                        divComplianceRole.Visible = true;
                    }
                }
                try
                {
                    if (rptDepartment.Items.Count > 0)
                    {
                        foreach (RepeaterItem aItem in rptDepartment.Items)
                        {
                            CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                            chkDepartment.Checked = false;
                            CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                            DepartmentSelectAll.Checked = false;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }

                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void chkIMEI_Checkedchanged(object sender, EventArgs e)
        {
            if (chkIMEI.Checked == true)
            {
                IMEINumber.Visible = true;
                chkIMEI.Checked = true;
            }
            else
            {
                IMEINumber.Visible = false;
                chkIMEI.Checked = false;
            }

        }
        protected void chkMobileAccess_Checkedchanged(object sender, EventArgs e)
        {
            if (chkMobileAccess.Checked == true)
            {
                chkIMEI.Visible = true;
            }
            else
            {
                chkIMEI.Visible = false;
                IMEINumber.Visible = false;
                chkIMEI.Checked = false;
            }
        }

        public void EditUserInformation(int userID)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "initializeDatePickerFunctionEndDate", string.Format("initializeDatePickerFunctionEndDate(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "initializeDatePickerFunctionEndDate1", string.Format("initializeDatePickerFunctionEndDate1(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "initializeDatePickerFunctionEndDate2", string.Format("initializeDatePickerFunctionEndDate2(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "initializeDatePickerFunctionEndDate3", string.Format("initializeDatePickerFunctionEndDate3(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(0);", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "disableCombobox", "disableCombobox();", true);
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;
                User user = UserManagement.GetByID(userID);
                List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);
                if (user.RoleID == 1 || user.RoleID == 2 || user.RoleID == 8 || user.RoleID == 12)
                {
                    rblAuditRole.Enabled = false;
                }
                tbxFirstName.Text = user.FirstName;
                tbxLastName.Text = user.LastName;
                tbxDesignation.Text = user.Designation;
                tbxEmail.Text = user.Email;
                tbxContactNo.Text = user.ContactNumber;
                tbxAddress.Text = user.Address;
                txtIMEI.Text = user.IMEINumber;
                if (user.RoleID == 9)
                {
                    txtStartDate.Text = Convert.ToString(Convert.ToDateTime(user.Startdate).ToString("dd-MM-yyyy"));
                    txtEndDate.Text = Convert.ToString(Convert.ToDateTime(user.Enddate).ToString("dd-MM-yyyy"));
                    txtperiodStartDate.Text = Convert.ToString(Convert.ToDateTime(user.AuditStartPeriod).ToString("dd-MM-yyyy"));
                    txtperiodEndDate.Text = Convert.ToString(Convert.ToDateTime(user.AuditEndPeriod).ToString("dd-MM-yyyy"));
                }
                var Productdetails = UserManagement.GetByProductIDList(Convert.ToInt32(user.CustomerID));
                #region Department
                BindDepartment((int)user.CustomerID);
                var vGetIndustryMappedIDs = Business.DepartmentHeadManagement.GetDepartmentData(userID);

                foreach (RepeaterItem aItem in rptDepartment.Items)
                {
                    CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                    chkDepartment.Checked = false;
                    CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");

                    for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                    {
                        if (((Label)aItem.FindControl("lblDeptID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                        {
                            chkDepartment.Checked = true;
                        }
                    }
                    if ((rptDepartment.Items.Count) == (vGetIndustryMappedIDs.Count))
                    {
                        DepartmentSelectAll.Checked = true;
                    }
                    else
                    {
                        DepartmentSelectAll.Checked = false;
                    }
                }
                #endregion

                #region edit group name
                if (UserGroupname.Visible == true)
                {
                    BindGroupName((int)user.CustomerID);                                 
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var result = (from row in entities.mst_UserList_Group
                                      where row.UserID == userID
                                      && row.IsActive == true
                                      select row.GroupID).FirstOrDefault();
                        if (result !=0)
                        {
                            rptUserGroup.SelectedValue = result != null ? result.ToString() : "-1";
                        }                        
                    }                  
                }
                #endregion
                if (Productdetails.Contains(3))
                {
                    divRiskRole.Visible = true;
                }
                else if (Productdetails.Contains(4))
                {
                    divRiskRole.Visible = true;
                }
                else
                {
                    divRiskRole.Visible = false;
                }
                ddlRole.SelectedValue = user.RoleID != null ? user.RoleID.ToString() : "-1";

                if (user.IsCertificateVisible != null)
                {
                    if (user.IsCertificateVisible == 1)
                    {
                        chkCertificateVisible.Checked = true;
                    }

                }
                ddlRole_SelectedIndexChanged(null, null);


                //ddlHRRole.SelectedValue = user.HRRoleID != null ? user.HRRoleID.ToString() : "-1";

                if (user.HRRoleID != null)
                {
                    if (ddlHRRole.Items.FindByValue(user.HRRoleID.ToString()) != null)
                        ddlHRRole.SelectedValue = user.HRRoleID != null ? user.HRRoleID.ToString() : "-1";
                }

                if (user.SecretarialRoleID != null)
                {
                    if (ddlSecRole.Items.FindByValue(user.SecretarialRoleID.ToString()) != null)
                        ddlSecRole.SelectedValue = user.SecretarialRoleID != null ? user.SecretarialRoleID.ToString() : "-1";
                }

                if (user.IsHead == true)
                    chkHead.Checked = true;
                else
                    chkHead.Checked = false;

                if (user.DesktopRestrict == true)
                {
                    chkDesktopUserRestricted.Checked = true;
                    UserGroupname.Visible = true;
                }
                else
                {
                    chkDesktopUserRestricted.Checked = false;
                    UserGroupname.Visible = false;
                    chkMobileAccess.Visible = false;
                    chkIMEI.Visible = false;
                    IMEINumber.Visible = false;
                    rptUserGroup.ClearSelection();
                }
                if (user.IsAuditHeadOrMgr != null)
                {
                    if (user.IsAuditHeadOrMgr == "AH")
                        rblAuditRole.Items.FindByValue("IAH").Selected = true;
                }
                if (user.IsAuditHeadOrMgr != null)
                { 
                    if (user.IsAuditHeadOrMgr == "AM")
                        rblAuditRole.Items.FindByValue("IAM").Selected = true;
                }
                if (user.MobileAccess == true)
                {
                    chkMobileAccess.Checked = true;
                    chkMobileAccess.Visible = true;
                    chkIMEI.Visible = true;
                }
                else
                {
                    chkMobileAccess.Checked = false;
                    chkIMEI.Visible = false;
                    IMEINumber.Visible = false;
                }
                if (user.IMEIChecked == true)
                {
                    IMEINumber.Visible = true;
                    chkIMEI.Checked = true;
                    
                }
                else
                {
                    IMEINumber.Visible = false;
                    chkIMEI.Checked = false;
                }
                if(chkMobileAccess.Checked!=true && chkMobileAccess.Visible==true)
                {
                    chkMobileAccess.Visible = true;
                }
                else
                {
                    chkMobileAccess.Checked = true;
                }
                if(user.Cer_OfficerRoleID == 1)
                {
                    ChkIsComplianceOfficer.Checked=true;
                }
                else
                {
                    ChkIsComplianceOfficer.Checked =false;
                }
                if (user.Cer_OwnerRoleID == 1)
                {
                    ChkIsComplianceOwner.Checked = true;
                }
                else
                {
                    ChkIsComplianceOwner.Checked = false;
                }
                if (!string.IsNullOrEmpty(txtIMEI.Text) && IMEINumber.Visible == true)
                {
                    user.IMEINumber = txtIMEI.Text;
                    chkIMEI.Checked = true;
                    IMEINumber.Visible = true;
                    chkIMEI.Visible = true;

                }
                else
                {
                    chkIMEI.Checked = false;
                    IMEINumber.Visible = false;                    
                }

                if (user.SSOAccess == true)
                {
                    chkSSOAccess.Checked = true;
                }
                else
                {
                    chkSSOAccess.Checked = false;                   
                }

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                //ddlRiskRole.Enabled = false;
                ddlRiskRole.SelectedValue = mstuser.RoleID != null ? mstuser.RoleID.ToString() : "-1"; // mstuser.RoleID.ToString();
                ddlRiskRole_SelectedIndexChanged(null, null);
                if (divCustomer.Visible && user.CustomerID.HasValue)
                {
                    ddlCustomer.SelectedValue = user.CustomerID.Value.ToString();
                    ddlCustomer_SelectedIndexChanged(null, null);
                }

                ddlReportingTo.SelectedValue = user.ReportingToID != null ? user.ReportingToID.ToString() : "-1";

                if (divCustomerBranch.Visible && user.CustomerBranchID.HasValue)
                {
                    Queue<TreeNode> queue = new Queue<TreeNode>();
                    foreach (TreeNode node in tvBranches.Nodes)
                    {
                        queue.Enqueue(node);
                    }
                    while (queue.Count > 0)
                    {
                        TreeNode node = queue.Dequeue();
                        if (node.Value == user.CustomerBranchID.Value.ToString())
                        {
                            node.Selected = true;
                            break;
                        }
                        else
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                queue.Enqueue(child);
                            }
                        }
                    }
                    tvBranches_SelectedNodeChanged(null, null);
                }

                if (user.ImagePath != null)


                    BindParameters(userParameterValues);
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open')", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean isDeptChecked = false;
                foreach (RepeaterItem aItem in rptDepartment.Items)
                {
                    CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                    if (chkDepartment.Checked)
                    {
                        isDeptChecked = true;
                        break;
                    }
                }
                if (isDeptChecked == false)
                {
                    cvEmailError.ErrorMessage = "Please select department";
                    cvEmailError.IsValid = false;
                    return;
                }
                                
                int customerID = -1;
                customerID = Convert.ToInt32(ddlCustomer.SelectedValue);                
                int getproductcOMPLIANCE = -1;                             
                int getproductrisk = -1;              
                int HRRoleID = -1;

                if (ddlRole.SelectedValue != "-1")
                {
                    getproductcOMPLIANCE = Convert.ToInt32(ddlRole.SelectedValue);
                }
                if (ddlRiskRole.SelectedValue != "-1")
                {
                    getproductrisk = Convert.ToInt32(ddlRiskRole.SelectedValue);
                }              
                if (ddlHRRole.SelectedValue != "-1")
                {
                    HRRoleID = Convert.ToInt32(ddlHRRole.SelectedValue);
                }

                int? SecRoleID = null;

                if (!string.IsNullOrEmpty(ddlSecRole.SelectedValue) && ddlSecRole.SelectedValue != "-1")
                {
                    SecRoleID = Convert.ToInt32(ddlSecRole.SelectedValue);
                }

                int? IScertificateVisible = null;
                if (chkCertificateVisible.Checked == true)
                {
                    IScertificateVisible = 1;
                }
                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    RoleID = getproductcOMPLIANCE,
                    IsExternal = false,
                    HRRoleID = HRRoleID,
                    SecretarialRoleID = SecRoleID,
                    IsCertificateVisible = IScertificateVisible,
                    CreatedFrom=1
                };
                if (getproductcOMPLIANCE == 21 || getproductcOMPLIANCE == 22)
                {
                    user.VendorRoleID = getproductcOMPLIANCE;
                }

                if (divCustomer.Visible)
                {
                    user.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
               
                if (divCustomerBranch.Visible)
                {
                    if(tvBranches.SelectedNode.Value != "-1" && tvBranches.SelectedNode.Value != null)
                    {
                        user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    }
                }

                if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                {
                    user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                }
                
                if (ddlRole.SelectedValue == "9" && Auditor1.Visible)
                {
                    try
                    {
                        user.Startdate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        user.Enddate = DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    catch { }
                }

                if (ddlRole.SelectedValue == "9" && Auditor2.Visible)
                {
                    try
                    {
                        user.AuditStartPeriod = DateTime.ParseExact(txtperiodStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        user.AuditEndPeriod = DateTime.ParseExact(txtperiodEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    }
                    catch { }
                }
                List<UserParameterValue> parameters = new List<UserParameterValue>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parameters.Add(new UserParameterValue()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }
                if (rblAuditRole.SelectedIndex != -1)
                {
                    if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                    {
                        user.IsAuditHeadOrMgr = "AH";
                    }

                    if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                    {
                        user.IsAuditHeadOrMgr = "AM";
                    }
                }
                if (ChkIsComplianceOfficer.Checked)
                {
                    user.Cer_OfficerRoleID = 1;
                }
                else
                {
                    user.Cer_OfficerRoleID = 0;
                }
                if (ChkIsComplianceOwner.Checked)
                {
                    user.Cer_OwnerRoleID = 1;
                }
                else
                {
                    user.Cer_OwnerRoleID = 0;
                }
                #endregion

                #region Risk User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    RoleID = getproductrisk,
                    IsExternal = false,
                    HRRoleID = HRRoleID,
                    SecretarialRoleID = SecRoleID,
                    CreatedFrom=1
                };
                if (getproductrisk == 21 || getproductrisk == 22)
                {
                    mstUser.VendorRoleID = getproductcOMPLIANCE;
                }
                if (ddlRole.SelectedValue == "9" && Auditor1.Visible)
                {
                    try
                    {
                        mstUser.Startdate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        mstUser.Enddate = DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    catch { }
                }

                if (ddlRole.SelectedValue == "9" && Auditor2.Visible)
                {
                    try
                    {
                        mstUser.AuditStartPeriod = DateTime.ParseExact(txtperiodStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        mstUser.AuditEndPeriod = DateTime.ParseExact(txtperiodEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    catch { }
                }

                if (chkDesktopUserRestricted.Checked)
                {
                    user.DesktopRestrict = true;
                    mstUser.DesktopRestrict = true;
                }
                else
                {
                    user.DesktopRestrict = false;
                    mstUser.DesktopRestrict = false;
                }
                if (chkMobileAccess.Checked == true)
                {
                    user.MobileAccess = true;
                    mstUser.MobileAccess = true;
                }
                else
                {
                    user.MobileAccess = false;
                    mstUser.MobileAccess = false;
                }
                if (chkIMEI.Checked)
                {
                    user.IMEIChecked = true;
                    mstUser.IMEIChecked = true;
                }
                else
                {
                    user.IMEIChecked = false;
                    mstUser.IMEIChecked = false;
                }
                if (!string.IsNullOrEmpty(txtIMEI.Text) && txtIMEI.Visible == true)
                {
                    user.IMEINumber = txtIMEI.Text;
                    mstUser.IMEINumber = txtIMEI.Text;
                }                
                if (chkHead.Checked)
                {
                    mstUser.IsHead = true;
                    user.IsHead = true;
                }
                else
                {
                    mstUser.IsHead = false;
                    user.IsHead = false;
                }

                if (divCustomer.Visible)
                {
                    mstUser.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                if (divCustomerBranch.Visible)
                {
                    mstUser.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }

                if (chkSSOAccess.Checked == true)
                {
                    user.SSOAccess = true;
                    mstUser.SSOAccess = true;
                }
                else
                {
                    user.SSOAccess = false;
                    mstUser.SSOAccess = false;
                }
                
                if (ChkIsComplianceOfficer.Checked)
                {
                    mstUser.Cer_OfficerRoleID = 1;
                }
                if (ChkIsComplianceOwner.Checked)
                {
                    mstUser.Cer_OwnerRoleID = 1;
                }

                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }

                if (rblAuditRole.SelectedIndex != -1)
                {
                    if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                    {
                        mstUser.IsAuditHeadOrMgr = "AH";
                    }

                    else if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                    {
                        mstUser.IsAuditHeadOrMgr = "AM";
                    }
                }
                #endregion
                
                if (getproductcOMPLIANCE == 2)
                {

                    user.VendorRoleID = 2;
                    mstUser.VendorRoleID = 2;

                    var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                    if (ProductMappingDetails.Contains(2))
                    {
                        if (user.LitigationRoleID == null)
                        {
                            user.LitigationRoleID = 2;
                            mstUser.LitigationRoleID = 2;
                        }
                    }
                    if (ProductMappingDetails.Contains(5))
                    {
                        if (user.ContractRoleID == null)
                        {
                            user.ContractRoleID = 2;
                            mstUser.ContractRoleID = 2;
                        }
                    }
                    if (ProductMappingDetails.Contains(6))
                    {
                        if (user.LicenseRoleID == null)
                        {
                            user.LicenseRoleID = 2;
                            mstUser.LicenseRoleID = 2;
                        }
                    }
                }
                if ((int)ViewState["Mode"] == 1)
                {
                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                    mstUser.ID = Convert.ToInt32(ViewState["UserID"]);
                }

                bool emailExists;
                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                bool result = false;
                int resultValue = 0;

                long cid = -1;
                if (divCustomer.Visible)
                {
                    cid = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    cid = AuthenticationHelper.UserID;
                }
                if ((int)ViewState["Mode"] == 0)
                {
                    user.CreatedBy = AuthenticationHelper.UserID;
                    user.CreatedByText = AuthenticationHelper.User;
                    string passwordText = Util.CreateRandomPassword(10);
                    user.Password = Util.CalculateAESHash(passwordText);
                    string message = SendNotificationEmail(user, passwordText);

                    mstUser.CreatedBy = AuthenticationHelper.UserID;
                    mstUser.CreatedByText = AuthenticationHelper.User;
                    mstUser.Password = Util.CalculateAESHash(passwordText);
                    resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    if (resultValue > 0)
                    {

                        result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (result == false)
                        {
                            UserManagement.deleteUser(resultValue);
                        }
                        #region Department
                        List<DepartmentMapping> objDepartmentMapping = new List<DepartmentMapping>();
                        List<int> DeptIds = new List<int>();
                        foreach (RepeaterItem aItem in rptDepartment.Items)
                        {
                            CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                            if (chkDepartment.Checked)
                            {
                                DeptIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()));


                                DepartmentMapping DeptMapping = new DepartmentMapping()
                                {
                                    UserID = resultValue,
                                    DepartmentID = Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()),
                                    IsActive = true,
                                    CustomerID = cid,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };
                                objDepartmentMapping.Add(DeptMapping);
                            }
                        }
                        if (objDepartmentMapping.Count > 0)
                        {
                            Business.DepartmentHeadManagement.CreateDepartmentMapping(objDepartmentMapping);
                        }
                        #endregion

                        #region UserGroup
                        if (!string.IsNullOrEmpty(rptUserGroup.SelectedValue ) && rptUserGroup.SelectedValue !="-1")
                        {
                            List<mst_UserList_Group> objGroupMapping = new List<mst_UserList_Group>();
                            mst_UserList_Group GroupMapping = new mst_UserList_Group()
                            {
                                UserID = resultValue,
                                GroupID = Convert.ToInt32(rptUserGroup.SelectedValue),
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                UpdatedOn = null,
                                UpdatedBy = null,
                            };
                            objGroupMapping.Add(GroupMapping);
                            if (objGroupMapping.Count > 0)
                            {
                                Business.IPAddBlock.CreateUserGroupNameMapping(objGroupMapping);
                            }
                        }                       
                        #endregion
                        
                        if (result == true)
                        {

                                widget swid = new widget()
                                {
                                    UserId = (Int32)user.ID,
                                    Performer = true,
                                    Reviewer = true,
                                    PerformerLocation = true,
                                    ReviewerLocation = true,
                                    DailyUpdate = true,
                                    NewsLetter = true,
                                    ComplianceSummary = true,
                                    FunctionSummary = true,
                                    RiskCriteria = true,
                                    EventOwner = true,
                                    PenaltySummary = true,
                                    TaskSummary = true,
                                    ReviewerTaskSummary = true,
                                    CustomWidget = false,
                                    Department = true,
                                };
                                result = UserManagement.Create(swid);

                            if (result == false)
                            {
                                UserManagement.deleteUser(resultValue);
                                UserManagementRisk.deleteMstUser(resultValue);
                            }
                        }
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    User User = UserManagement.GetByID(Convert.ToInt32(user.ID));
                    if (tbxEmail.Text.Trim() != User.Email)
                    {
                        string message = SendNotificationEmailChanged(user);
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    }
                    result = UserManagement.Update(user, parameters);
                    result = UserManagementRisk.Update(mstUser, parametersRisk);

                    #region Department
                    List<int> DeptIds = new List<int>();
                    List<DepartmentMapping> objDepartmentMapping = new List<DepartmentMapping>();
                    foreach (RepeaterItem aItem in rptDepartment.Items)
                    {
                        CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                        if (chkDepartment.Checked)
                        {
                            DeptIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()));

                            DepartmentMapping DeptMapping = new DepartmentMapping()
                            {
                                UserID = user.ID,
                                DepartmentID = Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()),
                                IsActive = true,
                                CustomerID = cid,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                            };
                            objDepartmentMapping.Add(DeptMapping);
                        }
                    }
                    if (objDepartmentMapping.Count > 0)
                    {
                        Business.DepartmentHeadManagement.DeselectAllDepartment(user.ID);
                        Business.DepartmentHeadManagement.CreateDepartmentMapping(objDepartmentMapping);
                    }
                    #endregion

                    #region update groupname
                    if (!string.IsNullOrEmpty(rptUserGroup.SelectedValue) && rptUserGroup.SelectedValue != "-1")
                    {
                        List<mst_UserList_Group> objGroupMapping = new List<mst_UserList_Group>();
                        mst_UserList_Group GroupMapping = new mst_UserList_Group()
                        {
                            UserID = (int)user.ID,
                            GroupID = Convert.ToInt32(rptUserGroup.SelectedValue),
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            UpdatedOn = null,
                            UpdatedBy = null,
                        };
                        objGroupMapping.Add(GroupMapping);
                        if (objGroupMapping.Count > 0)
                        {
                            Business.IPAddBlock.DeselectAllGroupName(user.ID);
                            Business.IPAddBlock.CreateUserGroupNameMapping(objGroupMapping);
                        }
                    }
                    #endregion
                    
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }

                if (result)
                {
                    if (UserImageUpload.HasFile)
                    {
                        string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);
                        string filepath = "~/UserPhotos/" + fileName;
                        UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                        UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                    }
                }
                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string accessURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user.CustomerID));
                if (Urloutput != null)
                {
                    accessURL = Urloutput.URL;
                }
                else
                {
                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                      .Replace("@Username", user.Email)
                                      .Replace("@User", username)
                                      .Replace("@PortalURL", Convert.ToString(accessURL))
                                      .Replace("@Password", passwordText)
                                      .Replace("@From", ReplyEmailAddressName)
                                      .Replace("@URL", Convert.ToString(accessURL));
                                  
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }
                string accessURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user.CustomerID));
                if (Urloutput != null)
                {
                    accessURL = Urloutput.URL;
                }
                else
                {
                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                     .Replace("@Username", user.Email)
                                     .Replace("@User", username)
                                     .Replace("@PortalURL", Convert.ToString(accessURL))
                                     .Replace("@From", ReplyEmailAddressName)
                                     .Replace("@URL", Convert.ToString(accessURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        public event EventHandler OnSaved;

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);            
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvDept\").hide(\"blind\", null, 5, function () { });", true);
        }
    }
}