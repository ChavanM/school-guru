﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternalManagementDashboard.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.InternalManagementDashboard" %>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<style type="text/css">
    table th {
        border: 1px solid White;
        height: 20px;
        color: White;
        font-size: 12px;
        font-family: Tahoma;
    }

    .graphDiv {
        width: 340px;
        height: 200px;
        float: left;
        border: 1px solid #4297d7;
    }

    .igraphImg {
        text-align: center;
        margin-top: 75px;
    }

    .mainGraph {
        width: 80%;
        float: left;
    }
</style>

<script type="text/javascript">


    $(function () {

        $('#sendManagementEmail').dialog({
            height: 400,
            width: 600,
            autoOpen: false,
            draggable: true,
            title: "Email Management Dashboards",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });


        $('#divSummaryDetails').dialog({
            height: 500,
            width: 1200,
            autoOpen: false,
            draggable: true,
            title: "Detail View",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });

    });

    function SelectheaderCheckboxes(headerchk, gridname) {
        var rolecolumn;
        var chkheaderid = headerchk.id.split("_");


        var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");


        var i;

        if (headerchk.checked) {
            for (i = 0; i < gvcheck.rows.length; i++) {
                gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
            }
        }

        else {
            for (i = 0; i < gvcheck.rows.length; i++) {
                gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
            }
        }
    }

    function Selectchildcheckboxes(header, gridname) {
        var i;
        var count = 0;
        var rolecolumn;

        var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");

        var headerchk = document.getElementById(header);
        var chkheaderid = header.split("_");

        var rowcount = gvcheck.rows.length;

        for (i = 1; i < gvcheck.rows.length - 1; i++) {
            if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                count++;
            }
        }

        if (count == gvcheck.rows.length - 2) {
            headerchk.checked = true;
        }
        else {
            headerchk.checked = false;
        }
    }

    function DownloadDoc() {
        var count = 0;
        var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");
        for (var i = 1; i < gvcheck.rows.length; i++) {
            if (gvcheck.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked)
                count++;
        }
        if (count > 0)
            return true;
        else {
            alert("please select at least one record to download.");
            return false;
        }
    }

</script>

<asp:UpdateProgress ID="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<div style="margin: 10px 20px 10px 30px">
    <div style="margin-bottom: 4px">
        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
    </div>
    <div style="margin-bottom: 7px; margin-left: 20px;">
        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <label style="width: 150px; padding-top: 5px; display: block; float: left; font-size: 13px; color: #333;">
                        Dashboard as on</label>
                    <label style="margin-left: 65px;">
                        Select Year</label>
                    <asp:DropDownList runat="server" ID="ddlyear" Style="height: 22px; margin-left: 10px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged" />
                    <label style="margin-left: 10px;">
                        Select Month
                    </label>
                    <asp:DropDownList runat="server" ID="ddlmonths" Style="margin-left: 10px; padding: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlmonths_SelectedIndexChanged" />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="ddlyear" />
                <asp:PostBackTrigger ControlID="ddlmonths" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="email" runat="server" UpdateMode="Conditional" style="float: right; margin-right: 18px; margin-top: -25px;">
            <ContentTemplate>
                <asp:ImageButton ID="btnEmail" runat="server" Text="Email" AutoPostBack="false" ImageUrl="~/Images/email.png" Width="30px" Height="30px" OnClientClick="$('#sendManagementEmail').dialog('open');"></asp:ImageButton>

                <asp:ImageButton ID="btnExcel" runat="server" AutoPostBack="true" Text="Excel" OnClick="btnExcel_Click" ImageUrl="~/Images/excel.png" Width="30px" Height="30px"></asp:ImageButton>
                <asp:ImageButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="true" OnClick="btnPrint_Click" ImageUrl="~/Images/print.png" Width="30px" Height="30px"></asp:ImageButton>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExcel" />
                <asp:PostBackTrigger ControlID="btnPrint" />
            </Triggers>
        </asp:UpdatePanel>

    </div>
    <%-- <asp:UpdatePanel ID="upManagementDashboard" runat="server" OnLoad="upManagementDashboard_Load" UpdateMode="Conditional">
            <ContentTemplate>--%>
    <table runat="server" id="mainTable">
        <tr>

            <td style="vertical-align: top;">
                <asp:UpdatePanel ID="dsf" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TreeView runat="server" ID="tvLocation_Internal" BackColor="White" Width="225px" SelectedNodeStyle-Font-Bold="true"
                            NodeStyle-ForeColor="Black" Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvLocation_Internal_SelectedNodeChanged"
                            NodeStyle-NodeSpacing="5px">
                        </asp:TreeView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="tvLocation_Internal" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>

            <td style="vertical-align: top">
                <table id="tbDashboardSummary" runat="server">
                    <tr>
                        <td>
                            <table id="companyOverview" runat="server" style="height: 229px; width: 400px; border: 1px solid #4297d7; border-collapse: collapse;">
                                <tr>
                                    <th style="border-color: #4297d7;" class="ui-widget-header">Company Overview
                                    </th>
                                </tr>
                                <tr style="height: 20px;">
                                    <td style="border: 1px solid #4297d7;">
                                        <div style="margin-bottom: 7px; margin-left: 5px">
                                            <label style="width: 120px; display: block; float: left; font-size: 13px; color: #333;">
                                                Location
                                            </label>
                                            <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td style="border: 1px solid #4297d7;">
                                        <div style="margin-bottom: 7px; margin-left: 5px">
                                            <label style="width: 120px; display: block; float: left; font-size: 13px; color: #333;">
                                                Total No of Users
                                            </label>
                                            <asp:Label ID="lblTotalNoofUsers" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height: 70px; vertical-align: top;">
                                    <td style="border: 1px solid #4297d7;">
                                        <div style="margin-bottom: 7px; margin-left: 5px">
                                            <label style="width: 120px; display: block; float: left; font-size: 13px; color: #333;">
                                                Approver Name</label>
                                            <%--<asp:Label ID="lblReviewerName" runat="server"></asp:Label>--%>
                                            <div id="divApproverName" runat="server" style="float: left">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #4297d7; vertical-align: top;">
                                        <div style="margin-bottom: 7px; margin-left: 5px">
                                            <label style="width: 120px; display: block; float: left; font-size: 13px; color: #333;">
                                                Reviewer Name</label>
                                            <div id="divReviewerName" runat="server" style="float: left">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table style="height: 229px; width: 640px; margin-left: 10px; border: 1px solid #4297d7; border-collapse: collapse;">
                                <tr>
                                    <th style="border-color: #4297d7;" class="ui-widget-header">Compliance Summary
                                    </th>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <div style="margin-top: 10px">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView runat="server" ID="grdComplianceSummary" AutoGenerateColumns="false"
                                                        ShowFooter="true" AllowSorting="true" GridLines="Vertical" OnRowDataBound="grdComplianceSummary_RowDataBound"
                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCommand="grdComplianceSummary_RowCommand"
                                                        CellPadding="4" ForeColor="Black" AllowPaging="True" Width="100%" Size="8px"
                                                        DataKeyNames="Catagory">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" ItemStyle-Width="150px" FooterStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSumaryCatagory" runat="server" Text='<%#Eval("Catagory") %>' />
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text="Total" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="High" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%-- <asp:Label ID="lblSumaryHigh" runat="server" Text='<%#Eval("High") %>' />--%>
                                                                    <asp:LinkButton ID="lblSumaryHigh" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "High;" + Eval("ID") +";"+ Eval("Catagory") %>'
                                                                        Text='<%# Eval("High") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%--<asp:Label ID="lblSHigh" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblSHigh" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "High;" + Eval("ID") +";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Medium" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%--     <asp:Label ID="lblSumaryMedium" runat="server" Text='<%#Eval("Medium") %>' />--%>
                                                                    <asp:LinkButton ID="lblSumaryMedium" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Medium;" + Eval("ID") +";"+ Eval("Catagory")%>'
                                                                        Text='<%# Eval("Medium") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%--<asp:Label ID="lblSMedium" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblSMedium" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Medium;" + Eval("ID") +";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Low" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%--<asp:Label ID="lblSumaryLow" runat="server" Text='<%#Eval("Low") %>' />--%>
                                                                    <asp:LinkButton ID="lblSumaryLow" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Low;" + Eval("ID") +";"+ Eval("Catagory")%>'
                                                                        Text='<%# Eval("Low") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%-- <asp:Label ID="lblSLow" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblSLow" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Low;" + Eval("ID") +";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <%-- <asp:Label ID="lblSumaryTotal" runat="server" Text='<%#Eval("Total") %>' />--%>
                                                                    <asp:LinkButton ID="lblSumaryTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("ID") +";Total"%>'
                                                                        Text='<%# Eval("Total") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%-- <asp:Label ID="lblSummarygrandTotal" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblSummarygrandTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("ID")+";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#F7F7DE" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top" colspan="2">
                            <table id="tblComplianceStatus" runat="server" style="margin-top: 10px; border: 1px solid #4297d7; width: 100%; border-collapse: collapse;">
                                <tr>
                                    <th style="border-color: #4297d7; height: 10px" class="ui-widget-header" colspan="2">Compliance Status
                                    </th>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; width: 30%">
                                        <div style="margin-top: 10px">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="Label1" runat="server" Style="display: block; font-weight: bold; font-size: 12px; color: #333;"
                                                        Text="Per Risk Criteria"></asp:Label>
                                                    <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false"
                                                        ShowFooter="true" AllowSorting="true" GridLines="Vertical" OnRowDataBound="grdComplianceTransactions_RowDataBound"
                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCommand="grdComplianceTransactions_RowCommand"
                                                        CellPadding="4" ForeColor="Black" AllowPaging="True" Width="100%" Size="8px"
                                                        DataKeyNames="RiskCatagory">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" ItemStyle-Width="150px" FooterStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRiskCatagory" runat="server" Text='<%#Eval("RiskCatagory") %>' />

                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text="Total" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Completed in Time" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblRiskCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "InTime;" + Eval("ID")+";"+Eval("RiskCatagory")%>'
                                                                        Text='<%# Eval("Completed") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%--<asp:Label ID="lblCompleted" Style="text-align: center" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "InTime;" + Eval("ID")+";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Completed after Due Date" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblRiskDelayed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Delayed;" + Eval("ID")+";"+Eval("RiskCatagory")%>'
                                                                        Text='<%# Eval("Delayed") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%--  <asp:Label ID="lblDelayed" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblDelayed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Delayed;" + Eval("ID")+";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Not Yet Completed" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblRiskPending" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Pending;" + Eval("ID")+";"+Eval("RiskCatagory")%>'
                                                                        Text='<%# Eval("Pending") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%--<asp:Label ID="lblPending" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblPending" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Pending;" + Eval("ID")+";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblRiskTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("ID")+";"+Eval("RiskCatagory")%>'
                                                                        Text='<%# Eval("Total") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%-- <asp:Label ID="lblgrandTotal" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblgrandTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("ID")+";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>

                                                        <FooterStyle BackColor="#F7F7DE" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div style="margin-top: 20px; margin-bottom: 10px;">
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="Label2" runat="server" Style="display: block; font-weight: bold; font-size: 12px; color: #333;"
                                                        Text="Per Functions"></asp:Label>
                                                    <asp:GridView runat="server" ID="grdMangementDashFunctionwise" AutoGenerateColumns="false"
                                                        ShowFooter="true" AllowSorting="true" GridLines="Vertical" OnRowDataBound="grdMangementDashFunctionwise_RowDataBound"
                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCommand="grdMangementDashFunctionwise_RowCommand"
                                                        CellPadding="4" ForeColor="Black" AllowPaging="True" Width="100%" Size="8px"
                                                        DataKeyNames="RiskCatagory">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" ItemStyle-Width="150px" FooterStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblfCatagory" runat="server" Text='<%#Eval("RiskCatagory") %>' />
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lblfunctionTotal" runat="server" Text="Total" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Completed in Time" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%--<asp:Label ID="lblfCompleted" runat="server" Text='<%#Eval("Completed") %>' />--%>
                                                                    <asp:LinkButton ID="lblfCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "InTime;" + Eval("ID")+";"+ Eval("RiskCatagory") %>'
                                                                        Text='<%# Eval("Completed") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%-- <asp:Label ID="lblfunctionCompleted" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblfunctionCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "InTime;" + Eval("ID")+";Total" %>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Completed after Due Date" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%-- <asp:Label ID="lblfDelayed" runat="server" Text='<%#Eval("Delayed") %>' />--%>
                                                                    <asp:LinkButton ID="lblfDelayed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Delayed;" + Eval("ID")+";"+ Eval("RiskCatagory") %>'
                                                                        Text='<%# Eval("Delayed") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%--<asp:Label ID="lblfunctionDelayed" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblfunctionDelayed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Delayed;" + Eval("ID")+";Total"%>'
                                                                        Text='<%# Eval("Delayed") %>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Not Yet Completed" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%-- <asp:Label ID="lblfPending" runat="server" Text='<%#Eval("Pending") %>' />--%>
                                                                    <asp:LinkButton ID="lblfPending" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Pending;" + Eval("ID") +";"+ Eval("RiskCatagory")%>'
                                                                        Text='<%# Eval("Pending") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%--<asp:Label ID="lblfunctionPending" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblfunctionPending" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Pending;" + Eval("ID")+";Total" %>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <%-- <asp:Label ID="lblfTotal" runat="server" Text='<%#Eval("Total") %>' />--%>
                                                                    <asp:LinkButton ID="lblfTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("ID") +";"+ Eval("RiskCatagory")%>'
                                                                        Text='<%# Eval("Total") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <%-- <asp:Label ID="lblfunctiongrandTotal" runat="server" />--%>
                                                                    <asp:LinkButton ID="lblfunctiongrandTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("ID") +";Total"%>'></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#F7F7DE" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </td>
                                    <td style="vertical-align: top">
                                        <div style="float: right; margin-top: 5px;">
                                            <div style="float: left; width: 100px;">
                                                <asp:Label runat="server">Completed</asp:Label>
                                                <div style="background-color: #70AD47; width: 15px; height: 15px; margin-left: 5px; float: left;">
                                                </div>
                                            </div>
                                            <div style="float: left; width: 100px;">
                                                <asp:Label runat="server">Delayed</asp:Label>
                                                <div style="background-color: #FFC000; width: 15px; height: 15px; margin-left: 5px; float: left;">
                                                </div>
                                            </div>
                                            <div style="float: left; width: 100px;">
                                                <asp:Label runat="server">Pending</asp:Label>
                                                <div style="background-color: #FF3300; width: 15px; height: 15px; margin-left: 5px; float: left;">
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel runat="server" ID="pnlGraph" Style="margin-left: 18px; margin-top: 24px;">
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <div>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px;">
                                <label style="margin-left: 232px; padding-top: 5px; display: block; font-weight: bold; float: left; font-size: 12px; color: #333;">
                                    For Past 12 Month's Summary</label>
                                <asp:Button Style="margin-left: 40px" ID="btnTwelthMonthSummary" runat="server" Text="Click Here"
                                    OnClick="btnTwelthMonthSummary_Click" />
                            </div>
                            <div id="divTelweMonthSummary" runat="server" visible="false" style="margin-left: 232px;">

                                <asp:GridView runat="server" ID="grdComplianceTwelveMonthSummary" AutoGenerateColumns="false" OnRowDataBound="grdComplianceTwelveMonthSummary_RowDataBound"
                                    AllowSorting="false" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" OnRowCommand="grdComplianceTwelveMonthSummary_RowCommand"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True"
                                    Width="100%" Size="8px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="" FooterStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblfcompliance" runat="server" Text='<%#Eval("Compliances") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%-- <asp:Label ID="a12" runat="server" Text='<%#Eval("12") %>' />--%>
                                                <asp:LinkButton ID="a12" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances") + ";1;" + Eval("Compliances") %>'
                                                    Text='<%#Eval("12") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="a11" runat="server" Text='<%#Eval("11") %>' />--%>
                                                <asp:LinkButton ID="a11" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances") + ";2;"+ Eval("Compliances") %>'
                                                    Text='<%#Eval("11") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="a10" runat="server" Text='<%#Eval("10") %>' />--%>
                                                <asp:LinkButton ID="a10" runat="server" CommandName="DRILLDOWN" CommandArgument='<%#Eval("Compliances") + ";3;"+ Eval("Compliances") %>'
                                                    Text='<%#Eval("10") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="a9" runat="server" Text='<%#Eval("9") %>' />--%>
                                                <asp:LinkButton ID="a9" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances")+ ";4;"+ Eval("Compliances")%>'
                                                    Text='<%#Eval("9") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="a8" runat="server" Text='<%#Eval("8") %>' />--%>
                                                <asp:LinkButton ID="a8" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances") + ";5;"+ Eval("Compliances")  %>'
                                                    Text='<%#Eval("9") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="a7" runat="server" Text='<%#Eval("7") %>' />--%>
                                                <asp:LinkButton ID="a7" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances") + ";6;" + Eval("Compliances")%>'
                                                    Text='<%#Eval("7") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%--  <asp:Label ID="a6" runat="server" Text='<%#Eval("6") %>' />--%>
                                                <asp:LinkButton ID="a6" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances") + ";7;"  + Eval("Compliances")%>'
                                                    Text='<%#Eval("6") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="a5" runat="server" Text='<%#Eval("5") %>' />--%>
                                                <asp:LinkButton ID="a5" runat="server" CommandName="DRILLDOWN" CommandArgument='<%#  Eval("Compliances") + ";8;"+ Eval("Compliances")%>'
                                                    Text='<%#Eval("5") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%-- <asp:Label ID="a4" runat="server" Text='<%#Eval("4") %>' />--%>
                                                <asp:LinkButton ID="a4" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances")+ ";9;"+ Eval("Compliances") %>'
                                                    Text='<%#Eval("4") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%-- <asp:Label ID="a3" runat="server" Text='<%#Eval("3") %>' />--%>
                                                <asp:LinkButton ID="a3" runat="server" CommandName="DRILLDOWN" CommandArgument='<%#  Eval("Compliances") + ";10;"+ Eval("Compliances") %>'
                                                    Text='<%#Eval("3") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%-- <asp:Label ID="a2" runat="server" Text='<%#Eval("2") %>' />--%>
                                                <asp:LinkButton ID="a2" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances")+ ";11;" + Eval("Compliances")%>'
                                                    Text='<%#Eval("2") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="a1" runat="server" Text='<%#Eval("1") %>' />--%>
                                                <asp:LinkButton ID="a1" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("Compliances")+";12;"+ Eval("Compliances") %>'
                                                    Text='<%#Eval("1") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <RowStyle HorizontalAlign="Center" />
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="margin-top: 15px; margin-left: 232px; float: left; margin-right: 60px;">
                    <asp:Label ID="lblEmailNotification" runat="server" Style="font-size: 11px; padding-left: 5px;"></asp:Label>
                    <asp:Label ID="lblemailError" runat="server" Style="font-size: 11px; color: red; padding-left: 5px;"></asp:Label><br />
                    <asp:TextBox ID="txtaMangementRemark" TextMode="multiline" Columns="50" Rows="3"
                        runat="server" />
                </div>
                <div style="margin-top: 30px;">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnMangementEmail" runat="server" Width="100px" Text="Email" OnClick="btnMangementEmail_Click" />
                            <br />
                            <asp:Button ID="BtnManagementNotification" runat="server" Width="100px" Text="Notification" Style="margin-top: 8px"
                                Enabled="false" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnMangementEmail" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </td>
        </tr>
    </table>
    <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    <div id="sendManagementEmail">
        <asp:UpdatePanel ID="upsendmail" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                        ValidationGroup="ManagementValidationGroup" />
                    <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                        ValidationGroup="ManagementValidationGroup" Display="None" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Enter Email Ids(comma seperated)</label>
                    <asp:TextBox ID="emailids" runat="server" TextMode="MultiLine" Height="200px" Columns="80" Rows="3" Width="350px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmailID" ErrorMessage="Please enter email ids in comma seperated." ControlToValidate="emailids"
                        runat="server" ValidationGroup="ManagementValidationGroup" Display="None" />
                </div>
                <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 10px">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnEmail_Click" CssClass="button"
                        ValidationGroup="ManagementValidationGroup" />
                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#sendManagementEmail').dialog('close');" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<div id="divSummaryDetails">
    <div style="float: right; margin-bottom: 5px; margin-left: 10px">
        <asp:Button Text="Download" runat="server" ID="btnDownload" OnClick="btnDownload_Click" ToolTip="Click here to download documnet for selected compliances."
            OnClientClick="javascript:return DownloadDoc();" CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
    </div>
    <div style="float: right">
        <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
                    title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
    </div>
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 10px; font-weight: bold; font-size: 12px;">
                <asp:Label runat="server" ID="lblDetailViewTitle"></asp:Label>
            </div>

            <asp:GridView runat="server" ID="grdSummaryDetails" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCommand="grdSummaryDetails_RowCommand" OnPageIndexChanging="grdSummaryDetails_PageIndexChanging"
                CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="12" Width="100%" OnRowDataBound="grdSummaryDetails_RowDataBound"
                Font-Size="12px" DataKeyNames="InternalComplianceInstanceID">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkCompliancesHeader" runat="server" onclick="javascript:SelectheaderCheckboxes(this,'grdSummaryDetails')" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCompliances" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Branch" HeaderText="Location" SortExpression="Branch" />
                    <asp:TemplateField HeaderText="Compliance" ItemStyle-Width="30%" SortExpression="ShortDescription">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="User" HeaderText="Performer Name" ItemStyle-HorizontalAlign="Center" SortExpression="User" />
                    <asp:TemplateField HeaderText="Due Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="InternalScheduledOn">
                        <ItemTemplate>
                            <%# Eval("InternalScheduledOn")!= null?((DateTime)Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy"):""%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ForMonth" HeaderText="For Month" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                    <asp:BoundField DataField="RiskCategory" HeaderText="Risk Category" SortExpression="RiskCategory" />
                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblDownLoadfile" runat="server" CommandName="Download" CommandArgument='<%# Eval("InternalScheduledOnID") %>'
                                ToolTip="Please download file from here." Visible='<%# ViewDownload((int)Eval("InternalComplianceStatusID")) %>'>Download</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
