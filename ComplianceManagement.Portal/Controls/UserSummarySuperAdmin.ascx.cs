﻿using System;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class UserSummarySuperAdmin : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUsersPerCustomer();
                BindEntitiesPerCustomer();
            }
        }

        private void BindEntitiesPerCustomer()
        {
            try
            {
                chrtEntitiesPerCustomers.DataSource = SummaryManagement.GetEntitiesPerCustomersSummaryForSuperAdmin();
                chrtEntitiesPerCustomers.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindUsersPerCustomer()
        {
            try
            {
                chrtUsersPerCustomer.DataSource = SummaryManagement.GetUsersPerCustomerSummaryForSuperAdmin();
                chrtUsersPerCustomer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}