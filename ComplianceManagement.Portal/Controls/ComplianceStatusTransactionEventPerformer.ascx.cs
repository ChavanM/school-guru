﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class ComplianceStatusTransactionEventPerformer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                lblPenalty.Text = string.Empty;
                lblRisk.Text = string.Empty;
                lbDownloadSample.Text = string.Empty;

                ViewState["complianceInstanceID"] = ScheduledOnID;
                var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
                var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                if (complianceInfo !=null)
                {
                    lblComplianceDiscription.Text = complianceInfo.Description;
                    string Penalty = Business.ComplianceManagement.GetPanalty(complianceInfo);
                    lblPenalty.Text = Penalty;
                    string risk = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);
                    lblRisk.Text = risk;
                    var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
                    if (ActInfo != null)
                    {
                        lblActName.Text = ActInfo.Name;
                    }
                    lblRule.Text = complianceInfo.Sections;
                    lblFormNumber.Text = complianceInfo.RequiredForms;                   
                }               
                if (RecentComplianceTransaction.ComplianceStatusID == 10)
                {
                    rfvFile.Visible = false;
                    BindDocument(ScheduledOnID);
                }
                else
                {
                    rfvFile.Visible = true;
                    divDeleteDocument.Visible = false;
                    rptComplianceDocumnets.DataSource =null;
                    rptComplianceDocumnets.DataBind();

                    rptWorkingFiles.DataSource = null;
                    rptWorkingFiles.DataBind();
                }

                if (complianceInfo.UploadDocument == true && complianceForm != null)
                {
                    lbDownloadSample.Text = "Click <u>here</u> to download sample form.";
                    lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                    lblNote.Visible = true;
                }
                else
                {
                    lblNote.Visible = false;
                }

                btnSave.Attributes.Remove("disabled");

                if (RecentComplianceTransaction.ComplianceStatusID == 1 || RecentComplianceTransaction.ComplianceStatusID == 6 || RecentComplianceTransaction.ComplianceStatusID == 10)
                {
                    if (complianceInfo.UpDocs == true)
                    {
                        divUploadDocument.Visible = true;
                        divWorkingfiles.Visible = true;
                        btnSave.Visible = true;
                        btnSaveDOCNotCompulsory.Visible = false;
                    }
                    else
                    {
                        rfvFile.Visible=false;
                        btnSaveDOCNotCompulsory.Visible = true;
                        btnSave.Visible = false;
                        divUploadDocument.Visible = true;
                        divWorkingfiles.Visible = true;
                    }
                   
                    //rfvFile.Enabled = (complianceInfo.UploadDocument ?? false);
                }
                else if (RecentComplianceTransaction.ComplianceStatusID != 1)
                {
                    divUploadDocument.Visible = false;
                    divWorkingfiles.Visible = false;
                    if ((complianceInfo.UploadDocument ?? false))
                    {
                        btnSave.Attributes.Add("disabled", "disabled");
                    }
                }

                BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));
                BindTransactions(ScheduledOnID);
                tbxRemarks.Text = string.Empty;
                hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divComplianceDetailsDialog\").dialog('open');", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindDocument(int ScheduledOnID)
        {
            try
            {
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                divDeleteDocument.Visible = true;
                ComplianceDocument = DocumentManagement.GetFileData1(ScheduledOnID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = ComplianceDocument.Where(entry => entry.FileType == 2).ToList();
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["complianceInstanceID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "Name";
                ddlStatus.DataValueField = "ID";

                var statusList = ComplianceStatusManagement.GetStatusList();

                List<ComplianceStatu> allowedStatusList = null;

                List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry=>entry.Name).ToList();

                ddlStatus.DataSource = allowedStatusList;
                ddlStatus.DataBind();

                ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
            try
            {
                long? StatusID = ComplianceManagement.Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID;

                ComplianceTransaction transaction = new ComplianceTransaction()
                {
                    ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                    ComplianceInstanceId =  Convert.ToInt64(hdnComplianceInstanceID.Value),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = Convert.ToInt32(ddlStatus.SelectedValue),
                    StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                    Remarks = tbxRemarks.Text
                };

         
                List<FileData> files = new List<FileData>();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                
                HttpFileCollection fileCollection = Request.Files;
                bool blankfileCount = true;

                if (fileCollection.Count > 0)
                {
                    int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                    var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                    string directoryPath = null;
                    string version = null;
                    if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                    {
                        version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                        directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + version);
                    }
                    else
                    {
                        version = "1.0";
                        directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                    }

                    DocumentManagement.CreateDirectory(directoryPath);

                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = fileCollection[i];
                        string[] keys = fileCollection.Keys[i].Split('$');
                        String fileName = "";
                        if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                        {
                            fileName = "ComplianceDoc_" + uploadfile.FileName;
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                        }
                        else
                        {
                            fileName = "WorkingFiles_" + uploadfile.FileName;
                            list.Add(new KeyValuePair<string, int>(fileName, 2));
                        }

                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                        Stream fs = uploadfile.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                        if (uploadfile.ContentLength > 0)
                        {
                            FileData file = new FileData()
                            {
                                Name = fileName,
                                FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                FileKey = fileKey.ToString(),
                                //Version = "1.0",
                                Version = version,
                                VersionDate = DateTime.UtcNow,
                                FileSize= uploadfile.ContentLength,
                            };

                            files.Add(file);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(uploadfile.FileName))
                                blankfileCount = false;
                        }
                    }
                }

                //Comment by Sachin for rejected version save

                //if (fileCollection.Count > 0)
                //{
                //    int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                //    var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                //    //string version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));

                //    string directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/1.0");
                //    DocumentManagement.CreateDirectory(directoryPath);

                //    for (int i = 0; i < fileCollection.Count; i++)
                //    {
                //        HttpPostedFile uploadfile = fileCollection[i];
                //        string[] keys = fileCollection.Keys[i].Split('$');
                //        String fileName = "";
                //        if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                //        {
                //            fileName = "ComplianceDoc_" + uploadfile.FileName;
                //            list.Add(new KeyValuePair<string, int>(fileName, 1));
                //        }
                //        else
                //        {
                //            fileName = "WorkingFiles_" + uploadfile.FileName;
                //            list.Add(new KeyValuePair<string, int>(fileName, 2));
                //        }
                //        Guid fileKey = Guid.NewGuid();
                //        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                //        Stream fs = uploadfile.InputStream;
                //        BinaryReader br = new BinaryReader(fs);
                //        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                //        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                //        if (uploadfile.ContentLength > 0)
                //        {
                //            FileData file = new FileData()
                //            {
                //                Name = fileName,
                //                FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                //                FileKey = fileKey.ToString(),
                //                Version = "1.0",
                //                VersionDate = DateTime.UtcNow,
                //            };
                //            files.Add(file);
                //        }
                //        else
                //        {
                //            if (!string.IsNullOrEmpty(uploadfile.FileName))
                //                blankfileCount = false;
                //        }
                //    }
                //}

                bool flag = false;
                if (blankfileCount)
                {
                   flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist);

                   upComplianceDetails.Update();
                   //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceTypeDialog\").dialog('close')", true);
                   ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseDialog", "$(\"#divComplianceDetailsDialog\").dialog('close');", true);
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                }

                //bool flag = true;
                if (flag != true)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }

                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }

                //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (OnSaved != null)
            {
                OnSaved(this, null);
            }
        }
        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               
                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.Data); // create the file
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer", string.Format("initializeDatePickerforPerformer(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer", "initializeDatePickerforPerformer(null);", true);
                }

                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                
                tbxRemarks.Text = tbxDate.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;
       
        protected void btnUpload_Click(object sender, EventArgs e)
        {

            
            HttpFileCollection fileCollection = Request.Files;
            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile uploadfile = fileCollection[i];
                string fileName = Path.GetFileName(uploadfile.FileName);
                if (uploadfile.ContentLength > 0)
                {
                    //uploadfile.SaveAs(Server.MapPath("~/UploadFiles/") + fileName);
                    //lblMessage.Text += fileName + "Saved Successfully<br>";
                }
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["complianceInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("Download"))
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            else if (e.CommandName.Equals("Delete"))
            {
                DeleteFile(Convert.ToInt32(e.CommandArgument));
                BindDocument(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }


            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file != null)
                {
                    string path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    DocumentManagement.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkDocbutton = (LinkButton)e.Item.FindControl("lbtLinkDocbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
                
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkbutton = (LinkButton)e.Item.FindControl("lbtLinkbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkbutton);
            }
        }   

    }
}