﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using System.Data;
using System.Web.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class ComplianceStatusTransaction : System.Web.UI.UserControl
    {
        static string sampleFormPath = "";
        public static string subTaskDocViewPath = "";
        public static List<long> Tasklist = new List<long>();
        protected string UploadDocumentLink;
        bool IsPenaltyVisible = true;
        protected static bool IsRemarkCompulsary = false;
        protected static string ActDocString;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";

                if (!this.IsPostBack)
                {
                    // fuSampleFile.Attributes["onchange"] = "UploadFileStatus(this)";

                    DateTime date = DateTime.MinValue;
                    if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", string.Format("initializeDatePickerforPerformer1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", "initializeDatePickerforPerformer1(null);", true);
                    }
                    // ddlStatus_SelectedIndexChanged(null, null);
                    //// chkPenaltySave_CheckedChanged(null, null);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }


        //protected void Upload(object sender, EventArgs e)
        //{
        //    if (fuSampleFile.HasFile)
        //    {
        //        string[] validFileTypes = { "exe", "bat", "dll" };

        //        string ext = System.IO.Path.GetExtension(fuSampleFile.PostedFile.FileName);
        //        bool isValidFile = true;
        //        for (int i = 0; i < validFileTypes.Length; i++)
        //        {
        //            if (ext == "." + validFileTypes[i])
        //            {
        //                isValidFile = false;
        //                break;
        //            }
        //        }
        //        if (!isValidFile)
        //        {
        //            cvDuplicateEntry.IsValid = false;
        //            cvDuplicateEntry.ErrorMessage = "Invalid file uploded. .exe,.bat formats not supported.";
        //            //cvDuplicateEntry.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
        //        }
        //    }
        //}

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {

                grdDocument.DataSource = null;
                grdDocument.DataBind();
                lblPenalty.Text = string.Empty;
                lblRisk.Text = string.Empty;
                lbDownloadSample.Text = string.Empty;
                chkPenaltySave.Checked = false;
                txtInterest.Text = "";
                txtPenalty.Text = "";
                txtInterest.ReadOnly = false;
                txtPenalty.ReadOnly = false;
                fieldsetpenalty.Visible = false;

                int customerID = -1;
                int customerbranchID = -1;

                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);             
                var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);               
                if (AllComData != null)
                {
                    customerbranchID = AllComData.CustomerBranchID;
                    lblLocation.Text = AllComData.Branch;
                    lblDueDate.Text = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MMM-yyyy");
                    hiddenDueDate.Value = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MM-yyyy");
                    lblPeriod.Text = AllComData.ForMonth;
                }         
                var IsAfter = TaskManagment.IsTaskAfter(ScheduledOnID, 3, lblPeriod.Text, customerbranchID, customerID);
                lbltask.Text = "";
                var showHideButton = false;
                if (IsAfter == false)
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                    if (showHideButton == false)
                    {
                        lbltask.Text = "Compliance releted task is not yet completed.";
                    }
                    btnSave.Enabled = showHideButton;
                }
                else
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                    lbltask.Text = "";
                    btnSave.Enabled = true;
                }

                BindTempDocumentData(ScheduledOnID);            
                ViewState["IsAfter"] = IsAfter;
                ViewState["Period"] = lblPeriod.Text;
                ViewState["CustomerBrachID"] = customerbranchID;              
                var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
                var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                if (complianceInfo != null)
                {                 
                    lblComplianceID.Text =Convert.ToString(complianceInfo.ID);
                    lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
                    lblComplianceDiscription.Text = complianceInfo.ShortDescription;
                    lblDetailedDiscription.Text = complianceInfo.Description;
                    lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));                    
                    lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);                    
                    lblPenalty.Text = complianceInfo.PenaltyDescription;
                    string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceInstanceID);
                    lblRiskType.Text = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);
                    lblRisk.Text = risk;
                    var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
                    if (ActInfo != null)
                    {
                        lblActName.Text = ActInfo.Name;
                    }

                    lblNatureofcompliance.Text = "";
                    txtValueAsPerSystem.Text = "";
                    txtValueAsPerReturn.Text = "";
                    txtLiabilityPaid.Text = "";
                    txtDiffAB.Text = "";
                    txtDiffBC.Text = "";

                    if (complianceInfo.NatureOfCompliance != null)
                    {
                        lblNatureofcomplianceID.Text = Convert.ToString(complianceInfo.NatureOfCompliance);

                        //lblNatureofcompliance.Text = Business.ComplianceManagement.GetNatureOfCompliance(Convert.ToInt32(complianceInfo.NatureOfCompliance));
                        string Nature = Business.ComplianceManagement.GetNatureOfComplianceFromID(Convert.ToInt32(complianceInfo.NatureOfCompliance));
                        lblNatureofcompliance.Text = Nature.ToString();

                        if (complianceInfo.NatureOfCompliance == 1)
                        {
                            //fieldsetTDS.Visible = true;
                            //lblLiabilityPaid.Visible = true;
                            //txtLiabilityPaid.Visible = true;
                            //lbldiffBC.Visible = true;
                            //txtLiabilityPaid.Visible = true;
                            //txtDiffBC.Visible = true;
                            //tdLiabilityPaidSC.Visible = true;
                            //tddiffBCSC.Visible = true;
                            trreturn.Visible = true;
                            lblValueAsPerSystem.InnerText = "Liability as per system (A)";
                            lblValueAsPerReturn.InnerText = "Liability as per return (B)";
                        }
                        else if (complianceInfo.NatureOfCompliance == 0)
                        {
                            fieldsetTDS.Visible = true;
                            lblValueAsPerSystem.InnerText = "Values as per system (A)";
                            lblValueAsPerReturn.InnerText = "Values as per return (B)";
                            trreturn.Visible = false;
                            //lblLiabilityPaid.Visible = false;
                            //txtLiabilityPaid.Visible = false;
                            //lbldiffBC.Visible = false;
                            //txtLiabilityPaid.Visible = false;
                            //tdLiabilityPaidSC.Visible = false;
                            //txtDiffBC.Visible = false;
                            //tddiffBCSC.Visible = false;
                        }
                        else
                        {
                            fieldsetTDS.Visible = false;
                        }
                    }
                    else
                    {
                        fieldsetTDS.Visible = false;
                    }

                    if (risk == "HIGH")
                    {
                        divRiskType.Attributes["style"] = "background-color:red;";
                    }
                    else if (risk == "MEDIUM")
                    {
                        divRiskType.Attributes["style"] = "background-color:yellow;";
                    }
                    else if (risk == "LOW")
                    {
                        divRiskType.Attributes["style"] = "background-color:green;";
                    }
                    lblRule.Text = complianceInfo.Sections;
                    lblFormNumber.Text = complianceInfo.RequiredForms;
                }

                var AuditChecklistName = Business.ComplianceManagement.GetAuditChecklistName(complianceInfo.ID,AuthenticationHelper.CustomerID);

                if(!string.IsNullOrEmpty(AuditChecklistName))
                { 
                    lblAuditChecklist.Text = AuditChecklistName;
                    trAuditChecklist.Visible = true;
                }
                else
                {
                    trAuditChecklist.Visible = false;
                }

                if (RecentComplianceTransaction.ComplianceStatusID == 10)
                {
                    //rfvFile.Visible = false;
                    BindDocument(ScheduledOnID);
                }
                else
                {
                    //rfvFile.Visible = true;
                    divDeleteDocument.Visible = false;
                    rptComplianceDocumnets.DataSource = null;
                    rptComplianceDocumnets.DataBind();

                    rptWorkingFiles.DataSource = null;
                    rptWorkingFiles.DataBind();
                }

                if (complianceInfo.UploadDocument == true && complianceForm != null)
                {
                   // lbDownloadSample.Text = "<u style=color:blue>Click here</u> to download sample form.";
                    lbDownloadSample.Text = "Download";
                    lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                    sampleFormPath = complianceForm.FilePath;
                    sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2);
                
                    lblNote.Visible = true;
                    lnkViewSampleForm.Visible = true;
                    lblSlash.Visible = true;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        lblpathsample.Text = sampleFormPath;                       
                    }
                    else
                    {
                        lblpathsample.Text = sampleFormPath;
                    }

                    var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample.CommandArgument));

                    rptComplianceSampleView.DataSource = complianceform;
                    rptComplianceSampleView.DataBind();
                }
                else
                {
                    lblNote.Visible = false;
                    lblSlash.Visible = false;
                    lnkViewSampleForm.Visible = false;
                    sampleFormPath = "";
                }

                btnSave.Attributes.Remove("disabled");
                
                if (RecentComplianceTransaction.ComplianceStatusID == 1 || RecentComplianceTransaction.ComplianceStatusID == 6 || RecentComplianceTransaction.ComplianceStatusID == 10 || RecentComplianceTransaction.ComplianceStatusID == 13 || RecentComplianceTransaction.ComplianceStatusID == 14)
                {
                    if (complianceInfo.EventFlag == true)
                    {
                        if (complianceInfo.UpDocs == true)
                        {
                            divUploadDocument.Visible = true;
                            divWorkingfiles.Visible = true;
                            btnSave.Visible = true;
                            btnSaveDOCNotCompulsory.Visible = false;
                            lblDocComplasary.Visible = true;
                        }
                        else
                        {
                            //rfvFile.Visible = false;
                            btnSaveDOCNotCompulsory.Visible = true;
                            btnSave.Visible = false;
                            divUploadDocument.Visible = true;
                            divWorkingfiles.Visible = true;
                            lblDocComplasary.Visible = false;
                        }
                    }
                    else
                    {
                        divUploadDocument.Visible = true;
                        divWorkingfiles.Visible = true;
                        btnSave.Visible = true;
                        btnSaveDOCNotCompulsory.Visible = false;
                        lblDocComplasary.Visible = true;
                    }
                    //rfvFile.Enabled = (complianceInfo.UploadDocument ?? false);
                }
                else if (RecentComplianceTransaction.ComplianceStatusID != 1)
                {
                    divUploadDocument.Visible = false;
                    divWorkingfiles.Visible = false;
                    if ((complianceInfo.UploadDocument ?? false))
                    {
                        btnSave.Attributes.Add("disabled", "disabled");
                    }
                }                
                if (!string.IsNullOrEmpty(Session["Interimdays"] as string))
                {
                    if (Session["Interimdays"] !=null)
                    {
                        int EscDays = Convert.ToInt32(Session["Interimdays"]);
                    }                  
                    BindEscalationStatusList();
                    fuSampleFile.Enabled = false;
                    FileUpload1.Enabled = true;
                }
                else
                {
                    fuSampleFile.Enabled = true;
                    FileUpload1.Enabled = true;
                    BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));
                }
                //Enteriem approve
                if (RecentComplianceTransaction.ComplianceStatusID == 13 )
                {
                    fuSampleFile.Enabled = true;
                    FileUpload1.Enabled = false;
                    BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));
                }

                //Enteriem reject
                if (RecentComplianceTransaction.ComplianceStatusID == 14)
                {
                    fuSampleFile.Enabled = false;
                    FileUpload1.Enabled = true;
                    BindEscalationStatusList();
                }

                BindTransactions(ScheduledOnID);
                tbxRemarks.Text = string.Empty;
                hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                #region Act_Document

                PopulateTreeView(complianceInfo.ActID);

                #endregion
                upComplianceDetails.Update();
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void PopulateTreeView(int ActID)
        {

            var MasterQuery = ActManagement.getFileNamebyID(ActID);
            var DistinctTransQuery = MasterQuery;
            DistinctTransQuery = DistinctTransQuery.GroupBy(x => x.DocumentTypeID).Select(x => x.FirstOrDefault()).ToList();
            System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
            stringbuilder.Append(@"<table id='basic' width='100%'>");
            foreach (var item in DistinctTransQuery)
            {
                stringbuilder.Append("<tr data-node-id=" + Convert.ToString(item.DocumentTypeID) + ">" +
                            " <td class='locationheadLocationbg'>" + Convert.ToString(item.DocumentType) + "</td>");
                stringbuilder.Append("</tr>");
                var actDocVersionData = MasterQuery.Where(entry => entry.Act_ID == ActID && entry.DocumentTypeID == item.DocumentTypeID).ToList();
                int i = 0;
                foreach (var item1 in actDocVersionData)
                {
                    i += 1;
                    string id = Convert.ToString(item.DocumentTypeID) + "." + i;
                    string FileName = "";
                    string[] filename = item1.FileName.Split('.');
                    string str = filename[0] + i + "." + filename[1];
                    if (filename[0].Length > 90)
                    {
                        FileName = filename[0].Substring(1, 90) + "." + filename[1];
                    }
                    else
                    {
                        FileName = item1.FileName; 
                    }

                    if (item.Act_TypeVersionDate != null)
                    {
                        FileName = FileName + " on " + Convert.ToDateTime(item1.Act_TypeVersionDate).ToString("MMM-yy");
                    }

                    stringbuilder.Append("<tr data-node-id=" + Convert.ToString(id) + " data-node-pid=" + Convert.ToString(item.DocumentTypeID) + " >" +
                         " <td class='locationheadLocationbg'>" + Convert.ToString(FileName) + "</td>");

                    //stringbuilder.Append("<td class='downloadcss' onclick ='<%# excecute(" + ActID + "," + item1.ID + ")%>'> Download </td>");

                    stringbuilder.Append("<td class='downloadcss' onclick ='OpenDocumentDowmloadOverviewpup(" + ActID + "," + item1.ID + ")'> Download </td>");
                    stringbuilder.Append("<td class='Viewcss' onclick='OpenDocumentOverviewpup(" + ActID + "," + item1.ID + ")'> View </td>");

                    stringbuilder.Append("</tr>");
                }
            }
            stringbuilder.Append("</table>");
            ActDocString = stringbuilder.ToString().Trim();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fr();", true);
        }

       
        
        public void BindDocument(int ScheduledOnID)
        {
            try
            {
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                divDeleteDocument.Visible = false;
                ComplianceDocument = DocumentManagement.GetFileData1(ScheduledOnID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = ComplianceDocument.Where(entry => entry.FileType == 2).ToList();
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["complianceInstanceID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEscalationStatusList()
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "Name";
                ddlStatus.DataValueField = "ID";

                var statusList = ComplianceStatusManagement.GetEscalationStatusList();

                ddlStatus.DataSource = statusList;
                ddlStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStatusList(int statusID)
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "Name";
                ddlStatus.DataValueField = "ID";

                var statusList = ComplianceStatusManagement.GetStatusList();

                List<ComplianceStatu> allowedStatusList = null;

                List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry=>entry.Name).ToList();

                ddlStatus.DataSource = allowedStatusList;
                ddlStatus.DataBind();

                ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
            try
            {
                Labelmsg.Text = "";
                Labelmsg.Style.Add("display", "none");
                Labelmsg.Style.Remove("class");

                #region File
                if (txtInterest.Text == "")
                    txtInterest.Text = "0";
                if (txtPenalty.Text == "")
                    txtPenalty.Text = "0";
                if (txtValueAsPerSystem.Text == "")
                    txtValueAsPerSystem.Text = "0";
                if (txtValueAsPerReturn.Text == "")
                    txtValueAsPerReturn.Text = "0";
                if (txtLiabilityPaid.Text == "")
                    txtLiabilityPaid.Text = "0";

                long? StatusID = ComplianceManagement.Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID;

                var PenaltySubmit = "";
                if (ddlStatus.SelectedValue == "3")
                {
                    PenaltySubmit = "P";
                }

                string tbxDate1 = Request.Form[tbxDate.UniqueID];

                ComplianceTransaction transaction = new ComplianceTransaction()
                {
                    ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                    ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = Convert.ToInt32(ddlStatus.SelectedValue),
                    StatusChangedOn = DateTime.ParseExact(tbxDate1, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                    Remarks = tbxRemarks.Text,
                    Interest = Convert.ToDecimal(txtInterest.Text),
                    Penalty = Convert.ToDecimal(txtPenalty.Text),
                    ComplianceSubTypeID = Convert.ToInt32(lblNatureofcomplianceID.Text),
                    ValuesAsPerSystem = Convert.ToDecimal(txtValueAsPerSystem.Text),
                    ValuesAsPerReturn = Convert.ToDecimal(txtValueAsPerReturn.Text),
                    LiabilityPaid = Convert.ToDecimal(txtLiabilityPaid.Text),
                    IsPenaltySave = chkPenaltySave.Checked,
                    PenaltySubmit = PenaltySubmit,
                };


                List<FileData> files = new List<FileData>();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                //HttpFileCollection fileCollection = Request.Files;
                bool blankfileCount = true;

                var TempDocData = Business.ComplianceManagement.GetTempComplianceDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));

                if (TempDocData.Count > 0)
                {
                    int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                    var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                    string directoryPath = null;
                    string version = null;
                    if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                    {
                        version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                        }
                        else
                        {
                            directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                        }
                    }
                    else
                    {
                        version = "1.0";
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                        }
                        else
                        {
                            directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                        }
                    }

                    DocumentManagement.CreateDirectory(directoryPath);

                    //for (int i = 0; i < TempDocData.Count; i++)
                    //{
                    foreach (var item in TempDocData)
                    {
                        //HttpPostedFile uploadfile = fileCollection[i];
                        //string[] keys = fileCollection.Keys[i].Split('$');
                        String fileName = "";
                        if (item.DocType == "S")
                        {
                            fileName = "ComplianceDoc_" + item.DocName;
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                        }
                        else
                        {
                            fileName = "WorkingFiles_" + item.DocName;
                            list.Add(new KeyValuePair<string, int>(fileName, 2));
                        }

                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                        
                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                        if (item.DocData.Length > 0)
                        {
                            string filepathvalue = string.Empty;
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {
                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                filepathvalue = vale.Replace(@"\", "/");
                            }
                            else
                            {
                                filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            }
                            FileData file = new FileData()
                            {
                                Name = fileName,
                                FilePath = filepathvalue,
                                FileKey = fileKey.ToString(),
                                //Version = "1.0",
                                Version = version,
                                VersionDate = DateTime.UtcNow,
                                FileSize = item.FileSize,
                            };

                            files.Add(file);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item.DocName))
                                blankfileCount = false;
                        }
                    }
                }

                #region Comment
                //List<FileData> files = new List<FileData>();
                //    List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                //    HttpFileCollection fileCollection = Request.Files;
                //    bool blankfileCount = true;

                //    if (fileCollection.Count > 0)
                //    {
                //        int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                //        var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                //        string directoryPath = null;
                //        string version = null;
                //        if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                //        {
                //            version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                //            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                //            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                //            {
                //                directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value +"\\"+ version;
                //            }
                //            else
                //            {
                //                directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value +"/"+ version);
                //            }
                //        }
                //        else
                //        {
                //            version = "1.0";
                //            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                //            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                //            {
                //                directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                //            }
                //            else
                //            {
                //                directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                //            }
                //        }

                //        DocumentManagement.CreateDirectory(directoryPath);

                //        for (int i = 0; i < fileCollection.Count; i++)
                //        {
                //            HttpPostedFile uploadfile = fileCollection[i];
                //            string[] keys = fileCollection.Keys[i].Split('$');
                //            String fileName = "";
                //            if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                //            {
                //                fileName = "ComplianceDoc_" + uploadfile.FileName;
                //                list.Add(new KeyValuePair<string, int>(fileName, 1));
                //            }
                //            else
                //            {
                //                fileName = "WorkingFiles_" + uploadfile.FileName;
                //                list.Add(new KeyValuePair<string, int>(fileName, 2));
                //            }

                //            Guid fileKey = Guid.NewGuid();
                //            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                //            Stream fs = uploadfile.InputStream;
                //            BinaryReader br = new BinaryReader(fs);
                //            Byte[] bytes = br.ReadBytes((Int32) fs.Length);

                //            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                //            if (uploadfile.ContentLength > 0)
                //            {
                //                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                //                string filepathvalue = string.Empty;
                //                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                //                {
                //                    string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                //                    filepathvalue = vale.Replace(@"\", "/");
                //                }
                //                else
                //                {
                //                    filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //                }
                //                FileData file = new FileData()
                //                {
                //                    Name = fileName,
                //                    FilePath = filepathvalue,
                //                    FileKey = fileKey.ToString(),
                //                    //Version = "1.0",
                //                    Version = version,
                //                    VersionDate = DateTime.UtcNow,
                //                };

                //                files.Add(file);
                //            }
                //            else
                //            {
                //                if (!string.IsNullOrEmpty(uploadfile.FileName))
                //                    blankfileCount = false;
                //            }
                //        }
                //    }
                #endregion

                bool flag = false;
                bool flag1 = false;
                if (blankfileCount)
                {
                    flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                    flag1 = Business.ComplianceManagement.DeleteTempDocumentFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                }

                //bool flag = true;
                if (flag != true)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }



                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fcloseandcallcal();", true);
                }

                #endregion

                //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               
                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.Data); // create the file
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var file in complianceform)
                    {
                        if (file.FilePath != null)
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            ComplianceZip.AddEntry(str, Business.DocumentManagement.ReadDocFiles(Server.MapPath(file.FilePath)));
                            i++;
                        }
                    }

                    string fileName = "ComplianceID_" + complianceID1 + "_SampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }

                //var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));
                //Response.Buffer = true;
                //Response.Clear();
                //Response.ClearContent();
                //Response.ContentType = "application/octet-stream";
                //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.FileData); // create the file
                //Response.Flush(); // send it to the client to download

                //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", string.Format("initializeDatePickerforPerformer1(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", "initializeDatePickerforPerformer1(null);", true);
                }

                if (lblDueDate.Text != "")
                {
                    if (ddlStatus.SelectedValue == "3")
                    {
                        DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Date = Date.AddDays(1);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDue", string.Format("initializeDatePickerOverDue(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                    }
                    if (ddlStatus.SelectedValue == "2")
                    {
                        DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTime", string.Format("initializeDatePickerInTime(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                    }
                }

                // ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                tbxRemarks.Text = tbxDate.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                ViewState["ScheduledOnID"] = ScheduledOnID;
                ViewState["complianceInstanceID"] = complianceInstanceID;

                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

  
        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["complianceInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["complianceInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        
                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }                                             
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file != null)
                {
                    string path = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                        path = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                       // path = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                    }
                    else
                    {
                        path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    }                                         
                    DocumentManagement.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkDocbutton = (LinkButton)e.Item.FindControl("lbtLinkDocbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
                
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkbutton = (LinkButton)e.Item.FindControl("lbtLinkbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkbutton);
            }
        }

        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm.Text != "")
                {
                    string url = lnkSampleForm.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkViewSampleForm_Click(object sender, EventArgs e)
        {

        }

        //protected void chkPenaltySave_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkPenaltySave.Checked == true)
        //    {
        //        txtInterest.Text = "0";
        //        txtPenalty.Text = "0";
        //        txtInterest.ReadOnly = true;
        //        txtPenalty.ReadOnly = true;
        //    }
        //    else
        //    {
        //        txtInterest.Text = "";
        //        txtPenalty.Text = "";
        //        txtInterest.ReadOnly = false;
        //        txtPenalty.ReadOnly = false;
        //    }

        //}

        
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                chkPenaltySave.Checked = false;
                txtInterest.Text = "";
                txtPenalty.Text = "";

                txtInterest.ReadOnly = false;
                txtPenalty.ReadOnly = false;
                if (ddlStatus.SelectedValue == "3")
                {
                    // Penalty visible for customer
                    string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {
                        int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == customerID.ToString())
                            {
                                IsPenaltyVisible = false;
                                break;
                            }
                        }
                    }
                    if (IsPenaltyVisible == true) 
                    {
                        var data = Business.ComplianceManagement.CheckMonetary(Convert.ToInt32(lblComplianceID.Text));
                        if (data == null)
                        {
                            fieldsetpenalty.Visible = false;
                        }
                        else
                        {
                            if (data.NonComplianceType == 1)
                            {
                                fieldsetpenalty.Visible = false;
                            }
                            else
                            {
                                fieldsetpenalty.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        fieldsetpenalty.Visible = false;
                    }
                    // Penalty visible for customer
                }
                else
                {
                    fieldsetpenalty.Visible = false;
                }


                if (lblDueDate.Text != "")
                {
                    if (ddlStatus.SelectedValue == "3")
                    {
                        DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Date = Date.AddDays(1);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDue", string.Format("initializeDatePickerOverDue(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                    }
                    if (ddlStatus.SelectedValue == "2")
                    {
                        DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTime", string.Format("initializeDatePickerInTime(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                    }
                }

                if (ddlStatus.SelectedValue != "3")
                {
                    IsRemarkCompulsary = false;
                }
                else
                {
                    IsRemarkCompulsary = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, string period, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                        where row.ParentID == null
                                        && row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        divTask.Visible = true;
                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }


        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }

        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            if (taskDocument.Count > 0)
                            {
                                string fileName = string.Empty;

                                GridViewRow row = (GridViewRow) (((Control) e.CommandSource).NamingContainer);

                                Label lblTaskTitle = null;

                                if (row != null)
                                {
                                    lblTaskTitle = (Label) row.FindControl("lblTaskTitle");
                                    if (lblTaskTitle != null)
                                        fileName = lblTaskTitle.Text + "-Documents";

                                    if (fileName.Length > 250)
                                        fileName = "TaskDocuments";
                                }

                                ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                int i = 0;
                                foreach (var eachFile in taskDocument)
                                {
                                    //comment by rahul on 20 JAN 2017
                                    string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));                                    
                                    if (eachFile.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = eachFile.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (eachFile.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in taskDocumenttoView)
                                {
                                    rptComplianceVersionView.DataSource = entitiesData;
                                    rptComplianceVersionView.DataBind();

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            subTaskDocViewPath = FileName;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            UpdatePanel4.Update();
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptComplianceVersionView.DataSource = taskFileData;
                                rptComplianceVersionView.DataBind();

                                foreach (var file in taskDocumenttoView)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            subTaskDocViewPath = FileName;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            //UpdatePanel4.Update();
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    CheckBox chkTask = (CheckBox)e.Row.FindControl("chkTask");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null) /*&& lblSlashReview != null*/
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    chkTask.Enabled = false;
                                    chkTask.Checked = true;
                                    btnSubTaskDocDownload.Visible = false;
                                    lblSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }

                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lblStatus = (Label) e.Row.FindControl("lblStatus");
                //    Label lblSlashReview = (Label) e.Row.FindControl("lblSlashReview");
                //    LinkButton btnSubTaskDocView = (LinkButton) e.Row.FindControl("btnSubTaskDocView");
                //    LinkButton btnSubTaskDocDownload = (LinkButton) e.Row.FindControl("btnSubTaskDocDownload");

                //    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                //    {
                //        if (lblStatus.Text != "")
                //        {
                //            if (lblStatus.Text == "Open")
                //            {
                //                btnSubTaskDocDownload.Visible = false;
                //                lblSlashReview.Visible = false;
                //                btnSubTaskDocView.Visible = false;
                //            }
                //            else
                //            {
                //                btnSubTaskDocDownload.Visible = true;
                //                lblSlashReview.Visible = true;
                //                btnSubTaskDocView.Visible = true;
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        
        public static List<NameValueHierarchy> GetAllHierarchyTask(int TaskID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Tasks
                             where row.ID == TaskID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    Tasklist.Add(item.ID);
                    LoadSubEntitiesTask(item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntitiesTask(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            var query = (from row in entities.Tasks
                         where row.ParentID != null
                         && row.ParentID == nvp.ID
                         select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                Tasklist.Add(item.ID);
                LoadSubEntitiesTask(item, false, entities);
            }
        }

        protected void lblNo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvrow in gridSubTask.Rows)
                {
                    CheckBox chk = (CheckBox) gvrow.FindControl("chkTask");
                    if (chk != null & chk.Checked)
                    {
                        int TaskID = Convert.ToInt32(gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString());
                        string str = gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString();
                        Label lblTaskScheduledOnID = (Label) gvrow.FindControl("lblTaskScheduledOnID");
                        int TaskScheduledOnID = Convert.ToInt32(lblTaskScheduledOnID.Text);
                        Label lblComplianceScheduleOnID = (Label) gvrow.FindControl("lblComplianceScheduleOnID");
                        int ComplianceScheduleOnID = Convert.ToInt32(lblComplianceScheduleOnID.Text);
                        Label lbllblTaskInstanceID = (Label) gvrow.FindControl("lblTaskInstanceID");
                        int TaskInstanceID = Convert.ToInt32(lbllblTaskInstanceID.Text);
                        Label lblMainTaskID = (Label) gvrow.FindControl("lblMainTaskID");
                        int MainTaskID = Convert.ToInt32(lblMainTaskID.Text);
                        Label lblForMonth = (Label) gvrow.FindControl("lblForMonth");
                        string ForMonth = Convert.ToString(lblForMonth.Text);


                        if (TaskID != -1)
                        {
                            long? parentID = TaskID;
                            List<NameValueHierarchy> hierarchy = new List<NameValueHierarchy>();
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Tasklist.Clear();
                                GetAllHierarchyTask(TaskID);
                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                var documentData = (from row in entities.TaskInstanceTransactionViews
                                                    where Tasklist.Contains(row.TaskID)
                                                    && row.ForMonth == ForMonth
                                                    && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                    && row.RoleID == 4
                                                    && row.CustomerID == customerID
                                                    select row).ToList();

                                for (int i = 0; i < documentData.Count; i++)
                                {
                                    var IsTaskTransactionPresent = TaskManagment.CheckTaskTransacion(Convert.ToInt64(documentData[i].TaskInstanceID), Convert.ToInt64(documentData[i].TaskScheduledOnID), Convert.ToInt64(documentData[i].ComplianceScheduleOnID));
                                    if (IsTaskTransactionPresent == false)
                                    {
                                        int StatusID = 4;
                                        TaskTransaction transaction = new TaskTransaction()
                                        {
                                            ComplianceScheduleOnID = Convert.ToInt64(documentData[i].ComplianceScheduleOnID),
                                            TaskScheduleOnID = Convert.ToInt64(documentData[i].TaskScheduledOnID),
                                            TaskInstanceId = Convert.ToInt64(documentData[i].TaskInstanceID),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedByText = AuthenticationHelper.User,
                                            StatusId = StatusID,
                                            StatusChangedOn = DateTime.ParseExact(DateTime.Now.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            Remarks = "Not applicable",
                                            IsTaskClose = true,
                                        };
                                        bool sucess = TaskManagment.CreateTaskTransaction(transaction);
                                    }
                                }
                            }
                        }
                    }
                }
                var IsAfter = "";
                var Period = "";
                int CustomerBrachID = -1;
                int ScheduledOnID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["IsAfter"])))
                {
                    IsAfter = Convert.ToString(ViewState["IsAfter"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Period"])))
                {
                    Period = Convert.ToString(ViewState["Period"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBrachID"])))
                {
                    CustomerBrachID = Convert.ToInt32(ViewState["CustomerBrachID"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["complianceInstanceID"])))
                {
                    ScheduledOnID = Convert.ToInt32(ViewState["complianceInstanceID"]);
                }
               
                var showHideButton = false;
                if (IsAfter == "false")
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave.Enabled = showHideButton;
                }
                else
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave.Enabled = true;
                }
                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceSampleView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    ComplianceForm CMPDocuments = Business.ComplianceManagement.GetSelectedComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

                    if (CMPDocuments != null)
                    {
                        string fullfilePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath));
                        string filePath = CMPDocuments.FilePath;
                        string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                        if (CMPDocuments.FilePath != null && File.Exists(fullfilePath))
                        {
                            string extension = System.IO.Path.GetExtension(CompDocPath);

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocPath + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView);
            }
        }

      
        //protected void rptActDocVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    try
        //    {
        //        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //        string version = Convert.ToString(commandArgs[1]);
        //        int actID = Convert.ToInt32(commandArgs[0]);
        //        lblMessageAct.Text = string.Empty;                       
        //        List<Sp_Act_Document_Result> ActDocumentlst = new List<Sp_Act_Document_Result>();
        //        ActDocumentlst = ActManagement.getFileNamebyID(Convert.ToInt32(actID)).ToList();
                
        //        if (e.CommandName.Equals("Download"))
        //        {
        //            #region Download                   
        //            var files = ActDocumentlst.Where(entry => entry.Version == version).ToList();
        //            if (files.Count > 0)
        //            {
        //                foreach (var file in files)
        //                {
        //                    Response.Buffer = true;
        //                    Response.ClearContent();
        //                    Response.ClearHeaders();
        //                    Response.Clear();
        //                    Response.ContentType = "application/octet-stream";
        //                    Response.AddHeader("content-disposition", "attachment; filename= " + file.FileName);
        //                    Response.TransmitFile(Server.MapPath(file.FilePath));
        //                    Response.Flush();
        //                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
        //                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
        //                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

        //                }
        //            }
        //            #endregion
        //        }
        //        else if (e.CommandName.Equals("View"))
        //        {
        //            #region View
        //            var ActDocument = ActDocumentlst.Where(entry => entry.Version == version).ToList();
        //            if (ActDocument.Count > 0)
        //            {
        //                foreach (var file in ActDocument)
        //                {
        //                    string filePath = Server.MapPath(file.FilePath);

        //                    if (file.FilePath != null && File.Exists(filePath))
        //                    {
        //                        rptActDocVersionView.DataSource = ActDocumentlst;
        //                        rptActDocVersionView.DataBind();
        //                        UpdatePanel6.Update();

        //                        string Folder = "~/TempFiles";
        //                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

        //                        string DateFolder = Folder + "/" + File;

        //                        string extension = System.IO.Path.GetExtension(filePath);
        //                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z" || extension.ToUpper() == ".RAR")
        //                        {
        //                            lblMessageAct.Text = extension.ToUpper().Trim() + " file can't view please download it";
        //                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileActPopUp();", true);
        //                        }
        //                        else
        //                        {
        //                            Directory.CreateDirectory(Server.MapPath(DateFolder));

        //                            if (!Directory.Exists(DateFolder))
        //                            {
        //                                Directory.CreateDirectory(Server.MapPath(DateFolder));
        //                            }

        //                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

        //                            string User = AuthenticationHelper.UserID + "" + FileDate;

        //                            string FileName = DateFolder + "/" + User + "" + extension;

        //                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
        //                            BinaryWriter bw = new BinaryWriter(fs);
        //                            bw.Write(DocumentManagement.ReadDocFiles(filePath));
        //                            bw.Close();
                                    
        //                            FileName = FileName.Substring(2, FileName.Length - 2);
        //                            lblMessageAct.Text = "";                                   
        //                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileAct('" + FileName + "');", true);

        //                        }
        //                    }
        //                    else
        //                    {
        //                        lblMessageAct.Text = "There is no file to preview";                               
        //                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileActPopUp();", true);
        //                        break;
        //                    }
        //                }
        //            }
        //            #endregion
        //        }
        //        upComplianceDetails.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void rptActDocVersion_ItemDataBound(object Sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnActVersionDoc");
        //        var scriptManager = ScriptManager.GetCurrent(this.Page);
        //        scriptManager.RegisterPostBackControl(lblDownLoadfile);                
        //    }
        //}

        //protected void rptActDocVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    try
        //    {
        //        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //        string version = Convert.ToString(commandArgs[1]);
        //        int actID = Convert.ToInt32(commandArgs[0]);
        //        lblMessageAct.Text = string.Empty;
        //        List<Sp_Act_Document_Result> ActDocumentlst = new List<Sp_Act_Document_Result>();
        //        ActDocumentlst = ActManagement.getFileNamebyID(Convert.ToInt32(actID)).ToList();

        //        if (e.CommandName.Equals("View"))
        //        {
        //            #region View
        //            var ActDocument = ActDocumentlst.Where(entry => entry.Version == version).ToList();
        //            if (ActDocument.Count > 0)
        //            {
        //                foreach (var file in ActDocument)
        //                {
        //                    string filePath = Server.MapPath(file.FilePath);

        //                    if (file.FilePath != null && File.Exists(filePath))
        //                    {
        //                        string Folder = "~/TempFiles";
        //                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

        //                        string DateFolder = Folder + "/" + File;

        //                        string extension = System.IO.Path.GetExtension(filePath);
        //                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z" || extension.ToUpper() == ".RAR")
        //                        {
        //                            lblMessageAct.Text = extension.ToUpper().Trim() + " file can't view please download it";
        //                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileActPopUp();", true);
        //                        }
        //                        else
        //                        {
        //                            Directory.CreateDirectory(Server.MapPath(DateFolder));

        //                            if (!Directory.Exists(DateFolder))
        //                            {
        //                                Directory.CreateDirectory(Server.MapPath(DateFolder));
        //                            }

        //                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

        //                            string User = AuthenticationHelper.UserID + "" + FileDate;

        //                            string FileName = DateFolder + "/" + User + "" + extension;

        //                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
        //                            BinaryWriter bw = new BinaryWriter(fs);
        //                            bw.Write(DocumentManagement.ReadDocFiles(filePath));
        //                            bw.Close();

        //                            FileName = FileName.Substring(2, FileName.Length - 2);
        //                            lblMessageAct.Text = "";
        //                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileAct('" + FileName + "');", true);

        //                        }
        //                    }
        //                    else
        //                    {
        //                        lblMessageAct.Text = "There is no file to preview";
        //                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileActPopUp();", true);
        //                        break;
        //                    }
        //                }
        //            }
        //            #endregion
        //        }
        //        UpdatePanel6.Update();
        //        upComplianceDetails.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void rptActDocVersionView_ItemDataBound(object Sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        var scriptManager = ScriptManager.GetCurrent(this.Page);
        //        LinkButton lblActDocumentVersionView = (LinkButton)e.Item.FindControl("lblActDocumentVersionView");               
        //        scriptManager.RegisterAsyncPostBackControl(lblActDocumentVersionView);
        //    }
        //}

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
                long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

                TempComplianceDocument tempComplianceDocument = null;
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    #region file upload
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        string fileName =Path.GetFileName(uploadfile.FileName);
                        string directoryPath = null;

                        if (!string.IsNullOrEmpty(fileName))
                        {
                            string[] keys = fileCollection.Keys[i].Split('$');
                            if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                            {
                                directoryPath = Server.MapPath("~/TempDocuments/Statutory/");
                            }
                            else
                            {
                                directoryPath = Server.MapPath("~/TempDocuments/Working/");
                            }

                            DocumentManagement.CreateDirectory(directoryPath);
                            string finalPath = Path.Combine(directoryPath, fileName);
                            finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                            fileCollection[i].SaveAs(Server.MapPath(finalPath));

                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            if (uploadfile.ContentLength > 0)
                            {
                                tempComplianceDocument = new TempComplianceDocument()
                                {
                                    ScheduleOnID = ScheduledOnID,
                                    ComplianceInstanceID = complianceInstanceID,
                                    DocPath = finalPath,
                                    DocData = bytes,
                                    DocName = fileCollection[i].FileName,
                                    FileSize= uploadfile.ContentLength,
                                };
                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    tempComplianceDocument.DocType = "S";
                                }
                                else
                                {
                                    tempComplianceDocument.DocType = "W";
                                }
                                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);
                                if (_objTempDocumentID > 0)
                                {
                                    BindTempDocumentData(ScheduledOnID);
                                }
                            }
                        }
                    }
                    #endregion
                }
               
                BindTempDocumentData(ScheduledOnID);                
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTempDocumentData(long ScheduledOnID)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                var DocData = DocumentManagement.GetTempComplianceDocumentData(ScheduledOnID);
                if (DocData.Count > 0)
                {
                    grdDocument.Visible = true;
                    grdDocument.DataSource = DocData;
                    grdDocument.DataBind();
                }
                else
                {
                    grdDocument.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);
                           
                            BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                            if (file != null)
                            {
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                Response.TransmitFile(Server.MapPath(file.DocPath));
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.DocPath));
                            if (file.DocName != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    string CompDocReviewPath = file.DocPath;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }
        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDocType = (Label)e.Row.FindControl("lblDocType");
                  
                    if (lblDocType.Text.Trim() == "S")
                    {
                        lblDocType.Text = "Compliance Document";
                    }
                    else
                    {
                        lblDocType.Text = "Working Files";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool DownloadFileAndSaveDocuments(string directoryPath, string downloadFilePath, string fileName, long userid, out string downloadedFilePath)
        {
            bool downloadSuccess = false;
            downloadedFilePath = string.Empty;

            try
            {
                string DocAPIURlwithPATH = string.Empty;

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                directoryPath = directoryPath + @"\" + fileName;

                DocAPIURlwithPATH = downloadFilePath;
                if (!string.IsNullOrEmpty(directoryPath))
                {
                    downloadSuccess = DownloadFile(DocAPIURlwithPATH, directoryPath);
                    if (downloadSuccess)
                        downloadedFilePath = directoryPath;
                }
                return downloadSuccess;
            }
            catch (Exception ex)
            {
                return downloadSuccess;
            }
        }
        public static bool DownloadFile(string urlAddress, string fileSavePath)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    //DownlaodFile method directely downlaod file on you given specific path ,Here i've saved in E: Drive
                    client.DownloadFile(urlAddress, fileSavePath);
                    client.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static string getFileName(string url)
        {
            string[] arr = url.Split('/');
            return arr[arr.Length - 1];
        }
        protected void UploadlinkWorkingfile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string userpath = null;
            string url = TxtworkingdocumentlnkStatutory.Text;
            string fileName = getFileName(url);
            string directoryPath = null;
            directoryPath = Server.MapPath("~/TempDocuments/Statutory/");
            bool GetResult = DownloadFileAndSaveDocuments(directoryPath, url, fileName, Convert.ToInt64(AuthenticationHelper.UserID), out userpath);
            if (GetResult)
            {
                TempComplianceDocument tempComplianceDocument = null;
                Byte[] bytes = null;
                using (var webClient = new WebClient())
                {
                    bytes = webClient.DownloadData(url);
                }
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "S"
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
            TxtworkingdocumentlnkStatutory.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
        }

        protected void UploadlinkCompliancefile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string userpath = null;
            string url = TxtCompliancedocumentlnkStatutory.Text;
            string fileName = getFileName(url);
            string directoryPath = null;
            directoryPath = Server.MapPath("~/TempDocuments/Working/");
            bool GetResult = DownloadFileAndSaveDocuments(directoryPath, url, fileName, Convert.ToInt64(AuthenticationHelper.UserID), out userpath);
            if (GetResult)
            {
                TempComplianceDocument tempComplianceDocument = null;
                Byte[] bytes = null;
                using (var webClient = new WebClient())
                {
                    bytes = webClient.DownloadData(url);
                }
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "W"
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
            TxtCompliancedocumentlnkStatutory.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
        }

    }
}