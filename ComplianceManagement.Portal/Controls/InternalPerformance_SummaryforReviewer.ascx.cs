﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Collections;
using Ionic.Zip;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class InternalPerformance_SummaryforReviewer : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string year = DateTime.Today.Year.ToString();
                if (DateTime.Today.Month < Util.FinancialMonth())
                {
                    year = (DateTime.Today.Year - 1).ToString();
                }
                txtStartDate.Text = "01-0" + Util.FinancialMonth().ToString() + "-" + year;
                txtEndDate.Text = DateTime.Now.Date.ToString("dd-MM-yyyy");
                BindFilters();
                dlFilters.SelectedIndex = 0;
                setGraphTitle();
                ddlLocations.Items.Insert(0, new ListItem("< All >", "-1"));
                BindReporteeSummary("01-0" + Util.FinancialMonth() + "-" + year, DateTime.Now.Date.ToString("dd-MM-yyyy"));
                BindComplianceSummary("01-0" + Util.FinancialMonth() + "-" + year, DateTime.Now.Date.ToString("dd-MM-yyyy"));
                grdPanelSummary.Update();
            }

        }

        protected void BindReporteeSummary(string fromdate, string todate)
        {
            try
            {

                int branchId = Convert.ToInt32(ddlLocations.SelectedValue);
                InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                if (filter == InternalPerformanceSummaryForPerformer.Location)
                {
                    divLocation.Visible = true;
                    divfilterforLocation.Visible = true;
                }
                else
                {
                    divLocation.Visible = false;
                    divfilterforLocation.Visible = false;
                }
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var ReporteeData = InternalDashboardManagement.PerformanceSummaryforReviewer(customerid,AuthenticationHelper.UserID, InternalPerformanceSummaryForPerformer.Reportee, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), branchId);
                grdReportee.DataSource = ReporteeData;
                grdReportee.DataBind();
                if (ReporteeData.Rows.Count > 0)
                {
                    lblReporteeTitle.Text = Convert.ToString(grdReportee.SelectedDataKey[0]) + "'s Performance Summary";
                    divReporteeDetailView.Visible = true;
                }
                else
                {
                    lblReporteeTitle.Text = string.Empty;
                    divReporteeDetailView.Visible = false;
                }
                //lblNonComplincePenalty.Text = DashboardManagement.GetSumofPenaltyForReviewer(AuthenticationHelper.UserID, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 500, function () { });", true);
                upReportee.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<InternalPerformanceSummaryForPerformer>().Where(en => en.ID != 0);
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                lblNote.Visible = filter != InternalPerformanceSummaryForPerformer.Risk ? true : false;
                setGraphTitle();
                BindLocation();
                BindReporteeSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
                BindComplianceSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
                grdComplianceTransactions.SelectedIndex = 0;
                grdLocationCatagory.SelectedIndex = 0;
                grdPanelSummary.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public DataTable GetGrid()
        {
            dlFilters_SelectedIndexChanged(null, null);
            return (grdComplianceTransactions.DataSource as List<InternalComplianceInstanceTransactionView>).ToDataTable();
        }

        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CannedReportFilterForPerformer>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                    GridView ProductGrid = (GridView)sender;
                    GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    HeaderRow.Style.Add("border-color", "White");

                    TableCell HeaderCell = new TableCell();
                    string header = "";
                    if (filter == InternalPerformanceSummaryForPerformer.Location)
                    {
                        header = "Risk";
                    }
                    else if (filter == InternalPerformanceSummaryForPerformer.Reportee)
                    {
                        header = "Reportee Name";
                    }
                    else
                    {
                        header = filter.ToString();
                    }
                    HeaderCell.Text = header;
                    HeaderCell.RowSpan = 2;
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Completed";
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.ColumnSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Pending";
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.RowSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    grdComplianceTransactions.Controls[0].Controls.AddAt(0, HeaderRow);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[3].Visible = false;

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                    if (filter == InternalPerformanceSummaryForPerformer.Category || filter == InternalPerformanceSummaryForPerformer.Reportee)
                    {
                        e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdComplianceTransactions, "Select$" + e.Row.RowIndex);
                        e.Row.ToolTip = "Click here to generate the graph";
                        e.Row.Attributes.Add("style", "cursor:pointer");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnGetSummary_Click(object sender, EventArgs e)
        {
            try
            {

                txtStartDate.Text = Request[txtStartDate.UniqueID].ToString();
                txtEndDate.Text = Request[txtEndDate.UniqueID].ToString();
                setGraphTitle();
                BindReporteeSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
                BindComplianceSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
                grdPanelSummary.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void BindComplianceSummary(string fromdate, string todate)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                InternalPerformanceSummaryForPerformer subFilter;
                InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                if (filter == InternalPerformanceSummaryForPerformer.Location)
                {
                    subFilter = InternalPerformanceSummaryForPerformer.Risk;
                }
                else
                {
                    subFilter = InternalPerformanceSummaryForPerformer.Reportee;
                }

                int branchId = Convert.ToInt32(ddlLocations.SelectedValue);
                divLocation.Visible = false;
                divfilterforLocation.Visible = false;
                grdComplianceTransactions.DataSource = null;

                if (grdReportee.Rows.Count > 0)
                {


                    if (grdReportee.SelectedDataKey[1] != null)
                    {
                        grdComplianceTransactions.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformer(customerid, Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), branchId, subFilter, true, AuthenticationHelper.UserID);
                        grdComplianceTransactions.DataBind();
                    }

                    int catagoryID = -1;
                    if (filter == InternalPerformanceSummaryForPerformer.Reportee)
                    {
                        //catagoryID = Convert.ToInt32(UserManagement.GetByUsername(grdComplianceTransactions.DataKeys[0].Value.ToString()).ID);
                        catagoryID = Convert.ToInt32(grdComplianceTransactions.SelectedDataKey[1]);
                    }
                    else
                    {
                        catagoryID = ComplianceCategoryManagement.GetIdByNameInternal(grdComplianceTransactions.SelectedDataKey[0].ToString());
                    }

                    bool displayPendingGraph;
                    bool displayDelayedGraph;
                    chrtPendingCompliances.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerPending(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, catagoryID, branchId, subFilter, true, AuthenticationHelper.UserID);
                    chrtPendingCompliances.DataBind();

                    chrtDelayedCompliances.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerDelayed(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, catagoryID, branchId, subFilter, true, AuthenticationHelper.UserID);
                    chrtDelayedCompliances.DataBind();

                    chrtPendingCompliances.Visible = displayPendingGraph;
                    chrtDelayedCompliances.Visible = displayDelayedGraph;

                    if (displayPendingGraph == false)
                        titlePendingCompliances.Text = "No data to display";
                    if (displayDelayedGraph == false)
                        titleDelayedCompliances.Text = "No data to display";

                    if (filter == InternalPerformanceSummaryForPerformer.Location)
                    {
                        divLocation.Visible = true;
                        var LocationTreeCount = InternalDashboardManagement.GetBranchCountforReviewer(Convert.ToInt32(grdReportee.SelectedDataKey[1]), DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture));

                        divfilterforLocation.Visible = LocationTreeCount > 0 ? true : false;

                        grdLocationCatagory.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformer(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), branchId, InternalPerformanceSummaryForPerformer.Category, true, AuthenticationHelper.UserID);
                        grdLocationCatagory.DataBind();

                        int LocationCatagoryID = ComplianceCategoryManagement.GetIdByNameInternal(grdLocationCatagory.SelectedDataKey.Value.ToString());
                        chrtLocationCatagoryPending.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerPending(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, LocationCatagoryID, branchId, InternalPerformanceSummaryForPerformer.Category, true, AuthenticationHelper.UserID);
                        chrtLocationCatagoryPending.DataBind();

                        chrtLocationCatagoryDelay.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerDelayed(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, LocationCatagoryID, branchId, InternalPerformanceSummaryForPerformer.Category, true, AuthenticationHelper.UserID);
                        chrtLocationCatagoryDelay.DataBind();

                        chrtLocationCatagoryPending.Visible = displayPendingGraph;
                        chrtLocationCatagoryDelay.Visible = displayDelayedGraph;

                        if (displayPendingGraph == false)
                            titlePendingCompliances1.Text = "No data to display";
                        else
                            titlePendingCompliances1.Text = "Risk Wise Pending Compliances";

                        if (displayDelayedGraph == false)
                            titleDelayedCompliances1.Text = "No data to display";
                        else
                            titleDelayedCompliances1.Text = "Risk Wise Delayed Compliances";

                    }

                    lblNonComplincePenalty.Text = InternalDashboardManagement.GetSumofPenaltyForReviewer(Convert.ToInt32(grdReportee.SelectedDataKey[1]), DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 500, function () { });", true);
                }//added by rahul on 11 JAn 2015
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdPanelSummary_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePickerReviewerControl", string.Format("InitializeDatePickerReviewerControl(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }

                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeBranchesReviewer", string.Format("initializeJQueryUIReviewer('{0}', 'divBranches1');", tbxBranch1.ClientID), true);
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "ScriptREv", "initializeComboboxRev();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void setGraphTitle()
        {
            try
            {
                InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                if (filter == InternalPerformanceSummaryForPerformer.Risk || filter == InternalPerformanceSummaryForPerformer.Location)
                {
                    titlePendingCompliances.Text = "Category Wise Pending Compliances";
                    titleDelayedCompliances.Text = "Category Wise Delayed Compliances";
                }
                else if (filter == InternalPerformanceSummaryForPerformer.Reportee) 
                {                                                           
                    titlePendingCompliances.Text = "Pending Compliances";
                    titleDelayedCompliances.Text = "Delayed Compliances";
                }
                else
                {
                    titlePendingCompliances.Text = "Risk Wise Pending Compliances";
                    titleDelayedCompliances.Text = "Risk Wise Delayed Compliances";   
                }                                     
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int catagoryID = -1;
                InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                if (filter == InternalPerformanceSummaryForPerformer.Reportee)
                {
                    catagoryID = Convert.ToInt32(grdComplianceTransactions.SelectedDataKey[1]);
                }
                else
                {
                    catagoryID = ComplianceCategoryManagement.GetIdByNameInternal(grdComplianceTransactions.SelectedDataKey[0].ToString());
                }

                bool displayPendingGraph;
                bool displayDelayedGraph;
                setGraphTitle();

                chrtPendingCompliances.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerPending(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, catagoryID, -1, InternalPerformanceSummaryForPerformer.Reportee, true, AuthenticationHelper.UserID);
                chrtPendingCompliances.DataBind();
                chrtDelayedCompliances.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerDelayed(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, catagoryID, -1, InternalPerformanceSummaryForPerformer.Reportee, true, AuthenticationHelper.UserID);
                chrtDelayedCompliances.DataBind();

                chrtPendingCompliances.Visible = displayPendingGraph;
                chrtDelayedCompliances.Visible = displayDelayedGraph;

                if (displayPendingGraph == false)
                    titlePendingCompliances.Text = "No data to display";
                if (displayDelayedGraph == false)
                    titleDelayedCompliances.Text = "No data to display";
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 500, function () { });", true);
                BindReporteeSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        private void BindLocation()
        {
            try
            {
                ddlLocations.DataTextField = "Name";
                ddlLocations.DataValueField = "ID";

                var mappedLocation = AssignEntityManagement.GetAssignedLocationUserAndRoleWiseInternal(3, -1, Convert.ToInt32(grdReportee.SelectedDataKey[1]));
                List<object> dataSource = new List<object>();
                foreach (var loc in mappedLocation)
                {
                    dataSource.Add(new
                    {
                        ID = loc.ID,
                        Name = loc.Name + " (" + CustomerBranchManagement.GetlegalEntity(loc.ID).Name + ")"
                    });
                }
                ddlLocations.DataSource = dataSource;
                ddlLocations.DataBind();

                ddlLocations.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                var Assignedbracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWiseInternal(3, -1, Convert.ToInt32(grdReportee.SelectedDataKey[1]));
                foreach (var item in nvp.Children)
                {
                    if (Assignedbracnhes.Where(entry => entry.ID == item.ID).FirstOrDefault() != null)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item);
                        parent.ChildNodes.Add(node);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdLocationCatagory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[3].Visible = false;

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                    if (filter == InternalPerformanceSummaryForPerformer.Location)
                    {
                        e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdLocationCatagory, "Select$" + e.Row.RowIndex);
                        e.Row.ToolTip = "Click here to generate the graph";
                        e.Row.Attributes.Add("style", "cursor:pointer");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdLocationCatagory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    HeaderRow.Style.Add("border-color", "White");

                    //Adding Risk Column
                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = "Category";
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.RowSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    //Adding Head Office Column
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Completed";
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.ColumnSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    //Adding Year Column
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Pending";
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.RowSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    grdLocationCatagory.Controls[0].Controls.AddAt(0, HeaderRow);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void grdLocationCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int branchId = Convert.ToInt32(ddlLocations.SelectedValue);
                int catagoryID = ComplianceCategoryManagement.GetIdByNameInternal(grdLocationCatagory.SelectedDataKey.Value.ToString());
                bool displayPendingGraph;
                bool displayDelayedGraph;
                setGraphTitle();
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                chrtPendingCompliances.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerPending(customerid, Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, catagoryID, branchId, InternalPerformanceSummaryForPerformer.Reportee, true, AuthenticationHelper.UserID);
                chrtPendingCompliances.DataBind();
                chrtDelayedCompliances.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerDelayed(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, catagoryID, branchId, InternalPerformanceSummaryForPerformer.Reportee, true, AuthenticationHelper.UserID);
                chrtDelayedCompliances.DataBind();

                chrtPendingCompliances.Visible = displayPendingGraph;
                chrtDelayedCompliances.Visible = displayDelayedGraph;
                if (displayPendingGraph == false)
                    titlePendingCompliances.Text = "No data to display";
                if (displayDelayedGraph == false)
                    titleDelayedCompliances.Text = "No data to display";

                if (filter == InternalPerformanceSummaryForPerformer.Location)
                {
                    divLocation.Visible = true;
                    divfilterforLocation.Visible = true;
                    grdLocationCatagory.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformer(customerid, Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), branchId, InternalPerformanceSummaryForPerformer.Category, true, AuthenticationHelper.UserID);
                    grdLocationCatagory.DataBind();

                    chrtLocationCatagoryPending.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerPending(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, catagoryID, branchId, InternalPerformanceSummaryForPerformer.Category, true, AuthenticationHelper.UserID);
                    chrtLocationCatagoryPending.DataBind();

                    chrtLocationCatagoryDelay.DataSource = InternalDashboardManagement.PerformanceSummaryforPerformerDelayed(customerid,Convert.ToInt32(grdReportee.SelectedDataKey[1]), filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, catagoryID, branchId, InternalPerformanceSummaryForPerformer.Category, true, AuthenticationHelper.UserID);
                    chrtLocationCatagoryDelay.DataBind();

                    chrtLocationCatagoryPending.Visible = displayPendingGraph;
                    chrtLocationCatagoryDelay.Visible = displayDelayedGraph;

                    if (displayPendingGraph == false)
                        titlePendingCompliances1.Text = "No data to display";
                    else
                        titlePendingCompliances1.Text = "Risk Wise Pending Compliances";

                    if (displayDelayedGraph == false)
                        titleDelayedCompliances1.Text = "No data to display";
                    else
                        titleDelayedCompliances1.Text = "Risk Wise Delayed Compliances";

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 500, function () { });", true);
                    BindReporteeSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    Session["filter"] = null;
                    BindDetailView(e.CommandArgument.ToString(), "Risk");
                    Session["Arguments"] = Convert.ToString(e.CommandArgument);
                    Session["SubFilter"] = "Risk";                                                                              
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void grdLocationCatagory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    Session["filter"] = null;
                    BindDetailView(e.CommandArgument.ToString(), "Category");
                    Session["Arguments"] = Convert.ToString(e.CommandArgument);
                    Session["SubFilter"] = "Category";                                      
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDetailView(string aregument, string subfilter)
        {
            List<int> statusIds = new List<int>();
            string[] arg = new string[2];
            arg = aregument.ToString().Split(';');
            if (arg[1].Trim().Equals("Delayed"))
            {
                statusIds.Add(5);
                statusIds.Add(9);
            }
            else if (arg[1].Trim().Equals("InTime"))
            {
                statusIds.Add(4);
                statusIds.Add(7);
            }
            else
            {
                statusIds.Add(1);
                statusIds.Add(10);
                statusIds.Add(6);
            }

            InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);

            int branchId = Convert.ToInt32(ddlLocations.SelectedValue);

            List<InternalComplianceDashboardSummaryView> DetailView = new List<InternalComplianceDashboardSummaryView>();
            if (Session["filter"] != null)
            {
                if (Session["filter"].ToString().Equals("Reportee"))
                {
                    List<int> statusIdsr = new List<int>();
                    if (arg[1].Trim().Equals("Delayed"))
                    {
                        statusIdsr.Add(5);
                        statusIdsr.Add(9);
                    }
                    else if (arg[1].Trim().Equals("InTime"))
                    {
                        statusIdsr.Add(4);
                        statusIdsr.Add(7);
                    }
                    else
                    {
                        statusIdsr.Add(1);
                        statusIdsr.Add(10);
                        statusIdsr.Add(6);

                    }

                    DetailView = InternalDashboardManagement.GetSummaryDetails(Convert.ToInt32(arg[0]), DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        -1, statusIdsr, branchId, Session["filter"].ToString(), subfilter, Convert.ToInt32(arg[0]), true, AuthenticationHelper.UserID);
                }
            }
            else
            {
                DetailView = InternalDashboardManagement.GetSummaryDetails(Convert.ToInt32(grdReportee.SelectedDataKey[1]), DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                   3, statusIds, branchId, filter.ToString(), subfilter, Convert.ToInt32(arg[0]),true,AuthenticationHelper.UserID);
            }

            string lable = string.Empty;
            lable = arg[2].ToString() + " = " + arg[1] + " " + "compliances";
           

            lblDetailViewTitle.Text = lable;
            grdSummaryDetails.DataSource = DetailView;
            grdSummaryDetails.DataBind();
            UpDetailView.Update();
            Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<InternalComplianceDashboardSummaryView>).ToDataTable();
            OnSelectedIndexChanged(null, null);
            grdLocationCatagory_SelectedIndexChanged(null, null);
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divSummaryDetails\").dialog('open')", true);

        }

        private void CmpPopulateCheckedValues()
        {
            ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliances"];

            if (complianceDetails != null && complianceDetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdSummaryDetails.Rows)
                {
                    LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                    int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                    if (complianceDetails.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        private void CmpSaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            int index = -1;
            foreach (GridViewRow gvrow in grdSummaryDetails.Rows)
            {
                LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;

                // Check in the Session
                if (ViewState["checkedCompliances"] != null)
                    userdetails = (ArrayList)ViewState["checkedCompliances"];
                if (result)
                {
                    if (!userdetails.Contains(index))
                        userdetails.Add(index);
                }
                else
                    userdetails.Remove(index);
            }
            if (userdetails != null && userdetails.Count > 0)
                ViewState["checkedCompliances"] = userdetails;
        }

        protected void grdSummaryDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CmpSaveCheckedValues();
                grdSummaryDetails.PageIndex = e.NewPageIndex;
                BindDetailView(Convert.ToString(Session["Arguments"]), Convert.ToString(Session["SubFilter"]));
                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSummaryDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        int ScheduledOnID = Convert.ToInt32(e.CommandArgument);
                        if (ScheduledOnID != 0)
                        {
                            var ComplianceData = DocumentManagement.GetForMonthInternal(ScheduledOnID);
                            List<GetInternalComplianceDocumentsView> fileData = DocumentManagement.GetFileDataInternal(ScheduledOnID);
                            //craeted subdirectory
                            //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                            string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                            int i = 0;
                            foreach (var file in fileData)
                            {
                                string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                if (dictionary == null)
                                    ComplianceZip.AddDirectoryByName(directoryName + "/" + version);

                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + i + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }
                        InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;
                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=" + filter.ToString() + " Document.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();
                    }

                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSummaryDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("lblDownLoadfile");
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterPostBackControl(lblDownLoadfile);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;

                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                        ExcelData = view.ToTable("Selected", false, "ShortDescription", "Branch", "InternalScheduledOn", "ForMonth", "Status", "RiskCategory");
                        exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["B2"].Value = lblDetailViewTitle.Text + " Report";
                        exWorkSheet.Cells["B2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 15;
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A4"].Value = "Short Description";
                        exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Value = "Start Date";
                        exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D4"].Value = "For Month";
                        exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D4"].Value = "Risk Category";

                        using (ExcelRange col = exWorkSheet.Cells[2, 1, 6 + ExcelData.Rows.Count, 8])
                        {
                            col.Style.Numberformat.Format = "dd/MM/yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.AutoFitColumns();
                        }

                        InternalPerformanceSummaryForPerformer filter = (InternalPerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + filter.ToString() + "Report.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        Response.End();
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        // Zip all files from folder
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    CmpSaveCheckedValues();

                    if (ViewState["checkedCompliances"] != null)
                    {
                        foreach (var gvrow in (ArrayList)ViewState["checkedCompliances"])
                        {

                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                            {
                                var ComplianceData = DocumentManagement.GetForMonthInternal(ScheduledOnID);
                                List<GetInternalComplianceDocumentsView> fileData = DocumentManagement.GetFileDataInternal(ScheduledOnID);
                                //craeted subdirectory
                                //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                int i = 0;
                                foreach (var file in fileData)
                                {
                                    string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                    var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                    if (dictionary == null)
                                        ComplianceZip.AddDirectoryByName(directoryName + "/" + version);

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = file.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;
                        byte[] data = zipMs.ToArray();
                   
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=InternalComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();

                    }
                }
                //BindComplianceDocument(DateTime.Now,DateTime.Now);
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected bool ViewDownload(int statusid)
        {
            if (statusid != 1)
                return true;
            else
                return false;
        }

        protected void grdReportee_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[3].Visible = false;

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdReportee, "Select$" + e.Row.RowIndex);
                    e.Row.ToolTip = "Click here to generate the graph";
                    e.Row.Attributes.Add("style", "cursor:pointer");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReportee_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    GridView ProductGrid = (GridView)sender;
                    GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    HeaderRow.Style.Add("border-color", "White");

                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = "Reportee Name";
                    HeaderCell.RowSpan = 2;
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Completed";
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.ColumnSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Pending";
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.RowSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    grdReportee.Controls[0].Controls.AddAt(0, HeaderRow);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void grdReportee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //grdReportee.PageIndex = e.NewPageIndex;
                BindLocation();
                BindReporteeSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReportee_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    Session["filter"] = "Reportee";
                    BindDetailView(e.CommandArgument.ToString(), "Category");
                    Session["Arguments"] = Convert.ToString(e.CommandArgument);
                    Session["SubFilter"] = "Category";
                    BindReporteeSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void grdReportee_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdReportee.PageIndex = e.NewPageIndex;
                BindReporteeSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


    }
}