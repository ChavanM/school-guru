﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class frmEventUpcomingReviewer : System.Web.UI.Page
    {
        public string Role;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Page.RouteData.Values["filter"] != null)
                {
                    if (Page.RouteData.Values["filter"].ToString().Equals("UpcomingOverdueReviewer"))
                    {
                        if (Page.RouteData.Values["Type"].ToString().Equals("Statutory"))
                        {
                            ViewState["Role"] = Page.RouteData.Values["Role"].ToString();                          
                        }
                    }
                }
                BindComplianceTransactions();
            }
            udcStatusTranscatopn.OnSaved += (inputForm, args) => { BindComplianceTransactions(); };
        }
        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {

        }
        protected void grdComplianceTransactionsUpcoming_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactionsUpcoming.PageIndex = e.NewPageIndex;
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactionsUpcoming_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<ComplianceInstanceTransactionView> assignmentList = null;
                if (ViewState["Role"].Equals("Performer"))
                {
                    assignmentList = DashboardManagement.DashboardDataForPerformerUpcoming(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Upcoming);
                }
                else if (ViewState["Role"].Equals("Reviewer"))
                {
                    assignmentList = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Upcoming);
                }
                else if (ViewState["Role"].Equals("Approver"))
                {
                    assignmentList = DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Upcoming);
                }

                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;

                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceTransactionsUpcoming.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactionsUpcoming.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactionsUpcoming.DataSource = assignmentList;
                grdComplianceTransactionsUpcoming.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactionsUpcoming_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ComplianceInstanceTransactionView rowView = (ComplianceInstanceTransactionView)e.Row.DataItem;
                    if (rowView.ComplianceInstanceID != -1)
                    {

                        Label lblImpact = (Label)e.Row.FindControl("lblImpact");
                        Label lblRisk = (Label)e.Row.FindControl("lblRisk");
                        Label lblComplianceId = (Label)e.Row.FindControl("lblComplianceId");
                        Image imtemplat = (Image)e.Row.FindControl("imtemplat");
                        //LinkButton lnkDownload = (LinkButton)e.Row.FindControl("lnkDownload");
                        Label lblReferenceMaterialText = (Label)e.Row.FindControl("lblReferenceMaterialText");
                        //Label lblFilePath = (Label)e.Row.FindControl("lblFilePath");
                        Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                        Label lblEventID = (Label)e.Row.FindControl("lblEventID");

                        if (!string.IsNullOrEmpty(lblImpact.Text))
                        {
                            if (Convert.ToInt32(lblImpact.Text) == 0)
                            {
                                lblImpact.Text = "Monetary";
                            }
                            else if (Convert.ToInt32(lblImpact.Text) == 1)
                            {
                                lblImpact.Text = "Non-Monetary";
                            }
                            else if (Convert.ToInt32(lblImpact.Text) == 2)
                            {
                                lblImpact.Text = "Monetary & Non-Monetary";
                            }
                        }
                        else
                        {
                            lblImpact.Text = "";
                        }

                        long Complianceid = Convert.ToInt64(lblComplianceId.Text);
                        lblImpact.ToolTip = ComplianceManagement.Business.ComplianceManagement.GetPanalty(ComplianceManagement.Business.ComplianceManagement.GetByID(Complianceid));

                        if (Convert.ToInt32(lblRisk.Text) == 0)
                        {
                            imtemplat.ImageUrl = "~/Images/red.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 1)
                        {
                            imtemplat.ImageUrl = "~/Images/yellow.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 2)
                        {
                            imtemplat.ImageUrl = "~/Images/green.png";
                        }

                      
                        string CheckDate = "1/1/0001 12:00:00 AM";
                        if (lblScheduledOn != null)
                        {
                            if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                            {
                               
                                lblEventID.Visible = false;
                                lblReferenceMaterialText.Visible = false;
                            }
                        }
                        else
                        {
                            lblEventID.Visible = false;
                         
                            lblReferenceMaterialText.Visible = false;
                        }
                    }
                    else
                    {
                        e.Row.Cells[0].Style.Add("border-style", "none");
                        e.Row.Cells[1].Style.Add("border-style", "none");
                        e.Row.Cells[1].Style.Add("font-weight", "700");
                        e.Row.Cells[2].Style.Add("border-style", "none");
                        e.Row.Cells[3].Style.Add("border-style", "none");
                        e.Row.Cells[4].Style.Add("border-style", "none");
                        e.Row.Cells[5].Style.Add("border-style", "none");
                        e.Row.Cells[6].Style.Add("border-style", "none");
                        e.Row.Cells[7].Style.Add("border-style", "none");
                        e.Row.Cells[8].Style.Add("border-style", "none");
                        e.Row.Cells[9].Style.Add("border-style", "none");
                        e.Row.Cells[10].Style.Add("border-style", "none");
                        //e.Row.Cells[11].Style.Add("border-style", "none");
                        //e.Row.Cells[12].Style.Add("border-style", "none");

                        e.Row.Cells[0].Text = string.Empty;
                        e.Row.Cells[2].Text = string.Empty;
                        e.Row.Cells[3].Text = string.Empty;
                        e.Row.Cells[4].Text = string.Empty;//added by Sachin
                        e.Row.Cells[5].Text = string.Empty;
                        e.Row.Cells[6].Text = string.Empty;
                        e.Row.Cells[7].Text = string.Empty;//added by Manisha
                        e.Row.Cells[8].Text = string.Empty;//added by Manisha
                        e.Row.Cells[9].Text = string.Empty;//added by Sachin
                        e.Row.Cells[10].Text = string.Empty;//added by Sachin
                        //e.Row.Cells[11].Text = string.Empty;//added by Sachin
                        //e.Row.Cells[12].Text = string.Empty;//added by Sachin
                        e.Row.Style.Add("background-color", "#9FCBEA");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactionsUpcoming_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceTransactionsUpcoming_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                    int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                    udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridView grdComplianceTransactions = (GridView)sender;
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<ComplianceInstanceTransactionView> assignmentList = null;
                if (ViewState["Role"].Equals("Performer"))
                {
                    assignmentList = DashboardManagement.DashboardDataForPerformerOverDue(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Overdue);
                }
                else if (ViewState["Role"].Equals("Reviewer"))
                {
                    assignmentList = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Overdue);
                }
                else if (ViewState["Role"].Equals("Approver"))
                {
                    assignmentList = DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Overdue);
                }

                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = assignmentList;
                grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ComplianceInstanceTransactionView rowView = (ComplianceInstanceTransactionView)e.Row.DataItem;
                    if (rowView.ComplianceInstanceID != -1)
                    {

                        Label lblImpact = (Label)e.Row.FindControl("lblImpact");
                        Label lblRisk = (Label)e.Row.FindControl("lblRisk");
                        Label lblComplianceId = (Label)e.Row.FindControl("lblComplianceId");
                        Image imtemplat = (Image)e.Row.FindControl("imtemplat");
                        //LinkButton lnkDownload = (LinkButton)e.Row.FindControl("lnkDownload");
                        Label lblReferenceMaterialText = (Label)e.Row.FindControl("lblReferenceMaterialText");
                        //Label lblFilePath = (Label)e.Row.FindControl("lblFilePath");
                        Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");


                        if (!string.IsNullOrEmpty(lblImpact.Text))
                        {
                            if (Convert.ToInt32(lblImpact.Text) == 0)
                            {
                                lblImpact.Text = "Monetary";
                            }
                            else if (Convert.ToInt32(lblImpact.Text) == 1)
                            {
                                lblImpact.Text = "Non-Monetary";
                            }
                            else if (Convert.ToInt32(lblImpact.Text) == 2)
                            {
                                lblImpact.Text = "Monetary & Non-Monetary";
                            }
                        }
                        else
                        {
                            lblImpact.Text = "";
                        }

                        long Complianceid = Convert.ToInt64(lblComplianceId.Text);
                        lblImpact.ToolTip = ComplianceManagement.Business.ComplianceManagement.GetPanalty(ComplianceManagement.Business.ComplianceManagement.GetByID(Complianceid));

                        if (Convert.ToInt32(lblRisk.Text) == 0)
                        {
                            imtemplat.ImageUrl = "~/Images/red.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 1)
                        {
                            imtemplat.ImageUrl = "~/Images/yellow.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 2)
                        {
                            imtemplat.ImageUrl = "~/Images/green.png";
                        }

                        string CheckDate = "1/1/0001 12:00:00 AM";
                        if (lblScheduledOn != null)
                        {
                            if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                            {
                                //lnkDownload.Visible = false;
                                lblReferenceMaterialText.Visible = false;
                            }
                        }
                        else
                        {
                            //lnkDownload.Visible = false;
                            lblReferenceMaterialText.Visible = false;
                        }

                        LinkButton btnChangeStatus = (LinkButton)e.Row.FindControl("btnChangeStatus");

                    }
                    else
                    {
                        e.Row.Cells[0].Style.Add("border-style", "none");
                        e.Row.Cells[1].Style.Add("border-style", "none");
                        e.Row.Cells[1].Style.Add("font-weight", "700");
                        e.Row.Cells[2].Style.Add("border-style", "none");
                        e.Row.Cells[3].Style.Add("border-style", "none");
                        e.Row.Cells[4].Style.Add("border-style", "none");
                        e.Row.Cells[5].Style.Add("border-style", "none");
                        e.Row.Cells[6].Style.Add("border-style", "none");
                        e.Row.Cells[7].Style.Add("border-style", "none");
                        e.Row.Cells[8].Style.Add("border-style", "none");
                        e.Row.Cells[9].Style.Add("border-style", "none");
                        e.Row.Cells[10].Style.Add("border-style", "none");
                        //e.Row.Cells[11].Style.Add("border-style", "none");
                        //e.Row.Cells[12].Style.Add("border-style", "none");


                        e.Row.Cells[0].Text = string.Empty;
                        e.Row.Cells[2].Text = string.Empty;
                        e.Row.Cells[3].Text = string.Empty;
                        e.Row.Cells[4].Text = string.Empty;//added by Sachin
                        e.Row.Cells[5].Text = string.Empty;//added by Sachin
                        e.Row.Cells[6].Text = string.Empty;
                        e.Row.Cells[7].Text = string.Empty;//added by Manisha
                        e.Row.Cells[8].Text = string.Empty;//added by Manisha
                        e.Row.Cells[9].Text = string.Empty;//added by Sachin
                        e.Row.Cells[10].Text = string.Empty;//added by Sachin
                        //e.Row.Cells[11].Text = string.Empty;//added by Sachin
                        //e.Row.Cells[12].Text = string.Empty;//added by Sachin

                        e.Row.Style.Add("background-color", "#9FCBEA");

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {


                    //if (e.CommandName.Equals("CHANGE_STATUS"))
                    //if (e.CommandName.Equals("RDownload"))
                    //{
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    //if (commandArgs.Length > 1)
                    //{
                    int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                    int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                    udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                    //}

                    // }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function bind the upcoming compliances to the grid view.
        /// </summary>
        protected void BindComplianceTransactions()
        {
            try
            {
                if (ViewState["Role"] != null)
                {
                    if (ViewState["Role"].Equals("Performer"))
                    {

                        grdComplianceTransactionsUpcoming.DataSource = DashboardManagement.DashboardDataForPerformerUpcoming(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Upcoming);
                        grdComplianceTransactions.DataSource = DashboardManagement.DashboardDataForPerformerOverDue(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Overdue).ToList();
                    }
                    else if (ViewState["Role"].Equals("Reviewer"))
                    {

                        grdComplianceTransactionsUpcoming.DataSource = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Upcoming);
                        grdComplianceTransactions.DataSource = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Overdue, true).ToList();

                    }
                    else if (ViewState["Role"].Equals("Approver"))
                    {

                        grdComplianceTransactionsUpcoming.DataSource = DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Upcoming);
                        grdComplianceTransactions.DataSource = DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, "E", CannedReportFilterForPerformer.Overdue).ToList();
                    }
                    grdComplianceTransactions.DataBind();
                    grdComplianceTransactionsUpcoming.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function return true false value for status image.
        /// </summary>
        /// <param name="userID">long</param>
        /// <param name="roleID">int</param>
        /// <param name="statusID">int</param>
        /// <returns></returns>
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        /// <summary>
        /// this function is set sorting images.
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="headerRow"></param>
        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// this function is set the sorting direction.
        /// </summary>
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        /// <summary>
        /// this function is return the string values for compliance type.
        /// </summary>
        /// <param name="userID">long</param>
        protected string ShowType(long? eventID)
        {
            try
            {
                if (eventID == null)
                {
                    return "Non-Event Based";
                }
                else
                {
                    return "Event Based";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }


        //added by Manisha
        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {


                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                Label lblFilePath = (Label)gvrow.FindControl("lblFilePath");
                Label lblFileName = (Label)gvrow.FindControl("lblFileName");

                Response.ContentType = "application/octet-stream";
                string filePath = lblFilePath.Text;

                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
                Response.TransmitFile(Server.MapPath(filePath));
                Response.End();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }     
        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }       
    }
}