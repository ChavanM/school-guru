﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="compliancestatusreviewer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.compliancestatusreviewer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
</head>
<body>
    <form id="form1" runat="server" style="background: #f7f7f7;">
         <asp:ScriptManager ID="Isdf" runat="server"></asp:ScriptManager>

    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/owl.carousel.css" type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/fullcalendar.css" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/jquery-ui-1.10.7.min.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.js"></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery-ui-1.9.2.custom.min.js"></script>

        <link href="../tree/jquerysctipttop.css" rel="stylesheet" />
        <script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
        <link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />

           <link href="https://avacdn.azureedge.net/newcss/kendo.common1.2.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.rtl.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.silver.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.mobile.all.min.css" rel="stylesheet" />

        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jszip.min.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/kendo.all.min.js"></script>


    <style type="text/css">
        .reviewdoc {
            height: 100%;
            width: auto;
            display: inline-block;
            font-size: 22px;
            position: relative;
            margin: 0;
            line-height: 34px;
            font-weight: 400;
            letter-spacing: 0;
            color: #666;
        }

       tr.spaceUnder > td {
                padding-bottom: 1em;
            }
             .clspenaltysave {
                font-weight: bold;
                margin-left: 15px;
            }
            .input-disabled {
                background-color: #f7f7f7;
                border: 1px solid #c7c7cc;            
                padding: 6px 12px;
            }
            .btnss {
                background-image: url(../Images/edit_icon_new.png);
                border: 0px;
                width: 24px;
                height: 24px;
                background-color: transparent;
            }

            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > tbody > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > thead > tr > th > a {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table tr th {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

            .clsheadergrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: bottom !important;
                border-bottom: 2px solid #dddddd !important;
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
                text-align: left;
            }

            .clsROWgrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: top !important;
                border-top: 1px solid #dddddd !important;
                color: #666666 !important;
                font-size: 14px !important;
                font-family: 'Roboto', sans-serif !important;
            }

            .Inforamative {
                color: blue !important;
                border-top: 1px solid #dddddd !important;
            }

            tr.Inforamative > td {
                color: blue !important;
                border-top: 1px solid #dddddd !important;
            }

            .circle {
                width: 15px;
                height: 15px;
                border-radius: 50%;
                display: inline-block;
                margin-right: 20px;
            }
    </style>
    
    <style type="text/css">

        .clspenaltysave
        {
            font-weight:bold;
            margin-left :15px;
        }
        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

          .Inforamative{
        color:blue   !important;
        border-top: 1px solid #dddddd !important;
    }
    tr.Inforamative > td{
        color:blue   !important;
        border-top: 1px solid #dddddd !important;
    }
        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }
    </style>
           <style type="text/css">
        table#basic > tr, td {
            border-radius: 5px;
        }

        table#basic {
            border-collapse: unset;
            border-spacing: 3px;
        }

        .locationheadbg {
            background-color: #999;
            color: #fff;
            border: #666;
        }

        .locationheadLocationbg {
            background-color: #fff;
        }

        td.locationheadLocationbg > span.tree-icon {
            background-color: #1976d2 !important;
            padding-right: 12px;
            color: white;
        }

        .GradingRating1 {
            background-color: #8fc156;
        }

        .GradingRating2 {
            background-color: #ffc107;
        }

        .GradingRating3 {
            background-color: #ef9a9a;
        }

        .Viewcss {
            width: 50px;
            color: blue;
            text-align: center;
            cursor: pointer;
        }

        .downloadcss {
            width: 100px;
            color: blue;
            text-align: center;
            cursor: pointer;
        }
    </style>

        <style type="text/css">
            /*.k-window div.k-window-content {
                overflow: hidden;
            }*/

            div.k-widget.k-window {
                top: 811px !important;
            }

            .k-combobox-clearable .k-input, .k-dropdowntree-clearable .k-dropdown-wrap .k-input, .k-dropdowntree-clearable .k-multiselect-wrap, .k-multiselect-clearable .k-multiselect-wrap {
                padding-right: 2em;
                text-align: center;
                display: grid;
            }

            .k-dropdown, .k-textbox {
                text-align: center;
            }
            
            .k-grid-content {
                min-height: 150px !important;
            }

            html {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

            .k-checkbox-label, .k-radio-label {
                display: inline;
            }

            .myKendoCustomClass {
                z-index: 999 !important;
            }

            .k-grid-toolbar {
                background: white;
            }

            #grid .k-grid-toolbar {
                padding: .6em 1.3em .6em .4em;
            }

            /*.k-grid td {
                line-height: 1em;
                border-bottom-width: 1px;
            }*/

            .k-i-more-vertical:before {
                content: "\e006";
            }

            .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                background-color: #1fd9e1;
                background-image: none;
            }

            k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
                color: #000000;
                border-color: #1fd9e1;
                background-color: #f6f6f6;
            }

            .k-pager-wrap > .k-link > .k-icon {
                margin-top: 0px;
                color: inherit;
            }

            .toolbar {
                float: left;
            }

            html .k-grid tr:hover {
                background: #E4F7FB;
            }

            html .k-grid tr.k-alt:hover {
                background: #E4F7FB;
                min-height: 30px;
            }

            .k-grid tbody .k-button {
                min-width: 30px;
                min-height: 30px;
                border-radius: 35px;
            }

            .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
                margin-right: 0px;
                margin-right: 0px;
                margin-left: 0px;
                margin-left: 0px;
            }

            .k-auto-scrollable {
                overflow: hidden;
            }

            .k-grid-header {
                padding-right: 0px !important;
            }

            .k-label input[type="checkbox"] {
                margin: 0px 5px 0 !important;
            }

            .k-filter-row th, .k-grid-header th.k-header {
                font-size: 15px;
                background: #f8f8f8;
                font-family: 'Roboto',sans-serif;
                color: #212121;
                font-weight: 400;
            }

            .k-primary {
                border-color: #1fd9e1;
                background-color: #1fd9e1;
            }

            .k-pager-wrap {
                background-color: #f8f8f8;
                color: #2b2b2b;
            }

            /*td.k-command-cell {
                border-width: 0 1px 1px 1px;
            }

            .k-grid-pager {
                border-width: 1px 1px 1px 1px;
            }*/

            span.k-icon.k-i-calendar {
                margin-top: 6px;
            }

            .col-md-2 {
                width: 20%;
            }

            .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
                border-left-width: 1px;
                line-height: 14px;
            }

            .k-grid tbody tr {
                height: 38px;
            }

            .k-textbox .k-icon {
                top: 50%;
                margin: -8px 0 0;
                margin-left: 56px;
                position: absolute;
            }

            .k-grid table
            {
                border-collapse: collapse;
            }

          label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }
        /*label {
            display: flex;
            margin-bottom: 0px;
        }*/


       .k-animation-container{

           width: 400px !important;
            white-space: pre-wrap;
             /*height: 255px !important;*/
            /*left: 735px !important;*/
           
       }

      k-filter-menu k-popup k-group k-reset k-state-border-up {
            height: 255px !important;
       }

       .k-multicheck-wrap{
          max-height:125px !important;
      }

         .input[type=checkbox], input[type=radio] {
    margin: 4px 10px 0;
    margin-top: 1px\9;
    line-height: normal;
}
        </style>

    <script type="text/javascript">

        function fCalltreeCollapsed() {
            $(document).ready(function () {
                $('#basic').simpleTreeTable({
                    collapsed: true
                });
            });
        }

        $(document).ready(function () {
            $('#basic').simpleTreeTable({
                collapsed: true
            });
        });

        function CloseAndBindData() {
            window.parent.CloseModalReviewer();
        }

        function CloseCalenderPERPop() {
            window.parent.fcloseandcallcal();
        }

        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };

        function SelectheaderCheckboxesGST1(headerchk) {
         var chkheaderid = headerchk.id.split("_");
             var gvcheck = document.getElementById("<%=grdGstMappedCompliance.ClientID %>");
             var i;

             if (headerchk.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                 }
             }

             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     if (gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].disabled == false)
                         gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                 }
             }
    }
    </script>
     


<div id="divReviewerComplianceDetailsDialog" style="width: 97%;margin-left: 20px; margin-top:-10px;">
    
    <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails1_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 5%" class="alert alert-block alert-danger fade in" ValidationGroup="ReviewerComplianceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                       Style="padding-left:40px" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                    <asp:Label ID="Labelmsgrev" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                    <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                    <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                </div>
                <div>
                    <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                    <div id="divRiskType1" runat="server" class="circle"></div>
                    <asp:Label ID="lblRiskType1" Style="width: 300px; margin-left: -17px; font-weight: bold; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                </div>
                <div id="ActDetails1" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1">
                                            <h2>Act Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapseActDetails1" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%;">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Act Name</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblActName1" Style="width: 88%; font-size: 13px; color: #333;"
                                                            autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Section /Rule</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblRule1" Style="width: 88%; font-size: 13px; color: #333;"
                                                            autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                 <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Act Document(s)</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                     <td style="width: 83%;">
                                                         <div style="width: 90%; margin-top: 15px;">
                                                             <%=ActDocString%>
                                                         </div>
                                                     </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="ComplianceDetails1" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">

                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails1">
                                            <h2>Compliance Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                               
                                <%--gaurav--%> <div id="collapseComplianceDetails1" class="panel-collapse collapse in"><%--gaurav--%>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Compliance Id</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceID1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Short Description</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceDiscription1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Short Form</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblshortform" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Detailed Description</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblDetailedDiscription1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Penalty</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblPenalty1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblFrequency1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="OthersDetails1" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails1">
                                            <h2>Additional Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseOthersDetails1" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblRisk1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Sample Form/Attachment</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">

                                                        <asp:UpdatePanel ID="upsample1" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblFormNumber1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:LinkButton ID="lbDownloadSample1" Style="width: 300px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClick="lbDownloadSample1_Click" />
                                                                <asp:Label ID="lblSlash1" Text="/" Style="color: blue;" runat="server" />
                                                                <asp:LinkButton ID="lnkViewSampleForm1" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClientClick="fopendocReviewfile();" />
                                                                <asp:Label ID="lblpathsample1" runat="server" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Regulatory website link</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:LinkButton ID="lnkSampleForm1" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                            runat="server" Font-Underline="false" OnClientClick="flinkpopup()" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblRefrenceText1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Location</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                   <tr class="spaceUnder" id="TRGSTNo" visible="false" runat="server">
                                                            <td style="width: 25%; font-weight: bold;">GST No.</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblGSTno" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Period</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder" runat="server" id="TROriginalDuedate" visible="false">
                                                    <td style="width: 15%; font-weight: bold;">Original Due Date</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblOriginalDuedate" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;" runat="server" id="duedatelbl">Due Date</td>
                                                     <td style="width: 25%; font-weight: bold;" runat="server" visible="false" id="duedatelbl1"> Extended Due Date </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                        <asp:HiddenField ID="hiddenReviewDueDate" runat="server" />
                                                    </td>
                                                </tr>
                                                 <tr class="spaceUnder" runat="server" id="TRactualduedate" visible="false">
                                                            <td style="width: 15%; font-weight: bold;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblActualDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                <tr class="spaceUnder" id="trAuditChecklist1" runat="server">
                                                    <td style="width: 25%; font-weight: bold;">Audit Checklist</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblAuditChecklist1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div runat="server" id="divTask" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                        <h2>Main Task Details</h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>

                                <div id="collapseTaskSubTask" class="collapse in">
                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                        <asp:GridView runat="server" ID="gridSubTaskReviewer" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                            OnRowCommand="gridSubTaskReviewer_RowCommand" OnRowDataBound="gridSubTaskReviewer_RowDataBound" AutoPostBack="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                        <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Task">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Performer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reviewer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                </asp:LinkButton>
                                                                <asp:Label ID="lblTaskSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                    Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                <%-- <asp:PostBackTrigger ControlID="btnSubTaskDocView" />--%>
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                        <div runat="server" id="divGSTComplianceList" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGSTComplianceList">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseGSTComplianceList">
                                        <h2>Compliance mapped Other Locations</h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>
                                <div id="collapseGSTComplianceList" class="collapse in" style="max-height: 100%;>
                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px; " >
                                        <asp:Panel runat="server" ScrollBars="Auto" Height="250px">
                                            <asp:GridView runat="server" ID="grdGstMappedCompliance" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                AllowPaging="false" OnRowCommand="grdGstMappedCompliance_RowCommand" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                            <asp:Label ID="lblInstanceID" runat="server" Visible="false" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                            <asp:Label ID="lblForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                            <asp:Label ID="lblComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ScheduledOnID") %>'></asp:Label>
                                                            <asp:Label ID="lblCustomerBranchID" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                                                <asp:Label ID="lblTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                    Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                <asp:Label ID="LabelBranch" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Due Date">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action">
                                                        <HeaderTemplate>
                                                            <asp:UpdatePanel ID="upCompliance1" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:CheckBox ID="chkComplianceAll" Text="Select All" runat="server" onclick="javascript:SelectheaderCheckboxesGST1(this)" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="upGSTDownloadView" runat="server">
                                                                <ContentTemplate>
                                                                           <asp:LinkButton CommandName="View" runat="server" ID="btnGSTDocView" CommandArgument='<%# Eval("ScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                    Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                    <asp:CheckBox ID="chkCompliance" CssClass="sbtask" Width="30px" data-toggle="tooltip" ToolTip="Click to perform" runat="server" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Right" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Step1Download" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Step1DownloadReview1">
                                            <%--<h2>Review Compliance Document</h2>--%>
                                            <asp:Label ID="lblReviewDoc" runat="server" CssClass="reviewdoc" Text="Review Compliance Document"></asp:Label>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1DownloadReview1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="Step1DownloadReview1" class="panel-collapse collapse in">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;" id="fieldsetdownloadreview" runat="server">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 15%; font-weight: bold;">Versions</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <table style="text-align: left;width:100%;">
                                                            <thead>
                                                                <tr>
                                                                    <td style="vertical-align:top;">
                                                                        <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                                                            OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table id="tblComplianceDocumnets">
                                                                                    <thead>
                                                                                        <%-- <th>Versions</th>--%>
                                                                                    </thead>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <tr>
                                                                                           
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                                                            runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                                    <td>
                                                                                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                                                                            <ContentTemplate>
                                                                                                                <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls1()'
                                                                                                                    ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                                </asp:LinkButton>
                                                                                                                <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />

                                                                                                                <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                                    runat="server" Font-Underline="false" />

                                                                                                                <%--OnClientClick="fopendocfileReview();"--%>
                                                                                                                <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                                                <asp:Label ID="lblpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                                <asp:LinkButton ID="lblpathDownload" CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                                    OnClientClick='javascript:enableControls1()' Text='Click Here' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                                    runat="server" Font-Underline="false" />
                                                                                                            </ContentTemplate>
                                                                                                        </asp:UpdatePanel>
                                                                                                    </td>
                                                                                              
                                                                                        </tr>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btnComplinceVersionDoc" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                                            OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table id="tblComplianceDocumnets">
                                                                                    <thead>
                                                                                        <th>Compliance Related Documents</th>
                                                                                    </thead>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("FileID")%>'
                                                                                            OnClientClick='javascript:enableControls1()'
                                                                                            ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                        </asp:LinkButton>
                                                                                          <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />    

                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                        <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                                            OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table id="tblWorkingFiles">
                                                                                    <thead>
                                                                                        <th>Compliance Working Files</th>
                                                                                    </thead>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("FileID")%>'
                                                                                            ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                        </asp:LinkButton>
                                                                                         <asp:Label ID="lblWorkCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblWorkCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="Step1UpdateCompliance" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus">
                                            <h2>Update Compliance Status </h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="Step1UpdateComplianceStatus" class="panel-collapse collapse in">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div style="margin-bottom: 7px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <label id="lblStatus1" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" />
                                                    <td style="width: 21%;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblNotComplied" runat="server" Text=""></asp:Label>
                                                        <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:HiddenField ID="hdnfieldPanaltyDisplay" runat="server" />
                                                                <asp:RadioButtonList ID="rdbtnStatus1" AutoPostBack="true" OnSelectedIndexChanged="rdbtnStatus1_SelectedIndexChanged" onchange="hideDiv1()" runat="server" RepeatDirection="Horizontal">
                                                                </asp:RadioButtonList>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <div style="margin-bottom: 7px" id="divDated1" runat="server">
                                                        <td style="width: 25%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label id="lbldate" runat="server" style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:TextBox runat="server" ID="tbxDate1" ReadOnly="true" placeholder="DD-MM-YYYY" class="form-control" Style="width: 115px; cursor: text;" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate1"
                                                                runat="server" ID="RequiredFieldValidator3" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                                                        </td>
                                                    </div>
                                                </tr>
                                            </table>
                                            <div style="margin-bottom: 7px">
                                                <fieldset id="fieldsetpenalty" runat="server" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr id="trPenalty" runat="server">
                                                            <td style="width: 11%; font-weight: bold;">
                                                                <asp:CheckBox ID="chkPenaltySaveReview" class="clspenaltysave" onclick="fillValuesInTextBoxesReview1()"
                                                                    Text="Value is not known at this moment" runat="server" />
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;"></td>
                                                            <td style="width: 10%;"></td>

                                                            <td style="width: 9%;"></td>
                                                            <td style="width: 2%; font-weight: bold;"></td>
                                                            <td style="width: 10%;"></td>
                                                        </tr>

                                                        <tr>
                                                            <td style="width: 11%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Interest(INR) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtInterestReview" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtInterestReview" />
                                                            </td>

                                                            <td style="width: 9%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lblpenaltytextchange" runat="server" style="font-weight: bold; vertical-align: text-top;">Penalty Amount(INR) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtPenaltyReview" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtPenaltyReview" />
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </fieldset>
                                                <br />

                                                <fieldset id="fieldsetTDS" runat="server" visible="false" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 11%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Nature of compliance </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:Label ID="lblNatureofcompliance" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                                <asp:Label ID="lblNatureofcomplianceID" Visible="false" runat="server" />
                                                            </td>
                                                            <td style="width: 9%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lblValueAsPerSystem" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per system (A) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtValueAsPerSystem" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtValueAsPerSystem" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 11%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lblValueAsPerReturn" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per return (B) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtValueAsPerReturn" onkeyup="Diff1();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtValueAsPerReturn" />
                                                            </td>
                                                            <td style="width: 9%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lbldiffAB" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (A)-(B) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">:</td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtDiffAB" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            </td>
                                                        </tr>

                                                        <tr id="trreturn" runat="server" class="spaceUnder">
                                                            <td style="width: 11%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lblLiabilityPaid" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability Paid (C) </label>
                                                            </td>
                                                            <td id="tdLiabilityPaidSC" runat="server" style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox ID="txtLiabilityPaid" runat="server" onkeyup="Diff1();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtLiabilityPaid" />
                                                            </td>
                                                            <td style="width: 9%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lbldiffBC" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (B)-(C) </label>
                                                            </td>
                                                            <td id="tddiffBCSC" runat="server" style="width: 2%; font-weight: bold;">:</td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtDiffBC" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>

                                            </div>
                                            <div style="margin-bottom: 7px" id="divbankdetails" runat="server"  visible="false" >
                                                       <fieldset id="fieldset1" runat="server" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr class="spaceUnder">
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;"> Challan No </label>
                                                                </td>
                                                                <td style="width:0.1%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                      <asp:TextBox runat="server" ID="txtChallanNo"  class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                             
                                                                </td>
                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="Label5" runat="server" style="font-weight: bold; vertical-align: text-top;"> Challan paid date </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                               
                                                                <asp:TextBox runat="server" ID="tbxChallanDate" placeholder="DD-MM-YYYY"  class="form-control" Style="width: 115px; cursor: text;" />
                                                  
                                                                      </td>
                                                            </tr>
                                                            <tr class="spaceUnder">
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="Label6" runat="server" style="font-weight: bold; vertical-align: text-top;">Challan Amount </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                   
                                                                     <asp:TextBox runat="server" ID="txtChallanAmount"  class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            
                                                                </td>
                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="Label7" runat="server" style="font-weight: bold; vertical-align: text-top;">Bank Name </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;float: right;margin-right: 35px;">:</td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtBankName"  class="form-control" Style="width: 136px;"/>
                                                                </td>
                                                            </tr>

                                                           

                                                           
                                                        </table>
                                                    </fieldset>
                                                    </div>
                                             <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 25%;">
                                                          <label id="remarkcompulsory" style="width: 10px; display: none; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:TextBox runat="server" ID="tbxRemarks1" TextMode="MultiLine" class="form-control" Rows="2" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style="margin-bottom: 7px; margin-left: 34%; margin-top: 10px;">
                                                <asp:Button Text="Approve" runat="server" ID="btnSave1" OnClientClick="javascript:return PenaltyValidateReview1()" OnClick="btnSave1_Click" CssClass="btn btn-search"
                                                    ValidationGroup="ReviewerComplianceValidationGroup" />
                                                <asp:Button Text="Reject" runat="server" ID="btnReject1" Style="margin-left: 15px" OnClientClick="javascript:return RejectRemark()" OnClick="btnReject1_Click" CssClass="btn btn-search"
                                                    ValidationGroup="ReviewerComplianceValidationGroup" CausesValidation="false" />
                                                <asp:Button Text="Close" runat="server" OnClick="btnReviewClose_Click" Style="margin-left: 15px" ID="btnReviewClose" CssClass="btn btn-search" data-dismiss="modal" />

                                                  <asp:Button Text="Go To Portal" ID="lnkgotoportal"  Visible="false" OnClick="lnkgotoportal_Click"
                                                        Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />
                                            </div>
                                        </div>
                                        </label>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div id="LegalUpdates" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 12px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseLegalUpdates">
                                                    <h2>Legal Updates</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseLegalUpdates"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseLegalUpdates" class="collapse">

                                                <div id="gridUpdate"></div>
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                <div id="Log1" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#AuditLog1">
                                            <h2>Audit Log</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#AuditLog1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="AuditLog1" class="collapse">
                                    <div runat="server" id="log" style="text-align: left;">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                    AllowPaging="true" PageSize="5" GridLines="none" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                    OnRowCreated="grdTransactionHistory_RowCreated" BorderWidth="0px" CssClass="table" OnSorting="grdTransactionHistory_Sorting"
                                                    DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                    <Columns>
                                                        <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                        <asp:TemplateField HeaderText="Date">
                                                            <ItemTemplate>
                                                   <%--gaurav--%>  <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString("dd-MM-yyyy") : ""%> <%--gaurav--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Interest" HeaderText="Interest" />
                                                        <asp:BoundField DataField="Penalty" HeaderText="Penalty" />
                                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                        <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
                 <div id="ViewUpdateDetails">
                     <label id="detailUpdate"></label>
                     </div>

            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
            <asp:PostBackTrigger ControlID="btnSave1" />
            <asp:PostBackTrigger ControlID="lbDownloadSample1" />
            <%--<asp:PostBackTrigger ControlID="txtInterestReview" />--%>
            <asp:AsyncPostBackTrigger ControlID="txtInterestReview" />
            <%-- <asp:PostBackTrigger ControlID="rdbtnStatus1"/>--%>
        </Triggers>
    </asp:UpdatePanel>


</div>

<div>
    <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div style="float: left; width: 10%">
                            <table width="100%" style="text-align: left; margin-left: 5%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                        OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Versions</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="float: left; width: 90%">
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="lblMessageReviewer1" runat="server" Style="color: red;"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%--<div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="550px"></iframe>
                </div>
            </div>
        </div>--%>
    </div>
</div>


<div class="modal fade" id="SampleFileReviewPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <%-- data-dismiss-modal="modal2"--%>
                <button type="button" class="close" onclick="$('#SampleFileReviewPopUp').modal('hide');" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
                <%-- <div style="width: 100%;">--%>
                <div style="float: left; width: 10%">
                    <table width="100%" style="text-align: left; margin-left: 5%;">
                        <thead>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptComplianceSampleView1" runat="server" OnItemCommand="rptComplianceSampleView1_ItemCommand"
                                                OnItemDataBound="rptComplianceSampleView1_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets1">
                                                        <thead>
                                                            <th>Sample Forms</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView"
                                                                        runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="rptComplianceSampleView1" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div style="float: left; width: 90%">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                        <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                    </fieldset>
                </div>
                <%-- </div>--%>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="modal fade" id="modalDocumentReviewerViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div style="float: left; width: 10%">
                            <table width="100%" style="text-align: left; margin-left: 5%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptTaskVersionView" runat="server" OnItemCommand="rptTaskVersionView_ItemCommand">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Versions</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblTaskDocumentVersionView"
                                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="lblTaskDocumentVersionView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="rptTaskVersionView" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="float: left; width: 90%">
                            <asp:Label runat="server" ID="lblMessagetask" Style="color: red;"></asp:Label>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docTaskViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

            <div class="modal fade" id="divRevActOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 100%;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="RevActOverViews" src="about:blank" width="100%;" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>


        <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 650px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="RevActDownloadViews" src="about:blank" width="535px" height="350px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript">

    function topFunction() {
        $("html,body").animate({ scrollTop: 0 }, "slow");
    }

    //$(function () {
    //    $('#divComplianceDetailsDialog').dialog({
    //        height: 600,
    //        width: 800,
    //        autoOpen: false,
    //        draggable: true,
    //        title: "Change Status",
    //        open: function (type, data) {
    //            $(this).parent().appendTo("form");
    //        }
    //    });

    //});

    function initializeDatePicker(date) {
        var startDate = new Date();
        $('#tbxDate1').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1,
        });

        if (date != null) {
            $("#tbxDate1").datepicker("option", "defaultDate", date);
        }
    }

    function initializeDatePickerOverDueReview(dateOverDue) {
        $('#tbxDate1').datepicker('destroy');
        $('#tbxDate1').datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: dateOverDue,
            numberOfMonths: 1,
        });

        $('#basic').simpleTreeTable({
            collapsed: true
        });
        <%--if (dateOverDue != null) {
            $("#<%= tbxDate1.ClientID %>").datepicker("option", "defaultDate", dateOverDue);
        }--%>
    }

    function initializeDatePickerInTimeReview(dateInTime) {
        $('#tbxDate1').datepicker('destroy');
       $('#tbxDate1').datepicker({
           dateFormat: 'dd-mm-yy',
           maxDate: dateInTime,
           numberOfMonths: 1,
       });

       $('#basic').simpleTreeTable({
           collapsed: true
       });

       <%--if (dateInTime != null) {
           $("#<%= tbxDate1.ClientID %>").datepicker("option", "maxDate", dateInTime);
        }--%>
    }
     function initializeDatePickerforChallanPaidDate(date1) {
                var startDate = new Date();
                $('#<%= tbxChallanDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: startDate,
                    numberOfMonths: 1,
                });
                if (date1 != null) {
                    $("#<%= tbxChallanDate.ClientID %>").datepicker("option", "defaultDate", date1);
                }
            }
    function enableControls1() {
        $("#btnSave1").removeAttr("disabled");
        $("#rdbtnStatus1").removeAttr("disabled");
        $("#tbxRemarks1").removeAttr("disabled");
        $("#tbxDate1").removeAttr("disabled");
        $("#btnReject1").removeAttr("disabled");
    }

    function fopendocfileReviewReviewer(file) {
        $('#DocumentReviewPopUp1').modal('show'); enableControls1();
        $('#docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopenSampleFileReviewer(file) {
        $('#docViewerAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopendocReviewfile() {
        $('#SampleFileReviewPopUp').modal('show'); 
        $('#docViewerAll').attr('src', "../docviewer.aspx?docurl=" + $("#lblpathsample1").text());
    }

    function fopendoctaskfileReview1(file) {
        $('#modalDocumentReviewerViewer').modal('show');
        $('#docTaskViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }
    function fopendoctaskfileReview1PopUp() {
        $('#modalDocumentReviewerViewer').modal('show');
    }
    function fopendocfileReviewReviewerPopUp() {
        $('#DocumentReviewPopUp1').modal('show');
    }
 
    $(document).ready(function () {
        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#DocumentReviewPopUp1').modal('hide');
            $('#modalDocumentReviewerViewer').modal('hide');
            $('#SampleFileReviewPopUp').modal('hide');
            $('#divActFilePopUp').modal('hide');
        });
     $('#OthersDetails1').click(function(){ if($('#TRactualduedate').length >0) {$('#duedatelbl').html('Revised Due Date');}else{$('#duedatelbl').html('Due Date');} });
    });



    function PenaltyValidateReview1() {
        var RB1 = document.getElementById("rdbtnStatus1");
         var radio = RB1.getElementsByTagName("input");
         var isChecked = false;
         for (var i = 0; i < radio.length; i++) {
             if (radio[i].checked) {
                 isChecked = true;
                 break;
             }
         }
         if (isChecked == false) {
             $("#Labelmsgrev").css('display', 'block');
             $('#Labelmsgrev').text("Please select Status.");
             $('#ValidationSummary1').IsValid = false;
             document.getElementById("ValidationSummary1").value = "Please select Status.";
             $("html,body").animate({ scrollTop: 0 }, "slow");
             return false;
         }
         var ispenaltyDisplay = document.getElementById("hdnfieldPanaltyDisplay").value;
        if (ispenaltyDisplay == "true") {
            if (document.getElementById('rdbtnStatus1_0') != null) {
                if (document.getElementById('rdbtnStatus1_0').checked) {
                    {
                        var checkedRadio = $("#rdbtnStatus1 input[type=radio]:checked");
                        if (checkedRadio.length > 0) {
                            var selectedText = checkedRadio.next().html();
                            if (selectedText == "Closed-Delayed") {

                                var chk = $("#chkPenaltySaveReview").is(":checked");
                                if (chk == false) {
                                    var txtInterestReview = $("#txtInterestReview").val();
                                    var txtPenaltyReview = $("#txtPenaltyReviewReview").val();
                                    if (txtInterestReview == "" || txtPenaltyReview == "" || txtInterestReview == "" || txtPenaltyReview == "0") {
                                        $("#Labelmsgrev").css('display', 'block');
                                        $('#Labelmsgrev').text("Please enter interest and penalty");
                                        $('#ValidationSummary1').IsValid = false;
                                        document.getElementById("ValidationSummary1").value = "Please enter interest and penalty";
                                        $("html,body").animate({ scrollTop: 0 }, "slow");
                                        return false;
                                    }
                                }

                                if (!$.trim($("#tbxRemarks1").val())) {
                                    $("#Labelmsgrev").css('display', 'block');
                                    $('#Labelmsgrev').text("Please enter remark");
                                    $('#ValidationSummary1').IsValid = false;
                                    document.getElementById("ValidationSummary1").value = "Please enter remark";
                                    $("html,body").animate({ scrollTop: 0 }, "slow");
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function RejectRemark() {
        if (!$.trim($("#tbxRemarks1").val())) {
            $("#Labelmsgrev").css('display', 'block');
            $('#Labelmsgrev').text("Please enter remark");
            $('#ValidationSummary1').IsValid = false;
            document.getElementById("ValidationSummary1").value = "Please enter remark";
            $("html,body").animate({ scrollTop: 0 }, "slow");
            return false;
        }
        
    }

    function hideDiv1() {
        $('#remarkcompulsory').css('display', 'none');
        if (document.getElementById('rdbtnStatus1_0') != null) {
            if (document.getElementById('rdbtnStatus1_0').checked) {
                var checkedRadio = $("#rdbtnStatus1 input[type=radio]:checked");
                if (checkedRadio.length > 0) {
                    var selectedText = checkedRadio.next().html();
                    if (selectedText == "Closed-Delayed") {
                        $('#remarkcompulsory').css('display', 'block');
                    }
                    else {
                        $('#remarkcompulsory').css('display', 'none');
                    }
                }
            }
        }

        try
        {
            var ispenaltyDisplay = document.getElementById("hdnfieldPanaltyDisplay").value;
            if (ispenaltyDisplay == "true") {
                if (document.getElementById('fieldsetpenalty') != null) {
                    document.getElementById('fieldsetpenalty').style.display = 'none';
                }

                if (document.getElementById('rdbtnStatus1_1') != null) {
                    if (document.getElementById('rdbtnStatus1_1').checked) {
                        var checkedRadio = $("#rdbtnStatus1 input[type=radio]:checked");
                        if (checkedRadio.length > 0) {
                            var selectedText = checkedRadio.next().html();
                            if (selectedText == "Closed-Timely") {
                                $("#chkPenaltySaveReview").prop('checked', false);
                            }
                        }
                    }
                }

                if (document.getElementById('rdbtnStatus1_0') != null) {
                    if (document.getElementById('rdbtnStatus1_0').checked) {
                        var checkedRadio = $("#rdbtnStatus1 input[type=radio]:checked");
                        var chk = $("#chkPenaltySaveReview").is(":checked");
                        if (checkedRadio.length > 0) {
                            var selectedText = checkedRadio.next().html();
                            if (selectedText == "Closed-Delayed" && chk == false) {
                                $("#txtInterestReview").attr("disabled", false);
                                $("#txtPenaltyReview").attr("disabled", false);
                                document.getElementById('fieldsetpenalty').style.display = 'block';
                            }
                            else {
                                document.getElementById('fieldsetpenalty').style.display = 'block';
                                $("#txtInterestReview").attr("disabled", true);
                                $("#txtPenaltyReview").attr("disabled", true);
                            }
                        }
                    }
                    else {
                        document.getElementById("txtInterestReview").value = "0";
                        document.getElementById("txtPenaltyReview").value = "0";
                    }
                }
            }
            else {
                document.getElementById('fieldsetpenalty').style.display = 'none';
            }
        }
        catch(err)
        {
        }
    }

    function fillValuesInTextBoxesReview1() {
        var chk = $("#chkPenaltySaveReview").is(":checked");
        if (chk == true) {
            document.getElementById("txtInterestReview").value = "0";
            document.getElementById("txtPenaltyReview").value = "0";
            $("#txtInterestReview").attr("disabled", true);
            $("#txtPenaltyReview").attr("disabled", true);
        }
        else {
            document.getElementById("txtInterestReview").value = "";
            document.getElementById("txtPenaltyReview").value = "";
            $("#txtInterestReview").attr("disabled", false);
            $("#txtPenaltyReview").attr("disabled", false);
        }
    }

    function Diff1() {
        document.getElementById('txtDiffAB').value = "";
        var ValueAsPerSystem = document.getElementById('txtValueAsPerSystem').value;
        var ValueAsPerReturn = document.getElementById('txtValueAsPerReturn').value;
        var AB = parseInt(ValueAsPerSystem) - parseInt(ValueAsPerReturn);
        if (!isNaN(AB)) {
            document.getElementById('txtDiffAB').value = AB;
        }
        else {
            document.getElementById('txtDiffAB').value = "";
        }
        document.getElementById('txtDiffBC').value = ""
        var LiabilityPaid = document.getElementById('txtLiabilityPaid').value;
        var BC = parseInt(ValueAsPerReturn) - parseInt(LiabilityPaid);
        if (!isNaN(BC)) {
            document.getElementById('txtDiffBC').value = BC;
        }
        else {
            document.getElementById('txtDiffBC').value = "";
        }
    }

</script>
            <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.bxslider.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/bootstrap.min.js"></script>
        <!-- nice scroll -->
         
        <!-- charts scripts -->
        <script type="text/javascript" src="../assets/jquery-knob/js/jquery.knob.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/owl.carousel.js"></script>
        <!-- jQuery full calendar -->
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/fullcalendar.min.js"></script>
        <!--script for this page only-->
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/calendar-custom.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.customSelect.min.js"></script>
        <!--custome script for all page-->
       <%-- <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/scripts.js"></script>--%>
    </form>
</body>

      <script type="text/javascript">

          function flinkpopup() {
              if ($('#lnkSampleForm1').text() != '')
                  window.open($('#lnkSampleForm1').text(), '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no');
          }
          
          function openInNewTab(url) {
              var win = window.open(url, '_blank');
              win.focus();
          }
        //function OpenDocumentOverviewpup(ActID, ID) {
        //    $('#divOverView').modal('show');
        //    $('#OverViews').attr('width', '100%');
        //    $('#OverViews').attr('height', '600px');
        //    $('.modal-dialog').css('width', '100%');
        //    $('#OverViews').attr('src', "../Common/ActOverview.aspx?ActID=" + ActID + "&ID=" + ID);
        //}

        //function OpenDocumentDowmloadOverviewpup(ActID, ID) {
        //    $('#DownloadViews').attr('src', "../Common/DownloadActOverview.aspx?ActID=" + ActID + "&ID=" + ID);
        //}

        function RevActODOPopPup(ActID, ID) {
            $('#divRevActOverView').modal('show');
            $('#RevActOverViews').attr('width', '100%');
            $('#RevActOverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '100%');
            $('#RevActOverViews').removeAttr('src');
            $('#RevActOverViews').attr('src', "../Common/ActOverview.aspx?ActID=" + ActID + "&ID=" + ID);
        }
        function RevActODDOPopPup(ActID, ID) {
            $('#RevActDownloadViews').attr('src', "../Common/DownloadActOverview.aspx?ActID=" + ActID + "&ID=" + ID);
        }
    </script>

    <script type="text/javascript">

    $(document).ready(function () {

        kendogridupdate();
       
    })

      function kendogridupdate() {
        debugger;
        var gridUpdate = $("#gridUpdate").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =UId%>&CustId=<% =CustId%>&complianceInstanceID=<% =compInstanceID%>',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =UId%>&CustId=<% =CustId%>&complianceInstanceID=<% =compInstanceID%>'
                        },
                        pageSize: 5,
                    },
                    //height: 150,
                    sortable: true,
                    filterable: true,
                    columnMenu: false,
                    pageable: true,
                    reorderable: true,
                    resizable: true,
                    multi: true,

                    noRecords: {
                        template: "No records available"
                    },
                    columns: [
                        {
                            field: "Title", title: 'Title',
                            width: "65%;",
                            attributes: {
                                style: 'white-space: nowrap;'
                            }, filterable: { multi: true, search: true }
                        },
                        //{
                        //    field: "Date", title: 'Date',
                        //    type: "date",
                        //    template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        //    attributes: {
                        //        style: 'white-space: nowrap;'
                        //    }, filterable: { multi: true, search: true }
                        //},
                        {
                            field: "Date", title: 'Date',
                            type: "date",
                            format: "{0:dd-MMM-yyyy}",
                            template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                            attributes: {
                                style: 'white-space: nowrap;'
                                //}, filterable: { multi: true, search: true }
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        type: "date",
                                        format: "{0:dd-MMM-yyyy}",
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                },
                            }
                        },
                        {
                            command: [
                                { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                            ], title: "Action", lock: true,// width: 150,
                        }
                    ]
                });

                $("#gridUpdate").kendoTooltip({
                    filter: ".k-grid-edit2",
                    content: function (e) {
                        return "Overview";
                    }
                });

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(1)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(2)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");
    }

            $(document).on("click", "#gridUpdate tbody tr .ob-overview", function (e) {

                var item = $("#gridUpdate").data("kendoGrid").dataItem($(this).closest("tr"));
                //OpendocumentsUpdates(item.Title);
                OpendocumentsUpdates(item.Description);

                return true;
            });


            function OpendocumentsUpdates(title) {

                document.getElementById('detailUpdate').innerHTML = title;// 'your tip has been submitted!';


                var myWindowAdv = $("#ViewUpdateDetails");

                function onClose() {

                }

                myWindowAdv.kendoWindow({
                    width: "85%",
                    height: "50%",
                    title: "Legal Updates",
                    visible: false,
                    actions: [
                        //"Pin",
                        //"Minimize",
                        //"Maximize",
                        "Close"
                    ],

                    close: onClose
                });

                //$("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

                myWindowAdv.data("kendoWindow").center().open();
                //e.preventDefault();
                return false;
            }

    </script>
</html>
