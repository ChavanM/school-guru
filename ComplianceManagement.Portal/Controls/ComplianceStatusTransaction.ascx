﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComplianceStatusTransaction.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.ComplianceStatusTransaction" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../tree/jquerysctipttop.css" rel="stylesheet" />
<script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
<link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />
   <style type="text/css">
            table#basic > tr, td {
                border-radius: 5px;
            }

            table#basic {
                border-collapse: unset;
                border-spacing: 3px;
            }

            .locationheadbg {
                background-color: #999;
                color: #fff;
                border: #666;
            }

            .locationheadLocationbg {
                background-color: #fff;
                 
            }

            td.locationheadLocationbg > span.tree-icon {
                background-color: #1976d2 !important;
                padding-right: 12px;
                color: white;
            }

            .GradingRating1 {
                background-color: #8fc156;
              
            }

            .GradingRating2 {
                background-color: #ffc107;
               
            }

            .GradingRating3 {
                background-color: #ef9a9a;
               
            }

            .Viewcss {
                width:50px;
                color: blue;
                text-align: center;
                cursor: pointer;
            }
             .downloadcss {
                width:100px;
                color: blue;
                text-align: center;
                cursor: pointer;
            }
        </style>
<script type="text/javascript">

    function fFilesubmit() {
       /// if (Displays()) {
            fileUpload = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_fuSampleFile');
            if (fileUpload.value != '') {
                document.getElementById("<%=UploadDocument.ClientID %>").click();
            }
        //}
    }
    function fWorkingFilesubmit() {
       
        fileUpload = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_FileUpload1');
       if (fileUpload.value != '') {
            document.getElementById("<%=UploadDocument.ClientID %>").click();
        }
    }
    
    function Workingdocumentlnktest() {
        WorkingDocumenttextboxempty = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_TxtworkingdocumentlnkStatutory');
        if (WorkingDocumenttextboxempty.value != '') {       
            document.getElementById("<%=UploadlinkWorkingfileStatutory.ClientID %>").click();
        }
    }
    function Compliancedocumentlnktest() {
        ComplianceDocumenttextboxempty = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_TxtCompliancedocumentlnkStatutory');
        if (ComplianceDocumenttextboxempty.value != '') {
            document.getElementById("<%=UploadlinkCompliancefileStatutory.ClientID %>").click();
        }
    }

    function fopenSampleFile(file) {
        $('#ContentPlaceHolder1_udcStatusTranscatopn_docViewerAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function SelectheaderCheckboxes(headerchk) {

        var count = 0;
        var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                var inputList = gv.getElementsByTagName("input");

                for (var i = 0; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox" && inputList[i].checked) {
                        count = count + 1;
                        $('#ConfirmationModel').modal('show');

                    }
                }
            }
            function callOnButtonYes1() {

                var count = 0;
                var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                var inputList = gv.getElementsByTagName("input");
                if (inputList != undefined) {
                    for (var i = 0; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox" && inputList[i].checked) {
                            inputList[i].checked = false;
                        }
                    }
                    $('#ConfirmationModel').modal('hide');
                }
                initializeDatePicker();
            }
            function callOnButtonNo1() {

                var count = 0;
                var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                var inputList = gv.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox" && inputList[i].checked) {
                        count = count + 1;
                    }
                }
                $('#ConfirmationModel').modal('hide');
                initializeDatePicker();
            }

            function fopendocfile(file) {
                $('#modalDocumentPerformerViewer').modal('show');
                $('#docViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }




            $(document).ready(function () {
                $("button[data-dismiss-modal=modal2]").click(function () {
                    $('#modalDocumentPerformerViewer').modal('hide');
                    $('#SampleFilePopUp').modal('hide');
                    $('#divActFilePopUp').modal('hide');
                    $('#DocumentPriview').modal('hide');
                });


            });
            function initializeComboboxUpcoming() {
                $("#<%= ddlStatus.ClientID %>").combobox();
         }

         function initializeDatePickerforPerformer1(date1) {

             var startDate = new Date();
             $('#<%= tbxDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1,
        });

        if (date1 != null) {
            $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date1);
        }
    }

    function initializeDatePickerOverDue(dateOverDue) {
        $('#<%= tbxDate.ClientID %>').datepicker('destroy');
        $('#<%= tbxDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: dateOverDue,
            numberOfMonths: 1,
        });

        if (dateOverDue != null) {
            $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", dateOverDue);
       }
   }

   function initializeDatePickerInTime(dateInTime) {
       $('#<%= tbxDate.ClientID %>').datepicker('destroy');
        $('#<%= tbxDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: dateInTime,
            numberOfMonths: 1,
        });

        if (dateInTime != null) {
            $("#<%= tbxDate.ClientID %>").datepicker("option", "maxDate", dateInTime);
        }
    }

    function fopendoctaskfileReview(file) {
        $('#modalDocumentPerformerViewer').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopn_docViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopenDocumentPriview(file) {
        $('#DocumentPriview').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopn_docPriview').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopendoctaskfileReviewPopUp() {
        $('#modalDocumentPerformerViewer').modal('show');
    }

    $(document).ready(function () {
        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#modalDocumentPerformerViewer').modal('hide');
        });

    });

    $(document).ready(function () {
        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#ContentPlaceHolder1_udcStatusTranscatopn_modalDocumentPerformerViewer').modal('hide');
        });

    });

    function fopendocfile() {
        $('#SampleFilePopUp').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopn_docViewerAll').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample.ClientID %>").text());
    }

    var validFilesTypes = ["exe", "bat", "dll"];
    function ValidateFilestatus() {

        var label = document.getElementById("<%=Label1.ClientID%>");
        var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
        var FileUpload1 = $("#<%=FileUpload1.ClientID%>").get(0).files;
        var isValidFile = true;

        for (var i = 0; i < fuSampleFile.length; i++) {
            var fileExtension = fuSampleFile[i].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                isValidFile = false;
                break;
            }
        }


        for (var i = 0; i < FileUpload1.length; i++) {
            var fileExtension = FileUpload1[i].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                isValidFile = false;
                break;
            }
        }

        if (!isValidFile) {
            label.style.color = "red";
            label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
        }
        return isValidFile;
    }

    function fillValuesInTextBoxes() {
        var chk = $("#ContentPlaceHolder1_udcStatusTranscatopn_chkPenaltySave").is(":checked");
        if (chk == true) {
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_txtInterest").value = "0";
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_txtPenalty").value = "0";
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_txtInterest").readOnly = true;
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_txtPenalty").readOnly = true;
        }
        else {
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_txtInterest").value = "";
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_txtPenalty").value = "";
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_txtInterest").readOnly = false;
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_txtPenalty").readOnly = false;
        }
    }

    function PenaltyValidate() {        
        var ddlstatus = $('select#ContentPlaceHolder1_udcStatusTranscatopn_ddlStatus option:selected').val();
        if (ddlstatus == -1) {
            $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
            $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select Status.");
            $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select Status.";
            return false;
        }

        if (ddlstatus == 12) {
            //var filename = $("#ContentPlaceHolder1_udcStatusTranscatopn_FileUpload1").val();
            //if (filename == "") {
            //    $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
            //    $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select working file for upload.");
            //    $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
            //    document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select working file for upload.";
            //    return false;
            //}

            var rowscount = $("#<%=grdDocument.ClientID %> tr").length;
            if (rowscount > 1) {
                var chkWorkingFileCount = 0;
                for (var i = 0; i < rowscount - 1; i++) {
                    var lblDocumentType = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_grdDocument_lblDocType_' + i);
                    if (lblDocumentType != null) {
                        if (lblDocumentType.innerText == 'Working Files') {
                            chkWorkingFileCount = 1;
                            break;
                        }
                    }
                    else {
                        $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                        $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select working file for upload.");
                        $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                        document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select working file for upload.";
                        return false;
                    }
                }
                if (chkWorkingFileCount == 0) {
                    $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select working file for upload.");
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                    document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select working file for upload.";
                    return false;
                }
            }
            else {
                $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select working file for upload.");
                $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select working file for upload.";
                return false;
            }
        }
        else {
            var filename = $("#ContentPlaceHolder1_udcStatusTranscatopn_fuSampleFile").val();

            //if (filename == "") {
            //    $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
            //    $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select documents for upload.");
            //    $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
            //    document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select documents for upload.";
            //    return false;
            //}

          

           // if (document.getElementById($("#grdDocument")) != null) {
            var rowscount = $("#<%=grdDocument.ClientID %> tr").length;
            if (rowscount == 0) {
                    $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select documents for upload.");
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                    document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select documents for upload.";
                    return false;
            }
            else {
                if (rowscount > 1) {

                    var chkWorkingFileCount = 0;
                    for (var i = 0; i < rowscount - 1; i++) {
                        var lblDocumentType = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_grdDocument_lblDocType_' + i);
                        if (lblDocumentType != null) {
                            if (lblDocumentType.innerText == "Compliance Document") {
                                chkWorkingFileCount = 1;
                                break;
                            }
                        }
                        else {
                            $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                            $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select documents for upload.");
                            $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select documents for upload.";
                            return false;
                        }
                    }
                    if (chkWorkingFileCount == 0) {
                        $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                        $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select documents for upload.");
                        $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                        document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select documents for upload.";
                        return false;
                    }
                }
                else {
                    $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select documents for upload.");
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                    document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select documents for upload.";
                    return false;
                }
              
            }
        }

        var tbxDate = $("#ContentPlaceHolder1_udcStatusTranscatopn_tbxDate").val();
        if (tbxDate == "") {
            $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
            $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select Date.");
            $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select Date.";
            return false;
        }
        if (ddlstatus == 3) {
            var chk = $("#ContentPlaceHolder1_udcStatusTranscatopn_chkPenaltySave").is(":checked");
            if (chk == false) {
                var txtInterest = $("#ContentPlaceHolder1_udcStatusTranscatopn_txtInterest").val();
                var txtPenalty = $("#ContentPlaceHolder1_udcStatusTranscatopn_txtPenalty").val();
                if (txtInterest == "" || txtPenalty == "" || txtInterest == "" || txtPenalty == "0") {
                    $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please enter interest and penalty");
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                    document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please enter interest and penalty";
                    return false;
                }
            }
            var txtRemark = $("#ContentPlaceHolder1_udcStatusTranscatopn_tbxRemarks").val();
            if (txtRemark == "" || txtRemark == "0") {
                $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please enter remark");
                $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please enter remark";
                return false;
            }

        }
    }

    function PenaltyEventValidate() {
        var ddlstatus = $('select#ContentPlaceHolder1_udcStatusTranscatopn_ddlStatus option:selected').val();
        if (ddlstatus == -1) {
            $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
            $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select Status.");
            $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select Status.";
            return false;
        }
        var tbxDate = $("#ContentPlaceHolder1_udcStatusTranscatopn_tbxDate").val();
        if (tbxDate == "") {
            $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
            $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please select Date.");
            $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
            document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please select Date.";
            return false;
        }
        if (ddlstatus == 3) {
            var chk = $("#ContentPlaceHolder1_udcStatusTranscatopn_chkPenaltySave").is(":checked");
            if (chk == false) {
                var txtInterest = $("#ContentPlaceHolder1_udcStatusTranscatopn_txtInterest").val();
                var txtPenalty = $("#ContentPlaceHolder1_udcStatusTranscatopn_txtPenalty").val();
                if (txtInterest == "" || txtPenalty == "" || txtInterest == "" || txtPenalty == "0") {
                    $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please enter interest and penalty");
                    $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                    document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please enter interest and penalty";
                    return false;
                }
            }

            var txtRemark = $("#ContentPlaceHolder1_udcStatusTranscatopn_tbxRemarks").val();
            if (txtRemark == "" || txtRemark == "0") {
                $("#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg").css('display', 'block');
                $('#ContentPlaceHolder1_udcStatusTranscatopn_Labelmsg').text("Please enter remark");
                $('#ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1').IsValid = false;
                document.getElementById("ContentPlaceHolder1_udcStatusTranscatopn_ValidationSummary1").value = "Please enter remark";
                return false;
            }
        }
    }
    function DiffPerformer() {
        document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtDiffAB').value = "";
        var ValueAsPerSystem = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtValueAsPerSystem').value;
        var ValueAsPerReturn = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtValueAsPerReturn').value;

        var AB = parseInt(ValueAsPerSystem) - parseInt(ValueAsPerReturn);

        if (!isNaN(AB)) {
            document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtDiffAB').value = AB;
        }
        else {
            document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtDiffAB').value = "";
        }

        document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtDiffBC').value = ""
        var LiabilityPaid = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtLiabilityPaid').value;
        var BC = parseInt(ValueAsPerReturn) - parseInt(LiabilityPaid);
        if (!isNaN(BC)) {
            document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtDiffBC').value = BC;
        }
        else {
            document.getElementById('ContentPlaceHolder1_udcStatusTranscatopn_txtDiffBC').value = "";
        }
    }

    function fopendocfileAct(file) {

        $('#divActFilePopUp').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopn_IframeActFile').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopendocfileActPopUp() {
        $('#divActFilePopUp').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopn_IframeActFile').attr('src', "../docviewer.aspx?docurl=");
    }
</script>
<script type="text/javascript">

    function fr() {
        $(document).ready(function () {
            $('#basic').simpleTreeTable({
                collapsed: true
            });
        });
    }

   
</script>
<style>
    tr.spaceUnder > td {
        padding-bottom: 1em;
    }
</style>

<div id="divComplianceDetailsDialog">
    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" Style="padding-left: 5%" runat="server" Display="none"
                        class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup1" />

                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in"
                        EnableClientScript="true" ValidationGroup="ComplianceValidationGroup1" Style="display: none;" />

                    <asp:Label ID="Label1" class="alert alert-block alert-danger fade in" Visible="false" runat="server"></asp:Label>
                    <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                    <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                    <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                </div>
                <div class="clearfix" style="margin-bottom: 10px"></div>

                <div>

                    <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                    <div id="divRiskType" runat="server" class="circle"></div>
                    <asp:Label ID="lblRiskType" Style="width: 300px; margin-left: -17px; font-weight: bold; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                </div>

                <div id="ActDetails" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">

                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                            <h2>Act Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseActDetails" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%;">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Act Name</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblActName" Style="width: 88%; font-size: 13px; color: #333;"
                                                            autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Section /Rule</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblRule" Style="width: 88%; font-size: 13px; color: #333;"
                                                            autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Act Document(s)</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <div style="width: 90%; margin-top: 15px;">
                                                            <%=ActDocString%>
                                                        </div>
                                               <%--         <table style="width: 100%; text-align: left">
                                                           
                                                            <thead>
                                                                <tr>
                                                                    <td style="vertical-align: top">
                                                                        <asp:Repeater ID="rptActDocVersion" runat="server" OnItemCommand="rptActDocVersion_ItemCommand"
                                                                            OnItemDataBound="rptActDocVersion_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <tr>
                                                                                            <td style="width: 40%">
                                                                                                <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("Act_ID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                                                    runat="server" 
                                                                                                    Text='<%# Eval("Act_TypeVersionDate") != null ? Eval("DocumentType").ToString() +" " + ((DateTime)Eval("Act_TypeVersionDate")).ToString("MMM-yyyy") : Eval("DocumentType").ToString() %>'>                                                                                                     
                                                                                                </asp:LinkButton></td>
                                                                                            <td style="width: 60%">
                                                                                                <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("Act_ID") + ","+ Eval("Version") %>'
                                                                                                    ID="btnActVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                </asp:LinkButton>
                                                                                                <asp:Label ID="lblSlashPerformer" Text="/" Style="color: blue;" runat="server" />
                                                                                                <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("Act_ID") + ","+ Eval("Version") %>' Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />
                                                                                                <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btnActVersionDoc" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                            </thead>
                                                        </table>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="ComplianceDetails" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">

                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                            <h2>Compliance Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseComplianceDetails" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Compliance Id</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceID" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Short Description</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Detailed Description</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblDetailedDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Penalty</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="OthersDetails" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails">
                                            <h2>Additional Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapseOthersDetails" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Sample Form/Attachment</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblFormNumber" Style="width: 150px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:LinkButton ID="lbDownloadSample" Style="width: 150px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                                <asp:Label ID="lblSlash" Text="/" Style="color: blue;" runat="server" />
                                                                <asp:LinkButton ID="lnkViewSampleForm" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClientClick="fopendocfile();" />
                                                                <asp:Label ID="lblpathsample" runat="server" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Regulatory website link</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:LinkButton ID="lnkSampleForm" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                            runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblRefrenceText" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Location</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Period</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                        <asp:HiddenField ID="hiddenDueDate" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder" id="trAuditChecklist" runat="server">
                                                    <td style="width: 25%; font-weight: bold;">Audit Checklist</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblAuditChecklist" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div runat="server" id="divTask" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                        <h2>Main Task Details</h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>

                                <div id="collapseTaskSubTask" class="collapse in">
                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                        <asp:Label ID="lbltask" runat="server" ForeColor="#ff3300"></asp:Label><br />
                                        <asp:GridView runat="server" ID="gridSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                            OnRowCommand="gridSubTask_RowCommand" OnRowDataBound="gridSubTask_RowDataBound" AutoPostBack="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                        <asp:Label ID="lblTaskScheduledOnID" runat="server" Visible="false" Text='<%# Eval("TaskScheduledOnID") %>'></asp:Label>
                                                        <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                        <asp:Label ID="lblTaskInstanceID" runat="server" Visible="false" Text='<%# Eval("TaskInstanceID") %>'></asp:Label>
                                                        <asp:Label ID="lblMainTaskID" runat="server" Visible="false" Text='<%# Eval("MainTaskID") %>'></asp:Label>
                                                        <asp:Label ID="lblForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                        <asp:Label ID="lblComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ComplianceScheduleOnID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Task">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Performer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>

                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Reviewer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                </asp:LinkButton>
                                                                <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                    Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                <asp:CheckBox ID="chkTask" CssClass="sbtask" Style="display: none;" data-attr='<%# Eval("IsYesNo") %>' Width="30px" data-toggle="tooltip" AutoPostBack="true" ToolTip="Click to close if not applicable" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="UpdateComplianceStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus">
                                            <h2>Update Compliance Status</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapseUpdateComplianceStatus" class="panel-collapse collapse in">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div style="margin-bottom: 7px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 25%;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                    </td>

                                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:DropDownList runat="server" ID="ddlStatus" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" class="form-control m-bot15" Style="width: 280px;" AutoPostBack="true" />

                                                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Status." ControlToValidate="ddlStatus"
                                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup1"
                                                            Display="None" />
                                                    </td>
                                                </tr>
                                                <% if (UploadDocumentLink == "True")
                                                {%>
                                                <tr>
                                                    <td style="width: 25%;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Working files(s)</label>
                                                    </td>

                                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:TextBox runat="server" ID="TxtworkingdocumentlnkStatutory" onchange="Workingdocumentlnktest()" class="form-control" />
                                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Button ID="UploadlinkWorkingfileStatutory" runat="server" Text="Upload Document" Style="display: none;" OnClick="UploadlinkWorkingfile_Click"
                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="UploadlinkWorkingfileStatutory" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 25%;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Compliance Document(s)</label>
                                                    </td>

                                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:TextBox runat="server" ID="TxtCompliancedocumentlnkStatutory" onchange="Compliancedocumentlnktest()" class="form-control" />
                                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Button ID="UploadlinkCompliancefileStatutory" runat="server" Text="Upload Document" Style="display: none;" OnClick="UploadlinkCompliancefile_Click"
                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="UploadlinkCompliancefileStatutory" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <%}%>
                                            </table>
                                        </div>
                                        <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 25%;">
                                                        <asp:Label ID="lblDocComplasary" runat="server" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</asp:Label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Upload Compliance Document(s)</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 60%;">
                                                        <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" onchange="fFilesubmit()"  Style="color: black" />
                                                        <%--  <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuSampleFile"
                                                            runat="server" ID="rfvFile" ValidationGroup="ComplianceValidationGroup1" Display="None" /> --%>
                                                    </td>
                                                    <td style="width: 13%;">
                                                        <%--<asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>--%>
                                                                <asp:Button ID="UploadDocument" runat="server" Text="Upload Document" style="display:none;" OnClick="UploadDocument_Click"
                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document"
                                                                    CausesValidation="true" />
                                                            <%--</ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="UploadDocument" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="margin-bottom: 7px" runat="server" id="divWorkingfiles">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 25%;">
                                                        <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Upload Working Files(s)</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:FileUpload ID="FileUpload1" Multiple="Multiple" onchange="fWorkingFilesubmit()" runat="server" Style="color: black" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="margin-bottom: 7px" runat="server" id="divgrdFiles">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 100%;">
                                                        <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                            PageSize="100" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" OnRowDataBound="grdDocument_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DocName") %>' ToolTip='<%# Eval("DocName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                            <asp:Label ID="lblDocType" runat="server" data-placement="bottom" Text='<%# Eval("DocType") %>' ToolTip='<%# Eval("DocType") %>'></asp:Label>
                                                                            <asp:Label ID="lblScheduleOnID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduleOnID") %>'></asp:Label>
                                                                            <asp:Label ID="lblComplianceInstanceID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="lnkDownloadDocument" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerSettings Visible="false" />
                                                            <PagerTemplate>
                                                            </PagerTemplate>
                                                            <EmptyDataTemplate>
                                                                No Record Found
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>

                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:TextBox runat="server" ID="tbxDate" placeholder="DD-MM-YYYY" ReadOnly="true" class="form-control" Style="width: 115px; cursor: text;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <fieldset id="fieldsetpenalty" runat="server" visible="false" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <table style="width: 100%">
                                                    <tr id="trPenalty" runat="server">
                                                        <td style="width: 11%; font-weight: bold;">
                                                            <asp:CheckBox ID="chkPenaltySave" onclick="fillValuesInTextBoxes()" Text="Value is not known at this moment" runat="server" />
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;"></td>
                                                        <td style="width: 10%;"></td>

                                                        <td style="width: 9%;"></td>
                                                        <td style="width: 2%; font-weight: bold;"></td>
                                                        <td style="width: 10%;"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 11%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label style="font-weight: bold; vertical-align: text-top;">Interest(INR) </label>
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 10%;">
                                                            <asp:TextBox runat="server" ID="txtInterest" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                TargetControlID="txtInterest" />
                                                        </td>

                                                        <td style="width: 9%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label style="font-weight: bold; vertical-align: text-top;">Penalty Amount(INR) </label>
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 10%;">
                                                            <asp:TextBox runat="server" ID="txtPenalty" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            <asp:CompareValidator runat="server" ID="cmpNumbers" ControlToValidate="txtPenalty" Type="Double" Operator="DataTypeCheck" ErrorMessage="Please enter proper value ofpenalty" />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                TargetControlID="txtPenalty" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                            <br />
                                            <fieldset id="fieldsetTDS" runat="server" visible="false" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <table style="width: 100%">
                                                    <tr class="spaceUnder">
                                                        <td style="width: 11%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label style="font-weight: bold; vertical-align: text-top;">Nature of compliance </label>
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 10%;">
                                                            <asp:Label ID="lblNatureofcompliance" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            <asp:Label ID="lblNatureofcomplianceID" Visible="false" runat="server" />
                                                        </td>
                                                        <td style="width: 9%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label id="lblValueAsPerSystem" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per system (A) </label>
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 10%;">
                                                            <%-- OnTextChanged="txtValueAsPerSystem_TextChanged" AutoPostBack="true"--%>
                                                            <asp:TextBox runat="server" ID="txtValueAsPerSystem" onkeyup="DiffPerformer();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                TargetControlID="txtValueAsPerSystem" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 11%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label id="lblValueAsPerReturn" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per return (B) </label>
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 10%;">
                                                            <%-- OnTextChanged="txtValueAsPerReturn_TextChanged" AutoPostBack="true"--%>
                                                            <asp:TextBox runat="server" ID="txtValueAsPerReturn" onkeyup="DiffPerformer();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                TargetControlID="txtValueAsPerReturn" />
                                                        </td>
                                                        <td style="width: 9%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label id="lbldiffAB" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (A)-(B) </label>
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;">:</td>
                                                        <td style="width: 10%;">
                                                            <asp:TextBox runat="server" ID="txtDiffAB" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                        </td>
                                                    </tr>

                                                    <tr id="trreturn" runat="server" class="spaceUnder">
                                                        <td style="width: 11%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label id="lblLiabilityPaid" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability Paid (C) </label>
                                                        </td>
                                                        <td id="tdLiabilityPaidSC" runat="server" style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 10%;">
                                                            <%--  OnTextChanged="txtLiabilityPaid_TextChanged" AutoPostBack="true"--%>
                                                            <asp:TextBox ID="txtLiabilityPaid" runat="server" onkeyup="DiffPerformer();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                TargetControlID="txtLiabilityPaid" />
                                                        </td>
                                                        <td style="width: 9%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label id="lbldiffBC" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (B)-(C) </label>
                                                        </td>
                                                        <td id="tddiffBCSC" runat="server" style="width: 2%; font-weight: bold;">:</td>
                                                        <td style="width: 10%;">
                                                            <asp:TextBox runat="server" ID="txtDiffBC" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>


                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 25%;">
                                                        <% if (IsRemarkCompulsary)
                                                            {%>
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <%}
                                                        else
                                                        {%>
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <%}%>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" class="form-control" Rows="2" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <%--  OnClientClick="javascript:return ValidateFilestatus();"--%>
                                        <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px;">
                                            <asp:Button Text="Submit" runat="server" ID="btnSave"
                                                OnClientClick="javascript:return PenaltyValidate();" OnClick="btnSave_Click" CssClass="btn btn-search"
                                                ValidationGroup="ComplianceValidationGroup1" />

                                            <asp:Button Text="Submit" runat="server" ID="btnSaveDOCNotCompulsory"
                                                OnClientClick="javascript:return PenaltyEventValidate(); "
                                                OnClick="btnSave_Click" CssClass="btn btn-search"
                                                ValidationGroup="ComplianceValidationGroup1" />

                                            <asp:Button Text="Close" ID="btnCancel" OnClientClick="fcloseandcallcal();"
                                                Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div runat="server" id="divDeleteDocument" visible="false" style="text-align: left;">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                        <table width="100%">
                            <tr>
                                <td style="width: 50%">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th>Compliance Related Documents</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                            </asp:LinkButton></td>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                ID="lbtLinkDocbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblWorkingFiles">
                                                        <thead>
                                                            <th>Compliance Working Files</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                            </asp:LinkButton></td>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                ID="lbtLinkbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>

                    </fieldset>
                </div>

                <div id="AuditLog" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog">
                                            <h2>Audit Log</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseAuditLog" class="collapse">
                                    <div runat="server" id="log" style="text-align: left;">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                    AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                    OnRowCreated="grdTransactionHistory_RowCreated" BorderWidth="0px" OnSorting="grdTransactionHistory_Sorting"
                                                    DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                    <Columns>
                                                        <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                        <asp:TemplateField HeaderText="Date">
                                                            <ItemTemplate>
                                                                <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Interest" HeaderText="Interest" />
                                                        <asp:BoundField DataField="Penalty" HeaderText="Penalty" />
                                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                        <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                        <div></div>
                                        <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                        <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="lbDownloadSample" />
            <asp:PostBackTrigger ControlID="btnSaveDOCNotCompulsory" />
            <asp:PostBackTrigger ControlID="UploadDocument"/> 
        </Triggers>
    </asp:UpdatePanel>
</div>

<div class="modal fade" id="SampleFilePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="$('#SampleFilePopUp').modal('hide');" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
                <div style="float: left; width: 10%">
                    <table width="100%" style="text-align: left; margin-left: 5%;">
                        <thead>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdatleMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptComplianceSampleView" runat="server" OnItemCommand="rptComplianceSampleView_ItemCommand"
                                                OnItemDataBound="rptComplianceSampleView_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th>Sample Forms</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView"
                                                                        runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div style="float: left; width: 90%">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                        <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="modal fade" id="modalDocumentPerformerViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div style="float: left; width: 10%">
                            <table width="100%" style="text-align: left; margin-left: 5%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Versions</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="float: left; width: 90%">
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="lblMessage" runat="server" Style="color: red;"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docViewerPerformerAll" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="DocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="$('#DocumentPriview').modal('hide');" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
                <div style="width: 100%;">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                        <iframe src="about:blank" id="docPriview" runat="server" width="100%" height="535px"></iframe>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ConfirmationModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 30%;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <div style="width: 10%; float: right">
                    <button type="button" class="close" onclick="$('#ConfirmationModel').modal('hide');" aria-hidden="true">×</button>
                </div>
                <div style="width: 90%; align-content: center; margin-left: 82px; float: left;">
                    <p style="font-size: 20px">Confirmation</p>
                </div>
            </div>
            <div class="modal-body">
                <div id="DivYesNoConf">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="height: 150px;">
                                <div style="width: 80%; margin-left: 15%;">
                                    <p>Proceeds to subsequent checks?</p>
                                    <div style="padding: 13px; padding-left: 65px;">
                                        <asp:LinkButton ID="lblYes" runat="server" Text="Yes" CssClass="btn btn-primary" OnClientClick="callOnButtonYes1()"></asp:LinkButton>
                                        <asp:LinkButton ID="lblNo" runat="server" Text="No" CssClass="btn btn-primary" OnClick="lblNo_Click" OnClientClick="callOnButtonNo1()"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>






