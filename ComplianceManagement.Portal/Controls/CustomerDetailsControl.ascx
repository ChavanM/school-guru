﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerDetailsControl.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.CustomerDetailsControl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<script type="text/javascript">
    $(function () {
        $('#divCustomersDialog').dialog({
            height: 580,
            width: 700,
            autoOpen: false,
            draggable: true,
            title: "Customer Information",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    });

    function initializeDatePicker(date) {

        var startDate = new Date();
        $("#<%= txtStartDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "minDate", startDate);
            }
        });

        $("#<%= txtEndDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "maxDate", startDate);
            }
        });

        if (date != null) {
            $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
            $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
        }
    }
        
    function initializeCombobox() {
        $("#<%= ddlCustomerStatus.ClientID %>").combobox();
        $("#<%= ddlLocationType.ClientID %>").combobox();
        $("#<%= ddlTaskApplicable.ClientID %>").combobox();
        $("#<%= ddlSPName.ClientID %>").combobox();
        $("#<%= ddlComplianceApplicable.ClientID %>").combobox();
        $("#<%= ddlLabelApplicable.ClientID %>").combobox();
        $("#<%= ddlComplianceProductType.ClientID %>").combobox();
    }   
</script>
<div id="divCustomersDialog">
    <asp:UpdatePanel ID="upCustomers" runat="server" UpdateMode="Conditional" OnLoad="upCustomers_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="CustomerValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="CustomerValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                </div>
                <div style="margin-bottom: 7px">
                    <asp:Label Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                       Is Service Provider</asp:Label>
                    <asp:CheckBox ID="chkSp" runat="server" AutoPostBack="true" OnCheckedChanged="chkSp_CheckedChanged" />
                </div>
                <div style="margin-bottom: 7px">
                    <asp:Label ID="lblast" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label ID="lblsp" Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                       Service Provider Name</asp:Label>
                    <asp:DropDownList runat="server" ID="ddlSPName" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <%--    <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />--%>
                    </asp:DropDownList>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Name</label>
                    <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 390px;" MaxLength="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty."
                        ControlToValidate="tbxName" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Address</label>
                    <asp:TextBox runat="server" ID="tbxAddress" Style="height: 50px; width: 390px;" MaxLength="500"
                        TextMode="MultiLine" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Location Type</label>
                    <asp:DropDownList runat="server" ID="ddlLocationType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Name</label>
                    <asp:TextBox runat="server" ID="tbxBuyerName" Style="height: 16px; width: 390px;"
                        MaxLength="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Buyer Name can not be empty."
                        ControlToValidate="tbxBuyerName" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Contact No</label>
                    <asp:TextBox runat="server" ID="tbxBuyerContactNo" Style="height: 16px; width: 390px;"
                        MaxLength="15" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Buyer Contact Number can not be empty."
                        ControlToValidate="tbxBuyerContactNo" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid contact number." ControlToValidate="tbxBuyerContactNo"
                        ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxBuyerContactNo" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                        ValidationGroup="CustomerValidationGroup" ErrorMessage="Please enter only 10 digit."
                        ControlToValidate="tbxBuyerContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Email</label>
                    <asp:TextBox runat="server" ID="tbxBuyerEmail" Style="height: 16px; width: 390px;"
                        MaxLength="200" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Buyer Email can not be empty."
                        ControlToValidate="tbxBuyerEmail" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid email." ControlToValidate="tbxBuyerEmail"
                        ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Customer Status</label>
                    <asp:DropDownList runat="server" ID="ddlCustomerStatus" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Start Date</label>
                    <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; width: 390px;" ReadOnly="true"
                        MaxLength="200" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        End Date</label>
                    <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; width: 390px;" ReadOnly="true"
                        MaxLength="200" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Disk Space</label>
                    <asp:TextBox runat="server" ID="txtDiskSpace" Style="height: 16px; width: 390px;"
                        MaxLength="200" />
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Internal Compliance Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlComplianceApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Task Management Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlTaskApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Label Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlLabelApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>

                <div style="margin-bottom: 7px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Compliance Product Type</label>
                    <asp:DropDownList runat="server" ID="ddlComplianceProductType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" CssClass="custom-combobox">
                        <asp:ListItem Text="Avacom" Value="0" Selected="True" />
                        <asp:ListItem Text="HRProduct Only" Value="2" />
                        <asp:ListItem Text="Avacom + TL" Value="3" />
                        <asp:ListItem Text="Avacom + HRProduct" Value="4" />
                    </asp:DropDownList>
                </div>
                   <div style="margin-bottom: 7px" id="divlogo" runat="server">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Upload Logo</label>
                    <asp:FileUpload ID="UserImageUpload" AllowMultiple="false" runat="server" />
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" Visible="false" />
                </div>
                <div style="margin-bottom: 7px" id="divlogoName" runat="server">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Upload Logo Name</label>
                    <asp:Label ID="lblLogoName" runat="server"></asp:Label>
                </div>
                <div style="margin-bottom: 7px">
                    <asp:Label ID="lblRErrormsg" class="alert alert-block alert-danger fade in" runat="server"></asp:Label>
                </div>

                <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;margin-top:23px">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClientClick="javascript:return ValidateFilestatus();" OnClick="btnSave_Click" CssClass="button"
                    style="margin-left:-25px"    ValidationGroup="CustomerValidationGroup" />
                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divCustomersDialog').dialog('close');" />
                </div>
            </div>

            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
               <p style="color: red;"><strong>Note:</strong> (*) Please Upload Logo of Size(80x58)pixels</p>

            </div>
        </ContentTemplate>
          <Triggers>
             <asp:PostBackTrigger ControlID="btnSave" />
             </Triggers>
    </asp:UpdatePanel>
</div>
<script type="text/javascript">
    function ValidateFilestatus() {
        var InvalidvalidFilesTypes = ["pptx", "docx","pdf", "ppt", "word", "exe", "bat", "dll", "docx", "xlsx", "html", "css", "js", "txt", "doc", "gif", "jsp",
            "php5","pht","phtml","shtml","asa","cer","asax","swf","xap","aspx","asp","zip","rar","php","reg","rdp"];
        var isValidFile = true;
        var lblerror = document.getElementById("<%=lblRErrormsg.ClientID%>");
        if (lblerror != null || lblerror != undefined) {
            var CheckFile = $("#<%=UserImageUpload.ClientID%>").files; 
            if (CheckFile != undefined) {
                var fuSampleFile = $("#<%=UserImageUpload.ClientID%>").get(0).files;
                for (var i = 0; i < fuSampleFile.length; i++) {
                    var fileExtension = fuSampleFile[i].name.split('.').pop();
                    if (InvalidvalidFilesTypes.indexOf(fileExtension) != -1) {
                        isValidFile = false;
                        break;
                    }
                }
                if (!isValidFile) {
                    alert("Invalid file extension. format not supported.");
                    //lblerror.style.color = "red";                
                    //lblerror.innerHTML = "Invalid file extension. format not supported.";                
                }
                else {
                    $('#btnSave')[0].Click();
                }
            }
            else {
                $('#btnSave')[0].Click();
            }
        }
    }
</script>
