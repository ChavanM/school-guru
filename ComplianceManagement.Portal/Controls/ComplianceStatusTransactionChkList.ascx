﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComplianceStatusTransactionChkList.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.ComplianceStatusTransactionChkList" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

    <link href="../tree/jquerysctipttop.css" rel="stylesheet" />
    <script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
    <link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />

<script type="text/javascript">
      function fFilesubmit() {
          fileUpload = document.getElementById('ContentPlaceHolder1_udcStatusTranscatopnChkList_fuSampleFile');
            if (fileUpload.value != '') {
                document.getElementById("<%=UploadDocument.ClientID %>").click();
            }
    }
    function openInNewTab(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }

    function fopenDocumentPriview(file) {
        $('#DocumentPriview').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopnChkList_docPriview').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopendoctaskfileReview(file) {
        $('#modalDocumentChecklistViewer').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopnChkList_docChecklistViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    $(document).ready(function () {

        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#SampleFilePopUp').modal('hide');
            $('#modalDocumentChecklistViewer').modal('hide');
        });

    });


    //var validFilesTypes = ["exe", "bat", "zip", "rar", "dll"];
    var validFilesTypes = ["exe", "bat", "dll"];
    function ValidateFile() {

        var label = document.getElementById("<%=Label1.ClientID%>");
        var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
       <%-- var FileUpload1 = $("#<%=FileUpload1.ClientID%>").get(0).files;--%>
        var isValidFile = true;

        for (var i = 0; i < fuSampleFile.length; i++) {
            var fileExtension = fuSampleFile[i].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                isValidFile = false;
                break;
            }
        }



        if (!isValidFile) {
            label.style.color = "red";
            //label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
            label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
        }
        return isValidFile;
    }

</script>
 <style type="text/css">
        table#basic > tr, td {
            border-radius: 5px;
        }

        table#basic {
            border-collapse: unset;
            border-spacing: 3px;
        }

        .locationheadbg {
            background-color: #999;
            color: #fff;
            border: #666;
        }

        .locationheadLocationbg {
            background-color: #fff;
        }

        td.locationheadLocationbg > span.tree-icon {
            background-color: #1976d2 !important;
            padding-right: 12px;
            color: white;
        }

        .GradingRating1 {
            background-color: #8fc156;
        }

        .GradingRating2 {
            background-color: #ffc107;
        }

        .GradingRating3 {
            background-color: #ef9a9a;
        }

        .Viewcss {
            width: 50px;
            color: blue;
            text-align: center;
            cursor: pointer;
        }

        .downloadcss {
            width: 100px;
            color: blue;
            text-align: center;
            cursor: pointer;
        }
    </style>
<style>
    tr.spaceUnder > td {
        padding-bottom: 1em;
    }
</style>

<div id="divComplianceDetailsDialog">
    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup1" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="true"
                        ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    <asp:Label ID="Label1" Visible="false" runat="server"></asp:Label>
                    <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                    <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                </div>
                <div class="clearfix" style="margin-bottom: 10px"></div>

                <div>

                    <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                    <div id="divRiskType" runat="server" class="circle"></div>
                    <asp:Label ID="lblRiskType" Style="width: 300px; margin-left: -17px; font-weight: bold; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                </div>

                <div id="ActDetails" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">

                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                            <h2>Act Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseActDetails" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%;">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Act Name</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblActName" Style="width: 88%; font-size: 13px; color: #333;"
                                                            autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Section /Rule</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblRule" Style="width: 88%; font-size: 13px; color: #333;"
                                                            autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Act Document(s)</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <div style="width: 90%; margin-top: 15px;">
                                                            <%=ActDocString%>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="ComplianceDetails" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">

                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                            <h2>Compliance Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseComplianceDetails" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Compliance Id</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceID" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Short Description</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Short Form</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblshortform" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Detailed Description</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblDetailedDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Penalty</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="OthersDetails" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails">
                                            <h2>Additional Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapseOthersDetails" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Sample Form</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblFormNumber" Style="width: 150px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:LinkButton ID="lbDownloadSample" Style="width: 150px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                                <asp:Label ID="lblSlash" Text="/" Style="color: blue;" runat="server" />
                                                                <asp:LinkButton ID="lnkViewSampleForm" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClientClick="fopendocfile();" />
                                                                <asp:Label ID="lblpathsample" runat="server" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Regulatory website link</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:LinkButton ID="lnkSampleForm" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                            runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblRefrenceText" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Location</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Period</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder" id="trAuditChecklist" runat="server">
                                                    <td style="width: 25%; font-weight: bold;">Audit Checklist</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblAuditChecklist" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div runat="server" id="divTask" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                        <h2>Main Task Details</h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>

                                <div id="collapseTaskSubTask" class="collapse in">
                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                        <asp:GridView runat="server" ID="gridSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            AllowPaging="true" PageSize="5" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                            OnRowCommand="gridSubTask_RowCommand" OnRowDataBound="gridSubTask_RowDataBound" AutoPostBack="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Task">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Performer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>

                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Reviewer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                </asp:LinkButton>
                                                                <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                    Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                <%-- <asp:PostBackTrigger ControlID="btnSubTaskDocView" />--%>
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="UpdateComplianceStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus">
                                            <h2>Update Compliance Status</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapseUpdateComplianceStatus" class="panel-collapse collapse in">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <%-- <div style="margin-bottom: 7px">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%;">
                                                          <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                         <label style="font-weight: bold; vertical-align:text-top;">Status</label>
                                                    </td>
                                                 
                                                    <td style="width: 2%; font-weight: bold; vertical-align:text-top;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15" Style="width: 280px;" />
                                                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Status." ControlToValidate="ddlStatus"
                                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup1"
                                                            Display="None" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>--%>
                                        <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                                            <table style="width: 100%">
                                                   <% if (UploadDocumentLink == "True")
                                                {%>
                                                 <tr>
                                                    <td style="width: 25%;">
                                                         <% if (IsDocumentCompulsary)
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <%}
                                                                else
                                                                {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <%}%>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Compliance Document(s)</label>
                                                    </td>

                                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:TextBox runat="server" ID="TxtChecklistDocument"  class="form-control" /> <%--onchange="Workingdocumentlnktest()"--%>
                                                         </td>
                                                    <td>
                                                       <%-- <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>--%>
                                                                <asp:Button ID="Uploadlingchecklistfile" runat="server" Text="Add Link" Style="" OnClick="Uploadlingchecklistfile_Click"
                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Add Link" />
                                                           <%-- </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="Uploadlingchecklistfile" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                  </td>
                                                </tr>
                                                 <%}%>
                                                 <% if (UploadDocumentLink == "False")
                                                {%>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%;">

                                                          <% if (IsDocumentCompulsary)
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <%}
                                                                else
                                                                {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <%}%>
                                                        <asp:Label ID="lblDocComplasary" runat="server" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</asp:Label>
                                                        <%-- <label id="lblDocComplasary" runat="server" style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>--%>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Upload Compliance Document(s)</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 60%;">
                                                        <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" onchange="fFilesubmit()" Style="color: black" />
                                                        <%-- <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuSampleFile"
                                                            runat="server" ID="rfvFile" ValidationGroup="ComplianceValidationGroup1" Display="None" />--%>
                                                    </td>
                                                    <td style="width: 13%;">
                                                         <%-- <asp:UpdatePanel runat="server">
                                                            <ContentTemplate>--%>
                                                                <asp:Button ID="UploadDocument" runat="server" Text="Upload Document" Style="display: none;" OnClick="UploadDocument_Click"
                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document" CausesValidation="true" />
                                                          <%--  </ContentTemplate>
                                                        </asp:UpdatePanel>--%>
                                                    </td>
                                                </tr>
                                                   <%}%>
                                            </table>
                                        </div>

                                               <div style="margin-bottom: 7px" runat="server" id="divgrdFiles">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 100%;">
                                                        <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                            PageSize="100" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" OnRowDataBound="grdDocument_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                              <asp:LinkButton ID="lnkRedierctDocument" Style="color: blue;text-decoration: underline;" runat="server" Text='<%# Eval("DocName") %>' OnClientClick=<%# "openInNewTab('" + Eval("DocPath") + "')" %>>                                                                                                                                                                             
                                                                              </asp:LinkButton>
                                                                            <asp:Label ID="lblRedirectDocument" runat="server" data-toggle="tooltip"  data-placement="bottom" Text='<%# Eval("DocName") %>' ToolTip='<%# Eval("DocName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                            <asp:Label ID="lblIsLinkTrue" Visible="false" runat="server" data-placement="bottom" Text='<%# Eval("ISLink") %>' ToolTip='<%# Eval("ISLink") %>'></asp:Label>
                                                                            <asp:Label ID="lblDocType" runat="server" data-placement="bottom" Text='<%# Eval("DocType") %>' ToolTip='<%# Eval("DocType") %>'></asp:Label>
                                                                            <asp:Label ID="lblScheduleOnID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduleOnID") %>'></asp:Label>
                                                                            <asp:Label ID="lblComplianceInstanceID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="lnkDownloadDocument" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerSettings Visible="false" />
                                                            <PagerTemplate>
                                                            </PagerTemplate>
                                                            <EmptyDataTemplate>
                                                                No Record Found
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>

                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <%--      <div style="margin-bottom: 7px" runat="server" id="divWorkingfiles">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                     <td style="width: 25%;">
                                                          <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                         <label style="font-weight: bold; vertical-align:text-top;">Upload Working Files(s)</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:FileUpload ID="FileUpload1" Multiple="Multiple" runat="server" Style="color: black" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>--%>
                                        <%--     <div style="margin-bottom: 7px">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%;">
                                                          <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                         <label style= "font-weight: bold; vertical-align:text-top;">Date</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:TextBox runat="server" ID="tbxDate" placeholder="DD-MM-YYYY" class="form-control" Style="width: 115px;" />
                                                        <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                                                            runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>--%>
                                                 <div style="margin-bottom: 7px">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%;">
                                                          <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                         <label style="font-weight: bold; vertical-align:text-top;">Remarks</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" class="form-control" Rows="2" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px;">
                                            <asp:Button Text="Submit" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-search"
                                                ValidationGroup="ComplianceValidationGroup1" />
                                            <%--OnClientClick="if (!ValidateFile()) return false;"--%>
                                            <%-- <asp:Button Text="submit" runat="server" ID="btnSaveDOCNotCompulsory" OnClick="btnSave_Click" CssClass="btn btn-search"
                                                ValidationGroup="ComplianceValidationGroup1" />--%>
                                            <asp:Button Text="Close" ID="btnCancel" OnClick="btnCancel_Click" Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div runat="server" id="divDeleteDocument" style="text-align: left;">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                        <table width="100%">
                            <tr>
                                <td style="width: 50%">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th>Compliance Related Documents</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                            </asp:LinkButton></td>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                ID="lbtLinkDocbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblWorkingFiles">
                                                        <thead>
                                                            <th>Compliance Working Files</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                            </asp:LinkButton></td>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                ID="lbtLinkbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>

                    </fieldset>
                </div>

                <div id="AuditLog" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog">
                                            <h2>Audit Log</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseAuditLog" class="collapse">
                                    <div runat="server" id="log" style="text-align: left;">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                    AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                    OnRowCreated="grdTransactionHistory_RowCreated" BorderWidth="0px" OnSorting="grdTransactionHistory_Sorting"
                                                    DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                    <Columns>
                                                        <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                        <asp:TemplateField HeaderText="Date">
                                                            <ItemTemplate>
                                                                <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                        <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                        <div></div>
                                        <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                        <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="lbDownloadSample" />
             <asp:PostBackTrigger ControlID="UploadDocument"/> 
        </Triggers>
    </asp:UpdatePanel>
</div>



<div>
    <div class="modal fade" id="modalDocumentChecklistViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div style="float: left; width: 10%">
                            <table width="100%" style="text-align: left; margin-left: 5%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptChecklistVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Versions</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblChecklistDocumentVersionView"
                                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="lblChecklistDocumentVersionView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="rptChecklistVersionView" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="float: left; width: 90%">
                            <asp:Label runat="server" ID="lblMessageChecklist" Style="color: red;"></asp:Label>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docChecklistViewerPerformerAll" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="modal fade" id="SampleFilePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <%-- data-dismiss-modal="modal2"--%>
                    <button type="button" class="close" onclick="$('#SampleFilePopUp').modal('hide');" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <%-- <div style="width: 100%;">--%>
                    <div style="float: left; width: 10%">
                        <table width="100%" style="text-align: left; margin-left: 5%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdatleMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptComplianceSampleView" runat="server" OnItemCommand="rptComplianceSampleView_ItemCommand"
                                                    OnItemDataBound="rptComplianceSampleView_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Sample Forms</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView"
                                                                            runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                            <%--<Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                            </Triggers>--%>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div style="float: left; width: 90%">
                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                            <iframe src="about:blank" id="docViewerRamAll" runat="server" width="100%" height="550px"></iframe>
                        </fieldset>
                    </div>
                    <%-- </div>--%>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="DocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="$('#DocumentPriview').modal('hide');" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
                <div style="width: 100%;">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                        <iframe src="about:blank" id="docPriview" runat="server" width="100%" height="535px"></iframe>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="divViews" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" style="width:100%;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" onclick="ClosesViews();" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <iframe id="FrameViews" src="about:blank" width="100%;" height="100%" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="divDownloads" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" style="width: 650px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <iframe id="FrameDownloads" src="about:blank" width="535px" height="350px" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    function fCalltreeCollapsed() {
        $(document).ready(function () {
            $('#basic').simpleTreeTable({
                collapsed: true
            });
        });
    }


     function fopendocfile() {
        $('#SampleFilePopUp').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopnChkList_docViewerRamAll').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample.ClientID %>").text());
    }

    function fram(file) {
        debugger;
        $('#ContentPlaceHolder1_udcStatusTranscatopnChkList_docViewerRamAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }
    function fopendocfileChkPfAct(file) {

        $('#divActChkPfFilePopUp').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopnChkList_IframeChkPfActFile').attr('src', "../docviewer.aspx?docurl=" + file);
    }
    function fopendocfileChkPfActPopUp() {
        $('#divActChkPfFilePopUp').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscatopnChkList_IframeChkPfActFile').attr('src', "../docviewer.aspx?docurl=");
    }
    function PDOVPopPup(ActID, ID) {
        $('#divViews').modal('show');
        $('#FrameViews').attr('width', '100%');
        $('#FrameViews').attr('height', '600px');
        $('.modal-dialog').css('width', '85%');
        $('#FrameViews').removeAttr('src');
        $('#FrameViews').attr('src', "../Common/ActOverview.aspx?ActID=" + ActID + "&ID=" + ID);
    }
    function ClosesViews() {
        $('#divViews').modal('hide');
    }
    function PDDOVPopPup(ActID, ID) {
        //$('#divDownloads').modal('show');
        //$('#FrameDownloads').attr('width', '100%');
        //$('#FrameDownloads').attr('height', '600px');
        //$('.modal-dialog').css('width', '100%');
        $('#FrameDownloads').attr('src', "../Common/DownloadActOverview.aspx?ActID=" + ActID + "&ID=" + ID);
    }
</script>




