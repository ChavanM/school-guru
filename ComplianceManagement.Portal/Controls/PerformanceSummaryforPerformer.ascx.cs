﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Data;
using System.Globalization;
using System.IO;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Ionic.Zip;
using System.Collections;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class Performance_SummaryforPerformer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string year = DateTime.Today.Year.ToString();
                if (DateTime.Today.Month < Util.FinancialMonth())
                {
                    year = (DateTime.Today.Year - 1).ToString();
                }
                txtStartDate.Text = "01-0" + Util.FinancialMonth().ToString() + "-" + year;
                txtEndDate.Text = DateTime.Now.Date.ToString("dd-MM-yyyy");
                BindFilters();
                dlFilters.SelectedIndex = 0;
                setGraphTitle();
                BindLocation();
                BindComplianceSummary("01-0" + Util.FinancialMonth().ToString() + "-" + year, DateTime.Now.Date.ToString("dd-MM-yyyy"));
                
            }

        }

        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<PerformanceSummaryForPerformer>().Where(entry => entry.Name != "Reportee");
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                lblNote.Visible = filter != PerformanceSummaryForPerformer.Risk ? true : false;
                setGraphTitle();
                BindLocation();
                BindComplianceSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
                grdComplianceTransactions.SelectedIndex = 0;
                grdLocationCatagory.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public DataTable GetGrid()
        {
            dlFilters_SelectedIndexChanged(null, null);
            return (grdComplianceTransactions.DataSource as List<ComplianceInstanceTransactionView>).ToDataTable();
        }

        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CannedReportFilterForPerformer>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                    GridView ProductGrid = (GridView)sender;
                    GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    HeaderRow.Style.Add("border-color", "White");


                    //Adding Risk Column
                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = filter == PerformanceSummaryForPerformer.Location ? "Risk" : filter.ToString();
                    HeaderCell.RowSpan = 2;
                    //HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.BorderColor = Color.White;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    //Adding Head Office Column
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Completed";
                    //HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.BorderColor = Color.White;
                    HeaderCell.ColumnSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    //Adding Year Column
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Pending";
                    //HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.BorderColor = Color.White;
                    HeaderCell.RowSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    grdComplianceTransactions.Controls[0].Controls.AddAt(0, HeaderRow);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[3].Visible = false;

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                    if (filter == PerformanceSummaryForPerformer.Category)
                    {
                        e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdComplianceTransactions, "Select$" + e.Row.RowIndex);
                        e.Row.ToolTip = "Click here to generate the graph";
                        e.Row.Attributes.Add("style", "cursor:pointer");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnGetSummary_Click(object sender, EventArgs e)
        {
            try
            {

                txtStartDate.Text = Request[txtStartDate.UniqueID].ToString();
                txtEndDate.Text = Request[txtEndDate.UniqueID].ToString();
                setGraphTitle();
                BindComplianceSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void BindComplianceSummary(string fromdate, string todate)
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                int branchId = Convert.ToInt32(ddlLocations.SelectedValue);
                divLocation.Visible = false;
                divfilterforLocation.Visible = false;
                grdComplianceTransactions.DataSource = null;
                grdComplianceTransactions.DataSource = DashboardManagement.PerformanceSummaryforPerformer(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), branchId);
                grdComplianceTransactions.DataBind();

                int catagoryID = ComplianceCategoryManagement.GetIdByName(grdComplianceTransactions.DataKeys[0].Value.ToString());

                bool displayPendingGraph;
                bool displayDelayedGraph;
                chrtPendingCompliances.DataSource = DashboardManagement.PerformanceSummaryforPerformerPending(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, catagoryID, branchId);
                chrtPendingCompliances.DataBind();

                chrtDelayedCompliances.DataSource = DashboardManagement.PerformanceSummaryforPerformerDelayed(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, catagoryID, branchId);
                chrtDelayedCompliances.DataBind();

                chrtPendingCompliances.Visible = displayPendingGraph;
                chrtDelayedCompliances.Visible = displayDelayedGraph;
                if (displayPendingGraph == false)
                    titlePendingCompliances.Text = "No data to display";
                if (displayDelayedGraph == false)
                    titleDelayedCompliances.Text = "No data to display";

                if (filter == PerformanceSummaryForPerformer.Location)
                {
                    divLocation.Visible = true;
                    var LocationTreeCount = DashboardManagement.GetBranchCountforPerformer(AuthenticationHelper.UserID, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                    divfilterforLocation.Visible = LocationTreeCount > 0 ? true : false;

                    grdLocationCatagory.DataSource = DashboardManagement.PerformanceSummaryforPerformer(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), branchId, PerformanceSummaryForPerformer.Category);
                    grdLocationCatagory.DataBind();

                    int LocationCatagoryID = ComplianceCategoryManagement.GetIdByName(grdLocationCatagory.SelectedDataKey.Value.ToString());
                    chrtLocationCatagoryPending.DataSource = DashboardManagement.PerformanceSummaryforPerformerPending(customerID, AuthenticationHelper.UserID, filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, LocationCatagoryID, branchId, PerformanceSummaryForPerformer.Category);
                    chrtLocationCatagoryPending.DataBind();

                    chrtLocationCatagoryDelay.DataSource = DashboardManagement.PerformanceSummaryforPerformerDelayed(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, LocationCatagoryID, branchId, PerformanceSummaryForPerformer.Category);
                    chrtLocationCatagoryDelay.DataBind();

                    chrtLocationCatagoryPending.Visible = displayPendingGraph;
                    chrtLocationCatagoryDelay.Visible = displayDelayedGraph;
                    if (displayPendingGraph == false)
                        titlePendingCompliances1.Text = "No data to display";
                    else
                        titlePendingCompliances1.Text = "Risk Wise Pending Compliances";

                    if (displayDelayedGraph == false)
                        titleDelayedCompliances1.Text = "No data to display";
                    else
                        titleDelayedCompliances1.Text = "Risk Wise Delayed Compliances";

                }

                lblNonComplincePenalty.Text = DashboardManagement.GetSumofPenaltyForPerformer(customerID,AuthenticationHelper.UserID, DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdPanelSummary_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }

                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Script", "initializeCombobox();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void setGraphTitle()
        {
            PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
            if (filter == PerformanceSummaryForPerformer.Risk || filter == PerformanceSummaryForPerformer.Location)
            {
                titlePendingCompliances.Text = "Category Wise Pending Compliances";
                titleDelayedCompliances.Text = "Category Wise Delayed Compliances";
            }
            else
            {
               
               
                titlePendingCompliances.Text =  "Risk Wise Pending Compliances";
                titleDelayedCompliances.Text =  "Risk Wise Delayed Compliances";
            }
        }

        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                int catagoryID = ComplianceCategoryManagement.GetIdByName(grdComplianceTransactions.SelectedDataKey.Value.ToString());
                bool displayPendingGraph;
                bool displayDelayedGraph;
                setGraphTitle();
                PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                chrtPendingCompliances.DataSource = DashboardManagement.PerformanceSummaryforPerformerPending(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, catagoryID);
                chrtPendingCompliances.DataBind();
                chrtDelayedCompliances.DataSource = DashboardManagement.PerformanceSummaryforPerformerDelayed(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, catagoryID);
                chrtDelayedCompliances.DataBind();

                chrtPendingCompliances.Visible = displayPendingGraph;
                chrtDelayedCompliances.Visible = displayDelayedGraph;

                if (displayPendingGraph == false)
                    titlePendingCompliances.Text = "No data to display";
                if (displayDelayedGraph == false)
                    titleDelayedCompliances.Text = "No data to display";

                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        

        private void BindLocation()
        {
            try
            {
                ddlLocations.DataTextField = "Name";
                ddlLocations.DataValueField = "ID";

                var mappedLocation = AssignEntityManagement.GetAssignedLocationUserAndRoleWise(3, AuthenticationHelper.UserID);
                List<object> dataSource = new List<object>();
                foreach (var loc in mappedLocation)
                {
                    dataSource.Add(new 
                    {
                        ID = loc.ID,
                        Name = loc.Name +" ("+ CustomerBranchManagement.GetlegalEntity(loc.ID).Name +")"
                    });
                }

                ddlLocations.DataSource = dataSource;
                ddlLocations.DataBind();

                ddlLocations.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                var Assignedbracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWise(3, AuthenticationHelper.UserID);
                foreach (var item in nvp.Children)
                {
                    if (Assignedbracnhes.Where(entry => entry.ID == item.ID).FirstOrDefault() != null)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item);
                        parent.ChildNodes.Add(node);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< All >";
        //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
        //        setGraphTitle();
        //        BindComplianceSummary(Request[txtStartDate.UniqueID].ToString(), Request[txtEndDate.UniqueID].ToString());
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void grdLocationCatagory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[3].Visible = false;

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                    if (filter == PerformanceSummaryForPerformer.Location)
                    {
                        e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdLocationCatagory, "Select$" + e.Row.RowIndex);
                        e.Row.ToolTip = "Click here to generate the graph";
                        e.Row.Attributes.Add("style", "cursor:pointer");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdLocationCatagory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    HeaderRow.Style.Add("border-color", "White");

                    //Adding Risk Column
                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = "Category";
                    HeaderCell.RowSpan = 2;
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    //Adding Head Office Column
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Completed";
                    HeaderCell.ColumnSpan = 2;
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    //Adding Year Column
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Pending";
                    HeaderCell.RowSpan = 2;
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderRow.Cells.Add(HeaderCell);

                    grdLocationCatagory.Controls[0].Controls.AddAt(0, HeaderRow);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void grdLocationCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                int branchId = Convert.ToInt32(ddlLocations.SelectedValue);
                int catagoryID = ComplianceCategoryManagement.GetIdByName(grdLocationCatagory.SelectedDataKey.Value.ToString());
                bool displayPendingGraph;
                bool displayDelayedGraph;
                setGraphTitle();
                PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                chrtPendingCompliances.DataSource = DashboardManagement.PerformanceSummaryforPerformerPending(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, catagoryID, branchId);
                chrtPendingCompliances.DataBind();
                chrtDelayedCompliances.DataSource = DashboardManagement.PerformanceSummaryforPerformerDelayed(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, catagoryID, branchId);
                chrtDelayedCompliances.DataBind();

                chrtPendingCompliances.Visible = displayPendingGraph;
                chrtDelayedCompliances.Visible = displayDelayedGraph;

                if (displayPendingGraph == false)
                    titlePendingCompliances.Text = "No data to display";
                if (displayDelayedGraph == false)
                    titleDelayedCompliances.Text = "No data to display";

                if (filter == PerformanceSummaryForPerformer.Location)
                {
                    divLocation.Visible = true;
                    divfilterforLocation.Visible = true;
                  
                    grdLocationCatagory.DataSource = DashboardManagement.PerformanceSummaryforPerformer(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), branchId, PerformanceSummaryForPerformer.Category);
                    grdLocationCatagory.DataBind();

                    chrtLocationCatagoryPending.DataSource = DashboardManagement.PerformanceSummaryforPerformerPending(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayPendingGraph, catagoryID, branchId, PerformanceSummaryForPerformer.Category);
                    chrtLocationCatagoryPending.DataBind();

                    chrtLocationCatagoryDelay.DataSource = DashboardManagement.PerformanceSummaryforPerformerDelayed(customerID,AuthenticationHelper.UserID, filter, DateTime.ParseExact(Request[txtStartDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(Request[txtEndDate.UniqueID].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), out displayDelayedGraph, catagoryID, branchId, PerformanceSummaryForPerformer.Category);
                    chrtLocationCatagoryDelay.DataBind();

                    chrtLocationCatagoryPending.Visible = displayPendingGraph;
                    chrtLocationCatagoryDelay.Visible = displayDelayedGraph;

                    if (displayPendingGraph == false)
                        titlePendingCompliances1.Text = "No data to display";
                    else
                        titlePendingCompliances1.Text = "Risk Wise Pending Compliances";

                    if (displayDelayedGraph == false)
                        titleDelayedCompliances1.Text = "No data to display";
                    else
                        titleDelayedCompliances1.Text = "Risk Wise Delayed Compliances";

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            try
            {
                string content = string.Empty;

                using (var stringWriter = new StringWriter())
                using (var htmlWriter = new HtmlTextWriter(stringWriter))
                {
                    // render the current page content to our temp writer
                    base.Render(htmlWriter);
                    htmlWriter.Close();

                    // get the content
                    content = stringWriter.ToString();
                }

                // replace our placeholders
                string newContent = content.Replace("/Common/ComplianceDashboardDisplay/Summary/ChartImg.axd", "~/Common/ComplianceDashboardDisplay/Summary/ChartImg.axd");

                // write the new html to the page
                writer.Write(newContent);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void grdLocationCatagory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    BindDetailView(Convert.ToString(e.CommandArgument),"Category");
                    Session["Arguments"] = Convert.ToString(e.CommandArgument);
                    Session["SubFilter"] = "Category";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        private void BindDetailView(string aregument,string subfilter)
        {
            List<int> statusIds = new List<int>();
            string[] arg = new string[3];
            arg = aregument.ToString().Split(';');
            if (arg[1].Trim().Equals("Delayed"))
            {
                statusIds.Add(5);
                statusIds.Add(9);
            }
            else if (arg[1].Trim().Equals("InTime"))
            {
                statusIds.Add(4);
                statusIds.Add(7);
            }
            else
            {
                statusIds.Add(1);
                statusIds.Add(10);
                statusIds.Add(6);
                statusIds.Add(8);
            }

            PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);

            int branchId = Convert.ToInt32(ddlLocations.SelectedValue);

            int customerID = -1;
            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
            var DetailView = DashboardManagement.GetSummaryDetails(customerID,AuthenticationHelper.UserID, DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                3, statusIds, branchId, filter.ToString(), subfilter, Convert.ToInt32(arg[0]));

            string lable = string.Empty;
            lable =  arg[2].ToString()+" = "+ arg[1] + " " + "compliances";
          
            lblDetailViewTitle.Text = lable;
            if (arg[1].Trim().Equals("Pending"))
            {
                grdSummaryDetails.Columns[7].Visible = false;
            }
            else
            {
                grdSummaryDetails.Columns[7].Visible = true;
            }
            grdSummaryDetails.DataSource = DetailView;
            grdSummaryDetails.DataBind();
           
            UpDetailView.Update();
            Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<ComplianceDashboardSummaryView>).ToDataTable();
            OnSelectedIndexChanged(null, null);
            grdLocationCatagory_SelectedIndexChanged(null, null);
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divSummaryDetails\").dialog('open')", true);
                   
        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {

                    BindDetailView(Convert.ToString(e.CommandArgument), "Risk");
                    Session["Arguments"] = Convert.ToString(e.CommandArgument);
                    Session["SubFilter"] = "Risk";
                    //List<int> statusIds = new List<int>();
                    //string[] arg = new string[3];
                    //arg = e.CommandArgument.ToString().Split(';');
                    //if (arg[1].Trim().Equals("Delayed"))
                    //    statusIds.Add(5);
                    //else if (arg[1].Trim().Equals("InTime"))
                    //{
                    //    statusIds.Add(4);
                    //}
                    //else
                    //{
                    //    statusIds.Add(1);
                    //    statusIds.Add(10);
                    //}

                    //PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);

                    //int branchId = tvBranches.SelectedNode != null ? Convert.ToInt32(tvBranches.SelectedNode.Value) : -1;
                   
                    //var DetailView = DashboardManagement.GetSummaryDetails(AuthenticationHelper.UserID, DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                    // 3, statusIds, branchId, filter.ToString(), "Risk", Convert.ToInt32(arg[0]));

                    //string lable = string.Empty;
                    //if (filter.ToString().Equals("Location"))
                    //{
                    //    lable = arg[2].ToString() + " Risk " + arg[1] + " " + "compliances";
                    //}
                    //else
                    //{
                    //    lable = arg[2].ToString() + " " + filter.ToString() + " " + arg[1] + " " + "compliances";
                    //}
                    //lblDetailViewTitle.Text = lable;
                    //grdSummaryDetails.DataSource = DetailView;
                    //grdSummaryDetails.DataBind();
                    //UpDetailView.Update();
                    //if (arg[1].Trim().Equals("InTime"))
                    //{
                    //    grdSummaryDetails.Columns[5].Visible = false;
                    //}

                    //Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<ComplianceDashboardSummaryView>).ToDataTable();
                    //OnSelectedIndexChanged(null, null);
                    //grdLocationCatagory_SelectedIndexChanged(null, null);
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divSummaryDetails\").dialog('open')", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                       
                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                        ExcelData = view.ToTable("Selected", false, "ShortDescription", "Branch", "ScheduledOn", "ForMonth", "Status", "RiskCategory");
                        exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["B2"].Value = lblDetailViewTitle.Text + " Report";
                        exWorkSheet.Cells["B2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 15;
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A4"].Value = "Short Description";
                        exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Value = "Start Date";
                        exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D4"].Value = "For Month";
                        exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D4"].Value = "Risk Category";

                        using (ExcelRange col = exWorkSheet.Cells[2, 1, 6 + ExcelData.Rows.Count, 8])
                        {
                            col.Style.Numberformat.Format = "dd/MM/yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.AutoFitColumns();
                        }

                        PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename="+filter.ToString()+"Report.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        Response.End();
                    }
                    catch (Exception)
                    {
                    }
                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdSummaryDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        int ScheduledOnID = Convert.ToInt32(e.CommandArgument);
                        if (ScheduledOnID != 0)
                        {
                            var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                            List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                            //craeted subdirectory
                            //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                            string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                            int i = 0;
                            foreach (var file in fileData)
                            {
                                string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                if (dictionary == null)
                                    ComplianceZip.AddDirectoryByName(directoryName + "/" + version);

                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + i + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }
                        PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;
                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=" + filter.ToString() + " Document.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();
                    }

                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSummaryDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("lblDownLoadfile");
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterPostBackControl(lblDownLoadfile);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSummaryDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CmpSaveCheckedValues();
                grdSummaryDetails.PageIndex = e.NewPageIndex;
                BindDetailView(Convert.ToString(Session["Arguments"]), Convert.ToString(Session["SubFilter"]));
                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void CmpPopulateCheckedValues()
        {
            ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliances"];

            if (complianceDetails != null && complianceDetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdSummaryDetails.Rows)
                {
                    LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                    int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                    if (complianceDetails.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        private void CmpSaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            int index = -1;
            foreach (GridViewRow gvrow in grdSummaryDetails.Rows)
            {
                LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;

                // Check in the Session
                if (ViewState["checkedCompliances"] != null)
                    userdetails = (ArrayList)ViewState["checkedCompliances"];
                if (result)
                {
                    if (!userdetails.Contains(index))
                        userdetails.Add(index);
                }
                else
                    userdetails.Remove(index);
            }
            if (userdetails != null && userdetails.Count > 0)
                ViewState["checkedCompliances"] = userdetails;
        }

        // Zip all files from folder
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    CmpSaveCheckedValues();

                    if (ViewState["checkedCompliances"] != null)
                    {
                        foreach (var gvrow in (ArrayList)ViewState["checkedCompliances"])
                        {

                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                            {
                                var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                                List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                                //craeted subdirectory
                                //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                int i = 0;
                                foreach (var file in fileData)
                                {
                                    string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                    var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                    if (dictionary == null)
                                        ComplianceZip.AddDirectoryByName(directoryName + "/" + version);

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = file.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;


                        //byte[] data = zipMs.ToArray();
                        //Response.BinaryWrite(data);
                        //Response.BinaryWrite(@"C:\Users\devlp1\AppData\Local\Temp\document.txt", File.WriteAllBytes(data));

                        byte[] data = zipMs.ToArray();
                        //File.WriteAllBytes(@"C:\Users\devlp1\AppData\Local\Temp\Compliance\document.zip", data);
                        //Response.BinaryWrite(data);
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=InternalComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();

                    }
                }
                //BindComplianceDocument(DateTime.Now,DateTime.Now);
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected bool ViewDownload(int statusid)
        {
            if (statusid != 1)
                return true;
            else
                return false;
        }


    }
}