﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class DashboardUpcomingInternalCompliances : System.Web.UI.UserControl
    {
        public string Role;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Role"] = Role;
                BindComplianceTransactions();
                udcStatusTranscation.OnSaved += (inputForm, args) => { BindComplianceTransactions(); };
            }
           
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<InternalComplianceInstanceTransactionView> assignmentList = null;
                if (ViewState["Role"].Equals("Performer"))
                {
                    assignmentList = InternalDashboardManagement.DashboardDataForPerformer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                }
                else if (ViewState["Role"].Equals("Reviewer"))
                {
                    assignmentList = InternalDashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                }
                else if (ViewState["Role"].Equals("Approver"))
                {
                    assignmentList = InternalDashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                }

                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;

                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = assignmentList;
                grdComplianceTransactions.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                   
                    InternalComplianceInstanceTransactionView rowView = (InternalComplianceInstanceTransactionView)e.Row.DataItem;
                    string icScheduleon = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "InternalScheduledOn"));
                    long nInternalComplianceID = rowView.InternalComplianceID;
                    Button btn_Edit = (Button)e.Row.FindControl("btn_Edit");                
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var IFrequency = (from row in entities.InternalCompliances
                                             where row.ID == nInternalComplianceID
                                             select row.IFrequency).FirstOrDefault();



                        string ic = "1/1/0001 12:00:00 AM";
                        if (Convert.ToDateTime(icScheduleon) == Convert.ToDateTime(ic))
                        {
                            btn_Edit.Visible = false;

                        }
                        else if ((IFrequency == 1) || (IFrequency == 2) || (IFrequency == 3) || (IFrequency == null))
                        {
                            btn_Edit.Visible = true;
                        }
                        else
                        {
                            btn_Edit.Visible = false;
                        }
                   
                    }
                                                                   

                    if (rowView.InternalComplianceInstanceID != -1)
                    {

                        Label lblRisk = (Label)e.Row.FindControl("lblRisk");
                        Image imtemplat = (Image)e.Row.FindControl("imtemplat");
                      
                        if (Convert.ToInt32(lblRisk.Text) == 0)
                        {
                            imtemplat.ImageUrl = "~/Images/red.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 1)
                        {
                            imtemplat.ImageUrl = "~/Images/yellow.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 2)
                        {
                            imtemplat.ImageUrl = "~/Images/green.png";
                        }

                        LinkButton btnChangeStatus = (LinkButton)e.Row.FindControl("btnChangeStatus");
                        //if (btnChangeStatus.Visible == true)
                        //{
                           
                        //    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                        //    e.Row.ToolTip = "Click on row to change Compliance Status";
                        //}
                    }
                    else
                    {
                        e.Row.Cells[0].Style.Add("border-style", "none");
                        e.Row.Cells[0].Style.Add("font-weight", "700");

                        e.Row.Cells[1].Style.Add("border-style", "none");
                        e.Row.Cells[2].Style.Add("border-style", "none");
                        e.Row.Cells[2].Text = string.Empty;
                        e.Row.Cells[3].Style.Add("border-style", "none");
                        
                     
                        e.Row.Cells[4].Style.Add("border-style", "none");
                        //e.Row.Cells[5].Style.Add("border-style", "none");
                        e.Row.Style.Add("background-color", "#9FCBEA");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
              
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 1)
                    {
                        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                        udcStatusTranscation.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function bind the upcoming compliances to the grid view.
        /// </summary>
        protected void BindComplianceTransactions()
        {
            try
            {
                if (ViewState["Role"] != null)
                {
                    if (ViewState["Role"].Equals("Performer"))
                    {
                        grdComplianceTransactions.DataSource = InternalDashboardManagement.DashboardDataForPerformer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                    }
                    else if (ViewState["Role"].Equals("Reviewer"))
                    {

                        grdComplianceTransactions.DataSource = InternalDashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                    }
                    else if (ViewState["Role"].Equals("Approver"))
                    {
                        grdComplianceTransactions.DataSource = InternalDashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                    }
                    grdComplianceTransactions.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function return true false value for status image.
        /// </summary>
        /// <param name="userID">long</param>
        /// <param name="roleID">int</param>
        /// <param name="statusID">int</param>
        /// <returns></returns>
        public bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        /// <summary>
        /// this function is set sorting images.
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="headerRow"></param>
        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// this function is set the sorting direction.
        /// </summary>
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        //added By Manisha on 28-Dec-2015
        protected void grdComplianceTransactions_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            grdComplianceTransactions.EditIndex = e.NewEditIndex;
            DateTime date = DateTime.Now;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
           
            BindComplianceTransactions();

        }

        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {
            try
            {
                //DateTime date = DateTime.Now;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
           
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public DateTime GetNextDueDate(DateTime date, string IFrequency, string ISubComplianceType, string IDueDate)
        {
            DateTime newdate = new DateTime();
            if (ISubComplianceType == "0" && IFrequency == null)
            {
                newdate = date.AddDays(Convert.ToInt32(IDueDate));
            }
            else if (ISubComplianceType == "1" && IFrequency == "2")
            {
                newdate = date.AddMonths(6);
            }
            else if (IFrequency == "1")
            {
                newdate = date.AddMonths(3);
            }
            else if (IFrequency == "2")
            {
                newdate = date.AddMonths(6);
            }
            else if (IFrequency == "3")
            {
                newdate = date.AddMonths(12);
            }
            return newdate;
        }
        public DateTime GetPreviousDueDate(DateTime date, string IFrequency, string ISubComplianceType, string IDueDate)
        {
            DateTime newdate = new DateTime();
            if (ISubComplianceType == "0" && IFrequency == null)
            {
                newdate = date.AddDays(-Convert.ToInt32(IDueDate));
            }
            else if (ISubComplianceType == "1" && IFrequency == "2")
            {
                newdate = date.AddMonths(-6);
            }
            else if (IFrequency == "1")
            {
                newdate = date.AddMonths(-3);
            }
            else if (IFrequency == "2")
            {
                newdate = date.AddMonths(-6);
            }
            else if (IFrequency == "3")
            {
                newdate = date.AddMonths(-12);
            }
            return newdate;
            
        }
        //public DateTime GetPreviousDueDate(DateTime date, string IFrequency, string ISubComplianceType, string IDueDate)
        //{

        //    DateTime newdate = new DateTime();
        //    if (IFrequency == "1")
        //    {
        //        if (ISubComplianceType == "0")
        //        {
        //            newdate = date.AddDays(-Convert.ToInt32(IDueDate));
        //        }
        //        else
        //        {
        //            newdate = date.AddMonths(-3);
        //        }
        //    }
        //    else if (IFrequency == "2")
        //    {
        //        newdate = date.AddMonths(-6);
        //    }
        //    return newdate;
        //}
        protected void grdComplianceTransactions_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            try
            {
                string textboxdate = Request[((TextBox)grdComplianceTransactions.Rows[e.RowIndex].FindControl("txt_DueDate")).UniqueID].ToString();
                Session["Duedate"] = textboxdate;

                //DateTime Objtextboxdate = GetDate(Convert.ToDateTime(textboxdate).ToString("dd-MM-yyyy"));

                DateTime Objtextboxdate;
                DateTime parsedDate;
                bool s = DateTime.TryParseExact(textboxdate, "dd-MM-yyyy", null, DateTimeStyles.None, out parsedDate);
                if (s == false)
                {
                    Objtextboxdate = Convert.ToDateTime(textboxdate);
                }
                else
                {
                    Objtextboxdate = GetDate(textboxdate.ToString());
                }
                if (!string.IsNullOrEmpty(textboxdate))
                {
                    long vInternalComplianceID = Convert.ToInt64((grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblInternalComplianceID") as Label).Text);
                    long vInternalScheduledOnID = Convert.ToInt64((grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblInternalScheduledOnID") as Label).Text);
                    long vInternalComplianceInstanceID = Convert.ToInt64((grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblInternalComplianceInstanceID") as Label).Text);
                    int UserId = Convert.ToInt32((grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblUserID") as Label).Text);
                    int RoleID = Convert.ToInt32((grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblRoleID") as Label).Text);
                    DateTime CurrentDueDate = Convert.ToDateTime((grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblInternalScheduledOn") as Label).Text);
                    string strShortDescription = (grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblShortDescription") as Label).Text;
                    string strBranch = (grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblBranch") as Label).Text;
                    Label lblErrorMsgI = grdComplianceTransactions.Rows[e.RowIndex].FindControl("lblErrorMsgI") as Label;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        var Icompliances = (from row in entities.InternalCompliances
                                            where row.ID == vInternalComplianceID
                                            select row).FirstOrDefault();
                        if (Icompliances.IComplianceOccurrence == 0 && Icompliances.IOneTimeDate != null)
                        {
                            int IComplianceAssignmentID = (from row in entities.InternalComplianceAssignments
                                                           where row.RoleID == RoleID && row.UserID == UserId && row.InternalComplianceInstanceID == vInternalComplianceInstanceID
                                                           select row.ID).FirstOrDefault();

                            InternalComplianceScheduledOn scheduleOnToUpdate = (from row in entities.InternalComplianceScheduledOns
                                                                                where row.ID == vInternalScheduledOnID
                                                                                select row).FirstOrDefault();

                            scheduleOnToUpdate.ScheduledOn = GetDate(Objtextboxdate.ToString("dd-MM-yyyy"));
                            entities.SaveChanges();

                            var PrevReminderDates = (from row in entities.InternalComplianceReminders
                                                     where row.ComplianceDueDate == CurrentDueDate && row.ComplianceAssignmentID == IComplianceAssignmentID
                                                     select row).ToList();

                            foreach (InternalComplianceReminder IC in PrevReminderDates)
                            {
                                entities.InternalComplianceReminders.Remove(IC);
                            }
                            entities.SaveChanges();
                            InternalComplianceManagement.CreateReminders(vInternalComplianceID, vInternalComplianceInstanceID, IComplianceAssignmentID, Objtextboxdate);
                            grdComplianceTransactions.EditIndex = -1;
                            BindComplianceTransactions();



                            H_Internal_ChangeDueDate historyDueDate = new H_Internal_ChangeDueDate()
                            {
                                InternalComplianceId = vInternalComplianceID,
                                InternalComplianceInstanceId = vInternalComplianceInstanceID,
                                InternalAssignmentId = IComplianceAssignmentID,
                                ScheduledonID = vInternalScheduledOnID,
                                PreviousScheduledonDate = CurrentDueDate,
                                NewScheduledonDate = Objtextboxdate,
                                CreatedOn = DateTime.UtcNow,
                                Createdby = Convert.ToInt32(Session["userID"]),
                            };
                            InternalComplianceManagement.CreateHistoryDueDate(historyDueDate);

                            //-----------------------------------------------------------------------------------------------------------------
                            int userID = -1;
                            userID = Convert.ToInt32(Session["userID"]);

                            long ReviewerId = -1;

                            ReviewerId = (from row in entities.InternalComplianceAssignments
                                          where row.RoleID == 4 && row.InternalComplianceInstanceID == vInternalComplianceInstanceID
                                          select row.UserID).FirstOrDefault();

                            int customerID = -1;
                            customerID = UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0;
                            string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;

                            User user = UserManagement.GetByID(UserId);
                            string accessURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                accessURL = Urloutput.URL;
                            }
                            else
                            {
                                accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            var AlertChangedDueDateEmail = Properties.Settings.Default.EMailTemplate_AlertChangedDueDate
                                                          .Replace("@User", user.FirstName + " " + user.LastName)
                                                          .Replace("@URL", Convert.ToString(accessURL))
                                                          .Replace("@From", ReplyEmailAddressName);

                            //var AlertChangedDueDateEmail = Properties.Settings.Default.EMailTemplate_AlertChangedDueDate
                            //                               .Replace("@User", user.FirstName + " " + user.LastName)
                            //                               .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                            //                               .Replace("@From", ReplyEmailAddressName);

                            StringBuilder details = new StringBuilder();


                            details.AppendLine(Properties.Settings.Default.EMailTemplate_AlertChangedDueDateRows

                                        .Replace("@Branch", strBranch)
                                        .Replace("@Compliance", strShortDescription)
                                        .Replace("@PreviousDueDate", CurrentDueDate.ToString("dd-MMM-yyyy"))
                                        .Replace("@NewDueDate", Objtextboxdate.ToString("dd-MMM-yyyy")));



                            string message = AlertChangedDueDateEmail.Replace("@User", "User").Replace("@Details", details.ToString());
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Alert :: Due Date of Internal Compliance Changed", message);

                            if ((ReviewerId != -1) && (ReviewerId != 0))
                            {
                                User userReviewer = UserManagement.GetByID((int)ReviewerId);

                                var AlertChangedDueDateEmailReviewer = Properties.Settings.Default.EMailTemplate_AlertChangedDueDate
                                                      .Replace("@User", userReviewer.FirstName + " " + userReviewer.LastName)
                                                      .Replace("@URL", Convert.ToString(accessURL))
                                                      .Replace("@From", ReplyEmailAddressName);

                                //var AlertChangedDueDateEmailReviewer = Properties.Settings.Default.EMailTemplate_AlertChangedDueDate
                                //                          .Replace("@User", userReviewer.FirstName + " " + userReviewer.LastName)
                                //                          .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                //                          .Replace("@From", ReplyEmailAddressName);

                                StringBuilder detailsReviewer = new StringBuilder();


                                detailsReviewer.AppendLine(Properties.Settings.Default.EMailTemplate_AlertChangedDueDateRows

                                            .Replace("@Branch", strBranch)
                                            .Replace("@Compliance", strShortDescription)
                                            .Replace("@PreviousDueDate", CurrentDueDate.ToString("dd-MMM-yyyy"))
                                            .Replace("@NewDueDate", Objtextboxdate.ToString("dd-MMM-yyyy")));




                                string messageReviewer = AlertChangedDueDateEmailReviewer.Replace("@User", "User").Replace("@Details", detailsReviewer.ToString());
                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { userReviewer.Email }).ToList(), null, null, "AVACOM Alert :: Due Date of Internal Compliance Changed", messageReviewer);

                            }



                        }//oNE tIME
                        else
                        {

                            DateTime nextDueDate = GetNextDueDate(CurrentDueDate, Convert.ToString(Icompliances.IFrequency), Convert.ToString(Icompliances.ISubComplianceType), Convert.ToString(Icompliances.IDueDate));
                            DateTime PreviousDueDate = GetPreviousDueDate(CurrentDueDate, Convert.ToString(Icompliances.IFrequency), Convert.ToString(Icompliances.ISubComplianceType), Convert.ToString(Icompliances.IDueDate));
                            if (Objtextboxdate > PreviousDueDate)
                            {

                                if (Objtextboxdate <= nextDueDate)
                                {
                                    int IComplianceAssignmentID = (from row in entities.InternalComplianceAssignments
                                                                   where row.RoleID == RoleID && row.UserID == UserId && row.InternalComplianceInstanceID == vInternalComplianceInstanceID
                                                                   select row.ID).FirstOrDefault();

                                    InternalComplianceScheduledOn scheduleOnToUpdate = (from row in entities.InternalComplianceScheduledOns
                                                                                        where row.ID == vInternalScheduledOnID
                                                                                        select row).FirstOrDefault();

                                    scheduleOnToUpdate.ScheduledOn = GetDate(Objtextboxdate.ToString("dd-MM-yyyy"));
                                    entities.SaveChanges();

                                    var PrevReminderDates = (from row in entities.InternalComplianceReminders
                                                             where row.ComplianceDueDate == CurrentDueDate && row.ComplianceAssignmentID == IComplianceAssignmentID
                                                             select row).ToList();

                                    foreach (InternalComplianceReminder IC in PrevReminderDates)
                                    {
                                        entities.InternalComplianceReminders.Remove(IC);
                                    }
                                    entities.SaveChanges();
                                    InternalComplianceManagement.CreateReminders(vInternalComplianceID, vInternalComplianceInstanceID, IComplianceAssignmentID, Objtextboxdate);
                                    grdComplianceTransactions.EditIndex = -1;
                                    BindComplianceTransactions();


                                    H_Internal_ChangeDueDate historyDueDate = new H_Internal_ChangeDueDate()
                                    {
                                        InternalComplianceId = vInternalComplianceID,
                                        InternalComplianceInstanceId = vInternalComplianceInstanceID,
                                        InternalAssignmentId = IComplianceAssignmentID,
                                        ScheduledonID = vInternalScheduledOnID,
                                        PreviousScheduledonDate = CurrentDueDate,
                                        NewScheduledonDate = Objtextboxdate,
                                        CreatedOn = DateTime.UtcNow,
                                        Createdby = Convert.ToInt32(Session["userID"]),
                                    };
                                    InternalComplianceManagement.CreateHistoryDueDate(historyDueDate);

                                    //-----------------------------------------------------------------------------------------------------------------
                                    int userID = -1;
                                    userID = Convert.ToInt32(Session["userID"]);


                                    long ReviewerId = -1;

                                    ReviewerId = (from row in entities.InternalComplianceAssignments
                                                  where row.RoleID == 4 && row.InternalComplianceInstanceID == vInternalComplianceInstanceID
                                                  select row.UserID).FirstOrDefault();

                                    int customerID = -1;
                                    customerID = UserManagement.GetByID(Convert.ToInt32(userID)).CustomerID ?? 0;
                                    string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;

                                    User user = UserManagement.GetByID(UserId);
                                    string accessURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        accessURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }
                                    var AlertChangedDueDateEmail = Properties.Settings.Default.EMailTemplate_AlertChangedDueDate
                                                                  .Replace("@User", user.FirstName + " " + user.LastName)
                                                                  .Replace("@URL", Convert.ToString(accessURL))
                                                                  .Replace("@From", ReplyEmailAddressName);
                                    //var AlertChangedDueDateEmail = Properties.Settings.Default.EMailTemplate_AlertChangedDueDate
                                    //                               .Replace("@User", user.FirstName + " " + user.LastName)
                                    //                               .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    //                               .Replace("@From", ReplyEmailAddressName);

                                    StringBuilder details = new StringBuilder();


                                    details.AppendLine(Properties.Settings.Default.EMailTemplate_AlertChangedDueDateRows

                                                .Replace("@Branch", strBranch)
                                                .Replace("@Compliance", strShortDescription)
                                                .Replace("@PreviousDueDate", CurrentDueDate.ToString("dd-MMM-yyyy"))
                                                .Replace("@NewDueDate", Objtextboxdate.ToString("dd-MMM-yyyy")));



                                    string message = AlertChangedDueDateEmail.Replace("@User", "User").Replace("@Details", details.ToString());
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Alert :: Due Date of Internal Compliance Changed", message);

                                    if ((ReviewerId != -1) && (ReviewerId != 0))
                                    {
                                        User userReviewer = UserManagement.GetByID((int)ReviewerId);

                                        var AlertChangedDueDateEmailReviewer = Properties.Settings.Default.EMailTemplate_AlertChangedDueDate
                                                                .Replace("@User", userReviewer.FirstName + " " + userReviewer.LastName)
                                                                .Replace("@URL", Convert.ToString(accessURL))
                                                                .Replace("@From", ReplyEmailAddressName);

                                        //var AlertChangedDueDateEmailReviewer = Properties.Settings.Default.EMailTemplate_AlertChangedDueDate
                                        //                          .Replace("@User", userReviewer.FirstName + " " + userReviewer.LastName)
                                        //                          .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                          .Replace("@From", ReplyEmailAddressName);

                                        StringBuilder detailsReviewer = new StringBuilder();


                                        detailsReviewer.AppendLine(Properties.Settings.Default.EMailTemplate_AlertChangedDueDateRows

                                                    .Replace("@Branch", strBranch)
                                                    .Replace("@Compliance", strShortDescription)
                                                    .Replace("@PreviousDueDate", CurrentDueDate.ToString("dd-MMM-yyyy"))
                                                    .Replace("@NewDueDate", Objtextboxdate.ToString("dd-MMM-yyyy")));




                                        string messageReviewer = AlertChangedDueDateEmailReviewer.Replace("@User", "User").Replace("@Details", detailsReviewer.ToString());
                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { userReviewer.Email }).ToList(), null, null, "AVACOM Alert :: Due Date of Internal Compliance Changed", messageReviewer);

                                    }


                                }
                                else
                                {
                                    lblErrorMsgI.Text = "Edited Due date should not be after next due date";

                                }
                            }
                            else
                            {

                                lblErrorMsgI.Text = "Edited Due date should not be before previous due date";
                            }
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdComplianceTransactions.EditIndex = -1;
            BindComplianceTransactions();
        }

        //added by Manisha
        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);      
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                udcStatusTranscation.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }

}