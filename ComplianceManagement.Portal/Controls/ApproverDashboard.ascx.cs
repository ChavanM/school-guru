﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using OfficeOpenXml;
using System.IO;
using System.Web.UI.HtmlControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Data;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Net.Mail;
using OfficeOpenXml.Style;
using System.Collections;
using Ionic.Zip;
using System.Text;
using System.Drawing;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class ApproverDashboard : System.Web.UI.UserControl
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((!IsPostBack))
            {
                if (Session["RahulRoleID"] !=null)
                {
                    if (Session["RahulRoleID"].ToString().Trim() == "6" || Session["RahulRoleID"].ToString().Trim() == "3")
                    {
                        BindLocationFilter();
                        BindMonth(DateTime.UtcNow.Year);
                        BindYear();
                        int year = Convert.ToInt32(ddlyear.SelectedValue);
                        int month = Convert.ToInt32(ddlmonths.SelectedValue);
                        if (tvLocation.Nodes.Count > 0)
                        {
                            ShowCompanySummary(Convert.ToInt32(tvLocation.Nodes[0].Value));
                            BindComplianceRiskCriteria(Convert.ToInt32(tvLocation.Nodes[0].Value));
                            DrowGraphs();
                        }
                    }                          
                }                        
            }          
        }

        private void BindMonth(int year)
        {
            try
            {
                ddlmonths.DataTextField = "Name";
                ddlmonths.DataValueField = "ID";
                int selectedMonth = 0;
                List<NameValue> months;
                if (year == DateTime.UtcNow.Year)
                {
                    months = Enumerations.GetAll<Month>().Where(entry => entry.ID <= DateTime.UtcNow.Month).ToList();
                    selectedMonth = months.Count;
                }
                else
                {
                    months = Enumerations.GetAll<Month>();
                    selectedMonth = 1;
                }

                ddlmonths.DataSource = months;
                ddlmonths.DataBind();
                ddlmonths.SelectedValue = Convert.ToString(selectedMonth);
                //ddlmonths.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindYear()
        {

            int CurrentYear = DateTime.Now.Year;
            for (int i = 1; i < 100; ++i)
            {
                System.Web.UI.WebControls.ListItem tmp = new System.Web.UI.WebControls.ListItem();
                tmp.Value = CurrentYear.ToString();
                tmp.Text = CurrentYear.ToString();
                ddlyear.Items.Add(tmp);
                CurrentYear = DateTime.Now.AddYears(-i).Year;

            }

        }

        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonth(Convert.ToInt32(ddlyear.SelectedValue));
            if (tvLocation.SelectedNode != null)
            {
                BindComplianceRiskCriteria(Convert.ToInt32(tvLocation.SelectedNode.Value));
                DrowGraphs();
            }
        }

        protected void ddlmonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tvLocation.SelectedNode != null)
            {
                BindComplianceRiskCriteria(Convert.ToInt32(tvLocation.SelectedNode.Value));
                DrowGraphs();
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                var bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWise(6, AuthenticationHelper.UserID);

                tvLocation.Nodes.Clear();
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    //BindBranchesHierarchy(node, item);
                    tvLocation.Nodes.Add(node);
                    tvLocation.Nodes[0].Select();
                }

                tvLocation.CollapseAll();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {

                ShowCompanySummary(Convert.ToInt32(tvLocation.SelectedNode.Value));
                BindComplianceRiskCriteria(Convert.ToInt32(tvLocation.SelectedNode.Value));
                DrowGraphs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

       
        protected void ShowCompanySummary(int barnchID)
        {
            try
            {
                int year = Convert.ToInt32(ddlyear.SelectedValue);
                int month = Convert.ToInt32(ddlmonths.SelectedValue);

                lblLocation.Text = tvLocation.SelectedNode.Text;
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                lblTotalNoofUsers.Text = Convert.ToString(AssignEntityManagement.GetCompanyOverview(customerid, barnchID));
                IEnumerable<dynamic> reviwerNames = AssignEntityManagement.GetReviwersName(customerid,barnchID);
                string reviewerNameslist = "";
                foreach (var rn in reviwerNames)
                {
                    reviewerNameslist += rn.User + "<br />";
                }
                divReviewerName.InnerHtml = reviewerNameslist;

                string performerNameslist = "";
                IEnumerable<dynamic> performerNames = AssignEntityManagement.GetPerformerName(customerid,barnchID);
                foreach (var rn in performerNames)
                {
                    performerNameslist += rn.User + "<br />";
                }
                divPerformerName.InnerHtml = performerNameslist;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindComplianceRiskCriteria(int barnchID)
        {
            try
            {
                int year = Convert.ToInt32(ddlyear.SelectedValue);
                int month = Convert.ToInt32(ddlmonths.SelectedValue);
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                grdComplianceTransactions.DataSource = AssignEntityManagement.GetCompliancesStatusRiskWise(customerid, barnchID, year, month, AuthenticationHelper.UserID, true);
                grdComplianceTransactions.DataBind();

                grdMangementDashFunctionwise.DataSource = AssignEntityManagement.GetCompliancesStatusFunctionWise(barnchID, year, month, AuthenticationHelper.UserID, true);
                grdMangementDashFunctionwise.DataBind();

                grdComplianceSummary.DataSource = AssignEntityManagement.GetManagementCompliancesSummary(barnchID, year, month, AuthenticationHelper.UserID, true);
                grdComplianceSummary.DataBind();

                grdComplianceTwelveMonthSummary.DataSource = AssignEntityManagement.GetPastTwelveMonthSummary(customerid, barnchID, year, month, AuthenticationHelper.UserID, true);
                grdComplianceTwelveMonthSummary.DataBind();

                int startDateyear = year;
                if (month < 4)
                {
                    startDateyear--;
                }
                DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
                DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);
                grdComplianceTwelveMonthSummary.HeaderRow.Cells[0].Text = "Compliances";
                for (int i = 12; i > 0; i--)
                {
                    grdComplianceTwelveMonthSummary.HeaderRow.Cells[12 - (i - 1)].Text = EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy");
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        decimal Completedtotal = 0;
        decimal Delayedtotal = 0;
        decimal Pendingtotal = 0;
        decimal Grandtotal = 0;
        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lblRiskCompleted = (LinkButton)e.Row.FindControl("lblRiskCompleted");
                    LinkButton lblRiskDelayed = (LinkButton)e.Row.FindControl("lblRiskDelayed");
                    LinkButton lblRiskPending = (LinkButton)e.Row.FindControl("lblRiskPending");
                    LinkButton lblRiskTotal = (LinkButton)e.Row.FindControl("lblRiskTotal");
                    Completedtotal += Convert.ToDecimal(lblRiskCompleted.Text);
                    Delayedtotal += Convert.ToDecimal(lblRiskDelayed.Text);
                    Pendingtotal += Convert.ToDecimal(lblRiskPending.Text);
                    Grandtotal += Convert.ToDecimal(lblRiskTotal.Text);

                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    LinkButton lblCompleted = (LinkButton)e.Row.FindControl("lblCompleted");
                    LinkButton lblDelayed = (LinkButton)e.Row.FindControl("lblDelayed");
                    LinkButton lblPending = (LinkButton)e.Row.FindControl("lblPending");
                    LinkButton lblgrandTotal = (LinkButton)e.Row.FindControl("lblgrandTotal");
                    lblCompleted.Text = Completedtotal.ToString();
                    lblDelayed.Text = Delayedtotal.ToString();
                    lblPending.Text = Pendingtotal.ToString();
                    lblgrandTotal.Text = Grandtotal.ToString();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        decimal Completedftotal = 0;
        decimal Delayedftotal = 0;
        decimal Pendingftotal = 0;
        decimal Grandftotal = 0;
        protected void grdMangementDashFunctionwise_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lblfCompleted = (LinkButton)e.Row.FindControl("lblfCompleted");
                    LinkButton lblfDelayed = (LinkButton)e.Row.FindControl("lblfDelayed");
                    LinkButton lblfPending = (LinkButton)e.Row.FindControl("lblfPending");
                    LinkButton lblfTotal = (LinkButton)e.Row.FindControl("lblfTotal");
                    Completedftotal += Convert.ToDecimal(lblfCompleted.Text);
                    Delayedftotal += Convert.ToDecimal(lblfDelayed.Text);
                    Pendingftotal += Convert.ToDecimal(lblfPending.Text);
                    Grandftotal += Convert.ToDecimal(lblfTotal.Text);

                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    LinkButton lblfunctionCompleted = (LinkButton)e.Row.FindControl("lblfunctionCompleted");
                    LinkButton lblfunctionDelayed = (LinkButton)e.Row.FindControl("lblfunctionDelayed");
                    LinkButton lblfunctionPending = (LinkButton)e.Row.FindControl("lblfunctionPending");
                    LinkButton lblfunctiongrandTotal = (LinkButton)e.Row.FindControl("lblfunctiongrandTotal");
                    lblfunctionCompleted.Text = Completedftotal.ToString();
                    lblfunctionDelayed.Text = Delayedftotal.ToString();
                    lblfunctionPending.Text = Pendingftotal.ToString();
                    lblfunctiongrandTotal.Text = Grandftotal.ToString();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        decimal Hightotal = 0;
        decimal Mediumtotal = 0;
        decimal Lowtotal = 0;
        decimal SummaryGrandtotal = 0;
        protected void grdComplianceSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lblSumaryHigh = (LinkButton)e.Row.FindControl("lblSumaryHigh");
                    LinkButton lblSumaryMedium = (LinkButton)e.Row.FindControl("lblSumaryMedium");
                    LinkButton lblSumaryLow = (LinkButton)e.Row.FindControl("lblSumaryLow");
                    LinkButton lblSumaryTotal = (LinkButton)e.Row.FindControl("lblSumaryTotal");
                    Hightotal += Convert.ToDecimal(lblSumaryHigh.Text);
                    Mediumtotal += Convert.ToDecimal(lblSumaryMedium.Text);
                    Lowtotal += Convert.ToDecimal(lblSumaryLow.Text);
                    SummaryGrandtotal += Convert.ToDecimal(lblSumaryTotal.Text);

                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    LinkButton lblSHigh = (LinkButton)e.Row.FindControl("lblSHigh");
                    LinkButton lblSMedium = (LinkButton)e.Row.FindControl("lblSMedium");
                    LinkButton lblSLow = (LinkButton)e.Row.FindControl("lblSLow");
                    LinkButton lblSummarygrandTotal = (LinkButton)e.Row.FindControl("lblSummarygrandTotal");
                    lblSHigh.Text = Hightotal.ToString();
                    lblSMedium.Text = Mediumtotal.ToString();
                    lblSLow.Text = Lowtotal.ToString();
                    lblSummarygrandTotal.Text = SummaryGrandtotal.ToString();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnTwelthMonthSummary_Click(object sender, EventArgs e)
        {
            try
            {

                if (divTelweMonthSummary.Visible == false)
                {
                    divTelweMonthSummary.Visible = true;

                }
                else
                {
                    divTelweMonthSummary.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Dashboard.xls"));
                    Response.ContentType = "application/ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);

                    string DashboardAsOn = "<h3>Dashboard Summary as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text + "</h3>";
                    sw.Write(DashboardAsOn);
                    sw.Write("<br/>");
                    sw.Write("Total No of Users : " + lblTotalNoofUsers.Text);
                    sw.Write("<br/>");
                    sw.Write("Location : " + tvLocation.SelectedNode.Text);
                    sw.Write("<br/>");
                    string ReviewerVal = divReviewerName.InnerHtml;
                    string ReviewerNames = ReviewerVal;
                    int indexLast = ReviewerVal.LastIndexOf("<br />");

                    if (indexLast >= 0)
                    {
                        ReviewerNames = ReviewerVal.Substring(0, indexLast).Replace("<br />", ", ") + ReviewerVal.Substring(indexLast);
                    }
                    sw.Write("Reviewer Name : " + ReviewerNames);
                    sw.Write("<br/>");

                    string val = divPerformerName.InnerHtml;
                    string PerformerNames = val;
                    int index = val.LastIndexOf("<br />");
                    if (index >= 0)
                    {
                        PerformerNames = val.Substring(0, index).Replace("<br />", ", ") + val.Substring(index);
                    }
                    sw.Write("Performer Name : " + PerformerNames);
                    sw.Write("<br/>");
                    sw.Write("Compliance Summary");
                    grdComplianceSummary.HeaderRow.Style.Add("background-color", "#5c9ccc");
                    grdComplianceSummary.RenderControl(htw);
                    sw.Write("<br/>");
                    sw.Write("Compliance Status Risk-wise");
                    grdComplianceTransactions.HeaderRow.Style.Add("background-color", "#5c9ccc");
                    grdComplianceTransactions.RenderControl(htw);
                    sw.Write("<br/>");
                    sw.Write("Compliance Status Function-wise");
                    grdMangementDashFunctionwise.HeaderRow.Style.Add("background-color", "#5c9ccc");
                    grdMangementDashFunctionwise.RenderControl(htw);
                    sw.Write("<br/>");
                    if (divTelweMonthSummary.Visible == true)
                    {
                        sw.Write("Compliance Past 12 Month's Summary");
                        grdComplianceTwelveMonthSummary.HeaderRow.Style.Add("background-color", "#5c9ccc");
                        grdComplianceTwelveMonthSummary.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string Script = string.Empty;
                StringWriter stringWrite = new StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
                if (grdComplianceTwelveMonthSummary is WebControl)
                {
                    Unit w = new Unit(100, UnitType.Percentage); ((WebControl)grdComplianceTwelveMonthSummary).Width = w;
                }
                Page pg = new Page();
                pg.EnableEventValidation = false;
                if (Script != string.Empty)
                {
                    pg.ClientScript.RegisterStartupScript(pg.GetType(), "PrintJavaScript", Script);
                }

                HtmlForm frm = new HtmlForm();
                pg.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                Label management = new Label();
                Label space = new Label();
                space.Width = Unit.Percentage(100);
                space.Height = Unit.Pixel(20);
                management.Text = "Approver Dashboard as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text;
                management.Font.Bold = true;
                management.Width = Unit.Percentage(100);
                frm.Controls.Add(management);
                frm.Controls.Add(space);
                Label location = new Label();
                location.Text = "Logged in as on " + AuthenticationHelper.User;
                location.Font.Bold = true;
                frm.Controls.Add(location);
                frm.Controls.Add(space);
                Panel panel = new Panel();
                panel.Controls.Add(tbDashboardSummary);
                frm.Controls.Add(panel);
                if (divTelweMonthSummary.Visible == true)
                {
                    frm.Controls.Add(space);
                    frm.Controls.Add(grdComplianceTwelveMonthSummary);
                }
                pg.DesignerInitialize();
                pg.RenderControl(htmlWrite);
                string strHTML = stringWrite.ToString();
                strHTML = strHTML + "<style type='text/css'>th{background: #5c9ccc;}</style>";
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(strHTML);
                HttpContext.Current.Response.Write("<script>window.onload= function () { window.print();window.close();   }  </script>");
                HttpContext.Current.Response.End();
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTwelveMonthSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(emailids.Text))
                {

                    string[] emailIsd = emailids.Text.Split(',');

                    ShowCompanySummary(Convert.ToInt32(tvLocation.SelectedNode.Value));
                    BindComplianceRiskCriteria(Convert.ToInt32(tvLocation.SelectedNode.Value));

                    Document pdfDoc = new Document(PageSize.A4_LANDSCAPE, 20, 20, 20, 20);
                    DataTable dataToSummary = grdComplianceSummary.DataSource as DataTable;
                    DataTable dataToExportTransactions = grdComplianceTransactions.DataSource as DataTable;
                    DataTable dataToExportFunctionwise = grdMangementDashFunctionwise.DataSource as DataTable;

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        writer.CloseStream = false;
                        pdfDoc.Open();

                        try
                        {
                            pdfDoc.Add(iTextSharp.text.Chunk.NEWLINE);
                            iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(PDFExportHelper.FormatPageHeaderPhrase("Approver Dashboards"));
                            paragraph.SpacingAfter = 10f;
                            pdfDoc.Add(paragraph);
                            string val = divReviewerName.InnerHtml;
                            string reviewerNames = val;
                            int index = val.LastIndexOf("<br />");
                            if (index >= 0)
                            {
                                reviewerNames = val.Substring(0, index).Replace("<br />", ", ") + val.Substring(index);
                            }

                            paragraph = new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Dashboard Summary as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text + " for " + tvLocation.SelectedNode.Text));
                            paragraph.SpacingAfter = 5f;
                            pdfDoc.Add(paragraph);

                            pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Total No of Users : " + lblTotalNoofUsers.Text)));
                            //pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Approver Name : " + lblapprover.Text)));
                            pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Reviewer Name : " + reviewerNames)));
                            pdfDoc.Add((new iTextSharp.text.Paragraph(PDFExportHelper.FormatPageHeaderPhrase("Compliance Summary")) { SpacingAfter = 5f }));
                            PdfPTable dataTable = new PdfPTable(dataToSummary.Columns.Count);
                            PdfPTable dataTable1 = new PdfPTable(dataToExportTransactions.Columns.Count);
                            PdfPTable dataTable2 = new PdfPTable(dataToExportFunctionwise.Columns.Count);


                            pdfDoc.Add(SetStyleForPDFDoc(dataTable, dataToSummary));
                            pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatPageHeaderPhrase("Compliance Status Risk-wise")) { SpacingAfter = 5f });
                            pdfDoc.Add(SetStyleForPDFDoc(dataTable1, dataToExportTransactions));
                            pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatPageHeaderPhrase("Compliance Status Function-wise")) { SpacingAfter = 5f });
                            pdfDoc.Add(SetStyleForPDFDoc(dataTable2, dataToExportFunctionwise));
                            if (divTelweMonthSummary.Visible == true)
                            {
                                pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatPageHeaderPhrase("Compliance Past 12 Month's Summary")) { SpacingAfter = 5f });
                                paragraph.SpacingAfter = 5f;
                                DataTable dataToExportTwelveMonthSummary = grdComplianceTwelveMonthSummary.DataSource as DataTable;
                                PdfPTable dataTable3 = new PdfPTable(dataToExportTwelveMonthSummary.Columns.Count);
                                pdfDoc.Add(SetStyleForPDFDoc(dataTable3, dataToExportTwelveMonthSummary));
                            }

                        }
                        finally
                        {
                            pdfDoc.Close();
                        }

                        memoryStream.Position = 0;

                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();

                        List<Attachment> attachment = new List<Attachment>();
                        attachment.Add(new Attachment(new MemoryStream(bytes), "ApproverDashboards.pdf"));
                        int customerID = -1;
                        string ReplyEmailAddressName = "";
                        if (AuthenticationHelper.Role == "SADMN")
                        {
                            ReplyEmailAddressName = "Avantis";
                        }
                        else
                        {
                            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                            ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                        }
                        string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                         + "Dear Sir,<br /><br />"
                                         + "Here is Attached Approver dashboard summary pdf.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "Approver Dashboards.", message, attachment);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#sendManagementEmail\").dialog('close')", true);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected PdfPTable SetStyleForPDFDoc(PdfPTable dataTable, DataTable dataToExport)
        {
            dataTable.DefaultCell.Padding = 3;
            dataTable.WidthPercentage = 100; // percentage
            dataTable.DefaultCell.BorderWidth = 1;
            dataTable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            dataTable.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

            for (int colIndex = 0; colIndex < dataToExport.Columns.Count; colIndex++)
            {
                dataTable.AddCell(PDFExportHelper.FormatHeaderPhrase(dataToExport.Columns[colIndex].ColumnName));
            }

            dataTable.HeaderRows = 0;  // this is the end of the table header

            for (int rowIndex = 0; rowIndex < dataToExport.Rows.Count; rowIndex++)
            {
                for (int colIndex = 0; colIndex < dataToExport.Columns.Count; colIndex++)
                {
                    dataTable.AddCell(PDFExportHelper.FormatPhrase(dataToExport.Rows[rowIndex][dataToExport.Columns[colIndex].ColumnName].ToString()));
                }
            }

            return dataTable;
        }

        //For drill down report.
        #region Previous Code
        //private void BindDetailView(string aregument, string filter, string TwelveMonth = "")
        //{
        //    int year = Convert.ToInt32(ddlyear.SelectedValue);
        //    int month = Convert.ToInt32(ddlmonths.SelectedValue);
        //    List<int> statusIds = new List<int>();
        //    string[] arg = new string[3];
        //    string lable = string.Empty;
        //    arg = aregument.ToString().Split(';');
        //    if (filter.Equals("Summary"))
        //    {
        //        if (arg[0].Trim().Equals("High"))
        //            statusIds.Add(0);
        //        else if (arg[0].Trim().Equals("Medium"))
        //        {
        //            statusIds.Add(1);
        //        }
        //        else if (arg[0].Trim().Equals("Low"))
        //        {
        //            statusIds.Add(2);
        //        }
        //        else
        //        {
        //            statusIds.Add(0);
        //            statusIds.Add(1);
        //            statusIds.Add(2);
        //        }

        //        if (string.IsNullOrEmpty(arg[1]))
        //        {
        //            arg[1] = "-1";
        //        }
        //        lable = arg[2].ToString() + " = " + arg[0].ToString() + " Compliances";
        //    }
        //    else if (filter.Equals("TwelveMonthSummary"))
        //    {
        //        if (arg[0].Trim().Equals("Completed In Time"))
        //        {
        //            statusIds.Add(4);
        //            statusIds.Add(7);
        //        }
        //        else if (arg[0].Trim().Equals("Completed after Due Date"))
        //        {
        //            statusIds.Add(5);
        //            statusIds.Add(9);
        //        }
        //        else if (arg[0].Trim().Equals("Not Yet Completed"))
        //        {
        //            statusIds.Add(1);
        //            statusIds.Add(10);
        //            statusIds.Add(2);
        //            statusIds.Add(3);
        //            statusIds.Add(6);
        //            statusIds.Add(8);
        //        }

        //        lable = arg[2].ToString() + " = " + TwelveMonth + " Month Compliances";
        //    }
        //    else
        //    {

        //        if (arg[0].Trim().Equals("Delayed"))
        //        {
        //            statusIds.Add(5);
        //            statusIds.Add(9);
        //        }
        //        else if (arg[0].Trim().Equals("InTime"))
        //        {
        //            statusIds.Add(4);
        //            statusIds.Add(7);
        //        }
        //        else if (arg[0].Trim().Equals("Pending"))
        //        {
        //            statusIds.Add(1);
        //            statusIds.Add(10);
        //            statusIds.Add(6);
        //            statusIds.Add(2);
        //            statusIds.Add(3);
        //        }
        //        else
        //        {
        //            statusIds.Add(1);
        //            statusIds.Add(6);
        //            statusIds.Add(10);
        //            statusIds.Add(4);
        //            statusIds.Add(5);
        //            statusIds.Add(7);
        //            statusIds.Add(9);
        //            statusIds.Add(8);
        //            statusIds.Add(2);
        //            statusIds.Add(3);
        //        }

        //        if (string.IsNullOrEmpty(arg[1]))
        //        {
        //            arg[1] = "-1";
        //        }
        //        lable = arg[2].ToString() + " = " + arg[0].ToString() + " Compliances";
        //    }

        //    int BranchID = Convert.ToInt32(tvLocation.SelectedNode.Value);

        //    var detailView = AssignEntityManagement.GetManagementDetailView(BranchID, year, month, statusIds, filter, Convert.ToInt32(arg[1]), TwelveMonth);

           
            
        //    lblDetailViewTitle.Text = lable;
        //    if (arg[0].Trim().Equals("Pending"))
        //    {
        //        grdSummaryDetails.Columns[7].Visible = false;
        //    }
        //    else
        //    {
        //        grdSummaryDetails.Columns[7].Visible = true;
        //    }
        //    grdSummaryDetails.DataSource = detailView;
        //    grdSummaryDetails.DataBind();

        //    UpDetailView.Update();
        //    Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<ComplianceDashboardSummaryView>).ToDataTable();
        //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divSummaryDetails\").dialog('open')", true);

        //}
        #endregion

        private void BindDetailView(string aregument, string filter, string TwelveMonth = "")
        {
            int customerID = -1;
            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
            int year = Convert.ToInt32(ddlyear.SelectedValue);
            int month = Convert.ToInt32(ddlmonths.SelectedValue);
            List<int> statusIds = new List<int>();
            string[] arg = new string[3];
            string lable = string.Empty;
            arg = aregument.ToString().Split(';');
            if (filter.Equals("Summary"))
            {
                if (arg[0].Trim().Equals("High"))
                    statusIds.Add(0);
                else if (arg[0].Trim().Equals("Medium"))
                {
                    statusIds.Add(1);
                }
                else if (arg[0].Trim().Equals("Low"))
                {
                    statusIds.Add(2);
                }
                else
                {
                    statusIds.Add(0);
                    statusIds.Add(1);
                    statusIds.Add(2);
                }

                if (string.IsNullOrEmpty(arg[1]))
                {
                    arg[1] = "-1";
                }
                lable = arg[2].ToString() + " = " + arg[0].ToString() + " Compliances";
            }
            else if (filter.Equals("TwelveMonthSummary"))
            {
                if (arg[0].Trim().Equals("Completed In Time"))
                {
                    statusIds.Add(4);
                    statusIds.Add(7);
                }
                else if (arg[0].Trim().Equals("Completed after Due Date"))
                {
                    statusIds.Add(5);
                    statusIds.Add(9);
                }
                else if (arg[0].Trim().Equals("Not Yet Completed"))
                {
                    statusIds.Add(1);
                    statusIds.Add(10);
                    statusIds.Add(2);
                    statusIds.Add(3);
                    statusIds.Add(6);
                    statusIds.Add(8);
                }

                lable = arg[2].ToString() + " = " + TwelveMonth + " Month Compliances";
            }
            else
            {

                if (arg[0].Trim().Equals("Delayed"))
                {
                    statusIds.Add(5);
                    statusIds.Add(9);
                }
                else if (arg[0].Trim().Equals("InTime"))
                {
                    statusIds.Add(4);
                    statusIds.Add(7);
                }
                else if (arg[0].Trim().Equals("Pending"))
                {
                    statusIds.Add(1);
                    statusIds.Add(10);
                    statusIds.Add(6);
                    statusIds.Add(2);
                    statusIds.Add(3);
                }
                else
                {
                    statusIds.Add(1);
                    statusIds.Add(6);
                    statusIds.Add(10);
                    statusIds.Add(4);
                    statusIds.Add(5);
                    statusIds.Add(7);
                    statusIds.Add(9);
                    statusIds.Add(8);
                    statusIds.Add(2);
                    statusIds.Add(3);
                }

                if (string.IsNullOrEmpty(arg[1]))
                {
                    arg[1] = "-1";
                }
                lable = arg[2].ToString() + " = " + arg[0].ToString() + " Compliances";
            }

            int BranchID = Convert.ToInt32(tvLocation.SelectedNode.Value);

            var detailView = AssignEntityManagement.GetManagementDetailView(customerID,BranchID, year, month, statusIds, filter, Convert.ToInt32(arg[1]), TwelveMonth);

            //var detailViewq = AssignEntityManagement.GetManagementDetailViewRahul(BranchID, year, month, statusIds, filter, Convert.ToInt32(arg[1]), TwelveMonth);
            
    
            lblDetailViewTitle.Text = lable;
            if (arg[0].Trim().Equals("Pending"))
            {
                grdSummaryDetails.Columns[7].Visible = false;
            }
            else
            {
                grdSummaryDetails.Columns[7].Visible = true;
            }
            grdSummaryDetails.DataSource = detailView;
            grdSummaryDetails.DataBind();

            UpDetailView.Update();
            Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<ComplianceDashboardSummaryView>).ToDataTable();
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divSummaryDetails\").dialog('open')", true);

        }
        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    BindDetailView(e.CommandArgument.ToString(), "Risk");
                    Session["Arguments"] = e.CommandArgument.ToString();
                    Session["Filter"] = "Risk";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void grdMangementDashFunctionwise_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    BindDetailView(e.CommandArgument.ToString(), "Function");
                    Session["Arguments"] = e.CommandArgument.ToString();
                    Session["Filter"] = "Function";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void grdComplianceTwelveMonthSummary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {

                    int hedercell = Convert.ToInt32(Convert.ToString(e.CommandArgument).Split(';')[1]);
                    string TwelveMonth = grdComplianceTwelveMonthSummary.HeaderRow.Cells[hedercell].Text;
                    BindDetailView(e.CommandArgument.ToString(), "TwelveMonthSummary", TwelveMonth);
                    Session["Arguments"] = e.CommandArgument.ToString();
                    Session["Filter"] = "TwelveMonthSummary";
                    Session["TwelveMonth"] = TwelveMonth;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void grdComplianceSummary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    BindDetailView(e.CommandArgument.ToString(), "Summary");
                    Session["Arguments"] = e.CommandArgument.ToString();
                    Session["Filter"] = "Summary";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;

                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                        ExcelData = view.ToTable("Selected", false, "ShortDescription", "Branch", "ScheduledOn", "ForMonth", "Status", "RiskCategory");
                        exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["B2"].Value = lblDetailViewTitle.Text + " Report";
                        exWorkSheet.Cells["B2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 15;
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A4"].Value = "Short Description";
                        exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Value = "Start Date";
                        exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D4"].Value = "For Month";
                        exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D4"].Value = "Risk Category";

                        using (ExcelRange col = exWorkSheet.Cells[2, 1, 6 + ExcelData.Rows.Count, 8])
                        {
                            col.Style.Numberformat.Format = "dd/MM/yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.AutoFitColumns();
                        }

                        //PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=Report.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        Response.End();
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdSummaryDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        int ScheduledOnID = Convert.ToInt32(e.CommandArgument);
                        if (ScheduledOnID != 0)
                        {
                            var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                            List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                            //craeted subdirectory
                            //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                            string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                            int i = 0;
                            foreach (var file in fileData)
                            {
                                string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                if (dictionary == null)
                                    ComplianceZip.AddDirectoryByName(directoryName + "/" + version);

                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + i + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }
                        //PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;
                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=Document.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSummaryDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("lblDownLoadfile");
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterPostBackControl(lblDownLoadfile);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSummaryDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CmpSaveCheckedValues();
                grdSummaryDetails.PageIndex = e.NewPageIndex;
                if (string.IsNullOrEmpty(Convert.ToString(Session["TwelveMonth"])))
                {
                    BindDetailView(Convert.ToString(Session["Arguments"]), Convert.ToString(Session["Filter"]));
                }
                else
                {
                    BindDetailView(Convert.ToString(Session["Arguments"]), Convert.ToString(Session["Filter"]), Convert.ToString(Session["TwelveMonth"]));
                }
                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void CmpPopulateCheckedValues()
        {
            ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliances"];

            if (complianceDetails != null && complianceDetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdSummaryDetails.Rows)
                {
                    LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                    int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                    if (complianceDetails.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        private void CmpSaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            int index = -1;
            foreach (GridViewRow gvrow in grdSummaryDetails.Rows)
            {
                LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;

                // Check in the Session
                if (ViewState["checkedCompliances"] != null)
                    userdetails = (ArrayList)ViewState["checkedCompliances"];
                if (result)
                {
                    if (!userdetails.Contains(index))
                        userdetails.Add(index);
                }
                else
                    userdetails.Remove(index);
            }
            if (userdetails != null && userdetails.Count > 0)
                ViewState["checkedCompliances"] = userdetails;
        }

        // Zip all files from folder
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    CmpSaveCheckedValues();

                    if (ViewState["checkedCompliances"] != null)
                    {
                        foreach (var gvrow in (ArrayList)ViewState["checkedCompliances"])
                        {

                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                            {
                                var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                                List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                                //craeted subdirectory
                                //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                int i = 0;
                                foreach (var file in fileData)
                                {
                                    string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                    var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                    if (dictionary == null)
                                        ComplianceZip.AddDirectoryByName(directoryName + "/" + version);

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = file.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;


                        //byte[] data = zipMs.ToArray();
                        //Response.BinaryWrite(data);
                        //Response.BinaryWrite(@"C:\Users\devlp1\AppData\Local\Temp\document.txt", File.WriteAllBytes(data));

                        byte[] data = zipMs.ToArray();
                        //File.WriteAllBytes(@"C:\Users\devlp1\AppData\Local\Temp\Compliance\document.zip", data);
                        //Response.BinaryWrite(data);
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();

                    }
                }
                //BindComplianceDocument(DateTime.Now,DateTime.Now);
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected bool ViewDownload(int statusid)
        {
            if (statusid != 1)
                return true;
            else
                return false;
        }

        //for Graph

        public void DrowGraphs(bool flag = false)
        {
            try
            {
                if (flag)
                {
                    BindLocationFilter();
                    int BranchID = Convert.ToInt32(tvLocation.Nodes[0].Value);
                    BindChartHumanResource(BranchID, 2);
                }
                else
                {
                    int BranchID = Convert.ToInt32(tvLocation.SelectedNode.Value);
                    BindChartHumanResource(Convert.ToInt32(tvLocation.SelectedNode.Value), 2);
                    //BindChartLegal(Convert.ToInt32(tvLocation.SelectedNode.Value), 3);
                    //BindChartSecretarial(Convert.ToInt32(tvLocation.SelectedNode.Value), 4);
                    //BindChartFinance(Convert.ToInt32(tvLocation.SelectedNode.Value), 5);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindChartHumanResource(int barnchID, int CategoryID)
        {
            DataTable dsChartData = new DataTable();
            StringBuilder strScript = new StringBuilder();

            try
            {
                int i = 0;
                var catagories = ComplianceCategoryManagement.GetAll();
                foreach (var cat in catagories)
                {
                    int year = Convert.ToInt32(ddlyear.SelectedValue);
                    int month = Convert.ToInt32(ddlmonths.SelectedValue);
                    dsChartData = AssignEntityManagement.GetCompliancesForGraphFunctionWise(barnchID, cat.ID, year, month, AuthenticationHelper.UserID, true);

                    int totalcount = Convert.ToInt32(dsChartData.Rows[0][4]) + Convert.ToInt32(dsChartData.Rows[1][4]) + Convert.ToInt32(dsChartData.Rows[2][4]);
                    if (totalcount != 0)
                    {
                        HtmlGenericControl Parent = new HtmlGenericControl();
                        Parent.Attributes.Add("class", "graphDiv");
                        HtmlGenericControl ParentChart = new HtmlGenericControl();
                        HtmlGenericControl divcontrol = new HtmlGenericControl();
                        divcontrol.Attributes.Add("class", "mainGraph");
                        divcontrol.TagName = "div";
                        divcontrol.ID = "chart_div" + i;
                        Literal ltScripts = new Literal();
                        divcontrol.Controls.Add(ltScripts);
                        ParentChart.Controls.Add(divcontrol);
                        Parent.Controls.Add(ParentChart);

                        HtmlGenericControl ParentImage = new HtmlGenericControl();
                        ParentImage.TagName = "div";
                        ParentImage.Attributes.Add("class", "igraphImg");
                        System.Web.UI.WebControls.Image img = new System.Web.UI.WebControls.Image();
                        img.Width = 50;
                        img.Height = 50;
                        ParentImage.Controls.Add(img);
                        Label heding = new Label();
                        heding.Text = "Grading";
                        ParentImage.Controls.Add(heding);
                        Parent.Controls.Add(ParentImage);

                        pnlGraph.Controls.Add(Parent);

                        strScript.Append(@"<script type='text/javascript'>  
                                    google.load('visualization', '1', {packages:['corechart']});</script>  
                                    <script type='text/javascript'>
                                    function drawVisualization" + i + @"() {       
                                    var data = google.visualization.arrayToDataTable([  
                                    ['" + cat.Name + "', 'Completed', 'Delayed', 'Pending',],");

                        foreach (DataRow row in dsChartData.Rows)
                        {
                            strScript.Append("['" + row["RiskCatagory"] + "'," + row["Completed"] + "," +
                                row["Delayed"] + "," + row["Pending"] + "],");
                        }

                        strScript.Remove(strScript.Length - 1, 1);
                        strScript.Append("]);");
                        strScript.Append("var options = { title: '" + cat.Name + "', titleTextStyle: {color: '#4297d7', fontName: 'Arial', fontSize: '14', fontWidth: 'normal',align:'center'}, legend: {position: 'none'},colors: ['#70AD47','#FFC000', '#FF3300']};");
                        strScript.Append(" var chart = new google.visualization.ColumnChart(document.getElementById('BodyContent_ucApproverDashboard_chart_div" + i + "'));  chart.draw(data, options); } google.setOnLoadCallback(drawVisualization" + i + ");");
                        strScript.Append(" </script>");

                        ltScripts.Text = strScript.ToString();

                        int grade = AssignEntityManagement.GetGradeForManagementDashboard(barnchID, cat.ID, year, month, AuthenticationHelper.UserID, true);
                        if (grade == 1)
                            img.ImageUrl = "~/Images/green.png";
                        else if (grade == 2)
                            img.ImageUrl = "~/Images/yellow.png";
                        else
                            img.ImageUrl = "~/Images/red.png";


                        i = i + 1;
                    }

                }
            }
            catch
            {
            }
            finally
            {
                //dsChartData.Dispose();
                strScript.Clear();
            }

        }

//        private void BindChartLegal(int barnchID, int CategoryID)
//        {
//            DataTable dsChartData = new DataTable();
//            StringBuilder strScript = new StringBuilder();

//            try
//            {
//                int year = Convert.ToInt32(ddlyear.SelectedValue);
//                int month = Convert.ToInt32(ddlmonths.SelectedValue);
//                dsChartData = AssignEntityManagement.GetCompliancesForGraphFunctionWise(barnchID, CategoryID, year, month, AuthenticationHelper.UserID, true);




//                strScript.Append(@"<script type='text/javascript'>  
//                                    google.load('visualization', '1', {packages:['bar']});</script>  
//                                    <script type='text/javascript'>  
//                                    function drawVisualization2() {         
//                                    var data2 = google.visualization.arrayToDataTable([  
//                                    ['Legal', 'Completed', 'Delayed', 'Pending'],");

//                foreach (DataRow row in dsChartData.Rows)
//                {
//                    strScript.Append("['" + row["RiskCatagory"] + "'," + row["Completed"] + "," +
//                        row["Delayed"] + "," + row["Pending"] + "],");
//                }

//                strScript.Remove(strScript.Length - 1, 1);
//                strScript.Append("]);");
//                strScript.Append("var options = { legend: {position: 'none'},colors: ['#70AD47','#FFC000', '#FF3300']};");
//                strScript.Append(" var chart = new google.charts.Bar(document.getElementById('chart_div2'));  chart.draw(data2, options); } google.setOnLoadCallback(drawVisualization2);");
//                strScript.Append(" </script>");

//                ltScripts2.Text = strScript.ToString();

//                int grade = AssignEntityManagement.GetGradeForManagementDashboard(barnchID, CategoryID, year, month, AuthenticationHelper.UserID, true);
//                if (grade == 1)
//                    imgLegal.ImageUrl = "~/Images/green.png";
//                else if (grade == 1)
//                    imgLegal.ImageUrl = "~/Images/yellow.png";
//                else
//                    imgLegal.ImageUrl = "~/Images/red.png";

//            }
//            catch
//            {
//            }
//            finally
//            {
//                dsChartData.Dispose();
//                strScript.Clear();
//            }

//        }

//        private void BindChartSecretarial(int barnchID, int CategoryID)
//        {
//            DataTable dsChartData = new DataTable();
//            StringBuilder strScript = new StringBuilder();

//            try
//            {
//                int year = Convert.ToInt32(ddlyear.SelectedValue);
//                int month = Convert.ToInt32(ddlmonths.SelectedValue);
//                dsChartData = AssignEntityManagement.GetCompliancesForGraphFunctionWise(barnchID, CategoryID, year, month, AuthenticationHelper.UserID, true);



//                strScript.Append(@"<script type='text/javascript'>  
//                                    google.load('visualization', '1', {packages:['bar']});</script>  
//                                    <script type='text/javascript'>  
//                                    function drawVisualization3() {         
//                                    var data3 = google.visualization.arrayToDataTable([  
//                                    ['Secretarial', 'Completed', 'Delayed', 'Pending'],");

//                foreach (DataRow row in dsChartData.Rows)
//                {
//                    strScript.Append("['" + row["RiskCatagory"] + "'," + row["Completed"] + "," +
//                        row["Delayed"] + "," + row["Pending"] + "],");
//                }


//                strScript.Remove(strScript.Length - 1, 1);
//                strScript.Append("]);");
//                strScript.Append("var options = {legend: {position: 'none'},colors: ['#70AD47','#FFC000', '#FF3300']};");
//                strScript.Append(" var chart = new google.charts.Bar(document.getElementById('chart_div3'));  chart.draw(data3, options); } google.setOnLoadCallback(drawVisualization3);");
//                strScript.Append(" </script>");

//                ltScripts3.Text = strScript.ToString();

//                int grade = AssignEntityManagement.GetGradeForManagementDashboard(barnchID, CategoryID, year, month, AuthenticationHelper.UserID, true);
//                if (grade == 1)
//                    imgSecretarial.ImageUrl = "~/Images/green.png";
//                else if (grade == 1)
//                    imgSecretarial.ImageUrl = "~/Images/yellow.png";
//                else
//                    imgSecretarial.ImageUrl = "~/Images/red.png";

//            }
//            catch
//            {
//            }
//            finally
//            {
//                dsChartData.Dispose();
//                strScript.Clear();
//            }

//        }

//        private void BindChartFinance(int barnchID, int CategoryID)
//        {
//            DataTable dsChartData = new DataTable();
//            StringBuilder strScript = new StringBuilder();

//            try
//            {
//                int year = Convert.ToInt32(ddlyear.SelectedValue);
//                int month = Convert.ToInt32(ddlmonths.SelectedValue);
//                dsChartData = AssignEntityManagement.GetCompliancesForGraphFunctionWise(barnchID, CategoryID, year, month, AuthenticationHelper.UserID, true);

//                strScript.Append(@"<script type='text/javascript'>  
//                                    google.load('visualization', '1', {packages:['bar']});</script>  
//                                    <script type='text/javascript'>  
//                                    function drawVisualization4() {         
//                                    var data4 = google.visualization.arrayToDataTable([  
//                                    ['Finance', 'Completed', 'Delayed', 'Pending'],");

//                foreach (DataRow row in dsChartData.Rows)
//                {
//                    strScript.Append("['" + row["RiskCatagory"] + "'," + row["Completed"] + "," +
//                        row["Delayed"] + "," + row["Pending"] + "],");
//                }

//                strScript.Remove(strScript.Length - 1, 1);
//                strScript.Append("]);");
//                strScript.Append("var options = {legend: {position: 'none'},colors: ['#70AD47','#FFC000', '#FF3300']};");
//                strScript.Append(" var chart = new google.charts.Bar(document.getElementById('chart_div4'));  chart.draw(data4, options); } google.setOnLoadCallback(drawVisualization4);");
//                strScript.Append(" </script>");

//                ltScripts4.Text = strScript.ToString();

//                int grade = AssignEntityManagement.GetGradeForManagementDashboard(barnchID, CategoryID, year, month, AuthenticationHelper.UserID, true);
//                if (grade == 1)
//                    imgFinance.ImageUrl = "~/Images/green.png";
//                else if (grade == 1)
//                    imgFinance.ImageUrl = "~/Images/yellow.png";
//                else
//                    imgFinance.ImageUrl = "~/Images/red.png";

//            }
//            catch
//            {
//            }
//            finally
//            {
//                dsChartData.Dispose();
//                strScript.Clear();
//            }

//        }   




    }
}