﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls {
    
    
    public partial class UserSummarySuperAdmin {
        
        /// <summary>
        /// chrtUsersPerCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.DataVisualization.Charting.Chart chrtUsersPerCustomer;
        
        /// <summary>
        /// chrtEntitiesPerCustomers control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.DataVisualization.Charting.Chart chrtEntitiesPerCustomers;
    }
}
