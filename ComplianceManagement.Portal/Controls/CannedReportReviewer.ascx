﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CannedReportReviewer.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.CannedReportReviewer" %>

<script type="text/javascript">

    function fComplianceOverviewReviewer(obj) {

        OpenOverViewpupReviewer($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
    }

    function fComplianceOverviewInternalReviewer(obj) {
        OpenOverViewpupInternalReviewer($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
    }

    function hidediv() {
        var div = document.getElementById('AdvanceSearch1');
        div.style.display == "none" ? "block" : "none";
        $('.modal-backdrop').hide();
        return true;
    }

    function myFunction2() {
        $('#ContentPlaceHolder1_udcCannedReportReviewer_divFilterLocationReviewer').show();
    }

    $(document).on("click", "#ContentPlaceHolder1_udcCannedReportReviewer_upCannedReportReviewer", function (event) {

        if (event.target.id == "") {
            var idvid = $(event.target).closest('div');
            if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                $("#ContentPlaceHolder1_udcCannedReportReviewer_divFilterLocationReviewer").show();
            } else {
                $("#ContentPlaceHolder1_udcCannedReportReviewer_divFilterLocationReviewer").hide();
            }
        }
        else if (event.target.id != "ContentPlaceHolder1_udcCannedReportReviewer_tbxFilterLocationReviewer") {
            $("#ContentPlaceHolder1_udcCannedReportReviewer_divFilterLocationReviewer").hide();
        } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
            $("#ContentPlaceHolder1_udcCannedReportReviewer_divFilterLocationReviewer").show();
        } else if (event.target.id == "ContentPlaceHolder1_udcCannedReportReviewer_tbxFilterLocationReviewer") {
            $("#ContentPlaceHolder1_udcCannedReportReviewer_tbxFilterLocationReviewer").unbind('click');

            $("#ContentPlaceHolder1_udcCannedReportReviewer_tbxFilterLocationReviewer").click(function () {
                $("#ContentPlaceHolder1_udcCannedReportReviewer_divFilterLocationReviewer").toggle("blind", null, 500, function () { });
            });

        }
    });
</script>

<asp:UpdatePanel ID="upCannedReportReviewer" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load1">
    <ContentTemplate>
        <div style="float: right; margin-right: 10px; margin-top: -35px; display: none">
            <asp:DataList runat="server" ID="dlFilters" RepeatDirection="Horizontal" OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"
                DataKeyField="ID">
                <SeparatorTemplate>
                    <span style="margin: 0 5px 0 5px">|</span>
                </SeparatorTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select" Style="text-decoration: none; color: Black">  
                                    <%# DataBinder.Eval(Container.DataItem, "Name") %>
                    </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Font-Names="Tahoma" Font-Size="13px" VerticalAlign="Middle" />
                <SelectedItemStyle Font-Names="Tahoma" Font-Size="13px" Font-Bold="True" VerticalAlign="Middle"
                    Font-Underline="true" />
            </asp:DataList>
        </div>

        <div class="col-md-12 colpadding0">
            <div class="col-md-2 colpadding0" style="width: 108px;">
                <div class="col-md-3 colpadding0" style="width: 39px;">
                    <p style="color: #999; margin-top: 5px;">Show </p>
                </div>
                <div class="col-md-3 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 63px; float: left">
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-10 colpadding0" style="text-align: right; float: left; width: 87.7%;    margin-left: 10px;">
                <%--<div class="col-md-8 colpadding0">--%>
                <div style="margin-left: 1px;">
                    <asp:DropDownList runat="server" ID="ddlComplianceType" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" class="form-control m-bot15 search-select" Style="margin-right: 1%;">
                        <asp:ListItem Text="Statutory" Value="-1" />
                        <asp:ListItem Text="Internal" Value="0" />
                        <asp:ListItem Text="Event Based" Value="1" />
                        <asp:ListItem Text="Statutory CheckList" Value="2" />
                        <asp:ListItem Text="Event Based CheckList" Value="4" />
                        <asp:ListItem Text="Internal CheckList" Value="3" />
                    </asp:DropDownList>
                </div>

                <div>
                    <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select" Style="margin-right: 1%; width: 80px;">
                        <asp:ListItem Text="Risk" Value="-1" />
                        <asp:ListItem Text="High" Value="0" />
                        <asp:ListItem Text="Medium" Value="1" />
                        <asp:ListItem Text="Low" Value="2" />

                    </asp:DropDownList>
                </div>

                <div>
                    <asp:DropDownList runat="server" ID="ddlStatus" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" class="form-control m-bot15 search-select" Style="margin-right: 1%;">
                    </asp:DropDownList>
                </div>

                <div style="float: left; margin-right: 1%;">
                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationReviewer" onfocus="myFunction2()" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 310px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                        CssClass="txtbox" />
                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocationReviewer" runat="server">
                        <asp:TreeView runat="server" ID="tvFilterLocationReviewer" SelectedNodeStyle-Font-Bold="true" Width="309px" NodeStyle-ForeColor="#8e8e93"
                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                            OnSelectedNodeChanged="tvFilterLocationReviewer_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                </div>

                <%--<asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15 search-select">
                    </asp:DropDownList>--%>
                <div>
                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" OnClick="btnSearch_Click" runat="server" Text="Apply" />
                </div>

                <div>
                    <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch1" title="Search">Advanced Search</a>
                </div>

            </div>
            <%-- <div class="col-md-4 colpadding0">
                    <div class="col-md-6 colpadding0">                       
                        
                    </div>
                    <div class="col-md-6" style="margin-left: -10%;">
                      
                    </div>
                </div>--%>
        </div>
        <div>
            <!--advance search starts-->
            <div class="modal fade" id="AdvanceSearch1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 1000px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        </div>
                        <div class="modal-body">
                            <h2 style="text-align: center; margin-top: 10px;">Advanced Search</h2>

                            <div class="col-md-12 colpadding0">
                                <div class="table-advanceSearch-selectOpt">
                                    <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                                    </asp:DropDownList>
                                </div>
                                <div class="table-advanceSearch-selectOpt">
                                    <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                                    </asp:DropDownList>
                                </div>
                                <asp:Panel ID="PanelAct" runat="server">
                                    <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="Panelsubtype" runat="server">
                                    <div id="DivComplianceSubTypeList" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:DropDownList runat="server" ID="ddlComplianceSubType" class="form-control m-bot15">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="PanelSearchType" runat="server">
                                    <div id="Div2" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="Type to Filter" class="form-group form-control" ID="txtSearchType" CssClass="form-control" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="Panel1" runat="server">
                                    <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:TextBox runat="server"  Style="padding-left: 7px; " placeholder="From Date" 
                                        ID="txtStartDate"     
                                        CssClass="StartDate form-group form-control" />
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="Panel2" runat="server">
                                    <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="To Date" 
                                        ID="txtEndDate"     
                                        CssClass="StartDate form-group form-control" />
                                    </div>
                                </asp:Panel>
                                <div class="clearfix"></div>
                                <div class="table-advanceSearch-buttons">
                                    <asp:Button ID="btnAdvSearch" Text="Search" class="btn btn-search" OnClick="btnAdvSearch_Click" runat="server" OnClientClick="return hidediv();" />
                                    <button type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                    <div class="clearfix"></div>

                                </div>
                                <br />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--advance search ends-->
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 colpadding0" style="text-align: right; float: right">
            <div id="divEvent" class="col-md-10 colpadding0" style="text-align: right; float: right" runat="server" visible="false">
                <div style="float: left; margin-right: 2%;">
                    <asp:DropDownList runat="server" ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" AutoPostBack="true" class="form-control m-bot15" Style="width: 255px;">
                    </asp:DropDownList>
                </div>
                <div style="float: left; margin-right: 2%;">
                    <asp:DropDownList runat="server" ID="ddlEventNature" class="form-control m-bot15" Style="width: 255px;">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-md-12 AdvanceSearchScrum">
            <div id="divAdvSearch" runat="server" visible="false">
                <p>
                    <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Advanced Search</asp:LinkButton>
                </p>
            </div>

            <div runat="server" id="DivRecordsScrum" style="float: right;">
                <p style="padding-right: 0px !Important;">
                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                </p>
            </div>
        </div>

        <asp:GridView runat="server" ID="grdComplianceTransactions" PageSize="5" AutoGenerateColumns="false" CssClass="table" BorderWidth="0px"
            GridLines="none" AllowPaging="True" OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCreated="grdComplianceTransactions_RowCreated" OnSorting="grdComplianceTransactions_Sorting"
            DataKeyNames="ComplianceInstanceID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging">
            <HeaderStyle CssClass="clsheadergrid" />
            <RowStyle CssClass="clsROWgrid" />
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Location">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Performer">
                    <ItemTemplate>
                        <asp:Label ID="lblPerformer" runat="server" Text='<%# GetPerformer((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>

                <asp:BoundField DataField="ForMonth" HeaderText="Period" />

                <asp:TemplateField HeaderText="Due Date">
                    <ItemTemplate>
                        <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Status" HeaderText="Status" />

                <asp:BoundField DataField="Name" Visible="false" />
                <asp:BoundField DataField="Risk" Visible="false" />
                <asp:BoundField DataField="ComplianceStatusID" Visible="false" />
                <asp:BoundField DataField="ScheduledOnID" Visible="false" />
                <asp:BoundField DataField="ComplianceInstanceID" Visible="false" />
                <asp:BoundField DataField="EventName" Visible="false" />
                <asp:BoundField DataField="EventNature" Visible="false" />
                <%--<asp:BoundField DataField="Role" HeaderText="Role" />--%>
                <%--<asp:BoundField DataField="User" HeaderText="User" ItemStyle-Width="10%" />--%>
                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:UpdatePanel ID="upDownloadFileReviewer" runat="server">
                            <ContentTemplate>
                                <asp:ImageButton ID="lblOverView33" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID")  %>'
                                    OnClientClick='fComplianceOverviewReviewer(this)' ToolTip="Click to OverView"></asp:ImageButton>
                            </ContentTemplate>
                                <Triggers>
                                <asp:AsyncPostBackTrigger ControlID ="lblOverView33" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" />
            <PagerTemplate>
                <table style="display: none">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </PagerTemplate>
            <EmptyDataTemplate>
                No Records Found
            </EmptyDataTemplate>
        </asp:GridView>

        <asp:GridView runat="server" ID="grdComplianceTransactionsInt" CssClass="table" BorderWidth="0px" Visible="false" AutoGenerateColumns="false"
            GridLines="none" PageSize="5" OnRowCreated="grdComplianceTransactions_RowCreated" OnRowDataBound="grdComplianceTransactionsInt_RowDataBound"
            AllowPaging="True" OnSorting="grdComplianceTransactions_Sorting" DataKeyNames="InternalComplianceInstanceID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging">
            <HeaderStyle CssClass="clsheadergrid" />
            <RowStyle CssClass="clsROWgrid" />
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Location">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                            <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="ForMonth" HeaderText="Period" />

                <asp:TemplateField HeaderText="Due Date" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblInternalScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Performer">
                    <ItemTemplate>
                        <asp:Label ID="lblPerformer" runat="server" Text='<%# GetPerformerInternal((long)Eval("InternalComplianceInstanceID")) %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>

                <asp:BoundField DataField="Status" HeaderText="Status" />

                <asp:BoundField DataField="Risk" Visible="false" />
                <asp:BoundField DataField="InternalComplianceStatusID" Visible="false" />
                <asp:BoundField DataField="InternalScheduledOnID" Visible="false" />
                <asp:BoundField DataField="InternalComplianceInstanceID" Visible="false" />
                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:UpdatePanel ID="upDownLoadReviewerInternal" runat="server">
                            <ContentTemplate>
                                <asp:ImageButton ID="lblOverView44" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("InternalScheduledOnID")%>' instanceId='<%#Eval("InternalComplianceInstanceID")  %>'
                                    OnClientClick='fComplianceOverviewInternalReviewer(this)' ToolTip="Click to OverView"></asp:ImageButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID ="lblOverView44"/>
                            </Triggers>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" />
            <PagerTemplate>
                <table style="display: none">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </PagerTemplate>
            <EmptyDataTemplate>
                No Records Found
            </EmptyDataTemplate>
        </asp:GridView>

        <div class="col-md-12 colpadding0">
            <div class="col-md-6 colpadding0">
            </div>
            <div class="col-md-6 colpadding0">
                <div class="table-paging">
                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                    <div class="table-paging-text">
                        <p>
                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                        </p>
                    </div>

                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                </div>
            </div>
        </div>

        <tr id="trErrorMessage" runat="server" visible="true">
            <td colspan="3" style="background-color: #e9e1e1;">
                <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                    ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="TotalRowsR" runat="server" Value="0" />
            </td>
        </tr>
    </ContentTemplate>
</asp:UpdatePanel>



<script type="text/javascript">

    function OpenOverViewpupReviewer(scheduledonid, instanceid) {
        $('#divOverViewReviewer').modal('show');
        $('#OverViewsReviewer').attr('width', '1150px');
        $('#OverViewsReviewer').attr('height', '600px');
        $('.modal-dialog').css('width', '1200px');
        $('#OverViewsReviewer').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

    }

    function OpenOverViewpupInternalReviewer(scheduledonid, instanceid) {
      
        $('#divOverViewInternalReviewer').modal('show');
        $('#OverViewsInternalReviewer').attr('width', '1050px');
        $('#OverViewsInternalReviewer').attr('height', '600px');
        $('.modal-dialog').css('width', '1100px');
        $('#OverViewsInternalReviewer').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

    }
</script>

