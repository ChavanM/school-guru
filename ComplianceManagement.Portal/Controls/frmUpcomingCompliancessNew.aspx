﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="frmUpcomingCompliancessNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.frmUpcomingCompliancessNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
 
    <style type="text/css">
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 300px !important;
        }

        #grid1 .k-grid-content {
            min-height: 380px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
            margin-left: -5px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: -7px;
            background-color: white;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }

        #grid1 .k-grid-content {
            min-height: 330px !important;
        }

        .k-filter-menu .k-button {
            width: 27%;
            background-color: #E9EAEA;
            color: black;
            border: 2px solid rgba(0, 0, 0, 0.5);
        }
        
.k-treelist .k-minus {
     background: url('http://icons.iconarchive.com/icons/icojam/blue-bits/16/math-minus-icon.png')center center;
}
.k-treelist .k-plus{
     background: url('http://icons.iconarchive.com/icons/icojam/blue-bits/16/math-add-icon.png')center center;
} 

    </style>

    <title></title>

    <%if (Session["filter"] == null)
        {
            Session["filter"] = "Upcoming";
        }%>

    <script id="msgTemplate" type="text/x-kendo-template">
            #=Getmsg()#
    </script>

    <script id="StatusTemplate" type="text/x-kendo-template">                        
            #=GetPerformerStatus(ComplianceStatusID)# 
    </script>

    <script type="text/x-kendo-template" id="template"> 

    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            fhead('My Workspace');

            BindGridFilters();

            BindGrid();

            BindAdvanceGrid();

            BindAdvanceGridFilters();

            $("#txtSearchComplianceID").on('input', function (e) {
                FilterAllMain(); 
            });
            $("#txtSearchComplianceID1").on('input', function (e) {
                FilterAllAdvancedSearch();
            });
        });

        function Getmsg() {
            <%if (RoleFlagLicData == 1)%>
            <%{%>
            return "Redirect to License Workspace";
            <%}%>
            <%else
        {%>
            return "No data available";
            <%}%>           
        }

        var FlagLocation = "S";
        if (<% =ComplianceTypeID%> == 0) {
            FlagLocation = "I";
        }

        function BindAdvanceGridFilters() {

            $("#Startdatepicker").kendoDatePicker({
                change: onChange,
                format: "dd-MMM-yyyy",
            });

            $("#Lastdatepicker").kendoDatePicker({
                change: onChange,
                format: "dd-MMM-yyyy",
            });

            function onChange() {
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != undefined && $("#Startdatepicker").val() != "") {
                    $("#dropdownPastData").data("kendoDropDownList").select(4);
                    FilterAllAdvancedSearch();
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != undefined && $("#Lastdatepicker").val() != "") {
                    $("#dropdownPastData").data("kendoDropDownList").select(4);
                    FilterAllAdvancedSearch();
                }
            }

            $("#dropdownlistUserRole1").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'User', '')
                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                 <%if (PerformerFlagID == 1)%>
                    <%{%>
                    { text: "Performer", value: "3" },
                     <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "4" }
                    <%}%>
                ]
            });

            var evalEventName1 = 0;

            if ($("#dropdownEventName1").val() != '') {
                evalEventName1 = $("#dropdownEventName1").val()
            }
            $("#dropdownEventName1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Event Name', '')
                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        if ($("#dropdownEventName1").val() != '') {
                            evalEventName1 = $("#dropdownEventName1").val()
                        }
                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature1").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });

            $("#dropdownEventNature1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Event Nature', '')
                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                    }
                }
            });

            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Location', '')
                    FilterAllAdvancedSearch();

                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + FlagLocation + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + FlagLocation + ''
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdownlistRisk1").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('My Workspace', 'Filtering', 'Risk', '')
                    FilterAllAdvancedSearch();

                    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    settracknew('My Workspace', 'Filtering', 'Financial Year', '');
                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" }
                ]

            });

            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    settracknew('My Workspace', 'Filtering', 'User', '')
                    FilterAllAdvancedSearch();

                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>"
                    },
                }
            });

            $("#dropdownlistStatus1").kendoDropDownList({
                placeholder: "Status",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Status', '');
                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Upcoming", value: "0" },
                    { text: "Overdue", value: "1" },
                    { text: "Pending For Review", value: "2" },
                    { text: "Rejected", value: "3" },
                    { text: "DueButNotSubmitted", value: "4" }
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                    settracknew('My Workspace', 'Filtering', 'Compliance Type', '');

                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" }
                ]
            });

            $("#dropdownDept").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Department",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Department', '');
                    FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>"
                    }
                }
            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Act', '')
                    FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindComplianceWiseActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>"
                    }
                },
                dataBound: function (e) {
                    e.sender.list.width("790");
                }
            });
            if (<% =ComplianceTypeID%> == 0) {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>',
                            dataType: "json",
                           <%-- beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },--%>
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=I'
                    }
                });
                dataSourceSequence.read();
                //  $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
            }
            $("#dropdownlistComplianceType1").data("kendoDropDownList").value(<% =ComplianceTypeID%>);
            $("#dropdownlistUserRole1").data("kendoDropDownList").value(<% =UserRoleID%>);
            $("#dropdownlistStatus1").data("kendoDropDownList").value(<% =StatusFlagID%>);
        }

        function BindGridFilters() {

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Location', '')
                    FilterAllMain();

                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                    settracknew('My Workspace', 'Filtering', 'Status, branch etc', '')

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + FlagLocation + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                //checkboxes: true,
                //checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" }
                ],
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    settracknew('My Workspace', 'Filtering', 'Compliance Type', '')
                }
            });

            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Sequence', '')
                    FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S',
                    }
                }
            });

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('My Workspace', 'Filtering', 'Risk', '')
                    FilterAllMain();

                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownlistStatus").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();

                    settracknew('My Workspace', 'Filtering', 'Status, branch etc', '');

                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Upcoming", value: "0" },
                    { text: "Overdue", value: "1" },
                    { text: "Pending For Review", value: "2" },
                    { text: "Rejected", value: "3" },
                    { text: "DueButNotSubmitted", value: "4" },
                ]
            });

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Period', '')
                    DataBindDaynamicKendoGrid();
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });

            $("#dropdownlistMoreLink").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'More Actions ', $("#dropdownlistMoreLink").data("kendoDropDownList").text(), '');
                    gotoMoreLink();
                },
                //index: 1,
                dataSource: [
                    { text: "More Actions", value: "-1" },
                    { text: "Update Penalty", value: "1" },
                    { text: "Revise Compliance", value: "2" },
                     <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reassign Performer", value: "3" },
                     <%}%>                                          
                    <%if (PerformerFlagID == 1)%>
                    <%{%>
                    { text: "Update Tasks", value: "4" },
                     <%}%>
                    // { text: "License Workspace", value: "5" },
                    // { text: "Update Leave", value: "6" }                     
                ]
            });

            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Period', '')
                    DataBindDaynamicKendoGriddMain();
                },
                //index: 1,
                dataSource: [
                    <%if (PerformerFlagID == 1)%>
                    <%{%>
                    { text: "Performer", value: "3" },
                     <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "4" }
                    <%}%>
                ]
            });

            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Period', '')
                    DataBindDaynamicKendoGriddMain();
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });

            var evalEventName = 0;
            if ($("#dropdownEventName").val() != '') {
                evalEventName = $("#dropdownEventName").val()
            }
            $("#dropdownEventName").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Event Name', '')
                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        if ($("#dropdownEventName").val() != '') {
                            evalEventName = $("#dropdownEventName").val()
                        }

                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });

            $("#dropdownEventNature").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Event Nature', '')

                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>'
                    }
                }
            });

            $("#dropdownlistUserRole").data("kendoDropDownList").value(<% =UserRoleID%>);
            $("#dropdownlistComplianceType").data("kendoDropDownList").value(<% =ComplianceTypeID%>);
            $("#dropdownlistStatus").data("kendoDropDownList").value(<% =StatusFlagID%>);
        }

        function onSorting(arg) {
            settracknew('My Workspace', 'Sorting', arg.sort.field, '');
        }

        function onPaging(arg) {
            settracknew('My Workspace', 'Paging', arg.page, '');
        }

        function GetPerformerStatus(value) {

            if (value == "2") {
                return "Complied";
            }
            else if (value == "3") {
                return "Complied Delayed";
            }
            else if (value == "10") {
                return "In Progress";
            }
            else if (value == "15") {
                return "Not Applicable";
            }
            else if (value == "16") {
                return "Not  Complied";
            }
            else {
                return "";
            }
        }

        function selectedrejectsubmit(e) {
            e.preventDefault();
            var dataSource = $("#gridremark").data("kendoGrid").dataSource;
            var filters = dataSource.filter();
            var allData = dataSource.data();
            var query = new kendo.data.Query(allData);
            var data = query.filter(filters).data;
            var things = [];
            for (var i = 0; i < data.length; i++) {
                data[i].UserID =<% =UId%>;
                data[i].UserName ='<% =UserName%>';
                data[i].ComplianceTypeID = $("#txtCompliaceType").val();
                things.push(data[i]);
            }

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: '<% =Path%>/Data/MultipleRejectedCompliance',
                data: JSON.stringify(things),
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                },
                success: function (data) {
                    if (data[0].Message == "Save") {
                        alert('Record Save Successfully.');
                        $('#divApproveSearchModel').data("kendoWindow").close();
                        $("#grid").data("kendoGrid").dataSource.read();
                        $("#grid").data("kendoGrid").refresh();
                        $('#grid').data('kendoGrid')._selectedIds = {};
                        $('#grid').data('kendoGrid').clearSelection();
                    }
                },
                failure: function (response) {
                    $('#result').html(response);
                }
            });
        }

        function selectedsubmit(e) {

            e.preventDefault();
            var dataSource = $("#gridremark").data("kendoGrid").dataSource;
            var filters = dataSource.filter();
            var allData = dataSource.data();
            var query = new kendo.data.Query(allData);
            var data = query.filter(filters).data;

            var things = [];
            for (var i = 0; i < data.length; i++) {
                data[i].UserID =<% =UId%>;
                data[i].UserName ='<% =UserName%>';
                data[i].ComplianceTypeID = $("#txtCompliaceType").val();
                things.push(data[i]);
            }

            var url = "";

            <%if (MultipleApprove == true)%><%{%>
            url = '<% =Path%>/Data/MultipleUpdateCompliance';
            <%}%>

            <%if (LockUnlockCustomerEnable == true)%><%{%>
            url = '<% =Path%>/Data/MultipleUnlockCompliance';
            <%}%>

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: url,
                data: JSON.stringify(things),
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                },
                success: function (data) {

                    if (data[0].Message == "Save" || data[0].Message == "Already Unlocked") {
                        //if (data[0].Message == "Already Unlocked") {
                        //    alert('Record Already Unlocked.');
                        //}
                        //if (data[0].Message == "Save") {
                        //    alert('Record Save Successfully.');
                        //}
                        alert('Record Save Successfully.');
                        $('#divApproveSearchModel').data("kendoWindow").close();
                        $("#grid").data("kendoGrid").dataSource.read();
                        $("#grid").data("kendoGrid").refresh();
                        $('#grid').data('kendoGrid')._selectedIds = {};
                        $('#grid').data('kendoGrid').clearSelection();
                    }
                },
                failure: function (response) {
                    $('#result').html(response);
                }
            });
        }

        function youFunction() {
            editAll();
            var theGrid = $("#gridremark").data("kendoGrid");
            var gridData = theGrid.dataSource._data;

            for (var i = 0; i < gridData.length; i++) {
                gridData[i].remark = $("#txtremark").val();
            }
            $("#gridremark").data("kendoGrid").refresh();
        }

        function gotoMoreLink() {

            //update Penalty
            if ($("#dropdownlistMoreLink").val() == 1) {
                if ($("#dropdownlistUserRole").val() == 3) {
                    window.location.href = "../Penalty/PenaltyUpdation.aspx";
                }
                else {
                    window.location.href = "../Penalty/PenaltyUpdationReviewer.aspx";
                }
            }

            //Revise Compliance
            else if ($("#dropdownlistMoreLink").val() == 2) {
                if ($("#dropdownlistComplianceType").val() == 0) {
                    window.location.href = "../InternalCompliance/ReviseCompliancesInternal.aspx";
                }
                else {
                    window.location.href = "../Compliances/ReviseCompliances.aspx";
                }
            }

            //License Workspace
            else if ($("#dropdownlistMoreLink").val() == 5) {
                window.location.href = "../Compliances/ComplianceLicenseList.aspx";
            }

            //Reassign Performer(only for Reviewer)
            else if ($("#dropdownlistMoreLink").val() == 3) {
                if ($("#dropdownlistUserRole").val() == 4) {
                    window.location.href = "../Compliances/ReassignReviewerToPerformer.aspx";
                }
                else { }
            }

            //Task(only for Perfomer)
            else if ($("#dropdownlistMoreLink").val() == 4) {
                if ($("#dropdownlistUserRole").val() == 3) {
                    window.location.href = "../Task/myTask.aspx?type=Statutory";
                }
                else { }
            }

            //My Leave
            //else if ($("#dropdownlistMoreLink").val() == 6)
            //{
            //    window.location.href = "../Users/PerformerOnLeave.aspx";
            //}           
        }

        $(document).on("click", "#sel_chkbx", function (e) {
            if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                <%if (MultipleApprove == true)%><%{%>
                $('#dvApproverreject').css('display', 'none');
                <%}%>
                <%if (LockUnlockCustomerEnable == true)%><%{%>
                $('#dvUnlock').css('display', 'none');
                <%}%>
            }
            else {
                <%if (MultipleApprove == true)%><%{%>
                $('#dvApproverreject').css('display', 'block');
                <%}%>
                 <%if (LockUnlockCustomerEnable == true)%><%{%>
                $('#dvUnlock').css('display', 'block');
                document.getElementById('dvUnlock').style = "height:30px";
                <%}%>
            }
            return true;
        });

        $(document).on("click", "#chkAll", function (e) {

            if ($('input[id=chkAll]').prop('checked')) {
                $('input[name="sel_chkbx"]').attr("checked", false);
                $('input[name="sel_chkbx"]').each(function (i, e) {
                    e.click();
                });
            }
            else {
                $('input[name="sel_chkbx"]').attr("checked", false);
            }
            if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                <%if (MultipleApprove == true)%><%{%>
                $('#dvApproverreject').css('display', 'none');
                <%}%>
                 <%if (LockUnlockCustomerEnable == true)%><%{%>
                $('#dvUnlock').css('display', 'none');
                <%}%>
            }
            else {
                <%if (MultipleApprove == true)%><%{%>
                $('#dvApproverreject').css('display', 'block');
                <%}%>
                 <%if (LockUnlockCustomerEnable == true)%><%{%>
                $('#dvUnlock').css('display', 'block');
                document.getElementById('dvUnlock').style = "height:30px";
                <%}%>
            }
            return true;
        });

        $(document).on("click", "#sel_chkbxADV", function (e) {
            if (($('input[name="sel_chkbxADV"]:checked').length) == 0) {

                <%if (MultipleApprove == true)%><%{%>
                $('#dvApproverrejectADV').css('display', 'none');
                <%}%>
                <%if (LockUnlockCustomerEnable == true)%><%{%>
                $('#divUnlockADV').css('display', 'none');
                <%}%>
            }
            else {
                <%if (MultipleApprove == true)%><%{%>
                $('#dvApproverrejectADV').css('display', 'block');
                <%}%>
                <%if (LockUnlockCustomerEnable == true)%><%{%>
                $('#divUnlockADV').css('display', 'block');
                <%}%>
            }
            return true;
        });

        $(document).on("click", "#chkAllADV", function (e) {

            if ($('input[id=chkAllADV]').prop('checked')) {
                $('input[name="sel_chkbxADV"]').attr("checked", false);
                $('input[name="sel_chkbxADV"]').each(function (i, e) {
                    e.click();
                });
            }
            else {

                $('input[name="sel_chkbxADV"]').attr("checked", false);
            }
            if (($('input[name="sel_chkbxADV"]:checked').length) == 0) {

                 <%if (MultipleApprove == true)%><%{%>
                $('#dvApproverrejectADV').css('display', 'none');
                <%}%>
                <%if (LockUnlockCustomerEnable == true)%><%{%>
                $('#divUnlockADV').css('display', 'none');
                <%}%>
            }
            else {
                <%if (MultipleApprove == true)%><%{%>
                $('#dvApproverrejectADV').css('display', 'block');
                <%}%>
                <%if (LockUnlockCustomerEnable == true)%><%{%>
                $('#divUnlockADV').css('display', 'block');
                <%}%>
            }
            return true;
        });

        function Bindremarkgrid(gridid) {

            var CTypeID = -1;
            $("#txtCompliaceType").val('');
            if (gridid == 0) {
                CTypeID = $('#dropdownlistComplianceType').val();
                $("#txtCompliaceType").val(CTypeID);
            }
            else {
                CTypeID = $('#dropdownlistComplianceType1').val();
                $("#txtCompliaceType").val(CTypeID);
            }
            var gridclear = $('#gridremark').data("kendoGrid");
            if (gridclear != undefined || gridclear != null)
                $('#gridremark').empty();

            var grid1 = $("#gridremark").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=' + CTypeID + '&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=<% =UserRoleID%>&MonthId=All&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                remark: { validation: { required: true, } }
                            }
                        },
                        data: function (response) {
                            if (CTypeID == -1) {
                                return response[0].Statutory;
                            }
                            else if (CTypeID == 0) {
                                return response[0].Internal;
                            }
                            else if (CTypeID == 1) {
                                return response[0].EventBased;
                            }
                        },
                        total: function (response) {
                            if (CTypeID == -1) {
                                return response[0].Statutory.length;
                            }
                            else if (CTypeID == 0) {
                                return response[0].Internal.length;
                            }
                            else if (CTypeID == 1) {
                                return response[0].EventBased.length;
                            }
                        },
                    },
                    //pageSize: 10,
                },
                sortable: true,
                filterable: false,
                groupable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                persistSelection: true,
                multi: true,
                editable: false,
                messages: {
                    noRecords: "No records found"
                },
                multi: false,
                selectable: false,
                pageable: false,
                dataBound: function (e) {
                    editAll();
                },
                columns: [
                    {
                        field: "Branch", title: 'Location',
                        width: "18.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "18.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%;",
                    },
                    {
                        template: "<input data-bind='value:remark' style='width: 100%;'  class='k-textbox' />",
                        title: "Remark",
                        width: "18.7%;",
                    },
                ]
            });

            var items = [];
            if (gridid == 0) {
                var grid = $("#grid").data("kendoGrid");
                var sel = $("input:checked", grid.tbody).closest("tr");
                $.each(sel, function (idx, row) {
                    var item = grid.dataItem(row);
                    items.push(item.ScheduledOnID);
                });
            }
            if (gridid == 1) {
                var grid = $("#grid1").data("kendoGrid");
                var sel = $("input:checked", grid.tbody).closest("tr");
                $.each(sel, function (idx, row) {
                    var item = grid.dataItem(row);
                    items.push(item.ScheduledOnID);
                });
            }

            var finalSelectedfilter = { logic: "and", filters: [] };
            var InstancecIdFilter = { logic: "or", filters: [] };
            $.each(items, function (i, v) {
                InstancecIdFilter.filters.push({
                    field: "ScheduledOnID", operator: "eq", value: parseInt(v)
                });
            });
            finalSelectedfilter.filters.push(InstancecIdFilter);
            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#gridremark").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);
            }
            else {
                $("#gridremark").data("kendoGrid").dataSource.filter({});
            }
            $("#txtremark").val('');

            $("#gridremark").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridremark").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function editAll() {
            var theGrid = $("#gridremark").data("kendoGrid");
            $("#gridremark tbody").find('tr').each(function () {
                var model = theGrid.dataItem(this);
                kendo.bind(this, model);
            });
            $("#gridremark").focus();
        }

        function BindGrid() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>&StatusFlagID=<% =StatusFlagID%>&FlagPR=<% =UserRoleID%>&MonthId=All&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                StatusChangedOn: { type: "date" }
                            }
                        },
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == -1) {
                                return response[0].Statutory;
                            }
                            else if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Internal;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].EventBased;
                            }
                        },
                        total: function (response) {
                            if (<% =ComplianceTypeID%> == -1) {
                                return response[0].Statutory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Internal.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].EventBased.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                noRecords: true,
                noRecords: {
                    template: kendo.template($('#msgTemplate').html())
                },
                //messages: {
                //    noRecords: "No records found"
                //},
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting,
                page: onPaging,
                pageable: {
                    pageSizes: ['All', 5, 10, 20],
                    buttonCount: 3,
                    numeric: true,
                    pageSize: 10
                },
                dataBound: OnGridDataBound,
                columns: [
                    <%if (LockUnlockCustomerEnable == true && UserRoleID==3)%><%{%>
                    {
                        //template: "#if (Status == 'Overdue' || Status=='Complied But Document Pending' || Status=='Interim Review Approved' || Status=='	Submitted For Interim Review') {# <input name='sel_chkbx' id='sel_chkbx' type='checkbox' > #} #",
                        template: "#if (Status == 'Overdue' || ((ComplianceStatusID == 1 || ComplianceStatusID == 10 || ComplianceStatusID == 12 || ComplianceStatusID == 13 || ComplianceStatusID == 19) && ScheduledOn < kendo.date.today())) {# <input name='sel_chkbx' id='sel_chkbx' type='checkbox' > #} #",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAll' />",
                        width: "5%;",
                    },
                    <%}%>
                    <%if (MultipleApprove == true)%><%{%>
                    {
                        template: "<input name='sel_chkbx' id='sel_chkbx' type='checkbox' >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAll' />",
                        width: "5%;",
                    },
                    <%}%>
                    {
                        field: "Branch", title: 'Location',
                        width: "18.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "30%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "25%"
                    },
                    {
                        field: "ShortForm", title: 'Short Form',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "CloseDate", title: "Close Date",
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "13%"
                    },
                    {
                        hidden: true,
                        field: "state", title: 'State',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        hidden: true,
                        field: "region", title: 'Region',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        hidden: true,
                        field: "zone", title: 'Zone',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },

                    {
                        hidden: true,
                        field: "RiskCategory", title: "Risk",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        hidden: true,
                        field: "CustomerBranchID", title: "Branch ID",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        hidden: true,
                        field: "Performer", title: "Performer",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        hidden: true,
                        field: "Reviewer", title: "Reviewer",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                     <%if (MultipleApprove == true)%><%{%>
                    {
                        field: "ComplianceStatusID", title: "Performer Status",
                        template: kendo.template($('#StatusTemplate').html()), width: "12%"
                    },
                    {
                        field: "StatusChangedOn", title: 'Performed On',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "14%"
                    },
                    <%}%>
                    {
                        command: [
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-edit", className: "ob-overview",
                            },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewnew" }
                        ], title: "Action", lock: true, width: "7%;", headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });

            function OnGridDataBound(e) {
                var grid = $("#grid").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;

                   <%-- $("#grid").data("kendoGrid").hideColumn(18);
                    $("#grid").data("kendoGrid").hideColumn(19);
                    //$("#grid").data("kendoGrid").showColumn(7);
                      <%if (MultipleApprove == true)%><%{%>
                    $("#grid").data("kendoGrid").hideColumn(0);
                      <%}%>--%>
                    if ($("#dropdownlistUserRole").val() == 3) {
                        if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1) {
                            var d = new Date();
                            if (gridData[i].Status == "Upcoming" && kendo.parseDate(gridData[i].ScheduledOn, 'yyyy-MM-dd') > d || gridData[i].Status == "Submitted For Interim Review" || gridData[i].Status == "Interim Review Approved" || gridData[i].Status == "Interim Rejected") {
                                if (gridData[i].Interimdays != null && gridData[i].Interimdays != undefined) {
                                    if (gridData[i].Interimdays != 0) {
                                        var row = grid.table.find("tr[data-uid='" + currentUid + "']");
                                        $.each(row.find('td'), function (index, element) {
                                            $(element).addClass("change-condition");
                                        });
                                    }
                                }
                            }
                        }
                        if (gridData[i].Status == "Upcoming" || gridData[i].Status == "Overdue" || gridData[i].Status == "Rejected" || gridData[i].Status == "Complied But Document Pending" || gridData[i].Status == "Interim Review Approved" || gridData[i].Status == "Interim Rejected") {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overview");
                            createUserButton.hide();
                        }
                        if (gridData[i].Status == "Overdue" || gridData[i].Status == "Complied But Document Pending") {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewnew");
                            createUserButton.hide();
                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewnew");
                            createUserButton.hide();
                        }

                    }
                    if ($("#dropdownlistUserRole").val() == 4) {
                        if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1) {
                            var d = new Date();
                            if (gridData[i].Status == "Submitted For Interim Review" && kendo.parseDate(gridData[i].ScheduledOn, 'yyyy-MM-dd') > d) {
                                if (gridData[i].Interimdays != null && gridData[i].Interimdays != undefined) {
                                    if (gridData[i].Interimdays != 0) {
                                        var row = grid.table.find("tr[data-uid='" + currentUid + "']");
                                        $.each(row.find('td'), function (index, element) {
                                            $(element).addClass("change-condition");
                                        });
                                    }
                                }
                            }
                        }
                        if (gridData[i].Status == "Submitted For Interim Review" || gridData[i].Status == "Pending For Review" || gridData[i].Status == "Revise Compliance") {
                     <%--       <%if (MultipleApprove == true)%><%{%>
                            $("#grid").data("kendoGrid").showColumn(0);
                            $("#grid").data("kendoGrid").showColumn(18);
                            $("#grid").data("kendoGrid").showColumn(19);
                            $("#grid").data("kendoGrid").hideColumn(7);
                            $("#grid").data("kendoGrid").hideColumn(3);
                            <%}%>--%>
                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overview");
                            createUserButton.hide();
                        }
                        if (gridData[i].Status == "Overdue" || gridData[i].Status == "Complied But Document Pending") {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewnew");
                            createUserButton.hide();
                        }
                    }
                }
            }

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Edit";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Overview";
                }
            });

            $("#grid").kendoTooltip({
                filter: ("th:nth-child(n+2)"),
                //filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid").kendoTooltip({
                filter: ("td:nth-child(n+2)"),
                //filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID, item.Status);
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-overviewnew", function (e) {

                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupnew(item.ScheduledOnID, item.ComplianceInstanceID, item.Status);
                return true;
            });
        }

        function BindAdvanceGrid() {
            $(".k-grid1-content tbody[role='rowgroup'] tr[role='row'] td:first-child").prepend('<span class="k-icon k-i-filter"</span>');

            var grid1 = $("#grid1").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>&StatusFlagID=<% =StatusFlagID%>&FlagPR=<% =UserRoleID%>&MonthId=All&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                StatusChangedOn: { type: "date" },
                            }
                        },
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == -1) {
                                return response[0].Statutory;
                            }
                            else if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Internal;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].EventBased;
                            }
                        },
                        total: function (response) {
                            if (<% =ComplianceTypeID%> == -1) {
                                return response[0].Statutory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Internal.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].EventBased.length;
                            }
                        }
                    },
                    pageSize: 10,
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                noRecords: true,
                //messages: {
                //    noRecords: "No records found"
                //},
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting,
                page: onPaging,
                noRecords: {
                    template: kendo.template($('#msgTemplate').html())
                },
                pageable: {
                    pageSizes: ['all', 5, 10, 20],
                    buttonCount: 3,
                    numeric: true,
                    pageSize: 10
                },
                dataBinding: function () {
         
                },
                dataBound: OnGridDataBoundAdvanced,
                columns: [
                    <%if (LockUnlockCustomerEnable == true && UserRoleID==3)%><%{%>
                    {
                        template: "#if (Status == 'Overdue' || ((ComplianceStatusID == 1 || ComplianceStatusID == 10 || ComplianceStatusID == 12 || ComplianceStatusID == 13 || ComplianceStatusID == 19) && ScheduledOn < kendo.date.today())) {# <input name='sel_chkbxADV' id='sel_chkbxADV' type='checkbox' > #} #",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAllADV' />",
                        width: "5%;"//,
                    },
                    <%}%>
                    <%if (MultipleApprove == true)%><%{%>
                    {
                        template: "<input name='sel_chkbxADV' id='sel_chkbxADV' type='checkbox' >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAllADV' />",
                        width: "5%;"//,
                    },
                    <%}%>
                    {
                        hidden: true,
                        field: "RiskCategory", title: "Risk",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        hidden: true,
                        field: "CustomerBranchID", title: "Branch ID",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        hidden: true,
                        field: "Performer", title: "Performer",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        hidden: true,
                        field: "Reviewer", title: "Reviewer",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        field: "Branch", title: 'Location',
                        width: "18.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "30%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "25%"
                    },
                    {
                        field: "ShortForm", title: 'Short Form',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "is equal to",
                                    neq: "is not equal to",
                                    contains: "contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "CloseDate", title: "Close Date",
                        type: "date",
                        hidden: true,
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        hidden: true,
                        field: "state", title: 'State',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        hidden: true,
                        field: "region", title: 'Region',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        hidden: true,
                        field: "zone", title: 'Zone',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    <%if (MultipleApprove == true)%><%{%>
                    {
                        field: "ComplianceStatusID", title: "Performer Status",
                        template: kendo.template($('#StatusTemplate').html()), width: "12%"
                    },
                    {
                        field: "StatusChangedOn", title: 'Performed On',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "14%"
                    },
                    <%}%>
                    {
                        command: [
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-edit", className: "ob-overviewMain"

                            },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMainnew" }

                        ], title: "Action", lock: true, width: 120, headerAttributes: {
                            style: "text-align: center;"
                        }

                    }
                ]
            });

            function OnGridDataBoundAdvanced(e) {
                var grid = $("#grid1").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                 <%--   $("#grid1").data("kendoGrid").hideColumn(18);
                    $("#grid1").data("kendoGrid").hideColumn(19);
                    <%if (MultipleApprove == true)%><%{%>
                    $("#grid1").data("kendoGrid").hideColumn(0);
                    <%}%>--%>

                    if ($("#dropdownlistUserRole1").val() == 3) {
                        if ($("#dropdownlistComplianceType1").val() == -1 || $("#dropdownlistComplianceType1").val() == 1) {
                            var d = new Date();
                            if (gridData[i].Status == "Upcoming" && kendo.parseDate(gridData[i].ScheduledOn, 'yyyy-MM-dd') > d || gridData[i].Status == "Submitted For Interim Review" || gridData[i].Status == "Interim Review Approved" || gridData[i].Status == "Interim Rejected") {
                                if (gridData[i].Interimdays != null && gridData[i].Interimdays != undefined) {
                                    if (gridData[i].Interimdays != 0) {
                                        var row = grid.table.find("tr[data-uid='" + currentUid + "']");
                                        $.each(row.find('td'), function (index, element) {
                                            $(element).addClass("change-condition");
                                        });
                                    }
                                }
                            }
                        }
                        if (gridData[i].Status == "Upcoming" || gridData[i].Status == "Overdue" || gridData[i].Status == "Complied But Document Pending" || gridData[i].Status == "Rejected" || gridData[i].Status == "Interim Review Approved" || gridData[i].Status == "Interim Rejected") {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMain");
                            createUserButton.hide();
                        }
                        if (gridData[i].Status == "Overdue" || gridData[i].Status == "Complied But Document Pending") {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMainnew");
                            createUserButton.hide();
                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMainnew");
                            createUserButton.hide();
                        }

                    }
                    if ($("#dropdownlistUserRole1").val() == 4) {
                        if ($("#dropdownlistComplianceType1").val() == -1 || $("#dropdownlistComplianceType1").val() == 1) {
                            var d = new Date();
                            if (gridData[i].Status == "Submitted For Interim Review" && kendo.parseDate(gridData[i].ScheduledOn, 'yyyy-MM-dd') > d) {
                                if (gridData[i].Interimdays != null && gridData[i].Interimdays != undefined) {
                                    if (gridData[i].Interimdays != 0) {
                                        var row = grid.table.find("tr[data-uid='" + currentUid + "']");
                                        $.each(row.find('td'), function (index, element) {
                                            $(element).addClass("change-condition");
                                        });
                                    }
                                }
                            }
                        }
                        if (gridData[i].Status == "Submitted For Interim Review" || gridData[i].Status == "Pending For Review" || gridData[i].Status == "Revise Compliance") {
                      <%--    <%if (MultipleApprove == true)%><%{%>
                            $("#grid1").data("kendoGrid").showColumn(0);
                            $("#grid1").data("kendoGrid").showColumn(18);
                            $("#grid1").data("kendoGrid").showColumn(19);
                            $("#grid1").data("kendoGrid").hideColumn(7);
                            $("#grid1").data("kendoGrid").hideColumn(10);
                        <%}%>--%>
                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMain");
                            createUserButton.hide();
                        }
                        if (gridData[i].Status == "Overdue" || gridData[i].Status == "Complied But Document Pending") {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMainnew");
                            createUserButton.hide();
                        }


                    }
                }
            }

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Edit";
                }
            });

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "overview";
                }
            });

            var CheckShowTooltip = "th";

            <%if (MultipleApprove == true || LockUnlockCustomerEnable == true)%><%{%>
            CheckShowTooltip = ("th:nth-child(n+2)");
            <%}%>

            $("#grid1").kendoTooltip({
                filter: CheckShowTooltip,
                //filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid1").kendoTooltip({
                filter: CheckShowTooltip,
                //filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $(document).on("click", "#grid1 tbody tr .ob-overviewMain", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID, item.Status);
                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-overviewMainnew", function (e) {

                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupnewone(item.ScheduledOnID, item.ComplianceInstanceID, item.Status);
                return true;
            });

        }

        function OpenAllApprove(e, param, grdid) {

            if (grdid == 0) {
                if (param == 1) {
                    $('#dvsubmitforreject').css('display', 'none');
                    $('#dvsubmit').css('display', 'block');
                }
                else {
                    $('#dvsubmit').css('display', 'none');
                    $('#dvsubmitforreject').css('display', 'block');
                }
            }
            if (grdid == 1) {
                if (param == 1) {
                    $('#dvsubmitforreject').css('display', 'none');
                    $('#dvsubmit').css('display', 'block');
                }
                else {
                    $('#dvsubmit').css('display', 'none');
                    $('#dvsubmitforreject').css('display', 'block');
                }
            }
            Bindremarkgrid(grdid);
            if (grdid == 1) {
                $("#divApproveSearchModel").kendoWindow({
                    modal: true,
                    width: "60%",
                    height: "75%",
                    title: "Add Remark",
                    visible: false,
                    draggable: false,
                    refresh: true,
                    pinned: true,
                    actions: [
                        "Close"
                    ], close: CloseAdvancePopup

                }).data("kendoWindow").open().center();
            }
            if (grdid == 0) {
                $("#divApproveSearchModel").kendoWindow({
                    modal: true,
                    width: "60%",
                    height: "75%",
                    title: "Add Remark",
                    visible: false,
                    draggable: false,
                    refresh: true,
                    pinned: true,
                    actions: [
                        "Close"
                    ], close: CloseMainPopup

                }).data("kendoWindow").open().center();
            }

            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearch(e) {

            $("#divAdvanceSearchModel").kendoWindow({
                modal: true,
                pinned: true,
                width: "97%",
                height: "93%",
                title: "Advanced Search",
                visible: false,
                draggable: false,
                refresh: true,
                actions: [
                    "Close"
                ], close: CloseAdvancePopup

            }).data("kendoWindow").open().center();;
            e.preventDefault();
            return false;
        }

        function CloseAdvancePopup() {
            $("#Clearfilter").click();
        }
        function CloseMainPopup() {
            $("#ClearfilterMain").click();
        }

        function ClearAllFilter(e) {
            $("#txtSearchComplianceID1").val('');
            $("#dropdownEventName1").data("kendoDropDownList").select(0);
            $("#dropdownEventNature1").data("kendoDropDownList").select(0);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdownPastData").data("kendoDropDownList").select(4);
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownList").select(0);
            $("#dropdownlistStatus1").data("kendoDropDownList").select(2);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $('#filterStartDate').css('display', 'none');
            $('#filterLastDate').css('display', 'none');
            $('#dvbtndownloadDocument').css('display', 'none');
            $("#grid1").data("kendoGrid").dataSource.filter({});
            $('input[id=chkAllADV]').prop('checked', false);
            <%if (MultipleApprove == true)%><%{%>
            $('#dvApproverrejectADV').css('display', 'none');
            <%}%>
            <%if (LockUnlockCustomerEnable == true && UserRoleID==3)%><%{%>
            $('#divUnlockADV').css('display', 'none');
            <%}%>
            $("#grid1").data("kendoGrid").dataSource.filter({});
            FilterAllAdvancedSearch();
            // e.preventDefault();
        }

        function CloseModalInternalPerformer() {
            $('#ComplainceInternalPerformaer').modal('hide');
            return true;
        }

        function ClearAllFilterMain(e) {
            $("#txtSearchComplianceID").val('');
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            // $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $('#dvbtndownloadDocumentMain').css('display', 'none');
            $("#dropdownEventName").data("kendoDropDownList").select(0);
            $("#dropdownEventNature").data("kendoDropDownList").select(0);
            $("#grid").data("kendoGrid").dataSource.filter({});
            $('input[id=chkAll]').prop('checked', false);
            <%if (MultipleApprove == true)%><%{%>
            $('#dvApproverreject').css('display', 'none');
            <%}%>
            <%if (LockUnlockCustomerEnable == true && UserRoleID==3)%><%{%>
            $('#dvUnlock').css('display', 'none');
            <%}%>
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.pageSize(10);
            e.preventDefault();
            return false;
        }

        function DataBindDaynamicKendoGriddMain() {
            $('#dvdropdownEventName').css('display', 'none');
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $('#dvbtndownloadDocumentMain').css('display', 'none');
            $("#grid").data('kendoGrid').dataSource.data([]);
            if ($("#dropdownlistComplianceType").val() == -1) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=-1&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=-1&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY='
                    },
                    schema: {
                        data: function (response) {
                            return response[0].Statutory;
                        },
                        total: function (response) {
                            return response[0].Statutory.length;
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                StatusChangedOn: { type: "date" }
                            }
                        },
                    },
                    pageSize: 10
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            else if ($("#dropdownlistComplianceType").val() == 1) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=1&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=1&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY='
                    },
                    schema: {
                        data: function (response) {
                            return response[0].EventBased;
                        },
                        total: function (response) {
                            return response[0].EventBased.length;
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                StatusChangedOn: { type: "date" }
                            }
                        },
                    },
                    pageSize: 10
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            else if ($("#dropdownlistComplianceType").val() == 0) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=0&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].Internal;
                        },
                        total: function (response) {
                            return response[0].Internal.length;
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                StatusChangedOn: { type: "date" }
                            }
                        },
                    },
                    pageSize: 10
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }

            $("#dropdownEventName").data("kendoDropDownList").select(0);
            $("#dropdownEventNature").data("kendoDropDownList").select(0);
            if ($("#dropdownlistComplianceType").val() == 1) {
                $('#dvdropdownEventName').css('display', 'block');

                //$("#grid").data("kendoGrid").showColumn(5);
                //$("#grid").data("kendoGrid").showColumn(6);

                //$("#grid").data("kendoGrid").hideColumn(4);
            }
            else {
                //$("#grid").data("kendoGrid").hideColumn(5);
                //$("#grid").data("kendoGrid").hideColumn(6);
                //$("#grid").data("kendoGrid").showColumn(4);
            }


            if ($("#dropdownlistComplianceType").val() == 0)//Internal
            {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
            }
            else {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
            }

        }

        function DataBindDaynamicKendoGrid() {

            $("#grid1").data('kendoGrid').dataSource.data([]);

            if ($("#dropdownFY").val() != "0") {
                $("#dropdownPastData").data("kendoDropDownList").select(4);
            }

            <%if (RoleFlag == 1)%>
            <%{%>
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            <%}%>
            $('input[id=chkAll]').prop('checked', false);

            $("#grid1").data("kendoGrid").dataSource.filter({});
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $('#filterStartDate').css('display', 'none');
            $('#filterLastDate').css('display', 'none');
            //$('#Clearfilter').css('display', 'none');
            $('#dvbtndownloadDocument').css('display', 'none');

            $("#dvdropdownACT").css('display', 'block');
            if ($("#dropdownlistComplianceType1").val() == 0)//Internal and Internal Checklist
            {
                $("#dvdropdownACT").css('display', 'none');
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);

                var dataSourceDept = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=0"
                    },
                });
                dataSourceDept.read();
                $("#dropdownDept").data("kendoDropDownList").setDataSource(dataSourceDept);
            }
            else {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);

                var dataSourceDept = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=-1"
                    },
                });
                dataSourceDept.read();
                $("#dropdownDept").data("kendoDropDownList").setDataSource(dataSourceDept);
            }

            if ($("#dropdownlistComplianceType1").val() == 1)//event based
            {
                $('#dvdropdownEventNature1').css('display', 'block');
                $('#dvdropdownEventName1').css('display', 'block');

                //$("#grid1").data("kendoGrid").showColumn(5);//Event Name
                //$("#grid1").data("kendoGrid").showColumn(6);//Event Nature
                //$("#grid1").data("kendoGrid").hideColumn(4);//Branch
            }
            else {
                $('#dvdropdownEventNature1').css('display', 'none');
                $('#dvdropdownEventName1').css('display', 'none');

                //$("#grid1").data("kendoGrid").hideColumn(5);//Event Name
                //$("#grid1").data("kendoGrid").hideColumn(6);//Event Nature
                //$("#grid1").data("kendoGrid").showColumn(4);//Branch
            }

            if ($("#dropdownlistComplianceType1").val() == -1) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=-1&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=-1&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                    },
                    schema: {
                        data: function (response) {
                            return response[0].Statutory;
                        },
                        total: function (response) {
                            return response[0].Statutory.length;
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                StatusChangedOn: { type: "date" }
                            }
                        },
                    },
                    pageSize: 10,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            if ($("#dropdownlistComplianceType1").val() == 0) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=0&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=0&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                    },
                    schema: {
                        data: function (response) {
                            return response[0].Internal;
                        },
                        total: function (response) {
                            return response[0].Internal.length;
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                StatusChangedOn: { type: "date" }
                            }
                        },
                    },
                    pageSize: 10,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            if ($("#dropdownlistComplianceType1").val() == 1) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=1&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=1&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                    },
                    schema: {
                        data: function (response) {
                            return response[0].EventBased;
                        },
                        total: function (response) {
                            return response[0].EventBased.length;
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                StatusChangedOn: { type: "date" }
                            }
                        },
                    },
                    pageSize: 10,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }

            if ($("#dropdownlistComplianceType1").val() == 0) {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=I'
                    }
                });
                dataSourceSequence.read();
                //    $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
            }
            else {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S'
                    }
                });
                dataSourceSequence.read();

                <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
                  <%}%>

            }
        }

        function CloseModalPerFormer() {

            $('#ComplaincePerformer').modal('hide');

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

            // DataBindDaynamicKendoGriddMain();
            //DataBindDaynamicKendoGrid();
            return true;
        }

        function CloseModalReviewer() {

            $('#ComplainceReviewer').modal('hide');

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

            //DataBindDaynamicKendoGriddMain();
            //DataBindDaynamicKendoGrid();
            return true;
        }

        function CloseModalInternalReviewer() {
            $('#ComplainceInternalReviewer').modal('hide');

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

            //DataBindDaynamicKendoGriddMain();
            //DataBindDaynamicKendoGrid();
            return true;
        }

        function CloseModalInternalPerformer() {

            $('#ComplainceInternalPerformaer').modal('hide');

            $('#ComplaincePerformer').modal('hide');

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

            //DataBindDaynamicKendoGriddMain();
            //DataBindDaynamicKendoGrid();
            return true;
        }

        var ComplianceType = "";

        function OpenOverViewpupMain(scheduledonid, instanceid, Status) {
            settracknew('My Workspace', 'Action', 'Review', 'UserID');
            ComplianceType = "";
            if ($("#dropdownlistUserRole1").val() == 3) {
                if ($("#dropdownlistComplianceType1").val() == -1 || $("#dropdownlistComplianceType1").val() == 1)
                {
                    if ($("#dropdownlistComplianceType1").val() == -1) {
                        ComplianceType = "S";
                    }
                    if ($("#dropdownlistComplianceType1").val() == 1) {
                        ComplianceType = "EB";
                    }
                    if (Status == "Upcoming" || Status == "Overdue" || Status == "Rejected" || Status == "Interim Review Approved" || Status == "Interim Rejected") {
                        $('#ComplaincePerformer1').modal('show');
                        $('#iPerformerFrame1').attr('src', '/controls/compliancestatusperformer.aspx?sId=' + scheduledonid + '&InId=' + instanceid + '&Status=' + Status + '&ComplainceType=' + ComplianceType);
                    }
                }
                if ($("#dropdownlistComplianceType1").val() == 0) {
                    if ($("#dropdownlistComplianceType1").val() == 0) {
                        ComplianceType = "I";
                    }
                    if (Status == "Upcoming" || Status == "Overdue" || Status == "Rejected") {
                        $('#ComplainceInternalPerformaer1').modal('show');
                        $('#iInternalPerformerFrame1').attr('src', '/controls/InternalComplianceStatusperformer.aspx?sId=' + scheduledonid + '&InId=' + instanceid + '&Status=' + Status + '&ComplainceType=' + ComplianceType);
                    }
                }
            }
            if ($("#dropdownlistUserRole1").val() == 4) {
                if ($("#dropdownlistComplianceType1").val() == -1 || $("#dropdownlistComplianceType1").val() == 1) {
                    if (Status == "Submitted For Interim Review" || Status == "Pending For Review" || Status == "Revise Compliance") {
                        $('#ComplainceReviewer1').modal('show');
                        $('#iReviewerFrame1').attr('src', '/controls/compliancestatusreviewer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType1").val() == 0) {
                    if (Status == "Pending For Review" || Status == "Revise Compliance") {
                        $('#ComplainceInternalReviewer1').modal('show');
                        $('#iInternalReviewerFrame1').attr('src', '/controls/InternalComplianceStatusReviewer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
            }
        }

        function OpenOverViewpup(scheduledonid, instanceid, Status) {
            settracknew('My Workspace', 'Action', $("#dropdownlistUserRole").data("kendoDropDownList").text(),<%=UId%>);
            ComplianceType = ""
            if ($("#dropdownlistUserRole").val() == 3) {
                if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1) {
                    if ($("#dropdownlistComplianceType").val() == -1) {
                        ComplianceType = "S";
                    }
                    if ($("#dropdownlistComplianceType").val() == 1) {
                        ComplianceType = "EB";
                    }
                    if (Status == "Upcoming" || Status == "Overdue" || Status == "Rejected" || Status == "Interim Review Approved" || Status == "Interim Rejected" || Status == "Complied But Document Pending") {
                        $('#ComplaincePerformer').modal('show');
                        $('#iPerformerFrame').attr('src', '/controls/compliancestatusperformer.aspx?sId=' + scheduledonid + '&InId=' + instanceid + '&Status=' + Status + '&ComplainceType=' + ComplianceType);
                    }
                }
                if ($("#dropdownlistComplianceType").val() == 0) {
                    if ($("#dropdownlistComplianceType").val() == 0) {
                        ComplianceType = "I";
                    }
                    if (Status == "Upcoming" || Status == "Overdue" || Status == "Rejected" || Status == "Complied But Document Pending") {
                        $('#ComplainceInternalPerformaer').modal('show');
                        $('#iInternalPerformerFrame').attr('src', '/controls/InternalComplianceStatusperformer.aspx?sId=' + scheduledonid + '&InId=' + instanceid + '&Status=' + Status + '&ComplainceType=' + ComplianceType);
                    }
                }
            }
            if ($("#dropdownlistUserRole").val() == 4) {
                if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1) {
                    if (Status == "Submitted For Interim Review" || Status == "Pending For Review" || Status == "Revise Compliance") {
                        $('#ComplainceReviewer').modal('show');
                        $('#iReviewerFrame').attr('src', '/controls/compliancestatusreviewer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType").val() == 0) {
                    if (Status == "Pending For Review" || Status == "Revise Compliance") {
                        $('#ComplainceInternalReviewer').modal('show');
                        $('#iInternalReviewerFrame').attr('src', '/controls/InternalComplianceStatusReviewer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
            }

        }

        function OpenOverViewpupnew(scheduledonid, instanceid, Status) {

            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1250px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1306px');

            if ($("#dropdownlistUserRole").val() == 4) {
                if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1) {
                    if (Status == "Overdue" || Status == "Complied But Document Pending") {
                        $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType").val() == 0) {
                    if (Status == "Overdue" || Status == "Complied But Document Pending") {
                        $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                    }
                }
            }
        }

        function OpenOverViewpupnewone(scheduledonid, instanceid, Status) {
            $('#divOverView1').modal('show');
            $('#OverViews1').attr('width', '1250px');
            $('#OverViews1').attr('height', '600px');
            $('.modal-dialog').css('width', '1306px');

            if ($("#dropdownlistUserRole").val() == 4) {


                if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1) {
                    if (Status == "Overdue" || Status == "Complied But Document Pending") {
                        $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType").val() == 0) {
                    if (Status == "Overdue" || Status == "Complied But Document Pending") {
                        $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                    }
                }
            }
        }

        $("#newModelClose").on("click", function () {
            myWindow3.close();
        });

        function CloseClearPopup() {
            $('#OverViews1').attr('src', "../Common/blank.html");
        }
        function FilterAllMain() {

            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push({
                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                });
            });

            //risk Details
            var Riskdetails = [];
            var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            $.each(list3, function (i, v) {
                Riskdetails.push({
                    field: "Risk", operator: "eq", value: parseInt(v)
                });
            });
            var ComplianceFilter = [];
            if ($("#txtSearchComplianceID").val() != "") {
                ComplianceFilter.push({
                    field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
                });
            }


            var dataSource = $("#grid").data("kendoGrid").dataSource;

            if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Riskdetails
                        },
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
            }

            else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
            }

            else if ($("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Riskdetails
                        }
                    ]
                });
            }
            else if (ComplianceFilter.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: ComplianceFilter
                        }
                    ]
                });
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        function FilterAllAdvancedSearch() {

            //location details
            var locationsdetails = [];
            if ($("#dropdowntree1").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree1").data("kendoDropDownTree")._values;
            }

            //risk Details
            var Riskdetails = [];
            if ($("#dropdownlistRisk1").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
            }

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (locationsdetails.length > 0
                || Riskdetails.length > 0
                || ($("#dropdownlistStatus1").val() != undefined && $("#dropdownlistStatus1").val() != null && $("#dropdownlistStatus1").val() != "")
                || $("#txtSearchComplianceID1").val() != ""
                || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")
                || ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "")
                || ($("#dropdownDept").val() != undefined && $("#dropdownDept").val() != null && $("#dropdownDept").val() != "")
                || ($("#dropdownACT").val() != undefined && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "")) {
                if ($("#dropdownDept").val() != undefined && $("#dropdownDept").val() != null && $("#dropdownDept").val() != "") {
                    var DeptFilter = { logic: "or", filters: [] };
                    DeptFilter.filters.push({
                        field: "DeptId", operator: "eq", value: parseInt($("#dropdownDept").val())
                    });
                    finalSelectedfilter.filters.push(DeptFilter);
                }

                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };
                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });
                    finalSelectedfilter.filters.push(ActFilter);
                }
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    var DateFilter = { logic: "or", filters: [] };
                    DateFilter.filters.push({
                        field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MMM-yyyy')
                    });
                    finalSelectedfilter.filters.push(DateFilter);
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    var DateFilter = { logic: "or", filters: [] };
                    DateFilter.filters.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MMM-yyyy')
                    });
                    finalSelectedfilter.filters.push(DateFilter);
                }

                if ($("#dropdownlistStatus1").val() != undefined && $("#dropdownlistStatus1").val() != null && $("#dropdownlistStatus1").val() != "") {
                    var StatusFilter = { logic: "or", filters: [] };
                    StatusFilter.filters.push({
                        field: "Status", operator: "eq", value: $("#dropdownlistStatus1").data("kendoDropDownList").text()
                    });
                    finalSelectedfilter.filters.push(StatusFilter);
                }


                if (Riskdetails.length > 0) {
                    var RiskFilter = { logic: "or", filters: [] };
                    $.each(Riskdetails, function (i, v) {
                        RiskFilter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }

                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }

                if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }
                if ($("#txtSearchComplianceID1").val() != "") {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID1").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid1").data("kendoGrid").dataSource.filter({});
            }
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');

        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;:');//Dashboard               
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom:4px;border-radius:10px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }

        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example" style="margin-left: 7px;">
        <div style="margin: 0% -0.2% 0.7%; width: 99%;">
            <input id="dropdownlistUserRole" style="width: 14%; margin-right: 8px;" />
            <input id="dropdowntree" style="width: 25.55%; margin-right: 8px;" />
            <input id="dropdownlistComplianceType" style="width: 14%; margin-right: 8px;" />
            <input id="dropdownlistStatus" style="width: 13%; margin-right: 8px;" />
            <input id="dropdownlistTypePastdata" style="width: 14%; margin-right: 8px;" />
            <input id="txtSearchComplianceID" class="k-textbox" placeholder="Compliance ID" style="width: 14%; margin-right: 0.8px;" />
        </div>

        <div style="margin: 0% -0.2% 0.7%; width: 99%;">
            <input id="dropdownlistRisk" style="width: 14%; margin-right: 8px;"/>
            <input id="dropdownlistMoreLink" style="width: 11.5%; margin-right: 8px;" />
            <button id="export" onclick="exportReport(event)" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width: 35px; height: 30px; background-color: white; border: none; display: none;"></button>
            <button id="AdavanceSearch" style="height: 30px;float:right;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
            <button id="dvUnlock" style="height: 30px; display: none;" onclick="OpenAllApprove(event,'1','0')">Unlock</button>
            <button id="ClearfilterMain" style="float: right; height: 30px;margin-right:8px;margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            <button id="dvbtndownloadDocumentMain" style="float: right; display: none;" onclick="selectedDocumentMain(event)">Download</button>
        </div>
        <div id="dvdropdownEventName" style="margin: 0% -0.2% 0.7%; width: 99%;display: none">
                <input id="dropdownEventName" data-placeholder="Event Name" style="width: 20%; margin-right: 8px;" />
                <input id="dropdownEventNature" data-placeholder="Event Nature" style="width: 19.5%;" />
         </div>
        <div id="dvApproverreject" style="display: none; margin: 1% -0.2% 0.7%; width: 99%;">
            <button id="dvApprover" class="k-button" style="margin-right: 10px; float: right;" onclick="OpenAllApprove(event,'1','0')">Approve</button>
            <button id="dvReject" class="k-button" style="margin-right: 10px; float: right;" onclick="OpenAllApprove(event,'2','0')">Reject</button>
        </div>

        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtertype">&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterrisk">&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterstatus">&nbsp;</div>
        <div>
            <div id="grid" style="width: 98.5%;"></div>
            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="divAdvanceSearchModel" style="display: none;">

            <div class="row">
                <div class="col-md-10" style="width: 20.2%; padding-left: 0px; padding-bottom: 4px;">
                    <input id="dropdownlistUserRole1" data-placeholder="Role" style="width: 100%;" />
                </div>
                <div class="col-md-2" style="width: 25.3%; padding-left: 0px;">
                    <input id="txtSearchComplianceID1" class="k-textbox" placeholder="Compliance ID" style="width: 61%;" />
                </div>
            </div>

            <div class="row" style="margin-left: -9px;">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                        <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" id="dvdropdownFY" style="width: 15%; padding-left: 0px;">
                        <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                    </div>
                    <div class="col-md-2" id="dvdropdownUser" style="width: 13%; padding-left: 4px;">
                        <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 15%; padding-left: 0px;">
                        <input id="dropdownlistRisk1" data-placeholder="Risk" style="width: 101%;" />
                    </div>
                    <div class="col-md-2" id="dvStartdatepicker" style="width: 15%; padding-left: 0px;">
                        <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" id="dvLastdatepicker" style="width: 13%; padding-left: 0px;">
                        <input id="Lastdatepicker" placeholder="End Date" style="width: 114%;" />
                    </div>
                </div>
            </div>

            <div class="row" style="margin-left: -9px; margin-top: 5px; margin-bottom: 5px;">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 20%; padding-left: 9px;">
                        <input id="dropdownPastData" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 15.3%; padding-left: 0px;">
                        <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" style="width: 13%; padding-left: 0px; display: none;">
                        <input id="SearchTag" type="text" style="width: 100%;" class="k-textbox" placeholder="Document Tag" />
                    </div>
                    <div class="col-md-4" id="dvdropdownACT" style="width: 29.3%; padding-left: 0px;">
                        <input id="dropdownACT" data-placeholder="Act" style="width: 100%;" />
                    </div>
                    <div class="col-md-4" style="width: 29.3%; padding-left: 0px;">
                        <input id="dropdownDept" data-placeholder="Dept" style="width: 105.7%; margin-left: -2px;" />
                    </div>
                    <div class="col-md-2" style="width: 13.4%; padding-left: 0px; display: none;" id="dvdropdownlistStatus1">
                        <input id="dropdownUser" data-placeholder="User" style="width: 112%;" />
                    </div>
                    <div class="col-md-2" style="width: 13%; padding-left: 0px; float: right; display: none;">
                        <button id="exportAdvanced" onclick="exportReportAdvanced(event)" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width: 35px; height: 30px; background-color: white; border: none;"></button>
                    </div>
                </div>
            </div>
            <div id="dvApproverrejectADV" style="display: none; margin: 1% -0.2% 0.7%; width: 99%;">
                <button id="dvApproverADV" class="k-button" style="margin-right: 10px; float: right;" onclick="OpenAllApprove(event,'1','1')">Approve</button>
                <button id="dvRejectADV" class="k-button" style="margin-right: 10px; float: right;" onclick="OpenAllApprove(event,'2','1')">Reject</button>
            </div>

            <div id="divUnlockADV" style="display: none; margin: 0.5% 0% 0.7%; width: 99%;">
                <button id="dvUnlockADV" style="height:23px;" onclick="OpenAllApprove(event,'1','1')">Unlock</button>
            </div>

            <div class="row" style="padding-bottom: 5px;">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-2" style="width: 20%; padding-left: 1px;">
                        <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
                        <input id="dropdownSequence" style="width: 100%;" />
                        <%}%>
                    </div>
                    <div class="col-md-2" style="width: 14.3%; padding-left: 1px;">
                        <div id="dvdropdownEventName1" style="display: none;">
                            <input id="dropdownEventName1" data-placeholder="Event Name" style="width: 198px;">
                        </div>
                    </div>
                    <div class="col-md-2" style="width: 10%;">
                        <div id="dvdropdownEventNature1" style="display: none;">
                            <input id="dropdownEventNature1" data-placeholder="Event Nature" style="width: 166px;">
                        </div>
                    </div>
                    <div class="col-md-2" style="width: 3%;">
                    </div>
                    <div class="col-md-1" style="padding-right: 6px; width: 37%; float: right;">
                        <button id="Clearfilter" style="float: right; height: 23px; display: block;" onclick="ClearAllFilter()"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                        <button id="dvbtndownloadDocument" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>
                    </div>
                </div>
            </div>

            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterCompType">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterCategory">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterAct">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterCompSubType">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterStartDate">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterLastDate">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard1">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtertype1">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterrisk1">&nbsp;</div>

            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterpstData1">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterUser">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterFY">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterstatus1">&nbsp;</div>


            <div id="grid1" style="width:99.4%"></div>
            <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="ComplaincePerformer1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
                <div class="modal-dialog" style="width: 98%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 20px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; max-height: 570px; overflow-y: hidden;">
                            <iframe id="iPerformerFrame1" src="about:blank" width="100%" height="570px" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ComplainceInternalPerformaer1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
                <div class="modal-dialog" style="width: 98%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 20px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; max-height: 570px; overflow-y: auto;">
                            <iframe id="iInternalPerformerFrame1" src="about:blank" width="100%" height="570px" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ComplainceReviewer1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
                <div class="modal-dialog" style="width: 98%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 20px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; max-height: 570px; overflow-y: auto;">
                            <iframe id="iReviewerFrame1" src="about:blank" width="100%" height="570px" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ComplainceInternalReviewer1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
                <div class="modal-dialog" style="width: 98%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 20px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; max-height: 570px; overflow-y: auto;">
                            <iframe id="iInternalReviewerFrame1" src="about:blank" width="100%" height="570px" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div id="divApproveSearchModel" style="display: none;">
            <div class="row">
                <div class="">
                    <div class="col-md-2" style="display: none;">
                        <input id="txtCompliaceType" type="text" class="k-textbox" />
                    </div>
                    <div class="col-md-6">
                        <label style="font-weight: 500; font-size: 16px; margin: 7px;">Remarks</label>
                    </div>
                    <div class="col-md-4">
                        <input id="txtremark" type="text" style="width: 129%; margin-left: 6px;" onchange="youFunction();" class="k-textbox" />
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top: 5px;">
                <div id="gridremark" style="width: 99%;"></div>
            </div>
            <div style="margin: 1% -0.2% 0.7%; width: 99%;">
                <button id="dvsubmit" class="k-button" onclick="selectedsubmit(event)" style="margin-left: 46%;">Save</button>
                <button id="dvsubmitforreject" class="k-button" onclick="selectedrejectsubmit(event)" style="margin-left: 46%;">Save</button>
            </div>
        </div>
        <iframe id="downloadfile" src="about:blank" width="0" height="0" style="visibility: hidden;"></iframe>

        <div class="modal fade" id="ComplaincePerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
            <div class="modal-dialog" style="width: 98%">

                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
                    <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 570px; overflow-y: hidden;">
                        <iframe id="iPerformerFrame" src="about:blank" width="100%" height="570px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ComplainceInternalPerformaer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
            <div class="modal-dialog" style="width: 98%">

                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
                    <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 570px; overflow-y: hidden;">
                        <iframe id="iInternalPerformerFrame" src="about:blank" width="100%" height="570px" frameborder="0" style="margin-top: -18px;"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ComplainceReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
            <div class="modal-dialog" style="width: 98%">

                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
                    <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 570px; overflow-y: hidden;">
                        <iframe id="iReviewerFrame" src="about:blank" width="100%" height="570px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="ComplainceInternalReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
            <div class="modal-dialog" style="width: 98%">

                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
                    <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 570px; overflow-y: hidden;">
                        <iframe id="iInternalReviewerFrame" src="about:blank" width="100%" height="570px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>