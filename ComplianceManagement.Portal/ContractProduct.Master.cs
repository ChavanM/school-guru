﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Configuration;

namespace Contract
{
    public partial class ContractProduct : System.Web.UI.MasterPage
    {
        protected string user_Roles;
        protected List<Int32> roles;

        protected string LastLoginDate;
        protected string CustomerName;
        protected int customerid;
        protected int userid;
        protected string CompanyAdmin = "";
        protected int reportCustID;
        public List<Cont_tbl_PageAuthorizationMaster> authRecords;
        protected string LogoName;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                user_Roles = AuthenticationHelper.Role;
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                userid = AuthenticationHelper.UserID;
                reportCustID = GetClientName(customerid);
                if (AuthenticationHelper.CustomerID != -1)
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);   
                userid = AuthenticationHelper.UserID;
                string CustomerLogo = string.Empty;
                Customer objCust = UserManagement.GetCustomerforLogo(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (objCust != null)
                {
                    if (objCust.LogoPath != null)
                    {
                        CustomerLogo = objCust.LogoPath;
                        LogoName = CustomerLogo.Replace("~", "../..");
                        // ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindLogo('" + LogoName + "');", true);
                    }
                }
                if (!IsPostBack)
                {
                    String key = "ContractAuthenticate" + AuthenticationHelper.UserID;
                    userid = AuthenticationHelper.UserID;
                  
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        authRecords = (List<Cont_tbl_PageAuthorizationMaster>)HttpContext.Current.Cache[key];
                        if (HttpContext.Current.Cache[key] == null || authRecords.Count() == 0)
                        {
                            authRecords = (from row in entities.Cont_tbl_PageAuthorizationMaster
                                           where row.isActive == true && row.UserID == userid
                                           select row).ToList();

                            int time = Convert.ToInt32(ConfigurationManager.AppSettings["Cont_PageAuthorization"]);
                            HttpContext.Current.Cache.Insert(key, authRecords, null, DateTime.Now.AddMinutes(time), TimeSpan.Zero); // add it to cache
                        }
                        else
                            authRecords = (List<Cont_tbl_PageAuthorizationMaster>)HttpContext.Current.Cache[key];
                    }

                    User LoggedUser = null;

                    if (hdnImagePath.Value != null && !String.IsNullOrEmpty(hdnImagePath.Value))
                    {
                        ProfilePic.Src = hdnImagePath.Value;
                        ProfilePicTop.Src = hdnImagePath.Value;
                        // ProfilePicSide.Src = hdnImagePath.Value;
                    }
                    else
                    {
                        LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                        Page.Header.DataBind();

                        if (LoggedUser != null)
                        {
                            if (LoggedUser.ImagePath != null)
                            {
                                ProfilePic.Src = LoggedUser.ImagePath;
                                ProfilePicTop.Src = LoggedUser.ImagePath;
                                //ProfilePicSide.Src = LoggedUser.ImagePath;
                                hdnImagePath.Value = LoggedUser.ImagePath;
                            }
                            else
                            {
                                ProfilePic.Src = "~/UserPhotos/DefaultImage.png";
                                ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                                //ProfilePicSide.Src = "~/UserPhotos/DefaultImage.png";
                                hdnImagePath.Value = "~/UserPhotos/DefaultImage.png";
                            }
                        }

                        roles = ContractManagement.GetAssignedRoles_Contract(AuthenticationHelper.UserID);
                    }

                    if (Session["LastLoginTime"] != null)
                    {
                        LastLoginDate = Session["LastLoginTime"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int GetClientName(int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "ReportCustID"
                            && row.ClientID == customerid
                            select row.ClientID).FirstOrDefault();
                return data;
            }
        }
    }
}