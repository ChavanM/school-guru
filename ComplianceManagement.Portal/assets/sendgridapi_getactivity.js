var data = "{}";
var url2 = "";

var xhr = new XMLHttpRequest();
xhr.withCredentials = false;


function getActivity(){
  $('#response').find("tr:gt(0)").remove();
  $('#response2').find("tr:gt(0)").remove();
  var userinput = document.getElementById("emailToDelete").value
  
  //String unix date
  var startDate = document.getElementById("unixStart").value+"000"
  var endDate = document.getElementById("unixEnd").value+"000"

  //Date object 
  var formattedStart = $.datepicker.parseDate('@', startDate);
  var formattedEnd = $.datepicker.parseDate('@', endDate);

  //string date as yyyy-mm-dd
  var start_iso8601 = $.datepicker.formatDate("yy-mm-dd", formattedStart)+"T00:00:00Z"
  var end_iso8601 = $.datepicker.formatDate("yy-mm-dd", formattedEnd)+"T00:00:00Z"
  
  var timestamp_query = 'last_event_time BETWEEN TIMESTAMP "' + start_iso8601 + '" AND TIMESTAMP "' + end_iso8601 +'"';
  console.log(start_iso8601);
  console.log(end_iso8601);
  if (userinput != '' && userinput != 'all'){
    url2 = 'https://api.sendgrid.com/v3/messages?query=to_email="'+userinput+'" AND '+ timestamp_query +'&limit=1000';
  }
  else if (userinput == 'all'){
    url2 = 'https://api.sendgrid.com/v3/messages?limit=1000&query=' + timestamp_query;
  }
  
  else{
    $('#loader').hide();
    alert("Please enter an email address");
    return false;
  }

  
  
  
  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === this.DONE) {
        //console.log("done");
    
    data = JSON.parse(this.responseText);
    //console.log(data.messages);
    //console.log(data.messages[0].from_email)
    var tr;
        
    for (var i = 0; i < data.messages.length; i++){
      
      tr = $('<tr/>');
      //tr.append("<td><input type='checkbox' id='"+data.messages[i].from_email+"' /></td>");
      tr.append("<td>" + data.messages[i].from_email + "</td>");
      tr.append("<td>" + data.messages[i].to_email + "</td>");
      tr.append("<td>" + data.messages[i].subject + "</td>");
      tr.append("<td>" + data.messages[i].status + "</td>");
      tr.append("<td>" + data.messages[i].last_event_time + "</td>");
      //console.log(data.messages[i])
      $('#response').append(tr);}
      $('#loader').hide();
      bindCheckbox();
    }
  });
    console.log(url2)
    xhr.open("GET", url2);
    xhr.setRequestHeader("authorization", "Bearer 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");
    xhr.setRequestHeader("accept", "application/json");
    xhr.send(data);
}

function getFormattedTime(unixTime)
{var a = new Date(unixTime * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}


