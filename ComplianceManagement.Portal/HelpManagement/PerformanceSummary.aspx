﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    

   <%--<link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
     <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1195px;

      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
      .MainContainer
    {
      
         /*height:1200px;*/
    } 
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1200px;
  width:1200px;
 

  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
  /*background-color:#1fd9e1;*/
}

* {
  box-sizing: border-box;
}

    </style>
 
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style> 
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
      
       <div class="row content"> 

           <div class="col-sm-3 sidenav">
                     
                    

                  <ul class="nav nav-pills nav-stacked" >
                    <li ><a href="../HelpManagement/dashboard.aspx" style="color:#333;font-size:16px"><b>Management Dashboard</b></a></li>
                        <li ><a href="../NewMgmtHelp.aspx" style="color:#333"><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                        
                                <li ><a href="../HelpManagement/Entities.aspx" style="color:#333" > Entities</a></li>
                                <li><a href="../HelpManagement/Location.aspx" style="color:#333">Location</a></li>
                                <li><a href="../HelpManagement/Categories.aspx" style="color:#333">Categories</a></li>
                                <li> <a href="../HelpManagement/Compliances.aspx" style="color:#333">Compliances</a></li>
                                <li> <a href="../HelpManagement/Users.aspx" style="color:#333">Users</a></li>
                                <li> <a href="../HelpManagement/Penalty.aspx" style="color:#333">Penalty</a></li>
                                <li><a href="../HelpManagement/OverdueSummary.aspx" style="color:#333">Summary of Overdue Compliances</a></li>
                                <li> <a href="../HelpManagement/PerformanceSummary.aspx" style="color:#333">Performance Summary</a></li>
                                <li> <a href="../HelpManagement/RiskSummary.aspx" style="color:#333">Risk Summary</a></li>
                                <li> <a href="../HelpManagement/PenaltySummary.aspx" style="color:#333">Penalty Summary</a></li>
                                <li> <a href="../HelpManagement/GradingReport.aspx" style="color:#333">Grading Reports</a></li>
                                <li><a href="../HelpManagement/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpManagement/DailyUpdates.aspx"style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpManagement/Newsletter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li ><a href="../HelpManagement/Reports.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li ><a href="../HelpManagement/Documents.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li ><a href="../HelpManagement/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>
                       
                      
                     </ul>
            <br/>
                
           </div>

           <div class="col-sm-9 MainContainer" style="border:groove;border-color:#f1f3f4">
            <h3>Performance summary</h3>
             <p style="font-size:14px;">With the help of graphical presentation this section gives brief of Performance summary, Risk summary and Penalty summary. </p>
               <ul >
                   <li style="list-style-type:decimal"> <b>Filters-</b> You can set filter of compliance type, start date, End date, Location, Year to date (Current, Current + Previous and all). And based on its Performance, Risk and Penalty summary will be shown. </li><br />
                   <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_0_Screenshot.png" /><br /><br />
                    <li style="list-style-type:decimal"><b>Performance Summary-</b> This sub section features chart and graph, where Pie chart is for overall compliances-completion status and Bar graph is for overall functions- completion status. </li><br />
                   <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_1_Screenshot.png" />
               </ul>
               <br />
                 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

	                <div class="panel panel-default">
	              		<div class="panel-heading" role="tab" id="headingOne">
			      	<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Completion Status Overall</b>
					</a>
				      </h4>
		              	</div>
		            	<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			            	<div class="panel-body">
					 
                           <p>The pie chart gives idea about compliances completed within time, not completed, completed after due date. The color code gives more clarification of status by stating Not completed, After Due date and In Time compliances. </p>
                           <img class="img1"  style="height:300px;width:700px;margin-left:30px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_2_Screenshot.png" /><br /><br />
                         <ul>
                           <li style="list-style-type:decimal">You can Drilldown to view risk type of selected status (Not completed, After Due date and In Time).  </li>
                           <li style="list-style-type:decimal">To Drilldown click on status type and you will be redirected to Risk type pie chart. Here color code will define three types of risks. </li><br />
                            <img class="img1" style="height:300px;width:700px"  src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_3_Screenshot.png" /><br /><br />
                             <li style="list-style-type:decimal"> On redirected chart the color code will define three types of risks High, Low and Medium. To go back to previous pie chart, click on Back button.</li><br />
                             <img class="img1" style="height:300px;width:700px"  src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_4_Screenshot.png" /><br /><br />
                             <li style="list-style-type:decimal">To view document of compliances following under particular risk type, click on risk type. </li><br />
                             <img class="img1" style="height:300px;width:700px"  src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_5_Screenshot.png" /><br /><br />
                             <li style="list-style-type:decimal"> A window will appear holding list of compliances and to view document click, on Action icon. </li>
                              <li style="list-style-type:decimal">Also, you can export this compliance list, and for same click on Export all to excel button.</li><br />
                             <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_6_Screenshot.png" />
                          </ul>
                      </div>
                
		         	</div>

	         </div>

                    <div class="panel panel-default">
		            	<div class="panel-heading" role="tab" id="headingTwo">
			            	<h4 class="panel-title">
				          	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
					     	<i class="short-full glyphicon glyphicon-plus"></i>
						     <b>Completion Status Overall Functions</b>
					       </a>
				           </h4>
			          </div>
		                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				          <div class="panel-body">
				        	<p>This bar graph gives idea of function wise compliances. In this graph the X axis holds different functions and for every function three bar that defines risk type (High, low and medium). And Y-axis holds number of compliances.</p><br />
                               <img class="img1" style="height:300px;margin-left:30px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_7_Screenshot.png" /><br /><br />
                              <p>You can also view compliance completion status by clicking on risk type bar of particular function and you will be redirected to a bar graph that gives idea about compliances completed- In time, After due date and Not completed.</p><br />
                               <img style="height:300px;margin-left:30px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_8_Screenshot.png" /><br /><br />
                              <p>To view documents, click on particular bar and a window will appear to holding list of compliances. And to view documents click on Action icon and click on Export all to Excel button to export list in excel sheet. </p><br />
                              <img class="img1" style="height:300px;width:700px;margin-left:30px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Performance%20Summary_9_Screenshot.png" />
                        </div>

			         </div>

                	</div>

                </div>
	
	
          </div>
     </div>

 </div><!-- container -->
    </form>
   
  <script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
