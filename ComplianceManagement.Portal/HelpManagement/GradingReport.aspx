﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <%--<link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
    
     <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1795px;

      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
           .MainContainer
    {
       
        height:1795px;
    }

    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1800px;
  width:1200px;
 

  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/
 
}

* {
  box-sizing: border-box;
}

    </style>
    <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                    

           <ul class="nav nav-pills nav-stacked" >
                    <li ><a href="../HelpManagement/dashboard.aspx" style="color:#333;font-size:16px"><b>Management Dashboard</b></a></li>
                        <li ><a href="../NewMgmtHelp.aspx" style="color:#333"><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                        
                                <li ><a href="../HelpManagement/Entities.aspx" style="color:#333" > Entities</a></li>
                                <li><a href="../HelpManagement/Location.aspx" style="color:#333">Location</a></li>
                                <li><a href="../HelpManagement/Categories.aspx" style="color:#333">Categories</a></li>
                                <li> <a href="../HelpManagement/Compliances.aspx" style="color:#333">Compliances</a></li>
                                <li> <a href="../HelpManagement/Users.aspx" style="color:#333">Users</a></li>
                                <li> <a href="../HelpManagement/Penalty.aspx" style="color:#333">Penalty</a></li>
                                <li><a href="../HelpManagement/OverdueSummary.aspx" style="color:#333">Summary of Overdue Compliances</a></li>
                                <li> <a href="../HelpManagement/PerformanceSummary.aspx" style="color:#333">Performance Summary</a></li>
                                <li> <a href="../HelpManagement/RiskSummary.aspx" style="color:#333">Risk Summary</a></li>
                                <li> <a href="../HelpManagement/PenaltySummary.aspx" style="color:#333">Penalty Summary</a></li>
                                <li> <a href="../HelpManagement/GradingReport.aspx" style="color:#333">Grading Reports</a></li>
                                <li><a href="../HelpManagement/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpManagement/DailyUpdates.aspx"style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpManagement/Newsletter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li ><a href="../HelpManagement/Reports.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li ><a href="../HelpManagement/Documents.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li ><a href="../HelpManagement/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>
                       
                      
                     </ul>
            <br/>
                
           </div>
        
          <div class="col-sm-9 MainContainer" style="line-height:25px;border:groove;border-color:#f1f3f4">
              <h2>Grading Reports</h2>
              <p style="font-size:14px;">This report gives glimpse of compliances completed in a month. And with the help of this report Management can overview whether the compliances where completed within timeline or not.</p>
              <ul>
                  <li style="list-style-type:decimal"> By default, the report shows all assigned entities compliance completion overview.</li>
                  <li style="list-style-type:decimal">You can set filter to view report as per desire and the filter can be set for Entities/Sub-Entities/Location, FY, Month and display as per 12/6/3 months.</li>
                  <li style="list-style-type:decimal">The color code gives more clarification about the compliance’s completion. 
                      <ul>
                          <li style="list-style-type:lower-roman"><b>Green-</b> Indicates 100% High risk and medium risk compliances are completed in time and at least 75% low risk compliances are completed in time. </li>
                          <li style="list-style-type:lower-roman"><b>Yellow-</b> Indicates 75% High risk compliances are completed in time and at least 50% medium risk and low risk compliances are completed in time.</li>
                          <li style="list-style-type:lower-roman"><b>Red-</b> Not falling under Green/Yellow color code criteria.</li>
                          <li style="list-style-type:lower-roman"><b>White-</b> No schedule for this month or Not applicable.  </li>
                      </ul>
                  </li>
                  <img class="img1" style="width:700px;height:400px;margin:10px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Grading%20report_1_Screenshot.png" /><br />
                  <li style="list-style-type:decimal">To view number of compliances completed within time/not click on grid and a window will appear with tab Graph and Details,
                      <ul>
                          <li style="list-style-type:lower-roman"><b>Graph-</b> This page holds bar graph featuring High, Medium and low compliances completed In time, Not completed and After due date. </li>
                          <img class="img1" style="width:700px;height:400px;margin:10px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Grading%20report_2_Screenshot.png" /><br />
                          <li style="list-style-type:lower-roman"><b>Details-</b> On this page you will find selected grid compliances details i.e. Location, Compliance, Performer, Due Date, Month and Status. </li>
                          <img class="img1" style="width:700px;height:400px;margin:10px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Grading%20report_3_Screenshot.png" />
                      </ul>
                  </li>
                  </ul>
              
            </div>
    </div>

   </div>
    </form>
</body>
</html>
