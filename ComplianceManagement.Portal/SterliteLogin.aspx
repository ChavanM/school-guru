﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SterliteLogin.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.SterliteLogin" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />

    <title>Login :: AVANTIS - Products that simplify</title>
    <!-- Bootstrap CSS -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="https://avacdn.azureedge.net/newcss/elegant-icons-style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="https://avacdn.azureedge.net/newcss/style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/style-responsive.css" rel="stylesheet" />
    <style type="text/css">
        .otpdiv > span > label {
            border: 0px;
            color: black;
            margin-left: -17px;
            margin-top: 2px;
            position: absolute;
            width: 111px;
        }

        .login-form {
            max-width: 1200px;

        }

        .login-form-head {
            max-width: 1200px;
            min-height: 55px;
        }

        .otpdiv > span {
            width: 233px;
            height: 26px;
            margin: auto;
            position: relative;
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
        }

            .otpdiv > span > input {
                width: 80px;
                height: 26px;
                margin: auto;
                position: relative;
                border-radius: 2px;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                margin-left: -9px;
            }
            .center {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 200px;
  border: 3px solid green; 
}
    </style>
    <script type="text/javascript">

        var _paq = window._paq || [];

        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function () {
            var u = "//analytics.avantis.co.in/";
            _paq.push(['setTrackerUrl', u + 'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
        })();

        function settracknew(e, t, n, r) {

            try {
                _paq.push(['trackEvent', e, t, n])
            } catch (t) { } return !0
        }

        function settracknewForSuccess() {

            settracknew('Login', 'Login', 'Success', '');
        }
        function settracknewForFailed() {
            settracknew('Login', 'Login', 'Failed', '');
        }
        function settracknewRememberUnchecked() {
            settracknew('Login', 'Login', 'RememberUnchecked', '');
        }
        function settracknewRememberchecked() {
            settracknew('Login', 'Login', 'Rememberchecked', '');
        }
    </script>

</head>
<body>
    <div class="container">

        <form runat="server" class="login-form" name="login">
            <asp:Panel ID="Panel2" runat="server" DefaultButton="Submit">

                <asp:ScriptManager ID="ScriptManager2" runat="server" />
                <cc2:NoBot ID="PageNoBot" runat="server" Enabled="true" ResponseMinimumDelaySeconds="0" />

                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head" style="background: none;">
                         
                                    <img src="/Images/Sterlite2.0_Logo.jpg" />
                       
                        </div>

                        <div class="login-wrap">
                            <div id="divLogin" class="row" runat="server">
                                <h1>CEO Message</h1>
                                <div style="margin-top: 15px; color: #555555; text-align: justify;">
                                Sterlite Power is committed to conduct business ethically and in compliance with applicable laws, rules and regulations and is an important part of our reassuring brand - demonstrating how we put the interests of our clients and members first to make a real difference in people’s lives.
                                <br /><br />
                                I request each one of you to be aware that Sterlite Power’s reputation and continued success is dependent on the conduct of our employees, contractors, officers and directors and our adherence to Compliance and ethical values.   Let us pledge that we will never define our goals nor make commitments that could only be achieved with conduct that would violate applicable laws or business ethics.
                                <br /><br />
                                Based on this awareness, we are attempting to perfect a compliance system which promotes compliance in the broadest sense.  We have partnered with Avantis RegTech (www.avantis.co.in, a Team Lease company) for implementation of the Compliance framework, which involves defining and implementing compliance activities, technology enabled compliance checks, monitoring, reporting and follow-up management and imparting ongoing education to prevent and minimize risks of non-compliance.
                                <br /><br />
                                I am happy to inform you that the AVACOM compliance management program will go live on April 1, 2021. The Compliance program has received the sponsorship of all BU Heads and Functional Heads and their respective Teams who undertook ownership and rendered full support and engagement to the Compliance Team and Avantis to make this happen. I trust you will continue to engage in the effective implementation of the Compliance program and ensure that Sterlite Power continues its responsible business operations on a robust compliance platform.  
                                <br /><br />
                                I understand that training is an integral part of this compliance program and I urge all to actively participate in the training programs and be part of developing a unique culture of compliance for Sterlite Power. You should feel free to ask questions or seek guidance from our General Counsel, who is also our Chief Compliance Officer.
                                <br /><br />
                                Let us all join together to underscore our culture of ethical conduct and compliance that gives us pride in our work and organization and help us sustain our standing as socially responsible organization and good corporate citizens.
                                <br /><br />
                                Our Compliance, Our Responsibility!
                                </div>

                                <div class="clearfix"></div>

                                <br />

                               

                            </div>
                           
                                <div style="margin-top: 0px;">
                                    <asp:Button ID="Submit" Visible="false" CssClass="btn btn-primary btn-lg btn-block" Text="Sign in" runat="server"></asp:Button>
                                </div>
                                <div class="col-md-12 login-form-head" style="background: none; width: 20%; margin-left:430px;">
                                    <asp:Button ID="btnAD" CssClass="btn btn-primary btn-lg btn-block" Text="Login in with SSO" runat="server" OnClick="btnAD_Click"></asp:Button>

                                </div>
                        </div>



                        <div class="clearfix" style="height: 10px"></div>

                        <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" />
                        <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" />


                        </div>                       
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAD" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <!--js-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.js"></script>
    <script type='text/javascript' src="https://avacdn.azureedge.net/newjs/bootstrap.min.js"></script>


    <script type="text/javascript">
        function socaillogin(data, type) { var DomainName = location.hostname.toLowerCase(); window.location.href = data }
        $(function () {

        });
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
    </script>

</body>
</html>
