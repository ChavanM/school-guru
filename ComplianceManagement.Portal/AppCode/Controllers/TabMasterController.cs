﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
  
    public class TabMasterController : Controller
    {
        private ComplianceDBEntities db = new ComplianceDBEntities();
        public class TabData
        {
            public int ID { get; set; }
            public string Name { get; set; }
            //  public string Name { get; set; }
            public int LocationID { get; set; }

            public int TabID { get; set; }

        


        }

        public int Id { get; private set; }
        // GET: TabMaster
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetCustomerName()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //int CLI_Id = Convert.ToInt32(Session["CLI_Id"]);
                entities.Configuration.ProxyCreationEnabled = false;
                var cust = entities.LocationTypes.Select(model => new { model.ID, model.Name }).ToList().OrderBy(model => model.Name);

                // ddlRiskRole.DataSource = roles.OrderBy(entry => entry.Name);
                return Json(cust, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateUser(TabMasterEntry Customers)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Customer assignment  Is Not Complete!";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var usr = db.TabMasters.Where(x => x.ID == Customers.ID).FirstOrDefault();
                        //usr.TabName = Customers.TabName;
                        //  usr.CreatedBy = Id;
                        //usr.IsDeleted = false;
                            
                        entities.SaveChanges();
                        foreach(var loc in Customers.locations)  //3  (1,2,3)
                        {
                            var locations = db.TabDetails.Where(x => x.TabMasterID == Customers.ID).ToList();   //4(2,3,4,5)
                                     foreach(var loc1 in locations)
                            {
                                
                            }
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                    }

                    result = "Tab Master Created Successfully!";
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult CheckExistance(string employee)
        {
            bool isExist = db.TabMasters.Where(x => x.IsDeleted == false && x.TabName.ToUpper().Trim() == employee.ToUpper().Trim()).Count() > 0 ? true : false;
            return Json(isExist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveCusAssignments(TabMasterEntry Customers)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Customer assignment  Is Not Complete!";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var istabNameExist = entities.TabMasters.Where(x => x.TabName == Customers.TabName).SingleOrDefault();

                        var istabIdExist = entities.TabMasters.Where(x => x.ID == Customers.ID).SingleOrDefault();
                        if (istabIdExist == null)
                        {
                            TabMaster cust = new TabMaster();
                            cust.TabName = Customers.TabName;
                            cust.CreatedBy = AuthenticationHelper.UserID;
                            cust.IsDeleted = false;
                            cust.CreatedDate = DateTime.Now;
                            entities.TabMasters.Add(cust);
                            entities.SaveChanges();
                            int tabid = cust.ID;
                            foreach (var loc in Customers.locations)
                            {
                                TabDetail td = new TabDetail();
                                td.TabMasterID = tabid;
                                td.LocationID = loc.LocationID;
                                td.CreatedBy = AuthenticationHelper.UserID;
                                td.IsDeleted = false;
                                td.CreatedDate = DateTime.Now;
                                entities.TabDetails.Add(td);
                            }
                            entities.SaveChanges();
                        }
                        else
                        {
                            //istabIdExist.ID = Customers.ID;
                            //istabIdExist.TabName = Customers.TabName;
                           
                           entities.Database.ExecuteSqlCommand("delete from tabdetails where TabMasterID=" + Customers.ID);
                            
                            foreach (var loc in Customers.locations)
                            {
                                TabDetail td = new TabDetail();
                                td.TabMasterID = Customers.ID;
                                td.LocationID = loc.LocationID;
                                td.CreatedBy = AuthenticationHelper.UserID;
                                td.IsDeleted = false;
                                td.CreatedDate = DateTime.Now;
                                entities.TabDetails.Add(td);
                            }
                            entities.SaveChanges();
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                    }

                    result = "Tab Master Created Successfully!";
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //public ActionResult SaveCusAssignments(TabMasterEntry Customers)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        string result = "Error! Customer assignment  Is Not Complete!";
        //        using (var dbContextTransaction = entities.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                foreach (TabDetail Tab in Customers.locations)
        //                {
        //                    entities.Database.ExecuteSqlCommand("delete from TabDetail where ID=" + Tab.ID);
        //                }

        //                foreach (TabDetail Tab in Customers.locations)
        //                {
        //                    var istabIdExist = entities.TabMasters.Where(x => x.ID == Customers.ID).SingleOrDefault();
        //                    if (istabIdExist == null)
        //                    {
        //                        TabMaster cust = new TabMaster();
        //                        cust.TabName = Customers.TabName;
        //                        cust.CreatedBy = AuthenticationHelper.UserID;
        //                        cust.IsDeleted = false;
        //                        cust.CreatedDate = DateTime.Now;
        //                        entities.TabMasters.Add(cust);
        //                        entities.SaveChanges();
        //                        int tabid = cust.ID;

        //                        foreach (var loc in Customers.locations)
        //                        {
        //                            TabDetail td = new TabDetail();
        //                            td.TabMasterID = tabid;
        //                            td.LocationID = loc.LocationID;
        //                            td.CreatedBy = AuthenticationHelper.UserID;
        //                            td.IsDeleted = false;
        //                            td.CreatedDate = DateTime.Now;
        //                            entities.TabDetails.Add(td);
        //                        }
        //                        entities.SaveChanges();
        //                    }
        //                    else
        //                    {
        //                        istabIdExist.ID = Customers.ID;
        //                        istabIdExist.TabName = Customers.TabName;
        //                        foreach (var loc in Customers.locations)
        //                        {
        //                            TabDetail td = new TabDetail();
        //                            // td.TabMasterID = tabid;
        //                            td.LocationID = loc.LocationID;
        //                            td.CreatedBy = AuthenticationHelper.UserID;
        //                            td.IsDeleted = false;
        //                            td.CreatedDate = DateTime.Now;
        //                            entities.TabDetails.Add(td);
        //                        }
        //                    }
        //                }
        //                dbContextTransaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                dbContextTransaction.Rollback();
        //            }

        //            result = "Tab Master Created Successfully!";
        //        }
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult CheckExistance(int employee)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool isExist = entities.TabMasters.Where(x => x.ID == employee).Count() > 0 ? true : false;
                return Json(isExist, JsonRequestBehavior.AllowGet);
            }                                                     
        }



        public JsonResult getAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var CsutomersList = (from tm in entities.TabMasters
                                     join td in entities.TabDetails on tm.ID equals td.TabMasterID
                                     join lt in entities.LocationTypes on td.LocationID equals lt.ID
                                     select new
                                     {
                                         ID = tm.ID,
                                         TabDetailsID = td.ID,
                                         CustomerName = tm.TabName,
                                         LocationName=lt.Name,
                                     }).ToList();

                var JsonResult = Json(CsutomersList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public JsonResult getCustomerAssignmentByNo(string id)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int no = Convert.ToInt32(id);
                    var CsutomersList = (from tm in entities.TabMasters
                                         join td in entities.TabDetails on tm.ID equals td.TabMasterID
                                         join lt in entities.LocationTypes on td.LocationID equals lt.ID
                                         where tm.ID == no
                                         select new
                                         {
                                             ID = tm.ID,
                                             CustomerName = tm.TabName,
                                             //LocationName = lt.Name,
                                             LocationID= td.LocationID,
                                         }).ToList();
                    return Json(CsutomersList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                LoggerMessage.InsertLog(exp, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json("Error in getting record !", JsonRequestBehavior.AllowGet);
            }
        }
        public string DeleteCustomerAssignment(string Id, string TabamsterId)
        {
            if (!String.IsNullOrEmpty(Id))
            {
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        using (var dbContextTransaction = entities.Database.BeginTransaction())
                        {
                            try
                            {
                                int TDId = Int32.Parse(Id);
                                int TMId = Int32.Parse(TabamsterId);

                                bool isExistinQM = db.QuestionMasters.Where(x => x.IsActive == false && x.TabID == TMId).Count() > 0 ? true : false;
                                if (!isExistinQM)
                                {
                                    entities.Database.ExecuteSqlCommand("delete from Tabdetails where ID=" + TDId);
                                    entities.SaveChanges();
                                    dbContextTransaction.Commit();

                                    bool isExist = db.TabDetails.Where(x => x.IsDeleted == false && x.TabMasterID == TMId).Count() > 0 ? true : false;
                                    if (isExist)
                                    {

                                    }
                                    else
                                    {
                                        entities.Database.ExecuteSqlCommand("delete from TabMaster where ID=" + TMId);
                                        entities.SaveChanges();
                                        //dbContextTransaction.Commit();
                                    }
                                    return "Selected Tab is deleted sucessfully";
                                }
                                else
                                {
                                    return "Tab Name is used in Question Master, So can't be deleted";
                                }
 
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                return ex.InnerException.ToString();
                            }
                        }
                        //return "Selected Customer assignment record deleted sucessfully";
                    }
                }
                catch (Exception ex1)
                {
                    return ex1.InnerException.ToString();
                }
            }
            else
            {
                return "Invalid operation";
            }
        }

        [HttpGet]
        public ActionResult Gettabdata()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var data = entities.TabMasters.Select(model => new { model.ID, model.TabName }).ToList();
               
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }
    }
}