﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class QuestionActReviewerController : Controller
    {
        // GET: QuestionActReviewer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult QuestionActReviewerQuestions()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetActAgrup()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;

                var role = (from A in entities.IMA_GetAssignedAct(AuthenticationHelper.UserID, "ACTGROUP", "REV")
                            select new
                            {
                                ID = A.ID,
                                Name = A.Name,
                                ActGroupID = A.ActGroupID
                            }).ToList();

                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        #region  Submitted Question  Act Mapping    
        [HttpGet]
        public ActionResult GetBusinessActivity()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.BusinessActivityTypes.Where(x => x.IsActive == false).Select(model => new { model.ID, model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetActApplicability()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.Act_Applicability.Select(model => new { model.ID, model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetLocationType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.LocationTypes.Select(model => new { model.ID, model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetCompanyType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.CompanyTypes.Select(model => new { model.ID, model.Name }).ToList();                
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetPossibleQuestionAnswer()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.PossibleQuestionAnswers.Select(model => new { QuestionId = model.QuestionId, ID = model.ID, Name = model.Answer }).ToList();                
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getAllActs(int actgroupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.IMA_GetReviewerSubmittedActDetails(actgroupid,AuthenticationHelper.UserID)
                                    select usr).ToList();


                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public class QAActSubmitted_MappingUpdate
        {
            public long SubmittedQACMMappingID { get; set; }
            public long SubmittedActID { get; set; }
            public long SubmittedQuestionID { get; set; }           
            public long SubmittedControllID { get; set; }
            public Nullable<long> SubmittedServiceID { get; set; }
            public Nullable<long> SubmittedAnswer { get; set; }
            public string SubmittedAnswerSingle { get; set; }
            public string SubmittedAnswerMultiple { get; set; }
            public Nullable<long> SubmittedAnsweRangeMin { get; set; }
            public Nullable<long> SubmittedAnsweRangeMax { get; set; }
            public Nullable<long> SubmittedCompanyTypeID { get; set; }
            public Nullable<long> SubmittedBusinessActivityID { get; set; }
            public Nullable<long> SubmittedLocationTypeID { get; set; }
            public Nullable<long> SubmittedActApplicabilityID { get; set; }
            public Nullable<long> SubmittedStatusid { get; set; }
        }
        public class QActSubmittedMappingEntry
        {
            public List<QAActSubmitted_MappingUpdate> Approvals { get; set; }
        }
        public string AddQuestionActMapping(QAActSubmitted_MappingUpdate detail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        Questions_ActMapping Qmst = new Questions_ActMapping();
                        Qmst.ActId = detail.SubmittedActID;
                        Qmst.QuestionID = detail.SubmittedQuestionID;
                        Qmst.ControllId = detail.SubmittedControllID;
                        Qmst.AnswerTOQ = detail.SubmittedServiceID;
                        Qmst.AnswerValue = detail.SubmittedAnswer;
                        if (detail.SubmittedControllID == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.SubmittedAnswerSingle;
                        }
                        else if (detail.SubmittedControllID == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.SubmittedAnswerMultiple;
                        }
                        Qmst.StatusId = 3;
                        Qmst.AnsweRangeMin = detail.SubmittedAnsweRangeMin;
                        Qmst.AnsweRangeMax = detail.SubmittedAnsweRangeMax;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        Qmst.LocationTypeID = detail.SubmittedLocationTypeID;
                        Act_CompanyTypeMapping ACTM = new Act_CompanyTypeMapping()
                        {
                            ActID = detail.SubmittedControllID,
                            TypeID = (int)detail.SubmittedCompanyTypeID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };

                        if (!Act_CompanyTypeMappingExists(ACTM))
                        {
                            entities.Act_CompanyTypeMapping.Add(ACTM);
                            entities.SaveChanges();
                        }

                        Act_BusinessActivityMapping ABAM = new Act_BusinessActivityMapping()
                        {
                            ActID = detail.SubmittedControllID,
                            BATypeID = (int)detail.SubmittedBusinessActivityID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };
                        if (!ActBusinessActivityMappingExists(ABAM))
                        {
                            entities.Act_BusinessActivityMapping.Add(ABAM);
                            entities.SaveChanges();
                        }

                        Act_ActApplicabilityMapping AAAM = new Act_ActApplicabilityMapping()
                        {
                            ActID = detail.SubmittedControllID,
                            Act_ApplicabilityID = (int)detail.SubmittedActApplicabilityID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };
                        if (!ActApplicabilityMappingExists(AAAM))
                        {
                            entities.Act_ActApplicabilityMapping.Add(AAAM);
                            entities.SaveChanges();
                        }
                        var query = (from row in entities.Acts
                                     where row.ID == detail.SubmittedControllID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.LocationTypeID = detail.SubmittedLocationTypeID;
                            entities.SaveChanges();
                        }
                        if (Questions_ActMappingExists(Qmst))
                        {
                            //result = "Question name already exists";
                            result = "Details already exists!";
                        }
                        else
                        {
                            entities.Questions_ActMapping.Add(Qmst);
                            entities.SaveChanges();
                            var QuestionID = Qmst.Id;
                            result = "Details Saved Successfully!";
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }
        public bool ActApplicabilityMappingExists(Act_ActApplicabilityMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_ActApplicabilityMapping
                             where row.IsActive == false
                                  && row.ActID == customer.ActID
                                  && row.Act_ApplicabilityID == customer.Act_ApplicabilityID
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public bool ActBusinessActivityMappingExists(Act_BusinessActivityMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_BusinessActivityMapping
                             where row.IsActive == false
                                  && row.ActID == customer.ActID
                                  && row.BATypeID == customer.BATypeID
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public bool Act_CompanyTypeMappingExists(Act_CompanyTypeMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_CompanyTypeMapping
                             where row.IsActive == false
                                  && row.ActID == customer.ActID
                                  && row.TypeID == customer.TypeID
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public bool Questions_ActMappingExists(Questions_ActMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Questions_ActMapping
                             where row.IsActive == false
                                  && row.ActId == customer.ActId
                                  && row.QuestionID == customer.QuestionID
                                  && row.ControllId == customer.ControllId
                             select row);

                if (customer.Id > 0)
                {
                    query = query.Where(entry => entry.Id != customer.Id);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        [HttpPost]
        public ActionResult SaveActs(QActSubmittedMappingEntry TimeSheets)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (QAActSubmitted_MappingUpdate detail in TimeSheets.Approvals)
                    {
                        int empId = Convert.ToInt32(Session["userID"]);
                        Questions_ActMapping Qmst = new Questions_ActMapping();
                        Qmst.ActId = detail.SubmittedActID;
                        Qmst.QuestionID = detail.SubmittedQuestionID;
                        Qmst.ControllId = detail.SubmittedControllID;
                        Qmst.AnswerTOQ = detail.SubmittedServiceID;
                        Qmst.AnswerValue = detail.SubmittedAnswer;
                        if (detail.SubmittedControllID == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.SubmittedAnswerSingle;
                        }
                        else if (detail.SubmittedControllID == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.SubmittedAnswerMultiple;
                        }
                        Qmst.StatusId = 3;
                        Qmst.AnsweRangeMin = detail.SubmittedAnsweRangeMin;
                        Qmst.AnsweRangeMax = detail.SubmittedAnsweRangeMax;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        Qmst.LocationTypeID = detail.SubmittedLocationTypeID;
                        entities.Questions_ActMapping.Add(Qmst);
                        entities.SaveChanges();



                        Act_CompanyTypeMapping ACTM = new Act_CompanyTypeMapping()
                        {
                            ActID = detail.SubmittedActID,
                            TypeID = (int)detail.SubmittedCompanyTypeID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };

                        if (!Act_CompanyTypeMappingExists(ACTM))
                        {
                            entities.Act_CompanyTypeMapping.Add(ACTM);
                            entities.SaveChanges();
                        }

                        Act_BusinessActivityMapping ABAM = new Act_BusinessActivityMapping()
                        {
                            ActID = detail.SubmittedActID,
                            BATypeID = (int)detail.SubmittedBusinessActivityID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };
                        if (!ActBusinessActivityMappingExists(ABAM))
                        {
                            entities.Act_BusinessActivityMapping.Add(ABAM);
                            entities.SaveChanges();
                        }

                        Act_ActApplicabilityMapping AAAM = new Act_ActApplicabilityMapping()
                        {
                            ActID = detail.SubmittedActID,
                            Act_ApplicabilityID = (int)detail.SubmittedActApplicabilityID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };
                        if (!ActApplicabilityMappingExists(AAAM))
                        {
                            entities.Act_ActApplicabilityMapping.Add(AAAM);
                            entities.SaveChanges();
                        }
                        var query = (from row in entities.Acts
                                     where row.ID == detail.SubmittedActID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.LocationTypeID = detail.SubmittedLocationTypeID;
                            entities.SaveChanges();
                        }

                    }
                    result = "Details Saved Successfully!";
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    
        #region Approved  Question  ACT Mapping
        public class QActApprovedMappingEntryUpdate
        {
            public List<QActApproved_MappingUpdate> BulkUpdates { get; set; }
        }
        public class QActApproved_MappingUpdate
        {
            public long ApprovedQACMMappingID { get; set; }
            public long ApprovedActID { get; set; }
            public long ApprovedQuestionID { get; set; }
            public long ApprovedControllID { get; set; }
            public Nullable<long> ApprovedServiceID { get; set; }
            public Nullable<long> ApprovedAnswer { get; set; }
            public string ApprovedAnswerSingle { get; set; }
            public string ApprovedAnswerMultiple { get; set; }
            public Nullable<long> ApprovedAnsweRangeMin { get; set; }
            public Nullable<long> ApprovedAnsweRangeMax { get; set; }
            public Nullable<long> ApprovedCompanyTypeID { get; set; }
            public Nullable<long> ApprovedBusinessActivityID { get; set; }
            public Nullable<long> ApprovedLocationTypeID { get; set; }
            public Nullable<long> ApprovedActApplicabilityID { get; set; }
        }
        public JsonResult getAllApprovedActs(int actgroupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.IMA_GetReviewerApprovedActDetails(actgroupid,AuthenticationHelper.UserID)
                                    select usr).ToList();
                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public string UpdateActApprovedQuestion(QActApproved_MappingUpdate detail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var query = (from row in entities.Questions_ActMapping
                                     where row.IsActive == false
                                          && row.Id == detail.ApprovedQACMMappingID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.ActId = detail.ApprovedActID;
                            query.QuestionID = detail.ApprovedQuestionID;
                            query.ControllId = detail.ApprovedControllID;
                            query.AnswerTOQ = detail.ApprovedServiceID;
                            query.AnswerValue = detail.ApprovedAnswer;
                            if (detail.ApprovedControllID == 1)
                            {
                                query.AnsweSingleMulti = detail.ApprovedAnswerSingle;
                            }
                            else if (detail.ApprovedControllID == 2)
                            {
                                query.AnsweSingleMulti = detail.ApprovedAnswerMultiple;
                            }
                            query.StatusId = 3;
                            query.AnsweRangeMin = detail.ApprovedAnsweRangeMin;
                            query.AnsweRangeMax = detail.ApprovedAnsweRangeMax;
                            query.UpdatedOn = DateTime.Now;
                            query.Updatedby = AuthenticationHelper.UserID;
                            query.LocationTypeID = detail.ApprovedLocationTypeID;
                            entities.SaveChanges();
                            Act_CompanyTypeMapping ACTM = new Act_CompanyTypeMapping()
                            {
                                ActID = detail.ApprovedActID,
                                TypeID = (int)detail.ApprovedCompanyTypeID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!Act_CompanyTypeMappingExists(ACTM))
                            {
                                entities.Act_CompanyTypeMapping.Add(ACTM);
                                entities.SaveChanges();
                            }
                            Act_BusinessActivityMapping ABAM = new Act_BusinessActivityMapping()
                            {
                                ActID = detail.ApprovedActID,
                                BATypeID = (int)detail.ApprovedBusinessActivityID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!ActBusinessActivityMappingExists(ABAM))
                            {
                                entities.Act_BusinessActivityMapping.Add(ABAM);
                                entities.SaveChanges();
                            }
                            Act_ActApplicabilityMapping AAAM = new Act_ActApplicabilityMapping()
                            {
                                ActID = detail.ApprovedActID,
                                Act_ApplicabilityID = (int)detail.ApprovedActApplicabilityID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!ActApplicabilityMappingExists(AAAM))
                            {
                                entities.Act_ActApplicabilityMapping.Add(AAAM);
                                entities.SaveChanges();
                            }
                            var query1 = (from row in entities.Acts
                                          where row.ID == detail.ApprovedActID
                                          select row).FirstOrDefault();
                            if (query1 != null)
                            {
                                query1.LocationTypeID = detail.ApprovedLocationTypeID;
                                entities.SaveChanges();
                            }


                            result = "Details Updated Successfully!";
                            dbContextTransaction.Commit();
                        }
                        else
                        {
                            result = "Question Not Found!";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }
        [HttpPost]
        public ActionResult BulkUpdateActApprovedQuestion(QActApprovedMappingEntryUpdate bupdate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (QActApproved_MappingUpdate detail in bupdate.BulkUpdates)
                    {
                        var query = (from row in entities.Questions_ActMapping
                                     where row.IsActive == false
                                          && row.Id == detail.ApprovedQACMMappingID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.ActId = detail.ApprovedActID;
                            query.QuestionID = detail.ApprovedQuestionID;
                            query.ControllId = detail.ApprovedControllID;
                            query.AnswerTOQ = detail.ApprovedServiceID;
                            query.AnswerValue = detail.ApprovedAnswer;
                            if (detail.ApprovedControllID == 1)
                            {
                                query.AnsweSingleMulti = detail.ApprovedAnswerSingle;
                            }
                            else if (detail.ApprovedControllID == 2)
                            {
                                query.AnsweSingleMulti = detail.ApprovedAnswerMultiple;
                            }
                            query.StatusId = 3;
                            query.AnsweRangeMin = detail.ApprovedAnsweRangeMin;
                            query.AnsweRangeMax = detail.ApprovedAnsweRangeMax;
                            query.UpdatedOn = DateTime.Now;
                            query.Updatedby = AuthenticationHelper.UserID;
                            query.LocationTypeID = detail.ApprovedLocationTypeID;
                            entities.SaveChanges();

                            Act_CompanyTypeMapping ACTM = new Act_CompanyTypeMapping()
                            {
                                ActID = detail.ApprovedActID,
                                TypeID = (int)detail.ApprovedCompanyTypeID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!Act_CompanyTypeMappingExists(ACTM))
                            {
                                entities.Act_CompanyTypeMapping.Add(ACTM);
                                entities.SaveChanges();
                            }
                            Act_BusinessActivityMapping ABAM = new Act_BusinessActivityMapping()
                            {
                                ActID = detail.ApprovedActID,
                                BATypeID = (int)detail.ApprovedBusinessActivityID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!ActBusinessActivityMappingExists(ABAM))
                            {
                                entities.Act_BusinessActivityMapping.Add(ABAM);
                                entities.SaveChanges();
                            }
                            Act_ActApplicabilityMapping AAAM = new Act_ActApplicabilityMapping()
                            {
                                ActID = detail.ApprovedActID,
                                Act_ApplicabilityID = (int)detail.ApprovedActApplicabilityID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!ActApplicabilityMappingExists(AAAM))
                            {
                                entities.Act_ActApplicabilityMapping.Add(AAAM);
                                entities.SaveChanges();
                            }
                            var query1 = (from row in entities.Acts
                                          where row.ID == detail.ApprovedActID
                                          select row).FirstOrDefault();
                            if (query1 != null)
                            {
                                query1.LocationTypeID = detail.ApprovedLocationTypeID;
                                entities.SaveChanges();
                            }
                        }
                    }
                    result = "Details Updated Successfully!";
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}