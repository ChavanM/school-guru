﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class EscalationController : Controller
    {
        // GET: Escalation
        public ActionResult SaveAllEscalationData(List<EscalationDetails> EscalationData)
        {
            string result = "";
            List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
            try
            {
                if (EscalationData.Count > 0)
                {
                    foreach (var item in EscalationData)
                    {
                        if (item.IntreimDays != null && item.EscDays != null)
                        {
                            int complianceID = Convert.ToInt32(item.complianceID);
                            int CustomerBranchID = Convert.ToInt32(item.CustomerBranchID);
                            int UserID = Convert.ToInt32(item.UserID);
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var NotificationRecordToUpdate = (from row in entities.ComplianceCustomEscalations
                                                                  where row.UserIdReviewer == UserID
                                                                  && row.ComplianceId == complianceID
                                                                  && row.CustomerBranchID == CustomerBranchID
                                                                  select row).FirstOrDefault();

                                if (NotificationRecordToUpdate == null)
                                {
                                    var transactionsQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                             where row.ComplianceID == complianceID
                                                             && row.RoleID == 3  //
                                                             && row.CustomerBranchID == CustomerBranchID
                                                             select (long)row.UserID).FirstOrDefault();

                                    ComplianceCustomEscalation cmpcustomescalation = new ComplianceCustomEscalation();
                                    {
                                        cmpcustomescalation.ComplianceId = Convert.ToInt32(item.complianceID);
                                        cmpcustomescalation.days = Convert.ToInt32(item.EscDays);
                                        cmpcustomescalation.Interimdays = Convert.ToInt32(item.IntreimDays);
                                        cmpcustomescalation.UserIdReviewer = UserID;
                                        cmpcustomescalation.CustomerBranchID = Convert.ToInt32(CustomerBranchID);
                                        cmpcustomescalation.UserIdPerformer = Convert.ToInt32(transactionsQuery);
                                    };

                                    CreateEscalationReminders(cmpcustomescalation);
                                    result = "Successfully Saved";
                                }
                                else
                                {
                                    ComplianceCustomEscalation cmpcustomescalation1 = new ComplianceCustomEscalation();
                                    {
                                        cmpcustomescalation1.ComplianceId = Convert.ToInt32(complianceID);
                                        cmpcustomescalation1.days = Convert.ToInt32(item.EscDays);
                                        cmpcustomescalation1.Interimdays = Convert.ToInt32(item.IntreimDays);
                                        cmpcustomescalation1.CustomerBranchID = Convert.ToInt32(CustomerBranchID);
                                        cmpcustomescalation1.UserIdReviewer = Convert.ToInt32(UserID);
                                    };
                                    UpdateEscalation(cmpcustomescalation1);
                                    result = "Successfully Updated";
                                }
                            }
                        }
                    }
                }
                return Json(new { status = "success", message = "Saved Successfully" });

            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = "Please select atleast one event" });
            }
        }
        public class UpdateNotification
        {
            public string Message { get; set; }
        }
        public class EscalationDetails
        {
            public string Frequency { get; set; }
            public long complianceID { get; set; }
            public string Branch { get; set; }
            public string ShortDescription { get; set; }
            public string IntreimDays { get; set; }
            public string EscDays { get; set; }
            public int CustomerBranchID { get; set; }
            public string EventFlag { get; set; }
            public string ActID { get; set; }
            public string ActName { get; set; }
            public string User { get; set; }
            public int UserID { get; set; }
        }
        public static void CreateEscalationReminders(ComplianceCustomEscalation cmpcustomescalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ComplianceCustomEscalations.Add(cmpcustomescalation);
                entities.SaveChanges();
            }
        }
        public static void UpdateEscalation(ComplianceCustomEscalation UpdateEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.ComplianceCustomEscalations
                                                  where row.UserIdReviewer == UpdateEscalation.UserIdReviewer
                                                  && row.ComplianceId == UpdateEscalation.ComplianceId
                                                  && row.CustomerBranchID == UpdateEscalation.CustomerBranchID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.Interimdays = UpdateEscalation.Interimdays;
                    NotificationRecordToUpdate.days = UpdateEscalation.days;
                    entities.SaveChanges();
                }
            }
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}